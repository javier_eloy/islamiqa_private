<?php
//	error_reporting(E_ALL);
	
	require_once 'qa-include/qa-base.php';
	require_once 'qa-include/qa-app-users.php';
	require_once 'qa-include/qa-app-posts.php';
	require_once 'qa-include/qa-db-admin.php'; // categories
	
	$loop = 0;
	
	if (($handle = fopen("DEVUPLOADFILE.txt", "r")) != FALSE) {
		while (($fields = fgetcsv($handle, 0, "\t")) !== FALSE) {
			if (strpos($fields[0], 'Question') !== false || $fields[0] == "") continue; // skip header or blank records
			
			$loop++;
			// check if question title already exists, prevent duplicate 
			if (qa_db_read_one_value( qa_db_query_sub('SELECT title FROM `^posts` WHERE title = # AND type = "Q" LIMIT 1', $fields[0]), true)) {
				echo "($loop) '$fields[0]' : is a duplicate question - excluding from database <br/>";
				continue;
			} else {
				echo "($loop) '$fields[0]' : imported successfully <br/>";
			}
			
//			var_dump($fields);
			
			// post a question
			$type       = 'Q'; // question
			$parentid   = null; // does not follow another answer
			$title      = $fields[0];
			$content    = $fields[1];
			$format     = ''; // plain text
			//$categoryid = qa_db_read_one_value(qa_db_query_sub('SELECT categoryid FROM ^categories WHERE parentid<=># AND title=$',NULL, $fields[2]), true);
			$categoryid = qa_db_read_one_value(qa_db_query_sub('SELECT categoryid FROM ^categories WHERE title=$',$fields[2]), true);
			$tags       = array($fields[5]);
			$userhandle = ($fields[6]=='' ? null : $fields[6]);
			$userid     = qa_handle_to_userid($userhandle);
			echo $userhandle . "---". $userid . "<br/>";
			$parentid = qa_post_create($type, $parentid, $title, $content, $format, $categoryid, $tags, $userid);

			$ctr = 10;
			while ($fields[$ctr] != '') {
				// post an answer
				$type       = 'A'; // question
				$title      = '';
				$content    = $fields[$ctr];
				$format     = ''; // plain text
				$categoryid = ''; 
				$tags       = '';
				$userhandle = ($fields[$ctr-2]=='' ? null : $fields[$ctr-2]);
				$userid     = qa_handle_to_userid($userhandle);
				qa_post_create($type, $parentid, $title, $content, $format, $categoryid, $tags, $userid);
				$ctr += 3;
				if ($fields[$ctr+3] != '') sleep(1);
			}
		}
	} else {  // error opening the file.
		echo "Could not open file!";
		exit;
	} 

	fclose($handle);	

	echo 'Complete'
?>