<?php
//	error_reporting(E_ALL);
	
	require_once 'qa-include/qa-base.php';
	require_once 'qa-include/qa-app-users.php';
	require_once 'qa-include/qa-app-posts.php';
	require_once 'qa-include/qa-db-admin.php'; // categories
		
	// type: t-topic, s-subtopic, u-subsidiary, w-words
	//if (($handle = fopen("DEVUPLOADTopics.csv", "r")) != FALSE) {
	if (($handle = fopen("DEVUploadTopics.txt", "r")) != FALSE) {
		//while (($fields = fgetcsv($handle, 0, ",")) !== FALSE) { // \t
		while (($fields = fgetcsv($handle, 0, "\t")) !== FALSE) { 
			if ($fields[0] == "" Or $fields[0] == "\t") continue; // skip header or blank records
			if (strpos($fields[0], "ï»¿") !== false) {
				echo "!!!$fields[0]<br/>";
				$fields[0] = str_replace("ï»¿", "", $fields[0]);
			} 
			/*else {
				echo "$fields[0]<br/>";
			}*/
			
			$loop = 0;
			$parentid = "";
			
			// check if topic already exists, prevent duplicates 
			$id = qa_db_read_one_value(qa_db_query_sub('SELECT id FROM `^islamiqa_topics` WHERE title = $ AND type = "t" LIMIT 1', $fields[0]), true);
			if ($id == "") {
				echo "1 ADDED: " . $fields[0] . "<br/>";
				qa_db_query_sub('INSERT INTO `^islamiqa_topics` (title, type) VALUES ($, "t")', $fields[0]);
				$id = qa_db_read_one_value(qa_db_query_sub('SELECT id FROM `^islamiqa_topics` WHERE title = $ LIMIT 1', $fields[0]), true);
				if ($id <> "") $parentid = $id;
				echo "1 NEW ID: " . $id . " - $parentid<br/>";				
			} else {
				$parentid = $id;
			}
			$loop++;
			
			// check if subtopic topic already exists, prevent duplicates 
			if (strlen($fields[1]) > 1) {
				$id = qa_db_read_one_value(qa_db_query_sub('SELECT id FROM `^islamiqa_topics` WHERE title = $ AND type = "s" LIMIT 1', $fields[1]), true);
				if ($id == "") {
					echo "2 ADDED: " . $fields[1] . "<br/>";
					qa_db_query_sub('INSERT INTO `^islamiqa_topics` (title, parentid, type) VALUES ($, #, "s")', $fields[1], $parentid);
					$id = qa_db_read_one_value(qa_db_query_sub('SELECT id FROM `^islamiqa_topics` WHERE title = $ LIMIT 1', $fields[1]), true);
					if ($id <> "") $parentid = $id;
					echo "2 NEW ID: " . $id . " - $parentid<br/>";				
				} else {
					$parentid = $id;
				}
			}
			$loop++;
				
			// check if subsidiary topic already exists, prevent duplicates 
			if (strlen($fields[2]) > 1) {
				$id = qa_db_read_one_value(qa_db_query_sub('SELECT id FROM `^islamiqa_topics` WHERE title = $ AND type = "u" LIMIT 1', $fields[2]), true);
				if ($id == "") {
					echo "3 ADDED: " . $fields[2] . "<br/>";
					qa_db_query_sub('INSERT INTO `^islamiqa_topics` (title, parentid, type) VALUES ($, #, "u")', $fields[2], $parentid);
					$id = qa_db_read_one_value(qa_db_query_sub('SELECT id FROM `^islamiqa_topics` WHERE title = $ LIMIT 1', $fields[2]), true);
					if ($id <> "") $parentid = $id;
					echo "3 NEW ID: " . $id . " - $parentid<br/>";				
				} else {
					$parentid = $id;
				}
			}
			$loop++;			

			while (strlen($fields[$loop]) > 1) {				
				$title = $fields[$loop]; // title
				qa_db_query_sub('INSERT INTO `^islamiqa_topics` (title, parentid, type) VALUES ($, #, "w")', $fields[$loop], $parentid);
				$loop++;
				echo "$loop ADDED ID: " . $title . "<br/>";
				if ($loop >= count($fields)) break;
			}
		}
	} else {  // error opening the file.
		echo "Could not open file!";
		exit;
	} 

	fclose($handle);	

	echo 'Complete'
?>