<?php
	
	if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}



/**
 * Defines the base directory of the theme 
 */
if (!defined('ISLAMIQA_THEME_BASE_DIR')) {
	define('ISLAMIQA_THEME_BASE_DIR', dirname(__FILE__));
}
/**
 * define the directory name of the theme directory 
 */
if (!defined('ISLAMIQA_THEME_BASE_DIR_NAME')) {
	define('ISLAMIQA_THEME_BASE_DIR_NAME', basename(dirname(__FILE__)));
}

/**
 * define the theme root URL
 */
if (!defined('ISLAMIQA_THEME_ROOT_URL')) {
	define('ISLAMIQA_THEME_ROOT_URL', qa_opt('site_url').'qa-theme/'.ISLAMIQA_THEME_BASE_DIR_NAME);
}

/**
 * Load format
 */
include_once ISLAMIQA_THEME_BASE_DIR . '/include/format.php';

