<?php

require_once 'qa-include/qa-app-posts.php';

echo '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>';

class qa_html_theme extends qa_html_theme_base {

	// use new ranking layout

	protected $ranking_block_layout = true;

	public function html() {
		$this->head();
		$this->body();
	}

	// outputs login form if user not logged in
	public function nav_user_search() {
		if (!qa_is_logged_in()) {
			$login = @$this->content['navigation']['user']['login'];

			if (isset($login) && !QA_FINAL_EXTERNAL_USERS) {
				$this->output(
						'<!--[Begin: login form]-->', '<form id="qa-loginform" action="' . $login['url'] . '" method="post">', '<input type="text" id="qa-userid" name="emailhandle" placeholder="' . trim(qa_lang_html(qa_opt('allow_login_email_only') ? 'users/email_label' : 'users/email_handle_label'), ':') . '" />', '<input type="password" id="qa-password" name="password" placeholder="' . trim(qa_lang_html('users/password_label'), ':') . '" />', '<div id="qa-rememberbox"><input type="checkbox" name="remember" id="qa-rememberme" value="1"/>', '<label for="qa-rememberme" id="qa-remember">' . qa_lang_html('users/remember') . '</label></div>', '<input type="hidden" name="code" value="' . qa_html(qa_get_form_security_code('login')) . '"/>', '<input type="submit" value="' . $login['label'] . '" id="qa-login" name="dologin" />', '</form>', '<!--[End: login form]-->'
				);

				unset($this->content['navigation']['user']['login']); // removes regular navigation link to log in page
			}
		}

		/* MODIFICATION qa_html_theme_base::nav_user_search(); */

		$this->nav('main');  // MODIFICATION
		/* 		$this->search(); */  // Search has been moved next to ask qu - no longer needed in nav bar
		$this->nav('user');
	}

	public function nav_main_sub() {
		/* MODIFICATION
		  $this->nav('main'); */

		if (in_array($this->template, array('questions', 'admin', 'user', 'users', 'account', 'favorites', 'user-wall', 'messages',
					'user-activity', 'user-questions', 'user-answers', 'plugin')))
			$this->nav('sub');
	}

	/* MODIFICATION */

	function nav_list($navigation, $class, $level = null) {
		$this->output('<UL CLASS="qa-' . $class . '-list' . (isset($level) ? (' qa-' . $class . '-list-' . $level) : '') . '">');

		if ($class == 'nav-main') {
			$new_order = array('activity', 'unanswered', 'questions', 'ask', 'tag', 'user', 'admin', 'custom-1');
			$temp_nav = array();
			foreach ($new_order as $key) {
				if (isset($navigation[$key]))
					$temp_nav[$key] = $navigation[$key];
			}
			$navigation = $temp_nav;
		}

		$index = 0;
		foreach ($navigation as $key => $navlink) {
			$this->set_context('nav_key', $key);
			$this->set_context('nav_index', $index++);
			$this->nav_item($key, $navlink, $class, $level);
		}

		$this->clear_context('nav_key');
		$this->clear_context('nav_index');

		$this->output('</UL>');
	}

	public function logged_in() {
		if (qa_is_logged_in()) // output user avatar to login bar
			$this->output(
					'<div class="qa-logged-in-avatar">', QA_FINAL_EXTERNAL_USERS ? qa_get_external_avatar_html(qa_get_logged_in_userid(), 24, true) : qa_get_user_avatar_html(qa_get_logged_in_flags(), qa_get_logged_in_email(), qa_get_logged_in_handle(), qa_get_logged_in_user_field('avatarblobid'), qa_get_logged_in_user_field('avatarwidth'), qa_get_logged_in_user_field('avatarheight'), 24, true), '</div>'
			);

		qa_html_theme_base::logged_in();

		if (qa_is_logged_in()) { // adds points count after logged in username
			$userpoints = qa_get_logged_in_points();

			$pointshtml = ($userpoints == 1) ? qa_lang_html_sub('main/1_point', '1', '1') : qa_lang_html_sub('main/x_points', qa_html(number_format($userpoints)));

			$this->output(
					'<span class="qa-logged-in-points">', '(' . $pointshtml . ')', '</span>'
			);
		}
	}

	// adds login bar, user navigation and search at top of page in place of custom header content
	public function body_header() {
		$this->output('<div id="qaDEV-entire-login-bar">');
		$this->logo(); // MODIFICATION

		$this->output('<div id="qa-login-bar">');
		// RSS Feed
		$feed = @$this->content['feed'];
		if (!empty($feed))
			$this->output('<a href="' . $feed['url'] . '" title="' . @$feed['label'] . '"><img src="' . $this->rooturl . 'images/rss.jpg" alt="" width="16" height="16" border="0" class="qa-rss-icon"/></a>');

		$this->output('<div id="qa-login-group">');
		$this->nav_user_search(); // Nav Bar
		$this->output('</div>');

		$this->output('</div>');

		$this->output('</div>');
	}

	// allows modification of custom element shown inside header after logo
	public function header_custom() {
		if (isset($this->content['body_header'])) {
			$this->output('<div class="header-banner">');
			$this->output_raw($this->content['body_header']);
			$this->output('</div>');
		}
	}

	// removes user navigation and search from header and replaces with custom header content. Also opens new <div>s
	public function header() {
		$this->output('<div class="qa-header">');

		//$this->logo();
		$this->header_clear();
		$this->header_custom();

		$this->output('</div> <!-- END qa-header -->', '');

		$this->output('<div class="qa-main-shadow">', '');
		$this->output('<div class="qa-main-wrapper">', '');
		$this->nav_main_sub();
	}

	public function logo() {
		$this->output(
				'<div class="qa-logo">', $this->content['logo'], '</div>'
		);
	}

	public function body_content() {
		if (!qa_is_logged_in() && $this->template == 'login') {
			$this->output('
			<div class="slideshow_area">
				<div class="container">
					<div class="signup-wrapper1" id="signup-wrapper1" style="display:inline">
						<div class="signup">
							<div class="button_primary_google signup_button button_block open-login-button context-menu action-login google"><a href="./?qa=login&login=google&amp;to=/qa-theme/Islamiqa/Page-Interests/islamiqa-interests-page.php" style="color:#fff;text-decoration:none;" >Sign up with Google</a></div>
							<div class="button_primary_facebook signup_button button_block open-login-button context-menu action-login facebook"><a href="./?qa=login&login=facebook&amp;to=/qa-theme/Islamiqa/Page-Interests/islamiqa-interests-page.php" style="color:#fff;text-decoration:none;" >Sign up with Facebook</a></div>
							<div class="button_primary_twitter signup_button button_block open-login-button context-menu action-login twitter"><a href="./?qa=login&login=twitter&amp;to=/qa-theme/Islamiqa/Page-Interests/islamiqa-interests-page.php" style="color:#fff;text-decoration:none;" >Sign up with Twitter</a></div>
								<p class="text-muted" >Alternatively, <a href="./index.php?qa=register&amp;to=/qa-theme/Islamiqa/Page-Interests/islamiqa-interests-page.php">Sign Up With Email</a>. By clicking Sign up, you agree to our terms of service and privacy policy. We will send you account related emails occasionally.</p>
						</div>
						<div class="signup-icon">
							<div class="icon_primary icon_block1">Google</div>
							<div class="icon_primary icon_block2">Facebook</div>
							<div class="icon_primary icon_block3">Twitter</div>
						</div>
					</div>
					<div class="main-heading">
						<p class="heading"><span class="textspacing">ISLAM!</span><br/>The Questions, The Answers</p>
						<p class="subheading">We are on a mission to dispel myths and stereotypes by crowdsourcing the best content on Islam, its civilisation and its people.
						<br/>Share knowledge, reduce fear and increase understanding by signing up or clicking on the menu to start...</p>
					</div>
				</div>
			</div>');
			$this->output('
			<div class="container">
					<p class="heading2">Why you\'ll love Islamiqa</p>
					<p class="subheading2">High quality content, delivered collaboratively...</p>
					<div class="one-half column1">
						<div class="octicon1">...</div>
						<p class="heading3">Great answers start with great insights</p>
						<p class="subheading3">Questions are answered by people with a deep interest in the subject.</p>
					</div>
					<div class="one-half column2">
						<div class="octicon2">...</div>
						<p class="heading3">Multiple perspectives through collaboration</p>
						<p class="subheading3">People from around the world review questions, post answers and add comments.</p>
					</div>
					<div class="one-half column1">
						<div class="octicon3">...</div>
						<p class="heading3">Vote content up or down</p>
						<p class="subheading3">Content becomes intriguing when it is voted up or down - ensuring the best answers float to the top.</p>
					</div>
					<div class="one-half column2">
						<div class="octicon4">...</div>
						<p class="heading3">Building an authoritative community</p>
						<p class="subheading3">Be part of the global discussion that is defining our generation and generations to come.</p>
					</div>
			</div>');

			$this->body_suffix();
			$this->body_hidden();
			$this->widgets('full', 'bottom');
		} else {
//			qa_html_theme_base::body_content();

			$this->body_prefix();
			$this->notices();

			$this->output('<div class="qa-body-wrapper">', '');

			$this->widgets('full', 'top');
			$this->header();

			if (!in_array($this->template, array('question', 'admin', 'user', 'users', 'account', 'favorites', 'user-wall', 'messages',
						'user-activity', 'user-questions', 'user-answers', 'plugin'))) {
				$this->widgets('full', 'high');
				$this->output('<div class="search-function">');
				$this->output('<p class="search-function-title">FIND A QUESTION:</p>');
				$this->search();
				$this->output('</div>');
			}

			$this->sidepanel();
			$this->main();
			$this->widgets('full', 'low');
			$this->footer();
			$this->widgets('full', 'bottom');

			$this->output('</div> <!-- END body-wrapper -->');

			$this->body_suffix();
		}
	}

	public function sidepanel() {

		$this->output('<div class="qa-sidepanel">');
		echo '<script>
				// Popup window code
				function newPopup(url) {
					popupWindow = window.open(url,"popUpWindow",
					"height=450,width=1000,left=20,top=20,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
				}
			 </script>';

		$this->output('<div class="qa-q-left">', '');
		$this->islamiqa_show_my_interests();
		$this->islamiqa_show_my_knowledges();
		$this->islamiqa_show_my_follows();
		$this->output('</div> <!-- END qa-q-left -->', '');

		$this->widgets('side', 'top');
		$this->sidebar();
		$this->widgets('side', 'high');
		$this->nav('cat', 1);
		$this->widgets('side', 'low');
		$this->output_raw(@$this->content['sidepanel']);
		$this->feed();
		$this->widgets('side', 'bottom');
		$this->output('</div>', '');
	}

	public function islamiqa_show_my_interests() {
		//if (qa_is_logged_in() && ($this->template == 'qa' || $this->template == 'tags' || $this->template == 'favorites' || $this->template == 'unanswered')) {

		if (qa_is_logged_in() && $this->template == 'updates') {
			$this->output('<script type="text/javascript"> $(function(){
				$(".mytopic").click(function(){
					$(".qadev-lhs-q").toggle(300);
					if ($(".mytopicp").text() == "+") {
						$(".mytopicp").text("-");
					} else {
						$(".mytopicp").text("+");
					}

					// Save config setting to database as qa_opt field
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
					}

					interests = ($(".mytopicp").text() == "-") ? 1:0;
					xmlhttp.open("GET","qa-theme/Islamiqa/Page-Interests/savemyinterestsknowledges.php?i="+interests);
					xmlhttp.send();
					});
				});

				</script>');

			$this->output('<h3><a class="mytopic" href="#!"><span class="mytopicp" >' . (qa_opt('interests') ? '-' : '+') . '</span> My Interests</a>', '');
			$this->output('<a class="TopicKnowledgeEdit" style="text-decoration:none"
				href="JavaScript:newPopup(' . "'" . 'qa-theme/Islamiqa/Page-Interests/islamiqa-edit-interests-page.php?p=i' . "'" . ');">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Edit</a></h3>', '');

			$favorites = qa_db_read_all_assoc(qa_db_query_sub(
							'SELECT uf.userid, qw.word FROM ^userfavorites uf LEFT JOIN ^words qw ON uf.entityid = qw.wordid WHERE uf.entitytype = "T" AND uf.userid=# ', qa_get_logged_in_userid()
			));

			foreach ($favorites as $list) {
				$this->output('<div class="qadev-lhs-q" style="display:' . (qa_opt('interests') ? 'block' : 'none') . '"> ' .
						'<img class="qadev-lhs-q2" style="float:left; display:' . (qa_opt('interests') ? 'inline' : 'inline') .
						'" src=qa-theme/Islamiqa/Page-Interests/images/' . str_replace(" ", "_", strtolower($list['word'])) . '.jpg alt="' . $list['word'] .
						'" height="21" width="21"> </img><ul style="float:left; display:' . (qa_opt('interests') ? 'inline' : 'inline') . '">&nbsp;&nbsp;<a href="' .
						qa_path_html('tag/' . $list['word']) . '">' . $list['word'] . '</a></ul><br/><br/></div>');
			}
		}
	}

	public function islamiqa_show_my_follows() {
		if (qa_is_logged_in() && in_array($this->template, array('updates'))) {
			$this->output('<script type="text/javascript"> $(function(){
				$(".myfollows").click(function(){
					$(".qadev-lhs-f").toggle(300);
					if ($(".myfollowsp").text() == "+") {
						$(".myfollowsp").text("-");
					} else {
						$(".myfollowsp").text("+");
					}
					// Save config setting to database as qa_opt field
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
					}

					follows = ($(".myfollowsp").text() == "-") ? 1:0;
					xmlhttp.open("GET","qa-theme/Islamiqa/Page-Interests/savemyinterestsknowledges.php?f="+follows);
					xmlhttp.send();
					});
				});

				</script>');

			$this->output('<h3><a class="myfollows" href="#!"><span class="myfollowsp" >' . (qa_opt('follows') ? '-' : '+') . '</span> I\'m Following</a>', '');
			$this->output('<a class="TopicKnowledgeEdit" style="text-decoration:none"
						href="?qa=users">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Edit</a></h3>', '');

			$favorites = qa_db_read_all_assoc(qa_db_query_sub(
							'SELECT uf.userid, u.flags, u.email, u.handle, u.avatarblobid, u.avatarwidth, u.avatarheight FROM ^userfavorites uf
				LEFT JOIN ^users u ON uf.entityid = u.userid WHERE uf.entitytype = "U" AND uf.userid=#', qa_get_logged_in_userid()
			));

			$i = 0;
			foreach ($favorites as $list) {
				// get user data
				$flags = $favorites[$i]['flags'];
				$email = $favorites[$i]['email'];
				$handle = $favorites[$i]['handle'];
				$avatarblobid = $favorites[$i]['avatarblobid'];
				$avatarwidth = $favorites[$i]['avatarwidth'];
				$avatarheight = $favorites[$i]['avatarheight'];
				$avatar = qa_get_user_avatar_html($flags, $email, $handle, $avatarblobid, $avatarwidth, $avatarheight, 22, false);

				$this->output('<div class="qadev-lhs-f" style="display:' . (qa_opt('follows') ? 'block' : 'none') . '">
									<div class="qadev-lhs-f2" style="float:left; display:' . (qa_opt('follows') ? 'inline' : 'inline') . '">' . $avatar . '</div>
									<ul style="float:left; display:' . (qa_opt('follows') ? 'inline' : 'inline') . ';"> &nbsp;&nbsp;
									<a href="' . qa_path_html('user/' . $list['handle'] . '/activity') . '">' . $list['handle'] . '</a></ul><br/><br/>
							   </div>');
				$i++;
			}
		}
	}

	public function islamiqa_show_my_knowledges() {
		if (qa_is_logged_in() && $this->template == 'unanswered') {
			$this->output('<script> $(function(){
				$(".myknowledge").click(function(){
					$(".qadev-lhs-k").toggle(300);
					if ($(".myknowledgep").text() == "+") {
						$(".myknowledgep").text("-");
					} else {
						$(".myknowledgep").text("+");
					}

					// Save config setting to database as qa_opt field
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
					}

					knowledges = ($(".myknowledgep").text() == "-") ? 1:0;
					xmlhttp.open("GET","qa-theme/Islamiqa/Page-Interests/savemyinterestsknowledges.php?k="+knowledges);
					xmlhttp.send();
					});
				});
				</script>');

			$this->output('<div class="qa-q-left">', '');
			$this->output('<h3><a class="myknowledge" href="#!"><span class="myknowledgep" >' . (qa_opt('knowledges') ? '-' : '+') . '</span> My Expertise</a>', '');
			$this->output('<a class="TopicKnowledgeEdit" style="text-decoration:none"
						href="JavaScript:newPopup(' . "'" . 'qa-theme/Islamiqa/Page-Interests/islamiqa-edit-interests-page.php?p=k' . "'" . ');">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Edit</a></h3>', '');

			$favorites = qa_db_read_all_assoc(qa_db_query_sub(
							'SELECT uf.userid, qw.word FROM ^userfavorites uf LEFT JOIN ^words qw ON uf.entityid = qw.wordid WHERE uf.entitytype = "K" AND uf.userid=# ', qa_get_logged_in_userid()
			));

			foreach ($favorites as $list) {
				$this->output('<div class="qadev-lhs-k" style="display:' . (qa_opt('knowledges') ? 'block' : 'none') . '"> ' .
						'<img class="qadev-lhs-k2" style="float:left; display:' . (qa_opt('knowledges') ? 'inline' : 'inline') .
						'" src=qa-theme/Islamiqa/Page-Interests/images/' . str_replace(" ", "_", strtolower($list['word'])) . '.jpg alt=<' . $list['word'] .
						' height="21" width="21"> </img><ul style="float:left; display:' . (qa_opt('knowledges') ? 'inline' : 'inline') . '">&nbsp;&nbsp;<a href="' .
						qa_path_html('tag/' . $list['word']) . '">' . $list['word'] . '</a></ul><br/><br/></div>');
			}

			$this->output('</div> <!-- END qa-q-left -->', '');
		}
	}

	// prevent display of regular footer content (see body_suffix()) and replace with closing new <div>s
	public function footer() {
		$this->output('</div> <!-- END main-wrapper -->');
		$this->output('</div> <!-- END main-shadow -->');
	}

	// add RSS feed icon after the page title
	public function title() {
		qa_html_theme_base::title();

//		$feed=@$this->content['feed'];
//		if (!empty($feed))
//			$this->output('<a href="'.$feed['url'].'" title="'.@$feed['label'].'"><img src="'.$this->rooturl.'images/rss.jpg" alt="" width="16" height="16" border="0" class="qa-rss-icon"/></a>');
	}

	public function q_view_main($q_view) {
		$this->output('<div class="qa-q-view-main">');

		if (isset($q_view['main_form_tags']))
			$this->output('<form ' . $q_view['main_form_tags'] . '>'); // form for buttons on question

		$this->post_avatar_meta($q_view, 'qa-q-view');
		$this->q_view_content($q_view);
		$this->q_view_extra($q_view);
		$this->q_view_follows($q_view);
		$this->q_view_closed($q_view);
		$this->post_tags($q_view, 'qa-q-view');

		$this->q_view_buttons($q_view);

		$this->c_list(isset($q_view['c_list']) ? $q_view['c_list'] : null, 'qa-q-view');

		if (isset($q_view['main_form_tags'])) {
			if (isset($q_view['buttons_form_hidden']))
				$this->form_hidden_elements($q_view['buttons_form_hidden']);
			$this->output('</form>');
		}

		$this->c_form(isset($q_view['c_form']) ? $q_view['c_form'] : null);

		$this->output('</div> <!-- END qa-q-view-main -->');
	}

	public function q_view_buttons($q_view) {
		if (!empty($q_view['form'])) {
			$this->output('<div class="qa-q-view-buttons">');
			$this->form($q_view['form']);
			$this->output('<a id="submenu" class="qa-submenu" href="#" style="right: 10px;">...</a>');
			$this->output('</div>');
		}
	}

	public function q_list($q_list) {

		//	Collect the question ids of all items in the question list (so we can do this in one DB query)
		if (count(@$q_list['qs'])) { // first check it is not an empty list and the feature is turned on
			$postids = array();
			foreach ($q_list['qs'] as $question)
				if (isset($question['raw']['postid']))
					$postids[] = $question['raw']['postid'];

			if (count($postids)) {
				//	Retrieve the content for these questions from the database and put into an array
				$result = qa_db_query_sub('SELECT postid, content, format, categoryid FROM ^posts WHERE postid IN (#)', $postids);
				$postinfo = qa_db_read_all_assoc($result, 'postid');

				//	Get the regular expression fragment to use for blocked words and the maximum length of content to show
				$blockwordspreg = qa_get_block_words_preg();
				$maxlength = 250;
			}
		}

		qa_html_theme_base::q_list($q_list); // call back through to the default function
	}

	public function favorite() {
		$favorite = isset($this->content['favorite']) ? $this->content['favorite'] : null;
		if (isset($favorite)) {
			$favoritetags = isset($favorite['favorite_tags']) ? $favorite['favorite_tags'] : '';
			$this->output('<span class="qa-favoriting" ' . $favoritetags . '>');
			$favorite = str_replace('return qa_favorite_click(this);', 'qa_favorite_click(this);app();', $favorite); // modification - force redraw!
			$this->favorite_inner_html($favorite);
			$this->output('</span>');
		}
	}

	public function q_list_item($q_item) {
		$this->output('<div class="qa-q-list-item' . rtrim(' ' . @$q_item['classes']) . '" ' . @$q_item['tags'] . '>');

		$this->q_item_main($q_item);
		$this->q_item_clear();

		$this->islamiqa_show_partial_answer($q_item);

		$this->q_item_stats($q_item);
		$this->q_item_clear();

		$this->output('</div> <!-- END qa-q-list-item -->', '');
	}

	public function q_item_content($q_item) {
		if (!empty($q_item['raw']['content'])) {
			$this->output('<div class="qa-q-item-content">');
			$this->output_raw($q_item['raw']['content']);
			$this->output('</div>');
		}
	}

	public function islamiqa_show_partial_answer($q_item) {
		$answercontent = $content = $remainingpostcontent = $otheranswers = $otheranswerslink = "";
		$rating = 0;

		// This should appear on page once
		// Used to hide/show comments
		$this->output("<script type='text/javascript'>
				function toggleMore(elm) {
					var linkContainer = elm.parentNode.getElementsByTagName('div')[0];
					var contentContainer = elm.parentNode.getElementsByTagName('div')[1];

					linkContainer.style.display =  'none' ;
					contentContainer.style.display = (contentContainer.style.display == 'inline' ) ? 'none' : 'inline';
				}
				function toggleMoreAnswers(elm) {
					var contentContainer = elm.parentNode.getElementsByTagName('div')[3];
					contentContainer.style.display = (contentContainer.style.display == 'inline' ) ? 'none' : 'inline';
				} </script>");

		// Get all the answers/comments for this post
		$childposts = qa_db_single_select(qa_db_full_child_posts_selectspec(null, $q_item['raw']['postid']));

		// Extract the answers and identify the one with the most votes
		foreach ($childposts as $postid => $answer) {
			if ($answer['basetype'] == 'A') {
				if ($answer['upvotes'] >= $rating) { // identify and split the highest ranking post
					$rating = $answer['upvotes'] + 1;

					// Save previous highest ranked answer in list of answers
					if ($answercontent != '')
						$otheranswers = $otheranswers . '<br/>' . $answercontent . $this->q_dev_get_avatar($answer) . '<br/>';

					// Newest highest ranked answer
					$answercontent = str_replace("</iframe>", "</iframe><br/><br/>", $this->embed_replace($answer['content']));
				} else { //add to the list of other answers
					$otheranswers = $otheranswers . str_replace("</iframe>", "</iframe><br/><br/>", $this->embed_replace($answer['content'])) . $this->q_dev_get_avatar($answer) . '<br/>';
				}
			}
		}

		// Shorten text
		$tag = 'div';
		$len = 300;
		$text = $answercontent;
		// Old trim content
		if (strlen(strip_tags(preg_replace("/<\/?" . $tag . "(.|\s)*?>/", '', $text))) > $len) {
			$content = substr(preg_replace("/<\/?" . $tag . "(.|\s)*?>/", '', $text), 0, $len);
			$remainder = substr(preg_replace("/<\/?" . $tag . "(.|\s)*?>/", '', $text), $len);
		} else {
			// if there is less then defined character count
			$content = preg_replace("/<\/?" . $tag . "(.|\s)*?>/", '', $text);
			$remainder = "";
		}

		/* if ($text <> "")
		  $content = Html::trim($text, 77); */

		// Display answers and avatars - qa_shorten_string_line to shorten!
		$this->output('<div class="devqa-q-answers">');

		echo $content . '<div class="devqa-q-more" onclick="toggleMore(this)">';
		echo ($remainder == '') ? '</div>' : '...<a href="#!">(read more)</a></div>';

		echo '<div class="devqa-q-moreanswers">';
		echo nl2br($remainder); // new lines to html break lines
		echo '</div>';

		// Show "other answers" link if there are multiple answers
		if ($otheranswers != '') {
			$this->output($this->q_dev_get_avatar($answer) . '<div class="devqa-q-others" onclick="toggleMoreAnswers(this)">');
			$this->output('<a href="#!">' . (count($childposts) - 1) . ' more answers </a>');
			$this->output('</div>');
			$this->output('<div class="devqa-q-otheranswers">');
			$this->output(nl2br($otheranswers));
			$this->output('</div>');
		}
		$this->output('</div>');
	}

	// Get avatar details and construct output html
	public function q_dev_get_avatar($answer) {
		$user_details = "";

		if (isset($answer['handle'])) {
			//$username = qa_db_read_all_assoc(qa_db_query_sub('SELECT handle FROM ^users WHERE userid = #',$answer['userid']), 'handle');
			//$user = qa_db_select_with_pending( qa_db_user_account_selectspec($username, false) );
			//$revlink = isset($answer['title']) ? $answer['title'] : "Answer was edited";
			//$htmloutput = (isset($answer['created']) ? date("Y-m-d H:m", strtotime($answer['created'])) : "");
			$user = qa_db_read_all_assoc(qa_db_query_sub(
							'SELECT u.flags, u.email, u.handle, u.avatarblobid, u.avatarwidth, u.avatarheight FROM ^users u WHERE u.handle=$', $answer['handle']));

			// get user data
			$flags = $user[0]['flags'];
			$email = $user[0]['email'];
			$handle = $user[0]['handle'];
			$avatarblobid = $user[0]['avatarblobid'];
			$avatarwidth = $user[0]['avatarwidth'];
			$avatarheight = $user[0]['avatarheight'];
			$avatar = qa_get_user_avatar_html($flags, $email, $handle, $avatarblobid, $avatarwidth, $avatarheight, 20, false);

//			$avatar = qa_get_user_avatar_html($user['flags'], $user['email'], $user['handle'], $user['avatarblobid'], $user['avatarwidth'], $user['avatarheight'], qa_opt('avatar_users_size'), false);

			$user_details = '<span class="devqa-q-answers-respondent"><span class="qa-q-item-what">answered by </span><a href="./index.php?qa=user&amp;qa_1=' . $answer['handle'] .
					'" class="qa-user-link url fn entry-title nickname"> ' . $answer['handle'] . ' </a><span class="qa-q-item-who-data">' . '(' . $answer['points'] . ') </span>' . $avatar . '</span>';
		}

		return $user_details;
	}

	// add view count to question list
	public function q_item_stats($q_item) {
		$this->output('<div class="qa-q-item-stats">');

		$this->voting($q_item);
		$this->a_count($q_item);
		qa_html_theme_base::view_count($q_item);
		$this->WriteAnswer($q_item);
		$this->output('</div>');
	}

	// Add button to write answer
	public function WriteAnswer($q_item) {
		$this->output('<div class="WriteAnswer"><a class="Write_answer" href=' . "'" . $q_item['url'] . "'" . '>Write Answer</a>');

		$this->output('</div>');
	}

	// prevent display of view count in the usual place
	public function view_count($q_item) {
		if ($this->template == 'question')
			qa_html_theme_base::view_count($q_item);
	}

	public function vote_buttons($post) {
		$this->output('<div class="qa-vote-buttons ' . (($post['vote_view'] == 'updown') ? 'qa-vote-buttons-updown' : 'qa-vote-buttons-net') . '">');

		switch (@$post['vote_state']) {
			case 'voted_up':
				$this->post_hover_button($post, 'vote_up_tags', '+', 'qa-vote-one-button qa-voted-up');
				break;

			case 'voted_up_disabled':
				$this->post_disabled_button($post, 'vote_up_tags', '+', 'qa-vote-one-button qa-vote-up');
				break;

			case 'voted_down':
				break;

			case 'voted_down_disabled':
				break;

			case 'up_only':
				$this->post_hover_button($post, 'vote_up_tags', '+', 'qa-vote-first-button qa-vote-up');
				break;

			case 'enabled':
				$this->post_hover_button($post, 'vote_up_tags', '+', 'qa-vote-first-button qa-vote-up');
				break;

			default:
				$this->post_disabled_button($post, 'vote_up_tags', '', 'qa-vote-first-button qa-vote-up');
				break;
		}

		$this->output('</div>');
	}

	public function vote_count($post) {
		// You can also use $post['upvotes_raw'], $post['downvotes_raw'], $post['netvotes_raw'] to get
		// raw integer vote counts, for graphing or showing in other non-textual ways

		$this->output('<div class="qa-vote-count ' . (($post['vote_view'] == 'updown') ? 'qa-vote-count-updown' : 'qa-vote-count-net') . '"' . @$post['vote_count_tags'] . '>');

		if ($post['vote_view'] == 'updown') {
			$this->output_split($post['upvotes_view'], 'qa-upvote-count');
			$this->output_split($post['downvotes_view'], 'qa-downvote-count');
		} else
			$this->output_split($post['netvotes_view'], 'qa-netvote-count');

		switch (@$post['vote_state']) {
			case 'voted_up':
				break;

			case 'voted_up_disabled':
				break;

			case 'voted_down':
				$this->post_hover_button($post, 'vote_down_tags', '&ndash;', 'qa-vote-one-button qa-voted-down');
				break;

			case 'voted_down_disabled':
				$this->post_disabled_button($post, 'vote_down_tags', '&ndash;', 'qa-vote-one-button qa-vote-down');
				break;

			case 'up_only':
				$this->post_disabled_button($post, 'vote_down_tags', '', 'qa-vote-second-button qa-vote-down');
				break;

			case 'enabled':
				$this->post_hover_button($post, 'vote_down_tags', '&ndash;', 'qa-vote-second-button qa-vote-down');
				break;

			default:
				$this->post_disabled_button($post, 'vote_down_tags', '', 'qa-vote-second-button qa-vote-down');
				break;
		}
		$this->output('</div>');
	}

	// to replace standard Q2A footer
	public function body_suffix() {
		$this->output('<div class="qa-footer-bottom-group">');
		qa_html_theme_base::footer();
		$this->output('</div> <!-- END footer-bottom-group -->', '');
	}

	public function attribution() {

	}

	function embed_replace($text) {

		$w = qa_opt('embed_video_width');
		$h = qa_opt('embed_video_height');
		$w2 = qa_opt('embed_image_width');
		$h2 = qa_opt('embed_image_height');

		$types = array(
			'youtube' => array(
				array(
					'https{0,1}:\/\/w{0,3}\.*youtube\.com\/watch\?\S*v=([A-Za-z0-9_-]+)[^< ]*',
					'<iframe width="' . $w . '" height="' . $h . '" src="http://www.youtube.com/embed/$1?wmode=transparent" frameborder="0" allowfullscreen></iframe>'
				),
				array(
					'https{0,1}:\/\/w{0,3}\.*youtu\.be\/([A-Za-z0-9_-]+)[^< ]*',
					'<iframe width="' . $w . '" height="' . $h . '" src="http://www.youtube.com/embed/$1?wmode=transparent" frameborder="0" allowfullscreen></iframe>'
				)
			),
			'vimeo' => array(
				array(
					'https{0,1}:\/\/w{0,3}\.*vimeo\.com\/([0-9]+)[^< ]*',
					'<iframe src="http://player.vimeo.com/video/$1?title=0&amp;byline=0&amp;portrait=0&amp;wmode=transparent" width="' . $w . '" height="' . $h . '" frameborder="0"></iframe>')
			),
			'metacafe' => array(
				array(
					'https{0,1}:\/\/w{0,3}\.*metacafe\.com\/watch\/([0-9]+)\/([a-z0-9_]+)[^< ]*',
					'<embed flashVars="playerVars=showStats=no|autoPlay=no" src="http://www.metacafe.com/fplayer/$1/$2.swf" width="' . $w . '" height="' . $h . '" wmode="transparent" allowFullScreen="true" allowScriptAccess="always" name="Metacafe_$1" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed>'
				)
			),
			'dailymotion' => array(
				array(
					'https{0,1}:\/\/w{0,3}\.*dailymotion\.com\/video\/([A-Za-z0-9]+)[^< ]*',
					'<iframe frameborder="0" width="' . $w . '" height="' . $h . '" src="http://www.dailymotion.com/embed/video/$1?wmode=transparent"></iframe>'
				)
			),
			'image' => array(
				array(
					'(https*:\/\/[-\%_\/.a-zA-Z0-9+]+\.(png|jpg|jpeg|gif|bmp))[^< ]*',
					'<img src="$1" style="max-width:' . $w2 . 'px;max-height:' . $h2 . 'px" />', 'img'
				)
			),
			'mp3' => array(
				array(
					'(https*:\/\/[-\%_\/.a-zA-Z0-9]+\.mp3)[^< ]*', qa_opt('embed_mp3_player_code'), 'mp3'
				)
			),
			'gmap' => array(
				array(
					'(https*:\/\/maps.google.com\/?[^< ]+)',
					'<iframe width="' . qa_opt('embed_gmap_width') . '" height="' . qa_opt('embed_gmap_height') . '" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="$1&amp;ie=UTF8&amp;output=embed"></iframe><br /><small><a href="$1&amp;ie=UTF8&amp;output=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>', 'gmap'
				)
			),
		);

		foreach ($types as $t => $ra) {
			foreach ($ra as $r) {
				if ((!isset($r[2]) && !qa_opt('embed_enable')) || (isset($r[2]) && !qa_opt('embed_enable_' . $r[2])))
					continue;

				if (isset($r[2]) && @$r[2] == 'img' && qa_opt('embed_enable_thickbox') && preg_match('/MSIE [5-7]/', $_SERVER['HTTP_USER_AGENT']) == 0) {
					preg_match_all('/' . $r[0] . '/', $text, $imga);
					if (!empty($imga)) {
						foreach ($imga[1] as $img) {
							$replace = '<a href="' . $img . '" class="thickbox"><img  src="' . $img . '" style="max-width:' . $w2 . 'px;max-height:' . $h2 . 'px" /></a>';
							$text = preg_replace('|<a[^>]+>' . $img . '</a>|i', $replace, $text);
							$text = preg_replace('|(?<![\'"=])' . $img . '|i', $replace, $text);
						}
					}
					continue;
				}
				$text = preg_replace('/<a[^>]+>' . $r[0] . '<\/a>/i', $r[1], $text);
				$text = preg_replace('/(?<![\'"=])' . $r[0] . '/i', $r[1], $text);
			}
		}
		return $text;
	}

	function head_custom() {
		qa_html_theme_base::head_custom();
		if (qa_opt('embed_enable_thickbox')) {
			$this->output('<script type="text/javascript" src="../../qa-plugin/q2a-embed-media-master/thickbox.js"></script>');
			$this->output('<link rel="stylesheet" href="../../qa-plugin/q2a-embed-media-master/thickbox.css" type="text/css" media="screen" />');
		}
	}

	function head_script() {
		qa_html_theme_base::head_script();
		$this->output('  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">');
		//$this->output('  <link rel="stylesheet" href="./qa-content/jquery-ui/jquery-ui.css">');
		$this->output('<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>');
		// $this->output('<script src="./qa-content/jquery-ui/jquery-ui.js"></script>');
	}

}
