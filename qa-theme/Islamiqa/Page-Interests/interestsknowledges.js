function closepopup()
{
	if(false == my_window.closed) {
		my_window.close();
	} else {
		alert('Window already closed!');
	}
}
function saveUserSelections()
{
	var interesttopic = [];		// topic
	var excesstopics = []; 		// if subtopic selected, topic can be ignored
	var knowledgetopic = [];		// topic
	var excessknowledgetopics = []; 	// if subtopic selected, topic can be ignored
	var t, et, st;
	var ctr1 = 0, ctr2 = 0, ctr3 = 0, ctr4 = 0;
	var interests_selections, knowledge_selections;

	interests_selections = document.getElementsByClassName('check_clicked'); // get all elements that have been selected - interests
	if (interests_selections.length > 0) {
		// Extract ids for topic / subtopic for interests
		for(var i = 0; i < interests_selections.length; i++) {
			if (interests_selections[i].id.indexOf("-") > -1) {  // if - appears in id, then this is a child with two ids in format parent-child
				//alert(interests_selections[i].id.indexOf("-")+":"+interests_selections[i].id);
				et = interests_selections[i].id.match(/([0-9]+)(?=\-)/g);     // extract topic
				excesstopics[ctr1] = et[0];
				st = interests_selections[i].id.match(/(?:\-)([0-9]+)/g);     // extract subtopic
				interesttopic[ctr2] = st[0].replace("-","");
				//alert(excesstopics[ctr1]+":"+interesttopic[ctr2]);
				ctr1++;
			} else { // parent id containing just one id
				t = interests_selections[i].id.match(/[0-9]+/)
				interesttopic[ctr2]= t[0];
				//alert(interesttopic[ctr2]);
			}
			ctr2++;
		}
	}

	knowledge_selections = document.getElementsByClassName('owl_clicked'); // get all elements that have been selected - knowledge
	ctr3 = 0;
	ctr4 = 0;
	if (knowledge_selections.length > 0) {
		// Extract ids for topic / subtopic for knowledge
		for(var i = 0; i < knowledge_selections.length; i++) {
			if (knowledge_selections[i].id.indexOf("-") > -1) {  // if - appears in id, then this is a child with two ids in format parent-child
				//alert(knowledge_selections[i].id.indexOf("-")+":"+knowledge_selections[i].id);
				et = knowledge_selections[i].id.match(/([0-9]+)(?=\-)/g);     // extract topic
				excessknowledgetopics[ctr3] = et[0];
				t = knowledge_selections[i].id.match(/(?:\-)([0-9]+)/g);     // extract subtopic
				knowledgetopic[ctr4] = t[0].replace("-","");
				//alert(excessknowledgetopics[ctr3]+":"+knowledgetopic[ctr4]);
				ctr3++;
			} else { // parent id containing just one id
				t = knowledge_selections[i].id.match(/[0-9]+/)
				knowledgetopic[ctr4]= t[0];
				//alert(knowledgetopic[ctr4]);
			}
			ctr4++;
		}
	}

	// Remove topics from interests topics list where subtopics have been selected
	for(var i = 0; i < ctr1; i++) {
		del = interesttopic.indexOf(excesstopics[i])
		if (del > -1) interesttopic.remove(del);
	}
	//	alert("i after "+interesttopic);

	// Remove topics from knowledge topics list where subtopics have been selected
	for(var i = 0; i < ctr3; i++) {
		del = knowledgetopic.indexOf(excessknowledgetopics[i])
		if (del > -1) knowledgetopic.remove(del);
	}
	//	alert("k after "+knowledgetopic);

	if (interesttopic  == "") interesttopic  = "-";
	if (knowledgetopic == "") knowledgetopic = "-";

	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	/*  Only needed if asynchronous call is made - ie true is set in open function
	 xmlhttp.onreadystatechange = function() {
	 if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	 //alert(xmlhttp.responseText);
	 }
	 }
	 */

	if (interesttopic.length > 0) {
		xmlhttp.open("GET","savefavorites.php?i="+JSON.stringify(interesttopic)+"&k="+JSON.stringify(knowledgetopic),false);
		xmlhttp.send();

		// Retrieve php variable 
		var page = "<?php echo $page; ?>";

		if (page == 'i')
			window.opener.location.href="/index.php";
		else if (page == 'k')
			window.opener.location.href="/?qa=unanswered";
		self.close();
	}
}

// Delete members of an array
Array.prototype.remove = function(from, to) {
	var rest = this.slice((to || from) + 1 || this.length);
	this.length = from < 0 ? this.length + from : from;
	return this.push.apply(this, rest);
};

function highlight_interest(element) {
	// activate/deactivate topic that has been selected
	element.className = (element.className == "check_clicked") ? "check_wrapper" : "check_clicked";

	// if element erroneously has no id or element is a child then return
	if (element.id == '' || element.id.indexOf("child") > -1) return;

	// identify the child class
	owlid = element.id.replace("check","owlcheck");
	owl = document.getElementById(owlid);

	// Ensure owl knowledge icon is not activated - otherwise we should not turn on/off subtopics if interest is clicked
	if (owl.className.indexOf("owl_wrapper") > -1) {

		// identify the child names
		parentid = element.id.replace("check","");
		childid = "child"+parentid;

		// if there are children activate them
		childidnone   = childid + "none";
		childidinline = childid + "inline";

		ce = document.getElementsByClassName(childidnone);
		if (ce.length == 0) {
			ce = document.getElementsByClassName(childidinline);
		} else {
		}

		for(var j = 0; j < ce.length; ) {
			ce[j].className = (ce[j].className.indexOf("inline") > -1) ? childidnone : childidinline;
		}

	}

	// Update the continue button
	if (document.getElementById("nooftopics")) {
		if (element.className == "check_clicked")
			document.getElementById("nooftopics").innerHTML--;
		else
			document.getElementById("nooftopics").innerHTML++;

		if (document.getElementById("nooftopics").innerHTML == 0) {
			document.getElementById("choose").style.display = "none";
			document.getElementById("submit").href = "../Guide-Pages/guide-pages1.html";
		} else if (document.getElementById("nooftopics").innerHTML > 0) {
			document.getElementById("submit").href = "#!";
			document.getElementById("choose").style.display = "inline";
		}
	}
}

function highlight_knowledge(element) {
	// activate/deactivate topic that has been selected
	element.className = (element.className == "owl_clicked") ? "owl_wrapper" : "owl_clicked";

	// if element erroneously has no id or element is a child then return
	if (element.id == '' || element.id.indexOf("child") > -1) return;

	// identify the child class of container!
	checkid = element.id.replace("owl","");

	// Ensure start interests icon is not activated - otherwise we should not turn on/off if owl knowledge icon is clicked
	check = document.getElementById(checkid);
	if (check.className.indexOf("check_wrapper") > -1) {
		parentid = element.id.replace("owlcheck","");
		childid = "child" + parentid;

		// if there are children activate them so long as their parent is not selected
		childidnone   = childid + "none";
		childidinline = childid + "inline";
		ce = document.getElementsByClassName(childidnone);
		if (ce.length == 0) {
			ce = document.getElementsByClassName(childidinline);
		}

		for(var j = 0; j < ce.length; ) {
			ce[j].className = (ce[j].className.indexOf("inline") > -1) ? childidnone : childidinline;
		}
	}
}