<?php
$GLOBALS['k_dir_root'] = "../../../";
define('QA_BASE_DIR', $GLOBALS['k_dir_root']);
require_once $GLOBALS['k_dir_root'] . 'qa-include/qa-base.php';
require_once QA_INCLUDE_DIR . 'qa-base.php';
require_once QA_INCLUDE_DIR . 'qa-app-users.php';
require_once QA_INCLUDE_DIR . 'qa-app-posts.php';
require_once QA_INCLUDE_DIR . 'qa-db-admin.php'; // categories
// Get both lists of favorited interests and knowledges
$topicids = array();
$topics = qa_db_read_all_assoc(qa_db_query_sub("SELECT entityid FROM `^userfavorites` WHERE (entitytype = 'T' OR  entitytype = 'K') AND userid = '" . qa_get_logged_in_userid() . "'"));

// If topics have been selected, skip initial process and go to home page
if (count($topics) > 0) {
	header("Location: /index.php");
	die();
}
?>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

		<script type='text/javascript'>

            function saveRegisterUserSelections()
            {
                var interesttopic = [];		// topic
                var excesstopics = []; 		// if subtopic selected, topic can be ignored
                var knowledgetopic = [];		// topic
                var excessknowledgetopics = []; 	// if subtopic selected, topic can be ignored
                var t, et, st;
                var ctr1 = 0, ctr2 = 0, ctr3 = 0, ctr4 = 0;
                var interests_selections, knowledge_selections;

                interests_selections = document.getElementsByClassName('check_clicked'); // get all elements that have been selected - interests
                if (interests_selections.length > 0) {
                    // Extract ids for topic / subtopic for interests
                    for (var i = 0; i < interests_selections.length; i++) {
                        if (interests_selections[i].id.indexOf("-") > -1) {  // if - appears in id, then this is a child with two ids in format parent-child
                            //alert(interests_selections[i].id.indexOf("-")+":"+interests_selections[i].id);
                            et = interests_selections[i].id.match(/([0-9]+)(?=\-)/g);     // extract topic
                            excesstopics[ctr1] = et[0];
                            st = interests_selections[i].id.match(/(?:\-)([0-9]+)/g);     // extract subtopic
                            interesttopic[ctr2] = st[0].replace("-", "");
                            //alert(excesstopics[ctr1]+":"+interesttopic[ctr2]);
                            ctr1++;
                        } else { // parent id containing just one id
                            t = interests_selections[i].id.match(/[0-9]+/)
                            interesttopic[ctr2] = t[0];
                            //alert(interesttopic[ctr2]);
                        }
                        ctr2++;
                    }
                }

                knowledge_selections = document.getElementsByClassName('owl_clicked'); // get all elements that have been selected - knowledge
                ctr3 = 0;
                ctr4 = 0;
                if (knowledge_selections.length > 0) {
                    // Extract ids for topic / subtopic for knowledge
                    for (var i = 0; i < knowledge_selections.length; i++) {
                        if (knowledge_selections[i].id.indexOf("-") > -1) {  // if - appears in id, then this is a child with two ids in format parent-child
                            //alert(knowledge_selections[i].id.indexOf("-")+":"+knowledge_selections[i].id);
                            et = knowledge_selections[i].id.match(/([0-9]+)(?=\-)/g);     // extract topic
                            excessknowledgetopics[ctr3] = et[0];
                            t = knowledge_selections[i].id.match(/(?:\-)([0-9]+)/g);     // extract subtopic
                            knowledgetopic[ctr4] = t[0].replace("-", "");
                            //alert(excessknowledgetopics[ctr3]+":"+knowledgetopic[ctr4]);
                            ctr3++;
                        } else { // parent id containing just one id
                            t = knowledge_selections[i].id.match(/[0-9]+/)
                            knowledgetopic[ctr4] = t[0];
                            //alert(knowledgetopic[ctr4]);
                        }
                        ctr4++;
                    }
                }

                // Remove topics from interests topics list where subtopics have been selected
                for (var i = 0; i < ctr1; i++) {
                    del = interesttopic.indexOf(excesstopics[i])
                    if (del > -1)
                        interesttopic.remove(del);
                }
                //	alert("i after "+interesttopic);

                // Remove topics from knowledge topics list where subtopics have been selected
                for (var i = 0; i < ctr3; i++) {
                    del = knowledgetopic.indexOf(excessknowledgetopics[i])
                    if (del > -1)
                        knowledgetopic.remove(del);
                }
                //	alert("k after "+knowledgetopic);

                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else {
                    // code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }

                /*  Only needed if asynchronous call is made - ie true is set in open function
                 xmlhttp.onreadystatechange = function() {
                 if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 //alert(xmlhttp.responseText);
                 }
                 }
                 */

                if (interesttopic.length > 0) {
                    xmlhttp.open("GET", "savefavorites.php?i=" + JSON.stringify(interesttopic) + "&k=" + JSON.stringify(knowledgetopic), false);
                    xmlhttp.send();
                    return true;
                }

            }
		</script>
	</head>
	<body>
		<div class="menu_wrapper">
			<div class="menu_logo">islamiqa</div>
			<div class="menu_title">Tell us about the topics you are interested in/have knowledge on. You'll be able to change these later.</div>
		</div>
		<div class="page_wrapper">

			<div class="how_it_works">
				<div class="how_title">Choose at least 5 main topics on the left - optional subtopics will appear on the right. </div>
				<div class="how_box">
					<div class="how_image1">Check</div>
					<div class="how_header">Pick 5 interesting topics</div>
					<div class="how_subheader">Select the star icon on topics you find interesting - questions will appear on your home page based on these interests.</div>
				</div>
				<div class="how_box">
					<div class="how_image2">Check</div>
					<div class="how_header">Indicate your expertise</div>
					<div class="how_subheader">Select the owl icon on topics you  can answer questions - unanswered questions will be selected accordingly.</div>
				</div>
				<div class="how_box">
					<div class="how_image3">Check</div>
					<div class="how_header">You're all set</div>
					<div class="how_subheader">Start reading and posting questions and answers - share your knowledge with the world.</div>
				</div>
			</div>

			<div class="body_wrapper">

				<?php
				$id = qa_db_read_all_assoc(qa_db_query_sub('SELECT title,id FROM `^islamiqa_topics` WHERE type = "t"'));
				$topics[] = array();
//print_r($id);
				echo '<div class="box_wrapper1"><div class="box_overflow">';

				for ($i = 0; $i < count($id); $i++) {
					$topics[$i][0] = $id[$i]['title'];
					$topics[$i][1] = $id[$i]['id'];
					$topic = $topics[$i][0];
					$topicid = $topics[$i][1];

					echo "	<div class='topic_box'>
				<div style='background-size:auto;float:left;margin-right:7px;overflow:hidden;background:url(images/" .
					str_replace(")", "", str_replace("(", "", str_replace(" ", "_", strtolower($topic)))) . ".jpg) no-repeat;text-indent:-9999px;width:115px;height:115px;'>" . strtolower($topic) . "</div>
				<div class='info_wrapper'>
					<span class='info_background'>
						<div name='checktopic' class='check_wrapper' id='check$topicid' onclick='highlight_interest(this);'>
							<div class='check'></div>
						</div>
						<div class='owl_wrapper' id='owlcheck$topicid' onclick='highlight_knowledge(this);'>
							<div class='owl'></div>
						</div>
						<div class='info'>
							<span class='topicname'>$topic</span>
						</div>
					</span>
				</div>
			</div>";
				}
				echo '</div></div>';

				echo '<div class="topic_arrow">islamiqa arrow</div>';


				$subtopics = qa_db_read_all_assoc(qa_db_query_sub('SELECT id,title,parentid FROM `^islamiqa_topics` WHERE type = "s"'));

				echo "<div class='box_wrapper2'><div class='box_overflow'>";

				for ($i = 0; $i < count($subtopics); $i++) {
					$id = $subtopics[$i]['id'];
					$subtopic = $subtopics[$i]['title'];
					$parentid = $subtopics[$i]['parentid'];
					$index = searcharray($parentid, 1, $topics); // get parent index for this child - needed for div id tag to link it to parent

					if (isset($topics[$index][1])) {
						echo "
			<div class='child" . $topics[$index][1] . "none' id='child" . $index . "." . $i . "'>
				<div class='topic_box'>
					<div style='background-size:auto;float:left;margin-right:7px;overflow:hidden;background:url(images/" . str_replace("/", "", str_replace(")", "", str_replace("(", "", str_replace(" ", "_", strtolower($subtopic))))) . ".jpg) no-repeat;text-indent:-9999px;width:115px;height:115px;'>" . strtolower($subtopic) . "</div>
					<div class='info_wrapper'>
						<span class='info_background'>
							<div name='checksubtopic' class='check_wrapper' id='childcheck" . $topics[$index][1] . "-" . $id . "' onclick='highlight_interest(this);'>
								<div class='check'></div>
							</div>
							<div class='owl_wrapper' id='childowlcheck" . $topics[$index][1] . "-" . $id . "' onclick='highlight_knowledge(this);'>
								<div class='owl'></div>
							</div>
							<div class='info'>
								<span class='topicname'>$subtopic</span>
							</div>
						</span>
					</div>
				</div>
			</div>";
					}
				}

				echo "<style>";
				for ($i = 0; $i < count($topics); $i++) {
					echo ".child" . $topics[$i][1] . "none{ display:none;} ";
					echo ".child" . $topics[$i][1] . "inline{ display:inline; }";
				}
				echo "</style>";

				echo "</div> </div>";

				function searcharray($value, $key, $array) {
					foreach ($array as $k => $val) {
						if ($val[$key] == $value) {
							return $k;
						}
					}
					return null;
				}
				?>

			</div>

			<div class="button_primary topic_button" id="button">
				<a href="#!" style="color:#fff;text-decoration:none;" id="submit" onclick="return saveUserSelections()">
					<span id="choose">Choose
						<span id="nooftopics">5</span>+
						topics then
					</span>Continue
				</a>
			</div>

		</div>
		<script src="interestsknowledges.js"></script>
	</body>
</html>