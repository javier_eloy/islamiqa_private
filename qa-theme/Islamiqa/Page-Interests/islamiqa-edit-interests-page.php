<?php
	$GLOBALS['k_dir_root'] = "../../../";
	define('QA_BASE_DIR', $GLOBALS['k_dir_root']);
	require_once $GLOBALS['k_dir_root'].'qa-include/qa-base.php';
	require_once QA_INCLUDE_DIR.'qa-base.php';
	require_once QA_INCLUDE_DIR.'qa-app-users.php';
	require_once QA_INCLUDE_DIR.'qa-app-posts.php';
	require_once QA_INCLUDE_DIR.'qa-db-admin.php'; // categories

	// determine which page we need to return to - the home page (topics) or the unanswered questions page (knowledge)
	if (isset($_GET['p']) && $_GET['p'] != '' ) {
		$page = $_GET['p'];
	}
	
		$topicids = array();
		$knowledgeids = array();
		
		// Get both lists of favorited topics and knowledges
		$topics     = qa_db_read_all_assoc(qa_db_query_sub("SELECT entityid FROM `^userfavorites` WHERE entitytype = 'T' AND userid = " . qa_get_logged_in_userid()));
		$knowledges = qa_db_read_all_assoc(qa_db_query_sub("SELECT entityid FROM `^userfavorites` WHERE entitytype = 'K' AND userid = " . qa_get_logged_in_userid()));

		// Replace entityid of each topic with id from islamiqa_topics table
		if (count($topics) > 0) {
			foreach($topics as $topic) {
				$topic_id = qa_db_read_one_value(qa_db_query_sub("SELECT it.id FROM `^islamiqa_topics` it JOIN `^words` w WHERE w.wordid = ".$topic['entityid']." AND it.title = w.word"),true);
				if ($topic_id <> "") $topicids[] = $topic_id;
			}
		}
		
		// Replace entityid of each knowledge with id from islamiqa_topics table
		if (count($knowledges) > 0) {
			foreach($knowledges as $knowledge) {
				$knowledge_id = qa_db_read_one_value(qa_db_query_sub("SELECT it.id FROM `^islamiqa_topics` it JOIN `^words` w WHERE w.wordid = ".$knowledge['entityid']." AND it.title = w.word"),true);
				if ($knowledge_id <> "") $knowledgeids[] = $knowledge_id;
			}
		}
		
		// Build the user topic/knowledge selection interface
		// Use the relationships of topic/knowledge topic/subtopic/subsidiary words  in the Islamiqa topic table for this
		$id = qa_db_read_all_assoc(qa_db_query_sub('SELECT title,id FROM `^islamiqa_topics` WHERE type = "t"')); // Get the highest level words in the hierarchy first
		$topics[] = array();
		echo '<html lang="en">
			<head>
			<link rel="stylesheet" type="text/css" href="style.css">
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		</head>
		<body>
			<div class="page_wrapper">
				<div class="body_wrapper">';
		echo '<div class="box_wrapper1"><div class="box_overflow">';
		
		$total_parents = 0;
		for ($i=0; $i < count($id); $i++) {
			$topics[$i][0] = $id[$i]['title'];
			$topics[$i][1] = $id[$i]['id'];
			$topic = $topics[$i][0];
			$topicid = $topics[$i][1];
			//echo array_search($topicid, $topicids). " : $topicid : "; 
			//echo array_search($topicid, $knowledgeids). "<br/>"; 

			echo "	<div class='topic_box'>
						<div style='background-size:auto;float:left;margin-right:7px;overflow:hidden;background:url(images/".str_replace(" ", "_", strtolower($topic)).".jpg) no-repeat;text-indent:-9999px;width:115px;height:115px;'>".strtolower($topic)."</div>
						<div class='info_wrapper'>
							<span class='info_background'>
								<div class='check_" . ((array_search($topicid, $topicids)>-1) ? 'clicked':'wrapper') . "' id='check$topicid' onclick='highlight_interest(this);'>
									<div class='check'></div> 
								</div>
								<div class='owl_" . ((array_search($topicid, $knowledgeids)>-1) ? 'clicked':'wrapper') . "' id='owlcheck$topicid' onclick='highlight_knowledge(this);'>
									<div class='owl'></div> 
								</div>
								<div class='info'>
									<span class='topicname'>$topic</span>
								</div>
							</span>		
						</div>
					</div>";
			if (array_search($topicid, $topicids)>-1) $total_parents++;
		}
		echo '</div></div>';
		echo '<div class="topic_arrow">islamiqa arrow</div>';

		$parents = $siblings = array();
		$subtopics = qa_db_read_all_assoc(qa_db_query_sub('SELECT id,title,parentid FROM `^islamiqa_topics` WHERE type = "s"'));
		if (count($topicids) > 0 || count($knowledgeids) > 0) {
			if (count($topicids) > 0) {
				if (count($knowledgeids) > 0)
					$ids = implode(',',$topicids) . ',' . implode(',',$knowledgeids);
				else
					$ids = implode(',',$topicids);
			} else
				$ids = implode(',',$knowledgeids);
				
			$siblings  = qa_db_read_all_assoc(qa_db_query_sub('select id,parentid from ^islamiqa_topics WHERE id in ('. $ids . ')'));
		}


		if (count($siblings) > 0) {
			foreach ($siblings as $sibling) {
				$parents[] = ($sibling['parentid']=="") ? $sibling['id'] : $sibling['parentid'];
//				if ($sibling['parentid'] == "") $total_parents++;
			}
			$parents = array_unique($parents);
		}
		
		echo "<div class='box_wrapper2'><div class='box_overflow'>";

		for ($i=0; $i < count($subtopics); $i++) {	
			$id       = $subtopics[$i]['id'];
			$subtopic = $subtopics[$i]['title'];
			$parentid = $subtopics[$i]['parentid'];
			$index    = searcharray($parentid, 1, $topics); // get parent index for this child - needed for div id tag to link it to parent

			if (isset($topics[$index][1])) {
				echo "<div class='child".$topics[$index][1].((array_search($parentid, $parents)>-1) ? 'inline' : 'none')."' id='child".$index.".".$i."'>
					<div class='topic_box'>
						<div style='background-size:auto;float:left;margin-right:7px;overflow:hidden;background:url(images/".str_replace("/", "", str_replace(")", "", 
						str_replace("(", "", str_replace(" ", "_", strtolower($subtopic))))).".jpg) no-repeat;text-indent:-9999px;width:115px;height:115px;'>".strtolower($subtopic)."</div>
						<div class='info_wrapper'>
							<span class='info_background'>
								<div class='check_" . ((array_search($id, $topicids)>-1) ? 'clicked':'wrapper') . " 'id='childcheck".$topics[$index][1]."-".$id."' onclick='highlight_interest(this);'>
									<div class='check'></div> 
								</div>
								<div class='owl_" . ((array_search($id, $knowledgeids)>-1) ? 'clicked':'wrapper') . " 'id='childowlcheck".$topics[$index][1]."-".$id."' onclick='highlight_knowledge(this);'>
									<div class='owl'></div> 
								</div>
								<div class='info'>
									<span class='topicname'>$subtopic</span>
								</div>
							</span>				
						</div>
					</div> 
				</div>";
			}
		}

		echo "<style>";
			for ($i=0; $i < count($topics); $i++) {	
				echo ".child".$topics[$i][1]."none{ display:none; } ";
				echo ".child".$topics[$i][1]."inline{ display:inline; }";
			}
		echo "</style>";
		echo "</div></div>";

		function searcharray($value, $key, $array) {
		   foreach ($array as $k => $val) {
			   if ($val[$key] == $value) return $k;
		   }
		   return null;
		}
	?>

	</div>

	<div class="button_primary topic_button" id="button">
		<a style="color:#fff;text-decoration:none;" id="submit" onclick="saveUserSelections()">
			
			<?php
			if ($total_parents >= 5)
				echo "Continue";
			else
				echo '<span id="choose">Choose <span id="nooftopics">'.(5-$total_parents).'</span>+ topics then </span>Continue';
			?>
		</a>
	</div>

</div>


<script type='text/javascript'> 
/*
function closepopup()
{
    if(false == my_window.closed) {
        my_window.close();
    } else {
        alert('Window already closed!');
    }
}
function saveUserSelections() 
{
	var interesttopic = [];		// topic
	var excesstopics = []; 		// if subtopic selected, topic can be ignored
	var knowledgetopic = [];		// topic
	var excessknowledgetopics = []; 	// if subtopic selected, topic can be ignored
	var t, et, st;
	var ctr1 = 0, ctr2 = 0, ctr3 = 0, ctr4 = 0;
	var interests_selections, knowledge_selections;
	
	interests_selections = document.getElementsByClassName('check_clicked'); // get all elements that have been selected - interests
	if (interests_selections.length > 0) { 
		// Extract ids for topic / subtopic for interests
		for(var i = 0; i < interests_selections.length; i++) {
			if (interests_selections[i].id.indexOf("-") > -1) {  // if - appears in id, then this is a child with two ids in format parent-child
				//alert(interests_selections[i].id.indexOf("-")+":"+interests_selections[i].id);
				et = interests_selections[i].id.match(/([0-9]+)(?=\-)/g);     // extract topic
				excesstopics[ctr1] = et[0];
				st = interests_selections[i].id.match(/(?:\-)([0-9]+)/g);     // extract subtopic
				interesttopic[ctr2] = st[0].replace("-","");
				//alert(excesstopics[ctr1]+":"+interesttopic[ctr2]);
				ctr1++;
			} else { // parent id containing just one id
				t = interests_selections[i].id.match(/[0-9]+/)
				interesttopic[ctr2]= t[0];
				//alert(interesttopic[ctr2]);
			}
			ctr2++;
		}
	}	
	
	knowledge_selections = document.getElementsByClassName('owl_clicked'); // get all elements that have been selected - knowledge
	ctr3 = 0;
	ctr4 = 0;
	if (knowledge_selections.length > 0) { 
		// Extract ids for topic / subtopic for knowledge
		for(var i = 0; i < knowledge_selections.length; i++) {
			if (knowledge_selections[i].id.indexOf("-") > -1) {  // if - appears in id, then this is a child with two ids in format parent-child
				//alert(knowledge_selections[i].id.indexOf("-")+":"+knowledge_selections[i].id);
				et = knowledge_selections[i].id.match(/([0-9]+)(?=\-)/g);     // extract topic
				excessknowledgetopics[ctr3] = et[0];
				t = knowledge_selections[i].id.match(/(?:\-)([0-9]+)/g);     // extract subtopic
				knowledgetopic[ctr4] = t[0].replace("-","");
				//alert(excessknowledgetopics[ctr3]+":"+knowledgetopic[ctr4]);
				ctr3++;
			} else { // parent id containing just one id
				t = knowledge_selections[i].id.match(/[0-9]+/)
				knowledgetopic[ctr4]= t[0];
				//alert(knowledgetopic[ctr4]);
			}
			ctr4++;
		}
	}

	// Remove topics from interests topics list where subtopics have been selected
	for(var i = 0; i < ctr1; i++) {
		del = interesttopic.indexOf(excesstopics[i])
		if (del > -1) interesttopic.remove(del);
	}
//	alert("i after "+interesttopic);
	
	// Remove topics from knowledge topics list where subtopics have been selected
	for(var i = 0; i < ctr3; i++) {
		del = knowledgetopic.indexOf(excessknowledgetopics[i])
		if (del > -1) knowledgetopic.remove(del);	
	}
//	alert("k after "+knowledgetopic);
	
	if (interesttopic  == "") interesttopic  = "-";
	if (knowledgetopic == "") knowledgetopic = "-";
	
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	if (interesttopic.length > 0) {
		xmlhttp.open("GET","savefavorites.php?i="+JSON.stringify(interesttopic)+"&k="+JSON.stringify(knowledgetopic),false);
		xmlhttp.send();
		
		// Retrieve php variable 
		var page = "<?php echo $page; ?>";

		if (page == 'i')
			window.opener.location.href="/index.php";
		else if (page == 'k')
			window.opener.location.href="/?qa=unanswered";
		self.close();
	}
}

// Delete members of an array
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

function highlight_interest(element) {
	// activate/deactivate topic that has been selected	
    element.className = (element.className == "check_clicked") ? "check_wrapper" : "check_clicked";
	
	// if element erroneously has no id then return
	if (element.id == '') return;

	// identify the child class
	owlid = element.id.replace("check","owlcheck");
	owl = document.getElementById(owlid);

	// Ensure owl knowledge icon is not activated - otherwise we should not turn on/off subtopics if interest is clicked
	if (owl.className.indexOf("owl_wrapper") > -1) {

		// identify the child names
		parentid = element.id.replace("check","");
		childid = "child"+parentid;
			
		// if there are children activate them
		childidnone   = childid + "none";
		childidinline = childid + "inline";

		ce = document.getElementsByClassName(childidnone);
		if (ce.length == 0) {
			ce = document.getElementsByClassName(childidinline);
		} else {
		}

		for(var j = 0; j < ce.length; ) {
			ce[j].className = (ce[j].className.indexOf("inline") > -1) ? childidnone : childidinline;
		}

	}
	
	// Update the continue button
	if (document.getElementById("nooftopics")) {
		if (element.className == "check_clicked") 
			document.getElementById("nooftopics").innerHTML++;
		else
			document.getElementById("nooftopics").innerHTML--;
		
		if (document.getElementById("nooftopics").innerHTML == 0) {
			document.getElementById("choose").style.display = "none";
			document.getElementById("submit").href = "/index.php"; 
		} else if (document.getElementById("nooftopics").innerHTML > 0) {
			document.getElementById("submit").href = "#!"; 
			document.getElementById("choose").style.display = "inline";
		}
	}
} 

function highlight_knowledge(element) {
	// activate/deactivate topic that has been selected
    element.className = (element.className == "owl_clicked") ? "owl_wrapper" : "owl_clicked";
	
	// if element erroneously has no id then return
	if (element.id == '') return;
	
	// identify the child class of container!
	checkid = element.id.replace("owl","");
	
	// Ensure start interests icon is not activated - otherwise we should not turn on/off if owl knowledge icon is clicked
	check = document.getElementById(checkid);
	if (check.className.indexOf("check_wrapper") > -1) {	
		parentid = element.id.replace("owlcheck","");
		childid = "child" + parentid;
		
		// if there are children activate them so long as their parent is not selected
		childidnone   = childid + "none";
		childidinline = childid + "inline";
		ce = document.getElementsByClassName(childidnone);
		if (ce.length == 0) {
			ce = document.getElementsByClassName(childidinline);
		}

		for(var j = 0; j < ce.length; ) {
			ce[j].className = (ce[j].className.indexOf("inline") > -1) ? childidnone : childidinline;
		}
	}
} 
*/
</script>

<script src="interestsknowledges.js"></script>

</body>
</html>