<?php
	$GLOBALS['k_dir_root'] = "../../../";
	define('QA_BASE_DIR', $GLOBALS['k_dir_root']);
	require_once $GLOBALS['k_dir_root'].'qa-include/qa-base.php';
	require_once QA_INCLUDE_DIR.'qa-base.php';
	
	//echo $_GET['k'];
	if (isset($_GET['i'])) {           // my interests
		$interests = json_decode($_GET['i']);
		qa_opt("interests", $interests);
	} elseif (isset($_GET['k'])) {     // knowledges I have
		$knowledges = json_decode($_GET['k']);
		qa_opt("knowledges", $knowledges);
	} elseif (isset($_GET['f'])) {     // users I follow
		$follows = json_decode($_GET['f']);
		qa_opt("follows", $follows);
	}
?>
