<?php
//	error_reporting(E_ALL);
	
	$GLOBALS['k_dir_root'] = "../../../";
	define('QA_BASE_DIR', $GLOBALS['k_dir_root']);
	require_once $GLOBALS['k_dir_root'].'qa-include/qa-base.php';
	require_once QA_INCLUDE_DIR.'qa-base.php';
	require_once QA_INCLUDE_DIR.'qa-app-users.php';
	require_once QA_INCLUDE_DIR.'qa-app-posts.php';
	require_once QA_INCLUDE_DIR.'qa-db-admin.php'; 
	require_once QA_INCLUDE_DIR.'qa-db-favorites.php';
	
	// if islamiqa_topics table does not already exist, flag up error and exit
	$table = qa_db_read_one_value(qa_db_query_sub("SHOW TABLES LIKE '^islamiqa_topics'"), true);
	if ($table == "") {
		echo "MySQL table missing.<br/>";
		exit;
	}
	
	if (isset($_GET['i'])) { // && $_GET['i'] !== '' ){
		$interests = json_decode($_GET['i']);
		//echo "INTEREST !!!".implode(',',$interests)."!!!";
	
		// select interest words from islamiqa_topics table corresponding to selected interests and extract entityids for these from the words table
		if ($interests <> "-")
			$words = qa_db_read_all_assoc(qa_db_query_sub("SELECT w.wordid, w.word, it.title FROM `^islamiqa_topics` it JOIN `^words` w WHERE it.title = w.word AND it.id in (" . implode(',',$interests) . ")" ));

		// if user has favorites already set up in user_favorites table, delete them
		if (qa_get_logged_in_userid()) {
			$favs = qa_db_read_all_assoc(qa_db_query_sub("SELECT entityid FROM `^userfavorites` WHERE entitytype = 'T' AND userid = " . qa_get_logged_in_userid()));
			//print_r($favs);
			foreach($favs as $fav) {
				if (count($favs) > 0) {
					qa_db_favorite_delete(qa_get_logged_in_userid(), 'T', $fav);
					//qa_db_query_sub("DELETE FROM `^userfavorites` WHERE entitytype = 'T' AND userid = " . qa_get_logged_in_userid());
				}
			}
		}

		// update user_favorites table with user interests
		if ($interests <> "-") {
			foreach($words as $word) {
				qa_db_favorite_create(qa_get_logged_in_userid(), 'T', $word['wordid']);
				//qa_db_query_sub('INSERT INTO `^userfavorites` (userid, entitytype, entityid) VALUES (#, "T", $)', qa_get_logged_in_userid(), $word['wordid']);
			}
		}
	}

	
	if (isset($_GET['k'])) { // && $_GET['k'] !== '') {
		$knowledges = json_decode($_GET['k']);
		//echo "KNOWLEDGE !!!".implode(',',$knowledges)."!!!";

		// select knowledge words from islamiqa_topics table corresponding to selected knowledges extract entityids for these from the words table
		if ($knowledges <> "-")
			$words = qa_db_read_all_assoc(qa_db_query_sub("SELECT w.wordid, w.word, it.title FROM `^islamiqa_topics` it JOIN `^words` w WHERE it.title = w.word AND it.id in (" . implode(',',$knowledges) . ")" ));

		// if user has favorites already set up in user_favorites table, delete them
		if (qa_get_logged_in_userid()) {
			$favs = qa_db_read_all_assoc(qa_db_query_sub("SELECT entityid FROM `^userfavorites` WHERE entitytype = 'K' AND userid = " . qa_get_logged_in_userid()));
			//print_r($favs);
			foreach($favs as $fav) {
				if (count($favs) > 0) {
					qa_db_favorite_delete(qa_get_logged_in_userid(), 'K', $fav);
					//qa_db_query_sub("DELETE FROM `^userfavorites` WHERE entitytype = 'K' AND userid = " . qa_get_logged_in_userid());
				}
			}
		}
		
		// update user_favorites table with user knowledges
		if ($knowledges <> "-") {
			foreach($words as $word) {
				qa_db_favorite_create(qa_get_logged_in_userid(), 'K', $word['wordid']);
				//qa_db_query_sub('INSERT INTO `^userfavorites` (userid, entitytype, entityid) VALUES (#, "K", #)', qa_get_logged_in_userid(), $word['wordid']);
			}	
		}
	}
?>
