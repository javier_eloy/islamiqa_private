/**
 * Created by gerardo on 21/07/16.
 */

coreService = (function(){

    return {
        vote: votePost,
        favoritequestion : setFavoriteQuestion,
        favoriteAnswer:setFavoriteAnswer,
        loadQuestions:loadQuestions,
        loadUserPoints:loadUserPoints
    }

    function votePost(postid,vote,parent){
        var path = (window.myConfigPath || "/") + "?qa=qajax-vote";
        return $.post(path, {vote: vote, postid: postid,parent:parent});
    }

    function setFavoriteQuestion(postid,questionid,favorite){

        var path =  (window.myConfigPath || "/") + "?qa=qajax-favorite";
        return $.post(path,{"entitytype":"Q","entityid": postid ,"favorite":favorite,"parent":questionid});

    }
    function setFavoriteAnswer(postid,questionid,favorite){

        var path =  (window.myConfigPath || "/") + "?qa=qajax-favorite";
        return $.post(path,{"entitytype":"Q","entityid": postid ,"favorite":favorite,"parent":questionid});

    }


    function loadQuestions(start,pagesize){
        pagesize = pagesize || 10;
        var path =  (window.myConfigPath || "/") + "?qa=qajax-question&start=" + start;
        return $.get(path);
    }


    function loadUserPoints(userid){
        var path =  (window.myConfigPath || "/") + "?qa=qajax-points";
        return $.post(path,{"id":userid});

    }

} )();


questions = (function ($,coreService) {

    question = {}
    question.init = init
    question.process = process
    question.showMoreText = showMoreText
    question.showLessText = showLessText
    question.postprocess = postprocess
    question.voteUp = voteUp
    question.voteUpAnswer = voteUpAnswer
    question.voteDown = voteDown
    question.voteDownAnswer = voteDownAnswer
    question.domore = domore
    question.doless = doless
    question.showMoreAnswer = showMoreAnswer
    question.loadMore = loadMore
    question.makeFavoriteAnswer = makeFavoriteAnswer;
    question.makeBestAnswer = makeBestAnswer;
    question.activity = activity
    question.sortDown = sortDown
    question.sortUp = sortUp

    question.init();

    return question

    //    --------------------

    function init(){
        this.listQuestions =[];
        this.userPoints={};
        this.countQuestion = 1;
        this.pageSize=10;
    }




    function process(userid, listQuestions) {
        this.userid = userid;

        listQuestions = listQuestions.map(function (item) {
            item = moreThanOne(item)
            item = readMore(item)
            item = owner(item, userid)
            item = pathAnswerQuestion(item)
            item = userVotes(item)
            item = tags(item)
            item = firstAnswer(item);
            return item
        })

        this.listQuestions = this.listQuestions.concat(listQuestions);
        return listQuestions
    }


    function sortDown() {


        $("#sortDown").removeClass("ds");
        $("#sortUp").addClass("ds");

        var containerDiv = $("#talktitles");
        var quesionsDiv = containerDiv.children("div");
        quesionsDiv.detach().sort(function(a, b) {
            return parseInt($(b).attr("data-order")) -parseInt($(a).attr("data-order")) ;
        });
        containerDiv.append(quesionsDiv);
    }

    function sortUp() {


        $("#sortUp").removeClass("ds");
        $("#sortDown").addClass("ds");

        var containerDiv = $("#talktitles");
        var quesionsDiv = containerDiv.children("div");
        quesionsDiv.detach().sort(function(a, b) {
            return parseInt($(a).attr("data-order")) -parseInt($(b).attr("data-order")) ;
        });
        containerDiv.append(quesionsDiv);
    }

    function activity(userid,postid){
        out = this;
        if( !this.userPoints[userid])
        {
            out.userPoints[userid]= userid
            coreService.loadUserPoints(userid).done(function(response){
                var html = Mustache.render($("#user-points").html(),response.result)
                out.userPoints[userid]= html
                 $("#points"+postid).html(html)

            }).fail(function () {
                delete out.userPoints[userid] 
            })
        }
        else{
            $("#points"+postid).html(this.userPoints[userid])
        }
    }



    function userVotes(item) {

        if(!item.uservote){
            item.uservoteup = 0;
            item.uservotedown = 0;
        }
        else if( parseInt(item.uservote)>=0)
        {
            item.uservoteup =item.uservote;
            item.uservotedown = 0;
        }else if(parseInt(item.uservote)<0){
            item.uservoteup = 0;
            item.uservotedown = item.uservote;
        }

        return item;
    }

    function tags(item)
    {
        item.listTags =  item.listTags || []
        item.listTags =  item.listTags.filter(function (item) {
           return item.trim().length>0;
        })
        item.hasTags = item.listTags.length>0;
        return item;
    }


    function postprocess(listQuestions) {
        userid =this.userid;
        listQuestions.forEach(function (question) {
            updateQuestion(question,userid)
        })
    }

    function domore(postid) {
        $("#showmore" + postid).hide()
        var post = findPost(this.listQuestions, postid);
        showMoreAnswer(postid, post,this.userid);

        // $("#rest-container-answer"+postid).show()
        $("#readless"+postid).show();


        return false;

    }

    function doless(postid) {
        $("#showmore" + postid).show()
         var post = findPost(this.listQuestions, postid);
         showLess(postid, post,this.userid);
        // $("#rest-container-answer"+postid).hide("slow")
        $("#readless"+postid).hide()
        return false;

    }


    function makeFavoriteAnswer(answerid,questionid){
        var question = findPost(this.listQuestions, questionid);

        var answer = question.answer.filter(function(answer_item){
            return parseInt(answer_item.postid) == parseInt(answerid)
        })[0];

        favoriteAnswer(answerid ,questionid, parseInt(answer.userfavoriteq) ? "0":"1" , this.userid);
    }

    
    function makeBestAnswer(postid){
       // console.log(postid)
    }
    
    /****************     events        *************************/
    function voteDown(postid) {  // vista

        var qst = findPost(this.listQuestions, postid);
        if (parseInt(qst.userid) == this.userid) {
            return;
        }
        if (!parseInt(qst.uservote) || parseInt(qst.uservote) >= 0) {
            vote(qst.postid, -1,this.userid,qst.postid)
        }

    }

    function voteUp(postid) {   // vista

        var qst = findPost(this.listQuestions, postid);
        if (parseInt(qst.userid) == parseInt(this.userid)) {
            return;
        }
        if (!parseInt(qst.uservote) || parseInt(qst.uservote) <= 0) {
            vote(qst.postid, 1,this.userid,qst.postid)
        }

    }

    function voteDownAnswer(answerid,questionid) {  // vista

        var question = findPost(this.listQuestions, questionid);

        if (!(question.answer || []).length)
            return;

        var answer = question.answer.filter(function(answer_item){
            return parseInt(answer_item.postid) == parseInt(answerid)
        })[0];

        if (parseInt(answer.userid) == this.userid) {
            return;
        }

        if (!parseInt(answer.uservote) || parseInt(answer.uservote) >= 0) {
            vote(answer.postid, -1,this.userid,questionid)
        }

    }

    function voteUpAnswer(answerid,questionid) {   // vista

        var question = findPost(this.listQuestions, questionid);

        if (!(question.answer || []).length)
            return;

        var answer = question.answer.filter(function(answer_item){
           return parseInt(answer_item.postid) == parseInt(answerid)
        })[0];

        if (parseInt(answer.userid) == parseInt(this.userid)) {
            return;
        }

        if (!parseInt(answer.uservote) || parseInt(answer.uservote) <= 0) {
            vote(answer.postid, 1,this.userid,questionid)
        }

    }

    /*******************  view Functions  *****************************/
    
    function orderQuestion(){
        
    }
    
    
    function loadMore(){

        var start =  (this.countQuestion *this.pageSize );
        this.countQuestion = this.countQuestion+1;

        outher =  this;


       // $("#loadMoreBtn").prop('disabled', true);
        // $("#loadMoreI").css('display', "inline");



        return coreService.loadQuestions(start).done(function (listResult) {
            var template = $("#dashboard-template").html();
            var plist =  outher.process(listResult.result.userid,listResult.result.data);
            var info = Mustache.to_html(template,{"result": plist});
            $("#talktitles").append(info);
            outher.postprocess(outher.listQuestions);
        }) .always(function() {
         //   $("#loadMoreBtn").prop('disabled', false);
            // $("#loadMoreI").css('display', "none");
        });

    }

    function updateBestAnswer(item) {
        $("#bestanswer"+item.postid).addClass("gold");
    }

    function  updateFavorite(item) {


        if(parseInt(item.userfavoriteq)>0){
            $("#favorite"+item.postid).removeClass("ds")
            $("#favorite"+item.postid).addClass("gold")
        }else{
            $("#favorite"+item.postid).removeClass("gold")
            $("#favorite"+item.postid).addClass("ds")
        }
    }
    
    function updateUp(qst, userid) {

        var item = $("#voteup"+qst.postid)
        var iTag =  $(item.find("i")[0])

        if (parseInt(qst.userid) == userid) {
            iTag.removeClass("gau").addClass("ds")
            return;
        }


        // no voted
        if (!(qst.uservote)) {
            $(item).css("cursor", "pointer");
            iTag.removeClass("ds").addClass("gau")
        }
        else if (parseInt(qst.uservote) <= 0) {
            $(item).css("cursor", "pointer");
            iTag.removeClass("ds").addClass("gau")
        }
        else {
            $(item).css("cursor", "default");
            iTag.removeClass("gau").addClass("ds")
        }

    }

    function updateDown(qst, userid) {

        var item = $("#votedown"+qst.postid)
        var iTag =  $(item.find("i")[0])

        if (parseInt(qst.userid) == userid) {
            iTag.removeClass("rad").addClass("ds")
            return;
        }

        if (!parseInt(qst.uservote)) {
            $(item).css("cursor", "pointer");
            iTag.removeClass("ds").addClass("rad")
        }
        else if (parseInt(qst.uservote) >= 0) {
            $(item).css("cursor", "pointer");
            iTag.removeClass("ds").addClass("rad")
        }
        else {
            $(item).css("cursor", "default");
            iTag.removeClass("rad").addClass("ds")
        }
    }


    function updateAnswer(answer,question,userid)
    {

        if(!$("#answer"+answer.postid).length) return;
        updateUp(answer,userid);
        updateDown(answer, userid)
        updateCounters(answer);
        updateBestAnswer(answer)
        updateFavorite(answer)

    }

    function updateQuestion(qst, userid){
        updateUp(qst,userid);
        updateDown(qst, userid)
        updateCounters(qst);
        qst.answer.forEach(function (item) {
            updateAnswer(item,qst,userid);
        })
    }

    function updateCounters(post)
    {
       /* $("#pup"+post.postid).html(post.upvotes)
         $("#pdown"+post.postid).html(post.downvotes) */
          post = userVotes(post);
         $("#userupnumber"+post.postid).html(post.upvotes)
         $("#userdownnumber"+post.postid).html(post.downvotes)
    }

    function showMoreText(idAnswer) {
        answer =  fidAnswer(this.listQuestions,idAnswer);
        $("#" + idAnswer).hide().html(answer.readMore).fadeIn("slow");
    }

    function showLessText(idAnswer){
        answer =  fidAnswer(this.listQuestions,idAnswer);
        $("#" + idAnswer).hide().html(answer.content).fadeIn("slow").focus();
    }

    function showMoreAnswer(idAnswer, post, userid) {
        var template = $("#user-question").html()
        var html = ""

        for (x = 1; x < post.answer.length; x++) {
            html = html + Mustache.to_html(template, post.answer[x])

        }
        $("#container-answer" + idAnswer).hide().append(html).fadeIn("slow");

        updateQuestion(post,userid)

    }

    function showLess(idAnswer, item,userid) {
        var template = $("#user-question").html()
        var html = ""

        if(item.answer && item.answer.length>0)
        {
            html =  Mustache.render($("#user-question").html(),item.answer[0])
        }
        $("#container-answer" + idAnswer).hide().html(html).fadeIn("slow");

        updateQuestion(item,userid)


    }

    /************************ calc fuctions  ***********************************/
    function readMore(item) {
        listAnswer = item.answer
        listAnswer.map(function (answer) {
            if (answer.content.length > 255) {
                answer.readMore = answer.content  + "... <a href='javascript:void(0)' onclick='questions.showLessText("  + answer.postid + ")' >(Show less)</a>";
                // answer.readMore = answer.content ;
                answer.content = answer.content.substr(0, 255) + "... <a href='javascript:void(0)'  class='dashboard-redmore' onclick='questions.showMoreText("  + answer.postid + ")' >(Read More)</a>";
            }
            return answer
        });
        return item
    }
    
    function firstAnswer(item){

        var html="";
        if(item.answer && item.answer.length>0)
        {  
            item.fisrtanswer =  Mustache.render($("#user-question").html(),item.answer[0])
        }
        return item;
    }    

    function moreThanOne(item) {
        if (item.answer.length > 1)
            item.coutresult = item.answer.length - 1
        else
            item.coutresult = false
        return item
    }

    function owner(item, userid) {

        if(parseInt(item.userid) == userid || parseInt(item.user_access)) {
            item.owner = true;
        } else {
            item.owner = false;
        }
        return item;
    }


    function pathAnswerQuestion(item) {
        item.answerpath = (window.myConfigPath || "/") + "?qa=" + item.postid
        item.edit = (window.myConfigPath || "/") + "?qa=" + item.postid + "&state=edit-"+ item.postid
        return item
    }

    /************************ helper  ***********************************/
    function findPost(listQuestions, idPost) {
        return listQuestions.filter(function (question) {
            return (parseInt(question.postid) == parseInt(idPost));
        })[0];
    }

    function fidAnswer(listQuestions,idAnswer){
        return listQuestions.reduce(function (result, item) {
            return (item.answer.filter(function (repuesta) {
                return parseInt(idAnswer) == parseInt(repuesta.postid);
            }).concat(result))[0];
        }, [])
    }

    function vote(answerid, vote, userid,questionid) {
        coreService.vote(answerid,vote,questionid).done(function (response) {
              replaceQuestion(response.result);
              updateQuestion(response.result,userid)

             }).fail(function (response) {

                $("#contenterror" + questionid).html(response.responseJSON.result)
                $("#error" + questionid).fadeIn("slow")
             })
    }

    // favoriteQuestion(answerid ,questionid, parseInt(qst.userfavoriteq) ? "0":"1" , this.userid);
    function favoriteAnswer(postid,questionid,faorite,userid){
        coreService.favoriteAnswer(postid,questionid,faorite).done(function(response){
            replaceQuestion(response.result)
            updateQuestion(response.result,userid)

        }).fail(function (response) {
            $("#contenterror" + questionid).html(response.responseJSON.result)
            $("#error" + questionid).fadeIn("slow")
        });
    }

    function replaceQuestion(qst){
        for(x=0;x<question.listQuestions.length;x++)
        {
            if(  question.listQuestions[x].postid ==qst.postid ){
                question.listQuestions[x] = qst
                return qst;
            }
        }
    }




}) (jQuery,coreService);

