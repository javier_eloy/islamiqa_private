 /*
 * Updated by Julio Velasquez
 * Date: 04/07/2016
 * Time: 17:48
 */

var ajaxLink; 

$(document).ready(function () {

    $('.signup_login ul.dropdown-menu').on('click', '.nav-tabs a', function () {
        $(this).closest('.dropdown').addClass('dontClose');
    });

    $('#signUpDropdown').on('hide.bs.dropdown', function (e) {
        if ($(this).hasClass('dontClose')) {
            e.preventDefault();
        }
        $(this).removeClass('dontClose');
    });
    //$('input#most_slide_index').hide();
    //$('input#most_total_slides').hide();
/*
    //In case that we use checkbox instead radio, to check only one checkbox of the group
    $('input[type=checkbox]').live('click', function() {
        var parent = $(this).parent().attr('id');
        $('#'+parent+' input[type=checkbox]').removeAttr('checked');
        $(this).attr('checked', 'checked');
    });
*/
    //This code is for load the image file in the new question dialog.

  /*  document.getElementById('get_file1').onclick = function() {
        document.getElementById('my_file1').click();
    }; */

    $("#get_file1").click(function () {
        document.getElementById('my_file1').click();
    })

    $('input#my_file1').change(function (e) {
        $('#customfileupload1').html($(this).val());
    });

    // document.getElementById('get_file2').onclick = function() {
    //     document.getElementById('my_file2').click();
    // };


    $("#get_file2").click(function () {
        document.getElementById('my_file2').click();
    })

    $('input#my_file2').change(function (e) {
        $('#customfileupload2').html($(this).val());
    });
});


function getBaseUrl() {
    var re = new RegExp(/^.*\//);
    return re.exec(window.location.href);
}

function getAjaxLink() {
  url = String(getBaseUrl());
  url = url.replace('index.php/', ''); 
  url = url + $('#ajaxlink').val();
//  console.log(url);
  return url;  
}

function getAjaxLinkHeadFilters() {
  url = String(getAjaxLink());
  url = url + 'header_filters.php';
//  console.log(url);
  return url;
}

function getAjaxLinkVotes() {
  url = String(getAjaxLink());
  url = url +'updownvote.php';
  return url;
}

function getAjaxAutocomplete()
{
    url = String(getAjaxLink());
    url = url + 'autocomplete.php';
//    console.log(url);
    return url;
}

function getAjaxLinkNewQuestion()
{
    url = String(getAjaxLink());
    url = url + 'newquestion.php';
//    console.log(url);
    return url;
}

function getAjaxLinkIsLogged()
{
    url = String(getAjaxLink());
    url = url + 'islogged.php';
//    console.log(url);
    return url;
}

function getAjaxLinkTags()
{
    url = String(getAjaxLink());
    url = url + 'tags.php';
//    console.log(url);
    return url;
}



//$ajaxlinkvotes = $context->rooturl . 'updownvote.php';

                        function addVote(id) {
                        
                           $.ajax({
                                    url: getAjaxLinkVotes(),
                                    data:"id="+id+"&type=up",
                                    type: "POST",     
                                    dataType: 'json',
                                    success: function(resp){ 
                                        if (resp.success){
                                            $("#span-"+id).html(resp.upvote);
                                            $("#span-"+id+"-downvote").html(resp.downvote);
                                        }
                                      }
                                 }); 
                        }
                        
                        function downVote(id) {
                             $.ajax({
                                    url: getAjaxLinkVotes(),
                                    data:"id="+id+"&type=down",
                                    type: "POST",                          
                                    dataType: 'json',
                                    success: function(resp){ 
                                        if (resp.success){
                                            $("#span-"+id).html(resp.upvote);
                                            $("#span-"+id+"-downvote").html(resp.downvote);
                                        }
                                    }
                                 }); 
                        }
                        
                        function addVoteunans(id) {
                        
                           $.ajax({
                                    url: getAjaxLinkVotes(),
                                    data:"id="+id+"&type=up",
                                    type: "POST",                          
                                    success: function(resp){ 
                                                 $("#spanunans-"+id).text(resp);
                                                 
                                             }
                                 }); 
                        }
                        
                        function downVoteunans(id) {
                      
                             $.ajax({
                                    url: getAjaxLinkVotes(),
                                    data:"id="+id+"&type=down",
                                    type: "POST",
                          
                                    success: function(resp){ 
                                                 $("#spanunans-"+id).text(resp);
                                             }
                                 }); 
                        }
                        
                        function fetchweek() {
                       
                            $.ajax({
                                    url:  getAjaxLinkHeadFilters(),
                                    data:"type=week",    
                                    type: "POST",
                                    dataType: "html",
                                    }).done(function ( data ) {
                                      $("#allthis-week").hide();                                      
                                      $("#allthis-week-res").html(data);
                                  });
                        }
                                                                      
                        function fetchmonth() {
                       
                            $.ajax({
                                    url:  getAjaxLinkHeadFilters(),
                                    data:"type=month",    
                                    type: "POST",
                                    dataType: "html",
                                    }).done(function ( data ) {
                                      $("#allthis-week").hide();                                      
                                      $("#allthis-week-res").html(data);
                                  });
                        }
                                                
                        function fetchyear() {
                         
                            $.ajax({
                                    url:  getAjaxLinkHeadFilters(),
                                    data:"type=year",    
                                    type: "POST",
                                    dataType: "html",
                                    }).done(function ( data ) {
                                      $("#allthis-week").hide();                                      
                                      $("#allthis-week-res").html(data);
                                  });
                        }
                        
                        function allfetch() {
                     
                            $.ajax({
                                    url:  getAjaxLinkHeadFilters(),
                                    data:"type=all",    
                                    type: "POST",
                                    dataType: "html",
                                    }).done(function ( data ) {
                                      $("#allthis-week").hide();
                                      
                                      $("#allthis-week-res").html(data);
                                  });
                        }

                        function catfetch(catname) {
                         
                               $.ajax({
                                    url:  getAjaxLinkHeadFilters(),
                                    data:"catname="+catname+"&type=allqcatchange",  
                                    type: "POST",
                                    dataType: "html",
                                    }).done(function ( data ) {
                                      $("#allthis-week").hide();
                                      
                                      $("#allthis-week-res").html(data);
                                  });
                        }

                        function fetchweekunans() {
                          
                            $.ajax({
                                    url:  getAjaxLinkHeadFilters(),
                                    data:"type=weekunans",    
                                    type: "POST",
                                    dataType: "html",
                                    }).done(function ( data ) {
                                      $("#allthis-weekunans").hide();
                                      
                                      $("#allthis-week-resunans").html(data);
                                  });
                        }

                        function fetchmonthunans() {
                         
                            $.ajax({
                                    url:  getAjaxLinkHeadFilters(),
                                    data:"type=monthunans",    
                                    type: "POST",
                                    dataType: "html",
                                    }).done(function ( data ) {
                                      $("#allthis-weekunans").hide();
                                      
                                      $("#allthis-week-resunans").html(data);
                                  });
                        }

                        function fetchyearunans() {
                        
                            $.ajax({
                                    url:  getAjaxLinkHeadFilters(),
                                    data:"type=yearunans",    
                                    type: "POST",
                                    dataType: "html",
                                    }).done(function ( data ) {
                                      $("#allthis-weekunans").hide();
                                      
                                      $("#allthis-week-resunans").html(data);
                                  });
                        }

                        function allfetchunans() {
                       
                            $.ajax({
                                    url:  getAjaxLinkHeadFilters(),
                                    data:"type=allunans",    
                                    type: "POST",
                                    dataType: "html",
                                    }).done(function ( data ) {
                                      $("#allthis-weekunans").hide();
                                      $("#allthis-week-resunans").html(data);
                                  });
                        }

                        function catfetchunans(catname) {
                         
                                $.ajax({
                                    url:  getAjaxLinkHeadFilters(),
                                    data:"catname="+catname+"&type=allqcatchangeunans",  
                                    type: "POST",
                                    dataType: "html",
                                    }).done(function ( data ) {
                                      $("#allthis-weekunans").hide();                                    
                                      $("#allthis-week-resunans").html(data);
                                });
                        }

						//Function developed by Julio Velasquez and finished at 20160510 in order to:
						//Call the ajax function to update the questions div in previous slide for most popular section.
            function arrowleft_mostpopular()
            {
              $.ajax({
                 url: getAjaxLinkHeadFilters(),
                 data:"view=grid&name=arrowleft&type=most",
                 type:"POST",
                 dataType:"html"

                 }).done(function(data) {
                    $("#most_popular_start").html(data);
                    $("#most_popular_start").hide();
                    $("div.qa_gallery.first.most_popular").hide();
                                
                  }).fail(function() {
                    console.log();

                  }).always(function() {
                    var most_slide_index = $("input#most_slide_index").val();
//                    console.log(most_slide_index);
                    //If there is not more questions to left side
                    if(most_slide_index <=1) {
                      $("#left").hide();
                    }
                    else {
                      $("#right").show();
                    }
                    $("div.qa_gallery.first.most_popular").show();
                    $("#most_popular_start").show("slide", {direction:"left"}, 1000);
								});
						}

						//Function developed by Julio Velasquez and finished at 20160510 in order to:
            //Call the ajax function to update the questions div in previous slide for most popular section.
						function arrowright_mostpopular()
            {
              $.ajax({
                  url: getAjaxLinkHeadFilters(),
                  data:"view=grid&name=arrowright&type=most",
                  type:"POST",
                  dataType:"html"

                  }).done(function (data) {
                      $("div#most_popular_start").html(data);
                      $("#most_popular_start").hide();
                      $("div.qa_gallery.first.most_popular").hide();
                                										        
                  }).fail(function () {
                      console.log();

                  }).always(function () {
                      var most_slide_index = $("input#most_slide_index").val();
                      var most_total_slides = $("input#most_total_slides").val();
//                      console.log(most_slide_index);
//                      console.log(most_total_slides);
                    //If there is not more questions to left side
                      if(most_slide_index == most_total_slides) {
                        $("#right").hide();
                      }
                      else {
                        $("#left").show();
                      }                      
                      $("div.qa_gallery.first.most_popular").show();
                      $("#most_popular_start").show("slide", {direction:"right"}, 1000);
								  });
						}

						function arrowleft_recently()
            {
              $.ajax({
                  url: getAjaxLinkHeadFilters(),
                  data:"view=grid&name=arrowleft&type=recent",
                  type:"POST",
                  dataType:"html"

                  }).done(function(data) {
                    $("#recently_start").html(data);
                    $("#recently_start").hide();
                    $("div.qa_gallery.first.recently_ask").hide();
                                
                  }).fail(function(data) {
                    console.log();

                  }).always(function(data) {
                    var recent_slide_index = $("input#recent_slide_index").val();
//                    console.log(recent_slide_index);
                    //If there is not more questions to left side
                    if(recent_slide_index <=1) {
                      $("#left-recently").hide();
                    }
                    else {
                      $("#right-recently").show();
                    }                    
                    $("div.qa_gallery.first.recently_ask").show();
                    $("#recently_start").show("slide", {direction:"left"}, 1000);
							});
						}

						function arrowright_recently()
            {
              $.ajax({
                  url: getAjaxLinkHeadFilters(),
                  data:"view=grid&name=arrowright&type=recent",
                  type:"POST",
                  dataType:"html"

                  }).done(function(data) {
                      $("#recently_start").html(data);
                      $("#recently_start").hide();
                      $("div.qa_gallery.first.recently_ask").hide();
                                
                  }).fail(function(data) {
                       console.log();

                  }).always(function(data) {
                       var recent_slide_index = $("input#recent_slide_index").val();
                       var recent_total_slides = $("input#recent_total_slides").val();
//                       console.log(recent_slide_index);
//                       console.log(recent_total_slides);
                       //If there is not more questions to left side
                       if(recent_slide_index == recent_total_slides) {
                         $("#right-recently").hide();
                       }
                       else {
                         $("#left-recently").show();
                       }
                       $("div.qa_gallery.first.recently_ask").show();
                       $("#recently_start").show("slide", {direction:"right"}, 1000);
								  });
						}

						function arrowleft_allquestions()
            {
              $.ajax({
                  url: getAjaxLinkHeadFilters(),
                  data:"view=grid&name=arrowleft&type=allque",
                  type:"POST",
                  dataType:"html"

                  }).done(function(data) {
                    $("#allquation_start").html(data);
                    $("#allquation_start").hide();
                    $("div.qa_gallery.first.allquation").hide();
                                
                  }).fail(function(data) {
                    console.log();

                  }).always(function(data) {
                    var allque_slide_index = $("input#allque_slide_index").val();
//                    console.log(allque_slide_index);
                    //If there is not more questions to left side
                    if(allque_slide_index <=1) {
                      $("#allquation-left").hide();
                    }
                    else {
                      $("#allquation-right").show();
                    }                    
                    $("div.qa_gallery.first.allquation").show();
                    $("#allquation_start").show("slide", {direction:"left"}, 1000);
              });
            }

						function arrowright_allquestions()
            {
              $.ajax({
                  url: getAjaxLinkHeadFilters(),
                  data:"view=grid&name=arrowright&type=allque",
                  type:"POST",
                  dataType:"html"

                  }).done(function(data) {
                      $("#allquation_start").html(data);
                      $("#allquation_start").hide();
                      $("div.qa_gallery.first.allquation").hide();
                                
                  }).fail(function(data) {
                       console.log();

                  }).always(function(data) {
                       var allque_slide_index = $("input#allque_slide_index").val();
                       var allque_total_slides = $("input#allque_total_slides").val();
//                       console.log(allque_slide_index);
//                       console.log(allque_total_slides);
                       //If there is not more questions to left side
                       if(allque_slide_index == allque_total_slides) {
                         $("#allquation-right").hide();
                       }
                       else {
                         $("#allquation-left").show();
                       }                      
                      $("div.qa_gallery.first.allquation").show();
                      $("#allquation_start").show("slide", {direction:"right"}, 1000);
                  });
            }

						function arrowleft_unanswered()
            {
              $.ajax({
                  url: getAjaxLinkHeadFilters(),
                  data:"view=grid&name=arrowleft&type=unans",
                  type:"POST",
                  dataType:"html"

                  }).done(function(data) {
                    $("#unansquation_start").html(data);
                    $("#unansquation_start").hide();
                    $("div.qa_gallery.first.unansquation").hide();
                                
                  }).fail(function(data) {
                    console.log();

                  }).always(function(data) {
                    var unans_slide_index = $("input#unans_slide_index").val();
//                    console.log(unans_slide_index);

                    //If there is not more questions to left side
                    if(unans_slide_index <=1) {
                      $("#unansquation-left").hide();
                    }
                    else {
                      $("#unansquation-right").show();
                    }                    
                    $("div.qa_gallery.first.unansquation").show();
                    $("#unansquation_start").show("slide", {direction:"left"}, 1000);
              });
            }

						function arrowright_unanswered()
            {
              $.ajax({
                  url: getAjaxLinkHeadFilters(),
                  data:"view=grid&name=arrowright&type=unans",
                  type:"POST",
                  dataType:"html"

                  }).done(function(data) {
                      $("#unansquation_start").html(data);
                      $("#unansquation_start").hide();
                      $("div.qa_gallery.first.unansquation").hide();
                                
                  }).fail(function(data) {
                       console.log();

                  }).always(function(data) {
                       var unans_slide_index = $("input#unans_slide_index").val();
                       var unans_total_slides = $("input#unans_total_slides").val();
//                       console.log(unans_slide_index);
//                       console.log(unans_total_slides);
                       //If there is not more questions to left side
                      if(unans_slide_index == unans_total_slides) {
                        $("#unansquation-right").hide();
                      }
                      else {
                        $("#unansquation-left").show();
                      }
                      $("div.qa_gallery.first.unansquation").show();
                      $("#unansquation_start").show("slide", {direction:"right"}, 1000);
                  });
            }

                        function mostpopular()
                        {
                          if ($("#mostpopular_grid").is(":visible")) 
                          {
                            $.ajax({
                              url: getAjaxLinkHeadFilters(),
                              data:"view=grid&name=mostpopular&type=most",
                              type:"POST",
                              dataType:"html"

                              }).done(function (data) {
                                $("#most_popular_start").html(data);
                                $("#mostpopular_table").hide();
                                
                              }).fail(function () {
                                console.log();
                                alert("Function mostpopular() in grid-view failed");
                                
                              }).always(function () {
                                var most_slide_index = $("input#most_slide_index").val();
                                var most_total_slides = $("input#most_total_slides").val();
//                                console.log(most_slide_index);
//                                console.log(most_total_slides);
                                if(most_slide_index <=1) {
                                  $("#left").hide();
                                }
                                if(most_slide_index == most_total_slides) {
                                  $("#right").hide();
                                }
                              });
                          }
                          else if ($("#mostpopular_table").is(":visible")) {
                            $.ajax({
                              url: getAjaxLinkHeadFilters(),
                              data:"view=list&name=mostpopular&type=most",
                              type:"POST",
                              dataType:"html"

                              }).done(function (data) {
                                $("#mostpopular_table").html(data);
                                $("#mostpopular_grid").hide();
                                
                              }).fail(function () {
                                console.log();
                                alert("Function mostpopular() in list-view failed");
                                
                              }).always(function () {
                                console.log("Function most_popular() in list-view completed");
                                var most_countque = $("input#most_countque").val();

                                if (most_countque < 10) {
                                  $("div#more_questions_most").hide();
                                }
                                else {
                                  $("div#more_questions_most").show();
                                }
                              });
                          }
                        }

                        function mostweek()
                        {
                          if ($("#mostpopular_grid").is(":visible"))
                          {
                            $.ajax({
                              url:  getAjaxLinkHeadFilters(),
                              data:"view=grid&name=mostweek&type=most",
                              type: "POST",
                              dataType: "html"

                              }).done( function (data) {
                                $("#most_popular_start").html(data);
                                $("#mostpopular_table").hide();

                              }).fail(function () {
                                console.log();
                                alert("Function mostweek() in grid-view failed");

                              }).always(function () {
                                var most_slide_index = $("input#most_slide_index").val();
                                var most_total_slides = $("input#most_total_slides").val();
//                                console.log(most_slide_index);
//                                console.log(most_total_slides);
                                if(most_slide_index <=1) {
                                  $("#left").hide();
                                }
                                if(most_slide_index >= most_total_slides) {
                                  $("#right").hide();
                                }
                              });
                          }
                          else if ($("#mostpopular_table").is(":visible"))
                          {
                            $.ajax({
                              url:  getAjaxLinkHeadFilters(),
                              data:"view=list&name=mostweek&type=most",
                              type: "POST",
                              dataType: "html"

                              }).done( function (data) {
                                $("#mostpopular_table").html(data);
                                $("#mostpopular_grid").hide();

                              }).fail(function () {
                                console.log();
                                alert("Function mostweek() in list-view failed");

                              }).always(function () {
                                console.log("Function mostweek() in list-view completed");
                                var most_countque = $("input#most_countque").val();

                                if (most_countque < 10) {
                                  $("div#more_questions_most").hide();
                                }
                                else {
                                  $("div#more_questions_most").show();
                                }
                              });
                          }
                        }

                        function mostmonth()
                        {
                          if ($("#mostpopular_grid").is(":visible"))
                          {
                            $.ajax({
                              url: getAjaxLinkHeadFilters(),
                              data: "view=grid&name=mostmonth&type=most",
                              type: "POST",
                              dataType: "html"

                              }).done(function (data) {
                                $("#most_popular_start").html(data);
                                $("#mostpopular_table").hide();
                                      
                              }).fail(function () {
                                console.log();
                                alert("Function mostmonth() in grid-view failed");

                              }).always(function () {
                                var most_slide_index = $("input#most_slide_index").val();
                                var most_total_slides = $("input#most_total_slides").val();
//                                console.log(most_slide_index);
//                                console.log(most_total_slides);
                                if(most_slide_index <=1) {
                                  $("#left").hide();
                                }
                                if(most_slide_index >= most_total_slides) {
                                  $("#right").hide();
                                }
                              });
                          }
                          else if ($("#mostpopular_table").is(":visible"))
                          {
                            $.ajax({
                              url: getAjaxLinkHeadFilters(),
                              data: "view=list&name=mostmonth&type=most",
                              type: "POST",
                              dataType: "html"

                              }).done(function (data) {
                                $("#mostpopular_table").html(data);
                                $("#mostpopular_grid").hide();
                                      
                              }).fail(function () {
                                console.log();
                                alert("Function mostmonth() in list-view failed");

                              }).always(function () {
                                console.log("Function mostmonth() in list-view completed");
                                var most_countque = $("input#most_countque").val();

                                if (most_countque < 10) {
                                  $("div#more_questions_most").hide();
                                }
                                else {
                                  $("div#more_questions_most").show();
                                }
                              });
                          }
                        }

                        function mostyear()
                        {
                          if ($("#mostpopular_grid").is(":visible"))
                          {
                            $.ajax({
                              url:  getAjaxLinkHeadFilters(),
                              data:"view=grid&name=mostyear&type=most",    
                              type: "POST",
                              dataType: "html"

                              }).done(function (data) {
                                $("#most_popular_start").html(data); 
                                $("#mostpopular_table").hide();
                                
                              }).fail(function () {
                                console.log();
                                alert("Function mostyear() in grid-view failed");

                              }).always(function () {
                                var most_slide_index = $("input#most_slide_index").val();
                                var most_total_slides = $("input#most_total_slides").val();
//                                console.log(most_slide_index);
//                                console.log(most_total_slides);
                                if(most_slide_index <=1) {
                                  $("#left").hide();
                                }
                                if(most_slide_index >= most_total_slides) {
                                  $("#right").hide();
                                }                                
                              });
                          }
                          else if ($("#mostpopular_table").is(":visible"))
                          {
                            $.ajax({
                              url:  getAjaxLinkHeadFilters(),
                              data:"view=list&name=mostyear&type=most",
                              type: "POST",
                              dataType: "html"

                              }).done(function (data) {
                                $("#mostpopular_table").html(data);
                                $("#mostpopular_grid").hide();
                                
                              }).fail(function () {
                                console.log();
                                alert("Function mostyear() in list-view failed");

                              }).always(function () {
                                console.log("Function mostyear() in list-view completed");
                                var most_countque = $("input#most_countque").val();

                                if (most_countque < 10) {
                                  $("div#more_questions_most").hide();
                                }
                                else {
                                  $("div#more_questions_most").show();
                                }
                              });
                          }
                        }
                          
                        function mostcategory(catname)
                        {
                          if ($("#mostpopular_grid").is(":visible"))
                          {
                            $.ajax({
                              url: getAjaxLinkHeadFilters(),
                              data:"catname="+catname+"&view=grid&name=mostcategory&type=most",
                              type:"POST",
                              dataType:"html"

                              }).done(function (data) {
                                $("#most_popular_start").html(data); 
                                $("#mostpopular_table").hide();                              
                                      
                              }).fail(function () {
                                console.log();
                                alert("Function mostcategory() in grid-view failed");

                              }).always(function () {
                                var most_slide_index = $("input#most_slide_index").val();
                                var most_total_slides = $("input#most_total_slides").val();
//                                console.log(most_slide_index);
//                                console.log(most_total_slides);
                                if(most_slide_index <=1) {
                                  $("#left").hide();
                                }
                                if(most_slide_index >= most_total_slides) {
                                  $("#right").hide();
                                }                                
                              });
                          }
                          else if ($("#mostpopular_table").is(":visible"))
                          {
                            $.ajax({
                              url: getAjaxLinkHeadFilters(),
                              data:"catname="+catname+"&view=list&name=mostcategory&type=most",
                              type:"POST",
                              dataType:"html"

                              }).done(function (data) {
                                $("#mostpopular_table").html(data); 
                                $("#mostpopular_grid").hide();                              
                                      
                              }).fail(function () {
                                console.log();
                                alert("Function mostcategory() in list-view failed");

                              }).always(function () {                                
                                var most_countque = $("input#most_countque").val();

                                if (most_countque < 10) {
                                  $("div#more_questions_most").hide();
                                }
                                else {
                                  $("div#more_questions_most").show();
                                }
                              });
                          }
                        }
                        
                    // recently
                    function recentlyask()
                    {
                      if ($("#recently_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=recent&name=recently",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#recently_start").html(data);
                          $("#recently_table").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function recentlyask() in grid-view failed");

                        }).always(function () {
                          var recent_slide_index = $("input#recent_slide_index").val();
                          var recent_total_slides = $("input#recent_total_slides").val();
//                          console.log(recent_slide_index);
//                          console.log(recent_total_slides);
                          if(recent_slide_index <=1) {
                            $("#left-recently").hide();
                          }
                          if(recent_slide_index >= recent_total_slides) {
                            $("#right-recently").hide();
                          }
                        });
                      }
                      else if ($("#recently_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=recent&name=recently",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#recently_table").html(data);
                          $("#recently_grid").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function recentlyask() in list-view failed");

                        }).always(function () {                          
                          var recent_countque = $("input#recent_countque").val();
                          console.log(recent_countque);
                          if (recent_countque < 10) {
                            $("div#more_questions_recent").hide();
                          }
                          else {
                            $("div#more_questions_recent").show();
                          }
                        });
                      }
                    }
                    
                    function recentlyweek()
                    {
                      if ($("#recently_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=recent&name=recentlyweek",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#recently_start").html(data);
                          $("#recently_table").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function recentlyweek() in grid-view failed");

                        }).always(function () {
                          var recent_slide_index = $("input#recent_slide_index").val();
                          var recent_total_slides = $("input#recent_total_slides").val();
//                          console.log(recent_slide_index);
//                          console.log(recent_total_slides);
                          if(recent_slide_index <=1) {
                            $("#left-recently").hide();
                          }
                          if(recent_slide_index >= recent_total_slides) {
                            $("#right-recently").hide();
                          }
                        });
                      }
                      else if ($("#recently_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=recent&name=recentlyweek",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#recently_table").html(data);
                          $("#recently_grid").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function recentlyweek() in list-view failed");

                        }).always(function () {
                          var recent_countque = $("input#recent_countque").val();
                          console.log(recent_countque);
                          if (recent_countque < 10) {
                            $("div#more_questions_recent").hide();
                          }
                          else {
                            $("div#more_questions_recent").show();
                          }
                        });
                      }
                    }
                    
                    function recentlymonth()
                    {
                      if ($("#recently_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=recent&name=recentlymonth",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#recently_start").html(data);
                          $("#recently_table").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function recentlymonth() in grid-view failed");

                        }).always(function () {
                          var recent_slide_index = $("input#recent_slide_index").val();
                          var recent_total_slides = $("input#recent_total_slides").val();
//                          console.log(recent_slide_index);
//                          console.log(recent_total_slides);
                          if(recent_slide_index <=1) {
                            $("#left-recently").hide();
                          }
                          if(recent_slide_index >= recent_total_slides) {
                            $("#right-recently").hide();
                          }
                        });
                      }
                      else if ($("#recently_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=recent&name=recentlymonth",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#recently_table").html(data);
                          $("#recently_grid").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function recentlymonth() in list-view failed");

                        }).always(function () {
                          var recent_countque = $("input#recent_countque").val();
                          console.log(recent_countque);
                          if (recent_countque < 10) {
                            $("div#more_questions_recent").hide();
                          }
                          else {
                            $("div#more_questions_recent").show();
                          }
                        });
                      }
                    }

                    function recentlyyear()
                    {
                      if ($("#recently_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=recent&name=recentlyyear",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#recently_start").html(data);
                          $("#recently_table").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function recentlyyear() in grid-view failed");

                        }).always(function () {
                          var recent_slide_index = $("input#recent_slide_index").val();
                          var recent_total_slides = $("input#recent_total_slides").val();
//                          console.log(recent_slide_index);
//                          console.log(recent_total_slides);
                          if(recent_slide_index <=1) {
                            $("#left-recently").hide();
                          }
                          if(recent_slide_index >= recent_total_slides) {
                            $("#right-recently").hide();
                          }
                        });
                      }
                      else if ($("#recently_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=recent&name=recentlyyear",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#recently_table").html(data);
                          $("#recently_grid").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function recentlyyear() in list-view failed");

                        }).always(function () {
                          var recent_countque = $("input#recent_countque").val();
                          console.log(recent_countque);
                          if (recent_countque < 10) {
                            $("div#more_questions_recent").hide();
                          }
                          else {
                            $("div#more_questions_recent").show();
                          }
                        });
                      }
                    }

                    function recentlycategory(catname)
                    {
                      if ($("#recently_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"catname="+catname+"&view=grid&type=recent&name=recentlycategory",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#recently_start").html(data);
                          $("#recently_table").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function recentlycategory() in grid-view failed");

                        }).always(function () {
                          var recent_slide_index = $("input#recent_slide_index").val();
                          var recent_total_slides = $("input#recent_total_slides").val();
//                          console.log(recent_slide_index);
//                          console.log(recent_total_slides);
                          if(recent_slide_index <=1) {
                            $("#left-recently").hide();
                          }
                          if(recent_slide_index >= recent_total_slides) {
                            $("#right-recently").hide();
                          }
                        });
                      }
                      else if ($("#recently_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"catname="+catname+"&view=list&type=recent&name=recentlycategory",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#recently_table").html(data);
                          $("#recently_grid").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function recentlycategory() in list-view failed");

                        }).always(function () {
                          var recent_countque = $("input#recent_countque").val();
                          console.log(recent_countque);
                          if (recent_countque < 10) {
                            $("div#more_questions_recent").hide();
                          }
                          else {
                            $("div#more_questions_recent").show();
                          }
                        });
                      }
                    }
                    //end recently
                    
                    //all quation start
                    function allquationask()
                    {
                      if ($("#allquation_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=allque&name=allquestions",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#allquation_start").html(data); 
                          $("#allquation_table").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function allquationask() in grid-view failed");

                        }).always(function () {
                          var allque_slide_index = $("input#allque_slide_index").val();
                          var allque_total_slides = $("input#allque_total_slides").val();
//                          console.log(allque_slide_index);
//                          console.log(allque_total_slides);
                          if(allque_slide_index <=1) {
                            $("#allquation-left").hide();
                          }
                          if(allque_slide_index >= allque_total_slides) {
                            $("#allquation-right").hide();
                          }
                        });
                      }
                      else if ($("#allquation_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=allque&name=allquestions",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#allquation_table").html(data); 
                          $("#allquation_grid").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function allquationask() in list-view failed");

                        }).always(function () {
                          var allque_countque = $("input#allque_countque").val();
                          console.log(allque_countque);
                          if (allque_countque < 10) {
                            $("div#more_questions_allque").hide();
                          }
                          else {
                            $("div#more_questions_allque").show();
                          }
                        });
                      }
                    }

                    function allquationweek()
                    {
                      if ($("#allquation_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=allque&name=allqueweek",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#allquation_start").html(data); 
                          $("#allquation_table").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function allquationweek() in grid-view failed");

                        }).always(function () {
                          var allque_slide_index = $("input#allque_slide_index").val();
                          var allque_total_slides = $("input#allque_total_slides").val();
//                          console.log(allque_slide_index);
//                          console.log(allque_total_slides);
                          if(allque_slide_index <= 1) {
                            $("#allquation-left").hide();
                          }
                          if(allque_slide_index >= allque_total_slides) {
                            $("#allquation-right").hide();
                          }
                        });
                      }
                      else if ($("#allquation_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=allque&name=allqueweek",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#allquation_table").html(data); 
                          $("#allquation_grid").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function allquationweek() in list-view failed");

                        }).always(function () {
                          var allque_countque = $("input#allque_countque").val();
                          console.log(allque_countque);
                          if (allque_countque < 10) {
                            $("div#more_questions_allque").hide();
                          }
                          else {
                            $("div#more_questions_allque").show();
                          }
                        });
                      }
                    }

                    function allquationmonth()
                    {
                      if ($("#allquation_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=allque&name=allquemonth",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#allquation_start").html(data); 
                          $("#allquation_table").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function allquationmonth() in grid-view failed");

                        }).always(function () {
                          var allque_slide_index = $("input#allque_slide_index").val();
                          var allque_total_slides = $("input#allque_total_slides").val();
//                          console.log(allque_slide_index);
//                          console.log(allque_total_slides);
                          if(allque_slide_index <= 1) {
                            $("#allquation-left").hide();
                          }
                          if(allque_slide_index >= allque_total_slides) {
                            $("#allquation-right").hide();
                          }
                        });
                      }
                      else if ($("#allquation_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=allque&name=allquemonth",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#allquation_table").html(data); 
                          $("#allquation_grid").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function allquationmonth() in list-view failed");

                        }).always(function () {
                          var allque_countque = $("input#allque_countque").val();
                          console.log(allque_countque);
                          if (allque_countque < 10) {
                            $("div#more_questions_allque").hide();
                          }
                          else {
                            $("div#more_questions_allque").show();
                          }
                        });
                      }
                    }
                    
                    function allquationyear()
                    {
                      if ($("#allquation_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=allque&name=allqueyear",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#allquation_start").html(data); 
                          $("#allquation_table").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function allquationyear() in grid-view failed");

                        }).always(function () {
                          var allque_slide_index = $("input#allque_slide_index").val();
                          var allque_total_slides = $("input#allque_total_slides").val();
//                          console.log(allque_slide_index);
//                          console.log(allque_total_slides);
                          if(allque_slide_index <= 1) {
                            $("#allquation-left").hide();
                          }
                          if(allque_slide_index >= allque_total_slides) {
                            $("#allquation-right").hide();
                          }
                        });
                      }
                      else if ($("#allquation_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=allque&name=allqueyear",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#allquation_table").html(data); 
                          $("#allquation_grid").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function allquationyear() in list-view failed");

                        }).always(function () {
                          var allque_countque = $("input#allque_countque").val();
                          console.log(allque_countque);
                          if (allque_countque < 10) {
                            $("div#more_questions_allque").hide();
                          }
                          else {
                            $("div#more_questions_allque").show();
                          }
                        });
                      }
                    }                      

                    function allquationcategory(catname)
                    {
                      if ($("#allquation_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"catname="+catname+"&view=grid&type=allque&name=allquecategory",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#allquation_start").html(data); 
                          $("#allquation_table").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function allquationcategory() in grid-view failed");

                        }).always(function () {
                          var allque_slide_index = $("input#allque_slide_index").val();
                          var allque_total_slides = $("input#allque_total_slides").val();
//                          console.log(allque_slide_index);
//                          console.log(allque_total_slides);
                          if(allque_slide_index <= 1) {
                            $("#allquation-left").hide();
                          }
                          if(allque_slide_index >= allque_total_slides) {
                            $("#allquation-right").hide();
                          }
                        });
                      }
                      else if ($("#allquation_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"catname="+catname+"&view=list&type=allque&name=allquecategory",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#allquation_table").html(data); 
                          $("#allquation_grid").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function allquationcategory() in list-view failed");

                        }).always(function () {
                          var allque_countque = $("input#allque_countque").val();
                          console.log(allque_countque);
                          if (allque_countque < 10) {
                            $("div#more_questions_allque").hide();
                          }
                          else {
                            $("div#more_questions_allque").show();
                          }
                        });
                      }
                    }
                    //end all quatonion
                    

                    //start unanswer section
                    function unansquationask()
                    {
                      if ($("#unansquation_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=unans&name=unanswered",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#unansquation_start").html(data);
                          $("#unansquation_table").hide();              

                        }).fail(function () {
                          console.log();
                          alert("Function unansquationask() in grid-view failed");                        

                        }).always(function () {
                          var unans_slide_index = $("input#unans_slide_index").val();
                          var unans_total_slides = $("input#unans_total_slides").val();
//                          console.log(unans_slide_index);
//                          console.log(unans_total_slides);
                          if(unans_slide_index <= 1) {
                            $("#unansquation-left").hide();
                          }
                          if(unans_slide_index >= unans_total_slides) {
                            $("#unansquation-right").hide();
                          }
                        });
                      }
                      else if ($("#unansquation_table").is(":visible")) {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=unans&name=unanswered",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#unansquation_table").html(data); 
                          $("#unansquation_grid").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function unansquationask() in list-view failed");                        

                        }).always(function () {
                          var unans_countque = $("input#unans_countque").val();
                          console.log(unans_countque);
                          if (unans_countque < 10) {
                            $("div#more_questions_unans").hide();
                          }
                          else {
                            $("div#more_questions_unans").show();
                          }
                        });
                      }
                    }
                     
                    function unansquationweek()
                    {
                      if ($("#unansquation_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=unans&name=unansweek",
                          type: "POST",
                          dataType: "html"
                      
                          }).done(function (data) {
                          $("#unansquation_start").html(data); 
                          $("#unansquation_table").hide();                          

                          }).fail(function () {
                          console.log();
                          alert("Function unansquationweek() in grid-view failed");                        

                          }).always(function () {
                          var unans_slide_index = $("input#unans_slide_index").val();
                          var unans_total_slides = $("input#unans_total_slides").val();
//                          console.log(unans_slide_index);
//                          console.log(unans_total_slides);
                          if(unans_slide_index <= 1) {
                            $("#unansquation-left").hide();
                          }
                          if(unans_slide_index >= unans_total_slides) {
                            $("#unansquation-right").hide();
                          }
                        });
                      }
                      else if ($("#unansquation_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=unans&name=unansweek",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#unansquation_table").html(data); 
                          $("#unansquation_grid").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function unansquationweek() in list-view failed");                        

                        }).always(function () {
                          var unans_countque = $("input#unans_countque").val();
                          console.log(unans_countque);
                          if (unans_countque < 10) {
                            $("div#more_questions_unans").hide();
                          }
                          else {
                            $("div#more_questions_unans").show();
                          }
                        });
                      }
                    }

                    function unansquationmonth()
                    {
                      if ($("#unansquation_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=unans&name=unansmonth",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#unansquation_start").html(data); 
                          $("#unansquation_table").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function unansquationmonth() in grid-view failed");                        

                        }).always(function () {
                          var unans_slide_index = $("input#unans_slide_index").val();
                          var unans_total_slides = $("input#unans_total_slides").val();
//                          console.log(unans_slide_index);
//                          console.log(unans_total_slides);
                          if(unans_slide_index <= 1) {
                            $("#unansquation-left").hide();
                          }
                          if(unans_slide_index >= unans_total_slides) {
                            $("#unansquation-right").hide();
                          }
                        });
                      }
                      else if ($("#unansquation_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=unans&name=unansmonth",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#unansquation_table").html(data); 
                          $("#unansquation_grid").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function unansquationmonth() in list-view failed");                        

                        }).always(function () {
                          var unans_countque = $("input#unans_countque").val();
                          console.log(unans_countque);
                          if (unans_countque < 10) {
                            $("div#more_questions_unans").hide();
                          }
                          else {
                            $("div#more_questions_unans").show();
                          }
                        });
                      }
                    }

                    function unansquationyear()
                    {
                      if ($("#unansquation_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=grid&type=unans&name=unansyear",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#unansquation_start").html(data); 
                          $("#unansquation_table").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function unansquationyear() in grid-view failed");                        

                        }).always(function () {
                          var unans_slide_index = $("input#unans_slide_index").val();
                          var unans_total_slides = $("input#unans_total_slides").val();
//                          console.log(unans_slide_index);
//                          console.log(unans_total_slides);
                          if(unans_slide_index <= 1) {
                            $("#unansquation-left").hide();
                          }
                          if(unans_slide_index >= unans_total_slides) {
                            $("#unansquation-right").hide();
                          }
                        });
                      }
                      else if ($("#unansquation_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"view=list&type=unans&name=unansyear",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#unansquation_table").html(data); 
                          $("#unansquation_grid").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function unansquationyear() in list-view failed");                        

                        }).always(function () {
                          var unans_countque = $("input#unans_countque").val();
                          console.log(unans_countque);
                          if (unans_countque < 10) {
                            $("div#more_questions_unans").hide();
                          }
                          else {
                            $("div#more_questions_unans").show();
                          }
                        });
                      }
                    }

                    function unansquationcategory(catname)
                    {
                      if ($("#unansquation_grid").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"catname="+catname+"&view=grid&type=unans&name=unanscategory",
                          type:"POST",
                          dataType:"html"
                      
                        }).done(function (data) {
                          $("#unansquation_start").html(data); 
                          $("#unansquation_table").hide();                          

                        }).fail(function () {
                          console.log();
                          alert("Function unansquationcategory() in grid-view failed");                        

                        }).always(function () {
                          var unans_slide_index = $("input#unans_slide_index").val();
                          var unans_total_slides = $("input#unans_total_slides").val();
//                          console.log(unans_slide_index);
//                          console.log(unans_total_slides);
                          if(unans_slide_index <= 1) {
                            $("#unansquation-left").hide();
                          }
                          if(unans_slide_index >= unans_total_slides) {
                            $("#unansquation-right").hide();
                          }
                        });
                      }
                      else if ($("#unansquation_table").is(":visible"))
                      {
                        $.ajax({
                          url: getAjaxLinkHeadFilters(),
                          data:"catname="+catname+"&view=list&type=unans&name=unanscategory",
                          type: "POST",
                          dataType: "html"
                      
                        }).done(function (data) {
                          $("#unansquation_table").html(data);
                          $("#unansquation_grid").hide();

                        }).fail(function () {
                          console.log();
                          alert("Function unansquationcategory() in list-view failed");                        

                        }).always(function () {
                          var unans_countque = $("input#unans_countque").val();
                          console.log(unans_countque);
                          if (unans_countque < 10) {
                            $("div#more_questions_unans").hide();
                          }
                          else {
                            $("div#more_questions_unans").show();
                          }
                        });
                      }
                    }
                    //end unanswer section

                    //Made by Julio A. Velasquez Basabe at 20160609
                    // in order to retrieve more questions in list-view sections
                    function more_questions_listview_most()
                    {
                          $.ajax({
                              url: getAjaxLinkHeadFilters(),
                              data:"view=list&name=more&type=most",
                              type:"POST",
                              dataType:"html"

                            }).done(function (data) {
                                $("#mostpopular_table").append(data);

                            }).fail(function() {
                                console.log();
                                alert("Function more_questions_listview_most() failed");
                                
                            }).always(function() {
                                var most_countque = parseInt($("input#most_countque:last").val());
                                var most_index = parseInt($("input#most_index:last").val());
//                                console.log("most_countque: " + most_countque);
//                                console.log("most_index: " + most_index);
                                if (most_index === most_countque) {
                                  $("div#more_questions_most").hide();
                                }
                                else {
                                  $("div#more_questions_most").show();
                                }
                            });
                    }

                    function more_questions_listview_recent()
                    {
                            $.ajax({
                              url: getAjaxLinkHeadFilters(),
                              data:"view=list&name=more&type=recent",
                              type:"POST",
                              dataType:"html"

                            }).done(function (data) {
                                $("#recently_table").append(data);

                            }).fail(function () {
                                console.log();
                                alert("Function more_questions_listview_recent() failed");
                                
                            }).always(function () {                                
                                var recent_countque = parseInt($("input#recent_countque:last").val());
                                var recent_index = parseInt($("input#recent_index:last").val());
//                                console.log("recent_countque: " + recent_countque);
//                                console.log("recent_index: " + recent_index);
                                if (recent_index === recent_countque) {
                                  $("div#more_questions_recent").hide();
                                }
                                else {
                                  $("div#more_questions_recent").show();
                                }
                            });
                    }

                    function more_questions_listview_allque()
                    {
                          $.ajax({
                              url: getAjaxLinkHeadFilters(),
                              data:"view=list&name=more&type=allque",
                              type:"POST",
                              dataType:"html"

                            }).done(function (data) {
                                $("#allquation_table").append(data);

                            }).fail(function () {
                                console.log();
                                alert("Function more_questions_listview_allque() failed");
                                
                            }).always(function () {
                                var allque_countque = parseInt($("input#allque_countque:last").val());
                                var allque_index = parseInt($("input#allque_index:last").val());
//                                console.log("allque_countque: " + allque_countque);
//                                console.log("allque_index: " + allque_index);
                                if (allque_index === allque_countque) {
                                  $("div#more_questions_allque").hide();
                                }
                                else {
                                  $("div#more_questions_allque").show();
                                }
                            });
                    }

                    function more_questions_listview_unans()
                    {
                          $.ajax({
                              url: getAjaxLinkHeadFilters(),
                              data:"view=list&name=more&type=unans",
                              type:"POST",
                              dataType:"html"

                            }).done(function (data) {                                                              
                                $("#unansquation_table").append(data);

                            }).fail(function () {
                                console.log();
                                alert("Function more_questions_listview_unans() failed");
                                
                            }).always(function () {                                
                                var unans_countque = parseInt($("input#unans_countque:last").val());
                                var unans_index = parseInt($("input#unans_index:last").val());
//                                console.log("unans_countque: " + unans_countque);
//                                console.log("unans_index: " + unans_index);
                                if (unans_index === unans_countque) {
                                  $("div#more_questions_unans").hide();
                                }
                                else {
                                  $("div#more_questions_unans").show();
                                }
                            });
                    }


    function NewQuestion()
    {
        var validations_flag = true;
        var atLeastOneIsChecked = false;

        var title = $("#title").val();
        var content = $("#newq_content").val();
        
        var fields = [];
        fields[0] = title;
        fields[1] = content;

        var string_tags = $("input#tags").val();
        var tags = [];
        tags = string_tags.split(" ");
/*
        $.ajax({
            url: getAjaxLinkTags(),
            data:"title:"+title,

        });
*/
        var catchk = [];
        catchk = $('div>input[name="cat[]"]:checked');
        atLeastOneIsChecked = catchk.length > 0;
        //console.log(atLeastOneIsChecked);
        
        if (atLeastOneIsChecked) {
          var categories = [];
          for (var i = 0; i < catchk.length; i++) {
            categories[i] = catchk[i].value;
            console.log(categories[i]);
          }
        }
        else validations_flag = false;

        var setchk = [];
        setchk = $('input[name="set[]"]:checked');
        atLeastOneIsChecked = setchk.length > 0;

        if (atLeastOneIsChecked) {
          var settings = [];
          for (var i = 0; i < setchk.length; i++) {
            settings[i] = setchk[i].value;
            console.log(settings[i]);
          }
        }

        $.ajax({
            url: getAjaxLinkNewQuestion(),
            data: {fld_array:fields,tag_array:tags,cat_array:categories,set_array:settings},
            type:"POST",
            dataType:"json",

            success: function(resp) {
                if (resp.success)
                {
                  console.log(resp.success);
                  console.log(resp.postid);
                  alert("Has been added a new question with postid: " + resp.postid);

                  var path = getBaseUrl() + "?qa=" + resp.postid + "/" + content.split(' ').join('-');
                  console.log(path);
                  location.href = path;
                }
                else
                {
                  $("button#postnew").attr('disabled', 'disabled');
                  $("textarea#newq_content").attr('disabled', 'disabled');
                  $("button#asknew").attr('disabled', 'disabled');
                  $('input#title').attr('disabled', 'disabled');
                  $("p#login_message").text("You need to be logged in to ask a new question");
                  alert("You need to be logged in to ask a new question");
                }
            },

            error: function( req, status, err ) {
                console.log( 'something went wrong', status, err );
                alert("ERROR: Posting a new question");
            }
/*
            complete : function(resp) {
                console.log(resp.success);
                console.log(resp.postid);
            }
*/
        });
    }
/*
        }).done(function (resp) {
            var sucess = resp.success;
            console.log(success);

            if (success) {
              var question = content;
              console.log(question);

              var path = getBaseUrl() + "?qa=" + resp.postid + "/" + question.split(' ').join('-');
              console.log(path);
              location.href = path;
            }
            else {
              console.log("You must log in to ask a new question");
              alert("You must log in to ask a new question");
            }

        }).fail(function (resp) {
            console.log(resp.success);
            

        }).always(function (resp) {
            console.log(resp.postid);
            alert(resp.postid);
        });
*/        


                     
$(document).ready(function() {

    $("#mostpopular_table").hide();
    $("#left").hide();

    $("div.qa_gallery.first.recently_ask").hide();
    $("#recently_start").hide();
    $("#recently_table").hide();
    $("#left-recently").hide();

    $("div.qa_gallery.first.allquation").hide();
    $("#allquation_start").hide();
    $("#allquation_table").hide();
    $("#allquation-left").hide();
                                                
    $("div.qa_gallery.first.unansquation").hide();
    $("#unansquation_start").hide();
    $("#unansquation_table").hide();
    $("#unansquation-left").hide();

    $("div.more_questions").hide();
		$(".inner .ans").hide();


    //$(document).on( eventName, selector, function(){} );
    $("body").on("mouseover", ".inner", function(e) {
        $(this).find(".ans").show();
    });
                                  
    $("body").on("mouseout", ".inner", function(e) {
        $(this).find(".ans").hide();
    });


    //Function created by Julio A. Velasquez Basabe at 20160519
    //for change css style class in landing page filter links
    $("ul.type_filter").on("click", "li", function()
    {
        $("ul.type_filter").find(".active").removeClass("active");
        $(this).addClass("active");
    });

    $("ul.filter").on("click", "li", function() {
        $("ul.type_filter").find(".active").removeClass("active");
    });


    $(function() {
      var cache = {};
      $( "#title" ).autocomplete({

        minLength: 3,
        maxShowItems: 7,
        dataType: "json",
        autoFocus: true,

        source: function( request, response ) {
          var term = request.term;
          if ( term in cache ) {
            response( cache[ term ] );
            return;
          }
          $.getJSON( getAjaxAutocomplete(), request, function( data, status, xhr )
          {
            cache[ term ] = data;
            var results = $.ui.autocomplete.filter(data, request.term);
            response(results.slice(0, 5));
//            response( data );
          });
        },

        open: function() {
          $("li.ui-menu-item:last").append(function() {
            $(this).html('<div class="search_input"><i class="fa fa-search fa-lg"></i><span class="search">&nbspSearch: </span><span class="bold"><a href="' + getBaseUrl() + '?qa=search&q=' + $("input#title").val() + '">' + $("input#title").val() + '</a></span></div>');
            //$(this).css('font-weight','900', 'vertical-align','middle!important');

            $("li.ui-menu-item:last").mouseover(function() {
              $(this).css('background', '#e3e3e3');
            });
            $("li.ui-menu-item:last").mouseout(function() {
              $(this).css('background', 'white');
            });
          });
          return false;
        },

        select: function( event, ui ) {
            console.log(ui.item.value);
            console.log(ui.item.label);

            //$(this).val(ui.item.label);
            location.href = getBaseUrl() + "?qa=" + ui.item.value + "/" + ui.item.label;
            return false;
        }
    });
  });

/*
  function postsList()
  {
    $( "#title" ).autocomplete({
      source: getAjaxAutocomplete,
      minLength: 2,
      select: function( event, ui ) {
        item = ui;
        event.preventDefault();
        $($this).val(ui.item.title + ui.item.Label);
      }
    });
  }
*/
/*
$(function() {
    function log( message ) {
      $( "<div>" ).text( message ).prepespan.newq_link.fa.fa-pencil.fa-lgndTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
$( "#title" ).autocomplete(
{
      source: function( request, response ) {

        $.ajax({
          url: getAjaxAutocomplete(),
          dataType: "json",
          data: {
            q: request.term
          },
          success: function( data ) {
            var results = $.ui.autocomplete.filter(data, request.term);
            response(data.slice(0, 5));
            //response( data );
          }
        });
      },
      minLength: 7,
      select: function( event, ui ) {
        log( ui.item ?
          "Selected: " + ui.item.title :
          "Nothing selected, input was " + this.value);
          $(this).val(ui.item.title);
      },
      open: function() {
        //$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
        //$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
    });
  });
*/
/*
  $(function() {
    var cache = {};
    $( "#title" ).autocomplete({
      minLength: 3,
      autoFocus: true,
      dataType: "json",

      source: function( request, response ) {
        var term = request.term;
        if ( term in cache ) {
          response( cache[ term ] );
          return;
        }
        $.getJSON( getAjaxAutocomplete(), request, function( data, status, xhr ) {
          cache[ term ] = data;
          response( data );
        });
      },

/*
    $("input#title").autocomplete({
//        source: [{"key1":"value1"},{"key1":"value2"},{"key1":"value3"},{"key1":"value4"}],
//        source: ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"],
        source: getAjaxAutocomplete(),
        dataType: "json",
        autoFocus: true,
        minLength: 3,
        maxShowItems: 7,
/*
        open: function() {
        setTimeout(function () {
            $('.ui-autocomplete').css('z-index', 99999999999999);
        }, 0);
        },
*/
/*
        open: function() {
          $("li.ui-menu-item:last").append(function() {
            $(this).html('&nbsp<div><li>&nbsp<i class="fa fa-search fa-lg"></i> Search: ' + $("input#title").val() + '</li></div>');
            $(this).css('font-weight','900', 'margin-bottom','20px!important');

            $("li.ui-menu-item:last").mouseover(function() {
              $(this).css('background', '#e3e3e3');
            });
            $("li.ui-menu-item:last").mouseout(function() {
              $(this).css('background', 'white');
            });
          });
          return false;
        },

        select: function(event, ui) {
          console.log(ui.item.postid);
          console.log(ui.item.title);
          $(this).val(ui.item.title);
          return false;
        }

    });

//  });
/*
        //Obtenemos el value del input
        var asking = $(this).val();
        console.log("asking value: " + asking);
        var dataString = 'title=' + asking;
        console.log("dataString value: " + dataString);

        //Le pasamos el valor del input al ajax
        var path = getAjaxAutocomplete();
        console.log("path: " + path);
        $.ajax({
            type: "POST",
            url: path,
            data: dataString,
            success: function(data) {
                //Escribimos las sugerencias que nos manda la consulta
                $('#suggestions').fadeIn(1000).html(data);
                //Al hacer click en algua de las sugerencias
                $('.suggest-element').on('click', function()
                {
                    //Obtenemos la id unica de la sugerencia pulsada
                    var id = $(this).attr('postid');
                    console.log("postid value: " + id);
                    //Editamos el valor del input con data de la sugerencia pulsada
                    $('#title').val($('#'+id).attr('data'));
                    //Hacemos desaparecer el resto de sugerencias
                    $('#suggestions').fadeOut(1000);
                });              
            }
        });
    });
/*
        $("div.ask_bar").dialog(
        {
            focus: function ()
            {
                var dialogIndex = parseInt($(this).parent().css("z-index"), 10);
                $(this).find(".ui-autocomplete-input").each(function (i, obj)
                {
                    $(obj).autocomplete("widget").css("z-index", dialogIndex + 1);
                });
            }
        });
*/

    $("#asknew").on("click", function() {

        var title = $("#title").val();

        $.ajax({
            url: getAjaxLinkTags(),
            data: "title="+title,
            type: "POST",
            dataType: "json",

            success: function(resp) {
                $("input#newq_title").val(title).attr('disabled', 'disabled');
                $("div#new_question").fadeIn(300);

              /*
                if (resp.islogged) {
                  console.log(resp.islogged);
                  $("div#new_question").toggle();
                }
                else {
                  alert("You need to be logged in to ask a new question");
                  var title = $("input#title");

                  title.attr('disabled', 'disabled');
                  title.val("You need to be logged in to ask a new question");
                  title.css('font-weight', 'bold');
                }
              */
            },

            error: function( req, status, err ) {
                console.log( 'something went wrong', status, err );
                alert("ERROR: Trying post a new question");
            }
        });
    });

    $("span.iconclose").on("click", function() {
        $("div#new_question").fadeOut(300);
    });

    $(document).keyup(function(e) {
        if (e.which == 27) { //ESC
          $("div#new_question").fadeOut(300); //Hide 'New Question' dialog
    }

    $("span.newq_link.fa.fa-pencil.fa-lg").on("click", function() {
        $("input#newq_title").prop('disabled', false);
        //$("input#newq_title").attr('disabled', 'enabled');
    });
});



    $("span#most-view-list").on("click", "i" , function(e) {
        $("div.more_questions").show();
    });

    $("span#most-view-grid").on("click", "i" , function(e) {
//        $("div#more_questions_most").hide();
        $("div.more_questions").hide();
    });

    $("span#recently-view-list").on("click", "i" , function(e) {
        $("div.more_questions").show();
    });

    $("span#recently-view-grid").on("click", "i" , function(e) {
        //$("div#more_questions_recent").hide();
        $("div#more_questions").hide();
    });

    $("span#allquation-list").on("click", "i" , function(e) {
        $("div.more_questions").show();
    });

    $("span#allquation-grid").on("click", "i", function(e) {
        $("div.more_questions").hide();
//        $("div#more_questions_allque").hide();
    });

    $("span#unansquation-list").on("click", "i", function(e) {
        $("div.more_questions").show();
    });

    $("span#unansquation-grid").on("click", "i" , function(e) {
        $("div.more_questions").hide();
//        $("div#more_questions_unans").hide();
    });


    $("a#more_link_unans").on("click", "b", function(e) {

        var listview_index = parseInt($("input#unans_index:last").val());
        var countque = parseInt($("input#unans_countque:last").val());

//        console.log(listview_index);
//        console.log(countque);

        if (listview_index === countque) {
//            console.log("true: " + listview_index + " = " + countque + " :show");
            $("div#more_questions_unans").show();
        }      
        else if (listview_index >= countque-10) {
//            console.log("false: " + listview_index + " >= " + countque + " :hide");
            $("div#more_questions_unans").hide();
        }
    });

    $("a#more_link_allque").on("click", "b", function(e) {

        var listview_index = parseInt($("input#allque_index:last").val());
        var countque = parseInt($("input#allque_countque:last").val());

//        console.log(listview_index);
//        console.log(countque);

        if (listview_index === countque) {
//            console.log("true: " + listview_index + " = " + countque + " :show by first time");
            $("div#more_questions_allque").show();
        }      
        else if (listview_index >= countque-10) {
//            console.log("false: " + listview_index + " >= " + countque + " :hide");
            $("div#more_questions_allque").hide();
        }
    });

    $("a#more_link_recent").on("click", "b", function(e) {

        var listview_index = parseInt($("input#recent_index:last").val());
        var countque = parseInt($("input#recent_countque:last").val());

//        console.log(listview_index);
//        console.log(countque);

        if (listview_index === countque) {
//            console.log("true: " + listview_index + " = " + countque + " :show by first time");
            $("div#more_questions_recent").show();
        }      
        else if (listview_index >= countque-10) {
//            console.log("false: " + listview_index + " >= " + countque + " :hide");
            $("div#more_questions_recent").hide();
        }
    });

    $("a#more_link_most").on("click", "b", function(e) {

        var listview_index = parseInt($("input#most_index:last").val());
        var countque = parseInt($("input#most_countque:last").val());

//        console.log(listview_index);
//        console.log(countque);

        if (listview_index === countque) {
//            console.log("true: " + listview_index + " = " + countque + " :show by first time");
            $("div#more_questions_most").show();
        }      
        else if (listview_index >= countque-10) {
//            console.log("false: " + listview_index + " >= " + countque + " :hide");
            $("div#more_questions_most").hide();
        }
    });


/*--------------------------	Hide all sections and switch between sections clicking arrows up/down
----------------------------	This was made by Julio A. Velasquez at 20160415	-------------------*/

								                //most popular
                                $("a.view_icons").on("click","#most-view-list", function()
                                {
                                    $("#most-view-grid").removeClass("red-active");
                                    $(this).addClass("red-active");
                                    $("#recently-view-grid").removeClass("red-active");
                                    $("#recently-view-list").addClass("red-active");
                                    $("#allquation-grid").removeClass("red-active");
                                    $("#allquation-list").addClass("red-active");
                                    $("#unansquation-grid").removeClass("red-active");
                                    $("#unansquation-list").addClass("red-active");

                                    $("#mostpopular_grid").hide();
                                    $("#mostpopular_table").show();
                                    $("#recently_grid").hide();
                                    $("#recently_table").show();
                                    $("#allquation_grid").hide();
                                    $("#allquation_table").show();
                                    $("#unansquation_grid").hide();
                                    $("#unansquation_table").show();
                                    //$("#left").hide();
                                });

                                $("a.view_icons").on("click","#most-view-grid", function()
                                {
                                    $("#most-view-list").removeClass("red-active");
                                    $(this).addClass("red-active");
                                    $("#recently-view-list").removeClass("red-active");
                                    $("#recently-view-grid").addClass("red-active");
                                    $("#allquation-list").removeClass("red-active");
                                    $("#allquation-grid").addClass("red-active");
                                    $("#unansquation-list").removeClass("red-active");
                                    $("#unansquation-grid").addClass("red-active");

                                    $("#mostpopular_table").hide();
                                    $("#mostpopular_grid").show();
                                    $("#recently_table").hide();
                                    $("#recently_grid").show();
                                    $("#allquation_table").hide();
                                    $("#allquation_grid").show();
                                    $("#unansquation_table").hide();
                                    $("#unansquation_grid").show();
                                    //$("#left").hide();
                                });

                                //recently asked                                
                                $("a.view_icons").on("click","#recently-view-list", function()
                                {
                                    $("#recently-view-grid").removeClass("red-active");
                                    $(this).addClass("red-active");                                    
                                    $("#most-view-grid").removeClass("red-active");
                                    $("#most-view-list").addClass("red-active");
                                    $("#allquation-grid").removeClass("red-active");
                                    $("#allquation-list").addClass("red-active");
                                    $("#unansquation-grid").removeClass("red-active");
                                    $("#unansquation-list").addClass("red-active");

                                    $("#mostpopular_grid").hide();
                                    $("#mostpopular_table").show();
                                    $("#recently_grid").hide();
                                    $("#recently_table").show();
                                    $("#allquation_grid").hide();
                                    $("#allquation_table").show();
                                    $("#unansquation_grid").hide();
                                    $("#unansquation_table").show();
                                });

                                $("a.view_icons").on("click","#recently-view-grid", function()
                                {                                    
                                    $("#recently-view-list").removeClass("red-active");
                                    $(this).addClass("red-active");
                                    $("#most-view-list").removeClass("red-active");
                                    $("#most-view-grid").addClass("red-active");
                                    $("#allquation-list").removeClass("red-active");
                                    $("#allquation-grid").addClass("red-active");
                                    $("#unansquation-list").removeClass("red-active");
                                    $("#unansquation-grid").addClass("red-active");

                                    $("#mostpopular_table").hide();
                                    $("#mostpopular_grid").show();
                                    $("#recently_table").hide();
                                    $("#recently_grid").show();
                                    $("#allquation_table").hide();
                                    $("#allquation_grid").show();
                                    $("#unansquation_table").hide();
                                    $("#unansquation_grid").show();
                                });   

                                //all questions								                
                                $("a.view_icons").on("click","#allquation-list", function()
                                {                                    
                                    $("#allquation-grid").removeClass("red-active");
                                    $(this).addClass("red-active");
                                    $("#most-view-grid").removeClass("red-active");
                                    $("#most-view-list").addClass("red-active");
                                    $("#recently-view-grid").removeClass("red-active");
                                    $("#recently-view-list").addClass("red-active");
                                    $("#unansquation-grid").removeClass("red-active");
                                    $("#unansquation-list").addClass("red-active");

                                    $("#mostpopular_grid").hide();
                                    $("#mostpopular_table").show();
                                    $("#recently_grid").hide();
                                    $("#recently_table").show();
                                    $("#allquation_grid").hide();
                                    $("#allquation_table").show();
                                    $("#unansquation_grid").hide();
                                    $("#unansquation_table").show();
                                });

                                $("a.view_icons").on("click","#allquation-grid", function()
                                {                                    
                                    $("#allquation-list").removeClass("red-active");
                                    $(this).addClass("red-active");
                                    $("#most-view-list").removeClass("red-active");
                                    $("#most-view-grid").addClass("red-active");
                                    $("#recently-view-list").removeClass("red-active");
                                    $("#recently-view-grid").addClass("red-active");
                                    $("#unansquation-list").removeClass("red-active");
                                    $("#unansquation-grid").addClass("red-active");

                                    $("#mostpopular_table").hide();
                                    $("#mostpopular_grid").show();
                                    $("#recently_table").hide();
                                    $("#recently_grid").show();
                                    $("#allquation_table").hide();
                                    $("#allquation_grid").show();
                                    $("#unansquation_table").hide();
                                    $("#unansquation_grid").show();
                                });

                                //unans questions                                
                                $("a.view_icons").on("click","#unansquation-list", function()
                                {                                    
                                    $("#unansquation-grid").removeClass("red-active");
                                    $(this).addClass("red-active");
                                    $("#most-view-grid").removeClass("red-active");
                                    $("#most-view-list").addClass("red-active");
                                    $("#recently-view-grid").removeClass("red-active");
                                    $("#recently-view-list").addClass("red-active");
                                    $("#allquation-grid").removeClass("red-active");
                                    $("#allquation-list").addClass("red-active");

                                    $("#mostpopular_grid").hide();
                                    $("#mostpopular_table").show();
                                    $("#recently_grid").hide();
                                    $("#recently_table").show();
                                    $("#allquation_grid").hide();
                                    $("#allquation_table").show();
                                    $("#unansquation_grid").hide();
                                    $("#unansquation_table").show();
                                });
                                
                                $("a.view_icons").on("click","#unansquation-grid", function()
                                {                                    
                                    $("#unansquation-list").removeClass("red-active");
                                    $(this).addClass("red-active");
                                    $("#most-view-list").removeClass("red-active");
                                    $("#most-view-grid").addClass("red-active");
                                    $("#recently-view-list").removeClass("red-active");
                                    $("#recently-view-grid").addClass("red-active");
                                    $("#allquation-list").removeClass("red-active");
                                    $("#allquation-grid").addClass("red-active");

                                    $("#mostpopular_table").hide();
                                    $("#mostpopular_grid").show();
                                    $("#recently_table").hide();
                                    $("#recently_grid").show();
                                    $("#allquation_table").hide();
                                    $("#allquation_grid").show();
                                    $("#unansquation_table").hide();
                                    $("#unansquation_grid").show();
                                });
                               
                                
                                $(".most_popular_fad_up").click(function()
                                {
                                	$("#most_popular_start").hide();
                                	$("div.qa_gallery.first.most_popular").hide();
                                	$("div.qa_gallery.first.unansquation").show();
                                	$("#unansquation_start").show("slide", {direction:"up"}, 1000);
                                });

                                $(".most_popular_fad_down").click(function() 
                                {
                                	$("#most_popular_start").hide();
                                	$("div.qa_gallery.first.most_popular").hide();
                                	$("div.qa_gallery.first.recently_ask").show();
                                	$("#recently_start").show("slide", {direction:"up"}, 1000);
                                });
                                
                                $(".recently_fad_up").click(function()
                                {
                                  $("#recently_start").hide();
                                  $("div.qa_gallery.first.recently_ask").hide();
                                  $("div.qa_gallery.first.most_popular").show();
                                  $("#most_popular_start").show("slide", {direction:"up"}, 1000);
                                });

                                $(".recently_fad_down").click(function()
                                {
                                    $("#recently_start").hide();
                                    $("div.qa_gallery.first.recently_ask").hide();
                                    $("div.qa_gallery.first.allquation").show();
                                    $("#allquation_start").show("slide", {direction:"up"}, 1000);
                                });
                                
                                $(".allque_fad_up").click(function()
                                {
                                    $("#allquation_start").hide();
                                    $("div.qa_gallery.first.allquation").hide();
                                    $("div.qa_gallery.first.recently_ask").show();
                                    $("#recently_start").show("slide", {direction:"up"}, 1000);
                                });

                                $(".allque_fad_down").click(function()
                                {
                                	$("#allquation_start").hide();
                                	$("div.qa_gallery.first.allquation").hide();
                                	$("div.qa_gallery.first.unansquation").show();
                                  $("#unansquation_start").show("slide", {direction:"up"}, 1000);
                                });
                                
                                $(".unansque_fad_up").click(function()
                                {
                                    $("#unansquation_start").hide();
                                    $("div.qa_gallery.first.unansquation").hide();
                                    $("div.qa_gallery.first.allquation").show();
                                    $("#allquation_start").show("slide", {direction:"up"}, 1000);
                                });

                                $(".unansque_fad_down").click(function()
                                {
                                     $("#unansquation_start").hide();
                                     $("div.qa_gallery.first.unansquation").hide();
                                     $("div.qa_gallery.first.most_popular").show();
                                     $("#most_popular_start").show("slide", {direction:"up"}, 1000);
                                });
});

/*--------------------------	End Changes by Julio A. Velasquez at 20160415	----------------------
----------------------------------------------------------------------------------------------------*/

                    
                        function wantAnswerClick(elem, userid, postid) {
                           $.ajax({
                                    url: getAjaxLinkVotes(),
                                    data: {type:'wantanswer',postid: postid, userid:userid},
                                    type: "POST",     
                                    dataType: 'json',
                                    success: function(data){
                                            $(($(elem)[0].parentElement.firstElementChild)).html(data.count);
                                      }
                                 }); 
                        }
