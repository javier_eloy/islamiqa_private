/**
 * Created by gerardo on 24/06/16.
 * g.a.rondon@gmail.com
 *
 *
 */

/**
 *
 *
 *     This is a Javascript DropDown variation plugins to support animatecss
 *     Made for Islamiqa Site
 *     githughosted :
 *
 *
 *     data-toggle="dropdown-animatecss"  animation="bounceInLeft animated"
 *     parent conatiner put "noclose" props for not close when click
 *
 *
 *     */

// https://github.com/paypal/bootstrap-accessibility-plugin/issues/13

/*!
 * Bootstrap v3.3.5 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

/*!
 * Generated using the Bootstrap Customizer (http://getbootstrap.com/customize/?id=47cd5208d263e5fa8714a7312a94293e)
 * Config saved to config.json and https://gist.github.com/47cd5208d263e5fa8714a7312a94293e
 */
if (typeof jQuery === 'undefined') {
    throw new Error('Bootstrap\'s JavaScript requires jQuery')
}
+function ($) {
    'use strict';
    var version = $.fn.jquery.split(' ')[0].split('.')
    if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1) || (version[0] > 2)) {
        throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3')
    }
}(jQuery);

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.6
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */
if (typeof jQuery === 'undefined') {
    throw new Error('Bootstrap\'s JavaScript requires jQuery')
}
+function ($) {
    'use strict';
    var version = $.fn.jquery.split(' ')[0].split('.')
    if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1) || (version[0] > 2)) {
        throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3')
    }
}(jQuery);

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.6
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
    'use strict';

    // DROPDOWN CLASS DEFINITION
    // =========================

    var backdrop = '.dropdown-backdrop'
    var toggle   = '[data-toggle-animate="dropdown-animatecss"]'
    var DropdownAnimate = function (element) {
        $(element).on('click.bs.dropdown', this.toggle)
    }

    DropdownAnimate.VERSION = '3.3.6'

    function getParent($this) {
        var selector = $this.attr('data-target')

        if (!selector) {
            selector = $this.attr('href')
            selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
        }

        var $parent = selector && $(selector)

        return $parent && $parent.length ? $parent : $this.parent()
    }

    function clearMenus(e) {


        if (e && e.which === 3) return

        $(backdrop).remove()
        $(toggle).each(function () {
            var $this         = $(this)
            var $parent       = getParent($this)
            var relatedTarget = { relatedTarget: this }


            // if (!$parent.hasClass($this.attr("animation"))) return

            if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

            if(e){

               // console.log($parent[0])
               // console.log(e.target)
                if($parent[0].hasAttribute("noclose") && $.contains($parent[0], e.target))
                    return;

            }

            //
            //  console.log($($parent[0]).attr("noclose"))
            // console.log($parent[0])

            // $($this).attr("noclose")

           // console.log($this);

            //
            // if(e)
            // console.log($.contains($parent[0], e.target))


            // $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

            // if (e.isDefaultPrevented()) return
            $this.attr('aria-expanded', 'false')
            //   $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget))



            //if(this.hasAttribute("animation-in"))
            $parent.find(".dropdown-menu").removeClass($($this).attr("animated-in"))
            $parent.find(".dropdown-menu").css("z-index","-1000")
            $parent.find(".dropdown-menu").addClass($($this).attr("animated-out"))
            // else
            //    $parent.find(".dropdown-menu").css("display","block")

            // if(this.hasAttribute("animation-out"))
            //   $parent.find(".dropdown-menu").addClass($this.attr("animation-out"))
            // else

            // $parent.find(".dropdown-menu").css("display","none")




            //  $parent.find(".dropdown-menu").css("display","none");
            //  $parent.removeClass('fadeIn animated');
        })
    }

    DropdownAnimate.prototype.toggle = function (e) {
        var $this = $(this)


        if ($this.is('.disabled, :disabled')) return

//    console.log($this.attr("data-toggle"));

        var $parent  = getParent($this)
        var isActive = $parent.hasClass('open')

        clearMenus()

        if (!isActive) {
            if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
                // if mobile we use a backdrop because click events don't delegate
                $(document.createElement('div'))
                    .addClass('dropdown-backdrop')
                    .insertAfter($(this))
                    .on('click', clearMenus)
            }

            var relatedTarget = { relatedTarget: this }
            $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

            if (e.isDefaultPrevented()) return

            $this
                .trigger('focus')
                .attr('aria-expanded', 'true')


            // $parent.find(".dropdown-menu").removeClass($this.attr("animation-out"));

            // console.log($this.data("animation-in"))

            $parent.find(".dropdown-menu").removeClass($($this).attr("animated-out"))
            $parent.find(".dropdown-menu").css("z-index","1000")
            $parent.find(".dropdown-menu").css("display","block");
            $parent.find(".dropdown-menu").addClass($($this).attr("animated-in"));

            $parent.trigger($.Event('shown.bs.dropdown', relatedTarget));

            // $parent
            //  .toggleClass('open')
            //  .trigger($.Event('shown.bs.dropdown', relatedTarget))
        }

        return false
    }

    // Dropdown.prototype.keydown = function (e) {
    //   if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return
    //
    //   var $this = $(this)
    //
    //   e.preventDefault()
    //   e.stopPropagation()
    //
    //   if ($this.is('.disabled, :disabled')) return
    //
    //   var $parent  = getParent($this)
    //   var isActive = $parent.hasClass('open')
    //
    //   if (!isActive && e.which != 27 || isActive && e.which == 27) {
    //     if (e.which == 27) $parent.find(toggle).trigger('focus')
    //       return $this.trigger('click')
    //   }
    //
    //   var desc = ' li:not(.disabled):visible a'
    //   console.log(desc)
    //   var $items = $parent.find('.dropdown-menu' + desc)
    //
    //   if (!$items.length) return
    //
    //   var index = $items.index(e.target)
    //
    //   if (e.which == 38 && index > 0)                 index--         // up
    //   if (e.which == 40 && index < $items.length - 1) index++         // down
    //   if (!~index)                                    index = 0
    //
    //   $items.eq(index).trigger('focus')
    // }


    // DROPDOWN PLUGIN DEFINITION
    // ==========================

    function Plugin(option) {
        //  console.log("call pluguin");
        return this.each(function () {
            var $this = $(this)
            var data  = $this.data('bs.dropdown')

            if (!data) $this.data('bs.dropdown', (data = new DropdownAnimate(this)))
            if (typeof option == 'string') data[option].call($this)
        })
    }

    var old = $.fn.dropdown

    $.fn.dropdown             = Plugin
    $.fn.dropdown.Constructor = DropdownAnimate


    // DROPDOWN NO CONFLICT
    // ====================

    $.fn.dropdown.noConflict = function () {
        // console.log("call pluguin");
        $.fn.dropdown = old
        return this
    }


    // APPLY TO STANDARD DROPDOWN ELEMENTS
    // ===================================

    $(document)
        .on('click.bs.dropdown.data-api', clearMenus)
        .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
        .on('click.bs.dropdown.data-api', toggle, DropdownAnimate.prototype.toggle)
    // .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    // .on('keydown.bs.dropdown.data-api', '.dropdown-menu', Dropdown.prototype.keydown)

}(jQuery);
