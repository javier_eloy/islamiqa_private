//validate form register new user
/*function validate_form_sign_up(){
	var name    = $("#inputName").val()
	var email   = $("#inputEmail").val()
	var pass    = $("#inputPassword").val()
	var rpass   = $("#inputConfirmPassword").val()
	var city    = $("#inputCity").val()
	var country = $("#inputCountry").val()

	if (name == "") {
		$("#inputName").addClass('error_input');
	}

	if (email == "") {
		$("#inputEmail").addClass('error_input');
	}

	if (pass == "") {
		$("#inputPassword").addClass('error_input');
	}

	if (rpass == "") {
		$("#inputConfirmPassword").addClass('error_input');
	}

	if (city == "") {
		$("#inputCity").addClass('error_input');
	}

	if (country == "") {
		$("#inputCountry").addClass('error_input');
	}

}// end function validate form register new user.

//remove css class error_input form register new user
function removetor(flag){	

  var eee = '#'+flag;
  console.log(eee);
	$(selector).removeClass("error_input");
}*/

$(document).ready(function () {

	$('#loginForm').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			emailhandle: {
				validators: {
					notEmpty: {
						message: 'The username is required'
					},
					stringLength: {
						min: 4,
						message: 'The username must be more than 4'
					}
				}
			},
			password: {
				validators: {
					notEmpty: {
						message: 'The password is required'
					}
				}
			}
		}
	});

	$('#frmSignup').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name: {
				validators: {
					notEmpty: {
						message: 'The username is required.'
					},
					stringLength: {
						min: 4,
						max: 10,
						message: 'The username must be more than 4 and less than 10 characters long.'
					},
					regexp: {
						regexp: /^[a-zA-Z0-9_\.]+$/,
						message: 'the username can only consist of alphabetical, number, dot and underscore.'
					},
					blank: {}
				}
			},
			email: {
				validators: {
					notEmpty: {
						message: 'The email is required.'
					},
					emailAddress: {
						message: 'The email is not valid.'
					},
					blank: {}
				}
			},
			password: {
				//enabled: false,
				validators: {
					notEmpty: {
						message: 'The password is required and cannot be empty.'
					},
					stringLength: {
						max: 12,
						min: 6,
						message: 'The password must contain at lest 6 characters and less than 12.'
					}
				}
			},
			rpass: {
				//enabled: false,
				validators: {
					notEmpty: {
						message: 'The conform password is required and cannot be empty.'
					},
					identical: {
						field: 'password',
						message: 'The password and its confirm must be the same.'
					}
				}
			},
			city: {
				validators: {
					notEmpty: {
						message: 'The city is required.'
					}
				}
			},
			country: {
				validators: {
					notEmpty: {
						message: 'The country is required.'
					}
				}
			}
		}
	})
	.on('success.field.fv', function(e, data) {


		e.preventDefault();


		var cnfigPath = window.myConfigPath || "";

	//	console.log(cnfigPath);

		// ?qa=check_mail&email=g.a.rondon@gmail.com

		 if(data.field =="name"){
			 $.get(cnfigPath + "?qa=check_mail&handle="+$("#inputName").val(), function(response){
				 if(response.result>0){
					 data.fv.updateMessage("name", 'blank',response.message)
						 .updateStatus("name", 'INVALID', 'blank');
				 }
			 })
		 }

		// ?qa=check_mail&email=g.a.rondon@gmail.com

		if(data.field =="email"){
			$.get(cnfigPath + "?qa=check_mail&email="+$("#inputEmail").val() , function(response){
				   if(response.result>0){
					   data.fv.updateMessage("email", 'blank',response.message)
						   .updateStatus("email", 'INVALID', 'blank');
				   }
			})
		}
		
		if (data.fv.getInvalidFields().length > 0) {
			data.fv.disableSubmitButtons(true);
		}
	})
	.on('keyup', '[name="pass"]', function(){
		var isEmpty = $(this).val() == '';
		$('#frmSignup')
			.formValidation('enableFieldValidators', 'pass', !isEmpty)
			.formValidation('enableFieldValidators', 'rpass', !isEmpty);
		
		if ($(this).val().length == 1) {
			$('#frmSignup')
				.formValidation('enableFieldValidators', 'pass')
				.formValidation('enableFieldValidators', 'rpass');
		}
	});

	$('#emailForm').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			'email[]': {
				row: '.col-xs-8',
				verbose: false,
				validators: {
					notEmpty: {
						message: 'The email address is required and can\'t be empty.'
					},
					emailAddress: {
						message: 'The input is not a valid email address.'
					},
					stringLength: {
						max: 512,
						message: 'Cannot exceed 512 characters'
					},
					remote: {
						type: 'GET',
						url: 'https://api.mailgun.net/v2/address/validate?callback=?',
						crossDomain: true,
						name: 'address',
						data: {
							api_key: 'pubkey-f2cfe7be19451b7ef9e1e89f34c4ffc3'
						},
						dataType: 'jsonp',
						validKey: 'is_valid',
						message: 'The email is not valid'
					}
				}
			}
		}
	})
	.on('click', '.addButton', function() {
		var $template = $('#emailTemplate'),
			$clone = $template
				.clone()
				.removeClass('hide')
				.removeAttr('id')
				.insertBefore($template);

		$('#emailForm').formValidation('addField', $clone.find('[name="email[]"]'))
	})
	.on('click', '.removeButton', function() {
		var $row = $(this).closest('.form-group');

		$('#emailForm').formValidation('removeField', $row.find('[name="email[]"]'));

		$row.remove();
	})
	.on('success.validator.fv', function(e, data) {
			if (data.field === 'email[]' && data.validator === 'remote') {
				var response = data.result;
				if (response.did_you_mean) {
					data.element
						.data('fv.messages')
						.find('[data-fv-validator="remote"][data-fv-for="email[]"]')
						.html('Did you mean ' + response.did_you_mean + '?')
						.show();
				}
			}
		})
	.on('err.validator.fv', function(e, data) {
		if (data.field === 'email[]' && data.validator === 'remote') {
			var response = data.result;
			data.element
				.data('fv.messages')
				.find('[data-fv-validator="remote"][data-fv-for="email[]"]')
				.html(response.did_you_mean
					? 'Did you mean ' + response.did_you_mean + '?'
					: 'The email is not valid')
				.show();
		}
	})
	.on('success.form.fv', function(e) {
		e.preventDefault();

		var  url = String(getAjaxCustomPageLink())+"fun_jq.php";
		var emailArray = new Array();

		function cleanArray(actual) {
			var newArray = new Array();
			for (var i = 0; i < actual.length; i++) {
				if (actual[i]) {
					newArray.push(actual[i]);
				}
			}
			return newArray;
		}

		$("input[name='email\\[\\]']").each(function() {
			emailArray.push($(this).val());
		});

		$.ajax({
			url: url,
			type: 'post',
			data: {
				flag: "send_friends_mails",
				emails: emailArray
			},
			error: function(data) {
				alert('error sending data');
			},
			success: function(data) {
			//	console.log(data);
			}
		});

		//console.log(cleanArray(emailArray));
	});
	
	var scrolled=0;
	
	$("#scrollDown").on("click", function(){
		scrolled=scrolled+900;

		$("#list_step1").animate({
			scrollTop: scrolled
		});
	});

	$("#scrollUp").on("click", function(){
		scrolled=scrolled-900;

		$("#list_step1").animate({
			scrollTop: scrolled
		});
	});

	$('#scrollUp').hide();

	$("#list_step1").scroll(function() {
		if ($(this).scrollTop()>0) {
			$('#scrollUp').fadeIn();

			if ($(this)[0].scrollHeight <= ($(this).height() + $(this).scrollTop())) {
				$('#scrollDown').fadeOut();
			} else {
				$('#scrollDown').fadeIn();
			}
		} else {
			$('#scrollUp').fadeOut();
		}
	});

	$("#scrollDownExt").on("click", function(){
		scrolled=scrolled+900;

		$("#list_step2").animate({
			scrollTop: scrolled
		});
	});

	$("#scrollUpExt").on("click", function(){
		scrolled=scrolled-900;

		$("#list_step2").animate({
			scrollTop: scrolled
		});
	});

	$('#scrollUpExt').hide();

	$("#list_step2").scroll(function() {
		if ($(this).scrollTop()>0) {
			$('#scrollUpExt').fadeIn();

			if ($(this)[0].scrollHeight <= ($(this).height() + $(this).scrollTop())) {
				$('#scrollDownExt').fadeOut();
			} else {
				$('#scrollDownExt').fadeIn();
			}
		} else {
			$('#scrollUpExt').fadeOut();
		}
	});

	$('#scrollLeft').hide();
	$('#scrollRight').hide();
	$('#scrollLeftExt').hide();
	$('#scrollRightExt').hide();
});






$(function(){
	// login drop down button close 
	$("#noClose").click(function(e) {
		if (e.target.nodeName !== 'DIV' ) e.stopPropagation();
	});

});



$(document).ready(function() {

	var owl = $("#owl-demo");

	owl.owlCarousel({
		items : 1, //10 items above 1000px browser width
		itemsDesktop : [1000,5], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,3], // betweem 900px and 601px
		itemsTablet: [600,2], //2 items between 600 and 0
		itemsMobile : false ,// itemsMobile disabled - inherit from itemsTablet option,
		mouseDrag : false,
		touchDrag : false,
		rewindNav:false,
		lazyLoad:true
	});



	// Custom Navigation Events
	$(".nextLogin").click(function(){
		owl.trigger('owl.next');
	})
	$(".prevLogin").click(function(){
		owl.trigger('owl.prev');
	})


});


$(document).ready(function () {

	

	new WOW().init();

});
