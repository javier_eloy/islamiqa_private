 /*
 * Updated by Julio Velasquez
 * Date: 09/06/2016
 * Time: 11:09
 */

$(document).ready(function() {

    //Configure/customize these variables.
    var showChar = 200;  //How many characters are shown by default
    var ellipsestext = " ";
    var moretext = "Read More >";
    var lesstext = "Read Less";

    $("span.more").each(function()
    {
        //This 'if' was added by Julio A. Velasquez Basabe at 20160622, in order to 
        //execute the code only to the new posts, append clicking the link: to see more questions click here.
        if (!$(this).hasClass("processed"))
        {
            var content = $(this).text($(this).html());
            content = String(content[0].innerText);

            if(content.length > showChar)
            {
                var c = content.substring(0, showChar);
                var h = content.substr(showChar, content.length);

                //This was made by Julio A. Velasquez Basabe at 20160622, in order to avoid cut off words in the middle.
                if (c.substring(showChar, showChar+1) != " ")
                {
                    var x = h.substring(0, h.indexOf(" "));
                    c = c + x;
                    h = h.replace(x, " ");
                }
                //This 'if' was added by Julio A. Velasquez Basabe at 20160622, in order to 
                //hide the rest of answer only if greater than 50 chars, else show complete answer.
                if (h.length >= 50)
                {
                    var html = c + '<span class="morecontent"><span>' + h + '</span></span><a href="" class="morelink">' + moretext + '</a>';
                    console.log("html: " + html.length);
                    $(this).html(html);
                    $(this).addClass("processed");
                }
            }
        }
    });

    //This was updated by Julio A. Velasquez Basabe at 20160622, in order to 
    //make works show more link in all new posts, append clicking the link: to see more questions click here.
    //Something works wrong...
    $("a.morelink").on("click", function()
    {
        if($(this).hasClass("less")) {
            console.log("has class less");
            $(this).removeClass("less");
            $(this).html(moretext);
        }
        else {
            console.log("has not class less");
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        var spanPrev = $(this).prev();
        spanPrev.toggle();
        spanPrev.removeClass();
        console.log(spanPrev);

        var spanChild = spanPrev.children();
        spanChild.attr('style', 'display: inline');
        console.log(spanChild);
        return false;
    });

});