<?php

require_once '../../qa-include/qa-base.php';
require_once QA_INCLUDE_DIR . 'db/selects.php';
require_once QA_INCLUDE_DIR.'qa-app-users.php';


if($_REQUEST['type'] == 'up'){
    $success = false;
    if(qa_is_logged_in() ){
        $postid = $_REQUEST['id'];
        $userid = qa_get_logged_in_userid();
        $row = votes($postid, $userid, "up");
        if (isset($row['netvote'])){
            $success = true;
        }   
    }
    $row['success'] = $success;
    echo json_encode($row);
}

elseif($_REQUEST['type'] == 'down'){
    $success = false;
    if(qa_is_logged_in() ){
        $postid = $_REQUEST['id'];
        $userid = qa_get_logged_in_userid();
        $row = votes($postid, $userid, "down");
        if (isset($row['netvote'])){
            $success = true;
        }   
    }
    $row['success'] = $success;
    echo json_encode($row);
}
elseif($_REQUEST['type'] == 'wantanswer'){
        $success = false;
        $postid = $_REQUEST['postid'];
        $userid = $_REQUEST['userid'];
        $row = qa_wantanswer_set($userid, $postid);
        echo json_encode($row);
}
function votes($postid, $userid, $type ){
    
    if ($type == "up"){
            $query = "INSERT INTO ^uservotes (postid, userid, vote, flag) VALUES (".$postid.", ".$userid.", 1, 0) ON DUPLICATE KEY UPDATE vote=1";     
            $numrows =  qa_db_affected_rows(qa_db_query_sub($query));
    }
    
    
    if ($type == "down"){
            $query = "INSERT INTO ^uservotes (postid, userid, vote, flag) VALUES (".$postid.", ".$userid.", -1, 0) ON DUPLICATE KEY UPDATE vote=-1";     
            $numrows =  qa_db_affected_rows(qa_db_query_sub($query));
        } 

        $query = "SELECT count(vote) as  upvote FROM ^uservotes WHERE  vote > 0  and postid = $postid";    
        $result = qa_db_read_one_assoc(qa_db_query_sub($query ), true);
        $upvote = $result['upvote'];

        
        $query = "SELECT count(vote) as  downvote FROM ^uservotes WHERE  vote < 0  and postid = $postid";    
        $result = qa_db_read_one_assoc(qa_db_query_sub($query ), true);
        $downvote = $result['downvote'];
        
        $netvote = $upvote - $downvote;
        $query = " UPDATE qa_posts a  SET upvotes = $upvote,  downvotes = $downvote, netvotes = $netvote WHERE a.postid = $postid ";    
        $numrows =  qa_db_affected_rows(qa_db_query_sub($query));
        
        $row['upvote'] = $upvote;
        $row['downvote'] = $downvote;
        $row['netvote'] = $netvote;
        
        return $row;
    
}


function qa_wantanswer_set($userid, $postid){
        
        $query = "SELECT COUNT(postid) as count FROM ^userwantanswer WHERE userid = #  AND postid = #";
        $result = qa_db_read_one_assoc(qa_db_query_sub($query, $userid, $postid ), true);
        $count = $result['count'];

        
        if ($count == 0){
            $query = "INSERT INTO ^userwantanswer (userid, postid) VALUES (#,#)"; 
            qa_db_query_sub($query, $userid, $postid );
        }
        $query = "SELECT COUNT(postid) as count FROM ^userwantanswer WHERE postid = #";
        $result = qa_db_read_one_assoc(qa_db_query_sub($query, $postid ), true);
        $count = $result['count'];
        $row['count'] = $count;
        return $row;
    }    