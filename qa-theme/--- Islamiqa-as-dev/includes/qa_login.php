<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 28/06/16
 * Time: 08:02 AM
 */


require_once 'qa-include/qa-base.php';



function login_popup_social_islamiqa($theme) {

    if (!qa_is_logged_in()) {
        login_and_sing_up_social($theme);
    }else{
        logged($theme);
    }

}


//require_once QA_THEME_DIR . 'Islamiqa-as-dev/includes/qa_login.php';
function script_login_social_islamica($theme){

    require_once QA_INCLUDE_DIR . 'qa-db-users.php';
    require_once QA_INCLUDE_DIR . 'app/users.php';
    require_once QA_INCLUDE_DIR . 'qa-base.php';

    $app_id = qa_opt('facebook_app_id');


    //  var_dump(QA_INCLUDE_DIR);
    $tourlxx = qa_opt('site_url');



    // $logout = qa_is_logged_in() ? 'auth.logout' : 'auth.login';
    $appId = qa_opt('facebook_app_id');
    $faceeBookurl = qa_path_absolute('facebook-login', array('to' => $tourlxx));


    // twitter config
    $twiter_appId = qa_opt('twitter_app_id');
    $tourlTwitter = qa_path_absolute('hauth.done=Twitter');

    $google_appId = qa_opt('google_app_id');
    $tourlGoogle = qa_path_absolute('hauth.done=Google');

    $linkedInappId = qa_opt('linkedin_app_id');
    $tourlLinkedIn = qa_path_absolute('hauth.done=LinkedIn');

//    hauth.done=Google


    if (!strlen($app_id)) {
        $facebookScript = 'function faceBooklogin() {} ';
    } else {
        $facebookScript = '

                     
					 window.fbAsyncInit = function() {
                        FB.init({
                          appId      : "' . $appId . '",
                          xfbml      : true,
                          version    : \'v2.5\',
                          status : true,
						 cookie : true,
						  oauth  : true
                        });
                      };

                      (function(d, s, id){
                         var js, fjs = d.getElementsByTagName(s)[0];
                         if (d.getElementById(id)) {return;}
                         js = d.createElement(s); js.id = id;
                         js.src = "//connect.facebook.net/en_US/sdk.js";
                         fjs.parentNode.insertBefore(js, fjs);
                       }(document, \'script\', \'facebook-jssdk\'));

					function faceBooklogin()
						{
                            FB.login(function(response) {
                                if (response.authResponse) {
                                 console.log("Welcome!  Fetching your information.... ");
                                 setTimeout(window.location="' . $faceeBookurl . '", 100);
//                                 setTimeout(window.location="http://localhost.com", 100);
//                                 FB.api("me", function(response) {
//                                   console.log("Good to see you, " + response.name );
//                                 });
                                } else {
                                 console.log("User cancelled login or did not fully authorize.");
                                }
                            });
                       }

				';
    }



    ///////  twitter script

    if (!strlen($twiter_appId))
    {
        $twtterScript = ' function twitterLogin() {} 
        
        ';
    }
    else{

        $twtterScript = ' function twitterLogin(){
            setTimeout(window.location="'.$tourlTwitter.'", 100);
        }
         
         ';


    }

    if (!strlen($google_appId))
    {
        $googleScript = ' function googleLogin() {} ';
    }else{
        $googleScript = ' function googleLogin(){
                setTimeout(window.location="'.$tourlGoogle.'", 100);
            }
             
             ';
    };


    if (!strlen($linkedInappId))
    {
        $linkedinScript = ' function linkedInLogin() {} ';
    }else{
        $linkedinScript = ' function linkedInLogin(){
                setTimeout(window.location="'.$tourlLinkedIn.'", 100);
            } 
            
            ';
    };



    $theme->output('<script type="text/javascript">'
        . $facebookScript
        . $twtterScript
        . $googleScript
        . $linkedinScript .
        ' </script> ');
}


function login_and_sing_up_social($theme){


    $plugionLoginUrl =  qa_path_absolute('hauth.done=Natural');
    //$plugionLoginUrl =  "http://localhost/islamiqa/?qa=login";
    $plugionForgotUrl =  qa_path_absolute('forgot');

    $plugionRegisterUrl =  qa_path_absolute('hauth.done=Register');
    $plugionStep1Url =  qa_path_absolute('step1');

    $placeHolderHandled = trim(qa_lang_html(qa_opt('allow_login_email_only') ? 'users/email_label' : 'users/email_handle_label'), ':');
    $placeHolderPassword =  trim(qa_lang_html('users/password_label'), ':') ;


    $theme->output('

                    <div class="signup_login-navbar">
                            <ul class="signup_login nav navbar-nav open">
                                           <li>
                                                <div class="dropdown" id="signUpDropdown" noclose>
            
                                                <!-- <button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" id="signUpDrop" type="button" class="dropdown-toggle" style = "padding-right: 15px"> -->
                                                        <button id="signupBtn"  aria-expanded="true" aria-haspopup="true" data-toggle-animate="dropdown-animatecss"  animated-in="fadeIn animated" animated-out="fadeOut animated" id="signUpDrop" type="button" class="dropdown-toggle" style = "padding-right: 20px">
                                                            Sign up <span class="caret"></span>
                                                        </button>
                                                        <!-- dropdown-menu-signUpDrop -->
                                                        <ul id="noClose" aria-labelledby="signUpDrop" class="dropdown-menu dropdown-menu-login"    style="width:400px">
                                                            
                                                             
                                                                  <div class="row text-center" >
                                                                       <div class="col-md-offset-2 col-md-8">
                                                                          <ul class="slimmenu-2x sing-in-menu">
                                                                             <li>
                                                                                <div class="prevLogin"> <a href="#">   Social  </a>  </div>
                                                                             </li>
                                                                             <li>
                                                                                <div class="nextLogin"> <a  href="#">E-Mail</a> </div>
                                                                             </li>
                                                                          </ul>
                                                                       </div>
                                                                  </div>
                                                                  <div id="owl-demo" >
                                                                      <div class="item">
                                                                                            <div class="sing-in-tab" >
                                                                                               <div class="row row-height" style="margin-top: 15px">
                                                                                                  <div class="col-md-12">
                                                                                                     <div class="btn btn-default btn-full-with btn-facebook menuLogin " onclick="faceBooklogin()"><i class="fa fa-facebook ileft"  aria-hidden="true"></i>  Sign up with facebook</div>
                                                                                                      
                                                                                                  </div>
                                                                                               </div>
                                                                                               <div class="row row-height">
                                                                                                  <div class="col-md-12">
                                                                                                     <div class="btn btn-default btn-full-with btn-twitter menuLogin" onclick="twitterLogin()" ><i class="fa fa-twitter ileft" aria-hidden="true"></i> Sign up with twitter</div>
                                                                                                  </div>
                                                                                               </div>
                                                                                               <div class="row row-height">
                                                                                                  <div class="col-md-12 ">
                                                                                                     <div class="btn btn-default btn-full-with btn-google menuLogin" onclick="googleLogin()"><i class="fa fa-google-plus ileft" aria-hidden="true" ></i>  Sign up with google</div>
                                                                                                  </div>
                                                                                               </div>
                                                                                               <div class="row row-height">
                                                                                                  <div class="col-md-12">
                                                                                                     <div class="btn btn-default btn-full-with btn-linkedin menuLogin"  onclick="linkedInLogin()" style="margin-bottom: 20px"><i class="fa fa-linkedin ileft" aria-hidden="true" ></i>   Sign up with linkedin</div>
                                                                                                  </div>
                                                                                               </div> 
                                                                                               <div>
                                                                                                  <p style="margin-bottom: 0px">Alternatively, <strong>Sign Up With Email</strong>. By clicking Sign up, you agree to our terms of service and privacy policy. We will send you account related emails occasionally - we will never spam you, honest!</p>
                                                                                               </div>
                                                                                            </div>
                                                                      
                                                                      </div>
                                                                      <div class="item">
                                                                                       
                                                                                             <div class="sing-in-tab">
                                                                                                           <div class="row">
                                                                                                                      <div class="col-md-12">
                                                                                                                      
                                                                                                                                  <form id="frmSignup" name="frmSignup" class="form-horizontal" method="POST" action="'. $plugionRegisterUrl.'">
                                                                                                                                  
                                                                                                                                            <input type="hidden" name="toRegister" value="' .$plugionStep1Url.'">
                                                                                                                                            <input type="hidden" name="toInvalid" value="">
                                                                                                                                            <div class="form-group ">
                                                                                                                                               <div class="col-sm-12">
                                                                                                                                                  <input type="text" class="form-control reduce"  id="inputName" placeholder="Name" name="name">
                                                                                                                                               </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="form-group ">
                                                                                                                                               <div class="col-sm-12">
                                                                                                                                                  <input type="email" class="form-control reduce" id="inputEmail" placeholder="E-mail" name="email">
                                                                                                                                               </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="form-group ">
                                                                                                                                               <div class="col-sm-12">
                                                                                                                                                  <input type="password" class="form-control reduce" id="inputPassword" placeholder="Password" name="password">
                                                                                                                                               </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="form-group">
                                                                                                                                               <div class="col-sm-12">
                                                                                                                                                  <input type="password" class="form-control reduce" id="inputConfirmPassword" placeholder="Confirm Password" name="rpass">
                                                                                                                                               </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="form-group ">
                                                                                                                                               <div class="col-sm-6">
                                                                                                                                                  <input type="text" class="form-control reduce" id="inputCity" placeholder="City" name="city">
                                                                                                                                               </div>
                                                                                                                                               <div class="col-sm-6 form-control-login">
                                                                                                                                                  <input type="text" class="form-control reduce" id="inputCountry" placeholder="Country" name="country">
                                                                                                                                               </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="form-group reduce">
                                                                                                                                               <div class="col-sm-12">
                                                                                                                                                  <button type="submit" class="btn btn-primary btn-block">Register</button>                                                                          
                                                                                                                                               </div>
                                                                                                                                            </div>
                                                                                                                                 </form>
                                                                                                                      </div>
                                                                                                           </div>
                                                                                                           <div>
                                                                                                              <p style="margin-bottom: 0px" >By clicking Sign up, you agree to our terms of service and privacy 
                                                                                                                 policy. We will send you account related emails occasionally 
                                                                                                                 - we will never spam you, honest!
                                                                                                              </p>
                                                                                                           </div>
                                                                                             </div>
                                                                       </div>
                                                              <!-- end dropdown-menu-signUpDrop -->
                                                        </ul>
                                                </div>                                    
                                           </li>
                                           <li>
                                               <div class="dropdown" noclose>
                                                            <button aria-expanded="true" aria-haspopup="true"  data-toggle-animate="dropdown-animatecss"  animated-in="fadeIn animated" animated-out="fadeOut animated" id="loginDrop" type="button" class="dropdown-toggle"><!-- Modifications ICON NEW Attention CESAR -->
                                                                <i class="fa fa-user" aria-hidden="true"></i> Login <span class="caret"></span>
                                                            </button>
                                                            <ul aria-labelledby="loginDrop" class="dropdown-menu dropdown-menu-login">
                                                                <li>  <div class="btn btn-default btn-full-with btn-facebook menuLogin" onclick="faceBooklogin()"><i class="fa fa-facebook" aria-hidden="true"></i>  Login with facebook</div> </li>
                                                                <li>  <div class="btn btn-default btn-full-with btn-twitter menuLogin" onclick="twitterLogin()"><i class="fa fa-twitter" aria-hidden="true"></i> Login with twitter</div> </li>
                                                                <li>  <div class="btn btn-default btn-full-with btn-google menuLogin" onclick="googleLogin()"><i class="fa fa-google-plus" aria-hidden="true"></i>  Login with google</div> </li>
                                                                <li>  <div class="btn btn-default btn-full-with btn-linkedin menuLogin" onclick="linkedInLogin()" ><i class="fa fa-linkedin" aria-hidden="true"></i>   Login with linkedin</div> </li>                                                
                                                                <li class="seprator" role="separator"><span>OR</span></li>
                                                                
                                                                        <div id="login-form-popup">
                                                                             <form id="loginForm" method="post" class="form-horizontal" action="' . $plugionLoginUrl . '">
                                                                                <input type="hidden" name="code" value="' . qa_html(qa_get_form_security_code('login')) . '"/>
                                                                                <input type="hidden" name="toInvalid" value="' . $plugionForgotUrl . '">
                                                                                
                                                                                <div class="form-group" style="margin-bottom:2px">
                                                                                    
                                                                                    <div class="col-md-12">
                                                                                        <input type="text" class="form-control" name="emailhandle" placeholder = "'.$placeHolderHandled.'" />
                                                                                    </div>
                                                                                </div>
                                                                            
                                                                                <div class="form-group" style="margin-bottom:2px">
                                                                                    
                                                                                    <div class="col-md-12">
                                                                                        <input type="password" class="form-control" name="password"  placeholder="'.$placeHolderPassword.'" />
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                 <div class="form-group" style="margin-bottom:2px">
                                                                                    <div class="col-md-12">
                                                                                        <div>
                                                                                            <input type="checkbox" name="remember"  value="1"/>
                                                                                             <label class="control-label">Keep me logged in</label>
                                                                                        </div>
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            
                                                                                <div class="form-group" style="margin-bottom:2px">
                                                                                    <div class="col-md-12">
                                                                                       <div class="pull-right">                                                                               
                                                                                            <button type="submit" class="btn btn-primary">Sign in</button>
                                                                                       </div>                                                                                
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                            </div>
                                                            </ul>
                                               </div>                       
                                           
                                           </li>                    
                                                            
                            </ul>
                    </div>                    '

    );


}


function logged($theme) {

    $theme->output('<div class="signup_login-navbar"><ul class="signup_login nav navbar-nav open">');

        /*admin panel link*/
        $level = qa_get_logged_in_level();
        if (qa_is_logged_in() && $level >= QA_USER_LEVEL_ADMIN) {
            $nav = $theme->content['navigation']["user"];
            $nav = array_reverse($nav);
            $nav["admin"]["url"] = "./?qa=admin";
            $nav["admin"]["label"] = "Admin ".'<i class="fa fa-envelope" aria-hidden="true"></i>'; ;?><?php
            $nav["admin"]["selected"] = "null";
            $nav = array_reverse($nav);
            $theme->content['navigation']["user"] = $nav;
            /*end admin panel link*/
        }

        $theme->nav_list_islamiqa("user");

    $theme->output('</ul></div>');


       
}
