﻿<?php
/*
 * Created by Julio Velasquez
 * Date: 04/05/2016
 * Time: 00:00
 */
/*
 * Updated by Julio A. Velasquez Basabe
 * Date: 11/06/2016
 * Time: 00:00
 */

require_once '../../qa-include/qa-base.php';
require_once QA_INCLUDE_DIR . 'qa-db.php';
require_once QA_THEME_DIR . 'Islamiqa-as-dev/myown_functions.php';

const posts_per_slide = 4;
const listview_questions = 10;

$most_html = '';
$recent_html = '';
$allque_html = '';
$unans_html = '';

if (!isset($_SESSION['most_sectionquery'])) {
    @session_start();
}

$rooturl = $_SESSION['rooturl'];

$most_sectionquery = $_SESSION['most_sectionquery'];
$most_queryans = $_SESSION['most_queryans'];
$most_countans = $_SESSION['most_countans'];

$recent_sectionquery = $_SESSION['recent_sectionquery'];
$recent_queryans = $_SESSION['recent_queryans'];
$recent_countans = $_SESSION['recent_countans'];

$allque_sectionquery = $_SESSION['allque_sectionquery'];
$allque_queryans = $_SESSION['allque_queryans'];
$allque_countans = $_SESSION['allque_countans'];

$unans_sectionquery = $_SESSION['unans_sectionquery'];
$unans_queryans = $_SESSION['unans_queryans'];
$unans_countans = $_SESSION['unans_countans'];

$most_index = $_SESSION['most_index'];
$most_countque = $_SESSION['most_countque'];
$most_slide_index = $_SESSION['most_slide_index'];
$most_total_slides = $_SESSION['most_total_slides'];
$most_firsttime = $_SESSION['most_firsttime'];
$most_listview_firsttime = $_SESSION['most_listview_firsttime'];

$recent_index = $_SESSION['recent_index'];
$recent_countque = $_SESSION['recent_countque'];
$recent_slide_index = $_SESSION['recent_slide_index'];
$recent_total_slides = $_SESSION['recent_total_slides'];
$recent_firsttime = $_SESSION['recent_firsttime'];
$recent_listview_firsttime = $_SESSION['recent_listview_firsttime'];

$allque_index = $_SESSION['allque_index'];
$allque_countque = $_SESSION['allque_countque'];
$allque_slide_index = $_SESSION['allque_slide_index'];
$allque_total_slides = $_SESSION['allque_total_slides'];
$allque_firsttime = $_SESSION['allque_firsttime'];
$allque_listview_firsttime = $_SESSION['allque_listview_firsttime'];

$unans_index = $_SESSION['unans_index'];
$unans_countque = $_SESSION['unans_countque'];
$unans_slide_index = $_SESSION['unans_slide_index'];
$unans_total_slides = $_SESSION['unans_total_slides'];
$unans_firsttime = $_SESSION['unans_firsttime'];
$unans_listview_firsttime = $_SESSION['unans_listview_firsttime'];

$listview_mostindex = $_SESSION['listview_mostindex'];
$listview_recentindex = $_SESSION['listview_recentindex'];
$listview_allqueindex = $_SESSION['listview_allqueindex'];
$listview_unansindex = $_SESSION['listview_unansindex'];


//most part ajax calling
switch ($_REQUEST['view'])
{
    case 'grid':

if($_REQUEST['type'] == 'most')
{
    $section_table = 'mostpopular';
    $most_html = '
        <div id="mostpopular_grid">
            <div class="container popular_answer_wrapper">
            <!--<div id="mostpopular_caresol">  -->
                    <a onclick="arrowleft_mostpopular();" class="left-arrow" id="left"><i class="fa fa-angle-left"></i></a>
                    <a onclick="arrowright_mostpopular();" class="right-arrow" id="right"><i class="fa fa-angle-right"></i></a>
                    <div class="row" id="most_first_five">';

    if($_REQUEST['name'] == 'mostpopular')
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);
        $most_total_slides = $most_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if (is_float($most_total_slides)) {
            $most_total_slides = intval($most_total_slides) + 1;
        }
        for($i=0; $i < $most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));

            if ($most_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }
        if($most_firsttime == TRUE) {
            $most_firsttime = FALSE;
        }
        $most_slide_index = 1;
        $most_index = 0;
    }

    elseif($_REQUEST['name'] == 'mostweek')
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 WEEK) ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);
        $most_total_slides = $most_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if (is_float($most_total_slides)) {
            $most_total_slides = intval($most_total_slides) + 1;
        }
        for($i=0; $i<$most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));

            if ($most_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }        
        if($most_firsttime == TRUE) {
            $most_firsttime = FALSE;
        }
        $most_slide_index = 1;
        $most_index = 0;
    }

    elseif($_REQUEST['name'] == 'mostmonth')
    {    
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 MONTH) ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);
        $most_total_slides = $most_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($most_total_slides)) {
            $most_total_slides = intval($most_total_slides) + 1;
        }
        for($i=0; $i<$most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));

            if ($most_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }
        if($most_firsttime == TRUE) {
            $most_firsttime = FALSE;
        }
        $most_slide_index = 1;
        $most_index = 0;
    }

    elseif($_REQUEST['name'] == 'mostyear')
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 YEAR) ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);
        $most_total_slides = $most_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($most_total_slides)) {
            $most_total_slides = intval($most_total_slides) + 1;
        }
        for($i=0; $i<$most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));

            if ($most_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }        
        if($most_firsttime == TRUE) {
            $most_firsttime = FALSE;
        }
        $most_slide_index = 1;
        $most_index = 0;
    }

    elseif($_REQUEST['name'] == 'mostcategory')
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND ^posts.categoryid='" . $_REQUEST['catname'] . "' ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);
        $most_total_slides = $most_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($most_total_slides)) {
            $most_total_slides = intval($most_total_slides) + 1;
        }
        for($i=0; $i<$most_countque; $i++)
        { 
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));

            if ($most_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            } 
        }
        if($most_firsttime == TRUE) {
            $most_firsttime = FALSE;
        }
        $most_slide_index = 1;
        $most_index = 0;
    }

    elseif($_REQUEST['name'] == 'arrowleft' && $most_firsttime == TRUE)
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);
        $most_total_slides = $most_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($most_total_slides)) {
            $most_total_slides = intval($most_total_slides) + 1;
        }
        for($i=0; $i<$most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));

            if ($most_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }
        $most_slide_index = 1;
        $most_index = 0;
        
        if($most_firsttime == TRUE) {
            $most_firsttime = FALSE;
        }
    }

    elseif($_REQUEST['name'] == 'arrowleft' && $most_firsttime == FALSE)
    {
        $most_slide_index -= 1;

        if($most_slide_index == 1) {
            $most_index = 0;
        }
        elseif ($most_slide_index <= 0) {
            $most_index = -1;
        }
        else {
            $most_index = $most_slide_index * posts_per_slide - posts_per_slide;
        }
    }

    elseif($_REQUEST['name'] == 'arrowright' && $most_firsttime == TRUE)
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);
        $most_total_slides = $most_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($most_total_slides)) {
            $most_total_slides = intval($most_total_slides) + 1;
        }
        for($i=0; $i<$most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));

            if ($most_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }
        if($most_firsttime == TRUE) {
            $most_firsttime = FALSE;
        }
        $most_slide_index = 2;
        $most_index = 4;
    }

    elseif($_REQUEST['name'] == 'arrowright' && $most_firsttime == FALSE)
    {
        if($most_index <= $most_countque) {
            $most_index += 1;
            $most_slide_index +=1;
        }
    }

    if($most_index < $most_countque && $most_slide_index != 0)
    {
        $most_html .= '
                <div class="col-md-6">
                    <div class="row">
                        <div class="gallery_four_thumb">
                            <div class="clearfix">
                                <div class="thumb_half">
                                    <div class="thumb_hover half">
                                        <div class="hover_overlay">';

        if($most_index < $most_countque)
        {
            $most_html .= '
                <div class="inner">
                    <div class="top_line">
                        <span class="time left">' . calculate_weeks_ago($most_sectionquery[$most_index]["created"]) . ' w ago</span>
                        <span class="topic right">' . $most_sectionquery[$most_index]["cattitle"] . '</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h6">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $most_sectionquery[$most_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[$most_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $most_html .= '
                            <a href="' . $ref . '">' . $most_sectionquery[$most_index]["title"] . '</a>
                        </div>
                        <div class="ans">';
/*                                                                                                       
                            $most_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $most_sectionquery[0]['postid'] . "' "));
                            $most_countans = sizeof($most_queryans);
*/                            
                            if($most_queryans[$most_index] != NULL) {
                                $most_html .= 'Ans: '.substr($most_queryans[$most_index][0]['content'],0,100);
                            }
                            else {
                                $most_html .= 'No Answer';
                            }

                            $most_html .= '
                            </div>
                            <div class="toolbar">
                                <div class="tools left">
                                <ul>
                                    <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                </ul>
                                <ul class="list-unstyled pull-left">
                                    <li><a onClick="addVote('.$most_sectionquery[$most_index]['postid'].');">
                                        <i class="fa fa-angle-double-up"></i>
                                        <span id=span-'.$most_sectionquery[$most_index]['postid'].'> '.$most_sectionquery[$most_index]['upvotes'].'</span>
                                        </a>
                                    </li>
                                    <li><a onClick="downVote('.$most_sectionquery[$most_index]['postid'].');">
                                        <i class="fa fa-angle-double-down"></i>
                                        <span id=span-'.$most_sectionquery[$most_index]['postid'].'-downvote> '.$most_sectionquery[$most_index]['downvotes'].'</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="share right">
                                <ul>';
                                                        
                                if ($most_countans[$most_index] > 1) {
                                    $ans = " answers";
                                }
                                else {
                                    $ans = " answer";
                                }
                                $most_html .= '
                                    <li>' . $most_countans[$most_index] . $ans . '</li>
                                    <li>
                                        <div class="tooltip-custom">
                                            <i class="fa fa-ellipsis-h"></i>
                                            <div class="tooltip-inner tooltip-inner2">
                                                <a href="#">Invite authoritative user to answer</a></br>
                                                <a href="#">Add to Reading List</a></br>
                                                <a href="#">Show Related Questions</a></br>
                                                <a href="#">Create Summary Answer</a></br>
                                                <a href="#">Mark Favorite</a></br>
                                                <a href="#">Report</a></br>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $most_sectionquery[$most_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $most_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $most_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }                        
                        $most_html .= '
                            </div>
                         </div>';

                        $most_index += 1;
        }


        if ($most_index < $most_countque)
        {
            $most_html .= '
    <div class="thumb_half">
        <div class="thumb_hover half">
            <div class="hover_overlay">
                <div class="inner">
                    <div class="top_line">
                        <span class="time left">' . calculate_weeks_ago($most_sectionquery[$most_index]["created"]) . ' w ago</span>
                        <span class="topic right">'.$most_sectionquery[$most_index]["cattitle"].'</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h6">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $most_sectionquery[$most_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[$most_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $most_html .= '
                            <a href="' . $ref . '">' . $most_sectionquery[$most_index]["title"] . '</a>
                        </div>
                        <div class="ans">';
/*                                                                                                       
                            $most_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $most_sectionquery[1]['postid'] . "' "));
                            $most_countans = sizeof($most_queryans);
*/
                            if($most_queryans[$most_index] != NULL) {
                                $most_html .= 'Ans: '.substr($most_queryans[$most_index][0]['content'],0,100);
                            }
                            else {
                                $most_html .= 'No Answer';
                            }
                            
                            $most_html .= '
                            </div>
                            <div class="toolbar">
                                <div class="tools left">
                                <ul>
                                    <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                </ul>
                                <ul class="list-unstyled pull-left">
                                    <li>
                                        <a onClick="addVote('.$most_sectionquery[$most_index]['postid'].');">
                                        <i class="fa fa-angle-double-up"></i><span id=span-'.$most_sectionquery[$most_index]['postid'].'> '.$most_sectionquery[$most_index]['upvotes'].'</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a onClick="downVote('.$most_sectionquery[$most_index]['postid'].');">
                                        <i class="fa fa-angle-double-down"></i><span id=span-'.$most_sectionquery[$most_index]['postid'].'-downvote> '.$most_sectionquery[$most_index]['downvotes'].'</span>
                                        </a>
                                    </li>
                                </ul>
                                </div>
                                <div class="share right">
                                    <ul>';
                                                        
                                if ($most_countans[$most_index] > 1) {
                                    $ans = " answers";
                                }
                                else {
                                    $ans = " answer";
                                }
                                $most_html .= '
                                    <li>' . $most_countans[$most_index] . $ans . '</li>
                                        <li>
                                            <div class="tooltip-custom">
                                                <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner tooltip-inner2">
                                                    <a href="#">Invite authoritative user to answer</a></br>
                                                    <a href="#">Add to Reading List</a></br>
                                                    <a href="#">Show Related Questions</a></br>
                                                    <a href="#">Create Summary Answer</a></br>
                                                    <a href="#">Mark Favorite</a></br>
                                                    <a href="#">Report</a></br>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $most_sectionquery[$most_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $most_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $most_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        $most_html .= '
                            </div>
                        </div>
                    </div>';

                    $most_index += 1;     
        }    

                        
        if ($most_index < $most_countque)
        {
            $most_html .= '
<div class="thumb_full">
    <div class="thumb_hover full">
        <div class="hover_overlay">
            <div class="inner">
                <div class="top_line">
                    <span class="time left">' . calculate_weeks_ago($most_sectionquery[$most_index]["created"]) . ' w ago</span>
                    <span class="topic right">' . $most_sectionquery[$most_index]["cattitle"] . '</span>
                    <div class="clearfix"></div>
                </div>
                <div class="bottom_line bounceInUp">
                    <div class="h5">';

                    //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                    $ref = str_replace($rooturl, "?qa=", $rooturl) . $most_sectionquery[$most_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[$most_index]["title"]);
                    //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[1]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[1]["title"]);
                    $most_html .= '                                      
                        <a href="' . $ref . '">' . $most_sectionquery[$most_index]["title"] . '</a>
                    </div>
                    <div class="ans" >';
/*                    
                    $most_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $most_sectionquery[2]['postid'] . "' "));
                    $most_countans = sizeof($most_queryans);
*/
                    if($most_queryans[$most_index] != NULL) {
                        $most_html .= 'Ans: '.substr($most_queryans[$most_index][0]['content'],0,300);
                    } 
                    else {
                            $most_html .= 'No Answer';
                    }

            $most_html .='
                </div>
                <div class="toolbar">
                    <div class="tools left">
                    <ul>
                        <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                            <ul class="list-unstyled pull-left">
                                <li><a onClick="addVote('.$most_sectionquery[$most_index]['postid'].');">
                                    <i class="fa fa-angle-double-up"></i>
                                    <span id=span-'.$most_sectionquery[$most_index]['postid'].'> '.$most_sectionquery[$most_index]['upvotes'].'</span>
                                    </a>
                                </li>
                                <li><a onClick="downVote('.$most_sectionquery[$most_index]['postid'].');">
                                    <i class="fa fa-angle-double-down"></i>
                                    <span id=span-'.$most_sectionquery[$most_index]['postid'].'-downvote> '.$most_sectionquery[$most_index]['downvotes'].'</span>
                                    </a>
                                </li>
                            </ul>
                        <li><a href="javascript:void(0);">3k views</a></li>
                        <li><a href="javascript:void(0);">235 comments</a></li>
                        <li>                                                            
                                <ul>
                                    <li>
                                    <div class="tooltip-social">
                                        <a href="#">Share</a>
                                        <div class="tooltip-inner tooltip-inner3">
                                            <div class="social_icon facebook_icon">
                                                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon twitter_icon">
                                                <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon google_icon">
                                                <a href="#"><i class="fa fa-google fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon instagram_icon">
                                                <a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>                                                       
                            </li>
                    </ul>
                </div>
                <div class="share right">
                    <ul>';
                                                        
                    if ($most_countans[$most_index] > 1) {
                        $ans = " answers";
                    }
                    else {
                        $ans = " answer";
                    }
                    $most_html .= '
                        <li>' . $most_countans[$most_index] . $ans . '</li>
                        <li>
                        <div class="tooltip-custom">
                            <i class="fa fa-ellipsis-h"></i>
                            <div class="tooltip-inner tooltip-inner2">
                                <a href="#">Invite authoritative user to answer</a><br>
                                <a href="#">Add to Reading List</a><br>
                                <a href="#">Show Related Questions</a><br>
                                <a href="#">Create Summary Answer</a><br>
                                <a href="#">Mark Favorite</a><br>
                                <a href="#">Report</a>
                            </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>';

                        //The next line is to get the postid of the question
                        $postid_num = $most_sectionquery[$most_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $most_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $most_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        //Note by Julio Velasquez: I add this </div> because it missing to put the second column (full_single_right) at right
                        //</div>

                        $most_html .= '      
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

            $most_index += 1;  
        }
                             
        if ($most_index < $most_countque)
        {
            $most_html .= '
<div class="col-md-6">
    <div class="row">
        <div class="thumb_full_single_right">
            <div class="thumb_hover full_single">
                <div class="hover_overlay">
                    <div class="inner">
                        <div class="top_line">
                            <span class="time left">' . calculate_weeks_ago($most_sectionquery[$most_index]["created"]) . ' w ago</span>
                            <span class="topic right">'.$most_sectionquery[$most_index]["cattitle"].'</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h5">';

                    //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                    $ref = str_replace($rooturl, "?qa=", $rooturl) . $most_sectionquery[$most_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[$most_index]["title"]);
                    //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[1]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[1]["title"]);
                    $most_html .= '                                      
                        <a href="' . $ref . '">' . $most_sectionquery[$most_index]["title"] . '</a>
                    </div>
                    <div class="ans">';
/*               
                $most_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $most_sectionquery[1]['postid'] . "' "));
                $most_countans = sizeof($most_queryans);
*/                
                    if($most_queryans[$most_index] != NULL) {
                        $most_html .='Ans: '.substr($most_queryans[$most_index][0]['content'],0,300);
                    }
                    else {
                        $most_html .='No Answer';
                    }

                    $most_html .='
                    </div>
                    <div class="toolbar">
                        <div class="tools left">
                        <ul>
                            <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                            <ul class="list-unstyled pull-left">
                                <li><a onClick="addVote('.$most_sectionquery[$most_index]['postid'].');">
                                    <i class="fa fa-angle-double-up"></i>
                                    <span id=span-'.$most_sectionquery[$most_index]['postid'].'> '.$most_sectionquery[$most_index]['upvotes'].'</span>
                                    </a>
                                </li>
                                <li><a onClick="downVote('.$most_sectionquery[$most_index]['postid'].');">
                                    <i class="fa fa-angle-double-down"></i>
                                    <span id=span-'.$most_sectionquery[$most_index]['postid'].'-downvote> '.$most_sectionquery[$most_index]['downvotes'].'</span>
                                    </a>
                                </li>
                            </ul>
                            <li><a href="javascript:void(0);">3k views</a></li>
                            <li><a href="javascript:void(0);">235 comments</a></li>
                            <li>                                                            
                                <ul>
                                    <li>
                                    <div class="tooltip-social">
                                        <a href="#">Share</a>
                                        <div class="tooltip-inner tooltip-inner3">
                                            <div class="social_icon facebook_icon">
                                                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon twitter_icon">
                                                <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon google_icon">
                                                <a href="#"><i class="fa fa-google fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon instagram_icon">
                                                <a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>                                                       
                            </li>
                        </ul>
                    </div>
                    <div class="share right">
                        <ul>';
                                                        
                        if ($most_countans[$most_index] > 1) {
                            $ans = " answers";
                        }
                        else {
                            $ans = " answer";
                        }
                        $most_html .= '
                            <li>' . $most_countans[$most_index] . $ans . '</li>
                            <li>
                            <div class="tooltip-custom">
                                <i class="fa fa-ellipsis-h"></i>
                            <div class="tooltip-inner tooltip-inner2">
                                <a href="#">Invite authoritative user to answer</a><br>
                                <a href="#">Add to Reading List</a><br>
                                <a href="#">Show Related Questions</a><br>
                                <a href="#">Create Summary Answer</a><br>
                                <a href="#">Mark Favorite</a><br>
                                <a href="#">Report</a>
                            </div>
                            </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $most_sectionquery[$most_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $most_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $most_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        $most_html .= '
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

//            $most_index += 1;  
        }

        $most_html .= '
        </div>
    </div>';

    }
    else {
        $most_html .= '<div class="noqueans">No questions and answers available</div>';
    }

    $most_html .= '</div></div></div>';

    $most_html .= '<input type="hidden" id="most_slide_index" value="' . $most_slide_index . '">
                 <input type="hidden" id="most_total_slides" value="' . $most_total_slides . '">';    

    echo $most_html;
}

//recently part
elseif($_REQUEST['type'] == 'recent')
{
    $section_table = 'recently';
    $recent_html .='
        <div id="recently_grid">  
            <div class="container popular_answer_wrapper">
                <a onclick="arrowleft_recently();" class="left-arrow" id="left-recently"><i class="fa fa-angle-left"></i></a>
                <a onclick="arrowright_recently();" class="right-arrow" id="right-recently"><i class="fa fa-angle-right"></i></a>
                <div class="row" id="recently_first_five">';

    if($_REQUEST['name'] == 'recently')
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);
        $recent_total_slides = $recent_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($recent_total_slides)) {
            $recent_total_slides = intval($recent_total_slides) + 1;
        }
        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($recent_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        if($recent_firsttime == TRUE) {
            $recent_firsttime = FALSE;
        }
        $recent_slide_index = 1;
        $recent_index = 0;
    }
    
    elseif($_REQUEST['name'] == 'recentlyweek')
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ),INTERVAL 1 WEEK) ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);
        $recent_total_slides = $recent_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($recent_total_slides)) {
            $recent_total_slides = intval($recent_total_slides) + 1;
        }
        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($recent_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        if($recent_firsttime == TRUE) {
            $recent_firsttime = FALSE;
        }
        $recent_slide_index = 1;
        $recent_index = 0;
    }

    elseif($_REQUEST['name'] == 'recentlymonth')
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ),INTERVAL 1 MONTH) ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);
        $recent_total_slides = $recent_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($recent_total_slides)) {
            $recent_total_slides = intval($recent_total_slides) + 1;
        }
        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($recent_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        if($recent_firsttime == TRUE) {
            $recent_firsttime = FALSE;
        }
        $recent_slide_index = 1;
        $recent_index = 0;
    }

    elseif($_REQUEST['name'] == 'recentlyyear')
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ),INTERVAL 1 YEAR) ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);
        $recent_total_slides = $recent_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($recent_total_slides)) {
            $recent_total_slides = intval($recent_total_slides) + 1;
        }
        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($recent_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        if($recent_firsttime == TRUE) {
            $recent_firsttime = FALSE;
        }
        $recent_slide_index = 1;
        $recent_index = 0;
    }

    elseif($_REQUEST['name'] == 'recentlycategory')
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND ^posts.categoryid='" . $_REQUEST['catname'] . "' ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);
        $recent_total_slides = $recent_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($recent_total_slides)) {
            $recent_total_slides = intval($recent_total_slides) + 1;
        }
        for($i=0; $i<$recent_countque; $i++)
        { 
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($recent_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        if($recent_firsttime == TRUE) {
            $recent_firsttime = FALSE;
        }
        $recent_slide_index = 1;
        $recent_index = 0;
    }

    elseif($_REQUEST['name'] == 'arrowleft' && $recent_firsttime == TRUE)
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);
        $recent_total_slides = $recent_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($recent_total_slides)) {
            $recent_total_slides = intval($recent_total_slides) + 1;
        }
        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($recent_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        $recent_slide_index = 1;
        $recent_index = 0;
        
        if($recent_firsttime == TRUE) {
            $recent_firsttime = FALSE;
        }
    }

    elseif($_REQUEST['name'] == 'arrowleft' && $recent_firsttime == FALSE)
    {
        $recent_slide_index -= 1;

        if($recent_slide_index == 1) {
            $recent_index = 0;
        }
        elseif ($recent_slide_index <= 0) {
            $recent_index = -1;
        }
        else {
            $recent_index = $recent_slide_index * posts_per_slide - posts_per_slide;
        }
    }

    elseif($_REQUEST['name'] == 'arrowright' && $recent_firsttime == TRUE)
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);
        $recent_total_slides = $recent_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($recent_total_slides)) {
            $recent_total_slides = intval($recent_total_slides) + 1;
        }
        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($recent_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        if($recent_firsttime == TRUE) {
            $recent_firsttime = FALSE;
        }
        $recent_slide_index = 2;
        $recent_index = 4;
    }

    elseif($_REQUEST['name'] == 'arrowright' && $recent_firsttime == FALSE) {

        if($recent_index <= $recent_countque) {
            $recent_index += 1;
            $recent_slide_index +=1;
        }
    }


    if($recent_index < $recent_countque && $recent_slide_index != 0)
    {
        $recent_html .= '
                <div class="col-md-6">
                    <div class="row">
                        <div class="gallery_four_thumb">
                            <div class="clearfix">
                                <div class="thumb_half">
                                    <div class="thumb_hover half">
                                        <div class="hover_overlay">';

        if ($recent_index < $recent_countque)
        {
            $recent_html .= '
                <div class="inner">
                    <div class="top_line">
                        <span class="time left">' . calculate_weeks_ago($recent_sectionquery[$recent_index]["created"]) . ' w ago</span>
                        <span class="topic right">' . $recent_sectionquery[$recent_index]["cattitle"] . '</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h6">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $recent_sectionquery[$recent_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $recent_sectionquery[$recent_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $recent_html .= '
                            <a href="' . $ref . '">' . $recent_sectionquery[$recent_index]["title"] . '</a>
                        </div>
                            <div class="ans">';
/*                                                                                                       
                            $recent_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $recent_sectionquery[0]['postid'] . "' "));
                            $recent_countans = sizeof($recent_queryans);
*/                            
                            if($recent_queryans[$recent_index] != NULL) {
                                $recent_html .= 'Ans: ' . substr($recent_queryans[$recent_index][0]['content'],0,100);
                            }
                            else {
                                $recent_html .= 'No Answer';
                            }

                            $recent_html .= '
                            </div>
                            <div class="toolbar">
                                <div class="tools left">
                                    <ul>
                                        <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                    </ul>
                                    <ul class="list-unstyled pull-left">
                                        <li>
                                            <a onClick="addVote('.$recent_sectionquery[$recent_index]['postid'].');">
                                                <i class="fa fa-angle-double-up"></i>
                                                <span id=span-'.$recent_sectionquery[$recent_index]['postid'].'> '.$recent_sectionquery[$recent_index]['upvotes'].'</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a onClick="downVote('.$recent_sectionquery[$recent_index]['postid'].');">
                                                <i class="fa fa-angle-double-down"></i>
                                                <span id=span-'.$recent_sectionquery[$recent_index]['postid'].'-downvote> '.$recent_sectionquery[$recent_index]['downvotes'].'</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="share right">
                                    <ul>';
                                                        
                                if ($recent_countans[$recent_index] > 1) {
                                    $ans = " answers";
                                }
                                else {
                                    $ans = " answer";
                                }
                                $recent_html .= '
                                    <li>' . $recent_countans[$recent_index] . $ans . '</li>
                                        <li>
                                            <div class="tooltip-custom">
                                                <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner tooltip-inner2">
                                                    <a href="#">Invite authoritative user to answer</a></br>
                                                    <a href="#">Add to Reading List</a></br>
                                                    <a href="#">Show Related Questions</a></br>
                                                    <a href="#">Create Summary Answer</a></br>
                                                    <a href="#">Mark Favorite</a></br>
                                                    <a href="#">Report</a></br>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                 <div class="clearfix"></div>
                            </div>
                        </div>
                     </div>
                 </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $recent_sectionquery[$recent_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $recent_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $recent_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }      
                        
                        $recent_html .= '
                            </div>
                         </div>';

                        $recent_index += 1;
        }

        if ($recent_index < $recent_countque)
        {
            $recent_html .= '
    <div class="thumb_half">
        <div class="thumb_hover half">
            <div class="hover_overlay">
                <div class="inner">
                    <div class="top_line">
                        <span class="time left">' . calculate_weeks_ago($recent_sectionquery[$recent_index]["created"]) . ' w ago</span>
                        <span class="topic right">'.$recent_sectionquery[$recent_index]["cattitle"].'</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h6">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $recent_sectionquery[$recent_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $recent_sectionquery[$recent_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $recent_html .= '
                            <a href="' . $ref . '">' . $recent_sectionquery[$recent_index]["title"] . '</a>
                        </div>
                        <div class="ans">';
/*                                                                                                       
                            $recent_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $recent_sectionquery[1]['postid'] . "' "));
                            $recent_countans = sizeof($recent_queryans);
*/
                            if($recent_queryans[$recent_index] != NULL) {
                                $recent_html .= 'Ans: '.substr($recent_queryans[$recent_index][0]['content'],0,100);
                            }
                            else {
                                $recent_html .='No Answer';
                            }
                            
                            $recent_html .='
                            </div>
                            <div class="toolbar">
                                <div class="tools left">
                                    <ul>
                                        <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                    </ul>
                                    <ul class="list-unstyled pull-left">
                                        <li><a onClick="addVote('.$recent_sectionquery[$recent_index]['postid'].');">
                                            <i class="fa fa-angle-double-up"></i><span id=span-'.$recent_sectionquery[$recent_index]['postid'].'> '.$recent_sectionquery[$recent_index]['upvotes'].'</span>
                                            </a>
                                        </li>
                                        <li><a onClick="downVote('.$recent_sectionquery[$recent_index]['postid'].');">
                                            <i class="fa fa-angle-double-down"></i><span id=span-'.$recent_sectionquery[$recent_index]['postid'].'-downvote> '.$recent_sectionquery[$recent_index]['downvotes'].'</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="share right">
                                    <ul>';
                                                        
                                if ($recent_countans[$recent_index] > 1) {
                                    $ans = " answers";
                                }
                                else {
                                    $ans = " answer";
                                }
                                $recent_html .= '
                                    <li>' . $recent_countans[$recent_index] . $ans . '</li>
                                        <li>
                                            <div class="tooltip-custom">
                                                <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner tooltip-inner2">
                                                    <a href="#">Invite authoritative user to answer</a></br>
                                                    <a href="#">Add to Reading List</a></br>
                                                    <a href="#">Show Related Questions</a></br>
                                                    <a href="#">Create Summary Answer</a></br>
                                                    <a href="#">Mark Favorite</a></br>
                                                    <a href="#">Report</a></br>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $recent_sectionquery[$recent_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $recent_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $recent_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        $recent_html .= '
                            </div>
                        </div>
                    </div>';

                    $recent_index += 1;     
        }

                        
        if ($recent_index < $recent_countque) 
        {
            $recent_html .= '
<div class="thumb_full">
    <div class="thumb_hover full">
        <div class="hover_overlay">
            <div class="inner">
                <div class="top_line">
                    <span class="time left">' . calculate_weeks_ago($recent_sectionquery[$recent_index]["created"]) . ' w ago</span>
                    <span class="topic right">'.$recent_sectionquery[$recent_index]["cattitle"].'</span>
                    <div class="clearfix"></div>
                </div>
            <div class="bottom_line bounceInUp">
                <div class="h5">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $recent_sectionquery[$recent_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $recent_sectionquery[$recent_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $recent_html .= '
                            <a href="' . $ref . '">' . $recent_sectionquery[$recent_index]["title"] . '</a>
                        </div>
                        <div class="ans">';

/*                    
                    $recent_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $recent_sectionquery[2]['postid'] . "' "));
                    $recent_countans = sizeof($recent_queryans);
*/
                    if($recent_queryans[$recent_index] != NULL) {
                        $recent_html .= 'Ans: '.substr($recent_queryans[$recent_index][0]['content'],0,300);
                    } 
                    else {
                        $recent_html .= 'No Answer';
                    }

            $recent_html .='
                </div>
                <div class="toolbar">
                    <div class="tools left">
                    <ul>
                        <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                            <ul class="list-unstyled pull-left">
                                <li><a onClick="addVote('.$recent_sectionquery[$recent_index]['postid'].');">
                                    <i class="fa fa-angle-double-up"></i>
                                    <span id=span-'.$recent_sectionquery[$recent_index]['postid'].'> '.$recent_sectionquery[$recent_index]['upvotes'].'</span>
                                    </a>
                                </li>
                                <li><a onClick="downVote('.$recent_sectionquery[$recent_index]['postid'].');">
                                    <i class="fa fa-angle-double-down"></i>
                                    <span id=span-'.$recent_sectionquery[$recent_index]['postid'].'-downvote> '.$recent_sectionquery[$recent_index]['downvotes'].'</span>
                                    </a>
                                </li>
                            </ul>
                        <li><a href="javascript:void(0);">3k views</a></li>
                        <li><a href="javascript:void(0);">235 comments</a></li>
                        <li>                                                            
                                <ul>
                                    <li>
                                    <div class="tooltip-social">
                                        <a href="#">Share</a>
                                        <div class="tooltip-inner tooltip-inner3">
                                            <div class="social_icon facebook_icon">
                                                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon twitter_icon">
                                                <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon google_icon">
                                                <a href="#"><i class="fa fa-google fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon instagram_icon">
                                                <a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>                                                       
                            </li>
                    </ul>
                </div>
                <div class="share right">
                    <ul>';
                                                        
                        if ($recent_countans[$recent_index] > 1) {
                            $ans = " answers";
                        }
                        else {
                            $ans = " answer";
                        }
                        $recent_html .= '
                            <li>' . $recent_countans[$recent_index] . $ans . '</li>
                        <li>
                        <div class="tooltip-custom">
                            <i class="fa fa-ellipsis-h"></i>
                            <div class="tooltip-inner tooltip-inner2">
                                <a href="#">Invite authoritative user to answer</a><br>
                                <a href="#">Add to Reading List</a><br>
                                <a href="#">Show Related Questions</a><br>
                                <a href="#">Create Summary Answer</a><br>
                                <a href="#">Mark Favorite</a><br>
                                <a href="#">Report</a>
                            </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>';

                        //The next line is to get the postid of the question
                        $postid_num = $recent_sectionquery[$recent_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $recent_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $recent_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        //Note by Julio Velasquez: I add this </div> because it missing to put the second column (full_single_right) at right
                        //</div>

                        $recent_html .= '      
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

            $recent_index += 1;   
        }
                             
        if ($recent_index < $recent_countque)
        {
            $recent_html .= '
<div class="col-md-6">
    <div class="row">
        <div class="thumb_full_single_right">
            <div class="thumb_hover full_single">
                <div class="hover_overlay">
                    <div class="inner">
                        <div class="top_line">
                            <span class="time left">' . calculate_weeks_ago($recent_sectionquery[$recent_index]["created"]) . ' w ago</span>
                            <span class="topic right">'.$recent_sectionquery[$recent_index]["cattitle"].'</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h5">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $recent_sectionquery[$recent_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $recent_sectionquery[$recent_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $recent_html .= '
                            <a href="' . $ref . '">' . $recent_sectionquery[$recent_index]["title"] . '</a>
                        </div>
                        <div class="ans">';
/*               
                $recent_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $recent_sectionquery[1]['postid'] . "' "));
                $recent_countans = sizeof($recent_queryans);
*/                
                    if($recent_queryans[$recent_index] != NULL) {
                        $recent_html .='Ans: '.substr($recent_queryans[$recent_index][0]['content'],0,300);
                    }
                    else {
                        $recent_html .='No Answer';
                    }

                    $recent_html .='
                    </div>
                    <div class="toolbar">
                        <div class="tools left">
                        <ul>
                            <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                            <ul class="list-unstyled pull-left">
                                <li><a onClick="addVote('.$recent_sectionquery[$recent_index]['postid'].');">
                                    <i class="fa fa-angle-double-up"></i>
                                    <span id=span-'.$recent_sectionquery[$recent_index]['postid'].'> '.$recent_sectionquery[$recent_index]['upvotes'].'</span>
                                    </a>
                                </li>
                                <li><a onClick="downVote('.$recent_sectionquery[$recent_index]['postid'].');">
                                    <i class="fa fa-angle-double-down"></i>
                                    <span id=span-'.$recent_sectionquery[$recent_index]['postid'].'-downvote> '.$recent_sectionquery[$recent_index]['downvotes'].'</span>
                                    </a>
                                </li>
                            </ul>
                            <li><a href="javascript:void(0);">3k views</a></li>
                            <li><a href="javascript:void(0);">235 comments</a></li>
                            <li>                                                            
                                <ul>
                                    <li>
                                    <div class="tooltip-social">
                                        <a href="#">Share</a>
                                        <div class="tooltip-inner tooltip-inner3">
                                            <div class="social_icon facebook_icon">
                                                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon twitter_icon">
                                                <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon google_icon">
                                                <a href="#"><i class="fa fa-google fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon instagram_icon">
                                                <a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>                                                       
                            </li>
                        </ul>
                    </div>
                    <div class="share right">
                        <ul>';
                                                        
                        if ($recent_countans[$recent_index] > 1) {
                            $ans = " answers";
                        }
                        else {
                            $ans = " answer";
                        }

                        $recent_html .= '
                            <li>' . $recent_countans[$recent_index] . $ans . '</li>
                            <li>
                            <div class="tooltip-custom">
                                <i class="fa fa-ellipsis-h"></i>
                            <div class="tooltip-inner tooltip-inner2">
                                <a href="#">Invite authoritative user to answer</a><br>
                                <a href="#">Add to Reading List</a><br>
                                <a href="#">Show Related Questions</a><br>
                                <a href="#">Create Summary Answer</a><br>
                                <a href="#">Mark Favorite</a><br>
                                <a href="#">Report</a>
                            </div>
                            </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $recent_sectionquery[$recent_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $recent_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $recent_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        $recent_html .= '
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

//            $recent_index += 1;    
        }

        $recent_html .= '
        </div>
    </div>';
       
    }
    else {
        $recent_html .= '<div class="noqueans">No questions and answers available</div>';
    }

    $recent_html .= '</div></div></div>';

    $recent_html .= '<input type="hidden" id="recent_slide_index" value="' . $recent_slide_index . '">
                 <input type="hidden" id="recent_total_slides" value="' . $recent_total_slides . '">';

    echo $recent_html;
}

//all quation part start
elseif($_REQUEST['type'] == 'allque')
{
    $section_table = 'allquation';
    $allque_html .= '
        <div id="allquation_grid">  
            <div class="container popular_answer_wrapper">
                <a onclick="arrowleft_allquestions();" class="left-arrow" id="allquation-left"><i class="fa fa-angle-left"></i></a>
                <a onclick="arrowright_allquestions();" class="right-arrow" id="allquation-right"><i class="fa fa-angle-right"></i></a>
                <div class="row" id="allquation_first_five">';
    
    if($_REQUEST['name'] == 'allquestions')
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);
        $allque_total_slides = $allque_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($allque_total_slides)) {
            $allque_total_slides = intval($allque_total_slides) + 1;
        }
        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($allque_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        if($allque_firsttime == TRUE) {
            $allque_firsttime = FALSE;
        }
        $allque_slide_index = 1;
        $allque_index = 0;
    }

    elseif($_REQUEST['name'] == 'allqueweek')
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 WEEK ) ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);
        $allque_total_slides = $allque_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($allque_total_slides)) {
            $allque_total_slides = intval($allque_total_slides) + 1;
        }
        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM qa_posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($allque_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        if($allque_firsttime == TRUE) {
            $allque_firsttime = FALSE;
        }
        $allque_slide_index = 1;
        $allque_index = 0;
    }

    elseif($_REQUEST['name'] == 'allquemonth')
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ) , INTERVAL 1 MONTH ) ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);
        $allque_total_slides = $allque_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if (is_float($allque_total_slides)) {
            $allque_total_slides = intval($allque_total_slides) + 1;
        }
        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($allque_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        if($allque_firsttime == TRUE) {
            $allque_firsttime = FALSE;
        }
        $allque_slide_index = 1;
        $allque_index = 0;
    }
    
    elseif($_REQUEST['name'] == 'allqueyear')
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ) , INTERVAL 1 YEAR ) ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);
        $allque_total_slides = $allque_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($allque_total_slides)) {
            $allque_total_slides = intval($allque_total_slides) + 1;
        }
        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($allque_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        if($allque_firsttime == TRUE) {
            $allque_firsttime = FALSE;
        }
        $allque_slide_index = 1;
        $allque_index = 0;
    }

    elseif($_REQUEST['name'] == 'allquecategory')
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND ^posts.categoryid = '" . $_REQUEST['catname'] . "' ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);
        $allque_total_slides = $allque_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($allque_total_slides)) {
            $allque_total_slides = intval($allque_total_slides) + 1;
        }
        for($i=0; $i<$allque_countque; $i++)
        { 
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($allque_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        if($allque_firsttime == TRUE) {
            $allque_firsttime = FALSE;
        }
        $allque_slide_index = 1;
        $allque_index = 0;
    }

    elseif($_REQUEST['name'] == 'arrowleft' && $allque_firsttime == TRUE)
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);
        $allque_total_slides = $allque_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($allque_total_slides)) {
            $allque_total_slides = intval($allque_total_slides) + 1;
        }
        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($allque_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }        
        if($allque_firsttime == TRUE) {
            $allque_firsttime = FALSE;
        }
        $allque_slide_index = 1;
        $allque_index = 0;
    }

    elseif($_REQUEST['name'] == 'arrowleft' && $allque_firsttime == FALSE)
    {
        $allque_slide_index -= 1;

        if($allque_slide_index == 1) {
            $allque_index = 0;
        }
        elseif ($allque_slide_index <= 0) {
            $allque_index = -1;
        }
        else {
            $allque_index = $allque_slide_index * posts_per_slide - posts_per_slide;
        }
    }

    elseif($_REQUEST['name'] == 'arrowright' && $allque_firsttime == TRUE)
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);
        $allque_total_slides = $allque_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($allque_total_slides)) {
            $allque_total_slides = intval($allque_total_slides) + 1;
        }
        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($allque_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        $allque_slide_index = 2;
        $allque_index = 4;
        
        if($allque_firsttime == TRUE) {
            $allque_firsttime = FALSE;
        }
    }

    elseif($_REQUEST['name'] == 'arrowright' && $allque_firsttime == FALSE)
    {
        if($allque_index <= $allque_countque) {
            $allque_index += 1;
            $allque_slide_index +=1;
        }
    }


    if($allque_index < $allque_countque && $allque_slide_index != 0)
    {
        $allque_html .= '
                <div class="col-md-6">
                    <div class="row">
                        <div class="gallery_four_thumb">
                            <div class="clearfix">
                                <div class="thumb_half">
                                    <div class="thumb_hover half">
                                        <div class="hover_overlay">';

        if ($allque_index < $allque_countque)
        {
            $allque_html .= '
                <div class="inner">
                    <div class="top_line">
                        <span class="time left">' . calculate_weeks_ago($allque_sectionquery[$allque_index]["created"]) . ' w ago</span>
                        <span class="topic right">'.$allque_sectionquery[$allque_index]["cattitle"].'</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h6">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $allque_sectionquery[$allque_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $allque_sectionquery[$allque_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $allque_html .= '
                            <a href="' . $ref . '">' . $allque_sectionquery[$allque_index]["title"] . '</a>
                        </div>
                            <div class="ans">';
/*                                                                                                       
                            $allque_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $allque_sectionquery[0]['postid'] . "' "));
                            $allque_countans = sizeof($allque_queryans);
*/                            
                            if($allque_queryans[$allque_index] != NULL) {
                                $allque_html .= 'Ans: '.substr($allque_queryans[$allque_index][0]['content'],0,100);
                            }
                            else {
                                $allque_html .= 'No Answer';
                            }

                            $allque_html .= '
                            </div>
                            <div class="toolbar">
                                <div class="tools left">
                                    <ul>
                                        <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                    </ul>
                                    <ul class="list-unstyled pull-left">
                                        <li>
                                            <a onClick="addVote('.$allque_sectionquery[$allque_index]['postid'].');">
                                                <i class="fa fa-angle-double-up"></i>
                                                <span id=span-'.$allque_sectionquery[$allque_index]['postid'].'> '.$allque_sectionquery[$allque_index]['upvotes'].'</span>
                                            </a>
                                        </li>
                                         <li>
                                            <a onClick="downVote('.$allque_sectionquery[$allque_index]['postid'].');">
                                                <i class="fa fa-angle-double-down"></i>
                                                <span id=span-'.$allque_sectionquery[$allque_index]['postid'].'-downvote> '.$allque_sectionquery[$allque_index]['downvotes'].'</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="share right">
                                    <ul>';
                                                        
                                if ($allque_countans[$allque_index] > 1) {
                                    $ans = " answers";
                                }
                                else {
                                    $ans = " answer";
                                }

                                $allque_html .= '
                                    <li>' . $allque_countans[$allque_index] . $ans . '</li>
                                        <li>
                                            <div class="tooltip-custom">
                                                <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner tooltip-inner2">
                                                    <a href="#">Invite authoritative user to answer</a></br>
                                                    <a href="#">Add to Reading List</a></br>
                                                    <a href="#">Show Related Questions</a></br>
                                                    <a href="#">Create Summary Answer</a></br>
                                                    <a href="#">Mark Favorite</a></br>
                                                    <a href="#">Report</a></br>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $allque_sectionquery[$allque_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $allque_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $allque_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }      
                        
                        $allque_html .= '
                            </div>
                         </div>';

                        $allque_index += 1;
        }

        if ($allque_index < $allque_countque)
        {
            $allque_html .= '
    <div class="thumb_half">
        <div class="thumb_hover half">
            <div class="hover_overlay">
                <div class="inner">
                    <div class="top_line">
                        <span class="time left">' . calculate_weeks_ago($allque_sectionquery[$allque_index]["created"]) . ' w ago</span>
                        <span class="topic right">'.$allque_sectionquery[$allque_index]["cattitle"].'</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h6">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $allque_sectionquery[$allque_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $allque_sectionquery[$allque_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $allque_html .= '
                            <a href="' . $ref . '">' . $allque_sectionquery[$allque_index]["title"] . '</a>
                        </div>
                        <div class="ans">';
/*                                                                                                       
                            $allque_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $allque_sectionquery[1]['postid'] . "' "));
                            $allque_countans = sizeof($allque_queryans);
*/
                            if($allque_queryans[$allque_index] != NULL) {
                                $allque_html .= 'Ans: '.substr($allque_queryans[$allque_index][0]['content'],0,100);
                            }
                            else {
                                $allque_html .='No Answer';
                            }
                            
                            $allque_html .='
                            </div>
                            <div class="toolbar">
                                <div class="tools left">
                                    <ul>
                                        <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                    </ul>
                                    <ul class="list-unstyled pull-left">
                                        <li><a onClick="addVote('.$allque_sectionquery[$allque_index]['postid'].');">
                                            <i class="fa fa-angle-double-up"></i><span id=span-'.$allque_sectionquery[$allque_index]['postid'].'> '.$allque_sectionquery[$allque_index]['upvotes'].'</span>
                                            </a>
                                        </li>
                                        <li><a onClick="downVote('.$allque_sectionquery[$allque_index]['postid'].');">
                                            <i class="fa fa-angle-double-down"></i><span id=span-'.$allque_sectionquery[$allque_index]['postid'].'-downvote> '.$allque_sectionquery[$allque_index]['downvotes'].'</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="share right">
                                    <ul>';
                                                        
                                if ($allque_countans[$allque_index] > 1) {
                                    $ans = " answers";
                                }
                                else {
                                    $ans = " answer";
                                }

                                $allque_html .= '
                                    <li>' . $allque_countans[$allque_index] . $ans . '</li>
                                        <li>
                                            <div class="tooltip-custom">
                                                <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner tooltip-inner2">
                                                    <a href="#">Invite authoritative user to answer</a></br>
                                                    <a href="#">Add to Reading List</a></br>
                                                    <a href="#">Show Related Questions</a></br>
                                                    <a href="#">Create Summary Answer</a></br>
                                                    <a href="#">Mark Favorite</a></br>
                                                    <a href="#">Report</a></br>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $allque_sectionquery[$allque_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $allque_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $allque_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        $allque_html .= '
                            </div>
                        </div>
                    </div>';

                    $allque_index += 1;     
        }

                        
        if ($allque_index < $allque_countque)
        {
            $allque_html .= '
<div class="thumb_full">
    <div class="thumb_hover full">
        <div class="hover_overlay">
            <div class="inner">
                <div class="top_line">
                    <span class="time left">' . calculate_weeks_ago($allque_sectionquery[$allque_index]["created"]) . ' w ago</span>
                    <span class="topic right">'.$allque_sectionquery[$allque_index]["cattitle"].'</span>
                    <div class="clearfix"></div>
                </div>
            <div class="bottom_line bounceInUp">
                <div class="h5">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $allque_sectionquery[$allque_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $allque_sectionquery[$allque_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $allque_html .= '
                            <a href="' . $ref . '">' . $allque_sectionquery[$allque_index]["title"] . '</a>
                        </div>
                        <div class="ans">';
/*                    
                    $allque_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $allque_sectionquery[2]['postid'] . "' "));
                    $allque_countans = sizeof($allque_queryans);
*/
                    if($allque_queryans[$allque_index] != NULL) {
                        $allque_html .= 'Ans: '.substr($allque_queryans[$allque_index][0]['content'],0,300);
                    } 
                    else {
                        $allque_html .= 'No Answer';
                    }

            $allque_html .='
                </div>
                <div class="toolbar">
                    <div class="tools left">
                    <ul>
                        <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                            <ul class="list-unstyled pull-left">
                                <li><a onClick="addVote('.$allque_sectionquery[$allque_index]['postid'].');">
                                    <i class="fa fa-angle-double-up"></i>
                                    <span id=span-'.$allque_sectionquery[$allque_index]['postid'].'> '.$allque_sectionquery[$allque_index]['upvotes'].'</span>
                                    </a>
                                </li>
                                <li><a onClick="downVote('.$allque_sectionquery[$allque_index]['postid'].');">
                                    <i class="fa fa-angle-double-down"></i>
                                    <span id=span-'.$allque_sectionquery[$allque_index]['postid'].'-downvote> '.$allque_sectionquery[$allque_index]['downvotes'].'</span>
                                    </a>
                                </li>
                            </ul>
                        <li><a href="javascript:void(0);">3k views</a></li>
                        <li><a href="javascript:void(0);">235 comments</a></li>
                        <li>                                                            
                                <ul>
                                    <li>
                                    <div class="tooltip-social">
                                        <a href="#">Share</a>
                                        <div class="tooltip-inner tooltip-inner3">
                                            <div class="social_icon facebook_icon">
                                                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon twitter_icon">
                                                <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon google_icon">
                                                <a href="#"><i class="fa fa-google fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon instagram_icon">
                                                <a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>                                                       
                            </li>
                    </ul>
                </div>
                <div class="share right">
                    <ul>';
                                                        
                    if ($allque_countans[$allque_index] > 1) {
                        $ans = " answers";
                    }
                    else {
                        $ans = " answer";
                    }
                    $allque_html .= '
                        <li>' . $allque_countans[$allque_index] . $ans . '</li>
                        <li>                        
                        <div class="tooltip-custom">
                            <i class="fa fa-ellipsis-h"></i>
                            <div class="tooltip-inner tooltip-inner2">
                                <a href="#">Invite authoritative user to answer</a><br>
                                <a href="#">Add to Reading List</a><br>
                                <a href="#">Show Related Questions</a><br>
                                <a href="#">Create Summary Answer</a><br>
                                <a href="#">Mark Favorite</a><br>
                                <a href="#">Report</a>
                            </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>';

                        //The next line is to get the postid of the question
                        $postid_num = $allque_sectionquery[$allque_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $allque_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $allque_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }                    

                        $allque_html .= '      
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

            $allque_index += 1;   
        }
                             
        if ($allque_index < $allque_countque)
        {
            $allque_html .= '
<div class="col-md-6">
    <div class="row">
        <div class="thumb_full_single_right">
            <div class="thumb_hover full_single">
                <div class="hover_overlay">
                    <div class="inner">
                        <div class="top_line">
                            <span class="time left">' . calculate_weeks_ago($allque_sectionquery[$allque_index]["created"]) . ' w ago</span>
                            <span class="topic right">'.$allque_sectionquery[$allque_index]["cattitle"].'</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h5">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $allque_sectionquery[$allque_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $allque_sectionquery[$allque_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $allque_html .= '
                            <a href="' . $ref . '">' . $allque_sectionquery[$allque_index]["title"] . '</a>
                        </div>
                    <div class="ans">';
/*               
                $allque_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $allque_sectionquery[1]['postid'] . "' "));
                $allque_countans = sizeof($allque_queryans);
*/                
                    if($allque_queryans[$allque_index] != NULL) {
                        $allque_html .='Ans: '.substr($allque_queryans[$allque_index][0]['content'],0,300);
                    }
                    else {
                        $allque_html .='No Answer';
                    }

                    $allque_html .='
                    </div>
                    <div class="toolbar">
                        <div class="tools left">
                        <ul>
                            <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                            <ul class="list-unstyled pull-left">
                                <li><a onClick="addVote('.$allque_sectionquery[$allque_index]['postid'].');">
                                    <i class="fa fa-angle-double-up"></i>
                                    <span id=span-'.$allque_sectionquery[$allque_index]['postid'].'> '.$allque_sectionquery[$allque_index]['upvotes'].'</span>
                                    </a>
                                </li>
                                <li><a onClick="downVote('.$allque_sectionquery[$allque_index]['postid'].');">
                                    <i class="fa fa-angle-double-down"></i>
                                    <span id=span-'.$allque_sectionquery[$allque_index]['postid'].'-downvote> '.$allque_sectionquery[$allque_index]['downvotes'].'</span>
                                    </a>
                                </li>
                            </ul>
                            <li><a href="javascript:void(0);">3k views</a></li>
                            <li><a href="javascript:void(0);">235 comments</a></li>
                            <li>                                                            
                                <ul>
                                    <li>
                                    <div class="tooltip-social">
                                        <a href="#">Share</a>
                                        <div class="tooltip-inner tooltip-inner3">
                                            <div class="social_icon facebook_icon">
                                                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon twitter_icon">
                                                <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon google_icon">
                                                <a href="#"><i class="fa fa-google fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon instagram_icon">
                                                <a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>                                                       
                            </li>
                        </ul>
                    </div>
                    <div class="share right">
                        <ul>';
                                                        
                        if ($allque_countans[$allque_index] > 1) {
                            $ans = " answers";
                        }
                         else {
                            $ans = " answer";
                        }
                        $allque_html .= '
                            <li>' . $allque_countans[$allque_index] . $ans . '</li>
                            <li>
                                <div class="tooltip-custom">
                                <i class="fa fa-ellipsis-h"></i>
                                    <div class="tooltip-inner tooltip-inner2">
                                        <a href="#">Invite authoritative user to answer</a><br>
                                        <a href="#">Add to Reading List</a><br>
                                        <a href="#">Show Related Questions</a><br>
                                        <a href="#">Create Summary Answer</a><br>
                                        <a href="#">Mark Favorite</a><br>
                                        <a href="#">Report</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $allque_sectionquery[$allque_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $allque_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $allque_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        $allque_html .= '
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

//            $allque_index += 1;    
        }

        $allque_html .= '
        </div>
    </div>';
       
    }
    else {
        $allque_html .= '<div class="noqueans">No questions and answers available</div>';
    }

    $allque_html .= '</div></div></div>';

    $allque_html .= '<input type="hidden" id="allque_slide_index" value="' . $allque_slide_index . '">
                 <input type="hidden" id="allque_total_slides" value="' . $allque_total_slides . '">';

    echo $allque_html;
}

//unans part start
elseif($_REQUEST['type'] == 'unans')
{
    $section_table = 'unansquation';
    $unans_html .='
        <div id="unansquation_grid">
            <div class="container popular_answer_wrapper">
                <a onclick="arrowleft_unanswered();" class="left-arrow" id="unansquation-left"><i class="fa fa-angle-left"></i></a>
                <a onclick="arrowright_unanswered();" class="right-arrow" id="unansquation-right"><i class="fa fa-angle-right"></i></a>
                <div class="row" id="unansquation_first_five">';

    if($_REQUEST['name'] == 'unanswered')
    {  
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        
        $unans_countque = sizeof($unans_sectionquery);
        $unans_total_slides = $unans_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($unans_total_slides)) {
            $unans_total_slides = intval($unans_total_slides) + 1;
        }
        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($unans_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        if($unans_firsttime == TRUE) {
            $unans_firsttime = FALSE;
        }
        $unans_slide_index = 1;
        $unans_index = 0;

    }

    elseif($_REQUEST['name'] == 'unansweek')
    {  
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND created > DATE_SUB( NOW(), INTERVAL 1 WEEK ) AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        
        $unans_countque = sizeof($unans_sectionquery);
        $unans_total_slides = $unans_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($unans_total_slides)) {
            $unans_total_slides = intval($unans_total_slides) + 1;
        }
        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($unans_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        if($unans_firsttime == TRUE) {
            $unans_firsttime = FALSE;
        }
        $unans_slide_index = 1;
        $unans_index = 0;
    }

    elseif($_REQUEST['name'] == 'unansmonth')
    {  
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND created > DATE_SUB( NOW(), INTERVAL 1 MONTH ) AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        
        $unans_countque = sizeof($unans_sectionquery);
        $unans_total_slides = $unans_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($unans_total_slides)) {
            $unans_total_slides = intval($unans_total_slides) + 1;
        }
        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($unans_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        if($unans_firsttime == TRUE) {
            $unans_firsttime = FALSE;
        }
        $unans_slide_index = 1;
        $unans_index = 0;
    }
    
    elseif($_REQUEST['name'] == 'unansyear')
    {  
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND created > DATE_SUB( NOW(), INTERVAL 1 YEAR ) AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        
        $unans_countque = sizeof($unans_sectionquery);
        $unans_total_slides = $unans_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($unans_total_slides)) {
            $unans_total_slides = intval($unans_total_slides) + 1;
        }
        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($unans_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        if($unans_firsttime == TRUE) {
            $unans_firsttime = FALSE;
        }
        $unans_slide_index = 1;
        $unans_index = 0;
    }

    elseif($_REQUEST['name'] == 'unanscategory')
    {
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND ^posts.categoryid = '" . $_REQUEST['catname'] . "' AND postid NOT IN(select parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        
        $unans_countque = sizeof($unans_sectionquery);
        $unans_total_slides = $unans_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($unans_total_slides)) {
            $unans_total_slides = intval($unans_total_slides) + 1;
        }
        for($i=0; $i<$unans_countque; $i++)
        { 
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($unans_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            } 
        }
        if($unans_firsttime == TRUE) {
            $unans_firsttime = FALSE;
        }
        $unans_slide_index = 1;
        $unans_index = 0;
    }

    elseif($_REQUEST['name'] == 'arrowleft' && $unans_firsttime == TRUE)
    {
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        $unans_countque = sizeof($unans_sectionquery);
        $unans_total_slides = $unans_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($unans_total_slides)) {
            $unans_total_slides = intval($unans_total_slides) + 1;
        }
        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($unans_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }        
        if($unans_firsttime == TRUE) {
            $unans_firsttime = FALSE;
        }
        $unans_slide_index = 1;
        $unans_index = 0;
    }

    elseif($_REQUEST['name'] == 'arrowleft' && $unans_firsttime == FALSE)
    {
        $unans_slide_index -= 1;

        if($unans_slide_index == 1) {
            $unans_index = 0;
        }
        elseif ($unans_slide_index <= 0) {
            $unans_index = -1;
        }
        else {
            $unans_index = $unans_slide_index * posts_per_slide - posts_per_slide;
        }
    }

    elseif($_REQUEST['name'] == 'arrowright' && $unans_firsttime == TRUE)
    {
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        $unans_countque = sizeof($unans_sectionquery);
        $unans_total_slides = $unans_countque / posts_per_slide;

        //This is in case that slides' amount is float, then the code set it as int in order to have the slide's number right
        if(is_float($unans_total_slides)) {
            $unans_total_slides = intval($unans_total_slides) + 1;
        }
        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($unans_queryans[$i] != NULL) {
                //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        $unans_slide_index = 2;
        $unans_index = 4;
        
        if($unans_firsttime == TRUE) {
            $unans_firsttime = FALSE;
        }
    }

    elseif($_REQUEST['name'] == 'arrowright' && $unans_firsttime == FALSE)
    {
        if($unans_index <= $unans_countque) {
            $unans_index += 1;
            $unans_slide_index +=1;
        }
    }

    if($unans_index < $unans_countque && $unans_slide_index != 0)
    {
        $unans_html .= '
                <div class="col-md-6">
                    <div class="row">
                        <div class="gallery_four_thumb">
                            <div class="clearfix">
                                <div class="thumb_half">
                                    <div class="thumb_hover half">
                                        <div class="hover_overlay">';

        if ($unans_index < $unans_countque)
        {
            $unans_html .= '
                <div class="inner">
                    <div class="top_line">
                        <span class="time left">' . calculate_weeks_ago($unans_sectionquery[$unans_index]["created"]) . ' w ago</span>
                        <span class="topic right">'.$unans_sectionquery[$unans_index]["cattitle"].'</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h6">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $unans_sectionquery[$unans_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $unans_sectionquery[$unans_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $unans_html .= '
                            <a href="' . $ref . '">' . $unans_sectionquery[$unans_index]["title"] . '</a>
                        </div>
                            <div class="ans">';
/*                                                                                                       
                            $unans_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $unans_sectionquery[0]['postid'] . "' "));
                            $unans_countans = sizeof($unans_queryans);
*/                            
                            if($unans_queryans[$unans_index] != NULL) {
                                $unans_html .= 'Ans: '.substr($unans_queryans[$unans_index][0]['content'],0,100);
                            }
                            else {
                                $unans_html .= 'No Answer';
                            }

                            $unans_html .= '
                            </div>
                            <div class="toolbar">
                                <div class="tools left">
                                    <ul>
                                        <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                    </ul>
                                    <ul class="list-unstyled pull-left">
                                        <li>
                                            <a onClick="addVote('.$unans_sectionquery[$unans_index]['postid'].');">
                                                <i class="fa fa-angle-double-up"></i>
                                                <span id=span-'.$unans_sectionquery[$unans_index]['postid'].'> '.$unans_sectionquery[$unans_index]['upvotes'].'</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a onClick="downVote('.$unans_sectionquery[$unans_index]['postid'].');">
                                                <i class="fa fa-angle-double-down"></i>
                                                <span id=span-'.$unans_sectionquery[$unans_index]['postid'].'-downvote> '.$unans_sectionquery[$unans_index]['downvotes'].'</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="share right">
                                    <ul>';
                                                        
                                if ($unans_countans[$unans_index] > 1) {
                                    $ans = " answers";
                                }
                                else {
                                    $ans = " answer";
                                }

                                $unans_html .= '
                                    <li>' . $unans_countans[$unans_index] . $ans . '</li>
                                        <li>
                                            <div class="tooltip-custom">
                                            <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner tooltip-inner2">
                                                    <a href="#">Invite authoritative user to answer</a><br>
                                                    <a href="#">Add to Reading List</a><br>
                                                    <a href="#">Show Related Questions</a><br>
                                                    <a href="#">Create Summary Answer</a><br>
                                                    <a href="#">Mark Favorite</a><br>
                                                    <a href="#">Report</a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $unans_sectionquery[$unans_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $unans_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $unans_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }      
                        
                        $unans_html .= '
                            </div>
                         </div>';

                        $unans_index += 1;
        }

        if ($unans_index < $unans_countque)
        {
            $unans_html .= '
    <div class="thumb_half">
        <div class="thumb_hover half">
            <div class="hover_overlay">
                <div class="inner">
                    <div class="top_line">
                        <span class="time left">' . calculate_weeks_ago($unans_sectionquery[$unans_index]["created"]) . ' w ago</span>
                        <span class="topic right">'.$unans_sectionquery[$unans_index]["cattitle"].'</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h6">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $unans_sectionquery[$unans_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $unans_sectionquery[$unans_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $unans_html .= '
                            <a href="' . $ref . '">' . $unans_sectionquery[$unans_index]["title"] . '</a>
                        </div>
                            <div class="ans">';
/*                                                                                                       
                            $unans_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $unans_sectionquery[1]['postid'] . "' "));
                            $unans_countans = sizeof($unans_queryans);
*/
                            if($unans_queryans[$unans_index] != NULL) {
                                $unans_html .= 'Ans: '.substr($unans_queryans[$unans_index][0]['content'],0,100);
                            }
                            else {
                                $unans_html .='No Answer';
                            }
                            
                            $unans_html .='
                            </div>
                            <div class="toolbar">
                                <div class="tools left">
                                    <ul>
                                        <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                    </ul>
                                    <ul class="list-unstyled pull-left">
                                        <li><a onClick="addVote('.$unans_sectionquery[$unans_index]['postid'].');">
                                            <i class="fa fa-angle-double-up"></i><span id=span-'.$unans_sectionquery[$unans_index]['postid'].'> '.$unans_sectionquery[$unans_index]['upvotes'].'</span>
                                            </a>
                                        </li>
                                        <li><a onClick="downVote('.$unans_sectionquery[$unans_index]['postid'].');">
                                            <i class="fa fa-angle-double-down"></i><span id=span-'.$unans_sectionquery[$unans_index]['postid'].'-downvote> '.$unans_sectionquery[$unans_index]['downvotes'].'</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="share right">
                                    <ul>';
                                                        
                                if ($unans_countans[$unans_index] > 1) {
                                    $ans = " answers";
                                }
                                else {
                                    $ans = " answer";
                                }
                                $unans_html .= '
                                        <li>' . $unans_countans[$unans_index] . $ans . '</li>
                                        <li>
                                            <div class="tooltip-custom">
                                            <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner tooltip-inner2">
                                                    <a href="#">Invite authoritative user to answer</a><br>
                                                    <a href="#">Add to Reading List</a><br>
                                                    <a href="#">Show Related Questions</a><br>
                                                    <a href="#">Create Summary Answer</a><br>
                                                    <a href="#">Mark Favorite</a><br>
                                                    <a href="#">Report</a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $unans_sectionquery[$unans_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $unans_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $unans_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        $unans_html .= '
                            </div>
                        </div>
                    </div>';

                    $unans_index += 1;     
        }

                        
        if ($unans_index < $unans_countque) 
        {
            $unans_html .= '
<div class="thumb_full">
    <div class="thumb_hover full">
        <div class="hover_overlay">
            <div class="inner">
                <div class="top_line">
                    <span class="time left">' . calculate_weeks_ago($unans_sectionquery[$unans_index]["created"]) . ' w ago</span>
                    <span class="topic right">'.$unans_sectionquery[$unans_index]["cattitle"].'</span>
                    <div class="clearfix"></div>
                </div>
            <div class="bottom_line bounceInUp">
                <div class="h5">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $unans_sectionquery[$unans_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $unans_sectionquery[$unans_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $unans_html .= '
                            <a href="' . $ref . '">' . $unans_sectionquery[$unans_index]["title"] . '</a>
                        </div>
                        <div class="ans">';

/*                    
                    $unans_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $unans_sectionquery[2]['postid'] . "' "));
                    $unans_countans = sizeof($unans_queryans);
*/
                    if($unans_queryans[$unans_index] != NULL) {
                        $unans_html .= 'Ans: '.substr($unans_queryans[$unans_index][0]['content'],0,300);
                    } 
                    else {
                        $unans_html .= 'No Answer';
                    }

            $unans_html .='
                </div>
                <div class="toolbar">
                    <div class="tools left">
                    <ul>
                        <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                            <ul class="list-unstyled pull-left">
                                <li><a onClick="addVote('.$unans_sectionquery[$unans_index]['postid'].');">
                                    <i class="fa fa-angle-double-up"></i>
                                    <span id=span-'.$unans_sectionquery[$unans_index]['postid'].'> '.$unans_sectionquery[$unans_index]['upvotes'].'</span>
                                    </a>
                                </li>
                                <li><a onClick="downVote('.$unans_sectionquery[$unans_index]['postid'].');">
                                    <i class="fa fa-angle-double-down"></i>
                                    <span id=span-'.$unans_sectionquery[$unans_index]['postid'].'-downvote> '.$unans_sectionquery[$unans_index]['downvotes'].'</span>
                                    </a>
                                </li>
                            </ul>
                        <li><a href="javascript:void(0);">3k views</a></li>
                        <li><a href="javascript:void(0);">235 comments</a></li>
                        <li>                                                            
                                <ul>
                                    <li>
                                    <div class="tooltip-social">
                                        <a href="#">Share</a>
                                        <div class="tooltip-inner tooltip-inner3">
                                            <div class="social_icon facebook_icon">
                                                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon twitter_icon">
                                                <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon google_icon">
                                                <a href="#"><i class="fa fa-google fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon instagram_icon">
                                                <a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>                                                       
                            </li>
                    </ul>
                </div>
                <div class="share right">
                    <ul>';
                                                        
                    if ($unans_countans[$unans_index] > 1) {
                        $ans = " answers";
                    }
                    else {
                        $ans = " answer";
                    }
                    $unans_html .= '
                        <li>' . $unans_countans[$unans_index] . $ans . '</li>
                        <li>
                        <div class="tooltip-custom">
                            <i class="fa fa-ellipsis-h"></i>
                            <div class="tooltip-inner tooltip-inner2">
                                <a href="#">Invite authoritative user to answer</a><br>
                                <a href="#">Add to Reading List</a><br>
                                <a href="#">Show Related Questions</a><br>
                                <a href="#">Create Summary Answer</a><br>
                                <a href="#">Mark Favorite</a><br>
                                <a href="#">Report</a>
                            </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>';

                        //The next line is to get the postid of the question
                        $postid_num = $unans_sectionquery[$unans_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $unans_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $unans_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        //Note by Julio Velasquez: I add this </div> because it missing to put the second column (full_single_right) at right
                        //</div>

                        $unans_html .= '      
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

            $unans_index += 1;   
        }
                             
        if ($unans_index < $unans_countque)
        {
            $unans_html .= '
<div class="col-md-6">
    <div class="row">
        <div class="thumb_full_single_right">
            <div class="thumb_hover full_single">
                <div class="hover_overlay">
                    <div class="inner">
                        <div class="top_line">
                            <span class="time left">' . calculate_weeks_ago($unans_sectionquery[$unans_index]["created"]) . ' w ago</span>
                            <span class="topic right">'.$unans_sectionquery[$unans_index]["cattitle"].'</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bottom_line bounceInUp">
                        <div class="h5">';

                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $unans_sectionquery[$unans_index]["postid"] . "/" . preg_replace("/[\s_]/", "-", $unans_sectionquery[$unans_index]["title"]);
                        //$ref = "http://" . $_SERVER['SERVER_NAME'] . "/islamiqa/?qa=" . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

                        $unans_html .= '
                            <a href="' . $ref . '">' . $unans_sectionquery[$unans_index]["title"] . '</a>
                        </div>
                    <div class="ans">';
/*               
                $unans_queryans = qa_db_read_all_assoc(qa_db_query_sub("select * from qa_posts where type='A' AND parentid ='" . $unans_sectionquery[1]['postid'] . "' "));
                $unans_countans = sizeof($unans_queryans);
*/                
                    if($unans_queryans[$unans_index] != NULL) {
                        $unans_html .='Ans: '.substr($unans_queryans[$unans_index][0]['content'],0,300);
                    }
                    else {
                        $unans_html .='No Answer';
                    }

                    $unans_html .='
                    </div>
                    <div class="toolbar">
                        <div class="tools left">
                        <ul>
                            <li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                            <ul class="list-unstyled pull-left">
                                <li><a onClick="addVote('.$unans_sectionquery[$unans_index]['postid'].');">
                                    <i class="fa fa-angle-double-up"></i>
                                    <span id=span-'.$unans_sectionquery[$unans_index]['postid'].'> '.$unans_sectionquery[$unans_index]['upvotes'].'</span>
                                    </a>
                                </li>
                                <li><a onClick="downVote('.$unans_sectionquery[$unans_index]['postid'].');">
                                    <i class="fa fa-angle-double-down"></i>
                                    <span id=span-'.$unans_sectionquery[$unans_index]['postid'].'-downvote> '.$unans_sectionquery[$unans_index]['downvotes'].'</span>
                                    </a>
                                </li>
                            </ul>
                            <li><a href="javascript:void(0);">3k views</a></li>
                            <li><a href="javascript:void(0);">235 comments</a></li>
                            <li>                                                            
                                <ul>
                                    <li>
                                    <div class="tooltip-social">
                                        <a href="#">Share</a>
                                        <div class="tooltip-inner tooltip-inner3">
                                            <div class="social_icon facebook_icon">
                                                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon twitter_icon">
                                                <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon google_icon">
                                                <a href="#"><i class="fa fa-google fa-2x"></i></a>
                                            </div>
                                            <div class="social_icon instagram_icon">
                                                <a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>                                                       
                            </li>
                        </ul>
                    </div>
                    <div class="share right">
                        <ul>';
                                                        
                        if ($unans_countans[$unans_index] > 1) {
                            $ans = " answers";
                        }
                        else {
                            $ans = " answer";
                        }
                        $unans_html .= '
                            <li>' . $unans_countans[$unans_index] . $ans . '</li>
                            <li>
                            <div class="tooltip-custom">
                                <i class="fa fa-ellipsis-h"></i>
                            <div class="tooltip-inner tooltip-inner2">
                                <a href="#">Invite authoritative user to answer</a><br>
                                <a href="#">Add to Reading List</a><br>
                                <a href="#">Show Related Questions</a><br>
                                <a href="#">Create Summary Answer</a><br>
                                <a href="#">Mark Favorite</a><br>
                                <a href="#">Report</a>
                            </div>
                            </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>';

                        //The next line is to get the postid of the question
                        $postid_num = $unans_sectionquery[$unans_index]['postid'];

                        //The next line is to complete the image's path         
                        //$imgpath = $context->rooturl . 'includes/images/' . $postid_num . '.jpg';
                        $imgpath = $rooturl . 'includes/images/' . $postid_num . '.jpg';
                        
                        //This code is to verify if the server path to the image exists
                        $imgexist = file_exists(__DIR__ . '/includes/images/' . $postid_num . '.jpg');

                        //This code is to set an image background for questions if the img file exist,
                        //else set a plain color background for questions
                        if ($imgexist) {
                            $unans_html .= '
                                <!-- Now, set the image path as source of the img element -->
                                <div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>';
                        }
                        else {
                            //Here the code to select the plain color to set in the background question
                            $unans_html .= '
                                <!-- In this case just apply the style to the question background  -->
                                <div class="thumb"></div>';
                        }

                        $unans_html .= '
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

//            $unans_index += 1;    
        }

        $unans_html .= '
        </div>
    </div>';
    }
    else {
        $unans_html .= '<div class="noqueans">No questions and answers available</div>';
    }

    $unans_html .= '</div></div></div>';

    $unans_html .= '<input type="hidden" id="unans_slide_index" value="' . $unans_slide_index . '">
                 <input type="hidden" id="unans_total_slides" value="' . $unans_total_slides . '">';

    echo $unans_html;
}
    
    break;

    case 'list':

if ($_REQUEST['type'] == 'most')
{
    $most_html .= '<div id="allthis-week-res">
                        <div id="allthis-week">';

    if ($_REQUEST['name'] == 'mostpopular')
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);

        for($i=0; $i<$most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($most_queryans[$i] != NULL) {                
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }
        $listview_mostindex = 0;
    }

    elseif ($_REQUEST['name'] == 'mostweek')
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 WEEK) ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);
    
        for($i=0; $i<$most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($most_queryans[$i] != NULL) {                
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }
        $listview_mostindex = 0;
    }

    elseif ($_REQUEST['name'] == 'mostmonth')
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 MONTH) ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);
    
        for($i=0; $i<$most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($most_queryans[$i] != NULL) {                
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }
        $listview_mostindex = 0;    
    }
    elseif ($_REQUEST['name'] == 'mostyear')
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 YEAR) ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);
    
        for($i=0; $i<$most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($most_queryans[$i] != NULL) {                
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }
        $listview_mostindex = 0;
    }

    elseif ($_REQUEST['name'] == 'mostcategory')
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND ^posts.categoryid='" . $_REQUEST['catname'] . "' ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);

        for($i=0; $i<$most_countque; $i++)
        { 
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($most_queryans[$i] != NULL) {                
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            } 
        }
        $listview_mostindex = 0;
    }

    elseif (($_REQUEST['name'] == 'more') && ($most_listview_firsttime == "TRUE"))
    {
        $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY netvotes DESC, upvotes DESC LIMIT 120"));
        $most_countque = sizeof($most_sectionquery);

        for($i=0; $i<$most_countque; $i++)
        {
            $most_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($most_queryans[$i] != NULL) {
                $most_countans[$i] = sizeof($most_queryans[$i]);
            }
            else {
                $most_countans[$i] = 0;
            }
        }
        $listview_mostindex = listview_questions;
    }

    if ($most_countque == 0) {
        $most_html .= '<div class="noqueans">No questions and answers available</div>';
    }
    else {

        if (($listview_mostindex + listview_questions) <= $most_countque) {
        $more = $listview_mostindex + listview_questions;
        }
        else {
        $more = $most_countque;
        }

        for ($i=$listview_mostindex; $i<$more; $i++)
        {
                $most_html .= '
                     <!-- ** LIST VIEW STRUCTURE MOST VIEW ** -->
                    <div class="container">
                        <div class="table-responsive">
                            <table class="table table-striped custom_table">
                                <tbody>
                                    <tr>
                                        <td style="width:5%;">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a onClick="addVote('.$most_sectionquery[$i]['postid'].');">
                                                        <i class="fa fa-angle-double-up"></i>
                                                        <span id=span-'.$most_sectionquery[$i]['postid'].'> '.$most_sectionquery[$i]['upvotes'].'</span>
                                                    </a>
                                                </li>
                                                <li>
                                                     <a onClick="downVote('.$most_sectionquery[$i]['postid'].');">
                                                        <i class="fa fa-angle-double-down"></i>
                                                        <span id=span-'.$most_sectionquery[$i]['postid'].'-downvote> '.$most_sectionquery[$i]['downvotes'].'</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </td>

                                        <td colspan="4" style="width:70%;">
                                        <h3 style="padding:0; margin:0;">';

                                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $most_sectionquery[$i]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[$i]["title"]);

                                        $most_html .= '<a href="' . $ref . '" style="color:black;">' . $most_sectionquery[$i]['title'] . '</a></h3>
                                            <p style="margin:0 0 5px 0; font-size:90%;">asked <strong>' . date("M d", strtotime($most_sectionquery[$i]['created'])) . '</strong> in <strong>' . $most_sectionquery[$i]['cattitle'] . '</strong> </p>
                                            <p id="more" style="padding:0; margin:0;">
                                                <span class="more">';

                        //start answer section
                                if ($most_queryans[$i] != NULL) {
                                    $most_html .= 'Last Answer: ' . $most_queryans[$i][0]['content'];
                                }
                                else {
                                    $most_html .= 'No Answer';
                                }
                        //end answer section
                                
                    $most_html .= '</span></p>';

                    $relatedqlink = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/ask?follow=' . $most_sectionquery[$i]['postid'] . '&cat=' . $most_sectionquery[$i]['categoryid'];
                            
                    $most_html .= '</p>
                                            <ul class="list-inline">
                                                    <li class="active">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-pencil"></i>
                                                            <span>                                                           
                                                               <a href=' . $ref . '>Answer</a>
                                                             </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                                <i class="fa fa-bars"></i>
                                                            <span>
                                                                <a href=' . $relatedqlink . '>Ask related question</a>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <div class="tooltip-social">
                                                            <a href="javascript:void(0);">Share</a>
                                                            <div class="tooltip-inner">
                                                                <div class="social_icon facebook_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-twitter fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon google_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-google fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon instagram_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-envelope fa-2x"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>                                                          
                                                    </li>
                                                </ul>
                                    </td>
                                    <td style="width:10%;">
                                            ' . ShowWantAnswer($most_countans[$i], $most_sectionquery[$i]['postid'], $ref) . '
                                        <div class="tooltip-social">
                                            <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner">
                                                    <a href="#">Invite authoritative user to answer</a><br>
                                                    <a href="#">Add to Reading List</a><br>
                                                    <a href="#">Show Related Questions</a><br>
                                                    <a href="#">Create Summary Answer</a><br>
                                                    <a href="#">Mark Favorite</a><br>
                                                    <a href="#">Report</a>
                                                </div>
                                        </div>
                                    </td>
                                    <td style="width:10%;">
                                        <a href="javascript:void(0)" class="btn btn-red">
                                            <i class="fa fa-circle"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
                </div>';

            $listview_mostindex += 1;
        }
    }
    $most_listview_firsttime = "FALSE";
       
    $most_html .= '</div></div></div></div>';

    $most_html .= '<input type="hidden" id="most_countque" value="' . $most_countque . '">
                       <input type="hidden" id="most_index" value="' . $listview_mostindex . '">
                       <input type="hidden" id="most_listview_firsttime" value="' . $most_listview_firsttime . '">';

    $most_html .= '<script src="' . $rooturl . 'js/readmore.js"></script>';

    echo $most_html;
}

elseif ($_REQUEST['type'] == 'recent')
{
    $recent_html .= '<div id="allthis-week-res">
                        <div id="allthis-week">';

    if ($_REQUEST['name'] == 'recently')
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);

        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($recent_queryans[$i] != NULL) {                
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        $listview_recentindex = 0;
    }

    elseif ($_REQUEST['name'] == 'recentlyweek')
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 WEEK) ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);
    
        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($recent_queryans[$i] != NULL) {                
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        $listview_recentindex = 0;
    }

    elseif ($_REQUEST['name'] == 'recentlymonth')
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 MONTH) ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);
    
        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($recent_queryans[$i] != NULL) {                
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        $listview_recentindex = 0;    
    }
    elseif ($_REQUEST['name'] == 'recentlyyear')
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 YEAR) ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);
    
        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($recent_queryans[$i] != NULL) {                
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        $listview_recentindex = 0;
    }

    elseif ($_REQUEST['name'] == 'recentlycategory')
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND ^posts.categoryid='" . $_REQUEST['catname'] . "' ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);

        for($i=0; $i<$recent_countque; $i++)
        { 
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($recent_queryans[$i] != NULL) {                
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            } 
        }
        $listview_recentindex = 0;
    }

    elseif (($_REQUEST['name'] == 'more') && ($recent_listview_firsttime == "TRUE"))
    {
        $recent_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created DESC LIMIT 120"));
        $recent_countque = sizeof($recent_sectionquery);

        for($i=0; $i<$recent_countque; $i++)
        {
            $recent_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $recent_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($recent_queryans[$i] != NULL) {
                $recent_countans[$i] = sizeof($recent_queryans[$i]);
            }
            else {
                $recent_countans[$i] = 0;
            }
        }
        $listview_recentindex = listview_questions;
    }

    if ($recent_countque == 0) {
        $recent_html .= '<div class="noqueans">No questions and answers available</div>';
    }
    else {

        if (($listview_recentindex + listview_questions) <= $recent_countque) {
            $more = $listview_recentindex + listview_questions;
        }
        else {
            $more = $recent_countque;
        }

        for ($i=$listview_recentindex; $i<$more; $i++)
        {
                $recent_html .= '
                     <!-- ** LIST VIEW STRUCTURE MOST VIEW ** -->
                    <div class="container">
                        <div class="table-responsive">
                            <table class="table table-striped custom_table">
                                <tbody>
                                    <tr>
                                        <td style="width:5%;">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a onClick="addVote('.$recent_sectionquery[$i]['postid'].');">
                                                        <i class="fa fa-angle-double-up"></i>
                                                        <span id=span-'.$recent_sectionquery[$i]['postid'].'> '.$recent_sectionquery[$i]['upvotes'].'</span>
                                                    </a>
                                                </li>
                                                <li>
                                                     <a onClick="downVote('.$recent_sectionquery[$i]['postid'].');">
                                                        <i class="fa fa-angle-double-down"></i>
                                                        <span id=span-'.$recent_sectionquery[$i]['postid'].'-downvote> '.$recent_sectionquery[$i]['downvotes'].'</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </td>

                                        <td colspan="4" style="width:70%;">
                                        <h3 style="padding:0; margin:0;">';

                                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $recent_sectionquery[$i]["postid"] . "/" . preg_replace("/[\s_]/", "-", $recent_sectionquery[$i]["title"]);

                                        $recent_html .= '<a href="' . $ref . '" style="color:black;">' . $recent_sectionquery[$i]['title'] . '</a></h3>
                                            <p style="margin:0 0 5px 0; font-size:90%;">asked <strong>' . date("M d", strtotime($recent_sectionquery[$i]['created'])) . '</strong> in <strong>' . $recent_sectionquery[$i]['cattitle'] . '</strong> </p>
                                            <p id="more" style="padding:0; margin:0;">
                                                <span class="more">';

                        //start answer section
                                if ($recent_queryans[$i] != NULL) {
                                    $recent_html .= 'Last Answer: ' . $recent_queryans[$i][0]['content'];
                                }
                                else {
                                    $recent_html .= 'No Answer';
                                }
                        //end answer section
                                
                    $recent_html .= '</span></p>';

                    $relatedqlink = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/ask?follow=' . $recent_sectionquery[$i]['postid'] . '&cat=' . $recent_sectionquery[$i]['categoryid'];
                            
                    $recent_html .= '</p>
                                            <ul class="list-inline">
                                                    <li class="active">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-pencil"></i>
                                                            <span>                                                           
                                                               <a href=' . $ref . '>Answer</a>
                                                             </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                                <i class="fa fa-bars"></i>
                                                            <span>
                                                                <a href=' . $relatedqlink . '>Ask related question</a>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <div class="tooltip-social">
                                                            <a href="javascript:void(0);">Share</a>
                                                            <div class="tooltip-inner">
                                                                <div class="social_icon facebook_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-twitter fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon google_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-google fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon instagram_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-envelope fa-2x"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>                                                          
                                                    </li>
                                                </ul>
                                    </td>
                                    <td style="width:10%;">
                                            ' . ShowWantAnswer($recent_countans[$i], $recent_sectionquery[$i]['postid'], $ref) . '
                                        <div class="tooltip-social">
                                            <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner">
                                                    <a href="#">Invite authoritative user to answer</a><br>
                                                    <a href="#">Add to Reading List</a><br>
                                                    <a href="#">Show Related Questions</a><br>
                                                    <a href="#">Create Summary Answer</a><br>
                                                    <a href="#">Mark Favorite</a><br>
                                                    <a href="#">Report</a>
                                                </div>
                                        </div>
                                    </td>
                                    <td style="width:10%;">
                                        <a href="javascript:void(0)" class="btn btn-red">
                                            <i class="fa fa-circle"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
                </div>';

            $listview_recentindex += 1;
        }
    }
    $recent_listview_firsttime = "FALSE";
       
    $recent_html .= '</div></div></div></div>';

    $recent_html .= '<input type="hidden" id="recent_countque" value="' . $recent_countque . '">
                       <input type="hidden" id="recent_index" value="' . $listview_recentindex . '">
                       <input type="hidden" id="recent_listview_firsttime" value="' . $recent_listview_firsttime . '">';

    $recent_html .= '<script src="' . $rooturl . 'js/readmore.js"></script>';

    echo $recent_html;
}

elseif ($_REQUEST['type'] == 'allque')
{
    $allque_html .= '<div id="allthis-week-res">
                        <div id="allthis-week">';

    if ($_REQUEST['name'] == 'allquestions')
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);

        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($allque_queryans[$i] != NULL) {
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        $listview_allqueindex = 0;
    }

    elseif ($_REQUEST['name'] == 'allqueweek')
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 WEEK ) ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);

        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM qa_posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($allque_queryans[$i] != NULL) {
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        $listview_allqueindex = 0;
    }

    elseif ($_REQUEST['name'] == 'allquemonth')
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 MONTH ) ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);

        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM qa_posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($allque_queryans[$i] != NULL) {                
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        $listview_allqueindex = 0;
    }

    elseif ($_REQUEST['name'] == 'allqueyear')
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND created > DATE_SUB( NOW( ), INTERVAL 1 YEAR ) ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);

        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM qa_posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($allque_queryans[$i] != NULL) {
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        $listview_allqueindex = 0;
    }

    elseif ($_REQUEST['name'] == 'allquecategory')
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' AND ^posts.categoryid = '" . $_REQUEST['catname'] . "' ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);

        for($i=0; $i<$allque_countque; $i++)
        { 
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
            if ($allque_queryans[$i] != NULL) {                
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        $listview_allqueindex = 0;
    }

    elseif (($_REQUEST['name'] == 'more') && ($allque_listview_firsttime == "TRUE"))
    {
        $allque_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created ASC LIMIT 120"));
        $allque_countque = sizeof($allque_sectionquery);

        for($i=0; $i<$allque_countque; $i++)
        {
            $allque_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $allque_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($allque_queryans[$i] != NULL) {
                $allque_countans[$i] = sizeof($allque_queryans[$i]);
            }
            else {
                $allque_countans[$i] = 0;
            }
        }
        $listview_allqueindex = listview_questions;
    }

    if ($allque_countque == 0) {
        $allque_html .= '<div class="noqueans">No questions and answers available</div>';
    }
    else {

        if (($listview_allqueindex + listview_questions) <= $allque_countque) {
            $more = $listview_allqueindex + listview_questions;
        }
        else {
            $more = $allque_countque;
        }

        for ($i=$listview_allqueindex; $i<$more; $i++)
        {
                $allque_html .= '
                     <!-- ** LIST VIEW STRUCTURE MOST VIEW ** -->
                    <div class="container">
                        <div class="table-responsive">
                            <table class="table table-striped custom_table">
                                <tbody>
                                    <tr>
                                        <td style="width:5%;">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a onClick="addVote('.$allque_sectionquery[$i]['postid'].');">
                                                        <i class="fa fa-angle-double-up"></i>
                                                        <span id=span-'.$allque_sectionquery[$i]['postid'].'> '.$allque_sectionquery[$i]['upvotes'].'</span>
                                                    </a>
                                                </li>
                                                <li>
                                                     <a onClick="downVote('.$allque_sectionquery[$i]['postid'].');">
                                                        <i class="fa fa-angle-double-down"></i>
                                                        <span id=span-'.$allque_sectionquery[$i]['postid'].'-downvote> '.$allque_sectionquery[$i]['downvotes'].'</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </td>

                                        <td colspan="4" style="width:70%;">
                                        <h3 style="padding:0; margin:0;">';

                                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $allque_sectionquery[$i]["postid"] . "/" . preg_replace("/[\s_]/", "-", $allque_sectionquery[$i]["title"]);

                                        $allque_html .= '<a href="' . $ref . '" style="color:black;">' . $allque_sectionquery[$i]['title'] . '</a></h3>
                                            <p style="margin:0 0 5px 0; font-size:90%;">asked <strong>' . date("M d", strtotime($allque_sectionquery[$i]['created'])) . '</strong> in <strong>' . $allque_sectionquery[$i]['cattitle'] . '</strong> </p>
                                            <p id="more" style="padding:0; margin:0;">
                                                <span class="more">';

                        //start answer section
                                if ($allque_queryans[$i] != NULL) {
                                    $allque_html .= 'Last Answer: ' . $allque_queryans[$i][0]['content'];
                                }
                                else {
                                    $allque_html .= 'No Answer';
                                }
                        //end answer section
                                
                    $allque_html .= '</span></p>';

                    $relatedqlink = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/ask?follow=' . $allque_sectionquery[$i]['postid'] . '&cat=' . $allque_sectionquery[$i]['categoryid'];
                            
                    $allque_html .= '</p>
                                            <ul class="list-inline">
                                                    <li class="active">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-pencil"></i>
                                                            <span>                                                           
                                                               <a href=' . $ref . '>Answer</a>
                                                             </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                                <i class="fa fa-bars"></i>
                                                            <span>
                                                                <a href=' . $relatedqlink . '>Ask related question</a>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <div class="tooltip-social">
                                                            <a href="javascript:void(0);">Share</a>
                                                            <div class="tooltip-inner">
                                                                <div class="social_icon facebook_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-twitter fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon google_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-google fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon instagram_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-envelope fa-2x"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>                                                          
                                                    </li>
                                                </ul>
                                    </td>
                                    <td style="width:10%;">
                                            ' . ShowWantAnswer($allque_countans[$i], $allque_sectionquery[$i]['postid'], $ref) . '
                                        <div class="tooltip-social">
                                            <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner">
                                                    <a href="#">Invite authoritative user to answer</a><br>
                                                    <a href="#">Add to Reading List</a><br>
                                                    <a href="#">Show Related Questions</a><br>
                                                    <a href="#">Create Summary Answer</a><br>
                                                    <a href="#">Mark Favorite</a><br>
                                                    <a href="#">Report</a>
                                                </div>
                                        </div>
                                    </td>
                                    <td style="width:10%;">
                                        <a href="javascript:void(0)" class="btn btn-red">
                                            <i class="fa fa-circle"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
                </div>';

            $listview_allqueindex += 1;
        }
    }
    $allque_listview_firsttime = "FALSE";
       
    $allque_html .= '</div></div></div></div>';

    $allque_html .= '<input type="hidden" id="allque_countque" value="' . $allque_countque . '">
                        <input type="hidden" id="allque_index" value="' . $listview_allqueindex . '">
                        <input type="hidden" id="allque_listview_firsttime" value="' . $allque_listview_firsttime . '">';

    $allque_html .= '<script src="' . $rooturl . 'js/readmore.js"></script>';

    echo $allque_html;
}

elseif ($_REQUEST['type'] == 'unans')
{
    $unans_html .= '<div id="allthis-week-res">
                        <div id="allthis-week">';

    if ($_REQUEST['name'] == 'unanswered')
    {
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        $unans_countque = sizeof($unans_sectionquery);

        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($unans_queryans[$i] != NULL) {
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        $listview_unansindex = 0;
    }

    elseif ($_REQUEST['name'] == 'unansweek')
    {
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND created > DATE_SUB( NOW(), INTERVAL 1 WEEK ) AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        $unans_countque = sizeof($unans_sectionquery);

        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($unans_queryans[$i] != NULL) {                
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        $listview_unansindex = 0;
    }

    elseif ($_REQUEST['name'] == 'unansmonth')
    {
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND created > DATE_SUB( NOW(), INTERVAL 1 MONTH ) AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        $unans_countque = sizeof($unans_sectionquery);

        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($unans_queryans[$i] != NULL) {                
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        $listview_unansindex = 0;
    }

    elseif ($_REQUEST['name'] == 'unansyear')
    {
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND created > DATE_SUB( NOW(), INTERVAL 1 YEAR ) AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        $unans_countque = sizeof($unans_sectionquery);

        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($unans_queryans[$i] != NULL) {
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        $listview_unansindex = 0;
    }

    elseif ($_REQUEST['name'] == 'unanscategory')
    {
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND ^posts.categoryid = '" . $_REQUEST['catname'] . "' AND postid NOT IN(select parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        $unans_countque = sizeof($unans_sectionquery);

        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($unans_queryans[$i] != NULL) {
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        $listview_unansindex = 0;
    }

    elseif (($_REQUEST['name'] == 'more') && ($unans_listview_firsttime == "TRUE"))
    {
        $unans_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT 120"));
        $unans_countque = sizeof($unans_sectionquery);

        for($i=0; $i<$unans_countque; $i++)
        {
            $unans_queryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $unans_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            if ($unans_queryans[$i] != NULL) {
                $unans_countans[$i] = sizeof($unans_queryans[$i]);
            }
            else {
                $unans_countans[$i] = 0;
            }
        }
        $listview_unansindex = listview_questions;
    }

    if ($unans_countque == 0) {
        $unans_html .= '<div class="noqueans">No questions and answers available</div>';
    }
    else {

        if (($listview_unansindex + listview_questions) <= $unans_countque) {
            $more = $listview_unansindex + listview_questions;
        }
        else {
            $more = $unans_countque;
        }

        for ($i=$listview_unansindex; $i<$more; $i++)
        {
            $unans_html .= '
                     <!-- ** LIST VIEW STRUCTURE MOST VIEW ** -->
                    <div class="container">
                        <div class="table-responsive">
                            <table class="table table-striped custom_table">
                                <tbody>
                                    <tr>
                                        <td style="width:5%;">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a onClick="addVote('.$unans_sectionquery[$i]['postid'].');">
                                                        <i class="fa fa-angle-double-up"></i>
                                                        <span id=span-'.$unans_sectionquery[$i]['postid'].'> '.$unans_sectionquery[$i]['upvotes'].'</span>
                                                    </a>
                                                </li>
                                                <li>
                                                     <a onClick="downVote('.$unans_sectionquery[$i]['postid'].');">
                                                        <i class="fa fa-angle-double-down"></i>
                                                        <span id=span-'.$unans_sectionquery[$i]['postid'].'-downvote> '.$unans_sectionquery[$i]['downvotes'].'</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </td>

                                        <td colspan="4" style="width:70%;">
                                        <h3 style="padding:0; margin:0;">';

                                        //Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
                                        $ref = str_replace($rooturl, "?qa=", $rooturl) . $unans_sectionquery[$i]["postid"] . "/" . preg_replace("/[\s_]/", "-", $unans_sectionquery[$i]["title"]);

                                        $unans_html .= '<a href="' . $ref . '" style="color:black;">' . $unans_sectionquery[$i]['title'] . '</a></h3>
                                            <p style="margin:0 0 5px 0; font-size:90%;">asked <strong>' . date("M d", strtotime($unans_sectionquery[$i]['created'])) . '</strong> in <strong>' . $unans_sectionquery[$i]['cattitle'] . '</strong> </p>
                                            <p id="more" style="padding:0; margin:0;">
                                                <span class="more">';

                        //start answer section
                                if ($unans_queryans[$i] != NULL) {
                                    $unans_html .= 'Last Answer: ' . $unans_queryans[$i][0]['content'];
                                }
                                else {
                                    $unans_html .= 'No Answer';
                                }
                        //end answer section
                                
                    $unans_html .= '</span></p>';

                    $relatedqlink = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/ask?follow=' . $unans_sectionquery[$i]['postid'] . '&cat=' . $unans_sectionquery[$i]['categoryid'];
                            
                    $unans_html .= '</p>
                                            <ul class="list-inline">
                                                    <li class="active">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-pencil"></i>
                                                            <span>                                                           
                                                               <a href=' . $ref . '>Answer</a>
                                                             </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                                <i class="fa fa-bars"></i>
                                                            <span>
                                                                <a href=' . $relatedqlink . '>Ask related question</a>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <div class="tooltip-social">
                                                            <a href="javascript:void(0);">Share</a>
                                                            <div class="tooltip-inner">
                                                                <div class="social_icon facebook_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-twitter fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon google_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-google fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon instagram_icon">
                                                                    <a href="javascript:void(0);"><i class="fa fa-envelope fa-2x"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>                                                          
                                                    </li>
                                                </ul>
                                    </td>
                                    <td style="width:10%;">
                                            ' . ShowWantAnswer($unans_countans[$i], $unans_sectionquery[$i]['postid'], $ref) . '
                                        <div class="tooltip-social">
                                            <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner">
                                                    <a href="#">Invite authoritative user to answer</a><br>
                                                    <a href="#">Add to Reading List</a><br>
                                                    <a href="#">Show Related Questions</a><br>
                                                    <a href="#">Create Summary Answer</a><br>
                                                    <a href="#">Mark Favorite</a><br>
                                                    <a href="#">Report</a>
                                                </div>
                                        </div>
                                    </td>
                                    <td style="width:10%;">
                                        <a href="javascript:void(0)" class="btn btn-red">
                                            <i class="fa fa-circle"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
                </div>';

            $listview_unansindex += 1;
        }
    }
    $unans_listview_firsttime = "FALSE";

    $unans_html .= '</div></div></div></div>';

    $unans_html .= '<input type="hidden" id="unans_countque" value="' . $unans_countque . '">
                    <input type="hidden" id="unans_index" value="' . $listview_unansindex . '">
                    <input type="hidden" id="unans_listview_firsttime" value="' . $unans_listview_firsttime . '">';

    $unans_html .= '<script src="' . $rooturl . 'js/readmore.js"></script>';
                      
    echo $unans_html;
}

    break;

    default: echo "ERROR SWITCH";
}

    $_SESSION['most_sectionquery'] = $most_sectionquery;
    $_SESSION['most_queryans'] = $most_queryans;
    $_SESSION['most_countans'] = $most_countans;

    $_SESSION['recent_sectionquery'] = $recent_sectionquery;
    $_SESSION['recent_queryans'] = $recent_queryans;
    $_SESSION['recent_countans'] = $recent_countans;

    $_SESSION['allque_sectionquery'] = $allque_sectionquery;
    $_SESSION['allque_queryans'] = $allque_queryans;
    $_SESSION['allque_countans'] = $allque_countans;

    $_SESSION['unans_sectionquery'] = $unans_sectionquery;
    $_SESSION['unans_queryans'] = $unans_queryans;
    $_SESSION['unans_countans'] = $unans_countans;

    $_SESSION['most_index'] = $most_index;
    $_SESSION['most_countque'] = $most_countque;
    $_SESSION['most_slide_index'] = $most_slide_index;
    $_SESSION['most_total_slides'] = $most_total_slides;
    $_SESSION['most_firsttime'] = $most_firsttime;
    $_SESSION['most_listview_firsttime'] = $most_listview_firsttime;

    $_SESSION['recent_index'] = $recent_index;
    $_SESSION['recent_countque'] = $recent_countque;
    $_SESSION['recent_slide_index'] = $recent_slide_index;
    $_SESSION['recent_total_slides'] = $recent_total_slides;
    $_SESSION['recent_firsttime'] = $recent_firsttime;
    $_SESSION['recent_listview_firsttime'] = $recent_listview_firsttime;

    $_SESSION['allque_index'] = $allque_index;
    $_SESSION['allque_countque'] = $allque_countque;
    $_SESSION['allque_slide_index'] = $allque_slide_index;
    $_SESSION['allque_total_slides'] = $allque_total_slides;
    $_SESSION['allque_firsttime'] = $allque_firsttime;
    $_SESSION['allque_listview_firsttime'] = $allque_listview_firsttime;

    $_SESSION['unans_index'] = $unans_index;
    $_SESSION['unans_countque'] = $unans_countque;
    $_SESSION['unans_slide_index'] = $unans_slide_index;
    $_SESSION['unans_total_slides'] = $unans_total_slides;
    $_SESSION['unans_firsttime'] = $unans_firsttime;
    $_SESSION['unans_listview_firsttime'] = $unans_listview_firsttime;

    $_SESSION['listview_mostindex'] = $listview_mostindex;
    $_SESSION['listview_recentindex'] = $listview_recentindex;
    $_SESSION['listview_allqueindex'] = $listview_allqueindex;
    $_SESSION['listview_unansindex'] = $listview_unansindex;

?>