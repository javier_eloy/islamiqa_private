<?php

require_once QA_INCLUDE_DIR . 'qa-db.php';
require_once QA_INCLUDE_DIR . 'app/users.php';
require_once QA_INCLUDE_DIR . 'app/options.php';


function calculate_weeks_ago($created_date)
{	
	$today_in_weeks = date("W", strtotime(date('Y-m-d H:m:s')));
	$created_in_weeks = date("W", strtotime($created_date));

	$current_year = date("o", strtotime(date('Y-m-d H:m:s')));
	$created_year = date("o", strtotime($created_date));

	if ($current_year === $created_year) {
		$ago = $today_in_weeks - $created_in_weeks;
	}
	elseif ($current_year > $created_year) {
		$years_diff = $current_year - $created_year;
		$weeks_diff = 53 * $years_diff;
		$ago = ($weeks_diff - $created_in_weeks) + $today_in_weeks;
	}
	else {
		echo "ERROR";
		return null;
	}
	return $ago;
}


function ShowWantAnswer($answercount, $postid = 0, $ref = "")
{
   // $ref = str_replace($context->rooturl, "?qa=", $context->rooturl) . $postid . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);
    $wantbutton = "
        <p class='table-countans'>".$answercount."</p>
            <a href='".$ref."'><p class='table-want-answers'>answers</p></a>
        </p>";

    if ($answercount > 0) {
        $wantbutton = "
        <p  class='table-countans'>".$answercount."</p>
            <a href='".$ref."'><p class='table-want-answers'>answers</p></a>
        </p>";
        
    }
    else {        
        $userid = qa_get_logged_in_userid();
        
        if (!isset($userid))
            $userid = 0;
        
        $wantanswer = qa_opt('plugin_want_answer');
        
        if ($wantanswer) {
            $linkwantanswer = "";

            if ($userid > 0) {
                $linkwantanswer = "<a href='#' onclick = 'wantAnswerClick(this, ".$userid.", ".$postid.");return false;'>";
            }
            $query = "SELECT COUNT(postid) as count FROM ^userwantanswer WHERE postid = #";
            $result = qa_db_read_one_assoc(qa_db_query_sub($query, $postid ), true);
        
            $count = $result['count'];
        
            $wantbutton = "<p class='table-countans'>".$count."</p>
                            $linkwantanswer                
                            <p class='table-want-answers'>want</br>
                            answers</p>
                            </p>";
            }
        }
        
     return $wantbutton;
}

  /*********************************************************************
  *** Function: Convert html path into link and select if requested
  ***
  *********************************************************************/
  
  function qa_link_selected($path,$text,$selected) {
	  $htmlpath=qa_path_html($path);
      $style= ($path == $selected) ?  "style='font-weight:900; color:#313131'" : "";
	   
	  return "<a href='{$htmlpath}' {$style} > {$text} </a>";
  }
?>