<?php
/*
 * Created by Julio Velasquez
 * Date: 13/06/2016
 * Time: 16:45
 */
/*
 * Updated by Julio A. Velasquez Basabe
 * Date: 04/07/2016
 * Time: 17:48
 */

require_once '../../qa-include/qa-base.php';
require_once QA_INCLUDE_DIR . 'db/selects.php';


  $search = $_GET['term'];
  $result = array();
  $data = array();

  $sql = "SELECT postid, title FROM qa_posts WHERE type='Q' AND title LIKE '%" . $search . "%' ORDER BY created ASC";
  $result = qa_db_read_all_assoc(qa_db_query_sub($sql));

  foreach($result as $r) {
   $data[] = array("value" => $r['postid'], "label" => $r['title']);
  }
  echo json_encode($data);


/*
		$search = $_GET['term'];
		$query = "SELECT postid, title FROM ^posts WHERE type='Q' AND title LIKE '%" . $search . "%' ORDER BY created ASC";
/*
		$result = qa_db_read_all_assoc(qa_db_query_sub($query));
		$json_data = json_encode($result, JSON_PRETTY_PRINT);
		echo $json_data;
*/
/*
		$result = qa_db_query_sub($query);
		$data = array();
		while ($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
//			$data[] = array("postid" => $row["postid"], "title" => $row["title"]);
        	array_push($data, utf8_encode($row['title']));
		}
		$json_data = json_encode($data, JSON_PRETTY_PRINT);
		echo $json_data;
*/

?>