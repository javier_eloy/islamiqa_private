<?php

require_once '../../qa-include/qa-base.php';
require_once QA_INCLUDE_DIR . 'qa-app-users.php';
require_once QA_INCLUDE_DIR . 'qa-app-posts.php';


$type = 'Q'; // question
$parentid = NULL; // does not follow another answer
$format = ''; // plain text
$category = $_REQUEST['cat_array']; //array of categories selected from new question dialog
$settings = $_REQUEST['set_array']; //array of settings selected from new question dialog
$tags = $_REQUEST['tag_array']; //array of tags added from selected from new question dialog
$userid = NULL; //in case that the new question is set with option "post anonymously"

$fld_array = $_REQUEST['fld_array'];
$title = $fld_array[0];
$content = $fld_array[1];

$string_tags = qa_tags_to_tagstring($tags);

$answer_anonymously = $settings[0];
$email_notif = $settings[1];
$post_anonymously = $settings[2];


if ($post_anonymously != NULL) {
	//create the new question in a anonymously way, the userid is null
	$result['postid'] = qa_post_create($type, $parentid, $title, $content, $format, $category, $string_tags, $userid);
	$success = true;
}
else {
	//check if an user is logged in
	$islogged = qa_is_logged_in();
	if( $islogged )
	{
		//create the new question with the userid of the user logged in
		$userid = qa_get_logged_in_userid();
		$result['postid'] = qa_post_create($type, $parentid, $title, $content, $format, $category, $string_tags, $userid);
		$success = true;
	}
	//if there isn't an user loggedin don't create the new question and return unsuccess
	else $success = false;
}


//$islogged = qa_is_logged_in();
//$success = true;

//if( $islogged ) {
//	$userid = qa_get_logged_in_userid();
//	$result['postid'] = qa_post_create($type, $parentid, $title, $content, $format, $category, $tag, $userid);
//}
//else {
//	$success = false;
//}

$result['success'] = $success;
echo json_encode($result);


?>