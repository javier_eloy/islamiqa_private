<?php

require_once '../../qa-include/qa-base.php';
require_once QA_INCLUDE_DIR . 'qa-app-users.php';


$data = islogged();
echo json_encode($data);


function islogged()
{
	if( qa_is_logged_in() ) {
		$result['islogged'] = TRUE;
		$result['userid'] = qa_get_logged_in_userid();
	}
	else {
		$result['islogged'] = FALSE;
	}
	return $result;
}


?>