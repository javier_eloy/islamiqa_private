<!doctype html>
<html lang='en'>
	<head>
		<meta charset='utf-8' />
		<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
		
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<meta name='viewport' content='width=device-width' />

		<title>Sssoon Page </title>
		
		<link href='coming-soon/bootstrap.css' rel='stylesheet' />
		<link href='coming-soon/coming-sssoon.css' rel='stylesheet' />    
		
		<!--     Fonts     -->
		<link href='http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css' rel='stylesheet'>
		<link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
<script data-cfasync="false" type="text/javascript" src="//filamentapp.s3.amazonaws.com/0e65c3ddf6ba364917255ec2ab2b217c.js" async="async"></script>
	</head>
	<body>
<!-- HE NEED SEO AND METAS -->
	<div class="main" style="background: url('coming-soon/pattern.gif') repeat">

	<!--    Change the image source '/images/default.jpg' with your favourite image.     -->

<!--		<div class="cover black" data-color="black"></div> -->
		 
	<!--   You can change the black color for the filter with those colors: blue, green, red, orange       -->

		<div class="container">
			<h1 class="logo cursive">
				Coming Soon
			</h1>
	<!--  H1 can have 2 designs: "logo" and "logo cursive"           -->
			
			<div class="content">
				<h4 class="motto">Questions and answers about Islam and its peoples...</h4>
				<div class="subscribe">
					<h5 class="info-text">
						Join the waiting list for the beta. We will keep you posted. 
					</h5>
					<div class="row">
						<div class="col-md-4 col-md-offset-4 col-sm6-6 col-sm-offset-3 ">
							<form class="form-inline" role="form" method="post">
							  <div class="form-group">
								<label class="sr-only" for="exampleInputEmail2">Email address</label>
								<input type="email" name="email" class="form-control transparent" placeholder="Your email here...">
							  </div>
							  <button type="submit" class="btn btn-danger btn-fill">Notify Me</button>
							</form>
						</div>
					</div>

				<?php
					// define variables and set to empty values

					if ($_SERVER["REQUEST_METHOD"] == "POST") {
						if (empty($_POST["email"])) {
							echo '<h5 class="info-text">Email is required</h5>';
						} else {
							$email = $_POST["email"];
							
							// check if e-mail address is well-formed
							if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
								echo '<h5 class="info-text">Invalid email format</h5>'; 
							} else {
								//Email information
								$admin_email = "delstoneservices@gmail.com";
								$subject = "Beta Test User";
								$comment = "I am interested!";
						  
								//send email
								if (mail($admin_email, $subject, $comment, "From:" . $email)) {
									echo '<h5 class="info-text">Your message has been sent.</h5>'; 
								} else {
									echo '<h5 class="info-text">Something went wrong, go back and try again!</h5>'; 	  
								}
								//Email response
								echo '<h5 class="info-text"> Thank you for contacting us. </h5>';
							}
						}
					}
				?>
				
				</div>
			</div>
		</div>
	</div>
	</body>
	   <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
	   <script src="js/bootstrap.min.js" type="text/javascript"></script>	   
	</html>
