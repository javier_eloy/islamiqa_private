<?php
// Remember to comment out welcome notification email qa_send_notification() of qa_create_new_user().
	require_once 'qa-include/qa-base.php'; 
	require_once 'qa-include/qa-db-users.php'; 
	require_once 'qa-include/qa-app-users-edit.php';
	require_once 'qa-include/qa-app-users.php';
	require_once 'qa-include/qa-app-posts.php';
	require_once 'qa-include/qa-db-admin.php'; // categories

	// Lists
	$users         = array("Alan","Cohen","Colin","David","Greg","Jack","Joseph","Paul","Sam","Anna","Claire","Julie","Samantha","Sharon","Tracy","AbdurRahman","Abumalik","Ahsan","Ali","Arif","Asif","Ata","Bilal","Clarke","Eesa","Eustachy","Halimi","Hamzah","Hasan","Hasim","Mazin","Moazzem","Mohammed","Nadeem","Nadeem","Roshan","Shah","Shahid","Sharif","Shehzeb","Shoaib","Sy","Tariq","Yousef","Zaf","Zubear","Aisha","Asiya","Fatima","Laila","Mahbuba");
	$passwords     = array("Alan_123","Cohen_123","Colin_123","David_123","Greg_123","Jack_123","Joseph_123","Paul_123","Sam_123","Anna_123","Claire_123","Julie_123","Samantha_123","Sharon_123","Tracy_123","AbdurRahman_123","Abumalik_123","Ahsan_123","Ali_123","Arif_123","Asif_123","Ata_123","Bilal_123","Clarke_123","Eesa_123","Eustachy_123","Halimi_123","Hamzah_123","Hasan_123","Hasim_123","Mazin_123","Moazzem_123","Mohammed_123","Nadeem_123","Nadeem_123","Roshan_123","Shah_123","Shahid_123","Sharif_123","Shehzeb_123","Shoaib_123","Sy_123","Tariq_123","Yousef_123","Zaf_123","Zubear_123","Aisha_123","Asiya_123","Fatima_123","Laila_123","Mahbuba_123");

	$categories    = array("Beliefs","Culture","Family","History","Sciences","Politics","People","Places","Languages","Organisations","Groups and Movements");
	$subcategories = array( 
					"History"=>array("1"=>"Prior Messengers","2"=>"Pre-Islam","3"=>"Seerah","4"=>"Khulafah Rashida","5"=>"Umayyads","6"=>"Abbasids","7"=>"Ottomans","8"=>"Nation States","9"=>"Others"),
					"Culture"=>array("1"=>"Quran","2"=>"Hadith","3"=>"Fiqh","4"=>"Usul al-Fiqh","5"=>"Kalam","6"=>"Falsafa","7"=>"Others"),
					"Places"=>array("1"=>"Europe","2"=>"Americas","3"=>"Africa","4"=>"Asia","5"=>"Australasia"),
					"Languages"=>array("1"=>"Arabic","2"=>"Malay","3"=>"Persian","4"=>"Turkish","5"=>"Urdu","6"=>"Others"),
					"Organisations"=>array("1"=>"Schools","2"=>"Universities","3"=>"Charities","4"=>"Businesses","5"=>"Media","6"=>"Prisons","7"=>"Government Offices"),
					"Groups and Movements"=>array("1"=>"al-Qaida","2"=>"Boko Haram","3"=>"Gulen","4"=>"Hamas","5"=>"Hizb ut-Tahrir","6"=>"Hizbullah","7"=>"Ikhwan al-Muslimoon","8"=>"ISIS","9"=>"Jamati Islami","10"=>"Minhaj al-Quran","11"=>"Nation of Islam","12"=>"Others","13"=>"Taliban","14"=>"Tehreek Insaaf"),
					 );
	
	// Create UserIds
	echo "<b>Processing...<br/><br/></b>";
	for($i = 0 ; $i < count($users) ; $i++) { 
	  echo "Processing email no $i $users[$i] : ";
	   
	  $handle   = $users[$i];
	  //$email    = 'info@delstoneservices.co.uk'; 
	  //$email    = $users[$i] . '@example.com'; 
	  $email    = 'info@delstoneservices.co.uk'; 
	  $password = $passwords[$i]; 
	  
	  if (qa_handle_to_userid($handle) > 0) {
		echo $handle . " already exists <br/>";
	  } else {
		$userid = qa_create_new_user($email, $password, $handle); 
		qa_db_user_set_flag($userid, QA_USER_FLAGS_EMAIL_CONFIRMED, true);
		echo "created successfully <br/>";
	  }
	}
	
	echo "<b>Completed User Creation<br/><br/></b>";
	
	
	// Create Categories	
	for($i = 0 ; $i < count($categories) ; $i++) {
		echo "Processing category $i $categories[$i] : ";
	
		$category         = $categories[$i];
		$categories[$i]   = strtolower(str_replace(' ', '-', $categories[$i]));
		
		$id = qa_db_read_one_value(qa_db_query_sub('SELECT categoryid FROM ^categories WHERE parentid<=># AND title=$',NULL, $category), true);
		
		if ($id > 0) {
			echo $categories[$i] . " $id already exists <br/>";
		} else {
			$categoryid = qa_db_category_create(NULL, $category, $categories[$i]);
			echo "created successfully <br/>";
		}
	}
		
	echo "<b>Completed Category Creation<br/></b>";
	

	// Create Sub-Categories	
	$ctr = count($subcategories);
	foreach($subcategories as $x=>$x_value) {
		if ($x_value == "") {
			echo $x." : no subcategories...<br/>";
			continue;
		}
		echo "Processing subcategory $x : ";
			
		foreach($x_value as $y=>$y_value) {
			$subcategory         = $y_value;
			$y_value		     = strtolower(str_replace(' ', '-', $y_value));

			$parentid = qa_db_read_one_value(qa_db_query_sub('SELECT categoryid FROM ^categories WHERE title=$', $x), true);
			$id       = qa_db_read_one_value(qa_db_query_sub('SELECT categoryid FROM ^categories WHERE parentid=# AND tags=$', $parentid, $y_value), true);
			
			if ($id > 0) {
				echo $x."-".$y_value . " $id already exists <br/>";
			} else {
				$categoryid = qa_db_category_create($parentid, $subcategory, $y_value);
				echo "$y_value created successfully <br/>";
			}
		}
	}

	echo "<b>Completed Sub-Category Creation<br/></b>";

?>