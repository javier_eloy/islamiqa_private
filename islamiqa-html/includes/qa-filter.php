<!-- ** START QUESTIONS & ANSWERS FILTER ** -->
<div class="container">
	<div class="row">
		<div class="qa_filter">
			<div class="col-md-7">
				<ul class="type_filter">
					<li class="for_next_sections">
						<a href="#top"><i class="fa fa-caret-up"></i></i></a>
						<a href="#one"><i class="fa fa-caret-down"></i></i></a>
					</li>
					<li><a class="active_section" href="javascript:void(0);">Most Popular</a></li>
					<li><a href="javascript:void(0);">This Week</a></li>
					<li><a href="javascript:void(0);">This Month</a></li>
					<li><a href="javascript:void(0);">This Year</a></li>
				</ul>
			</div>

			<div class="col-md-5">
				<ul class="filter">
					<li>View</li>
					<li><a href="javascript:void(0);"><i class="fa fa-align-right"></i> <span class="icon"></span> </a></li>
					<li><a href="javascript:void(0);"><i class="fa fa-th-large active"></i></span> </a></li>
					<li>
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								Categories <i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
								<li><a href="#">Category 1</a></li>
								<li><a href="#">Category 2</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- ** ENDS QUESTIONS & ANSWERS FILTER ** -->
