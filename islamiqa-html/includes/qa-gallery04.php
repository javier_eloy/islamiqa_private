!-- ** START PORTFOLIO GRID GALLERY ** -->
<div class="qa_gallery first">
	<div class="next_prev">
		<a href="javascript:void(0)" class="prev"><i class="fa fa-angle-left"></i></a>
		<a href="javascript:void(0)" class="next"><i class="fa fa-angle-right"></i></a>
	</div>
	<?php include "includes/qa-filter.php";?>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="gallery_four_thumb_left">

						<div class="thumb_full">
							<div class="thumb_hover">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">4w ago</span>
											<span class="topic right">People</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line">
											<div class="h5">Why is the Islamic State not our biggest problem?</div>
											<div class="toolbar">
												<div class="tools left">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														<li><a href="javascript:void(0);">3k views</a></li>
														<li><a href="javascript:void(0);">235 comments</a></li>
														<li><a href="javascript:void(0);">Share</a></li>
													</ul>
												</div>
												<div class="share right">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-ellipsis-h"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="thumb"><img src="/images/content/qa-gallery/tumblr_static_8o6gzjc8x2o88kcskw80884cg_640_v2.jpg" alt="img"></div>
							</div>
						</div>

						<div class="thumb_full">
							<div class="thumb_hover">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">4w ago</span>
											<span class="topic right">People</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line">
											<div class="h5">Why is the Islamic State not our biggest problem?</div>
											<div class="toolbar">
												<div class="tools left">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														<li><a href="javascript:void(0);">3k views</a></li>
														<li><a href="javascript:void(0);">235 comments</a></li>
														<li><a href="javascript:void(0);">Share</a></li>
													</ul>
												</div>
												<div class="share right">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-ellipsis-h"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="thumb"><img src="/images/content/qa-gallery/tumblr_static_8o6gzjc8x2o88kcskw80884cg_640_v2.jpg" alt="img"></div>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row">
					<div class="gallery_four_thumb_right">

						<div class="thumb_full">
							<div class="thumb_hover">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">4w ago</span>
											<span class="topic right">People</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line">
											<div class="h5">Why is the Islamic State not our biggest problem?</div>
											<div class="toolbar">
												<div class="tools left">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														<li><a href="javascript:void(0);">3k views</a></li>
														<li><a href="javascript:void(0);">235 comments</a></li>
														<li><a href="javascript:void(0);">Share</a></li>
													</ul>
												</div>
												<div class="share right">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-ellipsis-h"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="thumb"><img src="/images/content/qa-gallery/tumblr_static_8o6gzjc8x2o88kcskw80884cg_640_v4.jpg" alt="img"></div>
							</div>
						</div>

						<div class="thumb_full">
							<div class="thumb_hover">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">4w ago</span>
											<span class="topic right">People</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line">
											<div class="h5">Why is the Islamic State not our biggest problem?</div>
											<div class="toolbar">
												<div class="tools left">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														<li><a href="javascript:void(0);">3k views</a></li>
														<li><a href="javascript:void(0);">235 comments</a></li>
														<li><a href="javascript:void(0);">Share</a></li>
													</ul>
												</div>
												<div class="share right">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-ellipsis-h"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="thumb"><img src="/images/content/qa-gallery/tumblr_static_8o6gzjc8x2o88kcskw80884cg_640_v3.jpg" alt="img"></div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ** ENDS PORTFOLIO GRID GALLERY ** -->
