<!-- ** START CONTACT US SECTION ** -->
<section class="contact_us">
	<div class="container">
		<div class="row text-center">
			<div class="h1">Get in Touch</div>
			<p>If you are looking for a solid partner for your projects, send us an email. <br>We'd love to talk to you!</p>
		</div>
	</div>

	<div class="dt-hr-invisible-smaller"></div>

	<div class="contact_box clearfix">
		<div class="dt-hr-invisible-small"></div>
		<div class="container">
			<div class="row text-center">
				<div class="form_container_inner">
					<?php include_once "contact_form.php";?>
				</div>
			</div>
		</div>
		<div class="dt-hr-invisible-smaller"></div>
	</div>
</section>
<!-- ** ENDS CONTACT US SECTION ** -->
