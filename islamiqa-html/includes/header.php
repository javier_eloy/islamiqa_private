<!-- ** START HEADER ** -->
<header id="top">
	<nav class="web_nav">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<div class="navbar-header">
						<a class="navbar-brand" href="/">
							<img src="images/ui/logo.png" alt="logo">
						</a>
					</div>
					<ul class="slimmenu">
						<li><a href="javascript:void(0);"><i class="fa fa-home"></i></a></li>
						<li><a href="javascript:void(0)">Popular</a></li>
						<li><a class="active" href="javascript:void(0)">Most Recent</a></li>
						<li><a href="javascript:void(0)">All Questions</a></li>
						<li><a href="javascript:void(0);">Unanswered</a></li>
					</ul>
				</div>

				<div class="col-md-3">
					<div class="form-group top_search">
						<form role="search">
							<input type="text" class="form-control" placeholder="Search">
							<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>

				<div class="col-md-2">
					<?php include_once "includes/login-signup.php";?>
				</div>
			</div>
		</div>
	</nav>
</header>
<!-- ** END HEADER ** -->
