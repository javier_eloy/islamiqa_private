<!-- ** START PORTFOLIO GRID GALLERY ** -->
<section class="portfolio">
	<div class="dt-hr-invisible-small"></div>
	<div class="container">
		<div class="row">
			<div class="head text-center">
				<div class="h1">OUR WORK</div>
				<p>Here are just a few of the projects we’ve lovingly crafted…</p>
			</div>
		</div>
	</div>
	<div class="dt-hr-invisible-small"></div>
	<div class="main">
		<ul id="og-grid" class="og-grid">
			<li>
				<a href="mars-translation.php" data-largesrc="images/content/portfolio/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
					<img src="images/content/portfolio/thumbs/1.jpg" alt="img01"/>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" data-largesrc="images/content/portfolio/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
					<img src="images/content/portfolio/thumbs/2.jpg" alt="img02"/>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" data-largesrc="images/content/portfolio/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
					<img src="images/content/portfolio/thumbs/3.jpg" alt="img03"/>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" data-largesrc="images/content/portfolio/4.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
					<img src="images/content/portfolio/thumbs/4.jpg" alt="img04"/>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" data-largesrc="images/content/portfolio/5.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
					<img src="images/content/portfolio/thumbs/5.jpg" alt="img05"/>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" data-largesrc="images/content/portfolio/6.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
					<img src="images/content/portfolio/thumbs/6.jpg" alt="img06"/>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" data-largesrc="images/content/portfolio/7.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
					<img src="images/content/portfolio/thumbs/7.jpg" alt="img07"/>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" data-largesrc="images/content/portfolio/8.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
					<img src="images/content/portfolio/thumbs/8.jpg" alt="img08"/>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" data-largesrc="images/content/portfolio/9.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
					<img src="images/content/portfolio/thumbs/9.jpg" alt="img03"/>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" data-largesrc="images/content/portfolio/10.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
					<img src="images/content/portfolio/thumbs/10.jpg" alt="img10"/>
				</a>
			</li>
		</ul>
	</div>
	<div class="dt-hr-invisible-large"></div>
</section>
<!-- ** ENDS PORTFOLIO GRID GALLERY ** -->
