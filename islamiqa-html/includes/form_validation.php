<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/vendor/jquery.cycle2.min.js"></script>
<script type="text/javascript">
jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value);
    }, "type the correct answer -_-");

// validate contact form
$( "#sc_contact_form" ).submit(function( event ) {
  //alert( "Handler for .submit() called." );
  event.preventDefault();
});

$(function() {
    $('form').validate({
        rules: {
            full_name: {
                required: true,
                minlength: 2
            },
            email_address: {
                required: true,
                email: true
            },
            phone: {
                required: true
            },
            subject: {
                required: true,
            }
        },
        messages: {
            full_name: {
                required: "your full name.",
                minlength: "your name must consist of at least 2 characters"
            },
            phone: {
                required: "your phone no. is required",
                minlength: "so that we can get back to you"
            },			
            email_address: {
                required: "your email where we can reply you back"
            },
            subject: {
                required: "the reason to contatct us.",
                minlength: "why you are contacting us. "
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                type:"POST",
                data: $(form).serialize(),
                url:"process.php",
                success: function() {
					//alert('we got the data here');
					//return false;
                    //$('#sc_contact_form :input').attr('disabled', 'disabled');
                    //$('#sc_contact_form').fadeTo( "slow", 0.15, function() {
                       // $(this).find(':input').attr('disabled', 'disabled');
                        //$(this).find('label').css('cursor','default');
                       
                    //});
					 //$('#success').fadeIn();
					 $('#success').css("display","block");
					 $('#success').css("background-color","green");
					 $('#success').fadeOut( 3200, "linear", complete );
                },
                error: function() {
                    //$('#sc_contact_form').fadeTo( "slow", 0.15, function() {
                        //$('#error').fadeIn();
						$('#error').css("display","block");
						$('#error').css("background-color","red");
                   // });
                }
            });
        }
    });
});
</script>