<!-- ** START PORTFOLIO GRID GALLERY ** -->
<div class="qa_gallery first">
	<div class="next_prev">
		<a href="javascript:void(0)" class="prev"><i class="fa fa-angle-left"></i></a>
		<a href="javascript:void(0)" class="next"><i class="fa fa-angle-right"></i></a>
	</div>
	<?php include "includes/qa-filter.php";?>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="gallery_four_thumb">

						<div class="clearfix">
							<div class="thumb_half">
								<div class="thumb_hover">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">4w ago</span>
												<span class="topic right">People</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line">
												<div class="h6">Was it permitted to burn the Jordanian air pilot Muadh al-Kasasbeh?</div>
												<div class="toolbar">
													<div class="tools left">
														<ul>
															<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
													</div>
													<div class="share right">
														<ul>
															<li><a href="javascript:void(0);">325 answers</a></li>
														</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="thumb"><img src="/images/content/qa-gallery/jordanian-pilot-maaz-al-kassasbeh-being-burnt-alive.jpg" alt="img"></div>
								</div>
							</div>

							<div class="thumb_half">
								<div class="thumb_hover">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">4w ago</span>
												<span class="topic right">Beliefs</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line">
												<div class="h6">Is it permissible to do some sports training in Ramadan?</div>
												<div class="toolbar">
													<div class="tools left">
														<ul>
															<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
													</div>
													<div class="share right">
														<ul>
															<li><a href="javascript:void(0);">325 answers</a></li>
														</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="thumb"><img src="/images/content/qa-gallery/footballs.jpg" alt="img"></div>
								</div>
							</div>
						</div>

						<div class="thumb_full">
							<div class="thumb_hover">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">4w ago</span>
											<span class="topic right">People</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line">
											<div class="h5">Why is the Islamic State not our biggest problem?</div>
											<div class="toolbar">
												<div class="tools left">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														<li><a href="javascript:void(0);">3k views</a></li>
														<li><a href="javascript:void(0);">235 comments</a></li>
														<li><a href="javascript:void(0);">Share</a></li>
													</ul>
												</div>
												<div class="share right">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-ellipsis-h"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="thumb"><img src="/images/content/qa-gallery/tumblr_static_8o6gzjc8x2o88kcskw80884cg_640_v2.jpg" alt="img"></div>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row">
					<div class="thumb_full_single_right">
						<div class="thumb_hover">
							<div class="hover_overlay">
								<div class="inner">
									<div class="top_line">
										<span class="time left">4w ago</span>
										<span class="topic right">People</span>
										<div class="clearfix"></div>
									</div>
									<div class="bottom_line">
										<div class="h5">What is the cause of the strong relationship between America and Saudi Arabia?</div>
										<p>Rachel Bronson's "Thicker than Oil:America's Uneasy Partnership with Saudi Arabia" provides a good look at how the relationship developed. Both found common cause during the Cold War, starting in the late 1940s to oppose the Godless communist USSR. The Saudis focused on the "godless" part while t...(read more)</p>
										<div class="toolbar">
											<div class="tools left">
												<ul>
													<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
													<li><a href="javascript:void(0);">3k views</a></li>
													<li><a href="javascript:void(0);">235 comments</a></li>
													<li><a href="javascript:void(0);">Share</a></li>
												</ul>
											</div>
											<div class="share right">
												<ul>
													<li><a href="javascript:void(0);"><i class="fa fa-ellipsis-h"></i></a></li>
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="thumb"><img src="/images/content/qa-gallery/IMG_06112015_005646-copy.jpg" alt="img"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ** ENDS PORTFOLIO GRID GALLERY ** -->

<!-- ** START PORTFOLIO GRID GALLERY ** -->
<div id="one" class="qa_gallery first">
	<div class="next_prev">
		<a href="javascript:void(0)" class="prev"><i class="fa fa-angle-left"></i></a>
		<a href="javascript:void(0)" class="next"><i class="fa fa-angle-right"></i></a>
	</div>
	<?php include "includes/qa-filter.php";?>
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<div class="row">
					<div class="gallery_four_thumb">

						<div class="thumb_full">
							<div class="thumb_hover">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">4w ago by Marzuq33</span>
											<span class="topic right">politics</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line">
											<div class="h5">How and when did the division occur between Sunnis and Shias?</div>
											<p>Shi’a Muslims trace the division to the death of the Prophet muhammed, when Abu Bakr was chosen as caliph rather than Ali. In the Shi’a view, Ali and his followers had a religious... (read more)</p>
											<div class="toolbar">
												<div class="tools left">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														<li><a href="javascript:void(0);">3k views</a></li>
														<li><a href="javascript:void(0);">235 comments</a></li>
														<li><a href="javascript:void(0);">Share</a></li>
													</ul>
												</div>
												<div class="share right">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-ellipsis-h"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="thumb"><img src="/images/content/qa-gallery/IMG_06112015_005050_3.jpg" alt="img"></div>
							</div>
						</div>

						<div class="clearfix">
							<div class="thumb_quarter">
								<div class="thumb_hover">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">4w ago</span>
												<span class="topic right">People</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line">
												<div class="h6">Was it permitted to burn the Jordanian air pilot Muadh al-Kasasbeh?</div>
												<div class="toolbar">
													<div class="tools left">
														<ul>
															<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
													</div>
													<div class="share right">
														<ul>
															<li><a href="javascript:void(0);">325 answers</a></li>
														</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="thumb"><img src="/images/content/qa-gallery/islamic-leaders-accuse-the-uk-government-of-inciting-a-mccartyite-witch-hunt-against-muslims-1426097319.jpg" alt="img"></div>
								</div>
							</div>

							<div class="thumb_quarter">
								<div class="thumb_hover">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">4w ago</span>
												<span class="topic right">Beliefs</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line">
												<div class="h6">Is it permissible to do some sports training in Ramadan?</div>
												<div class="toolbar">
													<div class="tools left">
														<ul>
															<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
													</div>
													<div class="share right">
														<ul>
															<li><a href="javascript:void(0);">325 answers</a></li>
														</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="thumb"><img src="/images/content/qa-gallery/Layer-20.jpg" alt="img"></div>
								</div>
							</div>

							<div class="thumb_quarter">
								<div class="thumb_hover">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">4w ago</span>
												<span class="topic right">Beliefs</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line">
												<div class="h6">Is it permissible to do some sports training in Ramadan?</div>
												<div class="toolbar">
													<div class="tools left">
														<ul>
															<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
													</div>
													<div class="share right">
														<ul>
															<li><a href="javascript:void(0);">325 answers</a></li>
														</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="thumb"><img src="/images/content/qa-gallery/IMG_06112015_005049_1.jpg" alt="img"></div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="col-md-5">
				<div class="row">
					<div class="thumb_full_single_right">
						<div class="thumb_hover">
							<div class="hover_overlay">
								<div class="inner">
									<div class="top_line">
										<span class="time left">4w ago</span>
										<span class="topic right">politics</span>
										<div class="clearfix"></div>
									</div>
									<div class="bottom_line">
										<div class="h5">Why is the Islamic State not our biggest problem?</div>
										<div class="toolbar">
											<div class="tools left">
												<ul>
													<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
													<li><a href="javascript:void(0);">325 answers</a></li>
												</ul>
											</div>
											<div class="share right">
												<ul>
													<li><a href="javascript:void(0);"><i class="fa fa-ellipsis-h"></i></a></li>
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="thumb"><img src="/images/content/qa-gallery/2d63abe52.jpg" alt="img"></div>
						</div>
					</div>

					<div class="both_half_right clearfix">
						<div class="thumb_half">
							<div class="thumb_hover">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">4w ago</span>
											<span class="topic right">politics</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line">
											<div class="h6">Was it permitted to burn the Jordanian air pilot Muadh al-Kasasbeh?</div>
											<div class="toolbar">
												<div class="tools left">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
													</ul>
												</div>
												<div class="share right">
													<ul>
														<li><a href="javascript:void(0);">325 answers</a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="thumb"><img src="/images/content/qa-gallery/in-photos-48-hours-under-siege-in-kobane-body-image-1419461140.jpg" alt="img"></div>
							</div>
						</div>

						<div class="thumb_half">
							<div class="thumb_hover">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">4w ago</span>
											<span class="topic right">Beliefs</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line">
											<div class="h6">Is it permissible to do some sports training in Ramadan?</div>
											<div class="toolbar">
												<div class="tools left">
													<ul>
														<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
													</ul>
												</div>
												<div class="share right">
													<ul>
														<li><a href="javascript:void(0);">325 answers</a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="thumb"><img src="/images/content/qa-gallery/in-photos-48-hours-under-siege-in-kobane-body-image-1419460514.jpg" alt="img"></div>
							</div>
						</div>
					</div>

					<div class="thumb_full_single_right">
						<div class="thumb_hover">
							<div class="hover_overlay">
								<div class="inner">
									<div class="top_line">
										<span class="time left">4w ago</span>
										<span class="topic right">People</span>
										<div class="clearfix"></div>
									</div>
									<div class="bottom_line">
										<div class="h5">Why is the Islamic State not our biggest problem?</div>
										<div class="toolbar">
											<div class="tools left">
												<ul>
													<li><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Answer</a></li>
													<li><a href="javascript:void(0);">3k views</a></li>
													<li><a href="javascript:void(0);">235 comments</a></li>
													<li><a href="javascript:void(0);">Share</a></li>
												</ul>
											</div>
											<div class="share right">
												<ul>
													<li><a href="javascript:void(0);"><i class="fa fa-ellipsis-h"></i></a></li>
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="thumb"><img src="/images/content/qa-gallery/75974194_180947444.jpg" alt="img"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- ** ENDS PORTFOLIO GRID GALLERY ** -->
