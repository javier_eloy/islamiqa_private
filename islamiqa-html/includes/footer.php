<!-- ** START FOOTER ** -->
<section class="footer">
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h6>About</h6>
					<a class="logo" href="/"><img src="/images/ui/logo-grey.png" alt="logo"></a>
					<p>A crowd sourced site with questions worth answering responded to by those with a deep interest and insight into Islam and its peoples.</p>
				</div>

				<div class="col-md-4">
					<h6>Site Map</h6>
					<ul class="footer_links">
						<li class="_item"><a href="javasctipt:void(0);">Home</a></li>
						<li class="_item"><a href="javasctipt:void(0);">Popular Questions</a></li>
						<li class="_item"><a href="javasctipt:void(0);">Recently Asked</a></li>
						<li class="_item"><a href="javasctipt:void(0);">All Questions</a></li>
						<li class="_item"><a href="javasctipt:void(0);">Unanswered Questions</a></li>
						<li class="_item"><a href="javasctipt:void(0);">Blog</a></li>
						<li class="_item"><a href="javasctipt:void(0);">Volunteering</a></li>
					</ul>
					<ul class="footer_links">
						<li class="_item"><a href="javasctipt:void(0);">About</a></li>
						<li class="_item"><a href="javasctipt:void(0);">Privacy Policy</a></li>
						<li class="_item"><a href="javasctipt:void(0);">Terms</a></li>
						<li class="_item"><a href="javasctipt:void(0);">FAQ</a></li>
						<li class="_item"><a href="javasctipt:void(0);">Contact</a></li>
						<li class="_item"><a href="javasctipt:void(0);">Press</a></li>
					</ul>
				</div>

				<div class="col-md-4">
					<h6>Social</h6>
					<ul class="footer_social_links">
						<li>
							<a href="javasctipt:void(0);"><i class="fa fa-facebook-square"></i></a>
							<a href="javasctipt:void(0);"><i class="fa fa-twitter-square"></i></i></a>
							<a href="javasctipt:void(0);"><i class="fa fa-google-plus-square"></i></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
</section>
<!-- ** END FOOTER ** -->
