	<script src="js/vendor/tabs.js"></script>
	<script src="js/custom.js"></script>
	<script src="js/developer.js"></script>
	<script>
		//smooth scroll to top
		$back_to_top.on('click', function(event){
			event.preventDefault();
			$('body,html').animate({
				scrollTop: 0 ,
			 	}, scroll_top_duration
			);
		});

	</script>
</body>
</html>
