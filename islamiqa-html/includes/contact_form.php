<!-- ** START CONTACT US SECTION ** -->
<div class="contact_form">
	<form action="contact_form.php">
		<div class="row">
			<div class="col-xs-4">
				<input type="text" class="form-control" placeholder="Full Name">
			</div>
			<div class="col-xs-4">
				<input type="text" class="form-control" placeholder="Email">
			</div>
			<div class="col-xs-4">
				<input type="text" class="form-control" placeholder="Phone">
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<textarea class="form-control" rows="2" placeholder="Message..."></textarea>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<input class="btn btn-primary btn-lg" type="submit" value="Submit">
			</div>
		</div>

	</form>
</div>
<!-- ** ENDS CONTACT US SECTION ** -->
