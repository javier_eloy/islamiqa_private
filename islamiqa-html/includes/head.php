<!DOCTYPE html>
<html>
<head>
<title>Islamiqa</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, Zoom= false">
<link rel="icon" href="images/ui/favicon.ico" type="image/x-icon">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<script src="js/html5shiv.js"></script>
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/ie.css">
<link rel="stylesheet" href="css/font-awesome/font-awesome.css">
<link rel="stylesheet" href="css/shame.css">
<!--  START CUSTOM SCRIPTS  -->
<script src="js/jquery.min.js"></script>
<script src="js/vendor/modernizr.custom.29609.js"></script>
<script src="js/vendor/placeholders.min.js"></script>
<!-- IF IE - use Placeholder Fallback -->
<!--[if lt IE 10 ]>
<script>
  $("#frmLogin,#frmSignup").find('[placeholder]').each(function(){
  $(this).val($(this).attr('placeholder'));
  $(this).focus(function() {
    if ($(this).attr('placeholder')==$(this).val()) {
      $(this).val('');
    }
  });
});
</script>
<![endif]-->
<script src="js/vendor/bootstrap.min.js"></script>
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<script src="js/vendor/respond.min.js"></script>
</head>
<body>
