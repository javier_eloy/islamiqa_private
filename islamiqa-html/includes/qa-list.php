<!-- ** START QUESTIONS AND ANSWERS LIST ** -->
<section class="qa_list">
	<div class="container">
		<div class="row">
			<?php // include "includes/qa-filter.php"?>

			<ul>
				<li>
					<div class="qa-q-list-item" id="q3592">
						<div class="qa-q-item-stats">
							<div class="qa-voting qa-voting-updown" id="voting_3595">
								<div class="qa-vote-buttons qa-vote-buttons-updown">
									<span class="up-arrow">
										<input title="Click to vote up" name="vote_3595_1_q3595" onclick="return qa_vote_click(this);" type="submit" value=" " class="qa-vote-first-button qa-vote-up-button">
										<?xml version="1.0" encoding="utf-8"?>
										<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
										<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
											 viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
											<style type="text/css">
												.st0{fill:#313131;}
											</style>
											<g id="XMLID_1_">
												<path id="XMLID_3_" class="st0" d="M7.1,10.1c-0.3,0.2-0.5,0.2-0.7,0.2C6.2,10.3,6,10.1,5.9,9.7L5.4,8.3l4.4-3.7h0.4l4.4,3.7
													l-0.5,1.4c-0.1,0.4-0.3,0.6-0.5,0.6c-0.2,0.1-0.5,0-0.7-0.2l-2.3-1.7C10.3,8.3,10.1,8.1,10,8C9.9,8.1,9.7,8.3,9.5,8.5L7.1,10.1z
													 M7.1,15.2c-0.3,0.2-0.5,0.2-0.7,0.2c-0.2-0.1-0.4-0.3-0.5-0.6l-0.5-1.4l4.4-3.7h0.4l4.4,3.7l-0.5,1.4c-0.1,0.4-0.3,0.6-0.5,0.6
													c-0.2,0.1-0.5,0-0.7-0.2l-2.3-1.7c-0.2-0.2-0.4-0.3-0.5-0.5c-0.1,0.2-0.3,0.3-0.5,0.5L7.1,15.2z"/>
											</g>
										</svg>

									</span>
									<div class="qa-vote-count qa-vote-count-updown">
										<span class="qa-upvote-count">
											<span class="qa-upvote-count-data">0</span><span class="qa-upvote-count-pad"> like</span>
										</span>
									</div>

									<span class="down-arrow">
										<input title="Click to vote down" name="vote_3595_-1_q3595" onclick="return qa_vote_click(this);" type="submit" value=" " class="qa-vote-second-button qa-vote-down-button">
										<?xml version="1.0" encoding="utf-8"?>
										<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
										<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
											viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
											<style type="text/css">
												.st0{fill:#313131;}
											</style>
											<g id="XMLID_1_">
												<path id="XMLID_3_" class="st0" d="M12.9,9.9c0.3-0.2,0.5-0.2,0.7-0.2s0.4,0.3,0.5,0.6l0.5,1.4l-4.4,3.7H9.8l-4.4-3.7l0.5-1.4
												C6,9.9,6.2,9.7,6.4,9.7c0.2-0.1,0.5,0,0.7,0.2l2.3,1.7c0.2,0.2,0.4,0.3,0.5,0.5c0.1-0.2,0.3-0.3,0.5-0.5L12.9,9.9z M12.9,4.8
												c0.3-0.2,0.5-0.2,0.7-0.2c0.2,0.1,0.4,0.3,0.5,0.6l0.5,1.4l-4.4,3.7H9.8L5.4,6.7l0.5-1.4C6,4.9,6.2,4.7,6.4,4.6
												c0.2-0.1,0.5,0,0.7,0.2l2.3,1.7C9.7,6.7,9.9,6.8,10,7c0.1-0.2,0.3-0.3,0.5-0.5L12.9,4.8z"/>
											</g>
										</svg>

									</span>
									<div class="qa-vote-count qa-vote-count-updown">
										<span class="qa-downvote-count">
											<span class="qa-downvote-count-data">0</span><span class="qa-downvote-count-pad"> dislike</span>
										</span>
									</div>
								</div>
								<div class="qa-vote-clear">
								</div>
							</div>
						</div>

						<div class="qa-q-item-main">
							<div class="qa-q-item-title">
								<a href="./index.php/3595/what-your-opinion-isis-burning-jordanian-pilot-muadh-kasasbeh">Are ISIS attacking the same cities that the U.S. Attacked when they were in Iraq?</a>
								<p>there have been many matches. What do you think?</p>
							</div>
							<span class="qa-q-item-avatar-meta">
								<span class="qa-q-item-meta">
									<a href="./index.php/3595/what-your-opinion-isis-burning-jordanian-pilot-muadh-kasasbeh?show=3597#a3597" class="qa-q-item-what">answered</a>
									<span class="qa-q-item-when">
										<span class="qa-q-item-when-data">Feb 10</span>
									</span>
									<span class="qa-q-item-where">
										<span class="qa-q-item-where-pad">in </span><span class="qa-q-item-where-data"><a href="./index.php/groups-and-movements/isis" class="qa-category-link">ISIS</a></span>
									</span>
									<span class="qa-q-item-who">
										<span class="qa-q-item-who-pad">by </span>
										<span class="qa-q-item-who-data">anonymous</span>
									</span>
								</span>
							</span>
							<ul class="qa-q-options">
								<li class="qa-q-item-tag-item"><a href="javascript:void(0);" class="qa-tag-link"><i class="fa fa-pencil"></i> Answer</a></li>
								<li class="qa-q-item-tag-item"><a href="javascript:void(0);" class="qa-tag-link"><i class="fa fa-tasks"></i> Ask related question</a></li>
								<li class="qa-q-item-tag-item"><a href="javascript:void(0);" class="qa-tag-link">Share</a></li>
							</ul>
							<!-- <div class="qa-q-item-tags">
								<ul class="qa-q-item-tag-list">
									<li class="qa-q-item-tag-item"><a href="./index.php/tag/isis" class="qa-tag-link">isis</a></li>
									<li class="qa-q-item-tag-item"><a href="./index.php/tag/jordanian" class="qa-tag-link">jordanian</a></li>
									<li class="qa-q-item-tag-item"><a href="./index.php/tag/muadh+al-kasasbeh" class="qa-tag-link">muadh al-kasasbeh</a></li>
								</ul>
							</div> -->
						</div>
						<div class="qa-q-item-stats">
							<span class="qa-a-count">
								<span><i class="fa fa-comments"></i></span>
								<span class="qa-a-count-data">424</span>
								<span class="qa-a-count-pad"> answers</span>
								<span><i class="fa fa-ellipsis-h"></i></span>
							</span>
						</div>

						<div class="qa-q-item-catagory">
							<span class="cat"> </span>
						</div>
						<div class="qa-q-item-clear clearfix"></div>
					</div>
				</li>

				<li>
					<div class="qa-q-list-item" id="q3594">
						<div class="qa-q-item-stats">
							<div class="qa-voting qa-voting-updown" id="voting_3595">
								<div class="qa-vote-buttons qa-vote-buttons-updown">
									<span class="up-arrow">
										<input title="Click to vote up" name="vote_3595_1_q3595" onclick="return qa_vote_click(this);" type="submit" value=" " class="qa-vote-first-button qa-vote-up-button">
										<?xml version="1.0" encoding="utf-8"?>
										<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
										<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
											 viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
											<style type="text/css">
												.st0{fill:#313131;}
											</style>
											<g id="XMLID_1_">
												<path id="XMLID_3_" class="st0" d="M7.1,10.1c-0.3,0.2-0.5,0.2-0.7,0.2C6.2,10.3,6,10.1,5.9,9.7L5.4,8.3l4.4-3.7h0.4l4.4,3.7
													l-0.5,1.4c-0.1,0.4-0.3,0.6-0.5,0.6c-0.2,0.1-0.5,0-0.7-0.2l-2.3-1.7C10.3,8.3,10.1,8.1,10,8C9.9,8.1,9.7,8.3,9.5,8.5L7.1,10.1z
													 M7.1,15.2c-0.3,0.2-0.5,0.2-0.7,0.2c-0.2-0.1-0.4-0.3-0.5-0.6l-0.5-1.4l4.4-3.7h0.4l4.4,3.7l-0.5,1.4c-0.1,0.4-0.3,0.6-0.5,0.6
													c-0.2,0.1-0.5,0-0.7-0.2l-2.3-1.7c-0.2-0.2-0.4-0.3-0.5-0.5c-0.1,0.2-0.3,0.3-0.5,0.5L7.1,15.2z"/>
											</g>
										</svg>

									</span>
									<div class="qa-vote-count qa-vote-count-updown">
										<span class="qa-upvote-count">
											<span class="qa-upvote-count-data">0</span><span class="qa-upvote-count-pad"> like</span>
										</span>
									</div>

									<span class="down-arrow">
										<input title="Click to vote down" name="vote_3595_-1_q3595" onclick="return qa_vote_click(this);" type="submit" value=" " class="qa-vote-second-button qa-vote-down-button">
										<?xml version="1.0" encoding="utf-8"?>
										<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
										<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
											viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
											<style type="text/css">
												.st0{fill:#313131;}
											</style>
											<g id="XMLID_1_">
												<path id="XMLID_3_" class="st0" d="M12.9,9.9c0.3-0.2,0.5-0.2,0.7-0.2s0.4,0.3,0.5,0.6l0.5,1.4l-4.4,3.7H9.8l-4.4-3.7l0.5-1.4
												C6,9.9,6.2,9.7,6.4,9.7c0.2-0.1,0.5,0,0.7,0.2l2.3,1.7c0.2,0.2,0.4,0.3,0.5,0.5c0.1-0.2,0.3-0.3,0.5-0.5L12.9,9.9z M12.9,4.8
												c0.3-0.2,0.5-0.2,0.7-0.2c0.2,0.1,0.4,0.3,0.5,0.6l0.5,1.4l-4.4,3.7H9.8L5.4,6.7l0.5-1.4C6,4.9,6.2,4.7,6.4,4.6
												c0.2-0.1,0.5,0,0.7,0.2l2.3,1.7C9.7,6.7,9.9,6.8,10,7c0.1-0.2,0.3-0.3,0.5-0.5L12.9,4.8z"/>
											</g>
										</svg>

									</span>
									<div class="qa-vote-count qa-vote-count-updown">
										<span class="qa-downvote-count">
											<span class="qa-downvote-count-data">0</span><span class="qa-downvote-count-pad"> dislike</span>
										</span>
									</div>
								</div>
								<div class="qa-vote-clear">
								</div>
							</div>
						</div>

						<div class="qa-q-item-main">
							<div class="qa-q-item-title">
								<a href="./index.php/3595/what-your-opinion-isis-burning-jordanian-pilot-muadh-kasasbeh">Are ISIS attacking the same cities that the U.S. Attacked when they were in Iraq?</a>
								<p>there have been many matches. What do you think?</p>
							</div>
							<span class="qa-q-item-avatar-meta">
								<span class="qa-q-item-meta">
									<a href="./index.php/3595/what-your-opinion-isis-burning-jordanian-pilot-muadh-kasasbeh?show=3597#a3597" class="qa-q-item-what">answered</a>
									<span class="qa-q-item-when">
										<span class="qa-q-item-when-data">Feb 10</span>
									</span>
									<span class="qa-q-item-where">
										<span class="qa-q-item-where-pad">in </span><span class="qa-q-item-where-data"><a href="./index.php/groups-and-movements/isis" class="qa-category-link">ISIS</a></span>
									</span>
									<span class="qa-q-item-who">
										<span class="qa-q-item-who-pad">by </span>
										<span class="qa-q-item-who-data">anonymous</span>
									</span>
								</span>
							</span>
							<ul class="qa-q-options">
								<li class="qa-q-item-tag-item"><a href="javascript:void(0);" class="qa-tag-link"><i class="fa fa-pencil"></i> Answer</a></li>
								<li class="qa-q-item-tag-item"><a href="javascript:void(0);" class="qa-tag-link"><i class="fa fa-tasks"></i> Ask related question</a></li>
								<li class="qa-q-item-tag-item"><a href="javascript:void(0);" class="qa-tag-link">Share</a></li>
							</ul>
							<!-- <div class="qa-q-item-tags">
								<ul class="qa-q-item-tag-list">
									<li class="qa-q-item-tag-item"><a href="./index.php/tag/isis" class="qa-tag-link">isis</a></li>
									<li class="qa-q-item-tag-item"><a href="./index.php/tag/jordanian" class="qa-tag-link">jordanian</a></li>
									<li class="qa-q-item-tag-item"><a href="./index.php/tag/muadh+al-kasasbeh" class="qa-tag-link">muadh al-kasasbeh</a></li>
								</ul>
							</div> -->
						</div>
						<div class="qa-q-item-stats">
							<span class="qa-a-count">
								<span><i class="fa fa-comments"></i></span>
								<span class="qa-a-count-data">424</span>
								<span class="qa-a-count-pad"> answers</span>
								<span><i class="fa fa-ellipsis-h"></i></span>
							</span>
						</div>

						<div class="qa-q-item-catagory">
							<span class="cat"> </span>
						</div>
						<div class="qa-q-item-clear clearfix"></div>
					</div>
				</li>
				<li>
					<div class="qa-q-list-item" id="q3593">
						<div class="qa-q-item-stats">
							<div class="qa-voting qa-voting-updown" id="voting_3595">
								<div class="qa-vote-buttons qa-vote-buttons-updown">
									<span class="up-arrow">
										<input title="Click to vote up" name="vote_3595_1_q3595" onclick="return qa_vote_click(this);" type="submit" value=" " class="qa-vote-first-button qa-vote-up-button">
										<?xml version="1.0" encoding="utf-8"?>
										<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
										<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
											 viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
											<style type="text/css">
												.st0{fill:#313131;}
											</style>
											<g id="XMLID_1_">
												<path id="XMLID_3_" class="st0" d="M7.1,10.1c-0.3,0.2-0.5,0.2-0.7,0.2C6.2,10.3,6,10.1,5.9,9.7L5.4,8.3l4.4-3.7h0.4l4.4,3.7
													l-0.5,1.4c-0.1,0.4-0.3,0.6-0.5,0.6c-0.2,0.1-0.5,0-0.7-0.2l-2.3-1.7C10.3,8.3,10.1,8.1,10,8C9.9,8.1,9.7,8.3,9.5,8.5L7.1,10.1z
													 M7.1,15.2c-0.3,0.2-0.5,0.2-0.7,0.2c-0.2-0.1-0.4-0.3-0.5-0.6l-0.5-1.4l4.4-3.7h0.4l4.4,3.7l-0.5,1.4c-0.1,0.4-0.3,0.6-0.5,0.6
													c-0.2,0.1-0.5,0-0.7-0.2l-2.3-1.7c-0.2-0.2-0.4-0.3-0.5-0.5c-0.1,0.2-0.3,0.3-0.5,0.5L7.1,15.2z"/>
											</g>
										</svg>

									</span>
									<div class="qa-vote-count qa-vote-count-updown">
										<span class="qa-upvote-count">
											<span class="qa-upvote-count-data">0</span><span class="qa-upvote-count-pad"> like</span>
										</span>
									</div>

									<span class="down-arrow">
										<input title="Click to vote down" name="vote_3595_-1_q3595" onclick="return qa_vote_click(this);" type="submit" value=" " class="qa-vote-second-button qa-vote-down-button">
										<?xml version="1.0" encoding="utf-8"?>
										<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
										<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
											viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
											<style type="text/css">
												.st0{fill:#313131;}
											</style>
											<g id="XMLID_1_">
												<path id="XMLID_3_" class="st0" d="M12.9,9.9c0.3-0.2,0.5-0.2,0.7-0.2s0.4,0.3,0.5,0.6l0.5,1.4l-4.4,3.7H9.8l-4.4-3.7l0.5-1.4
												C6,9.9,6.2,9.7,6.4,9.7c0.2-0.1,0.5,0,0.7,0.2l2.3,1.7c0.2,0.2,0.4,0.3,0.5,0.5c0.1-0.2,0.3-0.3,0.5-0.5L12.9,9.9z M12.9,4.8
												c0.3-0.2,0.5-0.2,0.7-0.2c0.2,0.1,0.4,0.3,0.5,0.6l0.5,1.4l-4.4,3.7H9.8L5.4,6.7l0.5-1.4C6,4.9,6.2,4.7,6.4,4.6
												c0.2-0.1,0.5,0,0.7,0.2l2.3,1.7C9.7,6.7,9.9,6.8,10,7c0.1-0.2,0.3-0.3,0.5-0.5L12.9,4.8z"/>
											</g>
										</svg>

									</span>
									<div class="qa-vote-count qa-vote-count-updown">
										<span class="qa-downvote-count">
											<span class="qa-downvote-count-data">0</span><span class="qa-downvote-count-pad"> dislike</span>
										</span>
									</div>
								</div>
								<div class="qa-vote-clear">
								</div>
							</div>
						</div>

						<div class="qa-q-item-main">
							<div class="qa-q-item-title">
								<a href="./index.php/3595/what-your-opinion-isis-burning-jordanian-pilot-muadh-kasasbeh">Are ISIS attacking the same cities that the U.S. Attacked when they were in Iraq?</a>
								<p>there have been many matches. What do you think?</p>
							</div>
							<span class="qa-q-item-avatar-meta">
								<span class="qa-q-item-meta">
									<a href="./index.php/3595/what-your-opinion-isis-burning-jordanian-pilot-muadh-kasasbeh?show=3597#a3597" class="qa-q-item-what">answered</a>
									<span class="qa-q-item-when">
										<span class="qa-q-item-when-data">Feb 10</span>
									</span>
									<span class="qa-q-item-where">
										<span class="qa-q-item-where-pad">in </span><span class="qa-q-item-where-data"><a href="./index.php/groups-and-movements/isis" class="qa-category-link">ISIS</a></span>
									</span>
									<span class="qa-q-item-who">
										<span class="qa-q-item-who-pad">by </span>
										<span class="qa-q-item-who-data">anonymous</span>
									</span>
								</span>
							</span>
							<ul class="qa-q-options">
								<li class="qa-q-item-tag-item"><a href="javascript:void(0);" class="qa-tag-link"><i class="fa fa-pencil"></i> Answer</a></li>
								<li class="qa-q-item-tag-item"><a href="javascript:void(0);" class="qa-tag-link"><i class="fa fa-tasks"></i> Ask related question</a></li>
								<li class="qa-q-item-tag-item"><a href="javascript:void(0);" class="qa-tag-link">Share</a></li>
							</ul>
							<!-- <div class="qa-q-item-tags">
								<ul class="qa-q-item-tag-list">
									<li class="qa-q-item-tag-item"><a href="./index.php/tag/isis" class="qa-tag-link">isis</a></li>
									<li class="qa-q-item-tag-item"><a href="./index.php/tag/jordanian" class="qa-tag-link">jordanian</a></li>
									<li class="qa-q-item-tag-item"><a href="./index.php/tag/muadh+al-kasasbeh" class="qa-tag-link">muadh al-kasasbeh</a></li>
								</ul>
							</div> -->
						</div>
						<div class="qa-q-item-stats">
							<span class="qa-a-count">
								<span><i class="fa fa-comments"></i></span>
								<span class="qa-a-count-data">424</span>
								<span class="qa-a-count-pad"> answers</span>
								<span><i class="fa fa-ellipsis-h"></i></span>
							</span>
						</div>

						<div class="qa-q-item-catagory">
							<span class="cat"> </span>
						</div>
						<div class="qa-q-item-clear clearfix"></div>
					</div>
				</li>

			</ul>
		</div>
	</div>
</section>
<!-- ** ENDS QUESTIONS AND ANSWERS LIST ** -->
