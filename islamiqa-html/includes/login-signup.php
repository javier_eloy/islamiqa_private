<div class="main-nav">
	<ul>
		<li><a href="javascript:void(0);" id="signupButton"><span>Signup</span> <i class="fa fa-caret-down"></i></a></li>
		<li><a href="javascript:void(0);" id="loginButton"><span>Login</span> <i class="fa fa-caret-down"></i></a></li>
	</ul>

	<div class="signup_login_form" id="signupBox">
		<div id="signupForm">
			<div id="signup">
				<div class="cd-tabs">
					<nav>
						<ul class="cd-tabs-navigation">
							<li><a data-content="inbox" class="selected" href="#0">Social</a></li>
							<li><a data-content="new" href="#0">Email</a></li>
						</ul>
					</nav>

					<ul class="cd-tabs-content">
						<li data-content="inbox" class="selected">
							<form class="cd-form">
								<ul>
									<li><a href="#"><img src="/images/ui/login-with-facebook.png" alt="facebook"></a></li>
									<li><a href="#"><img src="/images/ui/login-with-twitter.png" alt="twitter"></a></li>
									<li><a href="#"><img src="/images/ui/login-with-gplus.png" alt="gplus"></a></li>
									<li><a href="#"><img src="/images/ui/login-with-linkedin.png" alt="linkedin"></a></li>
									<li>
										<p class="fieldset">
											Alternatively, <a href="javascript:void(0);">Sign Up With Email</a>. By clicking Sign up, you agree to our terms of service and privacy policy. We will send you account related emails occasionally - we'll never spam you, honest!
										</p>
									</li>
								</ul>
							</form>
						</li>

						<li data-content="new">
							<form class="cd-form">
								<p class="fieldset">
									<label class="image-replace cd-username" for="signin-email">User Name</label>
									<input class="full-width has-padding has-border" id="signin-username" type="user name" placeholder="User Name">
									<span class="cd-error-message">Error message here!</span>
								</p>

								<p class="fieldset">
									<label class="image-replace cd-email" for="signin-email">E-mail</label>
									<input class="full-width has-padding has-border" id="signin-email" type="email" placeholder="E-mail">
									<span class="cd-error-message">Error message here!</span>
								</p>

								<p class="fieldset">
									<label class="image-replace cd-password" for="signin-password">Password</label>
									<input class="full-width has-padding has-border" id="signin-password" type="text"  placeholder="Password">
									<a href="#0" class="hide-password">Hide</a>
									<span class="cd-error-message">Error message here!</span>
								</p>

								<p class="fieldset">
									<input type="checkbox" id="remember-me" checked>
									<label for="remember-me">Remember me</label>
								</p>

								<p class="fieldset">
									<input class="btn btn-primary" type="submit" value="Signup Now">
								</p>
							</form>

							<p class="cd-form-bottom-message"><a href="#0">Forgot your password?</a></p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="signup_login_form" id="loginBox">
		<div id="loginForm">
			<div class="cd-tabs">
				<nav>
					<ul class="cd-tabs-navigation">
						<li><a data-content="inbox" class="selected" href="#0">Social</a></li>
						<li><a data-content="new" href="#0">Email</a></li>
					</ul>
				</nav>

				<ul class="cd-tabs-content">
					<li data-content="inbox" class="selected">
						<form class="cd-form">
							<ul>
								<li><a href="#"><img src="/images/ui/login-with-facebook.png" alt="facebook"></a></li>
								<li><a href="#"><img src="/images/ui/login-with-twitter.png" alt="twitter"></a></li>
								<li><a href="#"><img src="/images/ui/login-with-gplus.png" alt="gplus"></a></li>
								<li><a href="#"><img src="/images/ui/login-with-linkedin.png" alt="linkedin"></a></li>
								<li>
									<p class="fieldset">
										Alternatively, <a href="javascript:void(0);">Sign in With Email</a>. By clicking Sign in, you agree to our terms of service and privacy policy. We will send you account related emails occasionally - we'll never spam you, honest!
									</p>
								</li>
							</ul>
						</form>
					</li>

					<li data-content="new">
						<form class="cd-form">
							<p class="fieldset">
								<label class="image-replace cd-email" for="signin-email">E-mail</label>
								<input class="full-width has-padding has-border" id="signin-email" type="email" placeholder="E-mail">
								<span class="cd-error-message">Error message here!</span>
							</p>

							<p class="fieldset">
								<label class="image-replace cd-password" for="signin-password">Password</label>
								<input class="full-width has-padding has-border" id="signin-password" type="text"  placeholder="Password">
								<a href="#0" class="hide-password">Hide</a>
								<span class="cd-error-message">Error message here!</span>
							</p>

							<p class="fieldset">
								<input type="checkbox" id="remember-me" checked>
								<label for="remember-me">Remember me</label>
							</p>

							<p class="fieldset">
								<input class="btn btn-primary" type="submit" value="Login">
							</p>
						</form>

						<p class="cd-form-bottom-message"><a href="#0">Forgot your password?</a></p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
