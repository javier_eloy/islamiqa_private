<!-- ** START HOME SLIDER ** -->
<section class="main_slider">
	<div class="container">
		<div class="row">
			<div class="inner">
				<h1>Questions worth answering...</h1>
				<h3>For those with an interest in Islam and its peoples</h3>
			</div>
		</div>
		<div class="row">
			<div class="form-group ask_bar">
				<form role="search">
					<input type="text" class="form-control" placeholder="">
					<button type="submit" class="btn btn-primary">Ask Question</button>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- ** START HOME SLIDER ** -->
