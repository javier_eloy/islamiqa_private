<?php
/*
	Question2Answer by Gideon Greenspan and contributors
	http://www.question2answer.org/

	File: qa-include/qa-widget-ask-box.php
	Description: Widget module class for ask a question box


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

class qa_ask_box
{
	public function allow_template($template)
	{
		$allowed = array(
			'activity', 'categories', 'custom', 'feedback', 'qa', 'questions',
			'hot', 'search', 'tag', 'tags', 'unanswered'
		);
		return in_array($template, $allowed);
	}

	public function allow_region($region)
	{
		return in_array($region, array('main', 'side', 'full'));
	}

	public function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
	{
		if (isset($qa_content['categoryids']))
			$params=array('cat' => end($qa_content['categoryids']));
		else
			$params=null;
?>










    <nav class="navbar navbar-default" style="border-color:transparent">
        <div class="nav nav-justified navbar-nav" ">
 
            <form   role="search" method="post" action="<?php echo qa_path_html('ask', $params); ?>">
                <div class="input-group">
        
                    <input name="title" type="text" class="form-control">
                
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary" style="border-radius:0px 20px 20px 0px;border-color:transparent;">
                            <span style="font-weight:bold;">Ask A Question</span>
                        </button>
                    </div>
                </div>  
                <input type="hidden" name="doask1" value="1">
            </form>   
         
        </div>
    </nav>







<!--
			<div class="search">

				<input type="text" name="title" class="form-control input-sm" maxlength="64" placeholder="Search" />
					 <button type="submit" class="btn btn-primary btn-sm">ASK A QUESTION</button>

		<input type="hidden" name="doask1" value="1">
	</form>
</div>
			</div>
</div>
<style>
#search {
    float: right;
    margin-top: 9px;
    width: 250px;
}

.search {
    padding: 5px 0;
    width: 230px;
    height: 30px;
    position: relative;
    left: 10px;
    float: left;
    line-height: 22px;
}

    .search input {
        position: absolute;
        width: 0px;
        float: Left;
        margin-left: 210px;
        -webkit-transition: all 0.7s ease-in-out;
        -moz-transition: all 0.7s ease-in-out;
        -o-transition: all 0.7s ease-in-out;
        transition: all 0.7s ease-in-out;
        height: 30px;
        line-height: 18px;
        padding: 0 2px 0 2px;
        border-radius:1px;
    }

        .search:hover input, .search input:focus {
            width: 200px;
            margin-left: 0px;
        }

.btn {
    height: 30px;
    position: absolute;
    right: 0;
    top: 5px;

}
</style>-->
<?php
	}
}

/*
<div class="container">
	<div class="row">
		<h2>Slider Search box</h2>
        <div class="search">
<input type="text" class="form-control input-sm" maxlength="64" placeholder="Search" />
 <button type="submit" class="btn btn-primary btn-sm">Search</button>
</div>
	</div>
</div>


<div class="container">
	<div class="row">
	<form method="post" action="<?php echo qa_path_html('ask', $params); ?>">
		<div class="search">


			<tr style="vertical-align:middle;">
				<td class="qa-form-tall-label" style="padding:8px; white-space:nowrap; <?php echo ($region=='side') ? 'padding-bottom:0;' : 'text-align:right;'?>" width="1">
					<?php echo strtr(qa_lang_html('question/ask_title'), array(' ' => '&nbsp;'))?>:
				</td>
<?php
			if ($region=='side') {
?>
			</tr>
			<div class="search">
<?php
			}
?>
				
					<input type="text" name="" class="form-control input-sm" maxlength="64" placeholder="Search" />
			</div>
		</table>
		<input type="hidden" name="doask1" value="1">
	</form>
</div>*/