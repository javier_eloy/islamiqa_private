## Q2A Tab Widget  [![Question2Answer](http://qa-themes.com/files/q2a-logo.png)](http://question2answer.org/)
================
## Description
This plugin adds a widget to **Question2Answer** to add Popular & Recent tabs to sidebar.

## Features
- you can set number of questions in each tab
- you can add HTML content(for advertisement, etc...) after a certain number of questions in list
- you can choose to enable Thumbnail picture which will read first image from the question's content
- you can add or choose a custom CSS files to the plug-in

## Installation
1. Download and extract the files to your plugins folder (e.g. `qa-plugins/q2a-tab-widget`).
2. In Q2A go to **Admin > Plugins** and set up the setting for "tabs widget" and save changes.
3. In Q2A go to **Admin > Layout** add it's widget anywhere you want.

## Author
this plug-in is created by `Towhid Nategheian`_ at QA-Themes.com_.

.. _Towhid Nategheian: http://TowhidN.com
.. _QA-Themes: http://QA-Themes.com


## Customizations
to customize widget's styling go to plugins directory then "styles" directory. then create a copy from the "default.css" file and rename it to anything you want.
then visit plugins options and choose it in "CSS Styles" field. you can edit this file to make it more compatible with your theme.
you can also copy content of "default.css" to your main style and set the plugin to not include this style in the page.

#### Disclaimer
This is **beta code**. It worked fine for me, but may not work as you expect. if any problems occur get help on http://www.question2answer.org/qa/ or our [Q2A plugin support forum](http://qa-themes.com/forums/forum/plugins-support)

#### copyright
this plugin and all it's source code is [Copylefted](http://en.wikipedia.org/wiki/Copyleft)

#### About q2A

[Question2Answer](http://www.question2answer.org/) is a free and open source platform for Question and Answer communities.


