﻿/*******************************************************************/
Image Resize plugin for question2answer
/*******************************************************************/

/*-----------------------------------------------------------------*/
1. Summary
/*-----------------------------------------------------------------*/
This package is plugin for question2answer.

question2answer: http://www.question2answer.org/

/*-----------------------------------------------------------------*/
2. Feature of this plugin
/*-----------------------------------------------------------------*/
1. Image file uploaded by CKEditor is resized in appointed size.
2. It is reduced when the long side of width or height is bigger than appointed size.
3. It is not reduced when width and height are smaller than appointed size.
4. Aspect ratio is always maintained.

/*-----------------------------------------------------------------*/
3. Correspondence Version
/*-----------------------------------------------------------------*/
question2answer V1.5 later


/*-----------------------------------------------------------------*/

4. Installation/Settings

/*-----------------------------------------------------------------*/

1.Unzip archive any local folder.

2.Upload image-resize folder under qa-plugin folder.

3.Log in administrator account.

4.Select admin -> plugins menu.

5.After setting, and save.

6.Change program of wysiwyg-editor plugin(see hack.txt)


/*-----------------------------------------------------------------*/
5. Uninstallation
/*-----------------------------------------------------------------*/
1.Log in administrator account.
2.Select admin -> plugins menu.
3.Click option of "image-resize".
4.Click "Restore default" button.
5.Delete image-resize folder under qa-plugin folder.

/*-----------------------------------------------------------------*/
6. Options
/*-----------------------------------------------------------------*/
[Enable Image Resize]
OFF: Disable Image Resize plugin
ON:  Enable Image Resize plugin

[Max size of long neighborhood]
Bigger picture than this figure is resized.

[Image quality of resampling]
Image quality of re-sampling. (0 - 100)

/*-----------------------------------------------------------------*/
7. Note
/*-----------------------------------------------------------------*/
This plugin does not work correctly in environment without GD.

/*-----------------------------------------------------------------*/
8. License / Disclaimer
/*-----------------------------------------------------------------*/
1.This software obeys license of Question2Answer.
2.The author does not always promise to support.
3.The author does not compensate you for what kind of damage that you used question2answer or this file.

/*-----------------------------------------------------------------*/
9. Author/Creator
/*-----------------------------------------------------------------*/
handle: sama55
site: http://cmsbox.jp/

/*-----------------------------------------------------------------*/
10. Version history
/*-----------------------------------------------------------------*/
■[2013/03/08] Beta1.0	Beta Release
■[2013/03/12] V1.0		Fix save/default button name
■[2013/04/24] V1.0.1	Bug Fix of uploading non image (Update hack.txt)
■[2013/05/23] V1.0.2	Fix small bug (treatment of boolean value)

Have fun !!