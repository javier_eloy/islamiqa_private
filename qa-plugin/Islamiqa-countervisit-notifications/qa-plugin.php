<?php
/*
	Plugin Name: Islamiqa Messajes On-Site-Notifications
	Plugin URI: http://www.q2apro.com/plugins/on-site-notifications
	Plugin Description:Contains best majors taha version  Facebook-like / Stackoverflow-like notifications on your question2answer forum that can replace all email-notifications.
	Plugin Version: 1.2
	Plugin Date: 2016-03-01
	Plugin Author:Islamiqa based in q2apro.com
	Plugin Author URI: http://www.q2apro.com/
	Plugin License: GPLv3
	Plugin Minimum Question2Answer Version: 1.6
	Plugin Update Check URI: https://raw.githubusercontent.com/q2apro/q2apro-on-site-notifications/master/qa-plugin.php
	
	This program is free software. You can redistribute and modify it 
	under the terms of the GNU General Public License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.gnu.org/licenses/gpl.html

*/

	if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
		header('Location: ../../');
		exit;
	}

	// language file
	qa_register_plugin_phrases('messages-q2a-onsitenotifications-lang-*.php', 'messages_q2a_onsitenotifications_lang');

	// page for ajax
	qa_register_plugin_module('page', 'messages-q2a-onsitenotifications-page.php', 'messages_q2a_onsitenotifications_page', 'Message-On-Site-Notifications Page');

	// layer
	qa_register_plugin_layer('messages-q2a-onsitenotifications-layer.php', 'messages_q2a Message-On-Site-Notifications Layer');

	// admin
	qa_register_plugin_module('module', 'messages-q2a-onsitenotifications-admin.php', 'messages_q2a_onsitenotifications_admin', 'messages-q2a Message-On-Site-Notifications Admin');
   
	// track events
	qa_register_plugin_module('event', 'messages-q2a-history-check.php','messages_q2a_history_check','MSGQ2APRO History Check Mod');

	/*require_once QA_INCLUDE_DIR.'qa-app-admin.php';
	echo qa_admin_plugin_options_path('q2a-pro-on-site-notifications');*/

/*
	Omit PHP closing tag to help avoid accidental output
*/