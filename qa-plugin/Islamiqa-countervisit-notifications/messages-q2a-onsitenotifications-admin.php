<?php
/*
	Plugin Name: Messajes On-Site-Notifications
	Plugin URI: http://www.q2apro.com/plugins/on-site-notifications
	Plugin Description: Facebook-like / Stackoverflow-like notifications on your question2answer forum that can replace all email-notifications.
	Plugin Version: → see qa-plugin.php
	Plugin Date: → see qa-plugin.php
	Plugin Author: q2apro.com
	Plugin Author URI: http://www.q2apro.com/
	Plugin License: GPLv3
	Plugin Minimum Question2Answer Version: → see qa-plugin.php
	Plugin Update Check URI: https://raw.githubusercontent.com/q2apro/q2apro-on-site-notifications/master/qa-plugin.php
	
	This program is free software. You can redistribute and modify it 
	under the terms of the GNU General Public License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.gnu.org/licenses/gpl.html

*/

	class messages_q2a_onsitenotifications_admin {

		// initialize db-table 'eventlog' if it does not exist yet
		function init_queries($tableslc) {
		
			$tablename = qa_db_add_table_prefix('eventlog');
			
			// check if event logger has been initialized already (check for one of the options and existing table)
			require_once QA_INCLUDE_DIR.'qa-app-options.php';
			if(qa_opt('event_logger_to_database') && in_array($tablename, $tableslc)) {
				// options exist, but check if really enabled
				if(qa_opt('event_logger_to_database')=='' && qa_opt('event_logger_to_files')=='') {
					// enabled database logging
					qa_opt('event_logger_to_database', 1);
				}
			}
			else {
				// not enabled, let's enable the event logger
			
				// set option values for event logger
				qa_opt('event_logger_to_database', 1);
				qa_opt('event_logger_to_files', '');
				qa_opt('event_logger_directory', '');
				qa_opt('event_logger_hide_header', '');
			
				if (!in_array($tablename, $tableslc)) {
					require_once QA_INCLUDE_DIR.'qa-app-users.php';
					require_once QA_INCLUDE_DIR.'qa-db-maxima.php';

					return 'CREATE TABLE IF NOT EXISTS ^eventlog ('.
						'datetime DATETIME NOT NULL,'.
						'ipaddress VARCHAR (15) CHARACTER SET ascii,'.
						'userid '.qa_get_mysql_user_column_type().','.
						'handle VARCHAR('.QA_DB_MAX_HANDLE_LENGTH.'),'.
						'cookieid BIGINT UNSIGNED,'.
						'event VARCHAR (20) CHARACTER SET ascii NOT NULL,'.
						'params VARCHAR (800) NOT NULL,'.
						'KEY datetime (datetime),'.
						'KEY ipaddress (ipaddress),'.
						'KEY userid (userid),'.
						'KEY event (event)'.
					') ENGINE=MyISAM DEFAULT CHARSET=utf8';
				}
			}
			// memo: would be best to check if plugin is installed in qa-plugin folder or using plugin_exists()
			// however this functionality is not available in q2a v1.6.3
			
			// create table qa_usermeta which stores the last visit of each user
			$tablename2 = qa_db_add_table_prefix('usermeta');
			if (!in_array($tablename2, $tableslc)) {
				qa_db_query_sub(
					'CREATE TABLE IF NOT EXISTS ^usermeta (
					meta_id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					user_id bigint(20) unsigned NOT NULL,
					meta_key varchar(255) DEFAULT NULL,
					meta_value longtext,
					PRIMARY KEY (meta_id),
					UNIQUE (user_id,meta_key)
					) ENGINE=MyISAM  DEFAULT CHARSET=utf8'
				);
			}

			// create table qa_postviews which stores the last visit of each user
			//add 01/03/2016 by Ronel Lezama
			$tablename3 = qa_db_add_table_prefix('qa_postviews');
			if (!in_array($tablename3, $tableslc)) {
				qa_db_query_sub(
					'CREATE TABLE IF NOT EXISTS ^postviews (
						postviewid int(10) NOT NULL AUTO_INCREMENT,
					  	postid int(10) unsigned NOT NULL,
					  	type enum("Q","A","C","Q_HIDDEN","A_HIDDEN","C_HIDDEN","Q_QUEUED","A_QUEUED","C_QUEUED","NOTE") CHARACTER SET utf8 NOT NULL,
					  	views int(11) NOT NULL,
					  	dateviews date NOT NULL,
					  PRIMARY KEY (postviewid),
					  KEY postid (postid),
					  FOREIGN KEY (postid) REFERENCES qa_post (postid)
					) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1' 
				);
			}


		} // end init_queries

		// option's value is requested but the option has not yet been set
		function option_default($option) {
			switch($option) {
				case 'messages_q2a_onsitenotifications_enabled':
					return 1; // true
				case 'messages_q2a_onsitenotifications_nill':
					return 'N'; // days
				case 'messages_q2a_onsitenotifications_maxage':
					return 365; // days
				case 'messages_q2a_onsitenotifications_maxevshow':
					return 100; // max events to show in notify box
				case 'messages_q2a_onsitenotifications_newwindow':
					return 1; // true
				case 'messages_q2a_onsitenotifications_rtl':
					return 0; // false
				case 'q2apro_onsitenotifications_enabled':
					return 1;
				default:
					return null;
			}
		}
		
		function allow_template($template) {
			return ($template!='admin');
		}
		
		function admin_form(&$qa_content){                       

			// process the admin form if admin hit Save-Changes-button
			$ok = null;
			if (qa_clicked('messages_q2a_onsitenotifications_save')) {
				qa_opt('messages_q2a_onsitenotifications_enabled', (bool)qa_post_text('messages_q2a_onsitenotifications_enabled')); // empty or 1
				qa_opt('messages_q2a_onsitenotifications_nill', qa_post_text('messages_q2a_onsitenotifications_nill')); // string
				qa_opt('messages_q2a_onsitenotifications_maxevshow', (int)qa_post_text('messages_q2a_onsitenotifications_maxevshow')); // int
				qa_opt('messages_q2a_onsitenotifications_newwindow', (bool)qa_post_text('messages_q2a_onsitenotifications_newwindow')); // int
				qa_opt('messages_q2a_onsitenotifications_rtl', (bool)qa_post_text('messages_q2a_onsitenotifications_rtl')); // int
				$ok = qa_lang('admin/options_saved');
			}
			
			// form fields to display frontend for admin
			$fields = array();
			
			$fields[] = array(
				'type' => 'checkbox',
				'label' => qa_lang('messages_q2a_onsitenotifications_lang/enable_plugin'),
				'tags' => 'name="messages_q2a_onsitenotifications_enabled"',
				'value' => qa_opt('messages_q2a_onsitenotifications_enabled'),
			);
			
			
			$fields[] = array(
				'type' => 'input',
				'label' => qa_lang('messages_q2a_onsitenotifications_lang/no_notifications_label'),
				'tags' => 'name="messages_q2a_onsitenotifications_nill" style="width:100px;"',
				'value' => qa_opt('messages_q2a_onsitenotifications_nill'),
			);

			$fields[] = array(
				'type' => 'input',
				'label' => qa_lang('messages_q2a_onsitenotifications_lang/admin_maxeventsshow'),
				'tags' => 'name="messages_q2a_onsitenotifications_maxevshow" style="width:100px;"',
				'value' => qa_opt('messages_q2a_onsitenotifications_maxevshow'),
			);

			$fields[] = array(
				'type' => 'checkbox',
				'label' => qa_lang('messages_q2a_onsitenotifications_lang/admin_newwindow'),
				'tags' => 'name="messages_q2a_onsitenotifications_newwindow"',
				'value' => qa_opt('messages_q2a_onsitenotifications_newwindow'),
			);

			$fields[] = array(
				'type' => 'checkbox',
				'label' => qa_lang('messages_q2a_onsitenotifications_lang/admin_rtl'),
				'tags' => 'name="messages_q2a_onsitenotifications_rtl"',
				'value' => qa_opt('messages_q2a_onsitenotifications_rtl'),
			);

			$fields[] = array(
				'type' => 'static',
				'note' => '<span style="font-size:12px;color:#789;">'.strtr( qa_lang('messages_q2a_onsitenotifications_lang/contact'), array( 
							'^1' => '<a target="_blank" href="http://www.q2apro.com/plugins/on-site-notifications">',
							'^2' => '</a>'
						  )).'</span>',
			);
			
			return array(           
				'ok' => ($ok && !isset($error)) ? $ok : null,
				'fields' => $fields,
				'buttons' => array(
					array(
						'label' => qa_lang_html('main/save_button'),
						'tags' => 'name="messages_q2a_onsitenotifications_save"',
					),
				),
			);
		}
	}


/*
	Omit PHP closing tag to help avoid accidental output
*/