<?php
/*
	Plugin Name: Islamiqa Infinite Ajax Scroll
	Plugin URI: 
	Plugin Description: Replace from page navigation to infinite ajax scroll. Powered by sama55@CMSBOX
	Plugin Version: 1.3
	Plugin Date: 2013-08-26
	Plugin Author: Islamiqa
	Plugin Author URI: http://www.cmsbox.jp/
	Plugin License: GPLv2
	Plugin Minimum Question2Answer Version: 1.5
	Plugin Update Check URI: 
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}

/**
 * Defines the base directory of the theme 
 */
if (!defined('ISLAMIQA_IAS_PLUGIN_BASE_DIR')) {
	define('ISLAMIQA_IAS_PLUGIN_BASE_DIR', dirname(__FILE__));
}
/**
 * define the directory name of the theme directory 
 */
if (!defined('ISLAMIQA_IAS_PLUGIN_BASE_DIR_NAME')) {
	define('ISLAMIQA_IAS_PLUGIN_BASE_DIR_NAME', 'qa-plugin/'.basename(dirname(__FILE__)).'/');
}

qa_register_plugin_phrases('lang/qa-ias-lang-*.php', 'ias');
qa_register_plugin_module('module', 'qa-ias.php', 'qa_ias', 'IAS');
qa_register_plugin_layer('qa-ias-layer.php', 'IAS Layer');
/*
	Omit PHP closing tag to help avoid accidental output
*/