$(function(){
	if($(".qa-q-list").length && $(".qa-page-links-list").length) {
		$.ias({
			container: ".qa-q-list"
			,item: ".qa-q-list-item"
			,pagination: ".qa-page-links-list"
			,next: ".qa-page-next"
			,loader: '<img class="qa-ias-loader" src="http://localhost/islamiqa/qa-plugin/islamiqa-infinite-ajax-scrolljs/images/loader.gif"/>'
			,noneleft: ''
			,loaderDelay: 600
			,triggerPageThreshold: 3
			,trigger: "Load more items"
			,thresholdMargin: 0
			,history: 1
			,scrollContainer: $(window)
		});
	}
});
