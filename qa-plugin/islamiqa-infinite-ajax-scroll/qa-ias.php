<?php
if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}
require_once QA_INCLUDE_DIR.'qa-app-options.php';

$qa_ias_option_cache = array();

class qa_ias {
	const PLUGIN							= 'ias';
	const ENABLE							= 'ias_enable';
	const ENABLE_DFL						= 0; // false
	
	const IAS_GUIDE							= 'ias_guide';
	
	const IAS1_ENABLE						= 'ias1_enable';
	const IAS1_ENABLE_DFL					= 1; // true
	const IAS1_CONTAINER					= 'ias1_container';
	const IAS1_CONTAINER_DFL				= '.qa-q-list';
	const IAS1_ITEM							= 'ias1_item';
	const IAS1_ITEM_DFL						= '.qa-q-list-item';
	const IAS1_PAGINATION					= 'ias1_pagination';
	const IAS1_PAGINATION_DFL				= '.qa-page-links-list';
	const IAS1_NEXT							= 'ias1_next';
	const IAS1_NEXT_DFL						= '.qa-page-next';
	const IAS1_SCROLLCONTAINER				= 'ias1_scrollcontainer';
	const IAS1_SCROLLCONTAINER_DFL			= 'window';
	const IAS1_NONELEFT						= 'ias1_noneleft';
	const IAS1_NONELEFT_DFL					= '';
	const IAS1_NONELEFT_ROWS				= 1;
	const IAS1_LOADERDELAY					= 'ias1_loaderDelay';
	const IAS1_LOADERDELAY_DFL				= 600;
	const IAS1_LOADERDELAY_MIN				= 0;
	const IAS1_LOADERDELAY_MAX				= 10000;
	const IAS1_TRIGGERPAGETHRESHOLD			= 'ias1_triggerPageThreshold';
	const IAS1_TRIGGERPAGETHRESHOLD_DFL		= 3;
	const IAS1_TRIGGERPAGETHRESHOLD_MIN		= 0;
	const IAS1_TRIGGERPAGETHRESHOLD_MAX		= 100;
	const IAS1_TRIGGER						= 'ias1_trigger';
	const IAS1_THRESHOLDMARGIN				= 'ias1_thresholdMargin';
	const IAS1_THRESHOLDMARGIN_DFL			= 0;
	const IAS1_THRESHOLDMARGIN_MIN			= -10000;
	const IAS1_THRESHOLDMARGIN_MAX			= 10000;
	const IAS1_HISTORY						= 'ias1_history';
	const IAS1_HISTORY_DFL					= 1; // true
	const IAS1_OTHER						= 'ias1_other';
	const IAS1_OTHER_DFL					= '';
	const IAS1_OTHER_ROWS					= 10;
	
	const IAS2_ENABLE						= 'ias2_enable';
	const IAS2_ENABLE_DFL					= 0; // false
	const IAS2_CONTAINER					= 'ias2_container';
	const IAS2_CONTAINER_DFL				= '';
	const IAS2_ITEM							= 'ias2_item';
	const IAS2_ITEM_DFL						= '';
	const IAS2_PAGINATION					= 'ias2_pagination';
	const IAS2_PAGINATION_DFL				= '.qa-page-links-list';
	const IAS2_NEXT							= 'ias2_next';
	const IAS2_NEXT_DFL						= '.qa-page-next';
	const IAS2_SCROLLCONTAINER				= 'ias2_scrollcontainer';
	const IAS2_SCROLLCONTAINER_DFL			= 'window';
	const IAS2_NONELEFT						= 'ias2_noneleft';
	const IAS2_NONELEFT_DFL					= '';
	const IAS2_NONELEFT_ROWS				= 1;
	const IAS2_LOADERDELAY					= 'ias2_loaderDelay';
	const IAS2_LOADERDELAY_DFL				= 600;
	const IAS2_LOADERDELAY_MIN				= 0;
	const IAS2_LOADERDELAY_MAX				= 10000;
	const IAS2_TRIGGERPAGETHRESHOLD			= 'ias2_triggerPageThreshold';
	const IAS2_TRIGGERPAGETHRESHOLD_DFL		= 3;
	const IAS2_TRIGGERPAGETHRESHOLD_MIN		= 0;
	const IAS2_TRIGGERPAGETHRESHOLD_MAX		= 100;
	const IAS2_TRIGGER						= 'ias2_trigger';
	const IAS2_THRESHOLDMARGIN				= 'ias2_thresholdMargin';
	const IAS2_THRESHOLDMARGIN_DFL			= 0;
	const IAS2_THRESHOLDMARGIN_MIN			= -10000;
	const IAS2_THRESHOLDMARGIN_MAX			= 10000;
	const IAS2_HISTORY						= 'ias2_history';
	const IAS2_HISTORY_DFL					= 1; // true
	const IAS2_OTHER						= 'ias2_other';
	const IAS2_OTHER_DFL					= '';
	const IAS2_OTHER_ROWS					= 10;
	
	const IAS3_ENABLE						= 'ias3_enable';
	const IAS3_ENABLE_DFL					= 0; // false
	const IAS3_CONTAINER					= 'ias3_container';
	const IAS3_CONTAINER_DFL				= '';
	const IAS3_ITEM							= 'ias3_item';
	const IAS3_ITEM_DFL						= '';
	const IAS3_PAGINATION					= 'ias3_pagination';
	const IAS3_PAGINATION_DFL				= '.qa-page-links-list';
	const IAS3_NEXT							= 'ias3_next';
	const IAS3_NEXT_DFL						= '.qa-page-next';
	const IAS3_SCROLLCONTAINER				= 'ias3_scrollcontainer';
	const IAS3_SCROLLCONTAINER_DFL			= 'window';
	const IAS3_NONELEFT						= 'ias3_noneleft';
	const IAS3_NONELEFT_DFL					= '';
	const IAS3_NONELEFT_ROWS				= 1;
	const IAS3_LOADERDELAY					= 'ias3_loaderDelay';
	const IAS3_LOADERDELAY_DFL				= 600;
	const IAS3_LOADERDELAY_MIN				= 0;
	const IAS3_LOADERDELAY_MAX				= 10000;
	const IAS3_TRIGGERPAGETHRESHOLD			= 'ias3_triggerPageThreshold';
	const IAS3_TRIGGERPAGETHRESHOLD_DFL		= 3;
	const IAS3_TRIGGERPAGETHRESHOLD_MIN		= 0;
	const IAS3_TRIGGERPAGETHRESHOLD_MAX		= 100;
	const IAS3_TRIGGER						= 'ias3_trigger';
	const IAS3_THRESHOLDMARGIN				= 'ias3_thresholdMargin';
	const IAS3_THRESHOLDMARGIN_DFL			= 0;
	const IAS3_THRESHOLDMARGIN_MIN			= -10000;
	const IAS3_THRESHOLDMARGIN_MAX			= 10000;
	const IAS3_HISTORY						= 'ias3_history';
	const IAS3_HISTORY_DFL					= 1; // true
	const IAS3_OTHER						= 'ias3_other';
	const IAS3_OTHER_DFL					= '';
	const IAS3_OTHER_ROWS					= 10;
	
	const LINK_CSS_FILES					= 'ias_link_css_files';
	const LINK_CSS_FILES_DFL				= 1;
	const LINK_JS_FILES						= 'ias_link_js_files';
	const LINK_JS_FILES_DFL					= 1;
	const LINK_JS_IAS						= 'ias_link_js_ias';
	const LINK_JS_IAS_DFL					= 1;
	
	const SAVE_BUTTON						= 'ias_save_button';
	const SAVED_MESSAGE						= 'ias_saved_message';
	const ERROR_MESSAGE						= 'ias_error_message';
	const RESET_BUTTON						= 'ias_reset_button';
	const RESET_CONFIRM						= 'ias_reset_confirm';
	const RESET_MESSAGE						= 'ias_reset_message';

	var $enable;
	
	var $ias1_enable;
	var $ias1_container;
	var $ias1_item;
	var $ias1_pagination;
	var $ias1_next;
	var $ias1_scrollcontainer;
	var $ias1_noneleft;
	var $ias1_loaderDelay;
	var $ias1_triggerPageThreshold;
	var $ias1_thresholdMargin;
	var $ias1_history;
	var $ias1_other;

	var $ias2_enable;
	var $ias2_container;
	var $ias2_item;
	var $ias2_pagination;
	var $ias2_next;
	var $ias2_scrollcontainer;
	var $ias2_noneleft;
	var $ias2_loaderDelay;
	var $ias2_triggerPageThreshold;
	var $ias2_thresholdMargin;
	var $ias2_history;
	var $ias2_other;

	var $ias3_enable;
	var $ias3_container;
	var $ias3_item;
	var $ias3_pagination;
	var $ias3_next;
	var $ias3_scrollcontainer;
	var $ias3_noneleft;
	var $ias3_loaderDelay;
	var $ias3_triggerPageThreshold;
	var $ias3_thresholdMargin;
	var $ias3_history;
	var $ias3_other;
	
	var $l_css;
	var $l_js;
	var $l_iasjs;

	var $pluginurl;
	var $plugincssurl;
	var $pluginjsurl;
	
	var $directory;
	var $urltoroot;

	function qa_ias() {
		$this->enable = $this->opt(self::ENABLE);
		
		$this->ias1_enable = $this->opt(self::IAS1_ENABLE);
		$this->ias1_container = $this->opt(self::IAS1_CONTAINER);
		$this->ias1_item = $this->opt(self::IAS1_ITEM);
		$this->ias1_pagination = $this->opt(self::IAS1_PAGINATION);
		$this->ias1_next = $this->opt(self::IAS1_NEXT);
		$this->ias1_scrollcontainer = $this->opt(self::IAS1_SCROLLCONTAINER);
		$this->ias1_noneleft = $this->opt(self::IAS1_NONELEFT);
		$this->ias1_loaderDelay = $this->opt(self::IAS1_LOADERDELAY);
		$this->ias1_triggerPageThreshold = $this->opt(self::IAS1_TRIGGERPAGETHRESHOLD);
		$this->ias1_thresholdMargin = $this->opt(self::IAS1_THRESHOLDMARGIN);
		$this->ias1_history = $this->opt(self::IAS1_HISTORY);
		$this->ias1_other = $this->opt(self::IAS1_OTHER);
		
		$this->ias2_enable = $this->opt(self::IAS2_ENABLE);
		$this->ias2_container = $this->opt(self::IAS2_CONTAINER);
		$this->ias2_item = $this->opt(self::IAS2_ITEM);
		$this->ias2_pagination = $this->opt(self::IAS2_PAGINATION);
		$this->ias2_next = $this->opt(self::IAS2_NEXT);
		$this->ias2_scrollcontainer = $this->opt(self::IAS2_SCROLLCONTAINER);
		$this->ias2_noneleft = $this->opt(self::IAS2_NONELEFT);
		$this->ias2_loaderDelay = $this->opt(self::IAS2_LOADERDELAY);
		$this->ias2_triggerPageThreshold = $this->opt(self::IAS2_TRIGGERPAGETHRESHOLD);
		$this->ias2_thresholdMargin = $this->opt(self::IAS2_THRESHOLDMARGIN);
		$this->ias2_history = $this->opt(self::IAS2_HISTORY);
		$this->ias2_other = $this->opt(self::IAS2_OTHER);
		
		$this->ias3_enable = $this->opt(self::IAS3_ENABLE);
		$this->ias3_container = $this->opt(self::IAS3_CONTAINER);
		$this->ias3_item = $this->opt(self::IAS3_ITEM);
		$this->ias3_pagination = $this->opt(self::IAS3_PAGINATION);
		$this->ias3_next = $this->opt(self::IAS3_NEXT);
		$this->ias3_scrollcontainer = $this->opt(self::IAS3_SCROLLCONTAINER);
		$this->ias3_noneleft = $this->opt(self::IAS3_NONELEFT);
		$this->ias3_loaderDelay = $this->opt(self::IAS3_LOADERDELAY);
		$this->ias3_triggerPageThreshold = $this->opt(self::IAS3_TRIGGERPAGETHRESHOLD);
		$this->ias3_thresholdMargin = $this->opt(self::IAS3_THRESHOLDMARGIN);
		$this->ias3_history = $this->opt(self::IAS3_HISTORY);
		$this->ias3_other = $this->opt(self::IAS3_OTHER);
		
		$this->l_css = $this->opt(self::LINK_CSS_FILES);
		$this->l_js = $this->opt(self::LINK_JS_FILES);
		$this->l_iasjs = $this->opt(self::LINK_JS_IAS);
		
		$this->pluginurl = $this->opt('site_url').ISLAMIQA_IAS_PLUGIN_BASE_DIR_NAME;
		$this->plugincssurl = $this->pluginurl.'css/';
		$this->pluginjsurl = $this->pluginurl.'js/';
	}
	function load_module($directory, $urltoroot) {
		$this->directory=$directory;
		$this->urltoroot=$urltoroot;
	}
	function init_queries($tableslc) {
		return null;
	}
	function option_default($option) {
		if ($option==self::ENABLE) return (int)self::ENABLE_DFL;
		
		if ($option==self::IAS1_ENABLE) return self::IAS1_ENABLE_DFL;
		if ($option==self::IAS1_CONTAINER) return self::IAS1_CONTAINER_DFL;
		if ($option==self::IAS1_ITEM) return self::IAS1_ITEM_DFL;
		if ($option==self::IAS1_PAGINATION) return self::IAS1_PAGINATION_DFL;
		if ($option==self::IAS1_NEXT) return self::IAS1_NEXT_DFL;
		if ($option==self::IAS1_SCROLLCONTAINER) return self::IAS1_SCROLLCONTAINER_DFL;
		if ($option==self::IAS1_NONELEFT) return self::IAS1_NONELEFT_DFL;
		if ($option==self::IAS1_LOADERDELAY) return (int)self::IAS1_LOADERDELAY_DFL;
		if ($option==self::IAS1_TRIGGERPAGETHRESHOLD) return (int)self::IAS1_TRIGGERPAGETHRESHOLD_DFL;
		if ($option==self::IAS1_THRESHOLDMARGIN) return (int)self::IAS1_THRESHOLDMARGIN_DFL;
		if ($option==self::IAS1_HISTORY) return (int)self::IAS1_HISTORY_DFL;
		if ($option==self::IAS1_OTHER) return self::IAS1_OTHER_DFL;
		
		if ($option==self::IAS2_ENABLE) return self::IAS2_ENABLE_DFL;
		if ($option==self::IAS2_CONTAINER) return self::IAS2_CONTAINER_DFL;
		if ($option==self::IAS2_ITEM) return self::IAS2_ITEM_DFL;
		if ($option==self::IAS2_PAGINATION) return self::IAS2_PAGINATION_DFL;
		if ($option==self::IAS2_NEXT) return self::IAS2_NEXT_DFL;
		if ($option==self::IAS2_SCROLLCONTAINER) return self::IAS2_SCROLLCONTAINER_DFL;
		if ($option==self::IAS2_NONELEFT) return self::IAS2_NONELEFT_DFL;
		if ($option==self::IAS2_LOADERDELAY) return (int)self::IAS2_LOADERDELAY_DFL;
		if ($option==self::IAS2_TRIGGERPAGETHRESHOLD) return (int)self::IAS2_TRIGGERPAGETHRESHOLD_DFL;
		if ($option==self::IAS2_THRESHOLDMARGIN) return (int)self::IAS2_THRESHOLDMARGIN_DFL;
		if ($option==self::IAS2_HISTORY) return (int)self::IAS2_HISTORY_DFL;
		if ($option==self::IAS2_OTHER) return self::IAS2_OTHER_DFL;
		
		if ($option==self::IAS3_ENABLE) return self::IAS3_ENABLE_DFL;
		if ($option==self::IAS3_CONTAINER) return self::IAS3_CONTAINER_DFL;
		if ($option==self::IAS3_ITEM) return self::IAS3_ITEM_DFL;
		if ($option==self::IAS3_PAGINATION) return self::IAS3_PAGINATION_DFL;
		if ($option==self::IAS3_NEXT) return self::IAS3_NEXT_DFL;
		if ($option==self::IAS3_SCROLLCONTAINER) return self::IAS3_SCROLLCONTAINER_DFL;
		if ($option==self::IAS3_NONELEFT) return self::IAS3_NONELEFT_DFL;
		if ($option==self::IAS3_LOADERDELAY) return (int)self::IAS3_LOADERDELAY_DFL;
		if ($option==self::IAS3_TRIGGERPAGETHRESHOLD) return (int)self::IAS3_TRIGGERPAGETHRESHOLD_DFL;
		if ($option==self::IAS3_THRESHOLDMARGIN) return (int)self::IAS3_THRESHOLDMARGIN_DFL;
		if ($option==self::IAS3_HISTORY) return (int)self::IAS3_HISTORY_DFL;
		if ($option==self::IAS3_OTHER) return self::IAS3_OTHER_DFL;
		
		if ($option==self::LINK_CSS_FILES) return self::LINK_CSS_FILES_DFL;
		if ($option==self::LINK_JS_FILES) return self::LINK_JS_FILES_DFL;
		if ($option==self::LINK_JS_IAS) return self::LINK_JS_IAS_DFL;
	}
	function admin_form(&$qa_content) {
		$saved = '';
		$error = array();
		// Save event
		if (qa_clicked(self::SAVE_BUTTON)) {
			qa_opt(self::ENABLE, (int)qa_post_text(self::ENABLE.'_field'));
			
			qa_opt(self::IAS1_ENABLE, (int)qa_post_text(self::IAS1_ENABLE.'_field'));
			if ((int)qa_post_text(self::IAS1_ENABLE.'_field')) {
				$this->check_required(self::IAS1_CONTAINER, $error);
				$this->check_required(self::IAS1_ITEM, $error);
				$this->check_required(self::IAS1_PAGINATION, $error);
				$this->check_required(self::IAS1_NEXT, $error);
				$this->check_required(self::IAS1_SCROLLCONTAINER, $error);
				qa_opt(self::IAS1_NONELEFT, qa_post_text(self::IAS1_NONELEFT.'_field'));
				$this->check_number(self::IAS1_LOADERDELAY, self::IAS1_LOADERDELAY_MIN, self::IAS1_LOADERDELAY_MAX, $error);
				$this->check_number(self::IAS1_TRIGGERPAGETHRESHOLD, self::IAS1_TRIGGERPAGETHRESHOLD_MIN, self::IAS1_TRIGGERPAGETHRESHOLD_MAX, $error);
				$this->check_number(self::IAS1_THRESHOLDMARGIN, self::IAS1_THRESHOLDMARGIN_MIN, self::IAS1_THRESHOLDMARGIN_MAX, $error);
				qa_opt(self::IAS1_HISTORY, (int)qa_post_text(self::IAS1_HISTORY.'_field'));
				qa_opt(self::IAS1_OTHER, qa_post_text(self::IAS1_OTHER.'_field'));
			}
			qa_opt(self::IAS2_ENABLE, (int)qa_post_text(self::IAS2_ENABLE.'_field'));
			if ((int)qa_post_text(self::IAS2_ENABLE.'_field')) {
				$this->check_required(self::IAS2_CONTAINER, $error);
				$this->check_required(self::IAS2_ITEM, $error);
				$this->check_required(self::IAS2_PAGINATION, $error);
				$this->check_required(self::IAS2_NEXT, $error);
				$this->check_required(self::IAS2_SCROLLCONTAINER, $error);
				qa_opt(self::IAS2_NONELEFT, qa_post_text(self::IAS2_NONELEFT.'_field'));
				$this->check_number(self::IAS2_LOADERDELAY, self::IAS2_LOADERDELAY_MIN, self::IAS2_LOADERDELAY_MAX, $error);
				$this->check_number(self::IAS2_TRIGGERPAGETHRESHOLD, self::IAS2_TRIGGERPAGETHRESHOLD_MIN, self::IAS2_TRIGGERPAGETHRESHOLD_MAX, $error);
				$this->check_number(self::IAS2_THRESHOLDMARGIN, self::IAS2_THRESHOLDMARGIN_MIN, self::IAS2_THRESHOLDMARGIN_MAX, $error);
				qa_opt(self::IAS2_HISTORY, (int)qa_post_text(self::IAS2_HISTORY.'_field'));
				qa_opt(self::IAS2_OTHER, qa_post_text(self::IAS2_OTHER.'_field'));
			}
			qa_opt(self::IAS3_ENABLE, (int)qa_post_text(self::IAS3_ENABLE.'_field'));
			if ((int)qa_post_text(self::IAS3_ENABLE.'_field')) {
				$this->check_required(self::IAS3_CONTAINER, $error);
				$this->check_required(self::IAS3_ITEM, $error);
				$this->check_required(self::IAS3_PAGINATION, $error);
				$this->check_required(self::IAS3_NEXT, $error);
				$this->check_required(self::IAS3_SCROLLCONTAINER, $error);
				qa_opt(self::IAS3_NONELEFT, qa_post_text(self::IAS3_NONELEFT.'_field'));
				$this->check_number(self::IAS3_LOADERDELAY, self::IAS3_LOADERDELAY_MIN, self::IAS3_LOADERDELAY_MAX, $error);
				$this->check_number(self::IAS3_TRIGGERPAGETHRESHOLD, self::IAS3_TRIGGERPAGETHRESHOLD_MIN, self::IAS3_TRIGGERPAGETHRESHOLD_MAX, $error);
				$this->check_number(self::IAS3_THRESHOLDMARGIN, self::IAS3_THRESHOLDMARGIN_MIN, self::IAS3_THRESHOLDMARGIN_MAX, $error);
				qa_opt(self::IAS3_HISTORY, (int)qa_post_text(self::IAS3_HISTORY.'_field'));
				qa_opt(self::IAS3_OTHER, qa_post_text(self::IAS3_OTHER.'_field'));
			}
			
			qa_opt(self::LINK_CSS_FILES, (int)qa_post_text(self::LINK_CSS_FILES.'_field'));
			qa_opt(self::LINK_JS_FILES, (int)qa_post_text(self::LINK_JS_FILES.'_field'));
			qa_opt(self::LINK_JS_IAS, (int)qa_post_text(self::LINK_JS_IAS.'_field'));
			
			if(!count($error)) {
				$saved = qa_lang(self::PLUGIN.'/'.self::SAVED_MESSAGE);
				global $qa_ias_option_cache;
				$qa_ias_option_cache = array();
				$this->qa_ias();
				if(qa_opt(self::ENABLE))
					$this->store_js();
			}else
				$saved = qa_lang(self::PLUGIN.'/'.self::ERROR_MESSAGE);
		}
		// Reset event
		if (qa_clicked(self::RESET_BUTTON)) {
			qa_opt(self::ENABLE, (int)self::ENABLE_DFL);
			
			qa_opt(self::IAS1_ENABLE, (int)self::IAS1_ENABLE_DFL);
			qa_opt(self::IAS1_CONTAINER, self::IAS1_CONTAINER_DFL);
			qa_opt(self::IAS1_ITEM, self::IAS1_ITEM_DFL);
			qa_opt(self::IAS1_PAGINATION, self::IAS1_PAGINATION_DFL);
			qa_opt(self::IAS1_NEXT, self::IAS1_NEXT_DFL);
			qa_opt(self::IAS1_SCROLLCONTAINER, self::IAS1_SCROLLCONTAINER_DFL);
			qa_opt(self::IAS1_NONELEFT, self::IAS1_NONELEFT_DFL);
			qa_opt(self::IAS1_LOADERDELAY, (int)self::IAS1_LOADERDELAY_DFL);
			qa_opt(self::IAS1_TRIGGERPAGETHRESHOLD, (int)self::IAS1_TRIGGERPAGETHRESHOLD_DFL);
			qa_opt(self::IAS1_THRESHOLDMARGIN, (int)self::IAS1_THRESHOLDMARGIN_DFL);
			qa_opt(self::IAS1_HISTORY, (int)self::IAS1_HISTORY_DFL);
			qa_opt(self::IAS1_OTHER, self::IAS1_OTHER_DFL);
			
			qa_opt(self::IAS2_ENABLE, (int)self::IAS2_ENABLE_DFL);
			qa_opt(self::IAS2_CONTAINER, self::IAS2_CONTAINER_DFL);
			qa_opt(self::IAS2_ITEM, self::IAS2_ITEM_DFL);
			qa_opt(self::IAS2_PAGINATION, self::IAS2_PAGINATION_DFL);
			qa_opt(self::IAS2_NEXT, self::IAS2_NEXT_DFL);
			qa_opt(self::IAS2_SCROLLCONTAINER, self::IAS2_SCROLLCONTAINER_DFL);
			qa_opt(self::IAS2_NONELEFT, self::IAS2_NONELEFT_DFL);
			qa_opt(self::IAS2_LOADERDELAY, (int)self::IAS2_LOADERDELAY_DFL);
			qa_opt(self::IAS2_TRIGGERPAGETHRESHOLD, (int)self::IAS2_TRIGGERPAGETHRESHOLD_DFL);
			qa_opt(self::IAS2_THRESHOLDMARGIN, (int)self::IAS2_THRESHOLDMARGIN_DFL);
			qa_opt(self::IAS2_HISTORY, (int)self::IAS2_HISTORY_DFL);
			qa_opt(self::IAS2_OTHER, self::IAS2_OTHER_DFL);
			
			qa_opt(self::IAS3_ENABLE, (int)self::IAS3_ENABLE_DFL);
			qa_opt(self::IAS3_CONTAINER, self::IAS3_CONTAINER_DFL);
			qa_opt(self::IAS3_ITEM, self::IAS3_ITEM_DFL);
			qa_opt(self::IAS3_PAGINATION, self::IAS3_PAGINATION_DFL);
			qa_opt(self::IAS3_NEXT, self::IAS3_NEXT_DFL);
			qa_opt(self::IAS3_SCROLLCONTAINER, self::IAS3_SCROLLCONTAINER_DFL);
			qa_opt(self::IAS3_NONELEFT, self::IAS3_NONELEFT_DFL);
			qa_opt(self::IAS3_LOADERDELAY, (int)self::IAS3_LOADERDELAY_DFL);
			qa_opt(self::IAS3_TRIGGERPAGETHRESHOLD, (int)self::IAS3_TRIGGERPAGETHRESHOLD_DFL);
			qa_opt(self::IAS3_THRESHOLDMARGIN, (int)self::IAS3_THRESHOLDMARGIN_DFL);
			qa_opt(self::IAS3_HISTORY, (int)self::IAS3_HISTORY_DFL);
			qa_opt(self::IAS3_OTHER, self::IAS3_OTHER_DFL);
			
			qa_opt(self::LINK_CSS_FILES, (int)self::LINK_CSS_FILES_DFL);
			qa_opt(self::LINK_JS_FILES, (int)self::LINK_JS_FILES_DFL);
			qa_opt(self::LINK_JS_IAS, (int)self::LINK_JS_IAS_DFL);
			
			$saved = qa_lang_html(self::PLUGIN.'/'.self::RESET_MESSAGE);
		}
		// Rules
		$rules = array();
		$rules[self::IAS1_ENABLE] = self::ENABLE.'_field';
		$rules[self::IAS_GUIDE] = self::ENABLE.'_field';
		
		$rules[self::IAS1_CONTAINER] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';
		$rules[self::IAS1_ITEM] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';
		$rules[self::IAS1_PAGINATION] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';
		$rules[self::IAS1_NEXT] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';
		$rules[self::IAS1_SCROLLCONTAINER] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';
		$rules[self::IAS1_NONELEFT] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';
		$rules[self::IAS1_LOADERDELAY] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';
		$rules[self::IAS1_TRIGGERPAGETHRESHOLD] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';
		$rules[self::IAS1_THRESHOLDMARGIN] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';
		$rules[self::IAS1_HISTORY] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';
		$rules[self::IAS1_OTHER] = self::ENABLE.'_field' . ' && '. self::IAS1_ENABLE.'_field';

		$rules[self::IAS2_ENABLE] = self::ENABLE.'_field';
		$rules[self::IAS2_CONTAINER] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';
		$rules[self::IAS2_ITEM] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';
		$rules[self::IAS2_PAGINATION] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';
		$rules[self::IAS2_NEXT] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';
		$rules[self::IAS2_SCROLLCONTAINER] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';
		$rules[self::IAS2_NONELEFT] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';
		$rules[self::IAS2_LOADERDELAY] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';
		$rules[self::IAS2_TRIGGERPAGETHRESHOLD] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';
		$rules[self::IAS2_THRESHOLDMARGIN] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';
		$rules[self::IAS2_HISTORY] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';
		$rules[self::IAS2_OTHER] = self::ENABLE.'_field' . ' && '. self::IAS2_ENABLE.'_field';

		$rules[self::IAS3_ENABLE] = self::ENABLE.'_field';
		$rules[self::IAS3_CONTAINER] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		$rules[self::IAS3_ITEM] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		$rules[self::IAS3_PAGINATION] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		$rules[self::IAS3_NEXT] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		$rules[self::IAS3_SCROLLCONTAINER] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		$rules[self::IAS3_NONELEFT] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		$rules[self::IAS3_LOADERDELAY] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		$rules[self::IAS3_TRIGGERPAGETHRESHOLD] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		$rules[self::IAS3_THRESHOLDMARGIN] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		$rules[self::IAS3_HISTORY] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		$rules[self::IAS3_OTHER] = self::ENABLE.'_field' . ' && '. self::IAS3_ENABLE.'_field';
		
		$rules[self::LINK_CSS_FILES] = self::ENABLE.'_field';
		$rules[self::LINK_JS_FILES] = self::ENABLE.'_field';
		$rules[self::LINK_JS_IAS] = self::ENABLE.'_field';

		qa_set_display_rules($qa_content, $rules);
		
		// Form output
		$form = array();
		
		if($saved != '')
			$form['ok'] = $saved;

		$form['fields'][] = $this->form_field_check(self::ENABLE, (int)qa_opt(self::ENABLE), @$error[self::ENABLE]);
		$form['fields'][] = $this->form_field_static(self::IAS_GUIDE, '');
		
		$form['fields'][] = $this->form_field_check(self::IAS1_ENABLE, (int)qa_opt(self::IAS1_ENABLE), @$error[self::IAS1_ENABLE]);
		$form['fields'][] = $this->form_field_text(self::IAS1_CONTAINER, $error);
		$form['fields'][] = $this->form_field_text(self::IAS1_ITEM, $error);
		$form['fields'][] = $this->form_field_text(self::IAS1_PAGINATION, $error);
		$form['fields'][] = $this->form_field_text(self::IAS1_NEXT, $error);
		$form['fields'][] = $this->form_field_text(self::IAS1_SCROLLCONTAINER, $error);
		$form['fields'][] = $this->form_field_textarea(self::IAS1_NONELEFT, self::IAS1_NONELEFT_ROWS, $error);
		$form['fields'][] = $this->form_field_number(self::IAS1_LOADERDELAY, $error);
		$form['fields'][] = $this->form_field_number(self::IAS1_TRIGGERPAGETHRESHOLD, $error);
		$form['fields'][] = $this->form_field_number(self::IAS1_THRESHOLDMARGIN, $error);
		$form['fields'][] = $this->form_field_check(self::IAS1_HISTORY, (int)qa_opt(self::IAS1_HISTORY), @$error[self::IAS1_HISTORY]);
		$form['fields'][] = $this->form_field_textarea(self::IAS1_OTHER, self::IAS1_OTHER_ROWS, $error);
		
		$form['fields'][] = $this->form_field_check(self::IAS2_ENABLE, (int)qa_opt(self::IAS2_ENABLE), @$error[self::IAS2_ENABLE]);
		$form['fields'][] = $this->form_field_text(self::IAS2_CONTAINER, $error);
		$form['fields'][] = $this->form_field_text(self::IAS2_ITEM, $error);
		$form['fields'][] = $this->form_field_text(self::IAS2_PAGINATION, $error);
		$form['fields'][] = $this->form_field_text(self::IAS2_NEXT, $error);
		$form['fields'][] = $this->form_field_text(self::IAS2_SCROLLCONTAINER, $error);
		$form['fields'][] = $this->form_field_textarea(self::IAS2_NONELEFT, self::IAS2_NONELEFT_ROWS, $error);
		$form['fields'][] = $this->form_field_number(self::IAS2_LOADERDELAY, $error);
		$form['fields'][] = $this->form_field_number(self::IAS2_TRIGGERPAGETHRESHOLD, $error);
		$form['fields'][] = $this->form_field_number(self::IAS2_THRESHOLDMARGIN, $error);
		$form['fields'][] = $this->form_field_check(self::IAS2_HISTORY, (int)qa_opt(self::IAS2_HISTORY), @$error[self::IAS2_HISTORY]);
		$form['fields'][] = $this->form_field_textarea(self::IAS2_OTHER, self::IAS2_OTHER_ROWS, $error);
		
		$form['fields'][] = $this->form_field_check(self::IAS3_ENABLE, (int)qa_opt(self::IAS3_ENABLE), @$error[self::IAS3_ENABLE]);
		$form['fields'][] = $this->form_field_text(self::IAS3_CONTAINER, $error);
		$form['fields'][] = $this->form_field_text(self::IAS3_ITEM, $error);
		$form['fields'][] = $this->form_field_text(self::IAS3_PAGINATION, $error);
		$form['fields'][] = $this->form_field_text(self::IAS3_NEXT, $error);
		$form['fields'][] = $this->form_field_text(self::IAS3_SCROLLCONTAINER, $error);
		$form['fields'][] = $this->form_field_textarea(self::IAS3_NONELEFT, self::IAS3_NONELEFT_ROWS, $error);
		$form['fields'][] = $this->form_field_number(self::IAS3_LOADERDELAY, $error);
		$form['fields'][] = $this->form_field_number(self::IAS3_TRIGGERPAGETHRESHOLD, $error);
		$form['fields'][] = $this->form_field_number(self::IAS3_THRESHOLDMARGIN, $error);
		$form['fields'][] = $this->form_field_check(self::IAS3_HISTORY, (int)qa_opt(self::IAS3_HISTORY), @$error[self::IAS3_HISTORY]);
		$form['fields'][] = $this->form_field_textarea(self::IAS3_OTHER, self::IAS3_OTHER_ROWS, $error);
		
		$form['fields'][] = $this->form_field_check(self::LINK_CSS_FILES, (int)qa_opt(self::LINK_CSS_FILES), @$error[self::LINK_CSS_FILES]);
		$form['fields'][] = $this->form_field_check(self::LINK_JS_FILES, (int)qa_opt(self::LINK_JS_FILES), @$error[self::LINK_JS_FILES]);
		$form['fields'][] = $this->form_field_check(self::LINK_JS_IAS, (int)qa_opt(self::LINK_JS_IAS), @$error[self::LINK_JS_IAS]);
		
		$form['buttons'][] = array(
			'label' => qa_lang_html(self::PLUGIN.'/'.self::SAVE_BUTTON),
			'tags' => 'NAME="'.self::SAVE_BUTTON.'" ID="'.self::SAVE_BUTTON.'"',
		);
		$form['buttons'][] = array(
			'label' => qa_lang_html(self::PLUGIN.'/'.self::RESET_BUTTON),
			'tags' => 'NAME="'.self::RESET_BUTTON.'" ID="'.self::RESET_BUTTON.'" onClick="javascript:return confirm(\''.qa_lang(self::PLUGIN.'/'.self::RESET_CONFIRM).'\')"',
		);
		return $form;
	}
	function opt($name) {
		global $qa_ias_option_cache;
		if(count($qa_ias_option_cache) == 0) {
			$results = qa_db_read_all_assoc(qa_db_query_sub('SELECT title, content FROM ^options WHERE title LIKE \''.self::PLUGIN.'_%\''));
			foreach($results as $result) {
				if (is_null($result['content']))
					$result['content'] = $this->option_default($result['title']);
				$qa_ias_option_cache[$result['title']] = $result['content'];
			}
		}
		if(is_array($qa_ias_option_cache) && array_key_exists($name, $qa_ias_option_cache)) {
			$result = $qa_ias_option_cache[$name];
		} else {
			$result = qa_db_read_one_value(qa_db_query_sub('SELECT content FROM ^options WHERE `title`=$', $name), true);
			if (is_null($result))
				$result = $this->option_default($name);
			$qa_ias_option_cache[$name] = $result;
		}
		return $result;
	}
	function form_field_check($name, $error) {
		return array(
			'type' => 'checkbox',
			'id' => $name,
			'tags' => 'NAME="'.$name.'_field" ID="'.$name.'_field"',
			'label' => qa_lang(self::PLUGIN.'/'.$name.'_label'),
			'value' => qa_opt($name),
			'note' => qa_lang(self::PLUGIN.'/'.$name.'_note'),
			'error' => @$error[$name],
		);
	}
	function form_field_select($name, $options, $error, $value=NULL) {
		if(is_null($value))
			$value = @$options[qa_opt($name)];
		return array(
			'type' => 'select',
			'id' => $name,
			'tags' => 'NAME="'.$name.'_field" ID="'.$name.'_field"',
			'label' => qa_lang(self::PLUGIN.'/'.$name.'_label'),
			'options' => $options,
			'value' => $value,
			'suffix' => qa_lang(self::PLUGIN.'/'.$name.'_suffix'),
			'note' => qa_lang(self::PLUGIN.'/'.$name.'_note'),
			'error' => @$error[$name],
		);
	}
	function form_field_number($name, $error) {
		return array(
			'type' => 'number',
			'id' => $name,
			'tags' => 'NAME="'.$name.'_field" ID="'.$name.'_field"',
			'label' => qa_lang(self::PLUGIN.'/'.$name.'_label'),
			'value' => qa_opt($name),
			'suffix' => qa_lang(self::PLUGIN.'/'.$name.'_suffix'),
			'note' => qa_lang(self::PLUGIN.'/'.$name.'_note'),
			'error' => @$error[$name],
		);
	}
	function form_field_text($name, $error) {
		return array(
			'type' => 'text',
			'id' => $name,
			'tags' => 'NAME="'.$name.'_field" ID="'.$name.'_field"',
			'label' => qa_lang(self::PLUGIN.'/'.$name.'_label'),
			'value' => qa_opt($name),
			'note' => qa_lang(self::PLUGIN.'/'.$name.'_note'),
			'error' => @$error[$name],
		);
	}
	function form_field_static($name, $value, $addfields=null) {
		$ret = array();
		$ret['type'] = 'static';
		$ret['id'] = $name;
		$ret['tags'] = 'NAME="'.$name.'_field" ID="'.$name.'_field"';
		$ret['label'] = qa_lang(self::PLUGIN.'/'.$name.'_label');
		$ret['value'] = $value;
		$ret['note'] = qa_lang(self::PLUGIN.'/'.$name.'_note');
		if(is_array($addfields)) {
			foreach($addfields as $key => $addfield)
				$ret[$key] = $addfield;
		}
		return $ret;
	}
	function form_field_textarea($name, $rows, $error) {
		return array(
			'type' => 'textarea',
			'id' => $name,
			'tags' => 'NAME="'.$name.'_field" ID="'.$name.'_field"',
			'label' => qa_lang(self::PLUGIN.'/'.$name.'_label'),
			'value' => qa_opt($name),
			'rows' => $rows,
			'note' => qa_lang(self::PLUGIN.'/'.$name.'_note'),
			'error' => @$error[$name],
		);
	}
	function check_number($name, $min, $max, &$error) {
		$value = qa_post_text($name.'_field');
		if (!is_numeric($value) || (int)$value < $min || (int)$value > $max) {
			$error[$name] = strtr(qa_lang(self::PLUGIN.'/'.$name.'_error'), array('^min' => $min, '^max' => $max));
		} else
			qa_opt($name, (int)qa_post_text($name.'_field'));
	}
	function check_required($name, &$error) {
		$value = qa_post_text($name.'_field');
		if ($value == '') {
			$error[$name] = qa_lang(self::PLUGIN.'/'.$name.'_error');
		} else
			qa_opt($name, qa_post_text($name.'_field'));
	}
	function store_js() {
		$js  = '';
		$js .= '$(function(){'."\n";
		if ($this->ias1_enable) {
			$js .= '	if($("'.$this->ias1_container.'").length && $("'.$this->ias1_pagination.'").length) {'."\n";
			$js .= '		$.ias({'."\n";
			$js .= '			container: "'.$this->ias1_container.'"'."\n";
			$js .= '			,item: "'.$this->ias1_item.'"'."\n";
			$js .= '			,pagination: "'.$this->ias1_pagination.'"'."\n";
			$js .= '			,next: "'.$this->ias1_next.'"'."\n";
			$js .= '			,loader: \'<img class="qa-ias-loader" src="'.$this->pluginjsurl.'images/loader.gif"/>\''."\n";
			$js .= '			,noneleft: \''.$this->ias1_noneleft.'\''."\n";
			$js .= '			,loaderDelay: '.$this->ias1_loaderDelay."\n";
			$js .= '			,triggerPageThreshold: '.$this->ias1_triggerPageThreshold."\n";
			$js .= '			,trigger: "'.qa_lang(self::PLUGIN.'/'.self::IAS1_TRIGGER).'"'."\n";
			$js .= '			,thresholdMargin: '.$this->ias1_thresholdMargin."\n";
			$js .= '			,history: '.$this->ias1_history."\n";
			$js .= '			,scrollContainer: $('.$this->ias1_scrollcontainer.')'."\n";
			if(!empty($this->ias1_other))
				$js .= '			,'.$this->ias1_other."\n";
			$js .= '		});'."\n";
			$js .= '	}'."\n";
		}
		if ($this->ias2_enable) {
			$js .= '	if($("'.$this->ias2_container.'").length && $("'.$this->ias2_pagination.'").length) {'."\n";
			$js .= '		$.ias({'."\n";
			$js .= '			container: "'.$this->ias2_container.'"'."\n";
			$js .= '			,item: "'.$this->ias2_item.'"'."\n";
			$js .= '			,pagination: "'.$this->ias2_pagination.'"'."\n";
			$js .= '			,next: "'.$this->ias2_next.'"'."\n";
			$js .= '			,loader: \'<img class="qa-ias-loader" src="'.$this->pluginjsurl.'images/loader.gif"/>\''."\n";
			$js .= '			,noneleft: \''.$this->ias2_noneleft.'\''."\n";
			$js .= '			,loaderDelay: '.$this->ias2_loaderDelay."\n";
			$js .= '			,triggerPageThreshold: '.$this->ias2_triggerPageThreshold."\n";
			$js .= '			,trigger: "'.qa_lang(self::PLUGIN.'/'.self::IAS1_TRIGGER).'"'."\n";
			$js .= '			,thresholdMargin: '.$this->ias2_thresholdMargin."\n";
			$js .= '			,history: '.$this->ias2_history."\n";
			$js .= '			,scrollContainer: $('.$this->ias2_scrollcontainer.')'."\n";
			if(!empty($this->ias2_other))
				$js .= '			,'.$this->ias2_other."\n";
			$js .= '		});'."\n";
			$js .= '	}'."\n";
		}
		if ($this->ias3_enable) {
			$js .= '	if($("'.$this->ias3_container.'").length && $("'.$this->ias3_pagination.'").length) {'."\n";
			$js .= '		$.ias({'."\n";
			$js .= '			container: "'.$this->ias3_container.'"'."\n";
			$js .= '			,item: "'.$this->ias3_item.'"'."\n";
			$js .= '			,pagination: "'.$this->ias3_pagination.'"'."\n";
			$js .= '			,next: "'.$this->ias3_next.'"'."\n";
			$js .= '			,loader: \'<img class="qa-ias-loader" src="'.$this->pluginjsurl.'images/loader.gif"/>\''."\n";
			$js .= '			,noneleft: \''.$this->ias3_noneleft.'\''."\n";
			$js .= '			,loaderDelay: '.$this->ias3_loaderDelay."\n";
			$js .= '			,triggerPageThreshold: '.$this->ias3_triggerPageThreshold."\n";
			$js .= '			,trigger: "'.qa_lang(self::PLUGIN.'/'.self::IAS1_TRIGGER).'"'."\n";
			$js .= '			,thresholdMargin: '.$this->ias3_thresholdMargin."\n";
			$js .= '			,history: '.$this->ias3_history."\n";
			$js .= '			,scrollContainer: $('.$this->ias3_scrollcontainer.')'."\n";
			if(!empty($this->ias3_other))
				$js .= '			,'.$this->ias3_other."\n";
			$js .= '		});'."\n";
			$js .= '	}'."\n";
		}
		$js .= '});'."\n";
		
		$jspath = $this->directory.'js/ias.js';
		file_put_contents($jspath, $js);
	}
}

/*
	Omit PHP closing tag to help avoid accidental output
*/