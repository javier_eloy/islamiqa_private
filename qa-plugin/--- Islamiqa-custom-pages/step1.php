<?php

require_once QA_INCLUDE_DIR . 'qa-app-users.php';
require_once QA_INCLUDE_DIR . 'qa-app-posts.php';

class class_step1 {

	var $directory;
	var $urltoroot;

	public function __construct() {

	}

	public function load_module($directory, $urltoroot) {
		$this->directory = $directory;
		$this->urltoroot = $urltoroot;
	}


	public function match_request($request) {
		$parts = explode('/', $request);

		return $parts[0] == 'step1';
	}

	public function process_request($request) {
		$qa_content = qa_content_prepare();

		unset($qa_content['widgets']);
		unset($qa_content['main']);
		unset($qa_content['direction']);
		unset($qa_content['logo']);
		unset($qa_content['sidebar']);
		unset($qa_content['sidepanel']);

		$qa_content['navigation']['main'] = array();
		$qa_content['navigation']['footer'] = array();
		$qa_content['sidebar'] = "";
		$step = '';
		$body = '
            <script type="text/javascript">
                $(document).ready(function() {
                    $(".container-outer").css({
                        "width": (($(".col-xs-8").width())-50),
                    });
					
                    $(".group").width( $(".col-xs-8").width() );

                    $(".group").css({
                        "height": (($(".col-xs-8").height())),
                    });
                });
            </script>
            <!-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- Step1 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -->
            <input type="hidden" value="' . $this->urltoroot . '" id="urltoroot">
            <div id="selected_step1"></div>
            <div id="selected_step2"></div>
            <div class="container" id="step1" onload="LoadStep1();"><!-- Start Step1 -->
                <div class="container"><!-- Start Header-->
                    <div class="row">
                        <div class="col-xs-12 slider-get">
                            <img class="dpn" src="' . $this->urltoroot . 'images/st0-slider.jpg">
                        </div>
                    </div>
                </div><!-- Ending Header-->
                <div class="clearfix colelem" id="slider-container"><!-- group -->
                    <div class="grpelem" id="line-top"><!-- simple frame --></div>
                    <div class="clearfix grpelem" id="slider-top"><!-- group -->
                        <div class="clearfix grpelem" id="text-container-1"><!-- column -->
                            <div class="bodytext clearfix colelem" id="text-title-1"><!-- content -->
                                <p>Pick at least 5 interests</p>
                            </div>
                            <div class="rounded-corners bodytext clearfix colelem" id="text-content-1-1"><!-- content -->
                                <p>Select topics you find interesting so we can show you interesting questions and answers.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-2"><!-- column -->
                            <div class="clearfix colelem" id="text-title-2"><!-- content -->
                                <p>Indicate your expertise</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-2"><!-- content -->
                                <p>Pick topics you know about so we can show you relevant unanswered questions.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-3"><!-- column -->
                            <div class="clearfix colelem" id="text-title-3"><!-- content -->
                                <p>Follow people</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-3"><!-- content -->
                                <p>Follow users with the same interests and expertise as you to see a dfferent point of view.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-4"><!-- column -->
                            <div class="clearfix colelem" id="text-title-4"><!-- content -->
                                <p>Ready to start !</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-4"><!-- content -->
                                <p>Start reading and sharing your knowledge with the world!</p>
                            </div>
                        </div>
                    </div>
                    <div class="gradient grpelem" id="step1-red-line"><!-- simple frame --></div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-1"><!-- group -->
                        <div class="rounded-corners clearfix grpelem" id="rounded-content-1"><!-- group -->
                            <div class="clearfix grpelem" id="rounded-text-1"><!-- content -->
                                <p>1</p>
                            </div>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-2"><!-- group -->
                        <div class="clearfix grpelem" id="rounded-text-2"><!-- content -->
                            <p>2</p>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-3"><!-- group -->
                        <div class="clearfix grpelem" id="rounded-text-3"><!-- content -->
                            <p>3</p>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-4"><!-- group -->
                        <div class="clearfix grpelem" id="rounded-text-4"><!-- content -->
                            <p>4</p>
                        </div>
                    </div>
                </div>
                <div class="container body"><!-- Start body step1 -->
                    <div class="row border-arround-noncondensed"><!-- Start main Table -->
                        <div class="col-xs-4 border-right-complete"><!-- Start Col Left -->
                            <li class="st-li tc"><span class="fa fa-angle-double-up" id="scrollUp"></span></li> <!-- Double arrow up -->
                            <div class="scr-register-step1 add-margin-rig-2px" id="list_step1"><!-- Start Options List -->
                            </div><!-- Ending Options List -->
                            <li class="st-li tc"><span class="fa fa-angle-double-down" id="scrollDown"></span></li> <!-- Double arrow down -->
                        </div><!-- Ending col left-->
                        <div class="col-xs-8" id="options_list_step1"><!-- Start Col Right-->
                            <div class="container-outer">
                                <div class="arrow-left" id="scrollLeft">
                                    <i class="fa fa-angle-double-left fa-3x"></i>
                                </div>
                                <div class="arrow-right" id="scrollRight">
                                    <i class="fa fa-angle-double-right fa-3x"></i>
                                </div>
                                <div class="fijo" id="block_step1">
                                </div>
                            </div>
                            <!--buttons-->
                            <div class="col-sm-12 btn-np-right">
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-previous btn-step loger-out-margins np hidden">Previous step</button>
                                </div>
                                <div class="col-xs-6">
                                    <button id="step1-next-button" type="button" class="btn btn-next btn-step complete np" onclick="gotostep2(); return false;" disabled="disabled">Next step</button>
                                </div>
                            </div>
                        </div><!-- Ending Col Right-->
                    </div><!-- Ending main Table -->
                </div><!-- Ending Body Step1-->
            </div><!-- Ending Step1 -->


            <!-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- Step2 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -->
            <div class="container" id="step2">
                <div id="selected_step2"></div>
                <div class="container"><!-- Header Step2 -->
                    <div class="row">
                        <div class="col-xs-12 slider-get">
                            <img class="dpn" src="' . $this->urltoroot . 'images/st2-slider.jpg">
                        </div>
                    </div>
                </div><!-- Ending Step2-->
                <div class="clearfix grpelem" id="slider-container"><!-- group -->
                    <div class="grpelem" id="line-top-2"><!-- simple frame --></div>
                    <div class="clearfix grpelem" id="slider-top"><!-- group -->
                        <div class="clearfix grpelem" id="text-container-1"><!-- column -->
                            <div class="bodytext clearfix colelem" id="text-title-1-2"><!-- content -->
                                <p>Pick at least 5 interests</p>
                            </div>
                            <div class="rounded-corners bodytext clearfix colelem" id="text-content-1"><!-- content -->
                                <p>Select topics you find interesting so we can show you interesting questions and answers.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-2"><!-- column -->
                            <div class="clearfix colelem" id="text-title-2-2"><!-- content -->
                                <p>Indicate your expertise</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-2-2"><!-- content -->
                                <p>Pick topics you know about so we can show you relevant unanswered questions.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-3"><!-- column -->
                            <div class="clearfix colelem" id="text-title-3"><!-- content -->
                                <p>Follow people</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-3"><!-- content -->
                                <p>Follow users with the same interests and expertise as you to see a dfferent point of view.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-4"><!-- column -->
                            <div class="clearfix colelem" id="text-title-4"><!-- content -->
                                <p>Ready to start !</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-4"><!-- content -->
                                <p>Start reading and sharing your knowledge with the world!</p>
                            </div>
                        </div>
                    </div>
                    <div class="grpelem" id="step2-red-line"><!-- simple frame --></div>
                    <div class="gradient grpelem" id="step2-red-line-2"><!-- simple frame --></div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-1"><!-- group -->
                        <div class="rounded-corners clearfix grpelem" id="rounded-content-1"><!-- group -->
                            <div class="clearfix grpelem" id="rounded-text-1"><!-- content -->
                                <p>1</p>
                            </div>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-2-2"><!-- group -->
                        <div class="rounded-corners clearfix grpelem" id="rounded-content-2"><!-- group -->
                            <div class="rounded-corners clearfix grpelem" id="rounded-content-2-2"><!-- group -->
                                <div class="clearfix grpelem" id="rounded-text-2-2"><!-- content -->
                                    <p>2</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-3"><!-- group -->
                        <div class="clearfix grpelem" id="rounded-text-3"><!-- content -->
                            <p>3</p>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-4"><!-- group -->
                        <div class="clearfix grpelem" id="rounded-text-4"><!-- content -->
                            <p>4</p>
                        </div>
                    </div>
                </div>
                <div class="container body">
                    <div class="row border-arround-noncondensed">
                        <div class="col-xs-4 border-right-complete"> <!-- Col Left options -->
                            <li class="st-li tc"><span class="fa fa-angle-double-up" id="scrollUpExt"></span></li>
                            <div class="scr-register-step1 add-margin-rig-2px" id="list_step2">
                                <li class="st-li shadow-topics-register-step1 border-top-register-step1">
                                    <div class="checkbox">
                                        <label>&nbsp;<input type="checkbox" value="">&nbsp;Beliefs</label>
                                    </div>
                                    <span class="fa fa-chevron-circle-right fa-lg"></span>
                                </li>
                            </div>
                            <li class="st-li tc"><span class="fa fa-angle-double-down" id="scrollDownExt"></span></li>
                        </div><!-- Col Left options -->
                        <div class="col-xs-8"><!-- Col Right  -->
                            <div class="container-outer">
                                <div class="arrow-left" id="scrollLeftExt">
                                    <i class="fa fa-angle-double-left fa-3x"></i>
                                </div>
                                <div class="arrow-right" id="scrollRightExt">
                                    <i class="fa fa-angle-double-right fa-3x"></i>
                                </div>
                                <div id="block_step2" class="<!--col-xs-12--> fijo"></div>
                            </div>
                            <!--buttons-->
                            <div class="col-sm-12 btn-np-right">
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-previous btn-step loger-out-margins complete np" onclick="gotostep1(); return false;">Previous step</button>
                                </div>
                                <div class="col-xs-6">
                                    <button id="step2-next-button" type="button" class="btn btn-next btn-step complete np" onclick="gotostep3(); return false;" disabled="disabled">Next step</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- Ending Main Table -->
                </div>
            </div>

            <!-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- Step3 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -->
            <div class="container" id="step3">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 slider-get">
                            <img class="dpn" src="' . $this->urltoroot . 'images/st1-slider.jpg">
                        </div>
                    </div>
                </div>
                <div class="clearfix grpelem" id="slider-container"><!-- group -->
                    <div class="clearfix grpelem" id="slider-top"><!-- group -->
                        <div class="clearfix grpelem" id="text-container-1"><!-- column -->
                            <div class="bodytext clearfix colelem" id="text-title-1-2"><!-- content -->
                                <p>Pick at least 5 interests</p>
                            </div>
                            <div class="rounded-corners bodytext clearfix colelem" id="text-content-1"><!-- content -->
                                <p>Select topics you find interesting so we can show you interesting questions and answers.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-2"><!-- column -->
                            <div class="clearfix colelem" id="text-title-2"><!-- content -->
                                <p>Indicate your expertise</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-2"><!-- content -->
                                <p>Pick topics you know about so we can show you relevant unanswered questions.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-3"><!-- column -->
                            <div class="clearfix colelem" id="text-title-3-3"><!-- content -->
                                <p>Follow people</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-3-3"><!-- content -->
                                <p>Follow users with the same interests and expertise as you to see a dfferent point of view.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-4"><!-- column -->
                            <div class="clearfix colelem" id="text-title-4"><!-- content -->
                                <p>Ready to start !</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-4"><!-- content -->
                                <p>Start reading and sharing your knowledge with the world!</p>
                            </div>
                        </div>
                    </div>
                    <div class="grpelem" id="step3-red-line"><!-- simple frame --></div>
                    <div class="gradient grpelem" id="step3-red-line-2"><!-- simple frame --></div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-1"><!-- group -->
                        <div class="rounded-corners clearfix grpelem" id="rounded-content-1"><!-- group -->
                            <div class="clearfix grpelem" id="rounded-text-1"><!-- content -->
                                <p>1</p>
                            </div>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-2-2"><!-- group -->
                        <div class="rounded-corners clearfix grpelem" id="rounded-content-2"><!-- group -->
                            <div class="rounded-corners clearfix grpelem" id="rounded-content-2-2"><!-- group -->
                                <div class="clearfix grpelem" id="rounded-text-2-2"><!-- content -->
                                    <p>2</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-3-3"><!-- group -->
                        <div class="rounded-corners clearfix grpelem" id="rounded-content-3"><!-- group -->
                            <div class="clearfix grpelem" id="rounded-text-3-3"><!-- content -->
                                <p>3</p>
                            </div>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-4"><!-- group -->
                        <div class="clearfix grpelem" id="rounded-text-4"><!-- content -->
                            <p>4</p>
                        </div>
                    </div>
                </div>
                <div class="container body">
                    <div class="row st-li">
                        <strong class="title">
                            + Top Scoring Users
                        </strong>
                        <a href="#" class="skip" onclick="gotostep4(); return false;">skip</a>
                    </div><!-- Row Border condensed -->
                    <!-- Start Body Repeat -->
                    <div class="col-xs-12" id="tofollow">
                        <!--Start Repeat-->
                        <!--End Repeat-->
                    </div>
                    <!-- Invite friends-->
                    <div class="col-xs-3">
                        <div class="col-xs-12 margin-top-5per">
                            <div class="row centered">
                                <strong class="invite-friends">
                                    <a href="#" id="invite-friends" data-toggle="modal" data-target="#send-mail">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Invite friends
                                    </a>
                                </strong>
                            </div><!-- Row Border condensed -->
                        </div>
                    </div>
                    <!-- modal sendmail -->
                    <div class="modal fade" id="send-mail">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form id="emailForm" type="post" class="form-horizontal">
                                    <div class="modal-header">
                                        <button class="close" type="button" data-dismiss="modal">
                                            <span>&times;</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Invite Friends</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label">Friend</label>
                                            <div class="col-xs-8">
                                                <input type="text" class="form-control" name="email[]" placeholder="Email" />
                                            </div>
                                            <div class="col-xs-1">
                                                <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <div class="form-group hide" id="emailTemplate">
                                            <div class="col-xs-8 col-xs-offset-2">
                                                <input type="text" class="form-control" name="email[]" placeholder="Email" />
                                            </div>
                                            <div class="col-xs-1">
                                                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-primary">Send</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End Body -->
                    <div class="row">
                        <div class="col-sm-12 btn-np-right">
                            <div class="col-xs-6">
                                <button type="button" class="btn btn-previous btn-step complete np" onclick="gotostep2(); return false;">Previous step</button>
                            </div>
                            <div class="col-xs-6">
                                <button type="button" class="btn btn-next btn-step complete np" onclick="gotostep4(); return false;">Next step</button>
                            </div>
                        </div>
                    </div>
                </div><!-- container -->
            </div>

            <!-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- Step4 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -->
            <div class="container" id="step4">
                <div class="container" align="center">
                    <div class="row" align="center">
                        <div class="col-xs-12 slider-get" align="center">
                            <img class="dpn" src="' . $this->urltoroot . 'images/get-start-page-slider.jpg">
                        </div>
                    </div>
                </div>
                <div class="clearfix colelem" id="slider-container"><!-- group -->
                    <div class="clearfix grpelem" id="slider-top"><!-- group -->
                        <div class="clearfix grpelem" id="text-container-1"><!-- column -->
                            <div class="bodytext clearfix colelem" id="text-title-1-2"><!-- content -->
                                <p>Pick at least 5 interests</p>
                            </div>
                            <div class="rounded-corners bodytext clearfix colelem" id="text-content-1"><!-- content -->
                                <p>Select topics you find interesting so we can show you interesting questions and answers.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-2"><!-- column -->
                            <div class="clearfix colelem" id="text-title-2"><!-- content -->
                                <p>Indicate your expertise</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-2"><!-- content -->
                                <p>Pick topics you know about so we can show you relevant unanswered questions.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-3"><!-- column -->
                            <div class="clearfix colelem" id="text-title-3"><!-- content -->
                                <p>Follow people</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-3"><!-- content -->
                                <p>Follow users with the same interests and expertise as you to see a dfferent point of view.</p>
                            </div>
                        </div>
                        <div class="clearfix grpelem" id="text-container-4"><!-- column -->
                            <div class="clearfix colelem" id="text-title-4-4"><!-- content -->
                                <p>Ready to start !</p>
                            </div>
                            <div class="bodytext clearfix colelem" id="text-content-4-4"><!-- content -->
                                <p>Start reading and sharing your knowledge with the world!</p>
                            </div>
                        </div>
                    </div>
                    <div class="grpelem" id="step4-red-line"><!-- simple frame --></div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-1"><!-- group -->
                        <div class="rounded-corners clearfix grpelem" id="rounded-content-1"><!-- group -->
                            <div class="clearfix grpelem" id="rounded-text-1"><!-- content -->
                                <p>1</p>
                            </div>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-2-2"><!-- group -->
                        <div class="rounded-corners clearfix grpelem" id="rounded-content-2"><!-- group -->
                            <div class="rounded-corners clearfix grpelem" id="rounded-content-2-2"><!-- group -->
                                <div class="clearfix grpelem" id="rounded-text-2-2"><!-- content -->
                                    <p>2</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-3-3"><!-- group -->
                        <div class="rounded-corners clearfix grpelem" id="rounded-content-3"><!-- group -->
                            <div class="clearfix grpelem" id="rounded-text-3-3"><!-- content -->
                                <p>3</p>
                            </div>
                        </div>
                    </div>
                    <div class="rounded-corners clearfix grpelem" id="rounded-container-4-4"><!-- group -->
                        <div class="rounded-corners clearfix grpelem" id="rounded-content-4"><!-- group -->
                            <div class="clearfix grpelem" id="rounded-text-4-4"><!-- content -->
                                <p>4</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix colelem" id="started-title"><!-- content -->
                    <p>Getting started is really simple !</p>
                </div>
                <div class="bodytext clearfix colelem" id="started-text"><!-- content -->
                    <p>This guide will quickly walk you through how to use Islamiqa. It\'s easy and fun !</p>
                </div>
                <div class="container" id="step4-2"><!-- container -->
                    <div class="col-xs-12 background-first-block">
                        <div class="col-xs-8 first-img" style="position: relative; right: 50px;">
                            <img src="' . $this->urltoroot . 'images/get-start-left.jpg">
                        </div>
                        <div class="col-xs-4 align-body-tour-page-text">
                            <p>Find the right questions to answer</p>
                        </div>
                        <div class="col-xs-4 align-body-tour-page-text2">
                            <p id="top-text">Your home page has recent questions and answers based on your interests and friends.</p>
                            <p id="top-text">Select "Write Answer" to add answers - pictures help bring an answer to life.</p>
                            <p id="top-text">The content you see on your home page is based on the interests you told us about as well as the people you follow. (We\'ve added a few cheeky friends to start you off.)</p>
                        </div>
                    </div>
                    <div class="col-xs-12 background-second-block">
                        <!--<div class="block-center-res ">-->
                            <div class="col-xs-4 text-second-block">
                                <p>Vote on questions and answers</p>
                            </div>
                            <div class="col-xs-4 align-body-tour-page-text2-2">
                           to      <p id="top-text">You can vote questions and content up or down.</p>
                                <p id="top-text">It helps us to improve the questions and answers we show to users by filtering out weak content and promoting quality content.</p>
                                <p id="top-text">With your help the quality of content will improve!</p>
                            </div>
                            <div class="col-xs-8 second-img">
                                <img src="' . $this->urltoroot . 'images/get-start-right.jpg">
                            <!--</div>-->
                        </div>
                    </div>
                    <div class="col-xs-12 background-first-block">
                        <div class="col-xs-8"style="position:relative; right: 30px;">
                            <img src="' . $this->urltoroot . 'images/get-start-left-bottom.jpg">
                        </div>
                        <div class="col-xs-4 align-body-tour-page-text adjunt-text">
                            <p>Help others to understand</p>
                        </div>
                        <div class="col-xs-4 align-body-tour-page-text3">
                            <p id="top-text">Islamiqa is about people helping each other to make sense of questions.</p>
                            <p id="top-text">The FAQ section provides lots of help and advice to help you make the most of your experience.</p>
                            <p id="top-text">It is a great place to start and feel free to add any tips you think may be missing</p>
                        </div>
                    </div>
                    <!--finish button-->
                    <div class="col-sm-12 btn-finish">
                        <div class="col-xs-6">
                            <a class="btn btn-next btn-step complete np" id="finish-button" href="?qa=questions&sort=views">Finish</a>
                        </div>
                    </div>
                </div>
            </div>

            <!--script-->
            <script>
            $( document ).ready(function() {
                LoadStep1();
            });

            //hide steps 2, 3 and 4
            $("#step2").hide();
            $("#step3").hide();
            $("#step4").hide();

            //Go To Step1
            function gotostep1(){
                $("#step1").show();
                $("#step2").hide();
                $("#step3").hide();
                $("#step4").hide();
            }

            //Go To Step2
            function gotostep2(){
                $("#step3").hide();
                $("#step4").hide();
                $("#step1").hide();
                $("#step2").show();
                LoadStep2();
                window.scrollTo(0, 0);
            }

            //Go To Step3
            function gotostep3(){
                $("#step1").hide();
                $("#step2").hide();
                $("#step4").hide();
                $("#step3").show();
                var urltoroot = $("#urltoroot").val();
                LoadStep3(urltoroot);
                window.scrollTo(0, 0);
            }

            //Go To Step4
            function gotostep4(){
                $("#step1").hide();
                $("#step2").hide();
                $("#step3").hide();
                $("#step4").show();
                LoadSaveEvent();
                window.scrollTo(0, 0);
            }
            </script>
        ';

		$qa_content['custom'] = $step . $body;

		return $qa_content;
	}
	

}

/*
Omit PHP closing tag to help avoid accidental output
*/
