<?php

/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 12/07/16
 * Time: 02:15 PM
 */

require_once QA_INCLUDE_DIR . 'db/selects.php';
require_once QA_INCLUDE_DIR . 'app/format.php';
require_once QA_INCLUDE_DIR . 'app/q-list.php';
require_once QA_INCLUDE_DIR . 'app/users.php';
require_once QA_INCLUDE_DIR . 'qa-base.php';
require_once QA_INCLUDE_DIR . 'app/votes.php';
require_once QA_INCLUDE_DIR.'app/cookies.php';
require_once QA_INCLUDE_DIR.'app/favorites.php';

class data_core
{


    public function questions($userid, $start, $sort, $categoryslugs = null, $pageSize = 10)
    {

        $countslugs = count($categoryslugs);
        
        switch ($sort) {
            case 'hot':
                $selectsort = 'hotness';
                break;
            case 'recent':
                $selectsort = 'created';
                break;
            case 'votes':
                $selectsort = 'netvotes';
                break;
            case 'answers':
                $selectsort = 'acount';
                break;
            case 'views':
                $selectsort = 'views';
                break;
            default:
                $selectsort = 'created';
                break;
        }

        // search questios sor by criteria
        $result = qa_db_select_with_pending(
            qa_db_qs_selectspec($userid, $selectsort, $start, $categoryslugs, null, false, false, $pageSize),
            qa_db_category_nav_selectspec($categoryslugs, false, false, true),
            $countslugs ? qa_db_slugs_to_category_id_selectspec($categoryslugs) : null
        );

        $ready = [];
        // search answer of question and aditional information

        foreach ($result[0] as $key => $value) {
            $ready[] = $this->question($result[0][$key],$userid);
        }

        $result = [];
        $result['userid'] = $userid;
        $result['data'] = $ready;

        return $result;

    }


    private function question($question,$userid){

//        $questionid = $result[0][$key]['postid'];
        $questionid = $question['postid'];
        $admin = qa_get_logged_in_level();

        list($question, $childposts, $achildposts, $parentquestion, $closepost, $extravalue, $categories, $favorite) = qa_db_select_with_pending(
            qa_db_full_post_selectspec($userid, $questionid),
            qa_db_full_child_posts_selectspec($userid, $questionid),
            qa_db_full_a_child_posts_selectspec($userid, $questionid),
            qa_db_post_parent_q_selectspec($questionid),
            qa_db_post_close_post_selectspec($questionid),
            qa_db_post_meta_selectspec($questionid, 'qa_q_extra'),
            qa_db_category_nav_selectspec($questionid, true, true, true),
            isset($userid) ? qa_db_is_favorite_selectspec($userid, QA_ENTITY_QUESTION, $questionid) : null
        );

        $answer = [];
        foreach ($childposts as $answerKey => $answervalue) {
            $answer[] = $childposts[$answerKey];
        }

        $question['user_access'] = $admin;
        $question['countcomments'] = $this->count_comments($questionid)[0]['count_comments'];
        $question['countviwed'] = $this->count_viwed($questionid);
        $question['listTags'] = explode(",", $question['tags']);
        $question['answer'] = $answer;
        $question['favorite'] = $favorite;
        $question['time_ago'] = qa_when_to_html($question['created'], null);
        if (isset($question['avatarblobid'])) {
            $question['user_logo'] = qa_path_absolute('') . "?qa=image&qa_blobid=" . $question['avatarblobid'];
        } else {
//                $result[0][$key]['user_logo'] = "http://civ.iqsociety.org/wp-content/uploads/2014/11/profile_icon-252x300.png";
            $question['user_logo'] = "https://placeholdit.imgix.net/~text?txtsize=30&bg=f7f7f7&txt=&w=150&h=150";
        }
        
        return $question;
    }



    public function count_comments($postid)
    {

        return qa_db_read_all_assoc(qa_db_query_sub(
            "select " .
//            " (select count(*) from  ^posts as ans where ans.parentid = ^posts.postid and ans.type='A') as count_answ,  " .
            " (select count(*) from  ^posts as com where com.type='C' and parentid in( " .
            " (select ans.postid from  ^posts as ans where ans.type='A' and ans.parentid = qa_posts.postid))) as count_comments " .
            " from   ^posts where type='Q' and  postid =  " . $postid));

    }

    public function count_viwed($postid)
    {


        $query = "SELECT COUNT(postid) as count FROM ^userviewedquestions WHERE  postid = #";
        $result = qa_db_read_one_assoc(qa_db_query_sub($query, $postid), true);
        return $result['count'];

    }

    public function vote($userid, $postid, $vote ,$post_parent)
    {

        // $post_parent = isset($post_parent) ? $post_parent : $postid;

        $post = qa_db_select_with_pending(qa_db_full_post_selectspec($userid, $postid));
        $qa_page_error_html = qa_vote_error_html($post, $vote, $userid, qa_request());

        if (!$qa_page_error_html) {
            qa_vote_set($post, $userid, qa_get_logged_in_handle(), qa_cookie_get(), $vote);
            $post = qa_db_select_with_pending(qa_db_full_post_selectspec($userid, $post_parent));
            $post = $this->question($post,$userid);
            return $post;
        }

        throw new Exception($qa_page_error_html);


    }

    public function favorite($userid,$handle,$cookie, $entitytype,$entityid,$favorite,$post_parent){

        qa_user_favorite_set($userid,$handle,$cookie, $entitytype,$entityid,$favorite);
       $post = (qa_db_select_with_pending(qa_db_full_post_selectspec($userid, $post_parent)));
       $post = $this->question($post,$userid);
       return $post;
        

    }

    public function user_points($userid){

        list($useraccount, $userprofile, $userfields, $userpoints, $userlevels, $navcategories, $userrank) =
            qa_db_select_with_pending(
               qa_db_user_account_selectspec($userid, $userid),
                 qa_db_user_profile_selectspec($userid, $userid),
                 qa_db_userfields_selectspec(),
                qa_db_user_points_selectspec($userid,$userid),
                qa_db_user_levels_selectspec($userid, $userid, true),
                qa_db_category_nav_selectspec(null, true),
                qa_db_user_rank_selectspec($userid,$userid)
            );

         $result=[];

        $result['userpoints']['voted_on_questions']= @$userpoints['qupvotes']+@$userpoints['qdownvotes'];
        $result['userpoints']['voted_on_answers']= @$userpoints['qupvotes']+@$userpoints['qdownvotes'];
        $result['userpoints']['gave_out_up_vote']= @$userpoints['qupvotes']+@$userpoints['aupvotes'];
        $result['userpoints']['gave_down_votes']= @$userpoints['qdownvotes']+@$userpoints['adownvotes'];
        $result['userpoints']['received_up_votes']=@$userpoints['upvoteds'];
        $result['userpoints']['received_down_votes']= @$userpoints['downvoteds'];
        $result['userpoints']['score']= @$userpoints['points'];
        $result['userpoints']['questions']= @$userpoints['qposts'];
        $result['userpoints']['answers']= @$userpoints['aposts'];
//        $result['userlevels'] = $userlevels;
//        $result['navcategories'] = $navcategories;
        $result['userrank'] = $userrank;



        return $result;

    }


}