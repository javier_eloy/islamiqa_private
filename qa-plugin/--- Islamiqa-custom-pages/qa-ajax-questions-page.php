<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 09/07/16
 * Time: 09:46 AM
 */


class qa_ajax_question_page{


    private $directory;
//    private $urltoroot;


    public function load_module($directory, $urltoroot)
    {
        $this->directory =$directory;
    }


    public function match_request($request)
    {

        return strpos($request,"qajax-question")!==FALSE;
    }


    public function process_request($request)
    {

        require_once($this->directory .'data-core.php');
        
//        $categoryslugs = qa_request_parts(1);
        $categoryslugs = qa_get('category');
        $countslugs = count($categoryslugs);

        $sort = ($countslugs && !QA_ALLOW_UNINDEXED_QUERIES) ? null : qa_get('sort');
        $start = qa_get_start();
        $userid = qa_get_logged_in_userid();
        $pageSize = qa_get('pageSize');
        $pageSize = isset($pageSize) ? (int)$pageSize : 10;

        $questions= (new data_core())->questions($userid,$start,$sort,$categoryslugs,$pageSize);
        header('Content-Type: application/json');
        print json_encode(array("result"=>$questions));
    }



}