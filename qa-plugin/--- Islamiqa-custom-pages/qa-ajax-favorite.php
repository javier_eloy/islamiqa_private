<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 25/07/16
 * Time: 07:47 PM
 */

class qa_ajax_favorite
{

    private $directory;


    public function load_module($directory, $urltoroot)
    {
        $this->directory =$directory;
    }


    public function match_request($request)
    {

        return strpos($request,"qajax-favorite")!==FALSE;
    }


    public function process_request($request)
    {

        header('Content-Type: application/json');

        $entitytype=qa_post_text('entitytype');
        $entityid=qa_post_text('entityid');
        $favorite=qa_post_text('favorite');
        $post_parent =  qa_post_text("parent");
        $userid=qa_get_logged_in_userid();


        if (!(isset($entitytype) && isset($entityid) && isset($favorite))) {
            http_response_code(422);
            print json_encode(array("result"=>"Enter data"));
            return;
        }

        if(!isset($userid))
        {
            http_response_code(422);
            print json_encode(array("result"=>qa_insert_login_links(qa_lang_html('misc/message_must_login'), qa_request())));
            return;
        }

        if(isset($userid))
        {
            require_once($this->directory .'data-core.php');
            $questions= (new data_core())->favorite(qa_get_logged_in_userid(), qa_get_logged_in_handle(), qa_cookie_get(), $entitytype, $entityid, $favorite,$post_parent);
            print json_encode(array("result"=>$questions));
        }


    }
    
    
}