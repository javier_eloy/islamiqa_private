<?php 

//librerias
require_once '../../qa-include/qa-base.php';
require_once QA_INCLUDE_DIR.'qa-app-users.php';
require_once QA_INCLUDE_DIR.'qa-app-posts.php';
require_once QA_INCLUDE_DIR . 'db/selects.php';

error_reporting(E_ALL & ~E_NOTICE);

//Validando que la variable flag exista y NO este vacia.
if (isset($_POST['flag']) && !empty($_POST['flag']) ) {
	//Captura de la variable bandera
	$flag = $_POST['flag'];

	//Llamado de las funciones
	//Flags de Step1
	if ($flag == 'LoadStep1') { LoadStep1(); }

	if ($flag == 'LoadStep1Especific') { 
		$value='';
		$value = $_POST['value'];
		LoadStep1Especific($value);
	}

	//Flags des step 2
	if ($flag == 'LoadStep2') { LoadStep2(); }

	if ($flag == 'LoadStep2Especific') { 
		$value='';
		$value = $_POST['value'];
		LoadStep2Especific($value);
	}

	if ($flag == 'LoadStep3') { 
		$value='';
		$value = $_POST['value'];
		LoadStep3($value); 
	}
        
    if ($flag == 'LoadTopicList') { LoadTopicList(); }
    
    if ($flag == 'LoadUsersList') { LoadUsersList(); }

	if ($flag == 'saveAllData') {
		$interests = $_POST['interests'];
		$expertices = $_POST['$expertices'];
		$follows = $_POST['follows'];
		saveAllData($interests, $expertices, $follows);
	}

	if ($flag == 'send_friends_mails') {
		$emails = $_POST['emails'];
		send_friends_mails($emails);
	}
}
 
//Function load step1
function LoadStep1($value ="") {
	$topics = qa_db_read_all_assoc(qa_db_query_sub(
		'SELECT id as id_topic, title, parentid, type as type_topic From ^islamiqa_topics WHERE type = "t" ORDER BY title ASC'
	));
	$flag_col_left_step1 = true;
	$col_left_step1 ='';
	$value='';
    $border_top_register_step1 = 'border-top-register-step1';

	/**
	 * Col-Right-Step1
	 */
	$col_right_step1 = '';
	$sub_topics = qa_db_read_all_assoc(qa_db_query_sub(
	    'SELECT id as id_topic, title, parentid, type as type_topic From ^islamiqa_topics WHERE parentid = '.$value
	  ));
	$indice=0;

	foreach ($sub_topics as $sub_topic) {
		$col_right_step1 .= '<div class="six-boxes-gs-register ">
								<input type="checkbox" name ="topics['.$indice.']" value ='.$sub_topic["id_topic"].' id ='.$sub_topic["id_topic"].'
								onclick="VerifyCheckbox('.$sub_topic["id_topic"].', 1)">';
		$col_right_step1 .= '	<span class="fa-stack fa-lg fa-yellow">';
		$col_right_step1 .= '		<i class="fa fa-circle-o fa-stack-2x"></i>';
		$col_right_step1 .= '		<i class="fa fa-star fa-stack-1x"></i>';
		$col_right_step1 .= '	</span>';
		$col_right_step1 .= '	<div>';
		$col_right_step1 .= '		<p class="text-square-register-step1 underlined">'.$sub_topic['title'].'</p>';
		$col_right_step1 .= '	</div>';
		$col_right_step1 .= '</div>';
		$indice++;
	}
	$result['list_step1'] = '';
	$result['block_step1'] = '';

	$result['list_step1'] = $col_left_step1;
	$result['block_step1'] = $col_right_step1;
	print json_encode($result);
	return $result;
}


//Load options specific list
function LoadStep1Especific($value) {
	$col_right_step1 = '';

	$sub_topics = qa_db_read_all_assoc(qa_db_query_sub(
	    'SELECT id as id_topic, title, parentid, type as type_topic From ^islamiqa_topics WHERE parentid = '.$value
	  ));
	$indice=0;
	foreach ($sub_topics as $sub_topic) {
		$col_right_step1 .= '<div class="six-boxes-gs-register ">
								<input type="checkbox" name ="topics['.$indice.']" value ='.$sub_topic["id_topic"].' id ='.$sub_topic["id_topic"].'
								onclick="VerifyCheckbox('.$sub_topic["id_topic"].', 1)">';
		$col_right_step1 .= '	<span class="fa-stack fa-lg fa-yellow">';
		$col_right_step1 .= '		<i class="fa fa-circle-o fa-stack-2x"></i>';
		$col_right_step1 .= '		<i class="fa fa-star fa-stack-1x"></i>';
		$col_right_step1 .= '	</span>';
		$col_right_step1 .= '	<div>';
		$col_right_step1 .= '		<p class="text-square-register-step1 underlined">'.$sub_topic['title'].'</p>';
		$col_right_step1 .= '	</div>';
		$col_right_step1 .= '</div>';
		$indice++;
	}
	$result['block_step1'] = '';
	$result['block_step1'] = $col_right_step1;
	print json_encode($result);
	return $result;
}

//Function load step2
function LoadStep2($value='') {
	$topics = qa_db_read_all_assoc(qa_db_query_sub(
		'SELECT id as id_topic, title, parentid, type as type_topic From ^islamiqa_topics WHERE type = "t" ORDER BY title ASC'
	));
	$flag_col_left_step2 = true;
	$col_left_step2 ='';
	$value='';
    $border_top_register_step2 = 'border-top-register-step1';
	foreach ($topics as $topic) {
		if ($flag_col_left_step2) {
			$col_left_step2 .= '<li class="st-li shadow-topics-register-step1 '.$border_top_register_step2.'">';
			$value = $topic['id_topic'];
		} else {
			$col_left_step2 .= '<li class="st-li shadow-topics-register-step1">';
		}
		$col_left_step2 .= "	<div class='checkbox'><input type='hidden' value='".$topic['id_topic']."' id='".$topic['id_topic']."' name='topics'>";
		$col_left_step2 .= "		<label>&nbsp;<input type='checkbox' value='".$topic["id_topic"]."' onclick='LoadStep2Especific(".$topic['id_topic'].");'>&nbsp;".$topic['title']."</label>";
		$col_left_step2 .= "	</div>";
		$col_left_step2 .= "	<span class='fa fa-chevron-circle-right fa-lg'></span>";
		$col_left_step2 .= "</li>";
		$flag_col_left_step2 = false;
	}
	/**
	 * Col-Right-Step2
	 */
	$col_right_step2 = '';
	$sub_topics = qa_db_read_all_assoc(qa_db_query_sub(
	    'SELECT id as id_topic, title, parentid, type as type_topic From ^islamiqa_topics WHERE parentid = '.$value
	  ));
	$indice=0;
	foreach ($sub_topics as $sub_topic) {
		$col_right_step2 .= '<div class="six-boxes-gs-register ">
								<input type="checkbox" name ="topics['.$indice.']" value ='.$sub_topic["id_topic"].' id ='.$sub_topic["id_topic"].'
								onclick="VerifyCheckbox('.$sub_topic["id_topic"].', 2)">';
		$col_right_step2 .= '	<span class="fa-stack fa-lg fa-yellow">';
		$col_right_step2 .= '		<i class="fa fa-circle-o fa-stack-2x"></i>';
		$col_right_step2 .= '		<i class="fa fa-star fa-stack-1x"></i>';
		$col_right_step2 .= '	</span>';
		$col_right_step2 .= '	<div>';
		$col_right_step2 .= '		<p class="text-square-register-step1 underlined">'.$sub_topic['title'].'</p>';
		$col_right_step2 .= '	</div>';
		$col_right_step2 .= '</div>';
		$indice++;
	}
	$result['list_step2'] = '';
	$result['block_step2'] = '';
	$result['list_step2'] = $col_left_step2;
	$result['block_step2'] = $col_right_step2;
	print json_encode($result);
	return $result;
}

//Load options specific list
function LoadStep2Especific($value) {
	$col_right_step2 = '';
	$sub_topics = qa_db_read_all_assoc(qa_db_query_sub(
		'SELECT id as id_topic, title, parentid, type as type_topic From ^islamiqa_topics WHERE parentid = '.$value
	  ));
	$indice=0;

	foreach ($sub_topics as $sub_topic) {
		$col_right_step2 .= '<div class="six-boxes-gs-register ">
								<input type="checkbox" name ="topics['.$indice.']" value ='.$sub_topic["id_topic"].' id ='.$sub_topic["id_topic"].'
								onclick="VerifyCheckbox('.$sub_topic["id_topic"].', 2)">';
		$col_right_step2 .= '	<span class="fa-stack fa-lg fa-yellow">';
		$col_right_step2 .= '		<i class="fa fa-circle-o fa-stack-2x"></i>';
		$col_right_step2 .= '		<i class="fa fa-star fa-stack-1x"></i>';
		$col_right_step2 .= '	</span>';
		$col_right_step2 .= '	<div>';
		$col_right_step2 .= '		<p class="text-square-register-step1 underlined">'.$sub_topic['title'].'</p>';
		$col_right_step2 .= '	</div>';
		$col_right_step2 .= '</div>';
		$indice++;
	}

	$result['block_step2'] = '';
	$result['block_step2'] = $col_right_step2;
	print json_encode($result);
	return $result;
}

//Function load step3
function LoadStep3($value='') {
		$users = qa_db_read_all_assoc(qa_db_query_sub(
		'SELECT userid, handle, avatarblobid, avatarwidth, avatarheight, level, wallposts From ^users LIMIT 300'
	));
	$users_to_follow="";

	foreach ($users as $user) {
		$users_to_follow .= "<div class='col-xs-3'>";
		$users_to_follow .= "	<div class='col-xs-12 margin-top-5per'>";
		$users_to_follow .= "		<div class='col-xs-6'>";
		$users_to_follow .= "			<img src='".$value."images/profile-pic.jpg' alt='img' class='images-circle'>";
		$users_to_follow .= "		</div>";
		$users_to_follow .= "		<div class='col-xs-6 margin-top-10per'>";
		$users_to_follow .= "			<strong class='username-bold'>".$user['handle']."</strong>";
		$users_to_follow .= "			<div class='smaller'><i class='fa fa-users' aria-hidden='true'></i> 43K &nbsp;&nbsp; <i class='fa fa-flag' aria-hidden='true'></i> ".$user['wallposts']."</div>";
		$users_to_follow .= "			<div><label class='follow'><i class='fa fa-plus' aria-hidden='true'></i>&nbsp;Follow</label></div>";
		$users_to_follow .= "		</div>";
		$users_to_follow .= "	</div>";
		$users_to_follow .= "</div>";
	}

	$result['users_to_follow'] = $users_to_follow;
	$result['value'] = $value;
	print json_encode($result);
	return $result;
}

//Function load topic list
function LoadTopicList($value ="") {
	$topics = qa_db_read_all_assoc(qa_db_query_sub(
		"SELECT id , title, parentid, type, imagepath From ^islamiqa_topics WHERE type <> 'w' "
	));

    foreach ($topics as $row) {
		$fila[] = $row;
	}

	$result['topics'] = $topics;
	echo json_encode($fila);
	//return $result;
}

//Function load Users list
function LoadUsersList($value =""){
	$Users = qa_db_read_all_assoc(qa_db_query_sub(
		"SELECT userid, email, handle, avatarblobid, avatarwidth, avatarheight, level, wallposts From ^users ORDER BY userid DESC LIMIT 120"
	));

	/*foreach ($Users as $row) {
		$fila[] = $row;
	}*/

    foreach ($Users as $row) {
		$avatar = gravatar($row['email'], 250);
		array_push($row, $avatar);
		$fila[] = $row;
    }

    $result['users'] = $Users;
	echo json_encode($fila);
	//return $result;
}

function saveAllData($interests, $expertices, $follows) {
	if ($interests) {
		foreach($interests as $interest) {
			qa_db_query_sub(
				'INSERT IGNORE INTO ^islamiqa_user_topics_interest (userid, topicid) VALUES ($, #)',
				qa_get_logged_in_userid(), $interest
			);
		}
	}
	if ($expertices) {
		foreach($expertices as $expertice) {
			qa_db_query_sub(
				'INSERT IGNORE INTO ^islamiqa_user_topics_expertise (userid, topicid) VALUES ($, #)',
				qa_get_logged_in_userid(), $expertice
			);
		}
	}
	if ($follows) {
		foreach($follows as $follow) {
			qa_db_query_sub(
				'INSERT IGNORE INTO ^islamiqa_user_follow (userid, followuserid) VALUES ($, #)',
				qa_get_logged_in_userid(), $follow
			);
		}
	}
}

function gravatar($email, $size) {
	if (qa_to_override(__FUNCTION__)) { $args=func_get_args(); return qa_call_override(__FUNCTION__, $args); }

	if ($size>0)
		return '<img src="'.(qa_is_https_probably() ? 'https' : 'http').
		'://www.gravatar.com/avatar/'.md5(strtolower(trim($email))).'?s='.(int)$size.
		'" width="'.(int)$size.'" height="'.(int)$size.'" class="images-circle" alt=""/>';
	else
		return null;
}

function send_friends_mails($friends) {
	foreach ($friends as $friend) {
		$email = $friend[0];
		$handle = explode("@", $email);
		$subject = "Hi, join me on islamiqa.com";
		$body = "this is a test";
		$more = "more";

		$subs = array(
			'^message' => $body,
			'^f_handle' => $handle,
			'^f_url' => qa_path_absolute('user/'.$handle),
			'^more' => $more,
			'^a_url' => qa_path_absolute('account'),
		);

		qa_send_notification_custom(null, $email, $handle[0], $subject, $body, $subs);

		return null;
	}
}

function qa_send_notification_custom($userid, $email, $handle, $subject, $body, $subs, $html = false)
{
	if (qa_to_override(__FUNCTION__)) { $args=func_get_args(); return qa_call_override(__FUNCTION__, $args); }

	global $qa_notifications_suspended;

	if ($qa_notifications_suspended>0)
		return false;

	require_once QA_INCLUDE_DIR.'db/selects.php';
	require_once QA_INCLUDE_DIR.'util/string.php';

	if (isset($userid)) {
		$needemail=!qa_email_validate(@$email);
		$needhandle=empty($handle);

		if ($needemail || $needhandle) {
			if (QA_FINAL_EXTERNAL_USERS) {
				if ($needhandle) {
					$handles=qa_get_public_from_userids(array($userid));
					$handle=@$handles[$userid];
				}

				if ($needemail)
					$email=qa_get_user_email($userid);

			} else {
				$useraccount=qa_db_select_with_pending(
					array(
						'columns' => array('email', 'handle'),
						'source' => '^users WHERE userid = #',
						'arguments' => array($userid),
						'single' => true,
					)
				);

				if ($needhandle)
					$handle=@$useraccount['handle'];

				if ($needemail)
					$email=@$useraccount['email'];
			}
		}
	}

	if (isset($email) && qa_email_validate($email)) {
		$subs['^site_title']=qa_opt('site_title');
		$subs['^handle']=$handle;
		$subs['^email']=$email;
		$subs['^open']="\n";
		$subs['^close']="\n";

		return qa_send_email(array(
			'fromemail' => qa_get_logged_in_email(),
			'fromname' => qa_get_logged_in_handle(),
			'toemail' => $email,
			'toname' => $handle,
			'subject' => strtr($subject, $subs),
			'body' => (empty($handle) ? '' : qa_lang_sub('emails/to_handle_prefix', $handle)).strtr($body, $subs),
			'html' => $html,
		));

	} else
		return false;
}