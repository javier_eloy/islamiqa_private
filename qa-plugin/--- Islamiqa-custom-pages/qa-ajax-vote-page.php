<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 15/07/16
 * Time: 01:07 PM
 */

class qa_ajax_vote_page{


    private $directory;
//    private $urltoroot;


    public function load_module($directory, $urltoroot)
    {
        $this->directory =$directory;
    }


    public function match_request($request)
    {

        return strpos($request,"qajax-vote")!==FALSE;
    }


    public function process_request($request)
    {
        header('Content-Type: application/json');
        try{

          $userid=qa_get_logged_in_userid();
          //  $postid = qa_get("postid");
          $postid = qa_post_text("postid");
          //  $vote = qa_get("vote");
          $vote = qa_post_text("vote");
            
          $post_parent =  qa_post_text("parent");
            
          require_once($this->directory .'data-core.php');
          $questions= (new data_core())->vote($userid,$postid,$vote,$post_parent);
          print json_encode(array("result"=>$questions));

        }catch (\Exception $ex)
        {
            http_response_code(422);
            print json_encode(array("result"=>$ex->getMessage()));
        }

    }



}

