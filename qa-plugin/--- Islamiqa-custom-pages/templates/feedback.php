<?php 

/// Created by: Javier Hernandez
/// Date: 28/07/2016

class Renderer_feedback
{

    public function render($sidepanel) {
		
	 $html="<div class='container feedback-page'>
			<div class='row'>
			<div class='col-sm-8'>
        	<p class='heading'>Feedback Form </p>
			<p class='sub-heading'> We would love to hear your thoughts, concerns or problems with anything so we can improve!</p>
            <div class='in-content'>
            	<p class='label-head'>Feedback Type</p>
                
				
				<span class='first-span'><label class='radio-inline'><input class='magic-radio' name='radio1' id='1' type='radio' value=''><label for='1'>Questions</label></label></span>
                <span class='first-span'><label class='radio-inline'><input class='magic-radio' name='radio2' id='2' type='radio' value=''><label for='2'>Bug Reports</label></span>
                <span class='first-span'><label class='radio-inline'><input class='magic-radio' name='radio3' id='3' type='radio' value=''><label for='3'>Comments</label></span>

				 <div class='sel-box2'>
					<p class='label-head ib'>Category</p>
					<div class='form-group'>
					<select class='category' id='sel1'>
						<option>Getting Started</option>
						<option>Questions</option>
						<option>Topics</option>
						<option>Point System</option>
						<option>Troubleshooting</option>
						<option>Answers</option>
						<option>Search</option>
						<option>Privacy & Security</option>	
					</select>
					</div>
              </div>
			  
			  <p class='label-head'>Describe Feedback: <span class='asterisc'>*</span></p>
              <textarea class='feedback'></textarea> 
			  
              <div class='input-box-feedback'>
                	<div class='it-box'>
                    	<p class='label-head'>Name</p>
                        <input type='text' class='iit'>
						<p class='label-tip'>First Name</p>
                    </div>
                    <div class='it-box'>
                    	<p class='label-head'></p>
                        <input type='text' class='iit'>
						<p class='label-tip'>Last Name</p>
                    </div>
                    <div class='it-box'>
                    	<p class='label-head'>E-mail<span class='asterisc'>*</span></p>
                        <input type='email' class='iit em' placeholder='ex: myname@example.com'>
						<p class='label-tip'>&nbsp;</p>
                    </div>
                </div>
            </div>
				<center>
				<button class='btn full-btn' type='button'>Submit Feedback</button>
				</center>
    	</div>
		{$sidepanel}
    </div>
</div>";
		return $html;
	
	
	}
}