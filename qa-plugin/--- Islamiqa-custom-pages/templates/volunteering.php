<?php 

/// Created by: Javier Hernandez
/// Date: 02/08/2016

class Renderer_volunteering
{

    public function render($searchbox) {
		
	  $html = "<div class='volunteer-page'>
			 <div class='first-section'>
				<div class='container'>
					<div class='row'>
						<div class='col-xs-12 text-center'>
							<p class='v-heading'>Volunteering</p>
							<p class='v-sub-heading'>Join a team of fun, talented and dedicated people <br> looking to join our mission...</p>
							<div class='input-group v-ig'>
							{$searchbox}
							</div>
						</div>  
					</div>
				</div>
			</div>
			
			<div class='container'>
				<div class='row'>
					<div class='col-sm-3'>
						<div class='inner-box-4'>
							<p class='text-in-box'>Designers & Developers</p>
						</div>
					</div>
					<div class='col-sm-3'>
						<div class='inner-box-4'>
							<p class='text-in-box'>Marketing & Creativity</p>
						</div>
					</div>    
					<div class='col-sm-3'>
						<div class='inner-box-4'>
							<p class='text-in-box'>Editors & Reviewers</p>
						</div>
					</div>    
					<div class='col-sm-3'>
						<div class='inner-box-4'>
							<p class='text-in-box'>Writers & Interns</p>
						</div>
					</div>                  
				</div>
			</div>
			<div class='container'>
				<div class='row'>
					<div class='col-sm-12'>
						<p class='bottom-v-heading'>All Opportunities <span>This Week</span><span>This Month</span><span>This Year</span></p>
						<div class='posts-right pull-right' style='margin-top:-40px;'>
						<span class='posts-right-text' style='margin-right:250px;'>View</span>
						<span class='fa fa-th-large pull-right' style='margin-right:80px;'></span>
						<span class='fa fa-align-right pull-right' ></span>
						<span class='span-cat pull-right' style='font-size:16px; margin-right:-170px;'>Categories 
							<i class='fa fa-caret-down fa-lg'></i>
						</span>
						<span class='fa fa-plus-circle fa-lg pull-right' style='color: #939393; margin-right:-220px;'></span>
					</div>                
					  <div class='posts-box-inner1'>
						<div class='posts-counting'>
                            <span class='fa fa-angle-double-up'></span>
                            <span class='nposts'>761</span>
                            <span class='fa fa-angle-double-down'></span>
						</div>
						
                        <div class='posts-des'>
                          <p class='posts-head'>In-Office Volunteeting</p>
                          <p>Our head office in London could always use extra hands and help in administrative work. If you’re in the area and have some hours to spare during the week, do drop us a line.</p>
                          <p class='p-date'><span>posted </span>Nov 6<span> by </span>Aaqib76</p>
                        </div>
						<div class='crcl'></div>
                        <div class='posts-arrow-right'>
                          <span class='fa fa-comments fa-lg'></span>
                          <span class='nmbr'>424</span>
                          <span class='nmbr-btm-txt'>answers</span>
                          <span class='three-dots'>. . .</span>
                        </div>
                        
					  </div>    

					  <div class='posts-box-inner1'>
					  <div class='posts-counting'>
                            <span class='fa fa-angle-double-up'></span>
                            <span class='nposts'>25</span>
                            <span class='fa fa-angle-double-down'></span>
					</div>						
					  <div class='posts-des'>
                          <p class='posts-head'>Fundraising - International</p>
                          <p>Volunteers are the back-bone of our organisation, and continue to leave us all in awe every single day.</p>
                          <p class='p-date'><span>posted </span>Nov 6<span> by </span>Aaqib76</p>
                        </div>
						<div class='crcl'></div>
                        <div class='posts-arrow-right'>
                          <span class='fa fa-comments fa-lg'></span>
                          <span class='nmbr'>424</span>
                          <span class='nmbr-btm-txt'>answers</span>
                          <span class='three-dots'>. . .</span>
                        </div>
                        
					  </div>    
					
					  <div class='posts-box-inner1'>
						<div class='posts-counting'>
                            <span class='fa fa-angle-double-up'></span>
                            <span class='nposts'>830</span>
                            <span class='fa fa-angle-double-down'></span>
					</div>											
					  <div class='posts-des'>
                          <p class='posts-head'>Start your own initiative</p>
                          <p>Would you like to utilize your free time and skills to change the lives of millions? Do you want to 'make a difference'?</p>
                          <p class='p-date'><span>posted </span>Nov 6<span> by </span>Aaqib76</p>
                        </div>
						<div class='crcl'></div>
                        <div class='posts-arrow-right'>
                          <span class='fa fa-comments fa-lg'></span>
                          <span class='nmbr'>424</span>
                          <span class='nmbr-btm-txt'>answers</span>
                          <span class='three-dots'>. . .</span>
                        </div>
                      
					  </div>  
					
				</div>
			</div>
			
			<div class='row'>
				<div class='col-xs-12 dpb'>
					<p class='r-c-head'>About us</p>
					<div class='grey-logo'>
					<img src='images/logo-grey.png'>
				</div>
				<span class='social-icons fa fa-facebook'></span>
				<span class='social-icons fa fa-twitter'></span>
				<span class='social-icons fa fa-google-plus'></span>
				<span class='social-icons fa fa-linkedin'></span>
				<span class='social-icons fa fa-pinterest'></span>
				<span class='social-icons fa fa-instagram'></span>            
				<p class='r-c-head'>Site map</p>
				<div class='one-col-nv'>
					<li>Home</li>
					<li>Popular Questions</li>
					<li>Recently Asked</li>
					<li>All Questions</li>
					<li>Unanswered Questions</li>
				</div>
				<div class='one-col-nv'>
					<li>Blog</li>
					<li>Volunteering</li>
					<li>About</li>
					<li>Privacy Policy</li>
					<li>Terms</li>
				</div>
				<div class='one-col-nv'>
					<li>FAQ</li>
					<li>Give Feedback</li>
					<li>Press</li>               
				</div>
			</div>  
		</div>
		</div>";
		
		return $html;
	
	
	}
}