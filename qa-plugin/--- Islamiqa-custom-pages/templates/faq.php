<?php 

/// Created by: Javier Hernandez
/// Date: 28/07/2016

class Renderer_faq
{

    public function render($sidepanel) {
		
	 $html="<div class='container faq-page'>
				<div class='row'>
					<div class='col-sm-8'>	
						<p class='heading'>Frequently Asked Questions</p>
						<div class='row row-border-bottom'>
						  <div class='col-sm-1'>
							<p class='tag-up'>«</p>
							<p class='vote-up'>1761</p>
							<p class='tag-down'>«</p>
							<p class='vote-down'>761<p/>
						  </div>	
						  <div class='col-sm-11'>
							<p class='faq-section-title'>What kinds of questions can I ask here?</p>
							<p class='faq-section-text'>Most importantly, questions should be relevant to our community. Before you ask, please make sure to search for a similar question. You can search for questions by their title or tags.</p>
						    <ul class='bottom-ul'>
							<li><div class='has-red'>357</div><div class='red-simplespan'>Answers</div></li>
							<li><div class='pncl fa fa-pencil'></div><div class='red-simplespan'>Answer</div></li>
							<li><div class='has'>3k</div><div class='simplespan'>Views</div></li>
							<li><div class='has'>235</div><div class='simplespan'>Comments</div></li>
							<li class='dots-three'>...</li>
							</ul>
						 </div>	
						</div>

					    <div class='row row-border-bottom'>
						  <div class='col-sm-1'>
							<p class='tag-up'>«</p>
							<p class='vote-up'>1761</p>
							<p class='tag-down'>«</p>
							<p class='vote-down'>761<p/>
						  </div>	
						  <div class='col-sm-11'>
							<p class='faq-section-title'>What kinds of questions should be avoided?</p>
							<p class='faq-section-text'>Please avoid asking questions that are not related to our community, too subjective or argumentative.</p>
						    <ul class='bottom-ul'>
							<li><div class='has-red'>357</div><div class='red-simplespan'>Answers</div></li>
							<li><div class='pncl fa fa-pencil'></div><div class='red-simplespan'>Answer</div></li>
							<li><div class='has'>3k</div><div class='simplespan'>Views</div></li>
							<li><div class='has'>235</div><div class='simplespan'>Comments</div></li>
							<li class='dots-three'>...</li>
							</ul>
						 </div>	
						</div>			

						<div class='row row-border-bottom'>
						  <div class='col-sm-1'>
							<p class='tag-up'>«</p>
							<p class='vote-up'>1761</p>
							<p class='tag-down'>«</p>
							<p class='vote-down'>761<p/>
						  </div>	
						  <div class='col-sm-11'>
							<p class='faq-section-title'>What should I avoid in my answers?</p>
							<p class='faq-section-text'>Islamiqa is a question and answer site - it is not a discussion group. Please avoid holding debates in your answers as they tend to dilute the quality of the forum.</p>
							<p class='faq-section-text'>For brief discussion, or to thank someone for their answer, please post comments, not answers.</p>

						    <ul class='bottom-ul'>
							<li><div class='has-red'>357</div><div class='red-simplespan'>Answers</div></li>
							<li><div class='pncl fa fa-pencil'></div><div class='red-simplespan'>Answer</div></li>
							<li><div class='has'>3k</div><div class='simplespan'>Views</div></li>
							<li><div class='has'>235</div><div class='simplespan'>Comments</div></li>
							<li class='dots-three'>...</li>
							</ul>
						 </div>	
						</div>			
						
						<div class='row row-border-bottom'>
						  <div class='col-sm-1'>
							<p class='tag-up'>«</p>
							<p class='vote-up'>1761</p>
							<p class='tag-down'>«</p>
							<p class='vote-down'>761<p/>
						  </div>	
						  <div class='col-sm-11'>
							<p class='faq-section-title'>Who moderates this community?</p>
							<p class='faq-section-text'>he short answer is: you. This website is moderated by the users. Points system allows users to earn rights to perform a variety of moderation tasks</p>
							
						    <ul class='bottom-ul'>
							<li><div class='has-red'>357</div><div class='red-simplespan'>Answers</div></li>
							<li><div class='pncl fa fa-pencil'></div><div class='red-simplespan'>Answer</div></li>
							<li><div class='has'>3k</div><div class='simplespan'>Views</div></li>
							<li><div class='has'>235</div><div class='simplespan'>Comments</div></li>
							<li class='dots-three'>...</li>
							</ul>
						 </div>	
						</div>			
  
						<div class='row row-border-bottom'>
						  <div class='col-sm-1'>
							<p class='tag-up'>«</p>
							<p class='vote-up'>1761</p>
							<p class='tag-down'>«</p>
							<p class='vote-down'>761<p/>
						  </div>	
						  <div class='col-sm-11'>
							<p class='faq-section-title'>How does the point system work?</p>
							<p class='faq-section-text'>When a question or answer is voted up, the user who posted it will gain points. These points serve as a rough measure of the community trust in that person. Various moderation tasks are gradually assigned to the users based on those points. 
								For example, if you ask an interesting question or useful answer, it will likely be voted up. On the other hand if the question is poorly-worded or the answer is misleading - it will likely be voted down. Each up vote on a question will generate 1 points, whereas each vote against will subtract 1 points. The following table lists points gained per activity:</p>
							<ul class='faq-section-text faq-list-item' >	
							<li>Posting a question:  20 points</li>
							<li>Selecting an answer for your question:   30 points</li>
							<li>Per up vote on your question:   + 10 points</li>
							<li>Per down vote on your question: - 10 points</li>
							<li>Limit from up votes on each question:   + 100 points</li>
							<li>Limit from down votes on each question: – 30 points</li>
							<li>Posting an answer:   40 points</li>
							<li>Having your answer selected as the best:     300 points</li>
							<li>Per up vote on your answer: + 20 points</li>
							<li>Per down vote on your answer:   - 20 points</li>
							<li>Limit from up votes on each answer: + 200 points</li>
							<li>Limit from down votes on each answer:   – 50 points</li>
							<li>Voting up a question:    10 points</li>
							<li>Voting down a question:  10 points</li>
							<li>Voting up an answer:     10 points</li>
							<li>Voting down an answer:   10 points</li>
							<li>Add for all users:  + 100 points</li>
							</ul>
						    <p class='faq-section-text'> The following table lists point requirements for each type of moderation task. </p>
						    <ul class='bottom-ul'>
							<li><div class='has-red'>357</div><div class='red-simplespan'>Answers</div></li>
							<li><div class='pncl fa fa-pencil'></div><div class='red-simplespan'>Answer</div></li>
							<li><div class='has'>3k</div><div class='simplespan'>Views</div></li>
							<li><div class='has'>235</div><div class='simplespan'>Comments</div></li>
							<li class='dots-three'>...</li>
							</ul>
						 </div>	
						</div>	
        
        <button class='btn btn-bottom-red-load-more full-btn' type='button'>Load more</button>
        </div>
		{$sidepanel}
    </div>
    </div>
 </div>";
		return $html;
	
	
	}
}