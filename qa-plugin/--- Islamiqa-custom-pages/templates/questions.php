<?php

/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 09/07/16
 * Time: 12:00 PM
 */
class Render_question
{

    public function render($questions)
    {

        $result = json_encode(array("result" => $questions));
        $html = '  <!--    <link rel="stylesheet" href="/qa-theme/Islamiqa-as-dev/includes/css/style.css">-->



    <script id="answer-template" type="text/template">

        {{#.}}
        <li class="ct">
            <p class="ansby"><b> Answer</b> by <b> {{^handle}}Anonymous{{/handle}}{{handle}} :</b></p>
            <p class="ans-content" id="{{postid}}" onclick=""> {{{content}}} </a></p>
        </li>
        {{/.}}
    </script>
    
    <script id="user-points" type="text/template">
       {{#.}}
          
            {{#userpoints}}      
                <div>  <div class="col-popup-header"> User Points </div>                           </div>
                <div>  <div class="col-popup-left" style="margin-top: 5px">Score </span>:{{score}}</div>                           </div>
                <div>  <div class="col-popup-left">Questions:{{questions}}</div>          </div>
                <div>  <div class="col-popup-left">Answers:{{answers}}</div>           </div>             
                <div>  <div class="col-popup-left">Voted on answer:{{voted_on_answers}}</div>    </div>
                <div>  <div class="col-popup-left">Voted on questions:{{voted_on_questions}}</div>  </div>
                <div>  <div class="col-popup-left">Gave up votes:{{gave_down_votes}}</div>     </div>
                <div>  <div class="col-popup-left">Gave down votes:{{gave_out_up_vote}}</div>  </div>                
                <div>  <div class="col-popup-left">Received up votes:{{received_up_votes}}</div>  </div>                
                <div>  <div class="col-popup-left">Received down votes:{{received_down_votes}}</div>  </div>                
            {{/userpoints}}
                <div>  <div class="col-popup-left">Ranked: #{{userrank}}</div>  </div>
              
      {{/.}}    
   </script>

  <script id="user-question" type="text/template">
  
  
  
             <div class="col-md-12 question" id="answer{{postid}}">
        
                <div class="col-md-2">  <!-- conatienr votes -->
        
        
                    <div class="container-votes">
        
                        <div class="clearfix row-votes ds" id="bestanswer{{postid}}">
                                                            
                                                                       <span class="container-col-left">
                                                                       
                                                                            <div class="left-content"> Best </div>
                                                                            <div class="left-content"> Answer</div>
                                                                       
                                                                       </span>
                            <span class="icons-content"><i class="fa fa-bookmark"></i> </span>
                        </div>
        
                        <div class="clearfix row-votes ds" id="favorite{{postid}}">
                                                            
                                                                       <span class="container-col-left">
                                                                       
                                                                            <div class="left-content"> Make </div>
                                                                            <div class="left-content"> Favorite</div>
                                                                       
                                                                       </span>
        
                        <span class="icons-content" onclick="questions.makeFavoriteAnswer({{postid}},{{parentid}})"><i
                                class="fa fa-star"></i> </span>
        
        
                        </div>
        
                        <div class="clearfix row-votes">
                                                            
                                   <span class="container-col-left">                               
                                        <div class="lef-number" id="userupnumber{{postid}}" >  {{upvotes}} </div>                               
                                   </span>
                                   <span class="icons-number" id="voteup{{postid}}" onclick="questions.voteUpAnswer({{postid}},{{parentid}})"><i
                                            class="fa fa-angle-double-up gau"></i> 
                                   </span>
                        </div>
        
                        <div class="clearfix row-votes">
                                                        
                                   <span class="container-col-left">                           
                                       <div class="lef-number" id="userdownnumber{{postid}}" >  {{downvotes}} </div>                           
                                   </span>
        
                                   <span class="icons-number" id="votedown{{postid}}" onclick="questions.voteDownAnswer({{postid}},{{parentid}})"><i
                                            class="fa fa-angle-double-down rad"></i> 
                                   </span>
                        </div>
                        
                    </div>
                </div> <!-- end conatienr votes -->
                <div class="col-md-10 align-dash">
        
                    <div id="containeranswer{{postid}}">
        
                        <li class="ct answer-item">
                            <p class="ansby"><b>Best Answer</b> by <b> {{^handle}}Anonymous{{/handle}}{{handle}} :</b></p>
                            <p class="ans-content" id="{{postid}}" onclick=""> {{{content}}} </a></p>
                        </li>
        
                    </div>
        
                    <!--        <button type="button" class="btn btn-read-more">(Read More)</button>    -->
        
                </div>
        
            </div>
          
  </script> 
  

  <script id="dashboard-template" type="text/template">
        {{#result}}
        
        
     <div id="question{{postid}}" data-order="{{created}}" class="row row-border-bottom">

    <div class="alert alert-danger dashboard-alert" id="error{{postid}}" role="alert">
        <a href="javascript:void(0)" class="close" onclick="$(\'#error{{postid}}\').fadeOut(\'fast\')" title="close">×</a>
        <div id="contenterror{{postid}}">

        </div>
    </div>


    <div class="col-md-12 top-question" >

        <div class="boxs-dash-top" >
            <div class="boxs-dash" onclick="question.voteUp({{postid}})"  id="voteup{{postid}}">
                <i class="fa fa-angle-double-up gau "></i>  
                <p class="nbr" id="userupnumber{{postid}}">
                    {{upvotes}}</p>
            </div>

        </div>

        <div class="boxs-dash-down" >

            <div class="boxs-dash" onclick="question.voteDown({{postid}})" id="votedown{{postid}}">
                <i class="fa fa-angle-double-down rad"></i> 
                <p class="nbr" id="userdownnumber{{postid}}">{{downvotes}} </p>
            </div>
        </div>


        <div class="col-md-2 img-avatar">

            <div>
                <img src="{{user_logo}}" class="one-c-img">
            </div>
        </div>

        <div class="col-md-10 align-dash">
            <div class="ans-top" >
                asked on {{categoryname}} · {{#time_ago}} {{prefix}} {{data}}
                {{suffix}}
                {{/time_ago}}
                {{^handle}}Anonymous{{/handle}}
                {{#handle}}
                                         <span class="tooltip-custom" 
                                               onmouseover="question.activity({{userid}},{{postid}})">
                                              <span class="red-simplespan">  <a
                                                      href="javascript:void(0)"> by {{handle}}</a>  </span>
                                                <div class="tooltip-inner">
                                                       <div id="points{{postid}}">   
                                                             <div id="loading{{userid}}"> <i
                                                                     class="fa fa-refresh fa-spin fa-3x fa-fw ds"></i> </div>
                                                       </div> 
                                                </div>
                                         </span>
                {{/handle}}
                {{#owner}}
                                        <span class="edit-question">
                                                <a href="{{edit}}"><span class="fa fa-pencil-square-o ds fa-lg"></span></a>
                                                <a href="#"><span class="fa fa-trash ds fa-lg"></span></a>
                                       </span>
                {{/owner}}
            </div>

            <p class="ans-head"><a href="{{answerpath}}">{{title}}</a></p>
        </div>

    </div>


    <!----------------->
    {{^answer}}
     <div class="col-md-12">

        <div class="col-md-2"></div>
        <div class="col-md-10">

            

            <li class="ct">
                <p class="ans-content"> </b> No Answer </b></p>
            </li>
            

        </div>

     </div>
    {{/answer}}

     <div id="container-answer{{postid}}">    
        {{{  fisrtanswer}}}    
     </div>

   <!--   <div  id="rest-container-answer{{postid}}">    
        {{{ restanswer }}}    
     </div> -->
 
  

     {{#coutresult}}
     
       <div class="col-md-12">

        <div class="col-md-2"></div>
        <div class="col-md-10">
                  <a href="javascript:void(0)" class="more" onclick="questions.domore({{postid}})" id="showmore{{postid}}" style="outline: 0;"><p
                    class="more-ans"> {{coutresult}} more answers <span class="fa fa-chevron-down"></span></p></a> 
                    
                   <a href="javascript:void(0)" class="more" onclick="questions.doless({{postid}})" id="readless{{postid}}" style="outline: 0;display: none"><p
                    class="more-ans"> Read less <span class="fa fa-chevron-up"></span></p></a> 
                    
       </div>
      </div>  
          
      {{/coutresult}} 


    <div class="col-md-12">

        <div class="col-md-2"></div>
        <div class="col-md-10" style="margin-bottom: 25px">

              {{#hasTags}}
            <ul class="six-btn">

                <li class="six-li-icon"><span class="fa fa-tags fa-lg"></span></li>
                {{#listTags}}
                <li class="six-li">{{.}}</li>
                {{/listTags}}

            </ul>
            {{/hasTags}}
 



            <ul class="ans-btm-ul">
                <li>
                    <span class="pncl fa fa-pencil"></span>
                    <span class="red-simplespan">  <a href="{{answerpath}}">Answer</a>  </span>
                </li>
                <li>
                    <span class="has">{{countviwed}}</span>
                    <span class="simplespan">Views</span>
                </li>
                <li>
                    <span class="has">{{countcomments}}</span>
                    <span class="simplespan">Comments</span>
                </li>

                <li>
                    <!-- <span class="simplespan">Share</span> -->
                    <div class="tooltip-social">
                        <p style="color: #757575;font-size: 12px;">Share</p>

                        <div class="tooltip-inner tooltip-inner3">
                            <div class="social_icon facebook_icon">
                                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                            </div>
                            <div class="social_icon twitter_icon">
                                <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                            </div>
                            <div class="social_icon google_icon">
                                <a href="#"><i class="fa fa-google fa-2x"></i></a>
                            </div>
                            <div class="social_icon instagram_icon">
                                <a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                            </div>
                        </div>
                    </div>
                </li>
                                                        
                                                        <span style="float: right;">
                                                         <!-- <p class="dots-three" style="margin-left: -30px;">...</p> -->
                                                         
                                                             <div class="tooltip-custom">
                                                                    <i class="fa fa-ellipsis-h"></i>
                                                                    <div class="tooltip-inner tooltip-inner2">
                                                                        <a href="#">Invite authoritative user to answer</a><br>
                                                                        <a href="#">Add to Reading List</a><br>
                                                                        <a href="#">Show Related Questions</a><br>
                                                                        <a href="#">Create Summary Answer</a><br>
                                                                        <a href="#">Mark Favorite</a><br>
                                                                        <a href="#">Report</a><br>
                                                                    </div>
                                                              </div>
                                                         
                                                        </span>

            </ul>
        </div>   <!-- end section -->
    </div>

</div>   

     

        {{/result}}
    </script>


<!--    <div class="container">-->
<!--        <div class="row">-->


            <div class="dashboard-page">

                <div class="col-sm-8 col-md-8">

                    <div class="second-top left-sec qa-title-top "">
                        <!--            <span class="fa fa-chevron-up"></span> -->
                        
                        <div style="float: left; width: 22px; margin-left: 5px">
                          <div>
                            <a  href="javascript:void(0)" onclick="question.sortDown()">  <i id="sortUp"  class="fa fa-caret-up fa-lg ds"></i> </a>                             
                          </div>
                          <div>
                             <a href="javascript:void(0)" onclick="question.sortUp()"> <i id="sortDown" class="fa fa-caret-down fa-lg"></i> </a>
                          </div>
                        </div>
                        
                        <div id="u2977-4"  style="float: left">Home</div>
                      <!--  <div class="posts-right pull-right">
                            <span class="posts-right-text">View</span>
                            <span class="fa fa-align-right"></span>
                            <span class="fa fa-th-large"></span>
                        </div>  -->

                    </div>

                    <div id="talktitles"></div>
                    
                
                    <button id="loadMoreBtn" class="btn btn-bottom-red-load-more full-btn" type="button" onclick="question.loadMore()"> Load more <i id="loadMoreI"  class="fa fa-refresh fa-spin fa-fw" style="display:none"></i></button>
                 </div>
            </div>
<!--        </div>-->
<!---->
<!--    </div>-->

    <script>

        $(function () {
            var template = $("#dashboard-template").html();
            var result = ' . $result . ';
            
             result = questions.process(result.result.userid, result.result.data);
             var info = Mustache.to_html(template, {"result": result});
            $("#talktitles").html(info);
            questions.postprocess(result);
        })

       </script> ';

        return $html;

    }


}



//$questions = (new data_core())->questions();





