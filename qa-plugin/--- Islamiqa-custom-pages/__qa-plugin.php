<?php

//Make sure you need to register your plguin using metadata.json (for q2a 1.7+) or plugin metadata comment (for q2a below 1.7)

// register page module
//qa_register_plugin_module('page', 'step1.php', 'class_step1', 'Steps');

qa_register_plugin_module(
	'page', // type of module
	'step1.php', // PHP file containing module class
	'class_step1', // module class name in that PHP file
	'Step 1 Islamiqa Register' // human-readable name of module
);

qa_register_plugin_module(
	'page', // type of module
	'qa-press.php', // PHP file containing module class
	'class_press', // module class name in that PHP file
	'Press from Landing Page' // human-readable name of module
);

qa_register_plugin_module(
	'page', // type of module
	'qa-privacy.php', // PHP file containing module class
	'class_privacy', // module class name in that PHP file
	'Privacy Policy from Landing Page' // human-readable name of module
);


qa_register_plugin_module(
	'page', // type of module
	'qa-feedback.php', // PHP file containing module class
	'class_feedback_page', // module class name in that PHP file
	'Feedback from Landing Page' // human-readable name of module
);

qa_register_plugin_module(
	'page', // type of module
	'qa-about.php', // PHP file containing module class
	'class_about_page', // module class name in that PHP file
	'About Page from Landing Page' // human-readable name of module
);

qa_register_plugin_module(
	'page', // type of module
	'qa-volunteering.php', // PHP file containing module class
	'class_volunteering', // module class name in that PHP file
	'Volunteering from Landing Page' // human-readable name of module
);

qa_register_plugin_module(
	'page', // type of module
	'qa-blog.php', // PHP file containing module class
	'class_blog', // module class name in that PHP file
	'Blog from Landing Page' // human-readable name of module
);

qa_register_plugin_module(
	'page', // type of module
	'qa-terms.php', // PHP file containing module class
	'class_terms_page', // module class name in that PHP file
	'Terms from Landing Page' // human-readable name of module
);

qa_register_plugin_module(
	'page', // type of module
	'qa-faq.php', // PHP file containing module class
	'class_faq_page', // module class name in that PHP file
	'Faq from Landing Page' // human-readable name of module
);
 

qa_register_plugin_module(
	'widget', // type of module
	'qa-custom-pages-widget.php', // PHP file containing module class
	'qa_islamiqa_custom_pages_widget', // module class name in that PHP file
	'Islamiqa Widget Custom Pages' // human-readable name of module
);

qa_register_plugin_layer(
	'qa-custom-pages-layer.php', // PHP file containing layer
	'Islamiqa Custom Pages' // human-readable name of layer
);

qa_register_plugin_overrides('qa-custom-pages-override.php');

qa_register_plugin_module('page', 'qa-ajax-questions-page.php', 'qa_ajax_question_page', 'Qa2 Ajax Question');
qa_register_plugin_module('page', 'qa-questions-page.php', 'qa_questions_page', 'Qa2  Question');
qa_register_plugin_module('page', 'qa-ajax-vote-page.php', 'qa_ajax_vote_page', 'Qa2  Votes');
qa_register_plugin_module('page', 'qa-ajax-favorite.php', 'qa_ajax_favorite', 'Qa2  favorite');
qa_register_plugin_module('page', 'qa-ajax-userpoints-page.php', 'qa_ajax_userpoints_page', 'Qa2  profile');