var arrayTopic = new Array();
var arrayExpertise = new Array();
var arrayUsers = new Array();
var newList = []; // data array for iterest
var newListExpertice = []; // data array for expertices
var arrayListFollows = []; // data array for follows
var arrayValue = [];
var arrayValueExpertice = [];

function getAjaxCustomPageLink() {
	url = String(getBaseUrl());
	url = url.replace('index.php/', '');
	url = url + $('#ajaxcustomlink').val();
	return url;
}

function LoadTopic() {
    var  url = String(getAjaxCustomPageLink())+"fun_jq.php";
    $.ajax({
        url: url,
        type: "POST",
        global: false,
        async:false,
        data: { flag: "LoadTopicList" },
        dataType: "json",

        error: function (data) {
            console.data(data)
            alert("Error loading data.");
        },

        success: function (data) {
            //console.log(data)
            //alert(data.topics)
            for (var i in data) {
                id = data[i].id;
                title = data[i].title;
                parentid = data[i].parentid;
                type = data[i].type;
                image = data[i].imagepath;

                var arrayCategory = new Array(6)
                arrayCategory[0] = id;
                arrayCategory[1] = title;
                arrayCategory[2] = parentid;
                arrayCategory[3] = type;
                arrayCategory[4] = false;
                arrayCategory[5] = image;
                arrayTopic[i] = arrayCategory;

                var arrayCategory2 = arrayCategory.slice(0);
                arrayExpertise[i] = arrayCategory2;
            }//end for
        }// end success
    });//end ajax
}

function LoadUsers() {
    var url = String(getAjaxCustomPageLink())+"fun_jq.php";
    var value = $("#urltoroot").val();

    function hashCode(str) {
        var hash = 0;
        for (var i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        return hash;
    }

    function intToRGB(i){
        var c = (i & 0x00FFFFFF)
            .toString(16)
            .toUpperCase();

        return "00000".substring(0, 6 - c.length) + c;
    }

    $.ajax({
        url: url,
        type: "POST",
        global: false,
        async: false,
        data: { flag: "LoadUsersList" },
        dataType: "json",
        error: function (data) {
            console.log(data);
            alert("Error loading data.");
        },
        success: function (data) {
            //alert(data)
            //console.log(data);
            var showme='';
            var length = 10;
            var length_exert = 2;
            var length_color = 1;

            for (var i in data) {
                userid = data[i].userid;
                emailhandle = data[i].email;
                handle = data[i].handle;
                avatarblobid = data[i].avatarblobid;
                avatarwidth = data[i].avatarwidth;
                avatarheight = data[i].avatarheight;
                level = data[i].level;
                wallposts = data[i].wallposts;

                var block = new Array(9)
                block[0] = userid;
                block[1] = handle;
                block[2] = avatarblobid;
                block[3] = avatarwidth;
                block[4] = avatarheight;
                block[5] = level;
                block[6] = wallposts;
                block[7] = false; //check or uncheck false by default

                var nombre = handle.substring(0, length);
                var nombre_exert = nombre.substring(0, length_exert);
                var nombre_exert_upp = nombre_exert.toUpperCase();
                var nc = nombre_exert.substring(0, length_color);
                var color_ini = nc + nc + nc + nc + nc + nc;
                var color_ini_upp = color_ini.toLowerCase();

                showme = showme + "<div class='col-xs-3'>";
                showme = showme + "    <div class='col-xs-12 margin-top-5per'>";
                showme = showme + "        <div class='col-xs-6'>";
                //showme = showme + "            <img src='"+value+"images/profile-pic.jpg' alt='img' class='images-circle'>";
                //showme = showme + data[i][0]; // TODO: change this after the system has his own image manager system.
                showme = showme + "            <img id='avatar"+userid+"' class='images-circle'>"; // TODO: change this after the system has his own image manager system.
                showme = showme + "        </div>";
                showme = showme + "        <div class='col-xs-6 margin-top-10per'>";
                showme = showme + "            <strong class='username-bold'>"+nombre+"</strong>";
                showme = showme + "            <div class='smaller'><i class='fa fa-users' aria-hidden='true'></i> 43K &nbsp;&nbsp; <i class='fa fa-flag' aria-hidden='true'></i> "+wallposts+"</div>";
                showme = showme + "            <div><label class='follow'><input type='checkbox' id='user"+userid+"' name='user"+userid+"' style='display: none;'/><i class='fa fa-plus' aria-hidden='true'></i>&nbsp;Follow</label></div>";
                showme = showme + "        </div>";
                showme = showme + "    </div>";
                showme = showme + "</div>";

                showme = showme + "<script>";
                showme = showme + "$('#user"+userid+"').on('click', function() {";
                showme = showme + "    if($(this).is(':checked')) {";
                showme = showme + "        ArrayFollow('"+userid+"');";
                //showme = showme + "        console.log(arrayListFollows);";
                showme = showme + "        $('#user"+userid+"').prop('checked', true).parent().css({'background' : '#B40404'});";
                //showme = showme + "        alert('activado');";
                showme = showme + "    } else {";
                showme = showme + "        DeleteFromArrayFollow('"+userid+"');";
                //showme = showme + "        console.log(arrayListFollows);";
                showme = showme + "        $('#user"+userid+"').prop('checked', false).parent().css({'background' : '#04B486'});";
                //showme = showme + "        alert('desactivado');";
                showme = showme + "    }";
                showme = showme + "});";
                showme = showme + "</script>";

                showme = showme + "<script>";
                showme = showme + "$.ajax({";
                showme = showme + "    url: 'https://www.gravatar.com/avatar/"+window.md5(emailhandle)+"?s=200&r=pg&d=404',";
                showme = showme + "    data: {value: 1},";
                showme = showme + "    method: 'post',";
                showme = showme + "    error: function(XMLHttpRequest, textStatus, errorThrown) {";
                showme = showme + "        $('#avatar"+userid+"').avatar({";
                showme = showme + "            useGravatar: false,";
                showme = showme + "            fallbackImage: '',";
                showme = showme + "            size: 250,";
                showme = showme + "            initials: '"+nombre_exert_upp+"',";
                showme = showme + "            initial_fg: '#FEFEFE',";
                showme = showme + "            initial_bg: '#"+intToRGB(hashCode(color_ini_upp))+"',";
                showme = showme + "            initial_size: null,";
                showme = showme + "            initial_weight: 100,";
                showme = showme + "            initial_font_family: 'Comic Sans',";
                showme = showme + "            hash: '"+window.md5(emailhandle)+"',";
                showme = showme + "            email: '"+emailhandle+"',";
                showme = showme + "            fallback: 'mm',";
                showme = showme + "            rating: 'x',";
                showme = showme + "            forcedefault: true,";
                showme = showme + "            allowGravatarFallback: true";
                showme = showme + "        });";
                showme = showme + "    },";
                showme = showme + "    success: function(data) {";
                showme = showme + "        $('#avatar"+userid+"').avatar({";
                showme = showme + "            useGravatar: true,";
                showme = showme + "            fallbackImage: '',";
                showme = showme + "            size: 250,";
                showme = showme + "            initials: null,";
                showme = showme + "            initial_fg: '#FEFEFE',";
                showme = showme + "            initial_bg: '#"+intToRGB(hashCode(color_ini_upp))+"',";
                showme = showme + "            initial_size: 0,";
                showme = showme + "            initial_weight: 100,";
                showme = showme + "            initial_font_family: 'Comic Sans',";
                showme = showme + "            hash: '"+window.md5(emailhandle)+"',";
                showme = showme + "            email: '"+emailhandle+"',";
                showme = showme + "            fallback: 'mm',";
                showme = showme + "            rating: 'x',";
                showme = showme + "            forcedefault: false,";
                showme = showme + "            allowGravatarFallback: false";
                showme = showme + "        });";
                showme = showme + "    }";
                showme = showme + "});";
                showme = showme + "</script>";

                block[8] = showme;
                arrayUsers[i] = block;
            }//end for
        }// end success
    });//end ajax
    //console.log(arrayUsers)
}

function LoadStep1() {
    LoadTopic();

    border_top_register_step1 = 'border-top-register-step1';
    col_left_step1 = '';
    col_right_step1 = '';
    flag_col_left_step1 = true;

    for (i=0;i<arrayTopic.length;i++) {
        id = arrayTopic[i][0];
        title = arrayTopic[i][1];
        parentid = arrayTopic[i][2];
        type = arrayTopic[i][3];
        checked = arrayTopic[i][4];

        if (type == 't') {
            border_top_register_step1 = 'border-top-register-step1';
            if (flag_col_left_step1) {
                col_left_step1 = col_left_step1 + '<li class="st-li shadow-topics-register-step1 ' +border_top_register_step1+'">';
                value = id;
            } else
                col_left_step1 = col_left_step1 + '<li class="st-li shadow-topics-register-step1">';
                col_left_step1 = col_left_step1 + '    <label class="step1-label-left">';
                col_left_step1 = col_left_step1 + '        <div class="checkbox">';
                col_left_step1 = col_left_step1 + '            <label>&nbsp;<input type="checkbox" value="'+id+'" name="topics[]" id="topic'+id+'" onclick="LoadStep1Especific('+id+');">&nbsp;'+title+'</label>';
                col_left_step1 = col_left_step1 + '        </div>';
                col_left_step1 = col_left_step1 + '        <span class="fa fa-chevron-circle-right fa-lg" id="span-topic'+id+'"></span>';
                col_left_step1 = col_left_step1 + '    </label>';
                col_left_step1 = col_left_step1 + '</li>';
                flag_col_left_step1 = false;
        }
    }

    $("#list_step1").html(col_left_step1);
}

function LoadStep1Especific(value) {
    var url = String(getAjaxCustomPageLink());
    var list = new Array();
    var length = 20;

    $('#step1-next-button').prop('disabled', false);
    col_right_step1 = '';

    if($('#topic'+value).is(':checked')) {
        $('#span-topic'+value).addClass('span-color');

        arrayValue.push(value);

        for (var i=0; i<arrayTopic.length; i++) {
            id = arrayTopic[i][0];
            title = arrayTopic[i][1];
            parentid = arrayTopic[i][2];
            type = arrayTopic[i][3];
            checked = arrayTopic[i][4];
            checkedvalue = '';

            var title_excert = title.substring(0, length);

            if (checked) {
                checkedvalue = 'checked';
            }

            // this is for rending the complete array for all the current topics selected.
            for (var j = 0; j < arrayValue.length; j++) {
                parentid2 = arrayValue[j];
                if (parentid2 == parentid) {
                    function escapeRegExp(str) {
                        var res = str.toLowerCase();
                        var newString = res.replace(/ /g, "_");
                        var finalString = newString.replace(/[()]/g, "");
                        return finalString;
                    }

                    col_right_step1 = col_right_step1 + '<label class="label-right-1">';
                    col_right_step1 = col_right_step1 + '    <div class="six-boxes-gs-register" style="background-image:url('+url+'images/'+escapeRegExp(title)+'.jpg); background-size: 100%;">';
                    col_right_step1 = col_right_step1 + '        <input class="test-checkbox" type="checkbox" name ="topic[]" value ="'+id+'" id ="topic'+id+'"';
                    col_right_step1 = col_right_step1 + '        onclick="VerifyCheckbox('+id+', 1)" onchange = "checkingValues(this);" '+ checkedvalue+' checked>';
                    col_right_step1 = col_right_step1 + '        <span class="fa-stack fa-lg fa-yellow">';
                    col_right_step1 = col_right_step1 + '            <!--<i class="fa fa-circle-o fa-stack-2x"></i>-->';
                    col_right_step1 = col_right_step1 + '            <!--<i class="fa fa-star fa-stack-1x"></i>-->';
                    col_right_step1 = col_right_step1 + '        </span>';
                    col_right_step1 = col_right_step1 + '        <div>';
                    col_right_step1 = col_right_step1 + '            <p class="text-square-register-step1 underlined">'+title_excert+'</p>';
                    col_right_step1 = col_right_step1 + '        </div>';
                    col_right_step1 = col_right_step1 + '    </div>';
                    col_right_step1 = col_right_step1 + '</label>';
                }
            }

            // this is for sending json to store data.
            if (parentid == value) {
                list.push(id);
            }
        }
        //console.log(value);
        //var listTopic = this.ArrayInterest(list);
        //console.log(list);
        //console.log(listTopic);
    } else {
        $('#span-topic'+value).removeClass('span-color');

        // split the array
        for (var y = 0, z = 0; y < arrayValue.length; y++) {
            if (arrayValue[y] != value)
                arrayValue[z++] = arrayValue[y];
        }
        arrayValue.length = z;

        for (i=0; i<arrayTopic.length; i++) {
            id = arrayTopic[i][0];
            title = arrayTopic[i][1];
            parentid = arrayTopic[i][2];
            type = arrayTopic[i][3];
            checked = arrayTopic[i][4];
            checkedvalue = '';

            var title_excert = title.substring(0, length);

            if (checked) {
                checkedvalue = 'checked';
            }

            // array data render in view.
            for (j = 0; j < arrayValue.length; j++) {
                parentid2 = arrayValue[j];
                if (parentid2 == parentid) {
                    function escapeRegExp(str) {
                        var res = str.toLowerCase();
                        var newString = res.replace(/ /g, "_");
                        var finalString = newString.replace(/[()]/g, "");
                        return finalString;
                    }

                    col_right_step1 = col_right_step1 + '<label class="label-right-1">';
                    col_right_step1 = col_right_step1 + '    <div class="six-boxes-gs-register" style="background-image:url('+url+'images/'+escapeRegExp(title)+'.jpg); background-size: 100%;">';
                    col_right_step1 = col_right_step1 + '        <input class="test-checkbox" type="checkbox" name ="topic[]" value ="'+id+'" id ="topic'+id+'"';
                    col_right_step1 = col_right_step1 + '        onclick="VerifyCheckbox('+id+', 1)" onchange = "checkingValues(this);" '+ checkedvalue+' checked>';
                    col_right_step1 = col_right_step1 + '        <span class="fa-stack fa-lg fa-yellow">';
                    col_right_step1 = col_right_step1 + '            <!--<i class="fa fa-circle-o fa-stack-2x"></i>-->';
                    col_right_step1 = col_right_step1 + '            <!--<i class="fa fa-star fa-stack-1x"></i>-->';
                    col_right_step1 = col_right_step1 + '        </span>';
                    col_right_step1 = col_right_step1 + '        <div>';
                    col_right_step1 = col_right_step1 + '            <p class="text-square-register-step1 underlined">'+title_excert+'</p>';
                    col_right_step1 = col_right_step1 + '        </div>';
                    col_right_step1 = col_right_step1 + '    </div>';
                    col_right_step1 = col_right_step1 + '</label>';
                }
            }

            // data for store
            if (parentid == value) {
                var listTopic2 = this.DeleteFromArrayInterest(id);
            }
        }

        if (listTopic2 == 0) {
            $('#step1-next-button').prop('disabled', true);
        }

        //console.log(listTopic2);
    }

    var listTopic = this.ArrayInterest(list);
    //console.log(list);
    //console.log(listTopic);
    $("#block_step1").html(col_right_step1);

    var $pArr = $('.label-right-1');
    var pArrLen = $pArr.length;
    var pPerDiv = 8;

    for (var i = 0;i < pArrLen;i+=pPerDiv){
        $pArr.filter(':eq('+i+'),:lt('+(i+pPerDiv)+'):gt('+i+')').wrapAll('<div class="group"/>');
    }

    var container_width = 688 * $("#block_step1 .group").length;

    $("#block_step1").css("width", container_width);

    var scrolled=0;

    if (container_width > 688) {
        $('#scrollLeft').fadeIn();
        $('#scrollRight').fadeIn();
    } else {
        $('#scrollLeft').fadeOut();
        $('#scrollRight').fadeOut();
    }

    $('#scrollRight').on('click', function(){
        scrolled=scrolled+container_width;

        $('.container-outer').animate({
            scrollLeft: scrolled
        });
    });

    $('#scrollLeft').on('click', function(){
        scrolled=scrolled-container_width;

        $(".container-outer").animate({
            scrollLeft: scrolled
        });
    });

    $('#scrollLeft').hide();

    $('.container-outer').scroll(function() {
        if ($(this).scrollLeft()>0) {
            $('#scrollLeft').fadeIn();
            if ($(this)[0].scrollWidth <= ($(this).width() + $(this).scrollLeft())) {
                $('#scrollRight').fadeOut();
            } else {
                $('#scrollRight').fadeIn();
            }
        } else {
            $('#scrollLeft').fadeOut();
        }
    });
}


function VerifyCheckbox(value, step) {
    $("#selected_step1").hide();
    $("#selected_step2").hide();
    //estableciendo el selector
    var selector = '#'+value;
    //obteniendo la bandera
    var flag = $(selector).val()?true:false;

    if (step == 1) {
        if(flag){
            $("#selected_step1").append(value+'-');
            return true;
        }
    }

    if (step == 2) {
        if(flag) {
            $("#selected_step2").append(value+'-');
            return true;
        }
    }
}

function checkingValues(elem) {
    elementid = $(elem).prop("id");
    checked = $(elem).prop("checked");
    value = $(elem).prop("value");

    for (i=0;i<arrayTopic.length;i++) {
        id = arrayTopic[i][0];

        if (value == id){
            arrayTopic[i][4] = checked;
            var x = arrayTopic[i][4] = checked;

            if (x == false) {
                for (var l = 0, j = 0; l < newList.length; l++) {
                    if (newList[l] != value)
                        newList[j++] = newList[l];
                }
                newList.length = j;
                //console.log(newList);
            } else {
                newList.push(value);
                //console.log(newList);
            }
        }
    }
}

function checkingValuesExp(elem) {
    elementid = $(elem).prop("id");
    checked = $(elem).prop("checked");
    value = $(elem).prop("value");

    for (i=0;i<arrayExpertise.length;i++) {
        id = arrayExpertise[i][0];

        if (value == id){
            arrayExpertise[i][4] = checked;
            var x = arrayExpertise[i][4] = checked;

            if (x == false) {
                for (var l = 0, j = 0; l < newListExpertice.length; l++) {
                    if (newListExpertice[l] != value)
                        newListExpertice[j++] = newListExpertice[l];
                }
                newListExpertice.length = j;
                //console.log(newListExpertice);
            } else {
                newListExpertice.push(value);
                //console.log(newListExpertice);
            }
        }
    }
}

function LoadStep2() {
    border_top_register_step1 = 'border-top-register-step1';
    col_left_step1 = '';
    col_right_step1 = '';
    flag_col_left_step1 = true;

    for (i=0;i<arrayExpertise.length;i++) {
        id = arrayExpertise[i][0];
        title = arrayExpertise[i][1];
        parentid = arrayExpertise[i][2];
        type = arrayExpertise[i][3];
        checked = arrayExpertise[i][4];

        if (type == 't') {
            border_top_register_step1 = 'border-top-register-step1';

            if (flag_col_left_step1) {
                col_left_step1 = col_left_step1 + '<li class="st-li shadow-topics-register-step1 ' +border_top_register_step1+'">';
                value = id;
            } else
                col_left_step1 = col_left_step1 + '<li class="st-li shadow-topics-register-step1">';
                col_left_step1 = col_left_step1 + '    <label class="step2-label-left">';
                col_left_step1 = col_left_step1 + '        <div class="checkbox">';
                col_left_step1 = col_left_step1 + '            <label>&nbsp;<input type="checkbox" value="'+id+'" name="topicexpertise[]" id="topicexpertise'+id+'" onclick="LoadStep2Especific('+id+');">&nbsp;'+title+'</label>';
                col_left_step1 = col_left_step1 + '        </div>';
                col_left_step1 = col_left_step1 + '        <span class="fa fa-chevron-circle-right fa-lg" id="span-topicexpertise'+id+'"></span>';
                col_left_step1 = col_left_step1 + '    </label>';
                col_left_step1 = col_left_step1 + '</li>';
                flag_col_left_step1 = false;
        }
    }

    $("#list_step2").html(col_left_step1);
}

function LoadStep2Especific(value) {
    var url = String(getAjaxCustomPageLink());
    var list = new Array();
    var length = 20;

    $('#step2-next-button').prop('disabled', false);
    col_right_step1 = '';

    if ($('#topicexpertise'+value).is(':checked')) {
        $('#span-topicexpertise'+value).addClass('span-color');

        arrayValueExpertice.push(value);

        for (var i=0; i < arrayExpertise.length; i++) {
            id = arrayExpertise[i][0];
            title = arrayExpertise[i][1];
            parentid = arrayExpertise[i][2];
            type = arrayExpertise[i][3];
            checked = arrayExpertise[i][4];
            checkedvalue = '';

            var title_excert = title.substring(0, length);

            if (checked) {
                checkedvalue = 'checked';
            }

            // this is for rending the complete array for all the current topics selected.
            for (var j = 0; j < arrayValueExpertice.length; j++) {
                parentid2 = arrayValueExpertice[j];
                if (parentid2 == parentid) {
                    function escapeRegExp(str) {
                        var res = str.toLowerCase();
                        var newString = res.replace(/ /g, "_");
                        var finalString = newString.replace(/[()]/g, "");
                        return finalString;
                    }

                    col_right_step1 = col_right_step1 + '<label class="label-right-2">';
                    col_right_step1 = col_right_step1 + '    <div class="six-boxes-gs-register" style="background-image:url('+url+'images/'+escapeRegExp(title)+'.jpg); background-size: 100%;">';
                    col_right_step1 = col_right_step1 + '        <input class="test-checkbox-ext" type="checkbox" name ="topic[]" value ="'+id+'" id ="topic'+id+'"';
                    col_right_step1 = col_right_step1 + '        onclick="VerifyCheckbox('+id+', 1)" onchange = "checkingValuesExp(this);" '+ checkedvalue+' checked>';
                    col_right_step1 = col_right_step1 + '        <span class="fa-stack fa-lg fa-yellow">';
                    col_right_step1 = col_right_step1 + '            <!--<i class="fa fa-circle-o fa-stack-2x"></i>-->';
                    col_right_step1 = col_right_step1 + '            <!--<i class="fa fa-star fa-stack-1x"></i>-->';
                    col_right_step1 = col_right_step1 + '        </span>';
                    col_right_step1 = col_right_step1 + '        <div>';
                    col_right_step1 = col_right_step1 + '            <p class="text-square-register-step1 underlined">'+title_excert+'</p>';
                    col_right_step1 = col_right_step1 + '        </div>';
                    col_right_step1 = col_right_step1 + '    </div>';
                    col_right_step1 = col_right_step1 + '</label>';
                }
            }

            // this is for sending json to store data.
            if (parentid == value) {
                list.push(id);
            }
        }
        //console.log(value);
        //var listTopic = this.ArrayExpertice(list);
        //console.log(list);
        //console.log(listTopic);
    } else {
        $('#span-topicexpertise'+value).removeClass('span-color');

        // split the array
        for (var y = 0, z = 0; y < arrayValueExpertice.length; y++) {
            if (arrayValueExpertice[y] != value)
                arrayValueExpertice[z++] = arrayValueExpertice[y];
        }
        arrayValueExpertice.length = z;

        for (i=0; i < arrayExpertise.length; i++) {
            id = arrayExpertise[i][0];
            title = arrayExpertise[i][1];
            parentid = arrayExpertise[i][2];
            type = arrayExpertise[i][3];
            checked = arrayExpertise[i][4];
            checkedvalue = '';

            var title_excert = title.substring(0, length);

            if (checked) {
                checkedvalue = 'checked';
            }

            // array data render in view.
            for (j = 0; j < arrayValueExpertice.length; j++) {
                parentid2 = arrayValueExpertice[j];
                if (parentid2 == parentid) {
                    function escapeRegExp(str) {
                        var res = str.toLowerCase();
                        var newString = res.replace(/ /g, "_");
                        var finalString = newString.replace(/[()]/g, "");
                        return finalString;
                    }

                    col_right_step1 = col_right_step1 + '<label class="label-right-2">';
                    col_right_step1 = col_right_step1 + '    <div class="six-boxes-gs-register" style="background-image:url('+url+'images/'+escapeRegExp(title)+'.jpg); background-size: 100%;">';
                    col_right_step1 = col_right_step1 + '        <input class="test-checkbox-ext" type="checkbox" name ="topic[]" value ="'+id+'" id ="topic'+id+'"';
                    col_right_step1 = col_right_step1 + '        onclick="VerifyCheckbox('+id+', 1)" onchange = "checkingValuesExp(this);" '+ checkedvalue+' checked>';
                    col_right_step1 = col_right_step1 + '        <span class="fa-stack fa-lg fa-yellow">';
                    col_right_step1 = col_right_step1 + '            <!--<i class="fa fa-circle-o fa-stack-2x"></i>-->';
                    col_right_step1 = col_right_step1 + '            <!--<i class="fa fa-star fa-stack-1x"></i>-->';
                    col_right_step1 = col_right_step1 + '        </span>';
                    col_right_step1 = col_right_step1 + '        <div>';
                    col_right_step1 = col_right_step1 + '            <p class="text-square-register-step1 underlined">'+title_excert+'</p>';
                    col_right_step1 = col_right_step1 + '        </div>';
                    col_right_step1 = col_right_step1 + '    </div>';
                    col_right_step1 = col_right_step1 + '</label>';
                }
            }

            // data for store
            if (parentid == value) {
                var listTopic2 = this.DeleteFromArrayExpertice(id);
            }
        }

        if (listTopic2 == 0) {
            $('#step2-next-button').prop('disabled', true);
        }

        //console.log(listTopic2);
    }

    var listTopic = this.ArrayExpertice(list);
    //console.log(list);
    //console.log(listTopic);
    $("#block_step2").html(col_right_step1);

    var $pArr = $('.label-right-2');
    var pArrLen = $pArr.length;
    var pPerDiv = 8;

    for (var i = 0;i < pArrLen;i+=pPerDiv){
        $pArr.filter(':eq('+i+'),:lt('+(i+pPerDiv)+'):gt('+i+')').wrapAll('<div class="group"/>');
    }

    var container_width = 688 * $("#block_step2 .group").length;

    $("#block_step2").css("width", container_width);

    var scrolled=0;

    if (container_width > 688) {
        $('#scrollLeftExt').fadeIn();
        $('#scrollRightExt').fadeIn();
    } else {
        $('#scrollLeftExt').fadeOut();
        $('#scrollRightExt').fadeOut();
    }

    $('#scrollRightExt').on('click', function(){
        scrolled=scrolled+container_width;

        $('.container-outer').animate({
            scrollLeft: scrolled
        });
    });

    $('#scrollLeftExt').on('click', function(){
        scrolled=scrolled-container_width;

        $(".container-outer").animate({
            scrollLeft: scrolled
        });
    });

    $('#scrollLeftExt').hide();

    $('.container-outer').scroll(function() {
        if ($(this).scrollLeft()>0) {
            $('#scrollLeftExt').fadeIn();
            if ($(this)[0].scrollWidth <= ($(this).width() + $(this).scrollLeft())) {
                $('#scrollRightExt').fadeOut();
            } else {
                $('#scrollRightExt').fadeIn();
            }
        } else {
            $('#scrollLeftExt').fadeOut();
        }
    });
}

function LoadStep3() {
    LoadUsers()

    var  url = String(getAjaxCustomPageLink())+"fun_jq.php";

    $.ajax({
        url: url,
        type: "POST",
        global: false,
        data: { flag: "LoadStep3", value: value },
        dataType: "json",

        error: function (data) {
            alert("Error loading data.");
        },

        success: function (data) {
            //antigua escuela
            block = '';

            for (i=0;i<arrayUsers.length;i++) {
                showme = arrayUsers[i][8];
            }//for del success

            $("#tofollow").html(showme);
        }// fin success
    });//fin ajax
}

function ArrayInterest(topic) {
    for (var i=0; i<topic.length; i++) {
        id = topic[i];
        newList.push(id);
    }

    return newList;
}

function DeleteFromArrayInterest(value) {
    for (var i = 0, j = 0; i < newList.length; i++) {
        if (newList[i] != value)
            newList[j++] = newList[i];
    }
    return newList.length = j;
}

function ArrayExpertice(topic) {
    for (var i = 0; i<topic.length; i++) {
        id = topic[i];
        newListExpertice.push(id);
    }

    return newListExpertice;
}

function DeleteFromArrayExpertice(value) {
    for (var i = 0, j = 0; i < newListExpertice.length; i++) {
        if (newListExpertice[i] != value)
            newListExpertice[j++] = newListExpertice[i];
    }
    return newListExpertice.length = j;
}

function ArrayFollow(id) {
    arrayListFollows.push(id);

    return arrayListFollows;
}

function DeleteFromArrayFollow(id) {
    for (var i = 0, j = 0; i < arrayListFollows.length; i++) {
        if (arrayListFollows[i] != id)
            arrayListFollows[j++] = arrayListFollows[i];
    }
    return arrayListFollows.length = j;
}

function LoadSaveEvent() {
    var  url = String(getAjaxCustomPageLink())+"fun_jq.php";
    $.ajax({
        type: 'post',
        url: url,
        data: {
            flag: "saveAllData",
            interests: newList,
            $expertices: newListExpertice,
            follows: arrayListFollows
        },
        error: function(data) {
            //console.data(data);
            alert('error sending data');
        },
        success: function(data) {
            //console.log(data);
        }
    });
}

function CheckImg(value, step, elem) {
    var selector = '#topic'+value;
    var selector_id = 'topic'+value;
    var list = [];

    var a = $(elem).find('input:checkbox[name="topic[]"]');

    if (step == 1) {
        if($('#topic'+value).is(':checked')) {
            this.DeleteFromArrayInterest(value);
            $(selector).attr("checked", false);
            //console.log(newList);
        } else {
            //checkingValues(a[0]);
            //console.log(a[0]);
            $(selector).prop("checked", true);
            list.push(value);
            newList.push(list[0]);
            //console.log(newList);
        }
    }
}


document.write('\x3Cscript src="' + (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//webfonts.creativecloud.com/lato:n4,n9,n7,i7:all;source-sans-pro:n4,n7:all;pt-serif:n4:all.js" type="text/javascript">\x3C/script>');
