<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 09/07/16
 * Time: 09:46 AM
 */


class qa_ajax_userpoints_page{


    private $directory;
//    private $urltoroot;


    public function load_module($directory, $urltoroot)
    {

        $this->directory =$directory;
    }


    public function match_request($request)
    {
        return strpos($request,"qajax-points")!==FALSE;
    }


    public function process_request($request)
    {
        require_once($this->directory .'data-core.php');
        $userid = qa_post_text('id');
        $questions= (new data_core())->user_points($userid);
        header('Content-Type: application/json');
        print json_encode(array("result"=>$questions));
    }



}