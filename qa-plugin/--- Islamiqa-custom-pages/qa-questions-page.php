<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 09/07/16
 * Time: 11:33 AM
 */


require_once QA_INCLUDE_DIR.'qa-app-users.php';
require_once QA_INCLUDE_DIR.'qa-app-posts.php';


class qa_questions_page{
    
    
    var $directory;
    var $urltoroot;

    public function __construct()
    {}

    public function load_module( $directory, $urltoroot ){
        $this->directory = $directory;
        $this->urltoroot = $urltoroot;
    }

    public function match_request( $request ){
        return strpos($request,"qa-question")!==FALSE;
    }

    public function process_request( $request ){

        $qa_content = qa_content_prepare();
        unset($qa_content['widgets']);
        unset($qa_content['main']);
        unset($qa_content['direction']);
        unset($qa_content['logo']);
        unset($qa_content['sidebar']);
        unset($qa_content['sidepanel']);

       // ob_start();   // http://php.net/manual/es/function.ob-get-clean.php -- en el vps actual no funciono se agregor ua clase y render

        require_once($this->directory .'data-core.php');
        require_once $this->directory ."templates/searchbox.php";
        $searchbox =  (new Rendeder_search_box())->render(null);

       //        $questions= (new data_core())->questions();

        require_once $this->directory ."templates/questions.php";

        $userid = qa_get_logged_in_userid();        
        $template= (new Render_question())->render((new data_core())->questions($userid,0,'recent',null,10));

        require_once $this->directory ."templates/sidepanel.php";
        $sidepanel = (new Rendeder_side_panel_logged())->render(null);


       // ob_end_flush();

        $qa_content['qa-searchbox'] = $searchbox;
        $qa_content['sidepanel'] = $sidepanel;

        $qa_content['custom']=  $template;
        $qa_content["islamiqa-bootstrap"] ="allquestion";
        return $qa_content;
    }
}
	