Ganbox filter plugin version 1.0 for Question2Answer (http://www.question2answer.org)
Tested on Question2Answer version 1.6.3 (minimum version: 1.5 required)
Author: Georgi Stefanov
Author website: http://ganbox.com
Date: 2014-04-18
Plugin License: GPLv2

Description: 
Plugin for additional filtering of user input during question adding. The plugin supports switchable options in admin panel:
- Prevent question title duplicates;
- Capitalize only the first character of the question title;
- Delete the different types of quotation marks at the start and the end of the question;
- Add a question mark (only one) at the end of the question title if not present (without raising error);
- Search for at least one latin letter (A-Za-z) in the question title (disabled by default);
- Search for at least one cyrillic symbol in the question title (disabled by default).

Localization:
The plugin supports language files. For new language you can copy qa-ganbox-lang-en-GB.php to new file qa-ganbox-lang-xx-XX.php (where xx and XX is your language code) and translate strings after sign "=>" to your language.

Installation:
1. Create new directory ganbox-filter in your qa-plugin directory. 
2. Put all files in directory ganbox-filter. 
3. Upload directory ganbox-filter to web server. 
4. Enter the admin panel of your website and choose needed options.
