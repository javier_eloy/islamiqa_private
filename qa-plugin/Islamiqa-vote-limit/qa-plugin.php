<?php

/*
	Plugin Name: Islamiqa Voting limits
	Plugin URI: http://www.interspire.com
	Plugin Description: allow the admin user no limit on how many times he can vote something up or down
	Plugin Version: 1.0
	Plugin Date: 2015-12-14
	Plugin Author: Islamiqa
	Plugin Author URI: 
	Plugin License: GPLv2
	Plugin Minimum Question2Answer Version: 1.5
	Plugin Update Check URI: 
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}

qa_register_plugin_module(
	'widget', // type of module
	'qa-vote-desc-widget.php', // PHP file containing module class
	'qa_vote_descriptions_widget', // module class name in that PHP file
	'Vote  Limit Descriptions' // human-readable name of module
);


qa_register_plugin_overrides('qa-vote-desc-overrides.php');

/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 
