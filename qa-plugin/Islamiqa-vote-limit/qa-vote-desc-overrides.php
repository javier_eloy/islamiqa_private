<?php
    

	function qa_vote_set($post, $userid, $handle, $cookieid, $vote)
	{
        $level  = qa_get_logged_in_level();
        $vote_limit = (int)qa_opt('plugin_vote_remove_limit');

        if(qa_is_logged_in() && $level < QA_USER_LEVEL_ADMIN && $vote_limit == 1){    
            return qa_vote_set_base($post, $userid, $handle, $cookieid, $vote);
            
        }
        else{
            require_once QA_INCLUDE_DIR.'db/points.php';
            require_once QA_INCLUDE_DIR.'db/hotness.php';
            require_once QA_INCLUDE_DIR.'db/votes.php';
            require_once QA_INCLUDE_DIR.'db/post-create.php';
            require_once QA_INCLUDE_DIR.'app/limits.php';

            $oldvote=(int)qa_db_uservote_get($post['postid'], $userid);
            $vote = (int)$oldvote + (int)$vote;

            qa_db_uservote_set2($post['postid'], $userid, $vote);
            
            qa_db_post_recount_votes($post['postid']);

            $postisanswer=($post['basetype']=='A');

            if ($postisanswer) {
                qa_db_post_acount_update($post['parentid']);
                qa_db_unupaqcount_update();
            }

            $columns=array();

            if ( ($vote>0) || ($oldvote>0) )
                $columns[]=$postisanswer ? 'aupvotes' : 'qupvotes';

            if ( ($vote<0) || ($oldvote<0) )
                $columns[]=$postisanswer ? 'adownvotes' : 'qdownvotes';

            qa_db_points_update_ifuser($userid, $columns);

            qa_db_points_update_ifuser($post['userid'], array($postisanswer ? 'avoteds' : 'qvoteds', 'upvoteds', 'downvoteds'));

            if ($post['basetype']=='Q')
                qa_db_hotness_update($post['postid']);

            if ($vote<0)
                $event=$postisanswer ? 'a_vote_down' : 'q_vote_down';
            elseif ($vote>0)
                $event=$postisanswer ? 'a_vote_up' : 'q_vote_up';
            else
                $event=$postisanswer ? 'a_vote_nil' : 'q_vote_nil';

            qa_report_event($event, $userid, $handle, $cookieid, array(
                'postid' => $post['postid'],
                'userid' => $post['userid'],
                'vote' => $vote,
                'oldvote' => $oldvote,
            ));
        }
	}
    
    
    function qa_db_uservote_set2($postid, $userid, $vote)
    /*
        Set the vote for $userid on $postid to $vote in the database
    */
	{
        qa_db_query_sub(
			'INSERT INTO ^uservotes (postid, userid, vote, flag) VALUES (#, #, #, 0) ON DUPLICATE KEY UPDATE vote=#',
			$postid, $userid, $vote, $vote
		);

	}    
    
    function qa_post_html_fields($post, $userid, $cookieid, $usershtml, $dummy, $options=array())
	{ 
        global $isbyuser; 
        $level  = qa_get_logged_in_level();
        $vote_limit = (int)qa_opt('plugin_vote_remove_limit');

        if(qa_is_logged_in() && $level >= QA_USER_LEVEL_ADMIN && $vote_limit == 1){    
            $post['hidden'] = "0";
            $post['uservote'] = 0;
            // we put userid null for the variable isbyuser always been false
            $userid = null;
        }
        return qa_post_html_fields_base($post, $userid, $cookieid, $usershtml, $dummy, $options);
        
    }
    
	function qa_vote_error_html($post, $vote, $userid, $topage)
	{
        $level  = qa_get_logged_in_level();
        $vote_limit = (int)qa_opt('plugin_vote_remove_limit');

        if(qa_is_logged_in() && $level >= QA_USER_LEVEL_ADMIN && $vote_limit == 1){    
            // we put userid null for the variable isbyuser always been false
            $post['userid'] = null;
        }
        return qa_vote_error_html_base($post, $vote, $userid, $topage);
	}
    
	
    
    
	
 /*
	MODIFIED 24-12-2015 by Carlos Parra
*/
   
	