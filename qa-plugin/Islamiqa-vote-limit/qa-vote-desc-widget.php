<?php

class qa_vote_descriptions_widget {
	
	function allow_template($template)
	{
		return ($template=='tag');
	}

	function allow_region($region)
	{
		return true;
	}
	
	function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
	{
		
	}

	function option_default($option)
	{
		if ($option=='plugin_vote_remove_limit')
			return 0;
		
			
		/*if ($option=='plugin_vote_remove_limit') {
			require_once QA_INCLUDE_DIR.'qa-app-options.php';
			return QA_PERMIT_EXPERTS;
		}*/

		return null;
	}

	function admin_form(&$qa_content)
	{
		/*require_once QA_INCLUDE_DIR.'qa-app-admin.php';
		require_once QA_INCLUDE_DIR.'qa-app-options.php';

		$permitoptions=qa_admin_permit_options(QA_PERMIT_USERS, QA_PERMIT_SUPERS, false, false);
*/
		$saved=false;
		
		if (qa_clicked('plugin_tag_desc_save_button')) {
			qa_opt('plugin_vote_remove_limit', (int)qa_post_text('plugin_vote_remove_limit_field'));
			$saved=true;
		}
		
		return array(
			'ok' => $saved ? 'Vote Limits settings saved' : null,
			
			'fields' => array(
				array(
					'label' => 'Remove voting limits from admin',
					'type' => 'checkbox',
					'value' => (int)qa_opt('plugin_vote_remove_limit'),
					'suffix' => '',
					'tags' => 'NAME="plugin_vote_remove_limit_field"',
				),
			),
			
			'buttons' => array(
				array(
					'label' => 'Save Changes',
					'tags' => 'NAME="plugin_tag_desc_save_button"',
				),
			),
		);
	}

}

/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 