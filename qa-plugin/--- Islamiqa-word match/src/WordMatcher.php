<?php

namespace DictionaryParser;

/**
 * Class CSVParser
 * @package DictionaryParser
 */
class WordMatcher
{
    /**
     * @var String
     */
    protected $dictionaryFile;

    /**
     * @var array
     */
    protected $wordList;

    /**
     * @var array
     */
    protected $dictList;

    public function __construct($dictionaryFile)
    {
        $this->dictionaryFile = $dictionaryFile;

        $this->reloadDictionary();
    }

    /**
     * Build the dictionary with the current CSV file
     *
     * @throws \Exception
     */
    public function reloadDictionary()
    {
        if (!$this->dictionaryFile || !file_exists($this->dictionaryFile)) {
            throw new \Exception('Not a valid file');
        }

        list($wordList, $dictList) = $this->buildDictionaries();

        $this->wordList = $wordList;
        $this->dictList = $dictList;
    }

    /**
     * Load the CSV file and build the dictionaries
     */
    private function buildDictionaries()
    {
        $csv = array_map('str_getcsv', file($this->dictionaryFile));

        $wordList = [];
        $dictList = [];

        foreach ($csv as $index => $dicts) {
            $validWord = array_shift($dicts);

            $dictList[$validWord] = $validWord;
            $wordList[] = $validWord;

            foreach ($dicts as $dictWord) {
                $dictList[$dictWord] = $validWord;
            }
        }

        return [$wordList, $dictList];
    }


    public function findClosest($input)
    {
        return $this->findClosestList($input, true);
    }

    /**
     * Find the closest word to the passed in input
     *
     * @param string $input
     * @param boolean $singleElement
     * @return array|null
     */
    public function findClosestList($input, $singleElement = false)
    {
        $closest = !array_key_exists($input, $this->dictList)
            ? $this->findClosestLevenshtein($input, $singleElement)
            : $input;

        return $closest ?: [];
    }

    /**
     * Find the closest Levenshtein matches
     *
     * @param $input
     * @return array
     *
     * @TODO: This method got very big. Separate it into smaller chunks
     */
    private function findClosestLevenshtein($input, $singleElement = true)
    {
        // no shortest distance found, yet
        $shortest = -1;
        $closest = null;
        $matches = [];

        // loop through words to find the closest
        foreach ($this->dictList as $word => $correction) {

            /**
             * If the input matches part of the word, then add it to the suggestion list
             */
            if (strpos(strtolower($word), strtolower($input)) !== false && !in_array($correction, $matches)) {
                $matches[] = $correction;
            }

            $lev = levenshtein($input, $word);

            // check for an exact match
            if ($lev == 0) {

                // closest word is this one (exact match)
                $closest = $correction;
                $shortest = 0;

                // break out of the loop; we've found an exact match
                break;
            }

            // if this distance ks less than the next found shortest
            // distance, OR if a next shortest word has not yet been found
            if ($lev <= $shortest || $shortest < 0) {
                // set the closest match, and shortest distance
                $closest  = $correction;
                $shortest = $lev;

            }

            /**
             * Do a levenshtein check for every subword to get the closest word match
             */
            $exploded = explode(" ", $word);
            foreach ($exploded as $small_word) {
                // calculate the distance between the input word,
                // and the current word
                $lev = levenshtein($input, $small_word) + levenshtein($input, $word);

                // if this distance ks less than the next found shortest
                // distance, OR if a next shortest word has not yet been found
                if ($lev <= $shortest || $shortest < 0) {
                    // set the closest match, and shortest distance
                    $closest  = $correction;
                    $shortest = $lev;

                }
            }
        }

        if ($singleElement) {
            return $closest;
        }

        /**
         * If the closest word
         */
        if ($closest != $input) {
            $matches[] = $closest;
        }

        return $matches;
    }
}
