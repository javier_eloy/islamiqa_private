<?php

/*
	Plugin Name: Profile Customizer
	Plugin URI: http://bitbucket.org/pupi1985/q2a-profile-customizer-public
	Plugin Description: Adds additional features and fields to the user profile section
	Plugin Version: 2.0.4
	Plugin Date: 2015-01-25
	Plugin Author: Gabriel Zanetti
	Plugin Author URI: http://question2answer.org/qa/user/pupi1985
	Plugin License: Commercial
	Plugin Minimum Question2Answer Version: 1.6
	Plugin Minimum PHP Version: 5.1.2
	Plugin Update Check URI: http://bitbucket.org/pupi1985/q2a-profile-customizer-public/raw/master/qa-plugin.php
*/

