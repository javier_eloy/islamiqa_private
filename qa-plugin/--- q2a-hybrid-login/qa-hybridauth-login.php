<?php
/**
 * Created by PhpStorm.
 * User: gerardo rondon
 * Date: 29/06/16
 * Time: 05:37 PM
 * g.a.rondon@gmail.com
 * @grondon01
 */

class qa_hybridauth_login
{

    var $directory;
    var $urltoroot;
    var $provider;

    function load_module($directory, $urltoroot, $type, $provider) {
        $this->directory = $directory;
        $this->urltoroot = $urltoroot;
        $this->provider = $provider;
    }


    public function match_source($source)
    {
        return $source=='hybridauth';
    }


    public function login_html($tourl, $context)
    {
        // this code does in qa-theme/islamiqa-as-dev/includes/qa_login.php for drop dawn
    }


    public function logout_html($tourl)
    {
        $app_id=qa_opt('facebook_app_id');
        if (!strlen($app_id))
            return;
        echo  '<li class="qa--item qa--logout"><a href="./index.php?qa=logout" class="qa--link">Logout</a></li>';
        //$this->facebook_html($tourl, true, 'menu');
    }


    public function admin_form()
    {
        $saved=false;

        if (qa_clicked('facebook_save_button')) {
            qa_opt('facebook_app_id', qa_post_text('facebook_app_id_field'));
            qa_opt('facebook_app_secret', qa_post_text('facebook_app_secret_field'));
            qa_opt('twitter_app_id', qa_post_text('twitter_app_id_field'));
            qa_opt('twitter_app_secret', qa_post_text('twitter_app_secret_field'));

            qa_opt('google_app_id', qa_post_text('google_app_id_field'));
            qa_opt('google_app_secret', qa_post_text('google_app_secret_field'));

            qa_opt('linkedin_app_id', qa_post_text('linkedin_app_id_field'));
            qa_opt('linkedin_app_secret', qa_post_text('linkedin_app_secret_field'));


            $saved=true;
        }

        $ready=strlen(qa_opt('facebook_app_id')) && strlen(qa_opt('facebook_app_secret'));

        return array(
            'ok' => $saved ? 'hybridauth-2.6.0 Application details saved' : null,

            'fields' => array(
                array(
                    'label' => 'Facebook App ID:',
                    'value' => qa_html(qa_opt('facebook_app_id')),
                    'tags' => 'name="facebook_app_id_field"',
                ),

                array(
                    'label' => 'Facebook App Secret:',
                    'value' => qa_html(qa_opt('facebook_app_secret')),
                    'tags' => 'name="facebook_app_secret_field"',
                    'error' => $ready ? null : 'To use Facebook Login, please <a href="http://developers.facebook.com/setup/" target="_blank">set up a Facebook application</a>.',
                ),

                array(
                    'label' => 'Twitter App ID:',
                    'value' => qa_html(qa_opt('twitter_app_id')),
                    'tags' => 'name="twitter_app_id_field"',
                ),
                array(
                    'label' => 'Twitter App Secret:',
                    'value' => qa_html(qa_opt('twitter_app_secret')),
                    'tags' => 'name="twitter_app_secret_field"',
                    'error' => $ready ? null : 'To use Facebook Login, please <a href="http://developers.facebook.com/setup/" target="_blank">set up a Facebook application</a>.',
                ),

                array(
                    'label' => 'Google App ID:',
                    'value' => qa_html(qa_opt('google_app_id')),
                    'tags' => 'name="google_app_id_field"',
                ),
                array(
                    'label' => 'Google App Secret:',
                    'value' => qa_html(qa_opt('google_app_secret')),
                    'tags' => 'name="google_app_secret_field"',
                    'error' => $ready ? null : 'To use Facebook Login, please <a href="http://developers.facebook.com/setup/" target="_blank">set up a Facebook application</a>.',
                ),


                array(
                    'label' => 'LinkedIn App ID:',
                    'value' => qa_html(qa_opt('linkedin_app_id')),
                    'tags' => 'name="linkedin_app_id_field"',
                ),
                array(
                    'label' => 'LinkedIn App Secret:',
                    'value' => qa_html(qa_opt('linkedin_app_secret')),
                    'tags' => 'name="linkedin_app_secret_field"',
                    'error' => $ready ? null : 'To use Facebook Login, please <a href="http://developers.facebook.com/setup/" target="_blank">set up a Facebook application</a>.',
                ),

            ),

            'buttons' => array(
                array(
                    'label' => 'Save Changes',
                    'tags' => 'name="facebook_save_button"',
                ),
            ),
        );
    }
}
