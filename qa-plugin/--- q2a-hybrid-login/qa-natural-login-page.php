<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 04/07/16
 * Time: 10:33 AM
 */


class qa_natural_login_page
{
    private $directory;

    public function load_module($directory, $urltoroot)
    {

        $this->directory = $directory;
    }

    public function match_request($request)
    {
        return  
                strpos($request, 'hauth.done=Natural')!==FALSE 
                || strpos($request, 'hauth.start=Natural')!==FALSE;
    }



    
    public function process_request($request)
    {

        if (!session_id()) {
            if (!session_start()) {
                session_start();
            }
        }

        require_once QA_INCLUDE_DIR.'app/users.php';
        require_once QA_INCLUDE_DIR.'db/users.php';
        require_once QA_INCLUDE_DIR.'qa-base.php';
        require_once($this->directory .'qa-utils.php');


        if (QA_FINAL_EXTERNAL_USERS)
            qa_fatal_error('User login is handled by external code');

        if (qa_is_logged_in())
            qa_redirect('');


        $inemailhandle=qa_post_text('emailhandle');
        $inpassword=qa_post_text('password');
        $inremember=qa_post_text('remember');


        if (qa_opt('allow_login_email_only') || (strpos($inemailhandle, '@')!==false)) // handles can't contain @ symbols
            $matchusers= qa_hybrid_user_find_by_email($inemailhandle);
        else
            $matchusers=qa_db_user_find_by_handle($inemailhandle);

        if (count($matchusers)==1) { // if matches more than one (should be impossible), don't log in
            $inuserid=$matchusers[0];
            $userinfo=qa_db_select_with_pending(qa_db_user_account_selectspec($inuserid, true));

            var_dump($userinfo);

            var_dump(strtolower(qa_db_calc_passcheck($inpassword, $userinfo['passsalt'])));
            var_dump(strtolower($userinfo['passcheck']));

            if (strtolower(qa_db_calc_passcheck($inpassword, $userinfo['passsalt'])) == strtolower($userinfo['passcheck'])) { // login and redirect
                require_once QA_INCLUDE_DIR.'app/users.php';

                qa_set_logged_in_user($inuserid, $userinfo['handle'], !empty($inremember));
                $tourl = toUrl("tologged");

            } else{

                $tourl = toUrl("toInvalid");
            }
                

        } else{
//            $errors['emailhandle']=qa_lang('users/user_not_found');
            $tourl = toUrl("toRegister");
        }
            
        qa_redirect_raw($tourl);
    }
}
