<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 30/06/16
 * Time: 03:04 PM
 */


class qa_google_login_page
{
    private $directory;

    public function load_module($directory, $urltoroot)
    {

        $this->directory = $directory;
    }

    public function match_request($request)
    {

        return     strpos($request, 'Google') !== FALSE
                   || strpos($request, 'hauth.start=Google') !== FALSE
                   || strpos($request, 'hauth.done=Google') !== FALSE;
    }

    public function process_request($request)
    {

        if (!session_id()) {
            if (!session_start()) {
                session_start();
            }
        }

        require_once($this->directory .'hybridauth-2.6.0/hybridauth/Hybrid/Auth.php');
        require_once($this->directory ."hybridauth-2.6.0/hybridauth/Hybrid/Endpoint.php");
        require_once QA_INCLUDE_DIR.'app/users.php';
        require_once QA_INCLUDE_DIR.'qa-base.php';


        $app_id=qa_opt('google_app_id');
        $secret_id=qa_opt('google_app_secret');

        $url_auth= qa_path_absolute('Google');
        $config = array(
//            "base_url" => "http://localhost.com/index.php?qa=Google",
            "base_url" => $url_auth,
            "providers" => array(
                "Google" => array(
                    "enabled" => true,
                    "keys" => array("id" => $app_id, "secret" => $secret_id),
                    "scope" => "https://www.googleapis.com/auth/userinfo.profile " . // optional
                        "https://www.googleapis.com/auth/userinfo.email", // optional
                )));


        if (isset($_REQUEST['hauth_start']) || isset($_REQUEST['hauth_done'])) {
            Hybrid_Endpoint::process();
        } else {


            $hybridauth = new Hybrid_Auth($config);
    		$authProvider = $hybridauth->authenticate("Google");
	    	$user_profile = $authProvider->getUserProfile();

            if($user_profile && isset($user_profile->identifier))
            {

                qa_log_in_external_user('google', $user_profile->identifier, array(
                    'email' => $user_profile->email,
                    'handle' => $user_profile->displayName,
                    'confirmed' => true,
                    'name' => $user_profile->displayName,
                    'website' => $user_profile->profileURL,
                    'location'=>"",
                    'about'=>"",
                    'avatar' => strlen($user_profile->photoURL) ? qa_retrieve_url($user_profile->photoURL) : null,
                ));
            }

            $tourl = qa_get('to');
            if (!strlen($tourl))
                $tourl = qa_path_absolute('');

            qa_redirect_raw($tourl);

        }
    }

}
