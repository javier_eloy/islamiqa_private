<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 04/07/16
 * Time: 02:09 PM
 */



function qa_hybrid_user_find_by_email($email)   
{

    require_once QA_INCLUDE_DIR.'qa-db.php';
    return qa_db_read_all_values(qa_db_query_sub(
        'SELECT userid FROM ^users WHERE email=$ and NOT exists (select userid from ^userlogins where ^users.userid =  ^userlogins.userid )',
        $email
    ));
}


function qa_hybrid_user_find_by_handle($handle)    
{
    require_once QA_INCLUDE_DIR.'qa-db.php';
    return qa_db_read_all_values(qa_db_query_sub(
        'SELECT userid FROM ^users WHERE handle=$ and NOT exists (select userid from ^userlogins where ^users.userid =  ^userlogins.userid )',
        $handle
    ));
}


function toUrl($to)
{
    require_once QA_INCLUDE_DIR.'qa-base.php';
    $tourl = qa_post_text($to);
    if (!strlen($tourl))
        $tourl = qa_path_absolute('');
    return $tourl;
}