<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 30/06/16
 * Time: 11:29 AM
 */


class qa_linkedin_login_page
{
    private $directory;

    public function load_module($directory, $urltoroot)
    {

        $this->directory = $directory;
    }

    public function match_request($request)
    {
        return strpos($request, 'hauth.done=LinkedIn')!==FALSE || strpos($request, 'hauth.start=LinkedIn')!==FALSE;
    }

    public function process_request($request)
    {

        if (!session_id()) {
            if (!session_start()) {
                session_start();
            }
        }

        require_once($this->directory .'hybridauth-2.6.0/hybridauth/Hybrid/Auth.php');
        require_once($this->directory ."hybridauth-2.6.0/hybridauth/Hybrid/Endpoint.php");
        require_once QA_INCLUDE_DIR.'app/users.php';
        require_once QA_INCLUDE_DIR.'qa-base.php';


        $app_id=qa_opt('linkedin_app_id');
        $secret_id=qa_opt('linkedin_app_secret');
        $url_auth= qa_path_absolute('hauth.start=LinkedIn');


        if (isset($_REQUEST['hauth_start']) || isset($_REQUEST['hauth_done'])) {
            Hybrid_Endpoint::process();
        } else {
            try {

                $config = array("base_url" => $url_auth,
                    "providers" => array (
                        "LinkedIn" => array (
                            "enabled" => true,
                            "keys"    => array ( "key" => $app_id, "secret" => $secret_id ),

                        )
                    ),
                    // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
                    "debug_mode" => false,
                    "debug_file" => "debug.log",
                );


                //email,name,verified,location,website,about,picture

                $hybridauth = new Hybrid_Auth($config);
                $facebook_adapter = $hybridauth->authenticate("LinkedIn");

                $user_profile = $facebook_adapter->getUserProfile();

                if($user_profile && isset($user_profile->identifier))
                {
                    qa_log_in_external_user('LinkedIn', $user_profile->identifier, array(
                        'email' => $user_profile->email,
                        'handle' => $user_profile->displayName,
                        'confirmed' => $user_profile->emailVerified,
                        'name' => $user_profile->displayName,
                        'location' => $user_profile->city . " " . $user_profile->region . $user_profile->country,
                        'website' => $user_profile->webSiteURL,
                        'about' => $user_profile->description,
                        'avatar' => strlen($user_profile->photoURL) ? qa_retrieve_url($user_profile->photoURL) : null,
                    ));

                }

                $tourl = qa_get('to');
                if (!strlen($tourl))
                    $tourl = qa_path_absolute('');

                qa_redirect_raw($tourl);


            } catch (Exception $ex) {

                echo $ex->getMessage();

            }
        }

    }
}
