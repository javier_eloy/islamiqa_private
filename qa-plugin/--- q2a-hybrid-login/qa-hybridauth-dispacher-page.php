<?php

/**
 * Created by PhpStorm.
 * User: gerardo rondon
 * Date: 29/06/16
 * Time: 05:37 PM
 * g.a.rondon@gmail.com
 * @grondon01
 */
class qa_hybridauth_dispacher_page
{
    private $directory;
    private $providers;
    private $selectedProvider;


    /**
     * qa_hybridauth_login_page constructor.
     * @param $directory
     */
    function init_queries( $tableslc ) {

      //  var_dump($tableslc);

    }

    public function load_module($directory, $urltoroot)
    {

//        var_dump("load module");

        $this->providers=[];
        $this->selectedProvider = null;

        require_once($this->directory."qa-facebook-login-page.php");
        require_once($this->directory."qa-twitter-login-page.php");
        require_once($this->directory."qa-google-login-page.php");
        require_once($this->directory."qa-natural-login-page.php");
        require_once($this->directory."qa-register-page.php");
        require_once($this->directory."qa-linkedin-login-page.php");
        require_once($this->directory."qa-valid-user-page.php");


        $this->providers[] = new qa_facebook_login_page();
        $this->providers[] = new qa_twitter_login_page();
        $this->providers[] = new qa_google_login_page();
        $this->providers[] = new qa_natural_login_page();
        $this->providers[] = new qa_register_page();
        $this->providers[] = new qa_linkedin_login_page();
        $this->providers[] = new qa_valid_user_page();


        $this->directory = $directory;
        foreach ($this->providers as $provider){
            $provider->load_module($directory,$urltoroot);
        }

//        var_dump("load module");
    }

    public function match_request($request)
    {
        
        foreach ($this->providers as $provider){

            if($provider->match_request($request))
            {
                $this->selectedProvider = $provider;
                return true;
            }

        }
        $this->selectedProvider = null;
        return false;
    }

    public function process_request($request)
    {
        if($this->selectedProvider){
            $this->selectedProvider->process_request($request);
        }

    }
}
