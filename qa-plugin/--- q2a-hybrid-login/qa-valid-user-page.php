<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 06/07/16
 * Time: 09:11 AM
 */


class qa_valid_user_page
{
    private $directory;

    public function load_module($directory, $urltoroot)
    {
        $this->directory = $directory;
    }

    public function match_request($request)
    {
        return strpos($request, 'check_mail') !== FALSE;
    }

    public function process_request($request)
    {
//        require_once QA_INCLUDE_DIR . 'app/users.php';
//        require_once QA_INCLUDE_DIR . 'qa-base.php';
//        require_once QA_INCLUDE_DIR . 'app/limits.php';
//        require_once QA_INCLUDE_DIR . 'app/users-edit.php';
        require_once($this->directory .'qa-utils.php');

        header('Content-Type: application/json');

        if(isset($_REQUEST['email'])){
            $matchusers = qa_hybrid_user_find_by_email($_REQUEST['email']);
            print ('{"result": ' .count($matchusers) . ' , "message": "' . qa_lang('users/email_exists') . '"  }');
        }
        
        else if(isset($_REQUEST['handle'])){
            $matchusers= qa_hybrid_user_find_by_handle($_REQUEST['handle']);
            print ('{"result": ' .count($matchusers) . ' , "message": "' . qa_lang('users/handle_exists') . '"  }');
        }
        else{
            print ('{"result": 0, "message" : "( no info " }');
        }


    }


}