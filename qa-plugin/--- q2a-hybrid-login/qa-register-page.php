<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 04/07/16
 * Time: 01:18 PM
 */


class qa_register_page
{
    private $directory;

    public function load_module($directory, $urltoroot)
    {

        $this->directory = $directory;
    }

    public function match_request($request)
    {
        return strpos($request, 'hauth.done=Register') !== FALSE || strpos($request, 'hauth.start=Register') !== FALSE;
    }


    function toUrl($to)
    {
        require_once QA_INCLUDE_DIR . 'qa-base.php';
        $tourl = qa_post_text($to);
        if (!strlen($tourl))
            $tourl = qa_path_absolute('');
        return $tourl;
    }

    public function process_request($request)
    {

        if (!session_id()) {
            if (!session_start()) {
                session_start();
            }
        }


        if (qa_is_logged_in())
            qa_redirect('');

        require_once QA_INCLUDE_DIR . 'app/users.php';
        require_once QA_INCLUDE_DIR . 'qa-base.php';
        require_once QA_INCLUDE_DIR . 'app/limits.php';
        require_once QA_INCLUDE_DIR . 'app/users-edit.php';
        require_once($this->directory .'qa-utils.php');

        $inemail = qa_post_text('email');
        $inpassword = qa_post_text('password');
        $inhandle = qa_post_text('name');

        $matchusers= qa_hybrid_user_find_by_email($inemail);
        $errors = [];

        if (count($matchusers)==1) { // if matches more than one (should be impossible), don't log in
            $errors['email']=qa_lang('users/email_exists');
        }

            // core validation
        $errors = array_merge(
            $errors
         //   qa_password_validate($inpassword)
        );


        if (empty($errors)) {
            qa_limits_increment(null, QA_LIMIT_REGISTRATIONS);

            $userid = qa_create_new_user($inemail, $inpassword, $inhandle);
            qa_set_logged_in_user($userid, $inhandle);
            $tourl = toUrl("toRegister");


        } else {
            $tourl = toUrl("toInvalid");
        }

        qa_redirect_raw($tourl);

    }
}
