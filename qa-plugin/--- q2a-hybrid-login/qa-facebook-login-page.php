<?php

/**
 * Created by PhpStorm.
 * User: gerardo rondon
 * Date: 29/06/16
 * Time: 05:37 PM
 * g.a.rondon@gmail.com
 * @grondon01
 */
class qa_facebook_login_page
{
    private $directory;

    public function load_module($directory, $urltoroot)
    {

        $this->directory = $directory;
    }

    public function match_request($request)
    {
        return ($request == 'facebook-login');
    }

    public function process_request($request)
    {

        if (!session_id()) {
            if (!session_start()) {
                session_start();
            }
        }

        require_once($this->directory .'hybridauth-2.6.0/hybridauth/Hybrid/Auth.php');
        require_once($this->directory ."hybridauth-2.6.0/hybridauth/Hybrid/Endpoint.php");
        require_once QA_INCLUDE_DIR.'app/users.php';
        require_once QA_INCLUDE_DIR.'qa-base.php';


        $app_id=qa_opt('facebook_app_id');
        $secret_id=qa_opt('facebook_app_secret');
        $url_auth= qa_path_absolute('facebook-login');


        if (isset($_REQUEST['hauth_start']) || isset($_REQUEST['hauth_done'])) {
            Hybrid_Endpoint::process();
        } else {
            try {
                $config_array = array(
                    "base_url" => $url_auth,
                    "providers" => array(
                        "Facebook" => array(
                            "enabled" => true,
                            "keys" => array("id" => $app_id, "secret" => $secret_id, 'cookie' => true),
                            "scope" => "email, user_about_me, user_birthday, user_hometown"  //optional.
                        ),
                    )
                );
                //email,name,verified,location,website,about,picture

                $hybridauth = new Hybrid_Auth($config_array);
                $facebook_adapter = $hybridauth->authenticate("Facebook");

                $facebook_user_profile = $facebook_adapter->getUserProfile();

                if ($facebook_user_profile) {
                    qa_log_in_external_user('facebook', $facebook_user_profile->identifier, array(
                        'email' => $facebook_user_profile->email,
                        'handle' => $facebook_user_profile->displayName,
                        'confirmed' => $facebook_user_profile->emailVerified,
                        'name' => $facebook_user_profile->displayName,
                        'location' => $facebook_user_profile->city . " " . $facebook_user_profile->region . $facebook_user_profile->country,
                        'website' => $facebook_user_profile->webSiteURL,
                        'about' => $facebook_user_profile->description,
                        'avatar' => strlen($facebook_user_profile->photoURL) ? qa_retrieve_url($facebook_user_profile->photoURL) : null,
                    ));

                }

                $tourl = qa_get('to');
                if (!strlen($tourl))
                    $tourl = qa_path_absolute('');

                qa_redirect_raw($tourl);


            } catch (Exception $ex) {

                echo $ex->getMessage();

            }
        }

    }
}
