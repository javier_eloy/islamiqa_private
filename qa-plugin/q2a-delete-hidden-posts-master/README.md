=============================
Delete Hidden Posts
=============================
-----------
Description
-----------
This is a plugin for **Question2Answer** that deletes all hidden posts having dependencies 

--------
Features
--------
- ability to delete all the hidden posts from q2a database having dependencies 
- when a question is deleted all its answers , comments gets deleted 
- when a answer is deleted all its comments and related questions gets deleted 
- buttons are displayed *after* a question's response buttons - which helps to delete the question right away 
- every options can be configured from admin panel 

----------
Disclaimer
----------
This is **beta** code.  It is probably okay for production environments, but may not work exactly as expected.  Refunds will not be given.  If it breaks, you get to keep both parts.

---------
About q2A
---------
Question2Answer is a free and open source platform for Q&A sites. For more information, visit:

http://www.question2answer.org/

