<?php


class qa_submenu_widget
{
	public function allow_template($template)
	{
		return ($template!='admin');
	}


	public function allow_region($region)
	{
		return in_array($region, array('main', 'side', 'full'));
	}


	public function admin_form(&$qa_content)
	{
		$saved=false;

		if (qa_clicked('adsense_save_button')) {
			$trimchars="=;\"\' \t\r\n"; // prevent common errors by copying and pasting from Javascript
			qa_opt('adsense_publisher_id', trim(qa_post_text('adsense_publisher_id_field'), $trimchars));
			$saved=true;
		}

		return array(
			'ok' => $saved ? 'AdSense settings saved' : null,

			'fields' => array(
				array(
					'label' => 'AdSense Publisher ID:',
					'value' => qa_html(qa_opt('adsense_publisher_id')),
					'tags' => 'name="adsense_publisher_id_field"',
					'note' => 'Example: <i>pub-1234567890123456</i>',
				),
			),

			'buttons' => array(
				array(
					'label' => 'Save Changes',
					'tags' => 'name="adsense_save_button"',
				),
			),
		);
	}


	public function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
	{
		$divstyle='';

		switch ($region) {
			case 'full': // Leaderboard
				$divstyle='width:728px; margin:0 auto;';
				// fall-through

			case 'main': // Leaderboard
				$width=728;
				$height=90;
				$format='728x90_as';
				break;

			case 'side': // Wide skyscraper
				$width=160;
				$height=600;
				$format='160x600_as';
				break;
		}

?>

<?php
	}
}
