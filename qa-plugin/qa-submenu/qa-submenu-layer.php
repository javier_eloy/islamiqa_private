<?php
class qa_html_theme_layer extends qa_html_theme_base
{    
            
    public function q_item_main($q_item)
    {
        //qa_html_theme_base::q_item_main($q_item);
        
        $this->output('<div class="qa-q-item-main">');

		$this->view_count($q_item);
		$this->q_item_title($q_item);
		$this->q_item_content($q_item);
		$this->post_avatar_meta($q_item, 'qa-q-item');
		//$this->post_tags($q_item, 'qa-q-item');
		$this->q_item_buttons($q_item);

		$this->output('</div>');
        
    }
    

    public function q_list_item($q_item)
    {
        
           if(stripos(qa_self_html(),'qa_1=flags')!==false){
                $this->output('<div class="qa-q-list-item'.rtrim(' '.@$q_item['classes']).'" '.@$q_item['tags'].'>');

                $this->q_item_main($q_item);
                $this->q_item_clear();

                $this->output('<div id="tabs" class="qa-flags-list">'
                        . ''
                        . '  <ul>
                                <li class="qa-flag-list">
                                <span>('.$this->qa_get_flags($q_item['raw']['postid'],"Spam").')</span>Spam
                                </li>
                                <li class="qa-flag-list">
                                <span>('.$this->qa_get_flags($q_item['raw']['postid'],"Poor formatting").')</span>Poor formatting
                                </li>
                                <li class="qa-flag-list">
                                <span>('.$this->qa_get_flags($q_item['raw']['postid'],"Insincere/disrespectful").')</span>Insincere/disrespectful
                                </li>
                                <li class="qa-flag-list">
                                <span>('.$this->qa_get_flags($q_item['raw']['postid'],"Wrong language").')</span>Wrong language
                                </li>
                                <li class="qa-flag-list">
                                <span>('.$this->qa_get_flags($q_item['raw']['postid'],"Other").')</span>Other
                                </li>
                              </ul>'
                        . '</div>');

                $this->output('</div> <!-- END qa-q-list-item -->', '');
                

            }else{
                qa_html_theme_base::q_list_item($q_item);
            }
    }
    
    public function q_item_stats($q_item)
	{
		$this->output('<div class="qa-q-item-stats">');

		$this->voting($q_item);
		$this->a_count($q_item);
		qa_html_theme_base::view_count($q_item);
		$this->WriteAnswer($q_item);
                $postid=$q_item['raw']['postid'];
                $userid=qa_get_logged_in_userid(); 
                $li="";
                $flag=$this->qa_check_flag($postid,$userid);
                $code=$this->content['q_list']['form']['hidden']['code'];
                $url=$q_item['url'];
                if($flag==0){
                          $li= '<li onclick="PopupData('.$flag.',\''.$url.'\',\''.$code.'\');Popup(\'qa-dialog-form\',250,\'left\');"><span class="qa-form-light-button qa-form-light-button-flag"></span>Report</li>';
                }else {
                          $li= '<li onclick="PopupData('.$flag.',\''.$url.'\',\''.$code.'\');Popup(\'qa-dialog-form\',280,\'center\');" ><span class="qa-form-light-button qa-form-light-button-flag"></span>Report</li>';
                }
 
            $menu_list='<ul id="submenulist'.$postid.'" class="qa-submenu-list"><li onclick="Popup(\'qa-popup\',300);">Invite expert</li>'
                    .$li.'</ul>';
            $menu_list=  ($q_item)?$menu_list:"";
                if(stripos(qa_self_html(),'qa=admin')===false)
                     $this->output('<a id="submenu" onclick="fillid('.$postid.');" class="qa-submenu" href="#" style="margin-left: 375px;">...</a>'.$menu_list);
		$this->output('</div>');
	}

    function footer() {
            qa_html_theme_base::footer();
            $q_view=(isset($this->content['q_view']))?$this->content['q_view']:false;
            $script = "<script>
                var id ;
                function fillid(v){
                    id=v;
                }
                var menu = $('.qa-submenu-list').menu().hide();
                $('.qa-submenu').click(function() {
                
                id=(id)?id:'';
                
                $('.qa-submenu-list').menu().hide();
                
                        $('#submenulist'+id).menu().show().position({
                              my: 'left top',
                              at: 'left bottom',
                              of: this
                        });
                        $( document ).on( 'click', function() {
                              $('#submenulist'+id).menu().hide();
                        });
                        return false;
                   })
//                var menu = $('.qa-submenu-list').menu().hide();
//                function showmenu(a,b){
//                    
//                    $('#submenulist-'+a).toggle().position({
//                              my: 'left top',
//                              at: 'left bottom',
//                              of: b
//                        });
//                        
//                }
                   

".'
                function Popup(qaId,w,a){
                    $(".qa-dialog-form").css("text-align",a);
                        $(function() {
                         //$("input[name=q_doflag]").click();
                        $( "#"+qaId ).dialog({
                                resizable: true,
                                width:w,
                                modal: true
                            });
                          });
                      }
                function PopupData(flag,url,code){
                    
                    if(flag==1){
                        $("#div1").css("display","none");
                        $("#div2").css("display","block");
                        $("#code").val(code);
                        $("#myform").attr("action",url);
                        
                    }else{
                        $("#div2").css("display","none");
                        $("#div1").css("display","block");
                        $("#code").val(code);
                        $("#myform").attr("action",url);
                    }
                    
                }
                    </script>';
            $popup='<div class="qa-popup" id="qa-popup" title="Invite expert">do you really want to invite an exper? '
                   . '<br><input  value="Invite Exper" class="qa-form-tall-button" style="margin-top:10px;" type="submit">'
                   . '</div>';
            $postid=$q_view['raw']['postid'];
            $userid=qa_get_logged_in_userid(); 
            $code=$q_view['buttons_form_hidden']['code'];
            $li="";
            $flag=$this->qa_check_flag($postid,$userid);
            if($flag==0){
                          $li= '<li onclick="PopupData('.$flag.',\'\',\''.$code.'\');Popup(\'qa-dialog-form\',250,\'left\');"><span class="qa-form-light-button qa-form-light-button-flag"></span>Report</li>';
            }else {
                          $li= '<li onclick="PopupData('.$flag.',\'\',\''.$code.'\');Popup(\'qa-dialog-form\',280,\'center\');" ><span class="qa-form-light-button qa-form-light-button-flag"></span>Report</li>';
            }
 
            $menu_list='<ul id="submenulist" class="qa-submenu-list"><li onclick="Popup(\'qa-popup\',300);">Invite expert</li>'
                    .$li.'</ul>';
            $menu_list=  ($q_view)?$menu_list:"";
            $this->output($menu_list.$script.$popup.$this->dialog_form($q_view)); 
    }



    public function dialog_form($q_view)
    {

        $string='<div id="qa-dialog-form" class="qa-dialog-form" title="report">'
                                    . '<form id="myform" action="" method="post">';
        
            
         //$form=$q_view['form'];
         
             $class='qa-form-tall-button qa-dialog-button';
             $report="Report this question";
             
             $string.='<div id="div1"><p class="validateTips">specify the type of report.</p>
                                <input type="radio" name="report" value="Spam"><label for="radio1">Spam</label><br>
                                <input type="radio" name="report" value="Poor formatting"><label for="radio2">Poor formatting</label><br>
                                <input type="radio" name="report" value="Insincere/disrespectful"><label for="radio3">Insincere/disrespectful</label><br>
                                <input type="radio" name="report" value="Incorrect category"><label for="radio4">Incorrect category</label><br>
                                <input type="radio" name="report" value="Wrong language"><label for="radio5">Wrong language</label><br>
                                <input type="radio" name="report" value="Other"><label for="report6">Other</label><br>';
            
        
            $string.='<input name="q_doflag" onclick="qa_show_waiting_after(this, false);" value="'.$report.'"  title="" class="'.$class.' " type="submit">';
            $string.='</div>';
            
             
             $class='qa-form-tall-button qa-form-tall-button-cancel';
             $report="Cancel your report";
             $string.='<div id="div2"><p class="validateTips">you already reported this question.</p>';
        
            
            $string.='<input name="q_dounflag" onclick="qa_show_waiting_after(this, false);" value="'.$report.'"  title="" class="'.$class.' " type="submit">';
            $string.='</div>';
                    $string.='<input id="code" type="hidden" name="code" value=""/>'
                    . '</form>'
               . '</div>';
            return $string;
         

    }
    function qa_get_flags($postid,$report){
        $count = qa_db_read_all_assoc(qa_db_query_sub(
                            'SELECT count(flag) as c FROM ^uservotes WHERE postid=# AND flag!=0 AND report=#',
                            $postid, $report
                    ));
                    return $count[0]['c'];
    }
    function qa_check_flag($postid,$userid){
        $count = qa_db_read_all_assoc(qa_db_query_sub(
                            'SELECT flag FROM ^uservotes WHERE postid=# AND userid=#',
                            $postid, $userid
                    ));
                    return isset($count[0]['flag'])?$count[0]['flag']:false;
    }
}