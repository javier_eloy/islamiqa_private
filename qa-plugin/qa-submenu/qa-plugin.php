<?php

/*
	Plugin Name: sub menu list
	Plugin URI: 
	Plugin Description: 
	Plugin Version: 1.0
	Plugin Date: 2015-05-07
	Plugin Author:
	Plugin Author URI:
	Plugin License: GPLv2
	Plugin Minimum Question2Answer Version: 1.5
	Plugin Update Check URI: 
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}

qa_register_plugin_module(
	'widget', // type of module
	'qa-submenu-widget.php', // PHP file containing module class
	'qa_submenu_widget', // module class name in that PHP file
	'submenu list' // human-readable name of module
);

qa_register_plugin_layer(
	'qa-submenu-layer.php', // PHP file containing layer
	'submenu list Plugin Layer' // human-readable name of layer
);


