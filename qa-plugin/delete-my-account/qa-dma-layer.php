<?php
if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}
require_once QA_PLUGIN_DIR.'delete-my-account/qa-dma.php';

class qa_html_theme_layer extends qa_html_theme_base {
	function doctype() {
		if(qa_opt(qa_dma::ENABLE)) {
			if($this->template == 'account') {
				$error = array();
				//******************************
				// Terms condition
				//******************************
				$show_terms = false;
				if(!qa_qa_version_below('1.7'))
					$show_terms = qa_opt('show_register_terms');
				//******************************
				// Delete my account
				//******************************
				if (qa_clicked('dodelete')) {
					if ($show_terms && !isset($_POST['terms'])) {
						$this->content['error'] = qa_lang_html(qa_dma::PLUGIN.'/'.qa_dma::ERROR_TERMS);
					} else {
						if (!qa_check_form_security_code('account', qa_post_text('code')))
							$this->content['error'] = qa_lang_html('misc/form_security_again');
						else {
							require_once QA_INCLUDE_DIR.'qa-app-users-edit.php';
							$handle =  qa_get_logged_in_handle();
							$userid =  qa_get_logged_in_userid();
							qa_delete_user($userid);
							qa_report_event('u_delete', $userid, $handle, qa_cookie_get(), array(
								'userid' => $userid,
								'handle' => $handle,
							));
							qa_set_logged_in_user(null);
							qa_redirect('users');
						}
					}
				}
				//******************************
				// Add button for deleting my account
				//******************************
				$this->content['form_profile']['buttons']['delete']=array(
					'tags' => 'name="dodelete" onclick="javascript:var rtn=confirm(\''.qa_lang_html(qa_dma::PLUGIN.'/'.qa_dma::DELETE_CONFIRM).'\');if(rtn){document.getElementsByName(\'dosaveprofile\')[0].remove();}return rtn;"',
					'label' => qa_lang_html(qa_dma::PLUGIN.'/'.qa_dma::DELETE_BUTTON),
				);
				if($show_terms) {
					$note = '<input name="terms" type="checkbox" value="0" class="qa-form-wide-checkbox">';
					$note .= '&nbsp;'.trim(qa_opt('register_terms'));
					$this->content['form_profile']['buttons']['delete']['note'] = $note;
				}
			}
		}
		qa_html_theme_base::doctype();
	}
}

/*
	Omit PHP closing tag to help avoid accidental output
*/