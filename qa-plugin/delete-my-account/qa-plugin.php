<?php
/*
	Plugin Name: Delete My Account
	Plugin URI: 
	Plugin Description: Add button for deleting my account in account page 
	Plugin Version: 1.1
	Plugin Date: 2015-02-03
	Plugin Author: sama55@CMSBOX
	Plugin Author URI: http://www.cmsbox.jp/
	Plugin License: GPLv2
	Plugin Minimum Question2Answer Version: 1.6
	Plugin Update Check URI: 
*/

/* Update history
	1.1
		Add Terms & Condition checkbox in case of V1.7
	1.0
		First release
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}
qa_register_plugin_phrases('lang/qa-dma-lang-*.php', 'dma');
qa_register_plugin_module('module', 'qa-dma.php', 'qa_dma', 'Delete My Account');
qa_register_plugin_layer('qa-dma-layer.php', 'Delete My Account Layer');
/*
	Omit PHP closing tag to help avoid accidental output
*/