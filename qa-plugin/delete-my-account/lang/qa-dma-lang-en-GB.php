<?php
return array(
	'dma_enable_label' => 'Enable plugin:',
	'dma_enable_note' => '',
	'dma_delete_button' => 'Delete my account',
	'dma_delete_confirm' => 'Your account will be deleted. You can not undo this operation. Are you OK?',
	'dma_error_terms' => 'You must agree to the Terms & Conditions and Privacy Policy',
	'dma_save_button' => 'Save Changes',
	'dma_saved_message' => 'Plugin settings saved',
	'dma_reset_button' => 'Reset (Clear settings)',
	'dma_reset_confirm' => 'Settings reset to default. Are you OK?',
	'dma_reset_message' => 'Settings were reset to default',
);

/*
	Omit PHP closing tag to help avoid accidental output
*/