<?php
if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}
class qa_dma {
	const PLUGIN					= 'dma';
	const ENABLE					= 'dma_enable';
	const ENABLE_DFL				= 0;
	const DELETE_BUTTON				= 'dma_delete_button';
	const DELETE_CONFIRM			= 'dma_delete_confirm';
	const ERROR_TERMS				= 'dma_error_terms';
	
	const SAVE_BUTTON				= 'dma_save_button';
	const SAVED_MESSAGE				= 'dma_saved_message';
	const ERROR_MESSAGE				= 'dma_error_message';
	const RESET_BUTTON				= 'dma_reset_button';
	const RESET_CONFIRM				= 'dma_reset_confirm';
	const RESET_MESSAGE				= 'dma_reset_message';

	var $directory;
	var $urltoroot;

	function load_module($directory, $urltoroot) {
		$this->directory=$directory;
		$this->urltoroot=$urltoroot;
	}
	function option_default($option) {
		if ($option==self::ENABLE) return (int)self::ENABLE_DFL;
		return null;
	}
	function admin_form(&$qa_content) {
		$saved = '';
		$error = array();
		
		if (qa_clicked(self::SAVE_BUTTON)) {
			qa_opt(self::ENABLE, (int)qa_post_text(self::ENABLE.'_field'));
			if(!count($error)) {
				$saved = qa_lang(self::PLUGIN.'/'.self::SAVED_MESSAGE);
			} else
				$saved = qa_lang(self::PLUGIN.'/'.self::ERROR_MESSAGE);
		}
		if (qa_clicked(self::RESET_BUTTON)) {
			qa_opt(self::ENABLE, $this->option_default(self::ENABLE));
			$saved = qa_lang(self::PLUGIN.'/'.self::RESET_MESSAGE);
		}
		
		// Field rules
		$rules = array();
		qa_set_display_rules($qa_content, $rules);
		
		$form = array();
		if($saved != '' && !$error)
			$form['ok'] = $saved;

		$form['fields'][] = $this->form_field_check(self::ENABLE, $error);
		
		$form['buttons'][] = array(
			'label' => qa_lang(self::PLUGIN.'/'.self::SAVE_BUTTON),
			'tags' => 'NAME="'.self::SAVE_BUTTON.'" ID="'.self::SAVE_BUTTON.'"',
		);
		$form['buttons'][] = array(
			'label' => qa_lang(self::PLUGIN.'/'.self::RESET_BUTTON),
			'tags' => 'NAME="'.self::RESET_BUTTON.'" ID="'.self::RESET_BUTTON.'" onClick="javascript:return confirm(\''.qa_lang(self::PLUGIN.'/'.self::RESET_CONFIRM).'\')"',
		);
		$form['style'] = 'wide';
		return $form;
	}
	function form_field_check($name, $error, $suffix='') {
		$fieldname = $name.$suffix;
		return array(
			'type' => 'checkbox',
			'id' => $fieldname,
			'tags' => 'NAME="'.$fieldname.'_field" ID="'.$fieldname.'_field"',
			'label' => qa_lang(self::PLUGIN.'/'.$name.'_label'),
			'value' => qa_opt($fieldname),
			'note' => qa_lang(self::PLUGIN.'/'.$name.'_note'),
			'error' => @$error[$fieldname],
		);
	}
}

/*
	Omit PHP closing tag to help avoid accidental output
*/