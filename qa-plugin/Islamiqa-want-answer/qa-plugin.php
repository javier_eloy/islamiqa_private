<?php

/*
	Plugin Name: Islamiqa Want a Answer
	Plugin URI: http://www.interspire.com
	Plugin Description: Gives users a notification when a question is answered
	Plugin Version: 1.0
	Plugin Date: 2015-12-14
	Plugin Author: Islamiqa
	Plugin Author URI: 
	Plugin License: GPLv2
	Plugin Minimum Question2Answer Version: 1.5
	Plugin Update Check URI: 
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}

qa_register_plugin_module(
	'widget', // type of module
	'qa-wantanswer-widget.php', // PHP file containing module class
	'qa_wantanswer_widget', // module class name in that PHP file
	'Want Answer Module' // human-readable name of module
);


qa_register_plugin_layer(
	'qa-wantanswer-layer.php', // PHP file containing layer
	'Want Answer' // human-readable name of layer
);

qa_register_plugin_overrides('qa-wantanswer-override.php');

/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 