<?php

class qa_html_theme_layer extends qa_html_theme_base
{
    // add we own want answer button
    function q_view($q_view)
	{
        
        $wantanswer = qa_opt('plugin_want_answer');
        $userid = qa_get_logged_in_userid();
        $postid = $q_view['raw']['postid'];
        $wantanswerbutton = "wantanswer";
        $tag = "name='q_dowantanswer' id='q_dowantanswer' onclick='return qa_wantanswer_click(".$userid.",".$postid.")'";

        if ($wantanswer && $userid != null ){
            
            $query = "SELECT COUNT(postid) as count FROM ^userwantanswer WHERE userid = #  AND postid = #";
            $result = qa_db_read_one_assoc(qa_db_query_sub($query, $userid, $postid ), true);
            $count = $result['count'];

            if ($count> 0){
                $tag = "name='q_dowantanswer' id='q_dowantanswer' ";
                $wantanswerbutton = "wantanswerclicked";
            }
            
          
            $query = "SELECT COUNT(postid) as count FROM ^userwantanswer WHERE postid =#";
            
            $result = qa_db_read_one_assoc(qa_db_query_sub($query, $postid ), true);
            $count = $result['count'];
            $url = QA_HTML_THEME_LAYER_URLTOROOT;
            $pos = strpos($url,"qa-plugin/");
            $url = substr($url, $pos);
            $root = qa_path_absolute("");

            $root = str_replace("index.php", "",$root);
            $url = $root.$url;

            $q_view['buttons_form_hidden']['wantanswerurl']= $url;
            $q_view['form']['buttons'][$wantanswerbutton]['label'] ="Want Answer (". $count .")";
            $q_view['form']['buttons'][$wantanswerbutton]['popup'] ="Want Answer" ;
            $q_view['form']['buttons'][$wantanswerbutton]['tags'] = $tag;
        }
        qa_html_theme_base::q_view($q_view);
       
	}
  
    // add we own css file
    function head_css() 
    { 
        
        $wantanswer = qa_opt('plugin_want_answer') && $userid = qa_get_logged_in_userid() != null; 
        qa_html_theme_base::head_css(); 
        if ($wantanswer){
            $folder = QA_HTML_THEME_LAYER_URLTOROOT."css/islamiqa.css"; 
            $scss = "<link rel='stylesheet' href='".$folder."'/>"; 
            $this->output($scss); 
        }

    }    
    //add own js file    
	function head_script()
	{
       
        $wantanswer = qa_opt('plugin_want_answer') && $userid = qa_get_logged_in_userid() != null; 
        if ($wantanswer){
            $folder = QA_HTML_THEME_LAYER_URLTOROOT."js/islamiqa.js"; 
            $script = "<script src='".$folder."'></script>"; 
            $this->content['script'][] = $script; 
        }
        qa_html_theme_base::head_script(); 
	}
	
}

/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 