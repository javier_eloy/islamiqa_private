<?php

class qa_wantanswer_widget {
    
    // initialize db-table 'userwantanswer' if it does not exist yet
		function init_queries($tableslc) {
		
			$tablename = qa_db_add_table_prefix('userwantanswer');
			
			// check if event logger has been initialized already (check for one of the options and existing table)
			require_once QA_INCLUDE_DIR.'qa-app-options.php';
			if(qa_opt('plugin_want_answer') && in_array($tablename, $tableslc)) {
				// options exist, but check if really enabled
				if(qa_opt('plugin_want_answer')=='' && qa_opt('plugin_want_answer')=='') {
					// enabled database logging
					qa_opt('plugin_want_answer', 1);
				}
			}
			else {
				// not enabled, let's enable the event logger
				if (!in_array($tablename, $tableslc)) {
					require_once QA_INCLUDE_DIR.'qa-app-users.php';
					require_once QA_INCLUDE_DIR.'qa-db-maxima.php';
					
					return 'create table ^userwantanswer (
						userid int (10),
						postid int (10),
						primary key(userid, postid)
					)ENGINE=MyISAM DEFAULT CHARSET=utf8'; 

				}
			}

		} // end init_queries
        
	
	function allow_template($template)
	{
		return ($template!='admin');
	}

	function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
	{
	}

	function option_default($option)
	{
		if ($option=='plugin_vote_want_answer')
			return 0;

		return null;
	}

	function admin_form(&$qa_content)
	{
		/*require_once QA_INCLUDE_DIR.'qa-app-admin.php';
		require_once QA_INCLUDE_DIR.'qa-app-options.php';

		$permitoptions=qa_admin_permit_options(QA_PERMIT_USERS, QA_PERMIT_SUPERS, false, false);
*/
		$saved=false;
		
		if (qa_clicked('plugin_wantanswer_save_button')) {
            qa_opt('plugin_want_answer' , (int)qa_post_text('plugin_want_answer_field'));
			$saved=true;
		}
		
		return array(
			'ok' => $saved ? 'Want Answer settings saved' : null,
			
			'fields' => array(
				array(
					'label' => 'Activate want answers link under questions',
					'type' => 'checkbox',
					'value' => (int)qa_opt('plugin_want_answer'),
					'suffix' => '',
					'tags' => 'NAME="plugin_want_answer_field"',
				),
			),
			
            
            
			'buttons' => array(
				array(
					'label' => 'Save Changes',
					'tags' => 'NAME="plugin_wantanswer_save_button"',
				),
			),
		);
	}

}
/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 