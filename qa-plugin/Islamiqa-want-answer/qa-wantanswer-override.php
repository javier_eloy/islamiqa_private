<?php
    function qa_report_event($event, $userid, $handle, $cookieid, $params=array())
    {
        qa_report_event_base($event, $userid, $handle, $cookieid, $params);

        $onsitenotifications_enabled = qa_opt('q2apro_onsitenotifications_enabled');    
        //returns blank the  notification plugin doesn't exist 
        if ($onsitenotifications_enabled == "")
            return; 

        if ($event == 'a_post'){
            $postid = $params['parentid'];

            $currentuser = $userid;
            if ($currentuser == null)
                $currentuser = 0;

                $query = "SELECT userid FROM ^userwantanswer WHERE postid =# and userid <> # " ;
                $result = qa_db_read_all_values(qa_db_query_sub($query, $postid, $currentuser ));
                foreach ($result as $userids){
                    process_event_wantanswer($event, $userids, $handle, $cookieid, $params); 
                }
        } 
    }
        
  function process_event_wantanswer($event, $userid, $handle, $cookieid, $params) {
			
			if(!qa_opt('event_logger_to_database')) return;
			
			// needed for function qa_post_userid_to_handle()
			require_once QA_INCLUDE_DIR.'qa-app-posts.php';

				// userid from the post
				$uid = qa_db_read_one_value(
					qa_db_query_sub(
						'SELECT userid FROM ^posts WHERE postid=#',
						$params['postid']
					),true);
                
				
				// if QA poster is not the same as answered
				if($uid != $userid) {
			
					$ohandle = qa_post_userid_to_handle($userid);
                    $oevent = 'in_a_question';

                    $paramstring='';
					
					foreach ($params as $key => $value){
						$paramstring.=(strlen($paramstring) ? "\t" : '').$key.'='.value_to_text($value);
                    }
					
					qa_db_query_sub(
						'INSERT INTO ^eventlog (datetime, ipaddress, userid, handle, cookieid, event, params) '.
						'VALUES (NOW(), $, $, $, #, $, $)',
						qa_remote_ip_address(), $userid, $ohandle, $cookieid, $oevent, $paramstring
					);				
				}
	
			
		} // end process_event


		// worker functions
		function value_to_text($value) {
			if (is_array($value))
				$text='array('.count($value).')';
			elseif (strlen($value)>40)
				$text=substr($value, 0, 38).'...';
			else
				$text=$value;
				
			return strtr($text, "\t\n\r", '   ');
		}    

/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 