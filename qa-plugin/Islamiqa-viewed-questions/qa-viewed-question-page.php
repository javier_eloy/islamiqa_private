<?php

class qa_viewed_question_page {
	
	function match_request($request)
	{
		$parts=explode('/', $request);
		return $parts[0]=='questionsviewed';
	}
	
	function process_request($request)
	{
        $parts=explode('/', $request);
		$viewed=$parts[0];
        if ($viewed == 'questionsviewed'){
		$path = dirname(__FILE__);
            require $path.'/qa-viewed-question-questions.php';
            global $qa_request;
            $qa_request ='questions';
            return $qa_content;
        }
	}
	
}
/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 