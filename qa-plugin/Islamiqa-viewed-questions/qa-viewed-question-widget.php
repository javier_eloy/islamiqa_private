<?php

class qa_viewed_question_widget {
    
// initialize db-table 'userwantanswer' if it does not exist yet
		function init_queries($tableslc) {
		
			$tablename = qa_db_add_table_prefix('userviewedquestions');
			
			// check if event logger has been initialized already (check for one of the options and existing table)
			require_once QA_INCLUDE_DIR.'qa-app-options.php';
			if(qa_opt('plugin_viewed_question') && in_array($tablename, $tableslc)) {
				// options exist, but check if really enabled
				if(qa_opt('plugin_viewed_question')=='' && qa_opt('plugin_viewed_question')=='') {
					// enabled database logging
					qa_opt('plugin_viewed_question', 1);
				}
			}
			else {
				// not enabled, let's enable the event logger
				if (!in_array($tablename, $tableslc)) {
					require_once QA_INCLUDE_DIR.'qa-app-users.php';
					require_once QA_INCLUDE_DIR.'qa-db-maxima.php';
					
					return 'create table ^userviewedquestions (
						userid int (10),
						postid int (10),
						primary key(userid, postid)
					)ENGINE=MyISAM DEFAULT CHARSET=utf8'; 

				}
			}

		} // end init_queries
            
	
	function allow_template($template)
	{
		return ($template=='questions');
	}

	function allow_region($region)
	{
		return true;
	}
	
	function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
	{
		return;
	}

	function option_default($option)
	{
		if ($option=='plugin_viewed_question')
			return 0;
		
		return null;
	}

	function admin_form(&$qa_content)
	{

		$saved=false;
		
		if (qa_clicked('plugin_viewed_question_save_button')) {
			qa_opt('plugin_viewed_question', (int)qa_post_text('plugin_viewed_question_field'));
			$saved=true;
		}
		
		return array(
			'ok' => $saved ? 'Viewed Questions settings saved' : null,
			
			'fields' => array(
				array(
					'label' => 'Prevents questions appearing on most feeds / lists when the user has seen them" to "Prevents questions appearing on user home feed after they have been viewed',
					'type' => 'checkbox',
					'value' => (int)qa_opt('plugin_viewed_question'),
					'suffix' => '',
					'tags' => 'NAME="plugin_viewed_question_field"',
				),
			),
			
			'buttons' => array(
				array(
					'label' => 'Save Changes',
					'tags' => 'NAME="plugin_viewed_question_save_button"',
				),
			),
		);
	}

}

/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 

