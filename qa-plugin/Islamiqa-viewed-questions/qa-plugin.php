<?php

/*
	Plugin Name: Viewed Question
	Plugin URI: http://www.interspire.com
	Plugin Description: prevents questions appearing on most feeds / lists
	Plugin Version: 1.0
	Plugin Date: 2015-12-14
	Plugin Author: Islamiqa
	Plugin Author URI: 
	Plugin License: GPLv2
	Plugin Minimum Question2Answer Version: 1.5
	Plugin Update Check URI: 
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}

qa_register_plugin_module(
	'widget', // type of module
	'qa-viewed-question-widget.php', // PHP file containing module class
	'qa_viewed_question_widget', // module class name in that PHP file
	'Viewed Question' // human-readable name of module
);

qa_register_plugin_module(
	'page', // type of module
	'qa-viewed-question-page.php', // PHP file containing module class
	'qa_viewed_question_page', // name of module class
	'Viewed Question Page' // human-readable name of module
);

qa_register_plugin_layer(
	'qa-viewed-question-layer.php', // PHP file containing layer
	'Viewed Question Layer' // human-readable name of layer
);

qa_register_plugin_overrides('qa-viewed-question-overrides.php');

/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 
