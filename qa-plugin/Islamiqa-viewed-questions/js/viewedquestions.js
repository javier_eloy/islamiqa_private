function qa_viewedquestions(postid)
{
    
    viewedurl = $("input[name=viewedquestionsurl]"); //buscas el control o selector por el nombre sin necesidad del id
    if (viewedurl.val() == undefined)
        return false;
    
    var url = viewedurl.val() + '/qa-viewed-question-db-function.php?action=qa_viewedquestion_db_set&postid=' + postid;
                $.ajax({
                url: url, 
                type: "POST",
                dataType: 'json',
                success:function(data){
                    //console.log('listo');
                    
                }
            });
            
    return false;            
}            
   
	$(function(){
            //detect scroll down
            var obj = $(document);          //objeto sobre el que quiero detectar scroll
            var obj_top = $(window).height() - 200;

            obj.scroll(function(){
                var obj_act_top = $(this).scrollTop() + 500;  //obtener scroll top instantaneo
                //console.log('primera '+obj_act_top)
                if(obj_act_top > obj_top){
                    checkQuestion(obj_act_top);
                }
                // scroll top before
               obj_top = obj_act_top;  
            });
        });
        
function checkQuestion(obj_top){
    listItem = document.getElementsByClassName('qa-q-list-item');
    count = listItem.length;
    
    for(var i= 0; i < count; i++ ){
        parent = listItem[i].id;
        obj = $('#'+parent+'');
        pos = obj.position(); 
        if (pos.top < obj_top ){
            postid = parent.substr(1);
            //console.log(i + ' control '+ pos.top);
            //console.log('scroll ' + obj_top);
            qa_viewedquestions(postid);
        }
    }
}        

/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 