<?php

class qa_html_theme_layer extends qa_html_theme_base
{

    //add own js file    
	function head_script()
	{
       
        $viewedquestion = qa_opt('plugin_viewed_question') && $userid = qa_get_logged_in_userid() != null; 
        if ($viewedquestion){
            $folder = QA_HTML_THEME_LAYER_URLTOROOT."js/viewedquestions.js"; 
            $script = "<script src='".$folder."'></script>"; 
            $this->content['script'][] = $script; 
        }
        qa_html_theme_base::head_script(); 
	}
    
   
	
}

/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 