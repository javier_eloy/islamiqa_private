<?php
/*
	Question2Answer by Gideon Greenspan and contributors
	http://www.question2answer.org/

	File: qa-plugin/islamiqa-csv-uploads/qa-plugin.php
	Description: Allows uploading of users, categories and q&a's via csv files

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

/*
	Plugin Name: Islamiqa CSV Uploader
	Plugin URI:
	Plugin Description: Uploads csv files for users, categories and q&a's
	Plugin Version: 1.0
	Plugin Date: 2015-02-06
	Plugin Author: Islamiqa
	Plugin Author URI:
	Plugin License: GPLv2
	Plugin Minimum Question2Answer Version: 1.7
	Plugin Update Check URI:
*/


if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}


//qa_register_plugin_layer('qa-mouseover-layer.php', 'Mouseover Layer');
qa_register_plugin_module('module', 'qa-csvupload-admin-form.php', 'qa_csvupload_admin_form', 'CSV Uploader');
