<?php
/*
	Question2Answer by Gideon Greenspan and contributors
	http://www.question2answer.org/

	File: qa-plugin/mouseover-layer/qa-mouseover-admin-form.php
	Description: Generic module class for mouseover layer plugin to provide admin form and default option


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

class qa_csvupload_admin_form
{
	public function option_default($option)
	{
		if ($option == 'csv_user_categories_filename')
			return 'DEVCreateUsers.php';
		elseif ($option == 'csv_qas_filename')
			return 'DEVUploadContent.php';
		elseif ($option == 'interests')
			return 1;
		elseif ($option == 'knowledges')
			return 1;
		elseif ($option == 'follows')
			return 1;
	}


	public function admin_form(&$qa_content)
	{
		$submitted1 = false;
		$submitted2 = false;
	
		if (qa_clicked('csvuploaduc_submit_button')) {
			/*qa_opt('mouseover_content_on', (int) qa_post_text('mouseover_content_on_field'));
			qa_opt('mouseover_content_max_len', (int) qa_post_text('mouseover_content_max_len_field'));*/
			qa_opt('csv_user_categories_filename', qa_post_text('csv_user_categories'));
			$this->Upload_Users_Categories();
			$submitted1 = true;
		} elseif (qa_clicked('csvuploadqa_submit_button')) {
			qa_opt('csv_qas_filename', qa_post_text('csv_qas'));
			//echo qa_opt('csv_qas_filename');
			$this->Upload_QAs();
			$submitted2 = true;
		}

		qa_set_display_rules($qa_content, array(
			/*'mouseover_content_max_len_display' => 'mouseover_content_on_field',*/
			/*'csv_user_categories_filename_display' => 'csv_user_categories',*/
		));

		return array(
			'ok' => $submitted1 ? 'Uploaded Users/Categories' :  ($submitted2 ? 'Uploaded Q&As': null),

			'fields' => array(
/*				array(
					'label' => 'Show content preview on mouseover in question lists',
					'type' => 'checkbox',
					'value' => qa_opt('mouseover_content_on'),
					'tags' => 'name="mouseover_content_on_field" id="mouseover_content_on_field"',
				),*/
				array(
					'id' => 'csv_user_categories_filename_display',
					'label' => 'Filename for user/categories file (.txt):',
                    'tags' => 'name="csv_user_categories" id="csv_user_categories"',
					'type' => 'text',
                    'value' => qa_opt('csv_user_categories_filename'),
                ),
				array(
					'id' => 'csv_qas_filename',
					'label' => 'Filename for Q&As file (.txt):',
                    'tags' => 'name="csv_qas" id="csv_qas"',
					'type' => 'text',
                    'value' => qa_opt('csv_qas_filename'),
                ),
/*				array(
					'id' => 'mouseover_content_max_len_display',
					'label' => 'Maximum length of preview:',
					'suffix' => 'characters',
					'type' => 'number',
					'value' => (int) qa_opt('mouseover_content_max_len'),
					'tags' => 'name="mouseover_content_max_len_field"',
				),*/
			),

			'buttons' => array(
				array(
					'label' => 'Upload Users+Categories',
					'tags' => 'name="csvuploaduc_submit_button"',
				),
				array(
					'label' => 'Upload Q&As',
					'tags' => 'name="csvuploadqa_submit_button"',
				),
			),
		);
	}

	
	public function Upload_Users_Categories()
	{
		// Remember to comment out welcome notification email qa_send_notification() of qa_create_new_user().
		require_once './qa-include/qa-base.php'; 
		require_once './qa-include/qa-db-users.php'; 
		require_once './qa-include/qa-app-users-edit.php';
		require_once './qa-include/qa-app-users.php';
		require_once './qa-include/qa-app-posts.php';
		require_once './qa-include/qa-db-admin.php'; // categories

		// Lists
		$users         = array("Alan","Cohen","Colin","David","Greg","Jack","Joseph","Paul","Sam","Anna","Claire","Julie","Samantha","Sharon","Tracy","AbdurRahman","Abumalik","Ahsan","Ali","Arif","Asif","Ata","Bilal","Clarke","Eesa","Eustachy","Halimi","Hamzah","Hasan","Hasim","Mazin","Moazzem","Mohammed","Nadeem","Nadeem","Roshan","Shah","Shahid","Sharif","Shehzeb","Shoaib","Sy","Tariq","Yousef","Zaf","Zubear","Aisha","Asiya","Fatima","Laila","Mahbuba");
		$passwords     = array("Alan_123","Cohen_123","Colin_123","David_123","Greg_123","Jack_123","Joseph_123","Paul_123","Sam_123","Anna_123","Claire_123","Julie_123","Samantha_123","Sharon_123","Tracy_123","AbdurRahman_123","Abumalik_123","Ahsan_123","Ali_123","Arif_123","Asif_123","Ata_123","Bilal_123","Clarke_123","Eesa_123","Eustachy_123","Halimi_123","Hamzah_123","Hasan_123","Hasim_123","Mazin_123","Moazzem_123","Mohammed_123","Nadeem_123","Nadeem_123","Roshan_123","Shah_123","Shahid_123","Sharif_123","Shehzeb_123","Shoaib_123","Sy_123","Tariq_123","Yousef_123","Zaf_123","Zubear_123","Aisha_123","Asiya_123","Fatima_123","Laila_123","Mahbuba_123");

		$categories    = array("Beliefs","Culture","Family","History","Sciences","Politics","People","Places","Languages","Organisations","Groups and Movements");
		$subcategories = array( 
						"History"=>array("1"=>"Prior Messengers","2"=>"Pre-Islam","3"=>"Seerah","4"=>"Khulafah Rashida","5"=>"Umayyads","6"=>"Abbasids","7"=>"Ottomans","8"=>"Nation States","9"=>"Others"),
						"Culture"=>array("1"=>"Quran","2"=>"Hadith","3"=>"Fiqh","4"=>"Usul al-Fiqh","5"=>"Kalam","6"=>"Falsafa","7"=>"Others"),
						"Places"=>array("1"=>"Europe","2"=>"Americas","3"=>"Africa","4"=>"Asia","5"=>"Australasia"),
						"Languages"=>array("1"=>"Arabic","2"=>"Malay","3"=>"Persian","4"=>"Turkish","5"=>"Urdu","6"=>"Others"),
						"Organisations"=>array("1"=>"Schools","2"=>"Universities","3"=>"Charities","4"=>"Businesses","5"=>"Media","6"=>"Prisons","7"=>"Government Offices"),
						"Groups and Movements"=>array("1"=>"al-Qaida","2"=>"Boko Haram","3"=>"Gulen","4"=>"Hamas","5"=>"Hizb ut-Tahrir","6"=>"Hizbullah","7"=>"Ikhwan al-Muslimoon","8"=>"ISIS","9"=>"Jamati Islami","10"=>"Minhaj al-Quran","11"=>"Nation of Islam","12"=>"Others","13"=>"Taliban","14"=>"Tehreek Insaaf"),
						 );
		
		// Create UserIds
//		echo "<b>Processing...<br/><br/></b>";
		for($i = 0 ; $i < count($users) ; $i++) { 
//		  echo "Processing email no $i $users[$i] : ";
		   
		  $handle   = $users[$i];
		  //$email    = 'info@delstoneservices.co.uk'; 
		  //$email    = $users[$i] . '@example.com'; 
		  $email    = 'info@delstoneservices.co.uk'; 
		  $password = $passwords[$i]; 
		  
		  if (qa_handle_to_userid($handle) > 0) {
//			echo $handle . " already exists <br/>";
		  } else {
			$userid = qa_create_new_user($email, $password, $handle); 
			qa_db_user_set_flag($userid, QA_USER_FLAGS_EMAIL_CONFIRMED, true);
//			echo "created successfully <br/>";
		  }
		}
		
//		echo "<b>Completed User Creation<br/><br/></b>";
		
		
		// Create Categories	
		for($i = 0 ; $i < count($categories) ; $i++) {
//			echo "Processing category $i $categories[$i] : ";
		
			$category         = $categories[$i];
			$categories[$i]   = strtolower(str_replace(' ', '-', $categories[$i]));
			
			$id = qa_db_read_one_value(qa_db_query_sub('SELECT categoryid FROM ^categories WHERE parentid<=># AND title=$',NULL, $category), true);
			
			if ($id > 0) {
//				echo $categories[$i] . " $id already exists <br/>";
			} else {
				$categoryid = qa_db_category_create(NULL, $category, $categories[$i]);
//				echo "created successfully <br/>";
			}
		}
			
//		echo "<b>Completed Category Creation<br/></b>";
		

		// Create Sub-Categories	
		$ctr = count($subcategories);
		foreach($subcategories as $x=>$x_value) {
			if ($x_value == "") {
//				echo $x." : no subcategories...<br/>";
				continue;
			}
//			echo "Processing subcategory $x : ";
				
			foreach($x_value as $y=>$y_value) {
				$subcategory         = $y_value;
				$y_value		     = strtolower(str_replace(' ', '-', $y_value));

				$parentid = qa_db_read_one_value(qa_db_query_sub('SELECT categoryid FROM ^categories WHERE title=$', $x), true);
				$id       = qa_db_read_one_value(qa_db_query_sub('SELECT categoryid FROM ^categories WHERE parentid=# AND tags=$', $parentid, $y_value), true);
				
				if ($id > 0) {
//					echo $x."-".$y_value . " $id already exists <br/>";
				} else {
					$categoryid = qa_db_category_create($parentid, $subcategory, $y_value);
//					echo "$y_value created successfully <br/>";
				}
			}
		}

//		echo "<b>Completed Sub-Category Creation<br/></b>";
	}

	public function Upload_QAs()
	{
		require_once './qa-include/qa-base.php';
		require_once './qa-include/qa-app-users.php';
		require_once './qa-include/qa-app-posts.php';
		require_once './qa-include/qa-db-admin.php'; // categories
		
		$loop = 0;
		
		if (($handle = fopen(qa_opt('csv_qas_filename'), "r")) != FALSE) {
			while (($fields = fgetcsv($handle, 0, "\t")) !== FALSE) {
				if (strpos($fields[0], 'Question') !== false || $fields[0] == "" || $fields[0] == "\t") continue; // skip header or blank records				
				$loop++;
				// check if question title already exists, prevent duplicate 
				if (qa_db_read_one_value( qa_db_query_sub('SELECT title FROM `^posts` WHERE title = # AND type = "Q" LIMIT 1', $fields[0]), true)) {
					echo "($loop) '$fields[0]' : is a duplicate question - excluding from database <br/>";
					continue;
				} else {
					echo "($loop) '$fields[0]' : imported successfully <br/>";
				}

				// post a question
				$type       = 'Q'; // question
				$parentid   = null; // does not follow another answer
				$title      = $fields[0];
				$content    = $fields[1];
				$format     = ''; // plain text
				//$categoryid = qa_db_read_one_value(qa_db_query_sub('SELECT categoryid FROM ^categories WHERE parentid<=># AND title=$',NULL, $fields[2]), true);
				$categoryid = qa_db_read_one_value(qa_db_query_sub('SELECT categoryid FROM ^categories WHERE title=$',$fields[2]), true);
				$tags       = array($fields[3]);
				$userhandle = ($fields[6]=='' ? null : $fields[6]);
				$userid     = qa_handle_to_userid($userhandle);
//				echo $userhandle . "---". $userid . "<br/>";
				$parentid = qa_post_create($type, $parentid, $title, $content, $format, $categoryid, $tags, $userid);

				$ctr = 10;
				while ($fields[$ctr] != '') {
					// post an answer
					$type       = 'A'; // question
					$title      = '';
					$content    = $fields[$ctr];
					$format     = ''; // plain text
					$categoryid = ''; 
					$tags       = '';
					$userhandle = ($fields[$ctr-2]=='' ? null : $fields[$ctr-2]);
					$userid     = qa_handle_to_userid($userhandle);
					qa_post_create($type, $parentid, $title, $content, $format, $categoryid, $tags, $userid);
					$ctr += 3;
					if ($fields[$ctr+3] != '') sleep(1);
				}
			}
		} else {  // error opening the file.
			echo "Could not open file!";
			return;
		} 

		fclose($handle);	

		//	echo 'Complete'
		}		
}
