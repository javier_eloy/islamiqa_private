<?php
if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}
require_once QA_PLUGIN_DIR.'infinite-ajax-scroll/qa-ias.php';

class qa_html_theme_layer extends qa_html_theme_base {

	private $ias;

	function qa_html_theme_layer($template, $content, $rooturl, $request) {
		qa_html_theme_base::qa_html_theme_base($template, $content, $rooturl, $request);
		$this->ias = new qa_ias();
	}
	function head_css() {
		qa_html_theme_base::head_css();
		if ($this->ias->enable && ($this->ias->ias1_enable || $this->ias->ias2_enable || $this->ias->ias3_enable)) {
			if($this->ias->l_css)
				$this->output('<LINK REL="stylesheet" TYPE="text/css" HREF="'.$this->ias->plugincssurl.'jquery.ias.css"/>');
		}
	}
	function head_script() {
		qa_html_theme_base::head_script();
		if ($this->ias->enable && ($this->ias->ias1_enable || $this->ias->ias2_enable || $this->ias->ias3_enable)) {
			if($this->ias->l_iasjs) {
				//$this->output('<SCRIPT TYPE="text/javascript" SRC="'.$this->ias->pluginjsurl.'jquery-ias.js"></SCRIPT>');
				$this->output('<SCRIPT TYPE="text/javascript" SRC="'.$this->ias->pluginjsurl.'jquery-ias-min.js"></SCRIPT>');
			}
			if($this->ias->l_js)
				$this->output('<SCRIPT TYPE="text/javascript" SRC="'.$this->ias->pluginjsurl.'ias.js"></SCRIPT>');
		}
	}
}

/*
	Omit PHP closing tag to help avoid accidental output
*/