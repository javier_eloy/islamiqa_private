/*******************************************************************/
Infinite Ajax Scroll plugin for question2answer
/*******************************************************************/

/*-----------------------------------------------------------------*/
1. Summary
/*-----------------------------------------------------------------*/
This package is plugin for question2answer.

question2answer: http://www.question2answer.org/

/*-----------------------------------------------------------------*/
2. Feature
/*-----------------------------------------------------------------*/
This plugin add "Infinite Ajax Scroll" feature to Q2A.

/*-----------------------------------------------------------------*/
3. Correspondence Version
/*-----------------------------------------------------------------*/
question2answer V1.5

/*-----------------------------------------------------------------*/
4. Installation/Settings
/*-----------------------------------------------------------------*/
1.Unzip archive any local folder.
2.Upload "infinite-ajax-scroll" folder under qa-plugin.
3.Log in administrator account.
4.Select admin -> plugins menu.
5.Check "Enable plugin".

[Enable plugin]
OFF: Disable this plugin.
ON : Enable this plugin.

[Enable IAS1]
OFF: Disable IAS1 script.
ON : Enable IAS1 script.

[Enable IAS2]
OFF: Disable IAS2 script.
ON : Enable IAS2 script.

[container - history]
Refer to IAS official repository about options.
https://github.com/webcreate/Infinite-Ajax-Scroll#readme

/*-----------------------------------------------------------------*/
5. Uninstallation
/*-----------------------------------------------------------------*/
1.Log in administrator account.
2.Select admin -> plugins menu.
3.Click "Reset" button.
4.Delete "infinite-ajax-scroll" folder under qa-plugin.

/*-----------------------------------------------------------------*/
6. License / Disclaimer
/*-----------------------------------------------------------------*/
1.This software obeys license of Question2Answer.
2.The author does not always promise to support.
3.The Author does not compensate you for what kind of damage that you used question2answer or this file.

/*-----------------------------------------------------------------*/
7. Author/Creator
/*-----------------------------------------------------------------*/
handle: sama55
site: http://cmsbox.jp/

/*-----------------------------------------------------------------*/
8. Version history
/*-----------------------------------------------------------------*/
■[2013/09/28] 1.0		released
■[2013/09/29] 1.0.1		Changed default value of noneleft option. (ON -> OFF)
■[2013/09/30] 1.0.2		Changed default value of "ias1_container" option. (.qa-part-q-list -> .qa-q-list) for V1.5
							Changed default value of "ias2_pagination" option. ('' ->.qa-page-links-list)
							Changed default value of "ias2_next" option. ('' ->.qa-page-next)
■[2013/10/02] 1.1		Added other option.
							Added IAS3.
							Added check logic for tag existing.
■[2013/10/08] 1.1.1		Fixed option cache bug.
■[2013/10/11] 1.1.2		Compress js file
■[2013/10/23] 1.1.3		Change CSS (Remove display:none of pager)
							Change IASx_TRIGGERPAGETHRESHOLD_DFL from 0 to 3
■[2013/11/07] 1.2		Replace javascript from inline to file
■[2013/11/12] 1.2.1		Fixed bug javascript generation
■[2013/11/13] 1.2.2		Chaged noneleft parameter type (from checkbox to textbox)

Have fun !!