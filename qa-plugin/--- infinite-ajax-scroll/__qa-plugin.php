<?php
/*
	Plugin Name: Infinite Ajax Scroll
	Plugin URI: 
	Plugin Description: Replace from page navigation to infinite ajax scroll. 
	Plugin Version: 1.3
	Plugin Date: 2013-08-26
	Plugin Author: sama55@CMSBOX
	Plugin Author URI: http://www.cmsbox.jp/
	Plugin License: GPLv2
	Plugin Minimum Question2Answer Version: 1.5
	Plugin Update Check URI: 
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}
qa_register_plugin_phrases('lang/qa-ias-lang-*.php', 'ias');
qa_register_plugin_module('module', 'qa-ias.php', 'qa_ias', 'IAS');
qa_register_plugin_layer('qa-ias-layer.php', 'IAS Layer');
/*
	Omit PHP closing tag to help avoid accidental output
*/