<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/tag-followers-widget/qa-tag-followers-widget-lang-default.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: US English language phrases for favorite user of tag widget plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

return array(
	'tagfollowerswidget_count_label' => 'Display Number',
	'tagfollowerswidget_count_suffix' => 'users',
	'tagfollowerswidget_count_error' => 'Please specify positive integer at display number',
	'tagfollowerswidget_title_label' => 'Display Title',
	'tagfollowerswidget_desc_label' => 'Display Description',
	'tagfollowerswidget_avatar_label' => 'Display Avatar: ^1recomend default avatar setting^2',
	'tagfollowerswidget_avatar_size_label' => 'Size',
	'tagfollowerswidget_avatar_size_suffix' => 'pix',
	'tagfollowerswidget_avatar_size_error' => 'Please specify positive integer at avatar size',
	'tagfollowerswidget_name_label' => 'Display Name',
	'tagfollowerswidget_name_suffix' => 'If fullname is empty, handle is displayed.',
	'tagfollowerswidget_custom_csspath_label' => 'CSS file path',
	'tagfollowerswidget_save_button' => 'Save Changes',
	'tagfollowerswidget_dfl_button' => 'Restore Default',
	'tagfollowerswidget_saved_message' => 'Plugin settings saved',
	'tagfollowerswidget_widget_title' => 'Topic followers',
	'tagfollowerswidget_widget_desc' => '^ users followed this topic.',
	'tagfollowerswidget_widget_setting' => '^1Setup^2',
);

/*
	Omit PHP closing tag to help avoid accidental output
*/