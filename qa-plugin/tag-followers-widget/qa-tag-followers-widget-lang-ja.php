<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/tag-followers-widget/qa-tag-followers-widget-lang-ja.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: Japanese language phrases for favorite user of tag plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

return array(
	'tagfollowerswidget_count_label' => '表示数',
	'tagfollowerswidget_count_suffix' => 'ユーザー',
	'tagfollowerswidget_count_error' => '表示件数は正の整数を指定してください。',
	'tagfollowerswidget_title_label' => 'タイトルを表示',
	'tagfollowerswidget_desc_label' => '説明を表示',
	'tagfollowerswidget_avatar_label' => 'アバターを表示: ^1デフォルトアバターの設定を推奨^2',
	'tagfollowerswidget_avatar_size_label' => 'サイズ',
	'tagfollowerswidget_avatar_size_suffix' => 'ピクセル',
	'tagfollowerswidget_avatar_size_error' => 'アバターサイズは正の整数を指定してください。',
	'tagfollowerswidget_name_label' => '名前を表示',
	'tagfollowerswidget_name_suffix' => 'fullnameが空の場合はhandleを表示',
	'tagfollowerswidget_custom_csspath_label' => 'スタイルシートのパス',
	'tagfollowerswidget_save_button' => '変更を保存',
	'tagfollowerswidget_dfl_button' => '初期値に戻す',
	'tagfollowerswidget_saved_message' => 'プラグインの設定を保存しました。',
	'tagfollowerswidget_widget_title' => 'タグフォローユーザー',
	'tagfollowerswidget_widget_desc' => '^ 人のユーザーがフォローしています。',
	'tagfollowerswidget_widget_setting' => '^1設定^2',
);

/*
	Omit PHP closing tag to help avoid accidental output
*/