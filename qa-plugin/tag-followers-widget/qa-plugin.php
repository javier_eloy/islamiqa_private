<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/tag-followers-widget/qa-plugin.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: Initiates tag followers widget plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

/*
	Plugin Name: Tag Followers Widget
	Plugin URI: 
	Plugin Description: Provides list of users who follow tag
	Plugin Version: 1.1.3
	Plugin Date: 2015-01-20
	Plugin Author: sama55@CMSBOX
	Plugin Author URI: http://www.cmsbox.jp/
	Plugin License: GPLv2
	Plugin Minimum Question2Answer Version: 1.5
	Plugin Update Check URI: 
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}

qa_register_plugin_phrases('qa-tag-followers-widget-lang-*.php', 'tagfollowerswidget');
qa_register_plugin_module('widget', 'qa-tag-followers-widget.php', 'qa_tag_followers_widget', 'Tag Followers Widget');
qa_register_plugin_layer('qa-tag-followers-widget-layer.php', 'Tag Followers Widget');

/*
	Omit PHP closing tag to help avoid accidental output
*/