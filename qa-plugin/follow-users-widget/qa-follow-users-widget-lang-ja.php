<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/follow-users-widget/qa-follow-users-widget-lang-ja.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: Japanese language phrases for follow users widget plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

return array(
	'followuserswidget_count_label' => '表示数',
	'followuserswidget_count_suffix' => 'ユーザー',
	'followuserswidget_count_error' => '表示件数は正の整数を指定してください。',
	'followuserswidget_title_label' => 'タイトルを表示',
	'followuserswidget_desc_label' => '説明を表示',
	'followuserswidget_avatar_label' => 'アバターを表示: ^1デフォルトアバターの設定を推奨^2',
	'followuserswidget_avatar_size_label' => 'サイズ',
	'followuserswidget_avatar_size_suffix' => 'ピクセル',
	'followuserswidget_avatar_size_error' => 'アバターサイズは正の整数を指定してください。',
	'followuserswidget_name_label' => '名前を表示',
	'followuserswidget_name_suffix' => 'fullnameが空の場合はhandleを表示',
	'followuserswidget_custom_csspath_label' => 'スタイルシートのパス',
	'followuserswidget_save_button' => '変更を保存',
	'followuserswidget_dfl_button' => '初期値に戻す',
	'followuserswidget_saved_message' => 'プラグインの設定を保存しました。',
	'followuserswidget_widget_title' => 'フォローしているユーザー',
	'followuserswidget_widget_desc' => '^ 人のユーザーをフォローしています',
	'followuserswidget_widget_setting' => '^1設定^2',
);

/*
	Omit PHP closing tag to help avoid accidental output
*/