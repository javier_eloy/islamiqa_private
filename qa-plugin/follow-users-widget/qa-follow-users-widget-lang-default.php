<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/follow-users-widget/qa-follow-users-widget-lang-default.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: US English language phrases for follow users widget plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

return array(
	'followuserswidget_count_label' => 'Display Number',
	'followuserswidget_count_suffix' => 'users',
	'followuserswidget_count_error' => 'Please specify positive integer at display number',
	'followuserswidget_title_label' => 'Display Title',
	'followuserswidget_desc_label' => 'Display Description',
	'followuserswidget_avatar_label' => 'Display Avatar: ^1recomend default avatar setting^2',
	'followuserswidget_avatar_size_label' => 'Size',
	'followuserswidget_avatar_size_suffix' => 'pix',
	'followuserswidget_avatar_size_error' => 'Please specify positive integer at avatar size',
	'followuserswidget_name_label' => 'Display Name',
	'followuserswidget_name_suffix' => 'If fullname is empty, handle is displayed.',
	'followuserswidget_custom_csspath_label' => 'CSS file path',
	'followuserswidget_save_button' => 'Save Changes',
	'followuserswidget_dfl_button' => 'Restore Default',
	'followuserswidget_saved_message' => 'Plugin settings saved',
	'followuserswidget_widget_title' => 'Follow users',
	'followuserswidget_widget_desc' => 'I have followed ^ users.',
	'followuserswidget_widget_setting' => '^1Setup^2',
);

/*
	Omit PHP closing tag to help avoid accidental output
*/