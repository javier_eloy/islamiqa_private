<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/question-followers-widget/qa-question-followers-widget-lang-default.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: US English language phrases for question followers widget plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

return array(
	'questionfollowerswidget_count_label' => 'Display Number',
	'questionfollowerswidget_count_suffix' => 'users',
	'questionfollowerswidget_count_error' => 'Please specify positive integer at display number',
	'questionfollowerswidget_title_label' => 'Display Title',
	'questionfollowerswidget_desc_label' => 'Display Description',
	'questionfollowerswidget_avatar_label' => 'Display Avatar: ^1recomend default avatar setting^2',
	'questionfollowerswidget_avatar_size_label' => 'Size',
	'questionfollowerswidget_avatar_size_suffix' => 'pix',
	'questionfollowerswidget_avatar_size_error' => 'Please specify positive integer at avatar size',
	'questionfollowerswidget_name_label' => 'Display Name',
	'questionfollowerswidget_name_suffix' => 'If fullname is empty, handle is displayed.',
	'questionfollowerswidget_custom_csspath_label' => 'CSS file path',
	'questionfollowerswidget_save_button' => 'Save Changes',
	'questionfollowerswidget_dfl_button' => 'Restore Default',
	'questionfollowerswidget_saved_message' => 'Plugin settings saved',
	'questionfollowerswidget_widget_title' => 'Question followers',
	'questionfollowerswidget_widget_desc' => '^ users followed this question.',
	'questionfollowerswidget_widget_setting' => '^1Setup^2',
);

/*
	Omit PHP closing tag to help avoid accidental output
*/