<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/question-followers-widget/qa-question-followers-widget-lang-ja.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: Japanese language phrases for question followers widget plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

return array(
	'questionfollowerswidget_count_label' => '表示数',
	'questionfollowerswidget_count_suffix' => 'ユーザー',
	'questionfollowerswidget_count_error' => '表示件数は正の整数を指定してください。',
	'questionfollowerswidget_title_label' => 'タイトルを表示',
	'questionfollowerswidget_desc_label' => '説明を表示',
	'questionfollowerswidget_avatar_label' => 'アバターを表示: ^1デフォルトアバターの設定を推奨^2',
	'questionfollowerswidget_avatar_size_label' => 'サイズ',
	'questionfollowerswidget_avatar_size_suffix' => 'ピクセル',
	'questionfollowerswidget_avatar_size_error' => 'アバターサイズは正の整数を指定してください。',
	'questionfollowerswidget_name_label' => '名前を表示',
	'questionfollowerswidget_name_suffix' => 'fullnameが空の場合はhandleを表示',
	'questionfollowerswidget_custom_csspath_label' => 'スタイルシートのパス',
	'questionfollowerswidget_save_button' => '変更を保存',
	'questionfollowerswidget_dfl_button' => '初期値に戻す',
	'questionfollowerswidget_saved_message' => 'プラグインの設定を保存しました。',
	'questionfollowerswidget_widget_title' => '質問フォローユーザー',
	'questionfollowerswidget_widget_desc' => '^ 人のユーザーがフォローしています。',
	'questionfollowerswidget_widget_setting' => '^1設定^2',
);

/*
	Omit PHP closing tag to help avoid accidental output
*/