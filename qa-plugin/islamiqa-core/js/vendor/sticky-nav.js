$(window).scroll(function() {
    //if you hard code, then use console
    //.log to determine when you want the 
    //nav bar to stick.  
    //console.log($(window).scrollTop())
    if ($(window).scrollTop() > 50) {
        $('#header').addClass('navbar-sticky');
    }
    if ($(window).scrollTop() < 50) {
        $('#header').removeClass('navbar-sticky');
    }
});