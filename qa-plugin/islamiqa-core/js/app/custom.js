 /*
 * Updated by Julio Velasquez
 * Date: 04/07/2016
 * Time: 17:48
 * Modified by Javier Hernandez
 * Date: 16/08/2016
 */

var ajaxLink; 
var arrayTopic = new Array();
var arrayExpertise = new Array();
var arrayUsers = new Array();
var newList = []; // data array for iterest
var newListExpertice = []; // data array for expertices
var arrayListFollows = []; // data array for follows
var arrayValue = [];
var arrayValueExpertice = [];
var current_view='grid';
var current_object = '';
var current_name = 'all';


function escapeRegExp(str) {
        var res = str.toLowerCase();
		var newString = res.replace(/ /g, "_");
        var finalString = newString.replace(/[()]/g, "");
        return finalString;
}

function getURLParameter(url, name) {
    return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
}

function getBaseUrl() {
    var re = new RegExp(/^.*\//);
    return re.exec(window.location.href);
}

function getAjaxLink() {
  url = String(getBaseUrl());
  url = url.replace('index.php/', ''); 
  url = url + $('#ajaxlink').val();
//  console.log(url);
  return url;  
}

function getAjaxLinkBestAnswer() {
  url = String(getBaseUrl());
  url = url + 'qajax-bestanswer';
  return url;
}




function getAjaxLinkFavorite() {
  url = String(getBaseUrl());
  url = url + 'qajax-favorite';
//  console.log(url);
  return url;
}

function getAjaxLinkStep1() {
  url = String(getBaseUrl());
  url = url + 'qajax-step1';
//  console.log(url);
  return url;
}

function getAjaxLinkStep1Images() {
  url = String(getBaseUrl());
  url = url + '/qa-plugin/islamiqa-core/images/';
//  console.log(url);
  return url;
}

function getAjaxLinkHeadFilters() {
  url = String(getBaseUrl());
  url = url + 'qajax-header';
//  console.log(url);
  return url;
}

function getAjaxLinkVotes() {
  url = String(getBaseUrl());
  url = url +'qajax-votes';
  return url;
}

function getAjaxAutocomplete()
{
    url = String(getBaseUrl());
    url = url + 'qajax-autocomplete';

	return url;
}

function getAjaxLinkNewQuestion()
{
    url = String(getBaseUrl());
    url = url + 'qajax-newquestion';

	return url;
}

function getAjaxLinkNewAnswer()
{
    url = String(getBaseUrl());
    url = url + 'qajax-newanswer';

    return url;
}

function getAjaxLinkIsLogged()
{
    url = String(getBaseUrl());
    url = url + 'qajax-islogged';
//    console.log(url);
    return url;
}

function getAjaxLinkTags()
{
    url = String(getBaseUrl());
    url = url + 'qajax-tags';

	//console.log(url);
    return url;
}

function getAjaxLinkHtmlFollow()
{
    url = String(getBaseUrl());
    url = url + 'qajax-html-userfollow';
    return url;
}

function getAjaxLinkHtmlFollower()
{
    url = String(getBaseUrl());
    url = url + 'qajax-html-userfollower';
    return url;
}

function getAjaxLinkGallery() {
  url = String(getBaseUrl());
  url = url + 'qajax-html-gallery';
  return url;
}


 // Popup window code
function newPopup(url) {
  popupWindow = window.open(url,"popUpWindow",
                 "height=450,width=1000,left=20,top=20,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no");
}

//$ajaxlinkvotes = $context->rooturl . 'updownvote.php';

function addVote(id) {

	$.ajax({
			url: getAjaxLinkVotes(),
			data:"id="+id+"&type=up",
			type: "POST",     
			dataType: 'json',
			success: function(resp){ 
				if (resp.success){
					$("#span-"+id).html(resp.upvote);
					$("#span-"+id+"-downvote").html(resp.downvote);
				} else
				{
					var mess=document.getElementById('errorbox');

					if (!mess) {
						var mess=document.createElement('div');
						mess.id='errorbox';
						mess.className='qa-error';
						mess.innerHTML=resp.reason;
						mess.style.display='none';
					}
					var anchor= 'q'+id;					
					
					var postelem=document.getElementById(anchor);
					if(postelem)
					{				    
				      var e=postelem.parentNode.insertBefore(mess, postelem);
					  qa_reveal(e);
					}  else
						jq_alert(resp.reason);

			    }
			  }
		 }); 
 }
                        
function downVote(id) {
	 $.ajax({
			url: getAjaxLinkVotes(),
			data:"id="+id+"&type=down",
			type: "POST",                          
			dataType: 'json',
			success: function(resp){ 
				if (resp.success){
					$("#span-"+id).html(resp.upvote);
					$("#span-"+id+"-downvote").html(resp.downvote);
				} else {
					var mess=document.getElementById('errorbox');

					if (!mess) {
						var mess=document.createElement('div');
						mess.id='errorbox';
						mess.className='qa-error';
						mess.innerHTML=resp.reason;
						mess.style.display='none';
					}					
					var anchor= 'q'+id;
					var postelem=document.getElementById(anchor);
					if(postelem)
					{	
						var e=postelem.parentNode.insertBefore(mess, postelem);
						qa_reveal(e);
					} else
						jq_alert(resp.reason);
				}
			}
		 }); 
}
                        
function addVoteunans(id) {

   $.ajax({
			url: getAjaxLinkVotes(),
			data:"id="+id+"&type=up",
			type: "POST",                          
			success: function(resp){ 
						 $("#spanunans-"+id).text(resp);
						 
					 }
		 }); 
}
                        
function downVoteunans(id) {

 $.ajax({
		url: getAjaxLinkVotes(),
		data:"id="+id+"&type=down",
		type: "POST",

		success: function(resp){ 
					 $("#spanunans-"+id).text(resp);
				 }
	 }); 
}


function NewAnswer()
{
	
	var id=$("#q_id").val(); // Question Id
	var content_editor= CKEDITOR.instances.qa_editor.getData(); // Content

	var settings = [" "];
	var setchk = $('input[name="setanswer[]"]:checked');
	atLeastOneIsChecked = setchk.length > 0;

	if (atLeastOneIsChecked) {
	  for (var i = 0; i < setchk.length; i++) {
		settings[i] = setchk[i].value;
	  }
	}
	
	$.ajax({
		url: getAjaxLinkNewAnswer(),
		data: {questionid:id,content:content_editor,setanswer:settings},
		type:"POST",
		dataType:"json",
		success: function(resp) { 						
				if(resp['success']) {
				   jq_alert('Answer created.');
				   $('div#new_answer').dialog('close');				   
				} else {
					jq_alert(resp['reason']);
				}
			
		},
		error: function( req, status, err ) {
				console.log( 'something went wrong', status, err );
				jq_alert("ERROR: Posting answer");
			}
		});
	
}

function NewQuestion()
{
	var validations_flag = true;
	var atLeastOneIsChecked = false;

	var title = $("#newq_title").val();
	var content = $("#newq_content").val();
	
	var fields = [];
	fields[0] = title;
	fields[1] = content;

	var string_tags = $("input#tags").val();
	var tags = [];
	tags = string_tags.split(" ");

	var catchk = [];
	var categories = [" "];
	catchk = $('div>input[name="cat[]"]:checked');
	atLeastOneIsChecked = catchk.length > 0;
	
	if (atLeastOneIsChecked) {
	  for (var i = 0; i < catchk.length; i++) {
		categories[i] = catchk[i].value;
	  }
	}
	else validations_flag = false;

	var setchk = [];
	var settings = [" "];
	setchk = $('input[name="set[]"]:checked');
	atLeastOneIsChecked = setchk.length > 0;
	
	if (atLeastOneIsChecked) {
	  for (var i = 0; i < setchk.length; i++) {
		settings[i] = setchk[i].value;
	  }
	}
	
	// Create a new FormData object.	
	var afile = $('#my_file1');	
	var fData = new FormData();
	fData.append('file_image', afile[0].files[0]);
	fData.append('fld_array', JSON.stringify(fields));
	fData.append('tag_array', JSON.stringify(tags));
	fData.append('set_array', JSON.stringify(settings));
	fData.append('cat_array', JSON.stringify(categories));
	//--- Start send
	$.ajax({
		url: getAjaxLinkNewQuestion(),
		data: fData,
		/*params: {fld_array:fields, tag_array:tags, set_array:settings, cat_array:categories},*/
		type:"POST",
		dataType:"json",
		processData: false,
		contentType: false,
		success: function(resp) {
			if (resp.success)
			{
			  jq_alert("Has been added a new question with postid: " + resp.postid);

			  var path = getBaseUrl() + "?qa=" + resp.postid + "/" + content.split(' ').join('-');
			  location.href = path;
			}
			else
			{
			  $("p#login_message").text(resp.reason);
			  jq_alert(resp.reason);
			}
		},

		error: function( req, status, err ) {
			console.log( 'something went wrong', status, err );
			jq_alert("ERROR: Posting a new question");
		}
	});
}
/*
        }).done(function (resp) {
            var sucess = resp.success;
            console.log(success);

            if (success) {
              var question = content;
              console.log(question);

              var path = getBaseUrl() + "?qa=" + resp.postid + "/" + question.split(' ').join('-');
              console.log(path);
              location.href = path;
            }
            else {
              console.log("You must log in to ask a new question");
              alert("You must log in to ask a new question");
            }

        }).fail(function (resp) {
            console.log(resp.success);
            

        }).always(function (resp) {
            console.log(resp.postid);
            alert(resp.postid);
        });
*/        

jQuery.fn.exists = function(){
	return this.length>0;
}
                    
function wantAnswerClick(elem, userid, postid) {
   $.ajax({
			url: getAjaxLinkVotes(),
			data: {type:'wantanswer',postid: postid, userid:userid},
			type: "POST",     
			dataType: 'json',
			success: function(data){
					$(($(elem)[0].parentElement.firstElementChild)).html(data.count);
			  }
		 }); 
}


/*---------------------------------- Step1 Scripts -----------------------------------------------------*/


//Go To Step1
function gotostep1(){
	$("#step1").show();
	$("#step2").hide();
	$("#step3").hide();
	$("#step4").hide();
}

//Go To Step2
function gotostep2(){
	$("#step3").hide();
	$("#step4").hide();
	$("#step1").hide();
	$("#step2").show();
	LoadStep2();
	window.scrollTo(0, 0);
}

//Go To Step3
function gotostep3(){
	$("#step1").hide();
	$("#step2").hide();
	$("#step4").hide();
	$("#step3").show();
	var urltoroot = $("#urltoroot").val();
	LoadStep3(urltoroot);
	window.scrollTo(0, 0);
}

//Go To Step4
function gotostep4(){
	$("#step1").hide();
	$("#step2").hide();
	$("#step3").hide();
	$("#step4").show();
	LoadSaveEvent();
	window.scrollTo(0, 0);
}


function LoadTopic() {

  $.ajax({
        url: getAjaxLinkStep1(),
        type: "POST",
        global: false,
        async:false,
        data: { flag: "LoadTopicList" },
        dataType: "json",

        error: function (data) {
            jq_alert("Error loading data.");
        },

        success: function (data) {
            //console.log(data)
            //alert(data.topics)
            for (var i in data) {
                id = data[i].id;
                title = data[i].title;
                parentid = data[i].parentid;
               /* type = data[i].type;*/
                image = data[i].blobid;

                var arrayCategory = new Array(6)
                arrayCategory[0] = id;
                arrayCategory[1] = title;
                arrayCategory[2] = parentid;
               /* arrayCategory[3] = type;*/
                arrayCategory[4] = false;
                arrayCategory[5] = image;
                arrayTopic[i] = arrayCategory;

                var arrayCategory2 = arrayCategory.slice(0);
                arrayExpertise[i] = arrayCategory2;
            }//end for
        }// end success
    });//end ajax
}

function LoadUsers() {
    var value = $("#urltoroot").val();

    function hashCode(str) {
        var hash = 0;
        for (var i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        return hash;
    }

    function intToRGB(i){
        var c = (i & 0x00FFFFFF)
            .toString(16)
            .toUpperCase();

        return "00000".substring(0, 6 - c.length) + c;
  }

    $.ajax({
        url: getAjaxLinkStep1(),
        type: "POST",
        global: false,
        async: false,
        data: { flag: "LoadUsersList" },
        dataType: "json",
        error: function (data) {
            console.log(data);
            jq_alert("Error loading data.");
        },
        success: function (data) {
            //alert(data)
            //console.log(data);
            var showme='';
            var length = 10;
            var length_exert = 2;
            var length_color = 1;

            for (var i in data) {
                userid = data[i].userid;
                emailhandle = data[i].email;
                handle = data[i].handle;
                avatarblobid = data[i].avatarblobid;
                avatarwidth = data[i].avatarwidth;
                avatarheight = data[i].avatarheight;
                level = data[i].level;
                wallposts = data[i].wallposts;

                var block = new Array(9)
                block[0] = userid;
                block[1] = handle;
                block[2] = avatarblobid;
                block[3] = avatarwidth;
                block[4] = avatarheight;
                block[5] = level;
                block[6] = wallposts;
                block[7] = false; //check or uncheck false by default

                var nombre = handle.substring(0, length);
                var nombre_exert = nombre.substring(0, length_exert);
                var nombre_exert_upp = nombre_exert.toUpperCase();
                var nc = nombre_exert.substring(0, length_color);
                var color_ini = nc + nc + nc + nc + nc + nc;
                var color_ini_upp = color_ini.toLowerCase();

                showme = showme + "<div class='col-xs-3'>";
                showme = showme + "    <div class='col-xs-12 margin-top-5per'>";
                showme = showme + "        <div class='col-xs-6'>";
                //showme = showme + "            <img src='"+value+"images/profile-pic.jpg' alt='img' class='images-circle'>";
                //showme = showme + data[i][0]; // TODO: change this after the system has his own image manager system.
                showme = showme + "            <img id='avatar"+userid+"' class='images-circle'>"; // TODO: change this after the system has his own image manager system.
                showme = showme + "        </div>";
                showme = showme + "        <div class='col-xs-6 margin-top-10per'>";
                showme = showme + "            <strong class='username-bold'>"+nombre+"</strong>";
                showme = showme + "            <div class='smaller'><i class='fa fa-users' aria-hidden='true'></i> 43K &nbsp;&nbsp; <i class='fa fa-flag' aria-hidden='true'></i> "+wallposts+"</div>";
                showme = showme + "            <div><label class='follow'><input type='checkbox' id='user"+userid+"' name='user"+userid+"' style='display: none;'/><i class='fa fa-plus' aria-hidden='true'></i>&nbsp;Follow</label></div>";
                showme = showme + "        </div>";
                showme = showme + "    </div>";
                showme = showme + "</div>";
				
                showme = showme + "<script>";
                showme = showme + "$('#user"+userid+"').on('click', function() {";
                showme = showme + "    if($(this).is(':checked')) {";
                showme = showme + "        ArrayFollow('"+userid+"');";
                //showme = showme + "        console.log(arrayListFollows);";
                showme = showme + "        $('#user"+userid+"').prop('checked', true).parent().css({'background' : '#B40404'});";
                //showme = showme + "        alert('activado');";
                showme = showme + "    } else {";
                showme = showme + "        DeleteFromArrayFollow('"+userid+"');";
                //showme = showme + "        console.log(arrayListFollows);";
                showme = showme + "        $('#user"+userid+"').prop('checked', false).parent().css({'background' : '#04B486'});";
                //showme = showme + "        alert('desactivado');";
                showme = showme + "    }";
                showme = showme + "});";
                showme = showme + "</script>";

                showme = showme + "<script>";
                showme = showme + "$.ajax({";
                showme = showme + "    url: 'https://www.gravatar.com/avatar/"+window.md5(emailhandle)+"?s=200&r=pg&d=404',";
                showme = showme + "    data: {value: 1},";
                showme = showme + "    method: 'post',";
                showme = showme + "    error: function(XMLHttpRequest, textStatus, errorThrown) {";
                showme = showme + "        $('#avatar"+userid+"').avatar({";
                showme = showme + "            useGravatar: false,";
                showme = showme + "            fallbackImage: '',";
                showme = showme + "            size: 250,";
                showme = showme + "            initials: '"+nombre_exert_upp+"',";
                showme = showme + "            initial_fg: '#FEFEFE',";
                showme = showme + "            initial_bg: '#"+intToRGB(hashCode(color_ini_upp))+"',";
                showme = showme + "            initial_size: null,";
                showme = showme + "            initial_weight: 100,";
                showme = showme + "            initial_font_family: 'Comic Sans',";
                showme = showme + "            hash: '"+window.md5(emailhandle)+"',";
                showme = showme + "            email: '"+emailhandle+"',";
                showme = showme + "            fallback: 'mm',";
                showme = showme + "            rating: 'x',";
                showme = showme + "            forcedefault: true,";
                showme = showme + "            allowGravatarFallback: true";
                showme = showme + "        });";
                showme = showme + "    },";
                showme = showme + "    success: function(data) {";
                showme = showme + "        $('#avatar"+userid+"').avatar({";
                showme = showme + "            useGravatar: true,";
                showme = showme + "            fallbackImage: '',";
                showme = showme + "            size: 250,";
                showme = showme + "            initials: null,";
                showme = showme + "            initial_fg: '#FEFEFE',";
                showme = showme + "            initial_bg: '#"+intToRGB(hashCode(color_ini_upp))+"',";
                showme = showme + "            initial_size: 0,";
                showme = showme + "            initial_weight: 100,";
                showme = showme + "            initial_font_family: 'Comic Sans',";
                showme = showme + "            hash: '"+window.md5(emailhandle)+"',";
                showme = showme + "            email: '"+emailhandle+"',";
                showme = showme + "            fallback: 'mm',";
                showme = showme + "            rating: 'x',";
                showme = showme + "            forcedefault: false,";
                showme = showme + "            allowGravatarFallback: false";
                showme = showme + "        });";
                showme = showme + "    }";
                showme = showme + "});";
                showme = showme + "</script>";

                block[8] = showme;
                arrayUsers[i] = block;
            }//end for
        }// end success
    });//end ajax
    //console.log(arrayUsers)
}

function LoadStep1() {
    LoadTopic();

    border_top_register_step1 = 'border-top-register-step1';
    col_left_step1 = '';
    col_right_step1 = '';
    flag_col_left_step1 = true;

    for (i=0;i<arrayTopic.length;i++) {
        id = arrayTopic[i][0];
        title = arrayTopic[i][1];
        parentid = arrayTopic[i][2];
       /* type = arrayTopic[i][3];
	   checked = arrayTopic[i][4];*/

        if (parentid == null) {
            border_top_register_step1 = 'border-top-register-step1';
            if (flag_col_left_step1) {
                col_left_step1 = col_left_step1 + '<li class="st-li shadow-topics-register-step1 ' +border_top_register_step1+'">';
                value = id;
            } else
                col_left_step1 = col_left_step1 + '<li class="st-li shadow-topics-register-step1">';
                col_left_step1 = col_left_step1 + '    <label class="step1-label-left">';
                col_left_step1 = col_left_step1 + '        <div class="checkbox">';
                col_left_step1 = col_left_step1 + '            <label>&nbsp;<input type="checkbox" value="'+id+'" name="topics[]" id="topic'+id+'" onclick="LoadStep1Especific('+id+');">&nbsp;'+title+'</label>';
                col_left_step1 = col_left_step1 + '        </div>';
                col_left_step1 = col_left_step1 + '        <span class="fa fa-chevron-circle-right fa-lg" id="span-topic'+id+'"></span>';
                col_left_step1 = col_left_step1 + '    </label>';
                col_left_step1 = col_left_step1 + '</li>';
                flag_col_left_step1 = false;
        }
    }

    $("#list_step1").html(col_left_step1);
}

function LoadStep1Especific(value) {
    var urlroot = getAjaxLinkStep1Images();
    var list = new Array();
	var listTopic2;
    var length = 20;
	var col_right_step1 = ''; 
	var checked_topic=$('#topic'+value).is(':checked');

    $('#step1-next-button').prop('disabled', false);

    if(checked_topic) 
	{
        $('#span-topic'+value).addClass('span-color');
        arrayValue.unshift(value);
	} else {
	     $('#span-topic'+value).removeClass('span-color');
        // split the array
        for (var y = 0, z = 0; y < arrayValue.length; y++) {
            if (arrayValue[y] != value)
                arrayValue[z++] = arrayValue[y];
        }
        arrayValue.length = z;	
	}
	
	/*------------------ Modifying - Add icons as check on topic side panel ----------------------*/	
	for(var i = 0; i < arrayValue.length; i++)
	{
		arrayTopic.filter( function(elem){ 
					var id=elem[0];
					var title = elem[1].substring(0, length);
					var parentid = elem[2];					
					var checked = elem[4];					
					var checkedvalue = (checked) ? 'checked' : '';
					var blobid = elem[5];
		

					if(parentid == arrayValue[i])
					{   var html;
						var imagehtml='./?qa=blob&qa_blobid=' + blobid;
						html = '<label class="label-right-1">';
						html += '    <div class="six-boxes-gs-register" style="background-image:url('+ imagehtml +'); background-size: 100%;">';
						html += '        <input class="test-checkbox" type="checkbox" name ="topic[]" value ="'+id+'" id ="topic'+id+'"';
						html += '        onclick="VerifyCheckbox('+id+', 1)" onchange = "checkingValues(this);" '+ checkedvalue+' checked>';
						html += '        <span class="fa-stack fa-lg fa-yellow">';
						html += '            <!--<i class="fa fa-circle-o fa-stack-2x"></i>-->';
						html += '            <!--<i class="fa fa-star fa-stack-1x"></i>-->';
						html += '        </span>';
						html += '        <div>';
						html += '            <p class="text-square-register-step1 underlined">'+title+'</p>';
						html += '        </div>';
						html += '    </div>';
						html += '</label>';
						
						// this is for sending json to store data.
						if (parentid == value) 
						{
							if(checked_topic) list.unshift(id);
							else  listTopic2=this.DeleteFromArrayInterest(id);
						}			
						col_right_step1 += html;
						return true;
					}
					return false;
					});
		
	}	
	/*--------------------- end ---------------------*/
       
    if (checked_topic && listTopic2 == 0) {
          $('#step1-next-button').prop('disabled', true);
      }
		   
    var listTopic = this.ArrayInterest(list);
    $("#block_step1").html(col_right_step1);

    var $pArr = $('.label-right-1');
    var pArrLen = $pArr.length;
    var pPerDiv = Math.ceil(pArrLen / 2); // Elements by group

    for (var i = 0;i < pArrLen; i+=pPerDiv){
        $pArr.filter(':eq('+i+'),:lt('+(i+pPerDiv)+'):gt('+i+')').wrapAll('<div class="group"/>');
    }
  
	// Set width dinamically = size for icons * elements by divs
    var container_width =  175 * pPerDiv;
    $("#block_step1").css("width", container_width);

    var scrolled=0;

    if (container_width > 610) {
        $('#scrollLeft').fadeIn();
        $('#scrollRight').fadeIn();
    } else {
        $('#scrollLeft').fadeOut();
        $('#scrollRight').fadeOut();
    }

    $('#scrollRight').on('click', function(){
        scrolled=scrolled+container_width;

        $('.container-outer').animate({
            scrollLeft: scrolled
        });
    });

    $('#scrollLeft').on('click', function(){
        scrolled=scrolled-container_width;

        $(".container-outer").animate({
            scrollLeft: scrolled
        });
    });

    $('#scrollLeft').hide();

    $('.container-outer').scroll(function() {
        if ($(this).scrollLeft()>0) {
            $('#scrollLeft').fadeIn();
            if ($(this)[0].scrollWidth <= ($(this).width() + $(this).scrollLeft())) {
                $('#scrollRight').fadeOut();
            } else {
                $('#scrollRight').fadeIn();
            }
        } else {
            $('#scrollLeft').fadeOut();
        }
    });
}


function VerifyCheckbox(value, step) {
    $("#selected_step1").hide();
    $("#selected_step2").hide();
	
    //estableciendo el selector
    var selector = '#'+value;
    //obteniendo la bandera
    var flag = $(selector).val()?true:false;

    if (step == 1) {
        if(flag){
            $("#selected_step1").append(value+'-');
            return true;
        }
    }

    if (step == 2) {
        if(flag) {
            $("#selected_step2").append(value+'-');
            return true;
        }
    }
}

function checkingValues(elem) {
    elementid = $(elem).prop("id");
    checked = $(elem).prop("checked");
    value = $(elem).prop("value");

    for (i=0;i<arrayTopic.length;i++) {
        id = arrayTopic[i][0];

        if (value == id){
            arrayTopic[i][4] = checked;
            var x = arrayTopic[i][4] = checked;

            if (x == false) {
                for (var l = 0, j = 0; l < newList.length; l++) {
                    if (newList[l] != value)
                        newList[j++] = newList[l];
                }
                newList.length = j;
                //console.log(newList);
            } else {
                newList.push(value);
                //console.log(newList);
            }
        }
    }
}

function checkingValuesExp(elem) {
    elementid = $(elem).prop("id");
    checked = $(elem).prop("checked");
    value = $(elem).prop("value");

    for (i=0;i<arrayExpertise.length;i++) {
        id = arrayExpertise[i][0];

        if (value == id){
            arrayExpertise[i][4] = checked;
            var x = arrayExpertise[i][4] = checked;

            if (x == false) {
                for (var l = 0, j = 0; l < newListExpertice.length; l++) {
                    if (newListExpertice[l] != value)
                        newListExpertice[j++] = newListExpertice[l];
                }
                newListExpertice.length = j;
                //console.log(newListExpertice);
            } else {
                newListExpertice.push(value);
                //console.log(newListExpertice);
            }
        }
    }
}

function LoadStep2() {
    border_top_register_step1 = 'border-top-register-step1';
    col_left_step1 = '';
    col_right_step1 = '';
    flag_col_left_step1 = true;

    for (i=0;i<arrayExpertise.length;i++) {
        id = arrayExpertise[i][0];
        title = arrayExpertise[i][1];
        parentid = arrayExpertise[i][2];
        /*type = arrayExpertise[i][3] ;*/
        checked = arrayExpertise[i][4];
		blobid = arrayExpertise[i][5];

        if (parentid == null) {
            border_top_register_step1 = 'border-top-register-step1';

            if (flag_col_left_step1) {
                col_left_step1 = col_left_step1 + '<li class="st-li shadow-topics-register-step1 ' +border_top_register_step1+'">';
                value = id;
            } else
                col_left_step1 = col_left_step1 + '<li class="st-li shadow-topics-register-step1">';
                col_left_step1 = col_left_step1 + '    <label class="step2-label-left">';
                col_left_step1 = col_left_step1 + '        <div class="checkbox">';
                col_left_step1 = col_left_step1 + '            <label>&nbsp;<input type="checkbox" value="'+id+'" name="topicexpertise[]" id="topicexpertise'+id+'" onclick="LoadStep2Especific('+id+');">&nbsp;'+title+'</label>';
                col_left_step1 = col_left_step1 + '        </div>';
                col_left_step1 = col_left_step1 + '        <span class="fa fa-chevron-circle-right fa-lg" id="span-topicexpertise'+id+'"></span>';
                col_left_step1 = col_left_step1 + '    </label>';
                col_left_step1 = col_left_step1 + '</li>';
                flag_col_left_step1 = false;
        }
    }

    $("#list_step2").html(col_left_step1);
}

function LoadStep2Especific(value) {
    var urlroot = getAjaxLinkStep1Images();
    var list = new Array();
    var length = 20;
	var listTopic2;
	var col_right_step2 = '';
	var checked_topic = $('#topicexpertise'+value).is(':checked');
	
    $('#step2-next-button').prop('disabled', false);
    
	if (checked_topic) 
	{
			$('#span-topicexpertise'+value).addClass('span-color');
			arrayValueExpertice.unshift(value);
	} else {
			$('#span-topicexpertise'+value).removeClass('span-color');
			// split the array
			for (var y = 0, z = 0; y < arrayValueExpertice.length; y++) {
				if (arrayValueExpertice[y] != value)
					arrayValueExpertice[z++] = arrayValueExpertice[y];
			}
			arrayValueExpertice.length = z;
	}		
		
	/*------------------ Modifying - Add icons as check on topic side panel ----------------------*/	
	for(var i = 0; i < arrayValueExpertice.length; i++)
	{
		arrayExpertise.filter( function(elem){ 
					var id=elem[0];
					var title = elem[1].substring(0, length);
					var parentid = elem[2];
					var checked = elem[4];
					var checkedvalue = (checked) ? 'checked' : '';
				    var blobid = elem[5];

					if(parentid == arrayValueExpertice[i])
					{   var html;
						var imagehtml =  './?qa=blob&qa_blobid=' + blobid;
						html= '<label class="label-right-2">';
						html+= '    <div class="six-boxes-gs-register" style="background-image:url(' + imagehtml + '); background-size: 100%;">';
						html+= '        <input class="test-checkbox-ext" type="checkbox" name ="topic[]" value ="'+id+'" id ="topic'+id+'"';
						html+= '        onclick="VerifyCheckbox('+id+', 1)" onchange = "checkingValuesExp(this);" '+ checkedvalue+' checked>';
						html+= '        <span class="fa-stack fa-lg fa-yellow">';
						html+= '            <!--<i class="fa fa-circle-o fa-stack-2x"></i>-->';
						html+= '            <!--<i class="fa fa-star fa-stack-1x"></i>-->';
						html+= '        </span>';
						html+= '        <div>';
						html+= '            <p class="text-square-register-step1 underlined">'+title+'</p>';
						html+= '        </div>';
						html+= '    </div>';
						html+= '</label>';
						
						// this is for sending json to store data.
						if (parentid == value) 
						{
							if(checked_topic) list.unshift(id);
							else  listTopic2=this.DeleteFromArrayInterest(id);
						}			
						col_right_step2 += html;
						return true;
					}
					return false;
					});
		
	}	
	/*--------------------- end ---------------------*/   
     if (checked_topic && listTopic2 == 0) {
           $('#step2-next-button').prop('disabled', true);
     }


    var listTopic = this.ArrayExpertice(list);
    $("#block_step2").html(col_right_step2);

    var $pArr = $('.label-right-2');
    var pArrLen = $pArr.length;
    var pPerDiv = Math.ceil(pArrLen / 2 );

    for (var i = 0;i < pArrLen;i+=pPerDiv){
        $pArr.filter(':eq('+i+'),:lt('+(i+pPerDiv)+'):gt('+i+')').wrapAll('<div class="group"/>');
    }

    var container_width = 175 * pPerDiv /*688 * $("#block_step2 .group").length*/;
    $("#block_step2").css("width", container_width);

    var scrolled=0;

    if (container_width > 610) {
        $('#scrollLeftExt').fadeIn();
        $('#scrollRightExt').fadeIn();
    } else {
        $('#scrollLeftExt').fadeOut();
        $('#scrollRightExt').fadeOut();
    }

    $('#scrollRightExt').on('click', function(){
        scrolled=scrolled+container_width;

        $('.container-outer').animate({
            scrollLeft: scrolled
        });
    });

    $('#scrollLeftExt').on('click', function(){
        scrolled=scrolled-container_width;

        $(".container-outer").animate({
            scrollLeft: scrolled
        });
    });

    $('#scrollLeftExt').hide();

    $('.container-outer').scroll(function() {
        if ($(this).scrollLeft()>0) {
            $('#scrollLeftExt').fadeIn();
            if ($(this)[0].scrollWidth <= ($(this).width() + $(this).scrollLeft())) {
                $('#scrollRightExt').fadeOut();
            } else {
                $('#scrollRightExt').fadeIn();
            }
        } else {
            $('#scrollLeftExt').fadeOut();
        }
    });
}

function LoadStep3() {
    LoadUsers()


    $.ajax({
        url: getAjaxLinkStep1(),
        type: "POST",
        global: false,
        data: { flag: "LoadStep3", value: value },
        dataType: "json",

        error: function (data) {
            jq_alert("Error loading data.");
        },

        success: function (data) {
            //antigua escuela
            block = '';
			
            for (i=0;i<arrayUsers.length;i++) {
                showme = arrayUsers[i][8];
            }//for del success

            $("#tofollow").html(showme);
        }// fin success
    });//fin ajax
}

function ArrayInterest(topic) {
    for (var i=0; i<topic.length; i++) {
        id = topic[i];
        newList.push(id);
    }

    return newList;
}

function DeleteFromArrayInterest(value) {
    for (var i = 0, j = 0; i < newList.length; i++) {
        if (newList[i] != value)
            newList[j++] = newList[i];
    }
    return newList.length = j;
}

function ArrayExpertice(topic) {
    for (var i = 0; i<topic.length; i++) {
        id = topic[i];
        newListExpertice.push(id);
    }

    return newListExpertice;
}

function DeleteFromArrayExpertice(value) {
    for (var i = 0, j = 0; i < newListExpertice.length; i++) {
        if (newListExpertice[i] != value)
            newListExpertice[j++] = newListExpertice[i];
    }
    return newListExpertice.length = j;
}

function ArrayFollow(id) {
    arrayListFollows.push(id);

    return arrayListFollows;
}

function DeleteFromArrayFollow(id) {
    for (var i = 0, j = 0; i < arrayListFollows.length; i++) {
        if (arrayListFollows[i] != id)
            arrayListFollows[j++] = arrayListFollows[i];
    }
    return arrayListFollows.length = j;
}

function LoadSaveEvent() {

	$.ajax({
        type: 'post',
        url: getAjaxLinkStep1(),
        data: {
            flag: "saveAllData",
            interests: newList,
            $expertices: newListExpertice,
            follows: arrayListFollows
        },
        error: function(data) {
            console.log(data);
            jq_alert('error sending data');
        },
        success: function(data) {
            //console.log(data);
        }
    });
}

function CheckImg(value, step, elem) {
    var selector = '#topic'+value;
    var selector_id = 'topic'+value;
    var list = [];

    var a = $(elem).find('input:checkbox[name="topic[]"]');

    if (step == 1) {
        if($('#topic'+value).is(':checked')) {
            this.DeleteFromArrayInterest(value);
            $(selector).attr("checked", false);
        } else {
            $(selector).prop("checked", true);
            list.push(value);
            newList.push(list[0]);
        }
    }
}

var setCookie = function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}; 


var getCookie = function(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
};

var setCurrentView=function(view)
{   
   setCookie('current_view',view,1);
   return view;
};

var getCurrentView=function()
{
	var view; 
	if (getCookie('current_view') !== "") 
	{
		view=getCookie('current_view');
	} else {
	   
		view=setCurrentView('grid');
	}
	return view;
};


var toggleMore = function(elm) {
/*	var linkContainer = elm.parentNode.getElementsByTagName('div')[0];
	var contentContainer = elm.parentNode.getElementsByTagName('div')[1];

	linkContainer.style.display =  'none' ;
contentContainer.style.display = (contentContainer.style.display == 'inline' ) ? 'none' : 'inline';*/
}

var toggleMoreAnswers= function(obj,elm) 
{	
	if($(elm).css('display') === 'none') 
	 {
	    $(elm).slideDown('slow')/*.css('display','inline-block')*/;
		$(obj).html("Show Less");
		return false;
	 } else 
	 { 
		$(elm).slideUp('slow');
		$(obj).html($(obj).attr("data-title-slide"));
		return false;
	 }
};


var check_bestanswer = function(qid, aid)
{
	   var params = {};
	   params.answerid = aid
	   params.questionid = qid;
	   params.code= $('input[name="code_button_'+aid+'"]').val();
	   
	   var obj= $('#bestanswer'+ aid)					
	   if(obj.hasClass('best-answer-selected'))  params['a'+aid+'_dounselect']= ''; 
										  else   params['a'+aid+'_doselect']='';
	   
		$.ajax({
			url: getAjaxLinkBestAnswer(),
			data:params,
			type: "POST",     
			dataType: 'json',
			error: function(resp)
			{ 
				console.log(resp);
			},
			success: function(resp){ 
				if (resp.success){				
					var obj= $('#bestanswer'+ aid)
					
					if(obj.hasClass('best-answer-selected')) {
					  obj.removeClass('best-answer-selected')
					  obj.addClass('ds');
					} else 
					{
					  obj.addClass('best-answer-selected')
					  obj.removeClass('ds');						
					}
					
				}  else
				{
					var mess=document.getElementById('errorbox');

					if (!mess) {
						var mess=document.createElement('div');
						mess.id='errorbox';
						mess.className='qa-error';
						mess.innerHTML=resp.reason;
						mess.style.display='none';
					}
					var anchor = 'q' + qid;
					var postelem=document.getElementById(anchor);
					
					if(postelem)
					{
				      var e=postelem.parentNode.insertBefore(mess, postelem);
					  qa_reveal(e);
					}  else
						jq_alert(resp.reason);

			    }
			  }
		 }); 	
	
};

var check_favorite = function(id, tag, qid)
{
	
	var lfavorite = $('#favorite'+ id).attr('data-value');
	var lentitytype = $('#'+ tag + id).attr('data-entity');
	
 	$.ajax({
			url: getAjaxLinkFavorite(),
			data:{ entitytype: lentitytype , entityid: id, favorite: lfavorite},
			type: "POST",     
			dataType: 'json',
			success: function(resp){ 
				if (resp.success){
					var obj= $('#favorite'+ id)
					obj.attr('data-value',resp.value);
					if(obj.hasClass('favorite-selected')) {
					  obj.removeClass('favorite-selected')
					  obj.addClass('ds');
					} else 
					{
					  obj.addClass('favorite-selected')
					  obj.removeClass('ds');						
					}
					
				}  else
				{
					var mess=document.getElementById('errorbox');

					if (!mess) {
						var mess=document.createElement('div');
						mess.id='errorbox';
						mess.className='qa-error';
						mess.innerHTML=resp.reason;
						mess.style.display='none';
					}
					var anchor = 'q' + qid;
					var postelem=document.getElementById(anchor);
					
					if(postelem)
					{
				      var e=postelem.parentNode.insertBefore(mess, postelem);
					  qa_reveal(e);
					}  else
						jq_alert(resp.reason);

			    }
			  }
		 }); 	
	
};


var showPage = function(div,page,pageSize) {	
	var last_page=$(div).attr('data-pageno');	
	var dir_hide = (last_page > page) ? 'right' : 'left';
	var dir_show = (last_page > page) ? 'left'  : 'right';
	
	$(div).hide('slide', {direction: dir_hide}, 600, function (){
		$(div +" .content").hide();
		$(div +" .content").each(function(n) {
			if (n >= pageSize * (page - 1) && n < pageSize * page)
				$(this).show();

		});       
	});

	$(div).show('slide',{direction : dir_show },600);
	$(div).attr('data-pageno',page);
};
    
	
var jq_alert = function(outputMsg, titleMsg = 'Notice', onCloseCallback) {
    if (!outputMsg) return;
    
    var div=$('<div></div>');
    div.html(outputMsg).dialog({
        title: titleMsg,
        resizable: false,
		draggable: false,
        modal: true,
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        },
        close: onCloseCallback
    });
	$('.ui-dialog-titlebar-close').hide();
    if (!titleMsg) div.siblings('.ui-dialog-titlebar').hide();
}



function setdiv_image(objfile, objpreview)
{
	var anyWindow = window.URL || window.webkit.URL;	
	var objectURL = anyWindow.createObjectURL($(objfile)[0].files[0]);
	
	$(objpreview).css( {"background-image" : "url(" + objectURL + ")"} );
	
}


function load_follow_user(start, offset, filter, object = ".follow_userlist")
{
	  $(object).html("Loading...");
	  // Follow User
	  $.ajax({
		  url: getAjaxLinkHtmlFollow(),
		  data:{'start':start, 'offset':offset , 'filter':''},
		  type: 'POST',
		  dataType: 'json',
		  success: function (response) {
		     if(response.success)
			    $(object).html(response.content);
			 else if(response.count > 0)
		        jq_alert(response.reason);
			 else $(object).html("");
			  
		 }
	  });
	
}


function load_follower_user(start, offset, filter, object = ".follower_userlist")
{
	  $(object).html("Loading...");
	  // Follow User
	  $.ajax({
		  url: getAjaxLinkHtmlFollower(),
		  data:{'start':start, 'offset':offset , 'filter':''},
		  type: 'POST',
		  dataType: 'json',
		  success: function (response) {
		     if(response.success)
			    $(object).html(response.content);
			 else if(response.count > 0)
		        jq_alert(response.reason);
			 else $(object).html("");
			  
		 }
	  });
	
}