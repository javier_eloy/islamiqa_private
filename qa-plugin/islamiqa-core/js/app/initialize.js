// Global Functions 

$(document).ready(function () 
{

	var new_question;
	var new_answer;


	
	$('#loginForm').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			emailhandle: {
				validators: {
					notEmpty: {
						message: 'The username is required'
					},
					stringLength: {
						min: 4,
						message: 'The username must be more than 4'
					}
				}
			},
			password: {
				validators: {
					notEmpty: {
						message: 'The password is required'
					}
				}
			}
		}
	});

	$('#frmSignup').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name: {
				validators: {
					notEmpty: {
						message: 'The username is required.'
					},
					stringLength: {
						min: 4,
						max: 10,
						message: 'The username must be more than 4 and less than 10 characters long.'
					},
					regexp: {
						regexp: /^[a-zA-Z0-9_\.]+$/,
						message: 'the username can only consist of alphabetical, number, dot and underscore.'
					},
					blank: {}
				}
			},
			email: {
				validators: {
					notEmpty: {
						message: 'The email is required.'
					},
					emailAddress: {
						message: 'The email is not valid.'
					},
					blank: {}
				}
			},
			password: {
				//enabled: false,
				validators: {
					notEmpty: {
						message: 'The password is required and cannot be empty.'
					},
					stringLength: {
						max: 12,
						min: 6,
						message: 'The password must contain at lest 6 characters and less than 12.'
					}
				}
			},
			rpass: {
				//enabled: false,
				validators: {
					notEmpty: {
						message: 'The conform password is required and cannot be empty.'
					},
					identical: {
						field: 'password',
						message: 'The password and its confirm must be the same.'
					}
				}
			},
			city: {
				validators: {
					notEmpty: {
						message: 'The city is required.'
					}
				}
			},
			country: {
				validators: {
					notEmpty: {
						message: 'The country is required.'
					}
				}
			}
		}
	})
	.on('success.field.fv', function(e, data) {


		e.preventDefault();


		var cnfigPath = window.myConfigPath || "";

	//	console.log(cnfigPath);

		// ?qa=check_mail&email=g.a.rondon@gmail.com

		 if(data.field =="name"){
			 $.get(cnfigPath + "?qa=check_mail&handle="+$("#inputName").val(), function(response){
				 if(response.result>0){
					 data.fv.updateMessage("name", 'blank',response.message)
						 .updateStatus("name", 'INVALID', 'blank');
				 }
			 })
		 }

		// ?qa=check_mail&email=g.a.rondon@gmail.com

		if(data.field =="email"){
			$.get(cnfigPath + "?qa=check_mail&email="+$("#inputEmail").val() , function(response){
				   if(response.result>0){
					   data.fv.updateMessage("email", 'blank',response.message)
						   .updateStatus("email", 'INVALID', 'blank');
				   }
			})
		}
		
		if (data.fv.getInvalidFields().length > 0) {
			data.fv.disableSubmitButtons(true);
		}
	})
	.on('keyup', '[name="pass"]', function(){
		var isEmpty = $(this).val() == '';
		$('#frmSignup')
			.formValidation('enableFieldValidators', 'pass', !isEmpty)
			.formValidation('enableFieldValidators', 'rpass', !isEmpty);
		
		if ($(this).val().length == 1) {
			$('#frmSignup')
				.formValidation('enableFieldValidators', 'pass')
				.formValidation('enableFieldValidators', 'rpass');
		}
	});

	$('#emailForm').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			'email[]': {
				row: '.col-xs-8',
				verbose: false,
				validators: {
					notEmpty: {
						message: 'The email address is required and can\'t be empty.'
					},
					emailAddress: {
						message: 'The input is not a valid email address.'
					},
					stringLength: {
						max: 512,
						message: 'Cannot exceed 512 characters'
					},
					remote: {
						type: 'GET',
						url: 'https://api.mailgun.net/v2/address/validate?callback=?',
						crossDomain: true,
						name: 'address',
						data: {
							api_key: 'pubkey-f2cfe7be19451b7ef9e1e89f34c4ffc3'
						},
						dataType: 'jsonp',
						validKey: 'is_valid',
						message: 'The email is not valid'
					}
				}
			}
		}
	})
	.on('click', '.addButton', function() {
		var $template = $('#emailTemplate'),
			$clone = $template
				.clone()
				.removeClass('hide')
				.removeAttr('id')
				.insertBefore($template);

		$('#emailForm').formValidation('addField', $clone.find('[name="email[]"]'))
	})
	.on('click', '.removeButton', function() {
		var $row = $(this).closest('.form-group');

		$('#emailForm').formValidation('removeField', $row.find('[name="email[]"]'));

		$row.remove();
	})
	.on('success.validator.fv', function(e, data) {
			if (data.field === 'email[]' && data.validator === 'remote') {
				var response = data.result;
				if (response.did_you_mean) {
					data.element
						.data('fv.messages')
						.find('[data-fv-validator="remote"][data-fv-for="email[]"]')
						.html('Did you mean ' + response.did_you_mean + '?')
						.show();
				}
			}
		})
	.on('err.validator.fv', function(e, data) {
		if (data.field === 'email[]' && data.validator === 'remote') {
			var response = data.result;
			data.element
				.data('fv.messages')
				.find('[data-fv-validator="remote"][data-fv-for="email[]"]')
				.html(response.did_you_mean
					? 'Did you mean ' + response.did_you_mean + '?'
					: 'The email is not valid')
				.show();
		}
	})
	.on('success.form.fv', function(e) {
		e.preventDefault();

		var  url = String(getAjaxCustomPageLink())+"fun_jq.php";
		var emailArray = new Array();

		function cleanArray(actual) {
			var newArray = new Array();
			for (var i = 0; i < actual.length; i++) {
				if (actual[i]) {
					newArray.push(actual[i]);
				}
			}
			return newArray;
		}

		$("input[name='email\\[\\]']").each(function() {
			emailArray.push($(this).val());
		});

		$.ajax({
			url: url,
			type: 'post',
			data: {
				flag: "send_friends_mails",
				emails: emailArray
			},
			error: function(data) {
				jq_alert('error sending data');
			},
			success: function(data) {
			//	console.log(data);
			}
		});

		//console.log(cleanArray(emailArray));
	});
	
	var scrolled=0;
	
	$("#scrollDown").on("click", function(){
		scrolled=scrolled+900;

		$("#list_step1").animate({
			scrollTop: scrolled
		});
	});

	$("#scrollUp").on("click", function(){
		scrolled=scrolled-900;

		$("#list_step1").animate({
			scrollTop: scrolled
		});
	});

	$('#scrollUp').hide();

	$("#list_step1").scroll(function() {
		if ($(this).scrollTop()>0) {
			$('#scrollUp').fadeIn();

			if ($(this)[0].scrollHeight <= ($(this).height() + $(this).scrollTop())) {
				$('#scrollDown').fadeOut();
			} else {
				$('#scrollDown').fadeIn();
			}
		} else {
			$('#scrollUp').fadeOut();
		}
	});

	$("#scrollDownExt").on("click", function(){
		scrolled=scrolled+900;

		$("#list_step2").animate({
			scrollTop: scrolled
		});
	});

	$("#scrollUpExt").on("click", function(){
		scrolled=scrolled-900;

		$("#list_step2").animate({
			scrollTop: scrolled
		});
	});

	$('#scrollUpExt').hide();

	$("#list_step2").scroll(function() {
		if ($(this).scrollTop()>0) {
			$('#scrollUpExt').fadeIn();

			if ($(this)[0].scrollHeight <= ($(this).height() + $(this).scrollTop())) {
				$('#scrollDownExt').fadeOut();
			} else {
				$('#scrollDownExt').fadeIn();
			}
		} else {
			$('#scrollUpExt').fadeOut();
		}
	});

	$('#scrollLeft').hide();
	$('#scrollRight').hide();
	$('#scrollLeftExt').hide();
	$('#scrollRightExt').hide();
	
	
	var owl = $("#owl-demo");
	owl.owlCarousel({
		items : 1, //10 items above 1000px browser width
		itemsDesktop : [1000,1], //1 items between 1000px and 901px
		itemsDesktopSmall : [900,1], //1  betweem 900px and 601px
		itemsTablet: [600,1], //1 items between 600 and 0
		itemsMobile : false ,// itemsMobile disabled - inherit from itemsTablet option,
		mouseDrag : false,
		touchDrag : false,
		rewindNav:false,
		lazyLoad:true
	});
	// Custom Navigation Events
	$(".nextLogin").click(function(){
		owl.trigger('owl.next');
	})
	
	$(".prevLogin").click(function(){
		owl.trigger('owl.prev');
	})
	
	new WOW().init();
	
	
	// Login events
    $('.signup_login ul.dropdown-menu').on('click', '.nav-tabs a', function () {
        $(this).closest('.dropdown').addClass('dontClose');
    });

    $('#signUpDropdown').on('hide.bs.dropdown', function (e) {
        if ($(this).hasClass('dontClose')) {
            e.preventDefault();
        }
        $(this).removeClass('dontClose');
    });

	// File manipulation into ask new question  
    $(document).on('click','#get_file1', function () {
        $('#my_file1').click();
    })

    $(document).on('change','input#my_file1',function (e) {
        $('#customfileupload1').html($(this).val());
    });

	$(document).on('click','#get_file2', function () {

        $('#new_gallery').dialog({
		    dialogClass:"new-gallery-css",	
			modal:true,
			title:"Image Gallery (click on image to select)",
			width:760,
			height:400,
			resizable: false,
			draggable: false,
			buttons: {
			   Cancel: function(){
				    $(this).dialog("close");
				}
			},
		   open: function (event, ui) {
			     $(".new-gallery-css button.ui-dialog-titlebar-close").html("<li class='fa fa-close' aria-hidden='true'></li>"); 
				 
				 	// Request info to server
					$.ajax({
						url: getAjaxLinkGallery(),
						type: "POST",
						dataType: "html",
						success: function(resp) {
						  $('#content_gallery').html(resp);
						}
				   });
			}			
	    });
		
     });
 /*   $(document).on('click','#get_file2', function () {
        $('#my_file2').click();
    })

    $(document).on('change','input#my_file2', function (e) {
        $('#customfileupload2').html($(this).val());
    });
 */
    //- End --- File Manipulation


    $(function() {
      var cache = {};
      $( "#title" ).autocomplete({

        minLength: 3,
        maxShowItems: 7,
        dataType: "json",
        autoFocus: true,

        source: function( request, response ) {
          var term = request.term;
          if ( term in cache ) {
            response( cache[ term ] );
            return;
          }
          $.getJSON( getAjaxAutocomplete(), request, function( data, status, xhr )
          {
            cache[ term ] = data;
            var results = $.ui.autocomplete.filter(data, request.term);
            response(results.slice(0, 5));
//            response( data );
          });
        },

        open: function() {
          $("li.ui-menu-item:last").append(function() {
            $(this).html('<div class="search_input"><i class="fa fa-search fa-lg"></i><span class="search">&nbspSearch: </span><span class="bold"><a href="' + getBaseUrl() + '?qa=search&q=' + $("input#title").val() + '">' + $("input#title").val() + '</a></span></div>');
            //$(this).css('font-weight','900', 'vertical-align','middle!important');

            $("li.ui-menu-item:last").mouseover(function() {
              $(this).css('background', '#e3e3e3');
            });
            $("li.ui-menu-item:last").mouseout(function() {
              $(this).css('background', 'white');
            });
          });
          return false;
        },

        select: function( event, ui ) {

            //$(this).val(ui.item.label);
            location.href = getBaseUrl() + "?qa=" + ui.item.value + "/" + ui.item.label;
            return false;
        }
    });
  });
   
	// == New Question =================================  
	new_question = $("#new_question").dialog({
		 dialogClass: 'new_question',
		 autoOpen:false,
		 modal:true,
		 width:710,
		 height:640,
		 resizable:false,
		 open: function(event, ui) {			
			var title = $("#title").val();

			// Backup content to clean
			back_content=$('#new_question').html();

			// Request info to server
			$.ajax({
				url: getAjaxLinkTags(),
				data: "title="+title,
				type: "POST",
				dataType: "json",
				success: function(resp) {
				   if (title) $("input#newq_title").val(title).attr('disabled', 'disabled');
				},
				error: function( req, status, err ) {
					console.log( 'something went wrong', status, err );
					jq_alert("ERROR: Trying post a new question");
				}
           })
		},
		close: function(event) {
			$('#new_question').html(back_content);
			back_content=null;
		}
	});
	
	// Call jquery ui dialog box for New Question
    $("#asknew").on("click", function() {
		new_question.dialog("open");
	});

	// Event to close window on JQuery UI Dialog
    $(document).on("click","span.iconclose", function() {
		$(this).parent().dialog("close");
    });

	// Event to activate title edit
    $("div.newq_link > span.fa-pencil").on("click", function() {
        $("input#newq_title").prop('disabled', false);
	});
	
	
    //== New answer box	=======================================
	$("body").on('click','a.answerjs', function (event)  {
			var qa=getURLParameter($(this).attr('href'),'qa');
			var title=qa.split("/")[1].replace(/-/g," ");
			var idtitle=qa.split("/")[0];
			
			new_answer=$("div#new_answer").dialog({ 
				dialogClass: 'new_answer',
				autoOpen:false,
				modal:true,
				width:650,
				height:650,
				resizable:false,
				closeOnEscape: true,
				open: function(event, ui) {	
				
				     /* Attach CKEditor */
					var instance = CKEDITOR.instances['qa_editor'];
					if(instance) { 
						 instance.destroy(true); instance=null;
					}
					
					var edit_ans=CKEDITOR.replace('qa_editor', {defaultLanguage: 'en',language: '', width: '600px', startupFocus: true});
					edit_ans.on('key', function (evt) {
						if(evt.data.keyCode === 27) {
							$("div#new_answer").dialog('close');
							evt.cancel();	
							return false;
						}
					});		
					/* End editor */
					
					$("div#q_answer").html(title);
					$("#q_id").val(idtitle);
					
				  },
				  close: function(event) {
				  	$("#setanswer01").attr("checked",false);
				  	$("#setanswer02").attr("checked",false);
				  }
				});
			
			new_answer.dialog('open');
			event.preventDefault();
			return false;
			
	});
 
 
	//$(function(){
		// login drop down button close 
		$("#noClose").click(function(e) {
			if (e.target.nodeName !== 'DIV' ) e.stopPropagation();
		});

//});

	
	
}); //******** End document.ready 



