$(document).ready(function () 
{
	
	// Hide or Show box
	$('.fa-section').click(function() {
		panel=$(this).parent().next();
		
		
		if($(this).hasClass('fa-angle-down')) 
		{ 
			$(this).removeClass('fa-angle-down').addClass('fa-angle-up');
			panel.show();
		} else
		{	
			$(this).removeClass('fa-angle-up').addClass('fa-angle-down');
			panel.hide();
		}
		
	});
	
		// Hide or Show box
	$('.line-page label.evtlist').click(function() {
		panel=$(this).parent().next();


		if(panel.hasClass('evtsubitem')) {
			if($(this).text() == '+') 
			{ 
				$(this).text('-');
				panel.show();
			} else {	
				$(this).text('+');
				panel.hide();
			}
		} else
		  jq_alert('No data to show');
	});
	
    // Interesting 
	$("#interest-pagin span.prev, #interest-pagin span.next").click(function() {

		var span_page = $('#interest-pagin span.current');
		var current_page = parseInt(span_page.text());
		var max_page= parseInt(span_page.attr('data-maxpage'));
		
		if($(this).hasClass('prev')) {
			page = (current_page > 1) ? (current_page - 1) : 1;
		} else if($(this).hasClass('next')){
			page = (current_page < max_page) ? (current_page + 1) : current_page;
		}		
		
		if(page != current_page)
		{
			showPage('.body-interest', page, 2);		
			span_page.text(page);
		}
	});
	
	
	// Following
	$("#follow-pagin span.prev, #follow-pagin span.next").click(function() {

		var span_page = $('#follow-pagin span.current');
		var current_page = parseInt(span_page.text());
		var max_page= parseInt(span_page.attr('data-maxpage'));

		if($(this).hasClass('prev')) {
			page = (current_page > 1) ? (current_page - 1) : 1;
		} else if($(this).hasClass('next')) {
			page = (current_page < max_page) ? (current_page + 1) : current_page;
		}

		if(page != current_page)
		{
			showPage('.body-follow', page, 4);		
			span_page.text(page);
		}
	});

	// follower
	$("#follower-pagin span.prev, #follower-pagin span.next").click(function() {

		var span_page = $('#follower-pagin span.current');
		var current_page = parseInt(span_page.text());
		var max_page= parseInt(span_page.attr('data-maxpage'));
		
		if($(this).hasClass('prev')) {
			page = (current_page > 1) ? (current_page - 1) : 1;
		} else if($(this).hasClass('next')) {
			page = (current_page < max_page) ? (current_page + 1) : current_page;
		}
		
		if(page != current_page)
		{
			showPage('.body-follower', page, 4);		
			span_page.text(page);
		}
		
	});
	
	
	$("i#edit-myinfo").bind("click", function () {
	     // Prepare tabs 	
		 $('#myinfo-edit').tabs({
			 show: {effect: "slide" , duration: 200},
			 hide: {effect: "slide", duration: 200},
		 });

		 // Open Dialog
	     $('#myinfo-edit-dialog').dialog({
		     dialogClass:"myinfo-edit-css",		 
			 width: 760,
			 height:500,
			 resizable: false,
			 modal: true,
			 draggable:false,
			 open: function (event, ui) {
			     $(".myinfo-edit-css button.ui-dialog-titlebar-close").html("<li class='fa fa-close' aria-hidden='true'></li>"); 

				 // Follow User
				  load_follow_user(0, 9, "");
				  
				  // Follower User
				  load_follower_user(0, 9, "");
			}
		});
	
	});
	
	$('.loadmore').bind("click", function () {
	    var ajaxLoad=$(this).attr('data-origin');
		var limit=$(this).attr('data-limit') | 9 ;
		
		
		limit = limit + 9;
		
		switch(ajaxLoad){
		   case 'follow':
		            load_follow_user(0,limit,"");
					break;
		   case 'follower':
					load_follower_user(0,limit,"");
					break;
	     }
		 
		 $(this).attr('data-limit',limit);
		
	});
	
	
	$('button.unfollow').bind("click", function() {
		 var uid=$(this).attr("data-uid");
		 var isfollower=$(this).attr('data-status');
		 
		 alert(uid);
		
	});
	
	 
	// Paging to my info widget
	showPage('.body-interest',1,2);
	showPage('.body-follow',1,4);
	showPage('.body-follower',1,4);
	
});