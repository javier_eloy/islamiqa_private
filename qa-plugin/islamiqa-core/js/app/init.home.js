   	//------------------------	Hide all sections and switch between sections clicking arrows up/down ------
	//------------------------	This was made by Julio A. Velasquez at 20160415			--------------------
	// -----------------------	Modified By Javier Hernandez to reduce spaggetti code 	--------------------

    //for change css style class in landing page filter links: This Week, This Month , This Year
$(document).ready(function () 
{
	$("#view_grid")
		.ajaxStart(function() { $(this).addClass("loading");  
		})
		.ajaxStop(function() { $(this).removeClass("loading");     
		});
		

	// Check filter view
	$("ul.filter").on("click", "li", function() {
		$("ul.type_filter").find(".active").removeClass("active");
	});

	// Events on Grid View to show answers
	$("body").on("mouseover", ".inner", function(e) {
		$(this).find(".ans").show();
	});
								  
	$("body").on("mouseout", ".inner", function(e) {
		$(this).find(".ans").hide();
	});

	// View Icons change Grid/List
	 $("a.view_icons > span#view-grid").click(function()
	{                                    	
			current_view=setCurrentView('grid');
			$("span#view-grid").addClass("red-active");
			$("span#view-list").removeClass("red-active");
			$("a.active_section:visible").click();

	});

	$("a.view_icons > span#view-list").click(function()
	 {                                    			
			current_view=setCurrentView('list');
			$("span#view-grid").removeClass("red-active");
			$("span#view-list").addClass("red-active");
			$("a.active_section:visible").click();
		
	});

	
	// Events for arrows up/down on home section Grid/List View
	$(".most_popular_fad_up").click(function()
	{
		$("#most_popular_start").hide();
		$("div.qa_gallery.first.unansquation").show();
		$("div.qa_gallery.first.most_popular").hide();
		
		show_data('unans', current_name, 1, 'up', 'unansquation');
	});

	$(".most_popular_fad_down").click(function() 
	{
		$("#most_popular_start").hide();
		$("div.qa_gallery.first.recently").show();
		$("div.qa_gallery.first.most_popular").hide();			
	
		show_data('recent', current_name, 1, 'up', 'recently');
	});
	
	$(".recently_fad_up").click(function()
	{
	  $("#recently_start").hide();
	  $("div.qa_gallery.first.most_popular").show();
	  $("div.qa_gallery.first.recently").hide();

	  show_data('most', current_name, 1, 'up', 'most_popular');
	});

	$(".recently_fad_down").click(function()
	{
		$("#recently_start").hide();
		$("div.qa_gallery.first.allquation").show();
		$("div.qa_gallery.first.recently").hide();

		show_data('allque', current_name, 1, 'up', 'allquation');
	});
	
	$(".allque_fad_up").click(function()
	{
		$("#allquation_start").hide();
		$("div.qa_gallery.first.recently").show();
		$("div.qa_gallery.first.allquation").hide();

		show_data('recent', current_name, 1, 'up', 'recently');
	});

	$(".allque_fad_down").click(function()
	{
		$("#allquation_start").hide();
		$("div.qa_gallery.first.unansquation").show();
		$("div.qa_gallery.first.allquation").hide();

		show_data('unans', current_name, 1, 'up', 'unansquation');
	});
	
	$(".unansque_fad_up").click(function()
	{
		$("#unansquation_start").hide();
		$("div.qa_gallery.first.allquation").show();
		$("div.qa_gallery.first.unansquation").hide();

		show_data('allque', current_name, 1, 'up', 'allquation');
	});

	$(".unansque_fad_down").click(function()
	{
		 $("#unansquation_start").hide();
		 $("div.qa_gallery.first.unansquation").hide();
		 $("div.qa_gallery.first.most_popular").show();
		 show_data('most', current_name, 1, 'up', 'most_popular');
	});
	
	//--- Initialize Panels and Load initial data on home sections --------------------	

/*	$("div.qa_gallery.first.recently").hide();
	$("#recently_start").hide();

	$("div.qa_gallery.first.allquation").hide();
	$("#allquation_start").hide();

												
	$("div.qa_gallery.first.unansquation").hide();
$("#unansquation_start").hide();*/
   
   $("div.qa_gallery.first.most_popular").show();
	$("#most_popular_start").show();
   
	$(".inner .ans").hide();
	
	current_view=getCurrentView();
	$("span#view-" + current_view).addClass("red-active");
	show_data('most', 'all', 1, 'up', 'most_popular');

  
});


/*----------------------------------- Function -------------------------------------------*/

/** New Code By Javier Hernandez ***/
function show_data(type, name, page, effect, object_show)
{
	  
  var div_start = "#" + object_show + "_start";
  var div_gallery = "div.qa_gallery.first." + object_show;
  
  $("ul.type_filter .active").removeClass("active");
  if (name != 'all') $("li#" + name).addClass("active");
  
  
  /* Global variable to identify object to show */
  current_object = object_show; 
  
  $.ajax({
	  url: getAjaxLinkHeadFilters(),
	  data:{'view':current_view, 'type':type, 'name': name, 'page': page},
	  type: 'POST',
	  dataType: 'html',
	  success: function (response) {
			 if( current_view == 'grid')
			 {
				 $(div_start).html(response);
				 $(div_start).hide();
				 $(div_start).toggle("slide", {direction:effect}, 1000);
			 } 
			  else if( current_view == 'list')
			 {
				 if(page == 1) {
					 $(div_start).html(response);
					 $(div_start).show();
				 } else
					 $("#row_list_new").replaceWith(response);
				 
			 }
	   }
	 })
	
	
}

function next_page(type,name,page)
{
	show_data(type,name,page,'right',current_object);
	
}

function prev_page(type,name,page)
{
	show_data(type,name,page,'left',current_object);
	
}

function more_page(type, name,page)
{
	if(current_view == 'list') show_data(type,name,page,null,current_object);
}

function change_section(type, name)
{
	current_name=name;
	show_data(type,current_name,1,'up',current_object);
}


/** End ***/