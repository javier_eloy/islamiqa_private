/*
 * Scrollable jQuery UI Autocomplete
 * https://github.com/anseki/jquery-ui-autocomplete-scroll
 *
 * Copyright (c) 2016 anseki
 * Licensed under the MIT license.
 */
 /*
 * Updated by Julio A. Velasquez Basabe
 * Date: 04/07/2016
 * Time: 16:44
 */

;(function($, undefined) {
'use strict';

$.widget('ui.autocomplete', $.ui.autocomplete, {
  _resizeMenu: function() {
    var ul, lis, ulW, barW;
    if (isNaN(this.options.maxShowItems)) { return; }
    ul = this.menu.element
      .scrollLeft(0).scrollTop(0) // Reset scroll position
      .css({overflowX: '', overflowY: '', width: '', maxHeight: '', maxWidth: '', textAlign: ''}); // Restore
    lis = ul.children('li').css('whiteSpace', 'normal');

    if (lis.length > this.options.maxShowItems) {
      ulW = ul.prop('clientWidth');
      ul.css({overflowX: 'hidden', overflowY: 'auto', maxWidth: '600px!important', textAlign: 'justify', 
        maxHeight: lis.eq(0).outerHeight() * this.options.maxShowItems + 1}); // 1px for Firefox
      barW = ulW - ul.prop('clientWidth');
      ul.width('+=' + barW);
    }

    // Original code from jquery.ui.autocomplete.js _resizeMenu()
    ul.outerWidth(Math.max(
      ul.outerWidth() + 1,
      this.element.outerWidth()
    ));
  }
});

})(jQuery);