<?php

//Make sure you need to register your plguin using metadata.json (for q2a 1.7+) or plugin metadata comment (for q2a below 1.7)


define ('ISLAMIQA_INCLUDE','includes/');
 
/*-------------------- New Format --------------------------*/

/**
 * Defines the base directory of the theme 
 */
if (!defined('ISLAMIQA_PLUGIN_BASE_DIR')) {
	define('ISLAMIQA_PLUGIN_BASE_DIR', dirname(__FILE__));
}

/**
 * Define the directory name of the theme directory to use in HTML format
 */
if (!defined('ISLAMIQA_PLUGIN_BASE_DIR_NAME')) {
	define('ISLAMIQA_PLUGIN_BASE_DIR_NAME', 'qa-plugin/'.basename(dirname(__FILE__)));
}

/**
 * Define constants
 */
 
if (!defined('ISLAMIQA_PLUGIN_NAME')) {
  define('ISLAMIQA_PLUGIN_NAME','plugin_islamiqa_custom_pages'); // Name plugin on admin page
}

if (!defined('QA_POST_CATEGORIES')) {
  define('QA_POST_CATEGORIES','qa_q_categories'); // Name plugin on admin page
}

if (!defined('QA_TOPIC_TAG')) {
  define('QA_TOPIC_TAG','TOPIC_TAG'); // Name use to add title to tagmeta
}

if (!defined('QA_CATEGORY_IMG')) {
	define('QA_CATEGORY_IMG','CATEGORY_IMG'); // Name use to add title to tagmeta
}

if (!defined('QA_GALLERY_IMG')) {
	define('QA_GALLERY_IMG','GALLERY_IMG'); // Name use to add title to tagmeta
}

if (!defined('QA_WIDGET_SEARCHBOX')) {
	define('QA_WIDGET_SEARCHBOX','Islamiqa Search Box'); // Name plugin on admin page
}

if (!defined('QA_WIDGET_ACCOUNT')) {
	define('QA_WIDGET_ACCOUNT','Islamiqa Account Status'); // Name plugin on admin page
}

if (!defined('QA_WIDGET_MYINFO')) {
	define('QA_WIDGET_MYINFO','Islamiqa My Info'); // Name plugin on admin page
}

if (!defined('QA_WORKS_AT')) {
	define('QA_WORKS_AT','work-at'); // Name plugin on admin page
}

if(!defined('QA_FAVORITE_ANSWER'))
{
	define('QA_FAVORITE_ANSWER','A');	 // Answer favorite
}

if(!defined('QA_FAVORITE_CATEGORY'))
{
	define('QA_FAVORITE_CATEGORY','C');	 // Category or interesting favorite
}

if(!defined('QA_FAVORITE_KNOWLEDGE'))
{
	define('QA_FAVORITE_KNOWLEDGE','K');	// knowledge favorite
}

if(!defined('QA_FAVORITE_USER'))
{
	define('QA_FAVORITE_USER','U');	 //  User Favorite
}


/**
 * External includes 
 */

require ISLAMIQA_PLUGIN_BASE_DIR."/includes/libraries.php";
require_once QA_INCLUDE_DIR.'app/limits.php';

/**
 * Layers
 */
 
qa_register_plugin_layer(ISLAMIQA_INCLUDE.'layer-main.php', 'layer main');	
qa_register_plugin_layer(ISLAMIQA_INCLUDE.'layer-categories.php', 'layer admin categories');	
qa_register_plugin_layer(ISLAMIQA_INCLUDE.'layer-question.php', 'layer questions');	
//qa_register_plugin_layer(ISLAMIQA_INCLUDE.'layer-account.php', 'layer account');	


/**
 * Modules
 */
qa_register_plugin_module('module', ISLAMIQA_INCLUDE.'module-admin.php', 'module_admin', 'module_admin');


/**
  * Pages 
  */
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-home.php','class_home_page','home_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-press.php','class_press','press_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-privacy.php', 'class_privacy', 'privacy_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-feedback.php', 'class_feedback_page', 'feedback_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-about.php', 'class_about_page', 'about_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-volunteering.php', 'class_volunteering', 'volunteering_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-blog.php', 'class_blog', 'blog_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-terms.php', 'class_terms_page', 'terms_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-faq.php', 'class_faq_page', 'faq_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-step1.php', 'class_step1', 'step1_page' );
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-questions.php', 'class_questions_page', 'questions_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-allquestion.php', 'class_allquestion_page', 'allquestion_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-login.php', 'class_login', 'login_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-topics.php', 'class_topics', 'login_topics');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-gallery.php', 'class_gallery', 'login_gallery');

qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-ajax-application.php', 'class_ajax', 'ajax_page');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-ajax-html.php', 'class_ajax_html', 'ajax_page_html');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-ajax-header.php', 'class_ajax_header', 'ajax_page_header');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-ajax-step1.php', 'class_ajax_step1', 'ajax_page_step1');
qa_register_plugin_module('page', ISLAMIQA_INCLUDE.'page-ajax-allquestion.php', 'class_ajax_allquestion', 'ajax_page_allquestion');
/**
  * Overrides 
  */
qa_register_plugin_overrides(ISLAMIQA_INCLUDE.'override.php');


/**
 * Widgets
 */
 // IMPORTANT: If change human readable verify on rest application by qa_load_module

 qa_register_plugin_module('widget', ISLAMIQA_INCLUDE.'widget-searchbox.php', 'class_widget_searchbox', QA_WIDGET_SEARCHBOX );
 qa_register_plugin_module('widget', ISLAMIQA_INCLUDE.'widget-account.php', 'class_widget_accountsetup', QA_WIDGET_ACCOUNT );
 qa_register_plugin_module('widget', ISLAMIQA_INCLUDE.'widget-myinfo.php', 'class_widget_myinfo', QA_WIDGET_MYINFO );
 
 /**
  * Login
  */
  
 qa_register_plugin_module('login', ISLAMIQA_INCLUDE.'login-main.php', 'class_login_main', 'hybrid_login_main');

