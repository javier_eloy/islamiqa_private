<?php

require_once QA_INCLUDE_DIR.'qa-db-selects.php';
require_once QA_INCLUDE_DIR.'qa-app-upload.php';
require_once QA_INCLUDE_DIR.'qa-db-blobs.php';
require_once QA_INCLUDE_DIR.'qa-db-metas.php';

require_once ISLAMIQA_PLUGIN_BASE_DIR.'/includes/data-core.php';

class class_gallery
{ 


		var $directory;
		var $urltoroot;
		
		function load_module($directory, $urltoroot)
		{
			$this->directory=$directory;
			$this->urltoroot=$urltoroot;
		}
		
		function suggest_requests() // for display in admin interface
		{	
			return array(
				array(
					'title' => 'Gallery Admin',
					'request' => 'admin/gallery',
					'nav' => null, // 'M'=main, 'F'=footer, 'B'=before main, 'O'=opposite main, null=none
				),
			);
		}
		
		function match_request($request)
		{
			if ($request=='admin/gallery')
				return true;

			return false;
		}

		function process_request($request) 
		{

		    require $this->directory ."includes/templates/gallery.php";
			$qa_content=qa_content_prepare();

			$saved = false;
			$error = false;			
			$id = '';
			$content = '';
			$img_content = '';
			$description = '';
			$label = 'Save Image Gallery';
			$action = 'save_gallery';			
			
			//--- Set values to modify
			if(qa_clicked('modify_gallery'))
			{	
		
				$description=qa_post_text('gallery');
				$id=qa_post_text('id');				
				$content=qa_post_text('content');
				$img_content = $this->set_htmlblob(qa_get_blob_url($content,true));

			} 
				
			//---  Action 
			if (qa_clicked('save_gallery')) 
			{	
		
			 $description=qa_post_text('gallery_desc');
			 $content=qa_post_text('content');
			 $id=qa_post_text('id');			 
	
			 if (empty($description)) 
				{
					$return['reason'] = "Empty gallery image description not allowed";
					$error=true;
				} 				
				  else 	
				{ 
			
					$return=$this->insert_gallery($content,$id);				
					$saved=$return['success'];  
					$error=!$saved;
					if(!$error) // Saved !
					{
						$img_content=$this->set_htmlblob(qa_get_blob_url($return['blobid'],true));
						$id = $return['id'];
					}
 				}
			} 
			
			if( $id != '') 
				  $label = 'Update Image Gallery'; 
			
			/*else if(qa_clicked('update_topic'))
			{
			    $return=$this->update_topic();
				$saved=$return['success'];  
				$error=!$saved;
			}*/
			
			//--- Set form
			$qa_content['title']="Image Gallery";			
			$qa_content['navigation']['sub']=qa_admin_sub_navigation();			
			$qa_content['form'] = array(
				'ok' => $saved ? 'Image Gallery Saved' : null,
				'tags' => 'METHOD = "POST" ACTION="'.qa_self_html().'" enctype="multipart/form-data"',
				'style' => 'tall',
				'fields' =>  array (
						  'description' => array( 'label' => "Title",
										   'type' => "text",
										   'tags' => "NAME='gallery_desc' ID='gallery_desc' ",
										   'value' => $description ),
						   'image' => array(
											'label' => "{$img_content} Image File",
											'type' => "file",
											'tags'=> "NAME='topic_image' ID='topic_image' type='file' onchange='setdiv_image(this,\"#preview\");'  "),
						   'preview' =>  array(
											'label' => " <div id='preview' class='box-gallery topic-image' ></div>",
											'tags' => " style='display:none' "
											
										)	
						   ),
				'hidden' => array(
						  'id' => $id,
						  'content' => $content,
					),	
				'buttons' => array (
				   array(
					'tags' => "NAME='{$action}'",
					'label' => "{$label}",
				   ),
				   array(
					'cancel' => "NAME=cancel",
					'label' => "Cancel",
				   )
				),			
			);		
			
			if($error) $qa_content['form']['fields']['image']['error'] = $return["reason"];
			
		    $list_gallery = (new Renderer_gallery())->render((new data_core)->qa_get_list_gallery());			
			$qa_content['customtopic'] = $list_gallery;
			
			return $qa_content;
		}
		
		private function set_htmlblob($url)
		{
			
			return "<div class='box-gallery topic-image' style='background-image:url(./?qa=image&qa_blobid={$url})'></div><br>";
		}
				
		private function insert_gallery($oldblobid, $idtag)
		/**
		*
		*/
		{
			$tag=str_replace(" ","_", qa_post_text('gallery_desc'));			
			
			$file=reset($_FILES);
			$return['success'] = false;
			
			$file_error = $file['error'];
			// Note if $_FILES['file']['error'] === 1 then upload_max_filesize has been exceeded
			switch($file_error)		
			{
			   case 1: 
					   $return['reason'] = 'Max size exceded';
					   return $return;	
			   case 4:
						$return['reason'] = 'No Image File selected';
						return $return;			   
			}

			
			// Check if duplicate topic
			$id=qa_db_word_mapto_ids(array($tag));
			if(count($id) > 0 ) {
				$tagmeta=qa_db_tagmeta_get($id[$tag], QA_GALLERY_IMG);				
				
				if (!empty($tagmeta) && ($idtag != $id[$tag]) )
				{
					$return['reason'] = 'Gallery Image Description duplicate';				
					return $return;
				}
			}
			
			$result=qa_upload_file($file['tmp_name'], $file['name'], null, true);
			if ($result['error'] == '' )
			{
			  $blobid = $result['blobid'];
			  // Find id inserted into words table
			  $id=qa_db_word_mapto_ids_add(array($tag));

			  // Insert tag with id into tagmetas
			  $newid=$id[$tag];
			  qa_db_tagmeta_set($newid, QA_GALLERY_IMG, $blobid);	
			  
			  // Remove old information after update
			  if(!empty($idtag) && $newid != $idtag) qa_db_tagmeta_clear($idtag, QA_GALLERY_IMG);
			  if(!empty($oldblobid)) qa_delete_blob($oldblobid);
				
			  // Return value
			  $return['id'] = $id[$tag];
			  $return['blobid'] = $blobid;
			  $return['success'] = true;
			} else 
			  $return['reason'] = $result['error'];
								
			return $return;
		  	
		}
		
}
