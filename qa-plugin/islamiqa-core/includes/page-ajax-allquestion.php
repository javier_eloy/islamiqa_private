<?php
/*
 * by Javier Hernandez (All code changed)
 * Date: 09/09/2016
 */


require_once QA_INCLUDE_DIR .'qa-base.php';
require_once QA_INCLUDE_DIR . 'qa-db.php';
require_once ISLAMIQA_PLUGIN_BASE_DIR.'/includes/libraries.php';


class class_ajax_allquestion
{
   
	private $directory;
    private $urltoroot;
	
    public function load_module($directory, $urltoroot)
    {
        $this->directory=$directory;
		$this->urltoroot=$urltoroot;
    }


    public function match_request($request)
    {

	   if ($request == "qajax-allquestion")
		   return true;
	 
	  return false;
    }

	
    public function process_request($request)
	/**
	*  Route to action
	*/
	{
		$page = $_REQUEST['page'];
		$order = $_REQUEST['order'];
		$page_size=5;
		$page_size_answer=2;
		$this->html_format($page, $page_size, $page_size_answer,$order); 
		
	}
	
	/*-------------------------------------------------------------------------------------------------------------*/
	/*--------------------------------------------- PRIVATE FUMCTIONS ---------------------------------------------*/
	/*-------------------------------------------------------------------------------------------------------------*/
	

	private function html_format($page, $page_size,$page_size_answer,$order)
	{
		
		
		$questions=$this->get_array_questions($page, $page_size,true, $order);
		$answers = $this->get_array_answers($questions);
		$morequestions=count($questions);
		$more = ($morequestions > $page_size) ? $page + 1: 0;
		
		$html = $this->format_html_question($more >0, $questions, $answers, $page,$page_size,$more);

		print $html;
	}
	
	private function format_html_answer()
	{
	
	
	
	}
	
	private function format_html_question($showmore, $questions, $answers,
										  $page,$page_size, $next_page)
	{
	 $html="";	
	 if($page == 1) $html="<div class='allquestion'>";
	 
	
     $max=min($page_size,count($questions));
	 
	 
	 
     for($key=0; $key < $max; $key++)
	 {
		$question=$questions[$key]; 
		
		$postid=$question['postid'];
		$upvotes=$question['upvotes'];
		$downvotes=$question['downvotes'];
		$title=$question['title'];
		$created=$question['created'];

		$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/upload/' . $postid . '.jpg';							
		$imgexist = file_exists(ISLAMIQA_PLUGIN_BASE_DIR . '/upload/' . $postid . '.jpg');
		$htmlimage =  ($imgexist) ? "<img class='rounded-corner' alt='' src='{$imgpath}' >" 
								  : "<div class='rounded-corner'></div>";
		
		
		$html.="	  
		  <div class='row' id='line-question'>
				<!-- All Question -->
				<div class='col-sm-12 col-md-12'>

				<div class='col-sm-1 col-md-1'>
					<div class='vote'  onclick=''  id='voteup'>
						<i class='fa fa-angle-double-up gau '></i>  
					    <div>{$upvotes}</div>
					</div>
					<div class='vote' onclick='' id='votedown'>
						<i class='fa fa-angle-double-down rad'></i> 
						<div>{$downvotes}</div>
					</div>
				</div>						
				<!-- Question -->	
				<div class='col-sm-11 col-md-11 row-question'>
				<div class='col-sm-12 col-md-12'>
					<div class='col-sm-2 col-md-2'>
					{$htmlimage}
					</div>
					<div class='col-sm-10 col-md-10'>
						<div class='asked'>
						   asked on  <b> Politics </b>15m ago
						</div>
						<div class='single-question'>
							{$title}
						</div>
					</div>
				</div>
			    
				<!-- Answer -->
				<div class='col-md-12 col-sm-12'>
				   <div class='col-md-2 col-sm-2 col-votes'>
				   <!-- Answer options -->
				   <div class='container-votes'>        
                        <div class='clearfix row-votes ds' id='bestanswer{{postid}}'>
                               <span class='container-col-left'>
                               <div class='left-content'> Best </div>
                               <div class='left-content'> Answer</div>
                               </span>
                               <span class='icons-content'><i class='fa fa-bookmark'></i> </span>
                        </div>        
                        <div class='clearfix row-votes ds' id='favorite{{postid}}'>
                              <span class='container-col-left'>
                              <div class='left-content'> Make </div>
                              <div class='left-content'> Favorite</div>
                              </span>
                         <span class='icons-content' onclick='questions.makeFavoriteAnswer({{postid}},{{parentid}})'>
							  <i class='fa fa-star'></i> 
						 </span>
                        </div>
						
                        <div class='clearfix row-votes'>                                                            
                                   <span class='container-col-left'>                               
                                        <div class='lef-number' id='userupnumber'>200</div>
                                   </span>
                                   <span class='icons-number' id='voteup{{postid}}' onclick='questions.voteUpAnswer({{postid}},{{parentid}})'>
										<i class='fa fa-angle-double-up'></i> 
                                   </span>
                        </div>
        
                        <div class='clearfix row-votes'>
                           <span class='container-col-left'>                           
                               <div class='lef-number' id='userdownnumber' >130</div>                           
                           </span>        
                           <span class='icons-number' id='votedown{{postid}}' onclick='questions.voteDownAnswer({{postid}},{{parentid}})'>
							   <i class='fa fa-angle-double-down'></i> 
                           </span>
                        </div> 
						
						<div class='clearfix row-votes ds'>
                           <span class='container-col-left'>                           
                               <div class='lef-number' id='usercomment' >30</div>                           
                           </span>        
                           <span class='icons-content' id='comment' onclick='questions.comment'>
							   <i class='fa fa-comment'></i> 
                           </span>
                        </div> 
						
                    </div>				   
				   </div>
				   
				   <div class='col-md-10 col-sm-10'>
				    <div class='answer'>
					   <div class='bestanswer-title'>
							<b>Best Answer</b> by <b> Anonymous: </b>
					   </div>
					   <!-- Answer content -->
					   <div class='answer-content'>
							This is an answer
					   </div>
					   <!-- Comments -->
					   <div class='comment-footer'>
							<i class='fa fa-comment'></i>
							<span>comment</span>
							<span class='margin-left-10'>share</span>
					   </div>
					</div>
				 </div>	 
				 <!-- Answer End -->	
				 
				 <!-- More Answer -->
				 <div class='col-push-md-2 col-sm-10 col-md-10'>
					   <div class='more-answer' > 
							<i class='fa fa-angle-down'></i>
							<span>4 more answer</span>
					   </div>
				 </div>
				 
			   </div>
			   
			   	 <div class='col-md-push-2 col-sm-push-2 col-md-10 col-sm-10'>
					<span class='tags-question ds'> <i class='fa fa-tags fa-lg'></i></span>
				 </div> 
				 
				 <div class='col-md-push-2 col-sm-push-2 col-md-10 col-sm-10'>
				   <ul class='ans-btm-ul'>
                   <li>
                    <span class='pncl fa fa-pencil'></span>
                    <span class='red-simplespan'>  <a href=''>Answer</a>  </span>
                </li>
                <li>
                    <span class='has'>200</span>
                    <span class='simplespan'>Views</span>
                </li>
                <li>
                    <span class='has'>102</span>
                    <span class='simplespan'>Comments</span>
                </li>

                <li>
                    <div class='tooltip-social'>
                        <p style='color: #757575;font-size: 12px;'>Share</p>

                        <div class='tooltip-inner tooltip-inner3'>
                            <div class='social_icon facebook_icon'>
                                <a href='#'><i class='fa fa-facebook fa-2x'></i></a>
                            </div>
                            <div class='social_icon twitter_icon'>
                                <a href='#'><i class='fa fa-twitter fa-2x'></i></a>
                            </div>
                            <div class='social_icon google_icon'>
                                <a href='#'><i class='fa fa-google fa-2x'></i></a>
                            </div>
                            <div class='social_icon instagram_icon'>
                                <a href='#'><i class='fa fa-envelope fa-2x'></i></a>
                            </div>
                        </div>
                    </div>
                </li>
				<span style='float: right;'>
                <div class='tooltip-custom'>
                <i class='fa fa-ellipsis-h'></i>
					<div class='tooltip-inner tooltip-inner2'>
						<a href='#'>Invite authoritative user to answer</a><br>
						<a href='#'>Add to Reading List</a><br>
                        <a href='#'>Show Related Questions</a><br>
                        <a href='#'>Create Summary Answer</a><br>
                        <a href='#'>Mark Favorite</a><br>
                        <a href='#'>Report</a><br>
                      </div>
                  </div>
                </span>
				</ul>
				 </div>
			   </div>	
			 </div>  <!-- Question -->
		  </div>";		
		
	 }
	
	 $html.= "<div>
				<button id='loadMoreBtn' class='btn btn-bottom-red-load-more full-btn' type='button' onclick='question.loadMore()'> Load more <i id='loadMoreI'  class='fa fa-refresh fa-spin fa-fw' style='display:none'></i></button>
			 </div>";
			 
	 if($page == 1 ) $html.="</div> <!-- div general -->";
	  
	 return $html;
	 
	}
	
	private function get_array_questions($current_page = 0, $pagesize = 4, $checkmore=false, $orders = null)
	/**
	*
	*/
	{
	  $sql_order="";
	  $sql_where="";
	  $sql_additional="";
	  $sql_limit="";
	  
		 
	  if($current_page > 0)
	  {
		$start_page= ($current_page - 1) * $pagesize;
		if($checkmore) $pagesize += 1;
		$sql_limit=" LIMIT {$pagesize} OFFSET {$start_page}";
	  }
	  // Create order by
	  switch($orders)
	  {
	     case 'asc': $sql_order = "ORDER BY  ^posts.created ASC"; break;
		 case 'desc': $sql_order = "ORDER BY ^posts.created DESC"; break;		
	  }
	  
	  
	  // Final SQL -------------------
	  $sql= " SELECT ^posts.*, ^categories.title AS cattitle 
					FROM ^posts 
					LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
			  WHERE type='Q'
				{$sql_order}
				{$sql_limit}";	   
				
	 $records = qa_db_read_all_assoc(qa_db_query_sub($sql));
	 
	 return $records;
		
	}
	
	private function get_array_answers($array_questions)
	/**
	*
	*/
	{	
	 $answer=array();
	 foreach($array_questions as $key => $question)
	 {
		
		$answer[$key]['answers'] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $question['postid'] . "' ORDER BY created DESC"));
		$answer[$key]['count']= count($answer[$key]['answers']);
	 }
	  return $answer;
	}
		
	
	
} /* End class */
	