<?php


class qa_html_theme_layer 
	extends qa_html_theme_base
{

	  function head_script()
	 /**
	 * Load custom script
	 */
	 {	
	 
		if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::head_script();
		   return;   
		}
	   
	   if (qa_opt('embed_enable_thickbox')) {
            $this->content['script'][]="<script src='".ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/thickbox.js'></script>";
        }
		
	
	    $this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/modernizr.custom.29609.js'></script>";
		$this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/placeholders.min.js'></script>";
		$this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/jquery.min.js'></script>";
        $this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/jquery-ui.js'></script>";
		$this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/md5.js'></script>";
		$this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/owl.carousel.js'></script>";
		$this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/wow.min.js'></script>";
		$this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/respond.min.js'></script>";
        $this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/formvalidation.min.js'></script>";
		$this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/vendor/framework_formvalidation/bootstrap.min.js'></script>";
		
        $this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/app/custom.js'></script>";
        $this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/app/readmore.js'></script>";		        
		$this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/app/avatar.js'></script>";
        $this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/app/login-taps.js'></script>";
        $this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/app/initialize.js'></script>";
        $this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME."/js/app/dropdownanimatecss.js'></script>";;
	    $this->content['script'][]="<script src='".qa_path_html("")."qa-plugin/wysiwyg-editor/ckeditor/ckeditor.js?1.7.4'></script>";
	    $this->content['script'][]= $this->script_social_network();
	    $this->content['script'][]= $this->script_initialize();
		
			// Customs css
			
		$request=($this->request == "" || $this->request=="login") ? "home" : $this->request;
		
		if ($request)			
		{	
			$pathrelative="/js/app/init.{$request}.js";
			if (file_exists(ISLAMIQA_PLUGIN_BASE_DIR.$pathrelative))
				$this->content['script'][]="<script src='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME. $pathrelative ."'></script>";;
			
		}
		
		qa_html_theme_base::head_script(); 

    }
 
 function head_css()
	/**
	* Load custom css files
	*/
    {
	    if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::head_css();
		   return;   
		}
        //if ($this->template == 'admin')
		if (qa_opt('embed_enable_thickbox')) {
            $this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME."/css/vendor/thickbox.css";
        }	
		// First vendor css
		$this->content['css_src'][] = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css";			
        $this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. "/css/vendor/jquery-ui.css";
		$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. "/css/vendor/readmore.css";
		$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. "/css/vendor/animate.css";
		$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. "/css/vendor/owl.carousel.css";
		$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. "/css/vendor/formvalidation.min.css";
		$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. "/css/vendor/magic-check.css";
		
		// Second app css
		// IMPORTANT: Add new custom css after vendor
		$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. "/css/app/shame.css";	
		$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. "/css/app/style.css";			
		$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. "/css/app/style_font.css";			
		$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME."/css/app/themestyle.css";
		$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. "/css/app/login.css";	
		
		// Customs css
		if ($this->request != "")
		{	$pathrelative="/css/app/custom.{$this->request}.css";

			if (file_exists(ISLAMIQA_PLUGIN_BASE_DIR.$pathrelative))
				$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME. $pathrelative;
			
		}

	//	Warning- Try to remove islamiqa_custom_pages.css, any configuration move to /css/app/style.css
	//$this->content['css_src'][]=ISLAMIQA_PLUGIN_BASE_DIR_NAME."/css/app/islamiqa_custom_pages.css";
		
		 
		qa_html_theme_base::head_css();
		
    }

    function head_metas()
	/**
	* Custom head metas
	*/
    {
	    if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::head_metas();
		   return;   
		}
        $this->output("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
        $this->output("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, Zoom= false\">");
		$this->output("<link rel='shortcut icon' type='image/ico' href='" . ISLAMIQA_PLUGIN_BASE_DIR_NAME . "/islamiqa-favicon.ico'/>");
		
		qa_html_theme_base::head_metas(); // TODO: Change the autogenerated stub 

    }
	
		 
    public function nav_user_search()
	/**
	* Outputs login form if user not logged in
	*/
    {
	    if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::nav_user_search();
		   return;   
		}
		
        if (!qa_is_logged_in()) {
            $login = @$this->content['navigation']['user']['login'];
			
            if (isset($login) && !QA_FINAL_EXTERNAL_USERS) {
                $this->output(
                    '<!--[Begin: login form]-->', '<form id="qa-loginform" action="' . $login['url'] . '" method="post">', '<input type="text" id="qa-userid" name="emailhandle" placeholder="' . trim(qa_lang_html(qa_opt('allow_login_email_only') ? 'users/email_label' : 'users/email_handle_label'), ':') . '" />', '<input type="password" id="qa-password" name="password" placeholder="' . trim(qa_lang_html('users/password_label'), ':') . '" />', '<div id="qa-rememberbox"><input type="checkbox" name="remember" id="qa-rememberme" value="1"/>', '<label for="qa-rememberme" id="qa-remember">' . qa_lang_html('users/remember') . '</label></div>', '<input type="hidden" name="code" value="' . qa_html(qa_get_form_security_code('login')) . '"/>', '<input type="submit" value="' . $login['label'] . '" id="qa-login" name="dologin" />', '</form>', '<!--[End: login form]-->'
                );

                unset($this->content['navigation']['user']['login']); // removes regular navigation link to log in page
            }
        }
        $this->nav('main');  
        $this->nav('user');
    }

    public function nav_main_sub()
	/**
	* Output nav main sub
	*/
    {
	    if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::nav_main_sub();
		   return;   
		}
        if (in_array($this->template, array('questions', 'admin', 'user', 'users', 'account', 'favorites', 'user-wall', 'messages',
            'user-activity', 'user-questions', 'user-answers', 'plugin')))
            $this->nav('sub');
    }


	public function nav_link($navlink, $class)
	{
	
	  if($class== '') $class="item";
	  parent::nav_link($navlink,$class);
	}
	
    function nav_list($navigation, $class, $level = null)
	/**
	* Nav list
	*/
    {
	    if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::nav_list($navigation, $class, $level);
		   return;   
		}
		if($class=="") $class='item';
		
        $this->output('<ul CLASS="qa-' . $class . '-list' . (isset($level) ? (' qa-' . $class . '-list-' . $level) : '') . '">');

        if ($class == 'nav-main') {
            $new_order = array('activity', 'unanswered', 'questions', 'ask', 'tag', 'user', 'admin', 'custom-1');
            $temp_nav = array();
            foreach ($new_order as $key) {
                if (isset($navigation[$key]))
                    $temp_nav[$key] = $navigation[$key];
            }
            $navigation = $temp_nav;
        }

        $index = 0;
        foreach ($navigation as $key => $navlink) {
            $this->set_context('nav_key', $key);
            $this->set_context('nav_index', $index++);
            $this->nav_item($key, $navlink, $class, $level);
        }

        $this->clear_context('nav_key');
        $this->clear_context('nav_index');
        $this->output('</UL>');
    }
	
	
	

    public function search()
	/**
	* Adds login bar, user navigation and search at top of page in place of custom header content
	*/
    {
	    if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::search();
		   return;   
		}
     /*   $search = $this->content['search'];

        $this->output('<div class="col-md-3 col-sm-3 col-xs-8">',
            '<div class="form-group top_search">', '
            <form  ' . $search['form_tags'] . '>', @$search['form_extra']
        );

        $this->search_field($search);
        $this->search_button($search);

        $this->output('</form>', '</div></div>');*/
    }

    public function search_field($search)
	/**
	*
	*/
    {
        $this->output('<input type="text" ' . $search['field_tags'] . '  placeholder="Search" value="' . @$search['value'] . '" class="form-control"/>');
    }

    public function search_button($search)
	/**
	*
	*/
    {
        $this->output('<button class="btn btn-default" value="' . $search["button_label"] . '" type="submit"><i class="fa fa-search"></i></button>');
    }


    public function body_header()
	/**
	*
	*/
    {
		if (!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
		{
		   qa_html_theme_base::body_header();
		   return;
		}

        $request = qa_get("sort");
        $request_admin = qa_request();
	    $home = "";
		
        $this->output('<input type="hidden" id="activeflag" value="' . $request . '">');
        $this->output('<input type="hidden" id="request" value="' . $request_admin . '">');


        $this->output('<header>
		<nav class="web_nav">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-6 col-xs-4" style = "width:70%">
                    <div class="navbar-header">
                        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="' . qa_path_to_root() . '?qa=login" class="navbar-brand"><img src="' . ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/ui/logo.png"/></a> 
                    </div>
                    <!-- Collection of nav links and other content for toggling -->
					<div id="navbarCollapse" class="collapse navbar-collapse">
						<ul class="slimmenu" style="padding:0;">');
   

	$this->output("<i class='black_home'><a href='" . qa_path_to_root() . "?qa=questions'><i class='fa fa-home'></i></a></i>");	

	
	$hot_class = "";
	/*$recent_class= "";*/
	$view_class = "";
	$unanswered_class = "";
											
	switch($request) {
	    case 'hot': $hot_class="class='active'"; break;
		/*case 'recent': $recent_class="class='active'"; break;*/
		case 'views': $view_class="class='active'"; break;
		case 'unanswered': $unanswered_class="class='active'"; break;
	 
	}
	$this->output('                                                               
                   <li '.$hot_class.' >
                      <div class="menu_header">
                      <a href="' . qa_path_to_root() . '?qa=questions&sort=hot">Popular</a>
                    </div>
                    </li>
                    <li '.$view_class.'>
						<div class="menu_header">
                        <a href="' . qa_path_to_root() . '?qa=questions&sort=views">Most Viewed</a>
						</div>
                    </li>
                    <li '.$unanswered_class.'>
                       <div class="menu_header">
                       <a href="' . qa_path_to_root() . '?qa=unanswered&sort=unanswered">Unanswered</a>
                       </div>
                     </li>');

					/* 
					 <li '.$views_class.'>
						<div class="menu_header">
						<a href="' . qa_path_to_root() . '?qa=questions&sort=views">All Questions</a>
						</div>
                    </li>
                    */
					 
        $this->output('</ul></div>');
        $this->output('</ul></div>');

        $this->login_popup_social();
        $this->output('</div>
			</div>
		</nav>
	</header>');
    }
	
  
    public function header_custom()
	/**
	* Allows modification of custom element shown inside header after logo
	*/
    {
	    if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::header_custom();
		   return;   
		}
		
        if (isset($this->content['body_header'])) {
            $this->output('<div class="header-banner">');
            $this->output_raw($this->content['body_header']);
            $this->output('</div>');
        }
    }

 
   
    public function header()
	/**
	* Removes user navigation and search from header and replaces with custom header content. Also opens new <div>s
	*/
    {

	    if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::header();
		   return;   
		}
        $this->output('<div class="qa-header">');

        //$this->logo();
        $this->header_clear();
        $this->header_custom();

        $this->output('</div> <!-- END qa-header -->', '');

        $this->output('<div class="qa-main-shadow">', '');
        $this->output('<div class="qa-main-wrapper">', '');
        $this->nav_main_sub();
    }
	
	
    public function logo()
	/**
	*
	*/
    {
	    if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::logo();
		   return;   
		}
        $this->output('<div class="qa-logo">', $this->content['logo'], '</div>');
    }

	public function body_prefix()
	/**
	*
	*/
	{
	   qa_html_theme_base::body_prefix();
	}
	
	
    public function body_content()
    {

      //  $this->get_question_islamiqa();
	
        if (qa_opt(ISLAMIQA_PLUGIN_NAME) && in_array($this->request, array('login', ''))) {
         
			$home=qa_load_module('page','home_page');
			$this->output_raw($home->process($this));
			
			
            $this->body_suffix();
            $this->body_hidden();
            $this->widgets('full', 'bottom');
			$this->footer();
			
        } else
			qa_html_theme_base::body_content();
       
    }
	
	
	  public function sidepanel()
	  /**
	  * Custom sidepanel
	  */
    {

	    if(!qa_opt(ISLAMIQA_PLUGIN_NAME)) 
	    {
		   qa_html_theme_base::sidepanel();
		   return;   
		}
	   
        $this->output('<div class="qa-sidepanel">');
      
        
		
	/*	include ISLAMIQA_PLUGIN_BASE_DIR.'/includes/templates/sidepanelglobal.php';
		$sidepanelglobal=(new Renderer_sidepanel_global());
		
        $resultmyinterest=$sidepanelglobal->show_my_interests($this,$this->template);
        $resultmyknowledge=$sidepanelglobal->show_my_knowledges($this,$this->template);
        $resultmyfollows=$sidepanelglobal->show_my_follows($this,$this->template);
		$sidewidget=$resultmyinterest.$resultmyknowledge.$resultmyfollows;
		
		if($sidewidget != '')
		{
				$this->output('<div class="qa-q-left">');
				$this->output($sidewidget);
				$this->output('</div>');
		}*/

        $this->widgets('side', 'top');
        $this->sidebar();
        $this->widgets('side', 'high');
        $this->nav('cat', 1);
        $this->widgets('side', 'low');
        $this->output_raw(@$this->content['sidepanel']);
        $this->feed();
        $this->widgets('side', 'bottom');
        $this->output('</div>', ''); 
    }
	
	
    
    public function footer()
	/**
	*  prevent displa of regular footer content (see body_suffix()) and replace with closing new <div>s
	*/
    {
		if(!qa_opt(ISLAMIQA_PLUGIN_NAME))  {
				qa_html_theme_base::footer();
				return;	
		}
		

		
        $this->output('</div> <!-- END main-wrapper -->');		
        $this->output('</div> <!-- END main-shadow -->');
	/* 	if (in_array($this->request, array('privacy-policy', 'community-guidelines',  'contact', 'blog'))) {
            return false;
        } */

        $this->output('<section class="footer">
		<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h6>About</h6>
					<ul class="footer_about"> 
                    <li><a href="'.qa_path_html("") .'" class="logo"><img alt="logo" src="' . ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/ui/logo-grey.png"></a></li>
                    <li>We are on a mission to dispel myths and stereotypes by crowd sourcing the best content on Islam, its civilization and its people.</li>
					<li>Share knowledge, increase understanding and encourage discussion by signing up!</li>
					</ul>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h6>Site Map</h6>
                    <ul class="footer_links">
                        <li class="_item"><a href="' . qa_path_html("") . '">Home</a></li>
                        <li class="_item"><a href="' . qa_path_html("questions", array('sort' => 'hot')) . '">Popular Questions</a></li>
                        <li class="_item"><a href="' . qa_path_html("questions") . '">Recently Asked</a></li>
                        <li class="_item"><a href="' . qa_path_html("questions", array('sort' => 'views')) . '">All Questions</a></li>
                        <li class="_item"><a href="' . qa_path_html("unanswered") . '">Unanswered Questions</a></li>
                        <li class="_item">'.qa_link_selected("blog_page","Blog",$this->request).'</li>
                        <li class="_item">'.qa_link_selected("volunteering_page","Volunteering",$this->request).'</li>
                    </ul>
                    <ul class="footer_links">
                        <li class="_ietm">'.qa_link_selected("about_page","About",$this->request).'</li>
                        <li class="_item">'.qa_link_selected("privacy_page","Privacy Policy",$this->request).'</li>
                        <li class="_item"><a href="' . qa_path_html("community-guidelines") . '">Community Guidelines</a></li>
                             
                        <li class="_item">' . qa_link_selected("terms_page","Terms",$this->request).'</li>
                        <li class="_item">' . qa_link_selected("faq_page","FAQ",$this->request).'</li>
						<li class="_item">' . qa_link_selected("feedback_page","Give Feedback",$this->request).'</li>
                        <li class="_item">'. qa_link_selected("press_page","Press",$this->request).'</li>
                    </ul>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h6>Social</h6>
                    <div class="footer_social_links">
                        <div class="colfooter "> 
                            <div class="grpfooter grpfacebook">
							    <div class="fa fa-facebook fa-2x"></div>
							</div>
                            <div class="grpfooter grptweet">
								<div class="fa fa-twitter fa-2x"></div>
							</div>
                            <div class="grpfooter grpgoogle">
								<div class="fa fa-google-plus fa-2x"></div>
							</div>
                        </div>
                        <div class="colfooter">                        
                            <div class="grpfooter grplinkedin">
							   <div class="fa fa-linkedin fa-2x"></div>
							</div>
                            <div class="grpfooter grppinterest">
								<div class="fa fa-pinterest fa-2x"></div>
							</div>
							<div class="grpfooter grpinstagram">
								<div class="fa fa-instagram fa-2x"></div>
							</div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </footer>
	</section>');
    }

    
    public function title()
	/**
	* Add RSS feed icon after the page title
	*/
    {
        qa_html_theme_base::title();

//      $feed=@$this->content['feed'];
//      if (!empty($feed))
//          $this->output('<a href="'.$feed['url'].'" title="'.@$feed['label'].'"><img src="'.$this->rooturl.'images/rss.jpg" alt="" width="16" height="16" border="0" class="qa-rss-icon"/></a>');
    }

	

    public function favorite()
	/**
	*
	*/
    {
        $favorite = isset($this->content['favorite']) ? $this->content['favorite'] : null;
        if (isset($favorite)) {
            $favoritetags = isset($favorite['favorite_tags']) ? $favorite['favorite_tags'] : '';
            $this->output('<span class="qa-favoriting" ' . $favoritetags . '>');
            $favorite = str_replace('return qa_favorite_click(this);', 'qa_favorite_click(this);app();', $favorite); // modification - force redraw!
            $this->favorite_inner_html($favorite);
            $this->output('</span>');
        }
    }
	
	
	
	/**********************************************************************************************
	***********************************************************************************************
	**  Custom Methods:
	**  << This method will be there in another place >>
	***********************************************************************************************
	***********************************************************************************************/	


	private function nav_list_islamiqa($type = "main", $class = "")
  /**
  *  Custom nav list islamiqa
  */
    {
        $navigation = @$this->content['navigation'][$type];
        $index = 0;

		foreach ($navigation as $key => $navlink) {
            $this->set_context('nav_key', $key);
            $this->set_context('nav_index', $index++);
            $this->nav_item($key, $navlink, $class);
        }

        $this->clear_context('nav_key');
        $this->clear_context('nav_index');
    }	

	private function script_social_network()
	/**
	*  Load scripts for social network
	*/
	{
	
    require_once QA_INCLUDE_DIR . 'qa-db-users.php';
    require_once QA_INCLUDE_DIR . 'app/users.php';
    require_once QA_INCLUDE_DIR . 'qa-base.php';

    $app_id = qa_opt('facebook_app_id');
    $tourlxx = qa_opt('site_url');

    $appId = qa_opt('facebook_app_id');
    $faceeBookurl = qa_path_absolute('facebook-login', array('to' => $tourlxx));

    // twitter config
    $twiter_appId = qa_opt('twitter_app_id');
    $tourlTwitter = qa_path_absolute('hauth.done=Twitter');

    $google_appId = qa_opt('google_app_id');
    $tourlGoogle = qa_path_absolute('hauth.done=Google');

    $linkedInappId = qa_opt('linkedin_app_id');
    $tourlLinkedIn = qa_path_absolute('hauth.done=LinkedIn');

//    hauth.done=Google

     $facebookScript = 'function faceBooklogin() {} ';
	$twtterScript = ' function twitterLogin() {} ';
	$googleScript = ' function googleLogin() {} ';
	$linkedinScript = ' function linkedInLogin() {} ';
		
    if (strlen($app_id))  $facebookScript = 'window.fbAsyncInit = function() {
                        FB.init({
                          appId      : "' . $appId . '",
                          xfbml      : true,
                          version    : \'v2.5\',
                          status : true,
						 cookie : true,
						  oauth  : true
                        });
                      };

                      (function(d, s, id){
                         var js, fjs = d.getElementsByTagName(s)[0];
                         if (d.getElementById(id)) {return;}
                         js = d.createElement(s); js.id = id;
                         js.src = "//connect.facebook.net/en_US/sdk.js";
                         fjs.parentNode.insertBefore(js, fjs);
                       }(document, \'script\', \'facebook-jssdk\'));

					function faceBooklogin()
						{
                            FB.login(function(response) {
                                if (response.authResponse) {
                                 console.log("Welcome!  Fetching your information.... ");
                                 setTimeout(window.location="' . $faceeBookurl . '", 100);
//                                 setTimeout(window.location="http://localhost.com", 100);
//                                 FB.api("me", function(response) {
//                                   console.log("Good to see you, " + response.name );
//                                 });
                                } else {
                                 console.log("User cancelled login or did not fully authorize.");
                                }
                            });
                       }

				';
   		
    if (strlen($twiter_appId)) $twtterScript = ' function twitterLogin(){ setTimeout(window.location="'.$tourlTwitter.'", 100);}';
    if (strlen($google_appId)) $googleScript = ' function googleLogin(){ setTimeout(window.location="'.$tourlGoogle.'", 100);} ';
    if (strlen($linkedInappId))$linkedinScript = ' function linkedInLogin(){setTimeout(window.location="'.$tourlLinkedIn.'", 100);}';

    return  '<script type="text/javascript">'.$facebookScript.$twtterScript.$googleScript.$linkedinScript.' </script> ';
	
  }

   private function logged_user()
	/**
	* DEACTIVATE --- Logged in print
	*/
    {
	// output user avatar to login bar
		$html='<div class="qa-logged-in-avatar">'. (QA_FINAL_EXTERNAL_USERS) ? 
												   qa_get_external_avatar_html(qa_get_logged_in_userid(), 24, true) : 
												   qa_get_user_avatar_html(qa_get_logged_in_flags(), qa_get_logged_in_email(), qa_get_logged_in_handle(), qa_get_logged_in_user_field('avatarblobid'), qa_get_logged_in_user_field('avatarwidth'), qa_get_logged_in_user_field('avatarheight'), 24, true).'</div>';

	 // adds points count after logged in username
		$userpoints = qa_get_logged_in_points();
		$pointshtml = ($userpoints == 1) ? qa_lang_html_sub('main/1_point', '1', '1') : qa_lang_html_sub('main/x_points', qa_html(number_format($userpoints)));
		$html.="<span class='qa-logged-in-points'> ({$pointshtml})</span>";
		
		return $html;
    }

  
  private function login_popup_social()   
  /**
  *  Load script para login popup
  */
  {

    if (!qa_is_logged_in()) {
        $this->login_and_sign_up_social();
    }else{
        $this->logged();
    }

}


	private function login_and_sign_up_social()
	/**
	* Load Login sign up
	*/
	{

    $plugionLoginUrl =  qa_path_absolute('hauth.done=Natural');
    //$plugionLoginUrl =  "http://localhost/islamiqa/?qa=login";
    $plugionForgotUrl =  qa_path_absolute('forgot');

    $plugionRegisterUrl =  qa_path_absolute('hauth.done=Register');
    $plugionStep1Url =  qa_path_absolute('step1_page');

    $placeHolderHandled = trim(qa_lang_html(qa_opt('allow_login_email_only') ? 'users/email_label' : 'users/email_handle_label'), ':');
    $placeHolderPassword =  trim(qa_lang_html('users/password_label'), ':') ;


    $this->output('

		<div class="signup_login-navbar">
		<ul class="signup_login nav navbar-nav open">
		<li>
		<div class="dropdown" id="signUpDropdown" noclose>

		<!-- <button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" id="signUpDrop" type="button" class="dropdown-toggle" style = "padding-right: 15px"> -->
		<button id="signupBtn"  aria-expanded="true" aria-haspopup="true" data-toggle-animate="dropdown-animatecss"  animated-in="fadeIn animated" animated-out="fadeOut animated" id="signUpDrop" type="button" class="dropdown-toggle" style = "padding-right: 20px">
		Sign up <span class="caret"></span>
		</button>
		<!-- dropdown-menu-signUpDrop -->
		<ul id="noClose" aria-labelledby="signUpDrop" class="dropdown-menu dropdown-menu-login dropdown-menu-signup" >
		<div class="row text-center" >
		   <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8">
			  <ul class="slimmenu-2x sing-in-menu">
				 <li>
					<div class="prevLogin"><a href="#">Social</a></div>
				 </li>
				 <li>
					<div class="nextLogin"><a  href="#">E-Mail</a></div>
				 </li>
			  </ul>
		   </div>
		</div>
		<div id="owl-demo" >
		  <div class="item">
				<div class="sing-in-tab" >
				   <div class="row row-height" style="margin-top: 15px">
					  <div class="col-md-12 col-sm-12 col-xs-12">
						 <div class="btn btn-default btn-full-with btn-facebook menuLogin " onclick="faceBooklogin()"><i class="fa fa-facebook ileft"  aria-hidden="true"></i>  Sign up with facebook</div>						  
					  </div>
				   </div>
				   <div class="row row-height">
					  <div class="col-md-12 col-sm-12 col-xs-12">
						 <div class="btn btn-default btn-full-with btn-twitter menuLogin" onclick="twitterLogin()" ><i class="fa fa-twitter ileft" aria-hidden="true"></i> Sign up with twitter</div>
					  </div>
				   </div>
				   <div class="row row-height">
					  <div class="col-md-12 col-sm-12 col-xs-12">
						 <div class="btn btn-default btn-full-with btn-google menuLogin" onclick="googleLogin()"><i class="fa fa-google-plus ileft" aria-hidden="true" ></i>  Sign up with google</div>
					  </div>
				   </div>
				   <div class="row row-height">
					  <div class="col-md-12 col-sm-12 col-xs-12">
						 <div class="btn btn-default btn-full-with btn-linkedin menuLogin"  onclick="linkedInLogin()" style="margin-bottom: 20px"><i class="fa fa-linkedin ileft" aria-hidden="true" ></i>   Sign up with linkedin</div>
					  </div>
				   </div> 
				   <div>
					  <p style="margin-bottom: 0px">Alternatively, <strong>Sign Up With Email</strong>. By clicking Sign up, you agree to our terms of service and privacy policy. We will send you account related emails occasionally - we will never spam you, honest!</p>
				   </div>
				</div>
		  
		  </div>
		  <div class="item">	   
			 <div class="sing-in-tab">
						   <div class="row">
									  <div class="col-md-12 col-sm-12 col-xs-12">									  
										  <form id="frmSignup" name="frmSignup" class="form-horizontal" method="POST" action="'. $plugionRegisterUrl.'">												  
													<input type="hidden" name="toRegister" value="' .$plugionStep1Url.'">
													<input type="hidden" name="toInvalid" value="">
													<div class="form-group ">
													   <div class="col-sm-12 col-xs-12">
														  <input type="text" class="form-control reduce"  id="inputName" placeholder="Name" name="name">
													   </div>
													</div>
													<div class="form-group ">
													   <div class="col-sm-12 col-xs-12">
														  <input type="email" class="form-control reduce" id="inputEmail" placeholder="E-mail" name="email">
													   </div>
													</div>
													<div class="form-group ">
													   <div class="col-sm-12 col-xs-12">
														  <input type="password" class="form-control reduce" id="inputPassword" placeholder="Password" name="password">
													   </div>
													</div>
													<div class="form-group">
													   <div class="col-sm-12 col-xs-12">
														  <input type="password" class="form-control reduce" id="inputConfirmPassword" placeholder="Confirm Password" name="rpass">
													   </div>
													</div>
													<div class="form-group ">
													   <div class="col-sm-6 col-xs-6">
														  <input type="text" class="form-control reduce" id="inputCity" placeholder="City" name="city">
													   </div>
													   <div class="col-sm-6 col-xs-6 form-control-login">
														  <input type="text" class="form-control reduce" id="inputCountry" placeholder="Country" name="country">
													   </div>
													</div>
													<div class="form-group reduce">
													   <div class="col-sm-12">
														  <button type="submit" class="btn btn-primary btn-block">Register</button>                                                                          
													   </div>
													</div>
										 </form>
							  </div>
						   </div>
						   <div>
							  <p style="margin-bottom: 0px" >By clicking Sign up, you agree to our terms of service and privacy 
								 policy. We will send you account related emails occasionally 
								 - we will never spam you, honest!
							  </p>
						   </div>
			 </div>
		   </div>
		<!-- end dropdown-menu-signUpDrop -->
		</ul>
		</div>                                    
		</li>
		<li>
		<div class="dropdown" noclose>
		<button aria-expanded="true" aria-haspopup="true"  data-toggle-animate="dropdown-animatecss"  animated-in="fadeIn animated" animated-out="fadeOut animated" id="loginDrop" type="button" class="dropdown-toggle"><!-- Modifications ICON NEW Attention CESAR -->
		<i class="fa fa-user" aria-hidden="true"></i> Login <span class="caret"></span>
		</button>
		<ul aria-labelledby="loginDrop" class="dropdown-menu dropdown-menu-login">
		<li>  <div class="btn btn-default btn-full-with btn-facebook menuLogin" onclick="faceBooklogin()"><i class="fa fa-facebook" aria-hidden="true"></i>  Login with facebook</div> </li>
		<li>  <div class="btn btn-default btn-full-with btn-twitter menuLogin" onclick="twitterLogin()"><i class="fa fa-twitter" aria-hidden="true"></i> Login with twitter</div> </li>
		<li>  <div class="btn btn-default btn-full-with btn-google menuLogin" onclick="googleLogin()"><i class="fa fa-google-plus" aria-hidden="true"></i>  Login with google</div> </li>
		<li>  <div class="btn btn-default btn-full-with btn-linkedin menuLogin" onclick="linkedInLogin()" ><i class="fa fa-linkedin" aria-hidden="true"></i>   Login with linkedin</div> </li>                                                
		<li class="seprator" role="separator"><span>OR</span></li>

			<div id="login-form-popup">
				 <form id="loginForm" method="post" class="form-horizontal" action="' . $plugionLoginUrl . '">
					<input type="hidden" name="code" value="' . qa_html(qa_get_form_security_code('login')) . '"/>
					<input type="hidden" name="toInvalid" value="' . $plugionForgotUrl . '">
					
					<div class="form-group" style="margin-bottom:2px">
						
						<div class="col-md-12">
							<input type="text" class="form-control" name="emailhandle" placeholder = "'.$placeHolderHandled.'" />
						</div>
					</div>
				
					<div class="form-group" style="margin-bottom:2px">
						
						<div class="col-md-12">
							<input type="password" class="form-control" name="password"  placeholder="'.$placeHolderPassword.'" />
						</div>
					</div>
					
					
					 <div class="form-group" style="margin-bottom:2px">
						<div class="col-md-12">
							<div>
								<input type="checkbox" name="remember"  value="1"/>
								 <label class="control-label">Keep me logged in</label>
							</div>
							
						</div>
					</div>
				
					<div class="form-group" style="margin-bottom:2px">
						<div class="col-md-12">
						   <div class="pull-right">                                                                               
								<button type="submit" class="btn btn-primary">Sign in</button>
						   </div>                                                                                
						</div>
					</div>
				</form>
				</div>
		</ul>
		</div>                       

		</li>                    

		</ul>
		</div>');


}


	function logged() {

		$this->output('<div class="signup_login-navbar"><ul class="signup_login nav navbar-nav open">');

			/*admin panel link*/
		$level = qa_get_logged_in_level();
		if (qa_is_logged_in()) {

			$nav = $this->content['navigation']["user"];
			$nav = array_reverse($nav);	
			
			
			$nav["updates"]["url"] = null;
			$nav["updates"]["label"] = $this->menu_account_logged();
			$nav["updates"]["selected"] = "null";
						
			if($level >= QA_USER_LEVEL_ADMIN) 
			{
				$nav["admin"]["url"] = "./?qa=admin";
				$nav["admin"]["label"] = "Admin ".'<i class="fa fa-envelope" aria-hidden="true"></i>';
				$nav["admin"]["selected"] = "null";
				//end admin panel link
				
			/*	if(@$this->content['navigation']['sub']) {
					$menu_tag['label'] = 'Topics';
					$menu_tag['url'] = './?qa=admin/topics';
					$menu_tag['selected'] = null;			
					$this->content['navigation']['sub']['admin/topics'] = $menu_tag;
				}*/
			}	
			
			$nav = array_reverse($nav);
			$this->content['navigation']["user"] = $nav;
		}

		
		$this->nav_list_islamiqa("user");
		$this->output('</ul></div>');
    

		   
	}
	
  private function menu_account_logged()
  {
    global $followcount;
	
	$userid=qa_get_logged_in_userid();
    $user=qa_get_logged_in_handle();
	$mail=qa_get_logged_in_email();
	
	
	list($numUsers, $numTags) = qa_db_select_with_pending(qa_db_selectspec_count(qa_db_user_favorite_users_selectspec($userid)),
														  qa_db_selectspec_count(qa_db_user_favorite_tags_selectspec($userid)));

	$flagscount=thousandsCurrencyFormat($numTags['count']);
	$followcount=thousandsCurrencyFormat($numUsers['count']);

	$urlimage=$this->url_currentavatar(50);
	if ($urlimage) $user_image="<img  class='logged-rounded' src='{$urlimage}' >";
			  else $user_image="<div class='logged-rounded'></div>";
	
    $html="	<div clasS='dropdown' noClose>
				<button aria-expanded='true' aria-haspopup='true'  data-toggle-animate='dropdown-animatecss'  animated-in='fadeIn animated' animated-out='fadeOut animated' id='loggedDrop' type='button' class='dropdown-toggle'>
					<i class='fa fa-user fa-lg' aria-hidden='true'></i>
					<span class='caret'></span>
				</button>				
				<ul aria-labelledby='loggedDrop' class='dropdown-menu dropdown-menu-logged fadeIn animated'>
					<li>
					  <div class='row logged-user'>
						<div class='col-md-3 col-sm-3'> 
							{$user_image}
						</div>
						<div class='col-md-7 col-sm-7'>
						    <div class='col-md-12 col-sm-12 logged-title'>
								<a href='?qa=user&qa_1={$user}'> Administrador</a>
							</div>
							<div class='col-md-12 col-sm-12 nowrap'>
								<span class='logged-stat'><i class='fa fa-users' aria-hidden='true'></i> {$followcount}</span> 
							    <span class='logged-stat'><i class='fa fa-flag' aria-hidden='true'></i>  {$flagscount}</span>
							</div>							
						</div>
						<div class='col-md-2 col-sm-2'>
						  <div class='logged-a'><i class='fa fa-angle-right fa-lg'></i></div>
						</div>
					  </div>
					</li>
					<li class='logged-mail'>
					  <div class='logged-mail'>{$mail}</div>
					</li>
					<li class='logged-link logged-link-first'>
					  <a href='./?qa=account'>Account</a>
					  <div class='logged-a'><i class='fa fa-angle-right fa-lg'></i></div>
					</li>
					<li class='logged-link'>
					  <a href='./?qa=favorites'>Favorites</a>
					  <div class='logged-a'><i class='fa fa-angle-right fa-lg'></i></div>
					</li>
					<li class='logged-link'>
					  <a href='./?qa=user&qa_1={$user}&qa_2=wall'>Wall</a>
					  <div class='logged-a'><i class='fa fa-angle-right fa-lg'></i></div>
					</li>
					<li class='logged-link'>
					  <a href='./?qa=messages'>Private Messages</a>
					  <div class='logged-a'><i class='fa fa-angle-right fa-lg'></i></div>
					</li>
					<li class='logged-link'>
					  <a href='./?qa=user&qa_1={$user}&qa_2=activity'>Recent Activity</a>
					  <div class='logged-a'><i class='fa fa-angle-right fa-lg'></i></div>
					</li>
					<li class='logged-link'>
					  <a href='./?qa=user&qa_1={$user}&qa_2=questions'>My questions</a>
					  <div class='logged-a'><i class='fa fa-angle-right fa-lg'></i></div>
					</li>
					<li class='logged-link'>
					  <a href='./?qa=user&qa_1={$user}&qa_2=answers'>My answers</a>
					  <div class='logged-a'><i class='fa fa-angle-right fa-lg'></i></div>
					</li>
				</ul>
			</div>";
	
	return $html;
	
  }
  
  
  private function url_currentavatar($size=50)
  {
  
    $item=array( 'flags' 		=> qa_get_logged_in_flags(),
			     'avatarblobid' => qa_get_logged_in_user_field('avatarblobid'));

    return $this->urlimage_avatar($item,$size);
  }
  
  private function script_initialize()
  /**
  * Initialize variables javascript
  */
  {
        return '<script type="text/javascript">$(function(){ window.myConfigPath =  "'.qa_path_absolute('').'" });</script> ';
  }

  /*	private function header_admin_link()
    {
        $level = qa_get_logged_in_level();
        if (qa_is_logged_in() && $level >= QA_USER_LEVEL_ADMIN) {
            return '<li><a href="' . qa_path_to_root() . '?qa=admin/general">Admin</a></li> ';
        }
    }
	*/

}