<?php

class qa_html_theme_layer extends qa_html_theme_base
{
    var $custom_pages = array("step1_page","step2","step3","step4", "press_page", "privacy_page", "terms_page", "about_page","volunteering_page","faq_page", "feedback_page", "blog_page");
    var $regular_header = array("press_page", "privacy_page", "terms_page", "about_page","volunteering_page","faq_page", "feedback_page","blog_page");
  
    /***
	**  Load files necesary to execute custom plugin
	**   
	***/

    function head_css()
    {
        qa_html_theme_base::head_css();

        $custompages = qa_opt('plugin_islamiqa_custom_pages') && in_array($this->request, $this->custom_pages);

        if ($custompages)
        {
            $folder = QA_HTML_THEME_LAYER_URLTOROOT."css/islamiqa_custom_pages.css";
            $bootstrap = QA_HTML_THEME_LAYER_URLTOROOT."css/bootstrap.min.css";
            //$style = QA_HTML_THEME_LAYER_URLTOROOT."css/style.css"; 
            $scss = "<link rel='stylesheet' href='".$folder."'/>"; 
            $scss2 = "<link rel='stylesheet' href='".$bootstrap."'/>"; 

            $this->output($scss); 
            $this->output($scss2); 
        }
    }

    //add own js file    


    function head_script()
    {
        $custompages = qa_opt('plugin_islamiqa_custom_pages');

        if ($custompages)
        {
            $folder = QA_HTML_THEME_LAYER_URLTOROOT."js/islamiqa_custom_pages.js";
            $script = "<script src='".$folder."'></script>";
            $this->output($script);
            //$this->content['script'][] = $script;
            //$this->content['script'][] = $script2;
        }

        qa_html_theme_base::head_script();
	}
        
    public function body_header()
    {
        if (in_array($this->request, $this->custom_pages) && !in_array($this->request, $this->regular_header))
        {
            $this->output('
                <header>
                    <nav class="web_nav">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-sm-6 col-xs-4">
                                    <div class="navbar-header">
                                        <a href="'.qa_path_to_root().'?qa=login" class="navbar-brand"><img src="' . $this->rooturl . 'images/ui/logo.png"/></a>
                                    </div>
            ');

            if (qa_is_logged_in())
            {
                //$this->nav_list_islamiqa();
            }

            $this->output('
                                </div>'
            );

            //$this->islamiqa_custom_pages_signup();
            $this->output('
                            </div>
                        </div>
                    </nav>
                </header>
            ');
        }
        else 
            qa_html_theme_base::body_header();
    }

    public function body_suffix()
    {
         qa_html_theme_base::body_suffix();
    }
    
    
    function islamiqa_custom_pages_signup()
    {
        $this->output('
            <div class="signup_login-navbar">
                <ul class="signup_login nav navbar-nav">
        ');
        
        if (!qa_is_logged_in())
        {
            $this->output('
                    <li>
                        <div class="dropdown" id="signUpDropdown">
                            <button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" id="signUpDrop" type="button" class="dropdown-toggle" style = "padding-right: 20px">
                                Sign up <span class="caret"></span>
                            </button>
                            <ul aria-labelledby="signUpDrop" class="dropdown-menu dropdown-menu-login">
                                <div class="tabs-wrapper">
                                    <a href="#" class="remove-register"><i class="fa fa-times"></i></a>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#social" aria-controls="social" role="tab" data-toggle="tab">Social</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#email" aria-controls="email" role="tab" data-toggle="tab">E-Mail</a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="social">
                                            <li><a href="/facebook-login"><img alt="facebook" src="' . $this->rooturl . '/images/ui/login-with-facebook.png"></a></li>
                                            <li><a href="#"><img alt="twitter" src="' . $this->rooturl . '/images/ui/login-with-twitter.png"></a></li>
                                            <li><a href="#"><img alt="gplus" src="' . $this->rooturl . '/images/ui/login-with-gplus.png"></a></li>
                                            <li><a href="#"><img alt="linkedin" src="' . $this->rooturl . '/images/ui/login-with-linkedin.png"></a></li>
                                            <!--
                                            <li class="seprator" role="separator"><span>OR</span></li>
                                            <li><input type="text" placeholder="Enter e-mail" class="form-control"></li>
                                            <li><input type="text" placeholder="Enter password" class="form-control"></li>
                                            <li><input type="checkbox" aria-label="Keep me logged in"> Keep me logged in</li>
                                            <li class="text-right"><button class="btn btn-primary" type="submit">Login</button></li>
                                            -->
                                            <p>Alternatively, <strong>Sign Up With Email</strong>. By clicking Sign up, you agree to our terms of service and privacy policy. We will send you account related emails occasionally - we will never spam you, honest!</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="email">
                                            <form class="form-horizontal" method="POST" action="./?qa=login">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <input type="text" class="form-control" id="inputName" placeholder="Name" name="name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <input type="email" class="form-control" id="inputEmail" placeholder="E-mail" name="email">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="pass">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <input type="password" class="form-control" id="inputConfirmPassword" placeholder="Confirm Password" name="rpass">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control" id="inputCity" placeholder="City" name="city">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="password" class="form-control" id="inputCountry" placeholder="Country" name="country">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-primary btn-block">Register</button>
                                                    </div>
                                                </div>
                                                <p>
                                                    By clicking Sign up, you agree to our terms of service and privacy
                                                    policy. We will send you account related emails occasionally
                                                    - we will never spam you, honest!
                                                </p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" id="loginDrop" type="button" class="dropdown-toggle">
                                Login <span class="caret"></span>
                            </button>
                            <ul aria-labelledby="loginDrop" class="dropdown-menu dropdown-menu-login">
                                <li><a href="#"><img alt="facebook" src="' . $this->rooturl . '/images/ui/login-with-facebook.png"></a></li>
                                <li><a href="#"><img alt="twitter" src="' . $this->rooturl . '/images/ui/login-with-twitter.png"></a></li>
                                <li><a href="#"><img alt="gplus" src="' . $this->rooturl . '/images/ui/login-with-gplus.png"></a></li>
                                <li><a href="#"><img alt="linkedin" src="' . $this->rooturl . '/images/ui/login-with-linkedin.png"></a></li>
                                <li class="seprator" role="separator"><span>OR</span></li>
            ');

            $login = @$this->content['navigation']['user']['login'];

            if (isset($login) && !QA_FINAL_EXTERNAL_USERS)
            {
                $this->output('
                    <div id="login-form-popup">',
                        '<!--[Begin: login form]-->',
                        '<form id="loginform" action="' . $login['url'] . '" method="post">',
                            '<input class="form-control" type="text"  name="emailhandle" placeholder="' . trim(qa_lang_html(qa_opt('allow_login_email_only') ? 'users/email_label' : 'users/email_handle_label'), ':') . '" />',
                            '<input type="password"  name="password" class="form-control" placeholder="' . trim(qa_lang_html('users/password_label'), ':') . '" />',
                            '<div><input type="checkbox" name="remember"  value="1"/>',
                                '<label for="qa-rememberme">Keep me logged in</label></div>',
                            '<input type="hidden" name="code" value="' . qa_html(qa_get_form_security_code('login')) . '"/>',
                            '<div class="text-right"><button class="btn btn-primary" type="submit" name="dologin">Register</button></div>',
                        '</form>', '<!--[End: login form]-->',
                    '</div>
                ');

                unset($this->content['navigation']['user']['login']);
                // removes regular navigation link to log in page
            }

            $this->output('
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            ');
        }
        else
        {
            $this->nav_list_islamiqa_custom_pages("user");
        }

        $this->output('
            <div class="col-md-3 col-sm-3">
                <ul class="signup_login nav navbar-nav" style = "background: red !important">
        ');

        if (isset($_POST['name']) && !empty($_POST['name']) && 
            isset($_POST['email']) && !empty($_POST['email']) &&
            isset($_POST['pass']) && !empty($_POST['pass']) && 
            isset($_POST['rpass']) && !empty($_POST['rpass']) &&
            isset($_POST['city']) && !empty($_POST['city']) &&
            isset($_POST['country']) && !empty($_POST['country']))
        {
            require_once 'qa-include/qa-base.php';
            require_once 'qa-include/qa-db-users.php'; 
            require_once 'qa-include/qa-app-users-edit.php';
            require_once 'qa-include/qa-app-users.php';
            require_once 'qa-include/qa-app-posts.php';
            require_once 'qa-include/qa-db-admin.php'; // categories
            require_once 'qa-plugin/q2a-open-login-master/qa-open-login.php';

            $passs    = $_POST['pass'];
            $rpass    = $_POST['rpass'];

            if ($passs == $rpass)
            {
                $handle   = $_POST['name'];
                $email    = $_POST['email']; 
                $password = $passs; 

                if (qa_handle_to_userid($handle) > 0)
                {
                    //qa_redirect('forgot'); //Redirect to reset password
                }
                else
                {
                    $userid = qa_create_new_user($email, $password, $handle); 
                    qa_set_logged_in_user($userid);
                    qa_redirect('account');
                }//else exist
            }//passs == rpass
        } //if issets vars and not empty
    }

    function nav_list_islamiqa_custom_pages($type = "main", $class = "")
    {
        $navigation = @$this->content['navigation'][$type];
       
        $index = 0;
        foreach ($navigation as $key => $navlink)
        {
            $this->set_context('nav_key', $key);
            $this->set_context('nav_index', $index++);
            $this->nav_item($key, $navlink, $class);
        }

        $this->clear_context('nav_key');
        $this->clear_context('nav_index');
    }
    
    public function body_content()
    {
        $root = qa_path_to_root();
        $url = QA_HTML_THEME_LAYER_URLTOROOT;
        $ajaxlink = str_replace($root, "",$url);

        qa_html_theme_base::body_content(); 
        $this->output("<input type = 'hidden' value = '$ajaxlink' id = 'ajaxcustomlink'>");
    }
}
