<?php 
	
class qa_html_theme_layer 
   extends qa_html_theme_base {


function doctype()
	{
		if($this->template == 'account') {
			// add answerhide form
			$answerhide_form = $this->answerhide_form();
			if($answerhide_form) {
				$this->content['form_2'] = $answerhide_form;
			}
		}
		qa_html_theme_base::doctype();
	}
	
	function answerhide_form() 
	{
		if($handle = qa_get_logged_in_handle()) {
			require_once QA_INCLUDE_DIR . 'db/metas.php';
			$userid = qa_get_logged_in_userid();
			if (qa_clicked('works_at_save')) {
				if(qa_post_text('works_at')){
					qa_db_user_profile_set($userid, 'works_at', qa_post_text('works_at')) ;
				}
				else {
					qa_db_user_profile_set($userid, 'works_at', "") ;
				}
				qa_redirect($this->request,array('ok'=>qa_lang_html('admin/options_saved')));
			}
			$profile=qa_db_single_select(qa_db_user_profile_selectspec($userid, true));

			$ok = qa_get('ok')?qa_get('ok'):null;
			$fields = array();
			$fields['works_at'] = array(
					'label' => 'Works at',
					'tags' => 'NAME="works_at"',
					'type' => 'input',
					'value' =>  @$profile['works_at'],
					);
			$form=array(
					'ok' => ($ok && !isset($error)) ? $ok : null,
					'style' => 'tall',
					'title' => '<a name="works_at_text"></a> Works Info',
					'tags' =>  'action="'.qa_self_html().'#works_at_text" method="POST"',
					'fields' => $fields,
					'buttons' => array(
						array(
							'label' => qa_lang_html('main/save_button'),
							'tags' => 'NAME="works_at_save"',
						     ),
						),
				   );
			return $form;
		}
	}
}	