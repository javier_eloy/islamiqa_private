<?php
	

class Renderer_sidepanel_global {

	protected $template;

    public function show_my_interests($context,$template)
	/**
	*
	*/
    {
	  $this->template = $template;
        //if (qa_is_logged_in() && ($this->template == 'qa' || $this->template == 'tags' || $this->template == 'favorites' || $this->template == 'unanswered')) {
		ob_start();
        if (qa_is_logged_in() && $this->template == 'updates') {
            $context->output('<script type="text/javascript"> $(function(){
                $(".mytopic").click(function(){
                    $(".qadev-lhs-q").toggle(300);
                    if ($(".mytopicp").text() == "+") {
                        $(".mytopicp").text("-");
                    } else {
                        $(".mytopicp").text("+");
                    }

                    // Save config setting to database as qa_opt field
                    if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
                    } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
                    }

                    interests = ($(".mytopicp").text() == "-") ? 1:0;
                    xmlhttp.open("GET","qa-theme/Islamiqa/Page-Interests/savemyinterestsknowledges.php?i="+interests);
                    xmlhttp.send();
                    });
                });

                </script>');

            $context->output('<h3><a class="mytopic" href="#!"><span class="mytopicp" >' . (qa_opt('interests') ? '-' : '+') . '</span> My Interests</a>', '');
            $context->output('<a class="TopicKnowledgeEdit" style="text-decoration:none"
                href="JavaScript:newPopup(' . "'" . 'qa-theme/Islamiqa/Page-Interests/islamiqa-edit-interests-page.php?p=i' . "'" . ');">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Edit</a></h3>', '');

            $favorites = qa_db_read_all_assoc(qa_db_query_sub(
                'SELECT uf.userid, qw.word FROM ^userfavorites uf LEFT JOIN ^words qw ON uf.entityid = qw.wordid WHERE uf.entitytype = "T" AND uf.userid=# ', qa_get_logged_in_userid()
            ));

            foreach ($favorites as $list) {
                $context->output('<div class="qadev-lhs-q" style="display:' . (qa_opt('interests') ? 'block' : 'none') . '"> ' .
                    '<img class="qadev-lhs-q2" style="float:left; display:' . (qa_opt('interests') ? 'inline' : 'inline') .
                    '" src=qa-theme/Islamiqa/Page-Interests/images/' . str_replace(" ", "_", strtolower($list['word'])) . '.jpg alt="' . $list['word'] .
                    '" height="21" width="21"> </img><ul style="float:left; display:' . (qa_opt('interests') ? 'inline' : 'inline') . '">&nbsp;&nbsp;<a href="' .
                    qa_path_html('tag/' . $list['word']) . '">' . $list['word'] . '</a></ul><br/><br/></div>');
            }
        }
		
		$html=ob_get_contents();
		ob_end_clean();
		return $html;
    }
	
	public function show_my_follows($context, $template)
	/**
	*
	*/
    {
	    $this->template=$template;
		
		ob_start();		
        if (qa_is_logged_in() && in_array($this->template, array('updates'))) {
            $context->output('<script type="text/javascript"> $(function(){
                $(".myfollows").click(function(){
                    $(".qadev-lhs-f").toggle(300);
                    if ($(".myfollowsp").text() == "+") {
                        $(".myfollowsp").text("-");
                    } else {
                        $(".myfollowsp").text("+");
                    }
                    // Save config setting to database as qa_opt field
                    if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
                    } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
                    }

                    follows = ($(".myfollowsp").text() == "-") ? 1:0;
                    xmlhttp.open("GET","qa-theme/Islamiqa/Page-Interests/savemyinterestsknowledges.php?f="+follows);
                    xmlhttp.send();
                    });
                });

                </script>');

            $context->output('<h3><a class="myfollows" href="#!"><span class="myfollowsp" >' . (qa_opt('follows') ? '-' : '+') . '</span> I\'m Following</a>', '');
            $context->output('<a class="TopicKnowledgeEdit" style="text-decoration:none"
                        href="?qa=users">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Edit</a></h3>', '');

            $favorites = qa_db_read_all_assoc(qa_db_query_sub(
                'SELECT uf.userid, u.flags, u.email, u.handle, u.avatarblobid, u.avatarwidth, u.avatarheight FROM ^userfavorites uf
                LEFT JOIN ^users u ON uf.entityid = u.userid WHERE uf.entitytype = "U" AND uf.userid=#', qa_get_logged_in_userid()
            ));

            $i = 0;
            foreach ($favorites as $list) {
                // get user data
                $flags = $favorites[$i]['flags'];
                $email = $favorites[$i]['email'];
                $handle = $favorites[$i]['handle'];
                $avatarblobid = $favorites[$i]['avatarblobid'];
                $avatarwidth = $favorites[$i]['avatarwidth'];
                $avatarheight = $favorites[$i]['avatarheight'];
                $avatar = qa_get_user_avatar_html($flags, $email, $handle, $avatarblobid, $avatarwidth, $avatarheight, 22, false);

                $context->output('<div class="qadev-lhs-f" style="display:' . (qa_opt('follows') ? 'block' : 'none') . '">
                                    <div class="qadev-lhs-f2" style="float:left; display:' . (qa_opt('follows') ? 'inline' : 'inline') . '">' . $avatar . '</div>
                                    <ul style="float:left; display:' . (qa_opt('follows') ? 'inline' : 'inline') . ';"> &nbsp;&nbsp;
                                    <a href="' . qa_path_html('user/' . $list['handle'] . '/activity') . '">' . $list['handle'] . '</a></ul><br/><br/>
                               </div>');
                $i++;
            }
        }
		
		$html=ob_get_contents();
		ob_end_clean();
		return $html;
    }

    public function show_my_knowledges($context, $template)
	/**
	*
	*/
    {
	   $this->template = $template;
	   ob_start();
        if (qa_is_logged_in() && $this->template == 'unanswered') {
            $context->output('<script> $(function(){
                $(".myknowledge").click(function(){
                    $(".qadev-lhs-k").toggle(300);
                    if ($(".myknowledgep").text() == "+") {
                        $(".myknowledgep").text("-");
                    } else {
                        $(".myknowledgep").text("+");
                    }

                    // Save config setting to database as qa_opt field
                    if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest(); // code for IE7+, Firefox, Chrome, Opera, Safari
                    } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // code for IE6, IE5
                    }

                    knowledges = ($(".myknowledgep").text() == "-") ? 1:0;
                    xmlhttp.open("GET","qa-theme/Islamiqa/Page-Interests/savemyinterestsknowledges.php?k="+knowledges);
                    xmlhttp.send();
                    });
                });
                </script>');

            $context->output('<div class="qa-q-left">', '');
            $context->output('<h3><a class="myknowledge" href="#!"><span class="myknowledgep" >' . (qa_opt('knowledges') ? '-' : '+') . '</span> My Expertise</a>', '');
            $context->output('<a class="TopicKnowledgeEdit" style="text-decoration:none"
                        href="JavaScript:newPopup(' . "'" . 'qa-theme/Islamiqa/Page-Interests/islamiqa-edit-interests-page.php?p=k' . "'" . ');">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Edit</a></h3>', '');

            $favorites = qa_db_read_all_assoc(qa_db_query_sub(
                'SELECT uf.userid, qw.word FROM ^userfavorites uf LEFT JOIN ^words qw ON uf.entityid = qw.wordid WHERE uf.entitytype = "K" AND uf.userid=# ', qa_get_logged_in_userid()
            ));

            foreach ($favorites as $list) {
                $context->output('<div class="qadev-lhs-k" style="display:' . (qa_opt('knowledges') ? 'block' : 'none') . '"> ' .
                    '<img class="qadev-lhs-k2" style="float:left; display:' . (qa_opt('knowledges') ? 'inline' : 'inline') .
                    '" src=qa-theme/Islamiqa/Page-Interests/images/' . str_replace(" ", "_", strtolower($list['word'])) . '.jpg alt=<' . $list['word'] .
                    ' height="21" width="21"> </img><ul style="float:left; display:' . (qa_opt('knowledges') ? 'inline' : 'inline') . '">&nbsp;&nbsp;<a href="' .
                    qa_path_html('tag/' . $list['word']) . '">' . $list['word'] . '</a></ul><br/><br/></div>');
            }

            $context->output('</div> <!-- END qa-q-left -->', '');
        }
		$html=ob_get_contents();
		ob_end_clean();
		return $html;		
    }
}