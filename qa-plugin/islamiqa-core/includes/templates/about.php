<?php 

/// Created by: Javier Hernandez
/// Date: 28/07/2016

class Renderer_about
{

    public function render($sidepanel) {
		
	 $html="<div class='container_custom_page about-page'>
				<div class='row'>
						<div class='col-sm-8'>
						<p class='heading'>About Islamiqa</p>
						<div class='in-content'>
							<p class='ct-main-head'>Islamiqa Privacy Policy</p>
							<p class='ct-main-content'>Welcome to Islamiqa.com...</p>
						</div>
						</div>
						{$sidepanel}
				</div>
			</div>";
		return $html;
	
	
	}
}