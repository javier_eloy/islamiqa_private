<?php 

/// Created by: Javier Hernandez
/// Date: 28/07/2016

class Renderer_privacy
{

    public function render($sidepanel) {
		
   $html = "<div class='container_custom_page privacy-page'>
            <div class='row'>
             <div class='col-sm-8'>
             <p class='heading'>Privacy Policy</p>
             <div class='in-content'>
              
                <p class='ct-heading'>Islamiqa Privacy Policy</p>
                <p class='ct-main-content'>Welcome to Islamiqa.com (“Islamiqa”, “we”, “us”, or “our”). We operate the Islamiqa website, browser extension and mobile application, which help you easily “keep” webpages you are interested in, share kept pages with your friends and contacts on a variety of social networks, publish kept pages in public “libraries”, add comments and messages to kept pages, and receive personalized recommendations based on the things you keep and libraries you follow (the “Service”). We provide this Privacy Policy to let you know what information we collect, how we use it, share it and protect it.</p>
                <p class='ct-main-content'>By using Islamiqa, you accept the practices described in this Privacy Policy. This Privacy Policy does not apply to information collected by third-party websites and web services linked to or otherwise accessible from Islamiqa. The information collected or received by third-party websites is subject to their own privacy policies.</p>
                <p class='ct-main-content'>To enable the Service, including providing you with customized Islamiqa search results and making the webpages you “kept” accessible to you and your social contacts, Islamiqa occasionally collects some information about your browsing history as well as your search queries and results. This information is de-identified, encrypted and held securely on our servers without becoming visible to anyone, including our employees, except in accordance with your privacy choices.</p>

				<p class='ct-heading'>Guiding Principles</p>
                <p class='ct-main-content'>To protect your privacy, we commit to the following principles:</p>
                 
				 <ul class='ct-main-content'>
					<li>We will give you control over your information and its visibility to other Islamiqa users.</li>
					<li>We will not collect any information from you except insomuch as collection is necessary to provide you with the Service.We will protect your browsing history information and search queries we collect. We cannot view this information, so furthermore cannot share this information with anyone else.</li>
					<li>To the maximum possible extent, we will use only aggregated information rather than personally identified or identifiable information.</li>
					<li>We will not store any of the personal content you may have on web pages you visit. Specifically, we will not record any passwords, credit card or financial account details. However, when you keep a page, we do store the page title, URL, and the date you kept the page.</li>
					<li>Any personal information that you provide will be secured using industry standard protocols and technology.</li>
				</ul>	
				<p class='ct-heading'>How Do We Secure Your Private Click and Browsing History?</p>
				<p class='ct-main-content'> Occasionally, we have to collect some information regarding parts of your browsing history in order to provide you with better search results and experience. This information may include your User ID and the URL you visited.</p>

				<p class='ct-main-content'>Occasionally, we have to collect some information regarding parts of your browsing history in order to provide you with better search results and experience. This information may include your User ID and the URL you visited.</p>
				<p class='ct-main-content'>To protect your privacy, we chose to use specific technological techniques (such as time-decaying one-way hash filters) to store it in a way that prevents us or anyone else from easily querying or viewing the data. Specifically, even if we were forced to give information related to your click history (such as which search results you click), our methods do not allow for direct retrieval; instead, we compute a signature of many combined data points. We could only tell probabilistically (with high false positive and false negative results) whether the data was previously stored, with certainty declining over time.</p>
				<p class='ct-main-content'>Whenever we will refer to a “Secured Manner” hereinafter, we will mean the technological techniques mentioned in this article.</p>
				
				<p class='ct-heading2'>Collection of Information</p>
				<p class='ct-heading'>Login</p>
				<p class='ct-main-content'>In order to become a Islamiqa user, you will register to the Service or log in using your social login. This means that upon your first login, Islamiqa collects your full name and email address, profile photo, gender, languages, devices and list of friends from the social network you connect through. Islamiqa is a social product, providing you added value by learning from the search queries and results of your social contacts. This is why when you login, we collect information about your friends, friends of friends, followers and social contacts. This information is used to improve your social search capabilities and enable you to communicate with your social contacts through the Service, for example with messages.</p>
				<p class='ct-main-content'>In addition, when you first login, Islamiqa, upon your approval, gives you the ability to send us all of your existing bookmarks and retains them as kept pages for your future reference.</p>

				<p class='ct-heading'>Search History</p>				
				<p class='ct-main-content'>Every time you search the Internet and click on a result, Islamiqa collects information about your search and choice of results in order to improve and personalize the Islamiqa search results that you and other Islamiqa users get. Specifically, Islamiqa collects your Islamiqa user ID ; the search query you searched and its URL; the result you selected and the time you selected it; and whether the result you selected was provided by Islamiqa or by an organic search engine. We do not store this information in an easily retrievable form (see “Secured manner” for more information), and your searches are not connected to your user account</p>
				
				<p class='ct-heading'>Browsing History</p>				
				<p class='ct-main-content'>To Improve the Keeper’s Behavior</p>
				<p class='ct-main-content'>When you visit a webpage, we transfer, but do not store, the page’s URL to our servers to retrieve whether:</p>
				
				<ul class='ct-main-content'>
				<li>You have previously kept the page</li>
				<li>Your connections or others have kept the page</li>
				<li>The page has any special settings, such as a custom Keeper position, etc.</li>
				<li>We do not store this request in any manner.</li>
				</ul>
				
				<p class='ct-main-content'>In order to improve the operati on of the Islamiqa Keeper (which appears at the bottom right corner of your screen when you use Islamiqa) and avoid bothering you with repetitive prompts, when the Keeper opens on a page, we transmit the URL and store in a way that we cannot easily retrieve (see “Secured Manner”), so that we do not automatically open the Keeper in the future.</p>

				<p class='ct-heading'>Technical Information</p>	
				<p class='ct-main-content'>We collect and store certain technical information when you use Islamiqa, including the type of operating system you are using; Internet protocol address; browser type; other information about your geographic location; and the domain name of your Internet service provider.</p>				
				
				<p class='ct-heading'>Cookies</p>	
				<p class='ct-main-content'>We may use cookies (pieces of data stored on your computer tied to information about you) to enhance and personalize your experience on Islamiqa; help authenticate your identity when you visit and transact with Islamiqa; remember your preferences and login information; present and help measure and research the effectiveness of Islamiqa offerings; and customize the content provided to you by Islamiqa.</p>				
				
				<p class='ct-heading'>Usage statistics</p>	
				<p class='ct-main-content'>We may track usage statistics in order to monitor and improve the Service performance and features. Usage information is retained in identified form. It includes some information about a user’s engagement with a web page (as mentioned on the “browsing history” section above), including interaction with the Islamiqa interface itself. For example, Islamiqa would retain the number of keeps a specific user made per day, or the number of clicks a keep received from search results, in aggregate.</p>				

				<p class='ct-heading'>Your Correspondence</p>	
				<p class='ct-main-content'>If you contact us for any reason (by sending us an email, fax, Islamiqa messaging, or completing a form on our website), we may keep a record of that correspondence in order to refer to it when investigating the issue you have raised.  We will not make your email address available to third parties without your explicit permission.</p>				

				<p class='ct-heading'>Your Privacy Choices</p>	
				<p class='ct-main-content'>Islamiqa is a social product, learning from the search queries and search results of your social contacts and allowing you to interact with social contacts in the context of existing web-pages. It is important that you take a minute to learn and understand the privacy defaults and choices you can make on the Service.</p>				

				
				<p class='ct-heading'>Keeping Webpages</p>	
				<p class='ct-main-content'>You can choose to “keep” any webpage into (1) “My Main Library”, (2) “My Private Library”, (3) a “Published” Library you create, or a (4) “Private” Library you create. The webpage addresses that you choose to “keep” into “My Main Library”, are viewable by your social contacts who use Islamiqa. They will be able to see that you “kept” a certain webpage when they make a related query in a search engine or on Islamiqa.com, which will appear to them in conjunction with your name and profile photo. They will not see the title of the page rendered at the time you kept it, and cannot see the list of pages you kept into “My Main Library” in its entirety. You can choose to “keep” a webpage into “My Private Library”, in which case your social contacts will not be able to see that you “kept” it. Islamiqa, however, will record that you “kept” a webpage privately, in order to provide you with access to that webpage when you later search for it again. You can choose to “keep” a webpage into a “Published” Library you create, in which case the address, title, and content of the webpages in the library are viewable and searchable by anyone on the internet. You can choose to “keep” a webpage into a “Private” Library you create, in which case the webpages in the library are viewable only by people you invite. If you “keep” pages that are protected behind a login wall, and we detect it as such (i.e your personal email messages or social networking posts), those pages will be, by default, not accessible by your social contacts.</p>				

				<p class='ct-heading'>Following Libraries</p>	
				<p class='ct-main-content'>You are able to “follow” libraries in Islamiqa. If you follow a published library, anyone on the internet will be able to see that you followed this library, which will appear to them in conjunction with your name and profile photo. If you “follow” private libraries in Islamiqa, the library owner and people who are also following this library will be able to see your name and profile photo.</p>				
				
				<p class='ct-heading'>Islamiqa Messages</p>	
				<p class='ct-main-content'>Islamiqa allows you to post messages on selected web pages to specific social contacts, even if they do not use Islamiqa. These messages are only viewable by those social contacts with whom you have selected to share them. If they are not Islamiqa users themselves, the recipients of such a message will be directed to the webpage in topic.</p>				

				<p class='ct-heading'>Deleting your information</p>	
				<p class='ct-main-content'>You can choose to permanently remove all your information related to the webpages you keep, and libraries you create from Islamiqa and terminate your profile, through your profile settings on the Islamiqa website, in which case all information associated with the webpages you kept, will be deleted. To delete your Islamiqa user and information, contact us here.</p>				

				<p class='ct-heading'>Using Your Information</p>	
				<p class='ct-main-content'>Your information is stored on our servers or on the servers of our storage service providers, which are located in the U.S. or the EU, and used to operate and improve the Service; to conduct research about your use of the Service; to personalize the content and advertisements provided to you; and to communicate with you and respond to your inquiries.</p>				
				<p class='ct-main-content'>We use elements of your browsing history and search queries to provide you with the Service, including to improve and personalize the Islamiqa search results that you and other Islamiqa users get and to enable you to communicate with your social contacts who use Islamiqa.</p>				
				<p class='ct-main-content'>We may contact you to tell you about new features, service issues or changes to this Privacy Policy.</p>											
				<p class='ct-main-content'>We will use your information to notify you when there has been any activity relating to your profile, such as messages from your social contacts.</p>											
				<p class='ct-main-content'>We may use your browsing information and search queries in an aggregated, completely anonymous manner to highlight trends and patterns in the way that Islamiqa users use the web.</p>											
				
				<p class='ct-heading'>Sharing Your Information</p>	
				<p class='ct-main-content'>We share your information with third parties only in limited circumstances where we believe such sharing is (a) permitted by you, (b) reasonably necessary to offer the service, or (c) legally required.</p>											
				<p class='ct-main-content'>Your browsing history and search queries will not be accessible by anyone, not even our employees, unless you so choose (for example, by publicly “keeping” a page).</p>											
				<p class='ct-main-content'>As discussed above, you may decide to share your information, including “kept” web pages and messages with your social contacts who use Islamiqa or with all Islamiqa users.</p>											
				<p class='ct-main-content'>We may disclose your information to certain trusted third-party providers we use to help provide the Service to you. For example, we may use a provider to host our website and to provide data processing activities. In this case, we will remain responsible for your information and contractually bind any third-party provider to act strictly on our instructions.</p>											
				<p class='ct-main-content'>We may be required to disclose certain data under legal process, including a court order, search warrant or subpoena. Even if we were so required to share a user’s browsing history or search queries, it would be extremely difficult to identify which information belonged to what user.</p>											
				<p class='ct-main-content'>In the event that ownership of FortyTwo Inc., or individual business units owned by FortyTwo Inc., was to change as a result of a merger, acquisition or transfer to another company, your information may be transferred to the new owner so the Service can continue. In any such transfer of information, your user information would remain subject to the promises made in this Privacy Policy.</p>											

				<p class='ct-heading'>Security</p>	
				<p class='ct-main-content'>We deploy industry standard security safeguards to help prevent unauthorized access to or misuse of your information. At the same time, we cannot guarantee that your information will never be disclosed in a manner inconsistent with this Privacy Policy (for example, as a result of unauthorized acts by third parties that violate applicable law or agreements). To protect your privacy and security, Islamiqa uses social networks login to help verify your identity before granting access or making corrections to any of your information.</p>											
				
				<p class='ct-heading'>Children's Privacy</p>	
				<p class='ct-main-content'>Islamiqa is intended for a general audience. Children under the age of 13 are not permitted to be on social networks, nor are they permitted to sign up or log in to Islamiqa.</p>											
				
				<p class='ct-heading'>Changes to this Privacy Policy</p>	
				<p class='ct-main-content'>We may update this Privacy Policy from time to time, so you should review it periodically. If there are significant changes to FortyTwo's information practices, you will be provided with appropriate online notice. You may be provided with additional privacy-related information in connection with special features and services that may be introduced in the future.</p>											

			</div>
			</div>
				{$sidepanel}
			</div>
		</div>";
		  
		return $html;
	
	
	}
}