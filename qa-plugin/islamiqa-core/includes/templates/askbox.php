<?php


class Renderer_askbox
{


 public function render() 
 {

	$htmlcategories=$this->load_categories();
	$htmlgallery=$this->html_gallery();
	
    return "{$htmlgallery} 
	<div class='new_question' id='new_question'>
	<span class='iconclose'><i class='fa fa-close fa-lg'></i></span>
	  <div class='newq_text_div'>
		<div class='newq_title'>
			<p class='newq_subtitle_post'>Title</p>
			<input type='text' class='newq_title' id='newq_title' name='newq_title' required >
		</div>
		<div class='newq_link'> 
			<span class=' fa fa-pencil fa-lg'></span>
		</div>
	   </div>
		<p class='newq_subtitle'>Augment your question</p>

		<div class='newq_text_div'>
			<div class='newq_text newq_text_text' id='newq_text'>
				<textarea id='newq_content' rows='3' cols='65%' required ></textarea>
			</div>
			<div class='newq_text newq_text_icon' id='newq_text_icon'>			
				<li class='newq_link fa fa-link fa-lg'></li>
				<li class='newq_image fa fa-image fa-lg'></li>
			</div>	
        </div>

		<div class='topics'>
        	<span class='six-li-icon'><i class='fa fa-tags fa-lg'></i></span>
        	<li class='newq_topics'>Topics </li>
            <input type='text' class='topics_input' name='tags' id='tags' onkeyup='qa_tag_hints();' onmouseup='qa_tag_hints();' value='' placeholder='Use hyphens to combine words'></input>
        </div>

        <div class='qa-form-tall-note'>
        	<span id='tag_examples_title' style='display:none;'>Example tags: </span>
        	<span id='tag_complete_title' style='display:none;'>Matching tags: </span>
        	<span id='tag_hints'></span>
        </div>

		<p class='newq_subtitle'>Categories *</p>
		<fieldset id='checkArray'>
			<div class='col-xs-12 col-sm-12 col-lg-12' id='categories'>
				{$htmlcategories}
			</div>
		</fieldset>

			<p class='newq_subtitle'>Header Images</p>
			<fieldset>
				<div class='col-xs-12 col-sm-12 col-lg-12' style='text-align:center'>
					<span class='newq_img_items' style='vertical-align:middle;'><i class='fa fa-image fa-2x' style='color:#cf2129;'></i></span>
					<span class='newq_img_items'>
						<input type='button' id='get_file1' value='Upload'>
					</span>
					<span class='newq_img_items'><li class='cat-li'><p style='color:#cf2129;'>or</p></li></span>
					<span class='newq_img_items'>
						<input type='button' id='get_file2' value='Pick from our gallery'>					
					</span>
					<div id='customfileupload1'></div>
				   <input type='file' id='my_file1' name='my_file1'  accept='.jpg' >
				</div>
			</fieldset>
			
				<p class='newq_subtitle'>Settings</p>
				<div class='col-xs-12 col-sm-12 col-lg-12' style='text-align:center'>
  					<span class='inline'><input type='checkbox' name='set[]' class='magic-check' id='set01' value='answer'><label for='set01' class='first-span'>Allow anonymous answers</label></span>
  					<span class='inline'><input type='checkbox' name='set[]' class='magic-check' id='set02' value='email'><label for='set02' class='first-span'>Enable E-Mail notifications</label></span>
  					<span class='inline'><input type='checkbox' name='set[]' class='magic-check' id='set03' value='post'><label for='set03' class='first-span'>Post Anonymously</label></span>
<!--				<form method='post' action=''>	-->
					<button type='button' onclick='NewQuestion();' class='postnew' id='postnew'>Post my question</button>
<!--				</form>	-->
					<p class='login_message' id='login_message'></p>
			    </div>
			</div>";
  
   }
   
   private function html_gallery()
   {
	$html="<div id='new_gallery' class='new_gallery'>
				<div id='content_gallery'>
				
				</div>
		  </div>";
	
	return $html;
	   
   }
   private function load_categories()
   /**
   * Load categories dinamically 
   */
   {
		require_once QA_INCLUDE_DIR.'db/selects.php';	
	    $categories=qa_db_select_with_pending(qa_db_category_nav_selectspec("", true, false, true));
		$html="";
		
		foreach ($categories as $key=>$category) 
		{ 
		  $title=$category['title'];
		  $shortTitle=shrink_text($title,14);
		  $idcat=$category['categoryid'];
		  $html.="<div class='col-xs-3 col-sm-3 col-lg-3'>";
		  $html.="<input type='radio' name='cat[]' class='magic-radio'  id='cat{$key}' value='{$idcat}'><label for='cat{$key}' title='{$title}' class='first-span'>{$shortTitle}</label>";
		  $html.="</div>";
		}
		/*    $navcategoryhtml.='<a href="'.qa_path_html('admin/categories', array('edit' => $category['categoryid'])).'">'.
					qa_html($category['title']).'</a> - '.qa_lang_html_sub('main/x_questions', $category['qcount']).'<br/>';
		*/
		return $html;
   }

}