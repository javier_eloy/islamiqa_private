<?php 

/// Created by: Javier Hernandez
/// Date: 28/07/2016

class Renderer_press
{

    public function render($sidepanel) {
	
	$linkterm ="<a class='nonblock' href='".qa_path_html("terms_page")."'>Terms of Service</a>";
	 $html=" 
			<div class='container_custom_page'>
                <div class='row'>
					<div class='col-sm-8'>
					<p class='heading'>Press</p>
					<div class='in-content'>
                        <p class='ct-main-content'>For all press-related inquiries, please contact us at press@Islamiqa.com. For general questions, please contact us at feedback@Islamiqa.com. Keep up with the latest Islamiqa news on the Islamiqa Blog.</p>
						<p class='ct-main-content'>Interested in using Islamiqa content in your stories or on other websites? The vast majority of Islamiqa content is available for you to republish freely as long as you directly link to the original content and give proper credit to Islamiqa and the author. Full details of Islamiqa's republishing policies can be found in our <b>{$linkterm}</b>.</p>
					</div>					
					</div>
					$sidepanel						
				</div>
			</div>";
		return $html;
	
	
	}
}