<?php 

/// Created by: Javier Hernandez
/// Date: 02/08/2016

class Renderer_blog
{

    public function render($searchbox) {
		
	 $html="<div class='blog-page'>
			<div>
				<div class='container first-section''>
					<div class='row'>
					<div class='col-xs-12 text-center'>
						<p class='v-heading'>Welcome to <br> Islamiqa's Blog</p>
						<p class='v-sub-heading'>Join a team of fun, talented, and dedicated people looking to crowdsource the best answers to your questions about Islam and its civilisation.</p>
						<div class='input-group v-ig'>
						{$searchbox}
						
						</div>
					</div>	
					</div>
				</div>
			</div>

			<div class='container'>
			<div class='row'>
				<div class='col-sm-2 float-right'>
				<div class='box-s'>
					<span class='fa fa-angle-double-up gau'></span>
					<p class='nbr'>2361</p>
				</div>
				<div class='box-s'>
					<span class='fa fa-angle-double-down rad'></span>
					<p class='nbr'>361</p>
				</div>
				</div>
				
				<div class='col-sm-6 question'>
					<div class='blog-social-top'>
						<span class='b-txt'>share on</span>
						<span class='fa fa-facebook-square fa-2x'></span>
						<span class='fa fa-twitter-square fa-2x'></span>
						<span class='fa fa-google-plus-square fa-2x'></span>
					</div>
				<p class='blog-heading'>It’s OK to Answer Your Own Questions on Islamiqa</p>
				<p class='blog-text'>Writers often come to Islamiqa knowing what they want to write about, but don’t see a question asking about that exact subject. Often the best questions come from people who know the subject matter best, so it makes sense for these writers to pose the perfect question to set up their answer. In other words, it’s OK to ask a new question and then write an answer yourself. Note that this isn’t a policy change from before, but we’re re-emphasizing it today, and adding a few reminders across the product.</p>
				<p class='blog-text'>Why do this? You get to write about the exact thing that’s on your mind, rather than waiting for someone to ask the perfect question. Your answer gets all the benefits any answer does: distribution in feeds and digest emails to people who care about the topics, and access via search for future people who have that question.</p>
				<p class='blog-text'>Though this isn’t something that’s done often, it’s always been allowed and encouraged, so there are great examples of writers who knew their subject matter and answered their own question. You can ask and answer your own question when…</p>
				<p class='blog-b-text'>You don’t know the answer when you ask, but you learn it later and want to help others who have this question in the future</p>
				<p class='blog-text'>You want to organize and share your thoughts on a complex subject: Sandro Pasquali's answer to Are patents valuable?</p>
				<p class='blog-text'>You want to provide a helpful resource for something you know future people will ask: Judith Meyer's answer to What was the cost of the German reunification?</p>
				<p class='blog-text'>You don’t know the answer when you ask, but you learn it later and want to help others who have this question in the future: Kevin Peterson's answer to Can I run Java 8 lambdas in a Java 7 JVM?</p>
				<p class='blog-text'>We’re also adding a few small reminders about this in the product to make it clear this is ok: On the Write page, you’ll see a link to “Ask Your Own” question, which lets you ask a question then immediately begin writing an answer. You’ll also see a small banner on questions that you ask reminding you that it’s ok to answer the question.</p>
				</div>           
				
				<div class='col-sm-3 text-center'> 
				<div class='l-imb aisha'></div>
				<div class='r-ct'>
					<div class='s-name'>A'isha Mai</div>
					<div class='ssb'>
					<span class='fa fa-users'><span>43k</span></span>
					<span class='fa fa-flag'><span>23,800</span></span>
					</div>					
				</div>
				<p class='right-4-col-text'>Content Marketing Manager with Islamiqa. Manages content to improve the user experience through clear explanation - in written, visual and verbal formats. Former UX analyst and Interaction designer.</p>          
				<p class='blog-subtext'>Lastest Posts</p>
				<ul class='text-last-post'>
					<li>It’s OK to Answer Your Own Questions on Islamiqa</li>
					<li>Post 2</li>
					<li>New Site design and mobile App available</li>
					<li>Answers</li>
					<li>Topics</li>
					<li>Search</li>
					<li>Point System</li>
					<li>Privacy &amp; Security</li>
				</ul>
				</div>
		</div>        
    </div>
	</div>";
		return $html;
	
	
	}
}