<?php 

class Renderer_myinfo
{

    public function render($follow = null, $follower = null, $category = null, $mycategory = null) {
	
			$follow_html= $this->html_pagging_block($follow, 'follow', $isempty);
			$csshidefollow = ($isempty) ? "style='display:none'" : "";
			$cssarrowfollow = ($isempty) ? "down" : "up";
			
		    $follower_html= $this->html_pagging_block($follower,'follower', $isempty);
			$csshidefollower = ($isempty) ? "style='display:none'" : "";
			$cssarrowfollower = ($isempty) ? "down" : "up";
			
			$category_html = $this->html_list($category);
			$mycategory_html = $this->html_pagging_list($mycategory,$isempty);
			$csshidemycategory = ($isempty) ? "style='display:none'" : "";
			$cssarrowcategory= ($isempty) ? "down" : "up";
			
			$html= $this->html_dialog_edit();
			
			$html.="<div class='myinfo'>

					<!-- My Interest -->
					<div class='title'>
						<i class='fa fa-angle-{$cssarrowcategory} fa-lg fa-section'></i>
						 My Interests
					     <span class='close-widget multiple-icon'>
							<i class='fa fa-edit fa-lg' id='edit-myinfo' data-source='interest' aria-hidden='true'></i>
							<i class='fa fa-times fa-lg' aria-hidden='true'></i>
						 </span>
					</div>
					<div class='panel-section' {$csshidemycategory} >
					    {$mycategory_html}
					</div>

					<!-- I follow -->
					<div class='title'>
						<i class='fa fa-angle-{$cssarrowfollow} fa-lg fa-section'></i>
						 I Follow
					     <span class='close-widget multiple-icon'>
						   <i class='fa fa-edit fa-lg' id='edit-myinfo' data-source='follow' aria-hidden='true'></i>
						   <i class='fa fa-times fa-lg' aria-hidden='true'></i>
						  </span>
					</div>
					<div class='panel-section' {$csshidefollow}>
						{$follow_html}						
					</div>	
				     <!-- followers -->
					<div class='title'>
						<i class='fa fa-angle-{$cssarrowfollower} fa-lg fa-section'></i>
						 My Followers
					     <span class='close-widget multiple-icon'>
						   <i class='fa fa-edit fa-lg'  id='edit-myinfo' data-source='follower' aria-hidden='true'></i>
						   <i class='fa fa-times fa-lg' aria-hidden='true'></i>
						 </span>
					</div>
					<div class='panel-section' {$csshidefollower}>
						{$follower_html}						 
					</div>
					 <!-- categories -->
					 <div class='title'>
						<i class='fa fa-angle-up fa-lg fa-section'></i>
						 Categories
					     <span class='close-widget multiple-icon'>
						   &nbsp;
						   <i class='fa fa-times fa-lg' aria-hidden='true'></i>
						  </span>
					</div>
				    <div class='body-category panel-section'>
						<div class='line-page'>
						{$category_html}
						</div>	
					</div>
					
				   </div>";
			
			
			return $html;
	}
	
	private function html_pagging_block($data, $css, &$is_empty)
	{
	
	  /* <div class='item content'><div class='avatar'></div><div> User 1</div></div>
							 <div class='item content'><div class='avatar'></div><div> User 2</div></div>
							 <div class='item content'><div class='avatar'></div><div> User 3</div></div>
							 <div class='item content'><div class='avatar'></div><div> User 4</div></div>
							 <div class='item content'><div class='avatar'></div><div> User 5</div></div>
							 <div class='item content'><div class='avatar'></div><div> User 6</div></div>
							 <div class='item content'><div class='avatar'></div><div> User 7</div></div>
							 <div class='item content'><div class='avatar'></div><div> User 8</div></div>
							 <div class='item content'><div class='avatar'></div><div> User 9</div></div>*/
	  
	   $maxpage=1;	   
   	   $html = "<div class='body-{$css} pagging' >
				     <div class='block-page'>";
	   
	   if(is_array($data) && !empty($data))
	   {
		foreach($data as $value)
		{
			$user= $value['handle'];
			$avatar= qa_get_blob_url($value['avatarblobid'],true);
			$html.="<div class='item content'><div class='avatar' style='background-image:url({$avatar})'></div><div> {$user}</div></div>";
			
		}
		// Paging count
		$count=count($data);
		$maxpage = (int)  ($count < 4) ? 1 : ($count / 4);
		$is_empty = false;
	   } else {
	       $is_empty=true;
		   $html.= "(empty)";
	   }
	   
	   $html.="</div>
			  </div>
			<ol id='{$css}-pagin' class='selector' >
				<span class='prev'><i class='fa fa-caret-left fa-lg' aria-hidden='true'></i></span>
				<span class='current' data-maxpage='{$maxpage}' >1</span>
				<span class='next'><i class='fa fa-caret-right fa-lg' aria-hidden='true'></i></span>
			</ol>";

	   return $html;
	
	}
	
	
	private function html_list($data, &$is_empty = null)
	/**
	*
	*/
	{

/*			<div class='item'><label class='evtlist'>-</label> Science <span>(92)</span></div>
			<div class='evtsubitem'>
				<div class='subitem'>Deep<span>(92)</span></div>
				<div class='subitem'>More Deep<span>(92)</span></div>
			</div>	
			<div class='item'><label class='evtlist'>+</label>Culture <span>(192)</span></div>
			<div class='item'><label class='evtlist'>+</label>Politics <span>(912)</span></div>*/
		if(is_array($data) && !empty($data)) 
		{  
			$i=0;
			$html ="";
			$countData = count($data);
			
			while($i < $countData)
			{
			  $row=$data[$i];		  
			  $parentid=$row['parentid'];
					 
			  if($parentid == null)
			  {
				  $titlehref=str_replace(' ','-',$row['title']);
				  $html.="<div class='item'><label class='evtlist'>+</label><a href='./?qa={$titlehref}'>{$row['title']}</a><span>(".$row['qcount'].")</span></div>";
				  $i++;
			  } else {  
				 $html.="<div class='evtsubitem' style='display:none'>";
				 if(!empty($data[$i]['parentid'])) {
					while( !empty($data[$i]['parentid']) && $parentid == $data[$i]['parentid']) 
					{   $sublist=$data[$i];
						$titlehref=str_replace(' ','-',$sublist['title']);
						$html.= "<div class='subitem'><a href='./?qa={$titlehref}'>".$sublist['title']."</a><span>(".$sublist['qcount'].")</span></div>";
						$i++;
					}
				}
			    $html.="</div>";
			  } 
			  
			}
		     $is_empty = false;
		} else {
		     $is_empty = true;
			 $html="";
		}
		return $html;
	}
	
	
	private function html_pagging_list($data, &$is_empty = null)
	{
	
	/*<div class='body-interest pagging'>
							<div class='square-page'>
								<div class='content'>
								<div class='item'><div class='avatar'></div><div>interest 1</div></div>
								<div class='item'><div class='avatar'></div><div>interest 2</div></div>
								</div>
								<div class='content'>
								<div class='item'><div class='avatar'></div><div>interest 3</div></div>
								<div class='item'><div class='avatar'></div><div>interest 4</div></div>
								</div>
								<div class='content'>
								<div class='item'><div class='avatar'></div><div>interest 5</div></div>
								</div>
							</div>	
						</div>
						<ol id='interest-pagin' class='selector' >
						<span class='prev'><i class='fa fa-caret-left fa-lg' aria-hidden='true'></i></span>
							<span class='current' data-maxpage='2' >1</span>
							<span class='next'><i class='fa fa-caret-right fa-lg' aria-hidden='true'></i></span>
						</ol>*/
						
		$html="<div class='body-interest pagging'>
			   <div class='square-page'>";
				
		$maxpage=1;
		if(is_array($data) && !empty($data))
		{
			$i=0;
			$countData = count($data);
			while($i < $countData)
			{
			  $row=$data[$i];	
			  $image = (!empty($row['blobid'])) ? "style='background-image:url(".qa_get_blob_url($row['blobid'],true).")'" : "";
			  $html.="<div class='content'>";
			  $html.="<div class='item'><div class='avatar' {$image}></div><div>{$row['title']}</div></div>";
			  
			  if(++$i < $countData)
			  {   
			      $row=$data[$i];
				  $html.="<div class='item'><div class='avatar'></div><div>{$row['title']}</div></div>";
			  }
			  $i++;
			  $html.="</div>";
			}
			// Paging count
			$count=count($data);
			$maxpage = (int)  ($count < 4) ? 1 : ($count / 4);
			$is_empty = false;
		} else {
		    $is_empty = true;
			$html.="(empty)";
		}
		
		$html.="</div>
			  </div>
			<ol id='interest-pagin' class='selector' >
				<span class='prev'><i class='fa fa-caret-left fa-lg' aria-hidden='true'></i></span>
				<span class='current' data-maxpage='{$maxpage}' >1</span>
				<span class='next'><i class='fa fa-caret-right fa-lg' aria-hidden='true'></i></span>
			</ol>";
		return $html;
	
	}
	
	
	private function html_dialog_edit()
	{
		
	  $html="<div id='myinfo-edit-dialog' class='myinfo-edit-css' style='display:none;'>
				<div id='myinfo-edit'>
				<ul>
				   <li><div><a href='#tab-i-follow'>I Follow</a></div></i>
				   <li><div><a href='#tab-my-follower'>My Followers</a></div></i>
				   <li><div><a href='#tab-my-interest'>My Interests</a></div></i>
				   <li><div><a href='#tab-my-knowledge'>My Knowledge</a></div></i>
				</ul>
				<span id='tab-search'><input type='text' maxlength='100'><button><i class='fa fa-search fa-lg'></i></button> </span>
				<div id='tab-i-follow'>
				  <div class='follow_userlist'>
				  <!-- User -->
				  <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				   
					<!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				   
				   <!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				   
				   <!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				   </div>
				   <button class='loadmore' data-origin='follow' >Load More</button>
				</div>
				<div id='tab-my-follower'>
				  <div class='follower_userlist'>
					<!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				   
				    <!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				   
				    <!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				   
				    <!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				    <!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				   
				    <!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				   
				    <!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>
				   
				    <!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner'></div>
					 </div>					
					 <div> 
							<div class='title-user'><i class='fa fa-star fa-yellow'></i>User</div>
							<span class='follow-user'><i class='fa fa-group'></i>2212</span>
							<span class='flag-user'><i class='fa fa-flag'></i>2222</span>
					 </div>
					 <button class='unfollow'>Unfollow</button>
				   </div>				   
				   </div>
				   
				   <button class='loadmore' data-origin='follower' >Load More</button>
				</div>
				<div id='tab-my-interest'>
				
				</div>
				<div id='tab-my-knowledge'>
				
				</div>	  
		   </div>
	   </div>";	
		
      return $html;	
	}
	
}
		