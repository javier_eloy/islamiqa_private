<?php 

/// Created by: Javier Hernandez
/// Date: 19/09/2016

class Renderer_account
{

    public function render($follow_count, $follow_objective,
						   $picktopic_count, $picktopic_objective, 
						   $is_usericon, $is_expertise, $is_workat) {
		
		
	 $follow_accomplish=($follow_count >= $follow_objective)  ? "check-setup" : "uncheck-setup";
	 $picktopic_accomplish=($picktopic_count >= $picktopic_objective) ? "check-setup" : "uncheck-setup";
	 $usericon_accomplish=($is_usericon) ? "check-setup" : "uncheck-setup";
	 $expertise_accomplish=($is_expertise) ? "check-setup" : "uncheck-setup";
	 $worksat_accomplish=($is_workat) ? "check-setup" : "uncheck-setup";
	 
	 $html="<div class='account-setup'>						
				<div class='account-title'>
				  <i class='fa fa-angle-up fa-lg fa-section'></i>
				  Account Setup
				  <span class='close-widget'><i class='fa fa-times fa-lg'></i></span>
				</div>
				<div class='panel-section panel-setup line-bottom-red'> <!-- panel -->
				<div class='row item-account'>
					<div class='col-sm-2 col-md-2'>
					 <div class='check-status {$picktopic_accomplish}'><i class='fa fa-check fa-lg'></i></div>
					</div>
					<div class='col-sm-10 col-md-10 item-account-content'>
					  <p>Pick at least {$picktopic_objective} interesting topics</p>
					</div>
				</div>

				<div class='row item-account'>
					<div class='col-sm-2 col-md-2'>
					 <div class='check-status {$expertise_accomplish}'><i class='fa fa-check fa-lg'></i></div>
					</div>
					<div class='col-sm-10 col-md-10 item-account-content'>
					  <p>Indicate your expertise</p>
					</div>
				</div>
				
				<div class='row item-account'>
					<div class='col-sm-2 col-md-2'>
					 <div class='check-status {$follow_accomplish}'><i class='fa fa-check fa-lg'></i></div>
					</div>
					<div class='col-sm-10 col-md-10 item-account-content'>
					  <p>Follow {$follow_objective} users</p>
					</div>
				</div>

				<div class='row item-account'>
					<div class='col-sm-2 col-md-2'>
					 <div class='check-status {$usericon_accomplish}'><i class='fa fa-check fa-lg'></i></div>
					</div>
					<div class='col-sm-10 col-md-10 item-account-content'>
					  <p>Set an user icon</p>
					</div>
				</div>
				
				<div class='row item-account'>
					<div class='col-sm-2 col-md-2'>
					 <div class='check-status {$worksat_accomplish}'><i class='fa fa-check fa-lg'></i></div>
					</div>
					<div class='col-sm-10 col-md-10 item-account-content'>
					  <p>Where have you worked at?</p>
					</div>
				</div>				
			 </div> <!-- Panel -->
		  </div>";
		return $html;
		
	}
}