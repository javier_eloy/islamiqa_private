<?php
	

class Renderer_home
{
  var $rooturl;
  
  function render($context,$rooturl) 
  {
        $this->rooturl=$rooturl;
		
       ob_start();
	   $this->landing_page_main_slider($context);
       $context->output('<section class="question_answer_section">');
       // Most Popular Section
       //$context->output('</br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br>');
       $this->landing_page_most_popular_header($context);
       $this->landing_page_most_popular_structure($context);

       // Recently Asked Question  Section 
       $this->landing_page_recently_asked_header($context);
       $this->landing_page_recently_asked_structure($context);

      // All Question  Section 
      $this->landing_page_all_questions_header($context);
      $this->landing_page_all_questions_structure($context);

      // Unanswered  Question  Section 
      $this->landing_page_unanswered_header($context);
      $this->landing_page_unanswered_structure($context); 

      $context->output('</section>');
	  
	  $html=ob_get_contents();
	  ob_end_clean();
	  
	  return $html;
  }

  function landing_page_main_slider($context)
	/**
	*
	*/
	{
    $context->output('
	<section class="main_slider">
	<div class="container">
		<div class="row">
			<div class="inner">
				<h1>Questions worth answering...</h1>
				<h3>For those with an interest in Islam and its peoples</h3>
			</div>
		</div>
		<div class="row">
			<div class="form-group ask_bar">
<!--				<form role="search" method="post" action="' . qa_path_to_root() . '?qa=ask"> -->
					<input type="text" name="title" id="title" onchange="qa_title_change(this.value);" placeholder="ask a new question or search for exists questions" class="form-control">
					<button class="btn btn-primary" type="button" id="asknew">Ask Question</button>
<!--				</form>	-->
			</div>
		</div>
	</div>
	<div class="new_question" id="new_question">
		<span class="iconclose"><i class="fa fa-close fa-lg"></i></span>
		<div class="newq_title">
			<p class="newq_subtitle_post">Title</p>
			<input type="text" class="newq_title" id="newq_title">
			<span class="newq_link fa fa-pencil fa-lg"></span>
		</div>
		<p class="newq_subtitle">Augment your question</p>

		<div class="newq_text_div" style="display:inline-block;">
			<div class="newq_text" id="newq_text">
				<textarea id="newq_content" rows="4" cols="70"></textarea>
				<span class="newq_link fa fa-link fa-lg"></span>
				<span class="newq_image fa fa-image fa-lg"></span>
			</div>
        </div>

		<div class="topics">
        	<span class="six-li-icon"><i class="fa fa-tags fa-lg"></i></span>
        	<li class="newq_topics">Topics </li>
            <input type="text" class="topics_input" name="tags" id="tags" onkeyup="qa_tag_hints();" onmouseup="qa_tag_hints();" value="" placeholder="Use hyphens to combine words"></input>
        </div>

        <div class="qa-form-tall-note">
        	<span id="tag_examples_title" style="display:none;">Example tags: </span>
        	<span id="tag_complete_title" style="display:none;">Matching tags: </span>
        	<span id="tag_hints"></span>
        </div>

		<p class="newq_subtitle">Categories *</p>
		<fieldset id="checkArray">
			<div class="col-xs-12 col-sm-12 col-lg-12" id="categories">
				<div class="col-xs-3 col-sm-3 col-lg-3">
					<input type="radio" name="cat[]" class="magic-radio" id="cat01" value="5"><label for="cat01" class="first-span">Science</label>
					<input type="radio" name="cat[]" class="magic-radio" id="cat02" value="4"><label for="cat02" class="first-span">History</label>
					<input type="radio" name="cat[]" class="magic-radio" id="cat03" value="2"><label for="cat03" class="first-span">Culture</label>
				</div>
				<div class="col-xs-3 col-sm-3 col-lg-3">
  					<input type="radio" name="cat[]" class="magic-radio" id="cat04" value="6"><label for="cat04" class="first-span">Politics</label>
  					<input type="radio" name="cat[]" class="magic-radio" id="cat05" value="10"><label for="cat05" class="first-span">Organizations</label>
  					<input type="radio" name="cat[]" class="magic-radio" id="cat06" value="23"><label for="cat06" class="first-span">Fiqh</label>
  				</div>
  				<div class="col-xs-3 col-sm-3 col-lg-3">
  					<input type="radio" name="cat[]" class="magic-radio" id="cat07" value="7"><label for="cat07" class="first-span">People</label>
  					<input type="radio" name="cat[]" class="magic-radio" id="cat08" value="8"><label for="cat08" class="first-span">Places</label>
  					<input type="radio" name="cat[]" class="magic-radio" id="cat09" value="9"><label for="cat09" class="first-span">Languages</label>
  				</div>
  				<div class="col-xs-3 col-sm-3 col-lg-3">
  					<input type="radio" name="cat[]" class="magic-radio" id="cat10" value="1"><label for="cat10" class="first-span">Beliefs</label>
  					<input type="radio" name="cat[]" class="magic-radio" id="cat11" value="11"><label for="cat11" class="first-span">Movements</label>
  					<input type="radio" name="cat[]" class="magic-radio" id="cat12" value="3"><label for="cat12" class="first-span">Family</label>
				</div>
			</div>
		</fieldset>

			<p class="newq_subtitle">Header Images</p>
			<fieldset>
				<div>
					<span class="newq_img_items" style="vertical-align:middle;"><i class="fa fa-image fa-2x" style="color:#cf2129;"></i></span>
					<span class="newq_img_items">
						<input type="button" id="get_file1" value="Upload">
						<input type="file" id="my_file1">
						<div id="customfileupload1"></div>
					</span>
					<span class="newq_img_items"><li class="cat-li"><p style="color:#cf2129;">or</p></li></span>
					<span class="newq_img_items">
						<input type="button" id="get_file2" value="Pick from our gallery">
						<input type="file" id="my_file2">
						<div id="customfileupload2"></div>
					</span>
				</div>
			</fieldset>
			
				<p class="newq_subtitle">Settings</p>
				<div>
  					<span class="inline"><input type="checkbox" name="set[]" class="magic-check" id="set01" value="answer"><label for="set01" class="first-span">Allow anonymous answers</label></span>
  					<span class="inline"><input type="checkbox" name="set[]" class="magic-check" id="set02" value="email"><label for="set02" class="first-span">Enable E-Mail notifications</label></span>
  					<span class="inline"><input type="checkbox" name="set[]" class="magic-check" id="set03" value="post"><label for="set03" class="first-span">Post Anonymously</label></span>
				</div>');
				
				//$ref = str_replace($context->rooturl, "?qa=", $context->rooturl) . $most_sectionquery[1]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[1]["title"]);

				$context->output('
<!--				<form method="post" action="">	-->
					<button type="button" onclick="NewQuestion();" class="postnew" id="postnew">Post my question</button>
<!--				</form>	-->
					<p class="login_message" id="login_message"></p>
			</div>
		</section>');
		
		
	}
		
		
		
	function landing_page_most_popular_header($context)
	/**
	*
	*/
	{
    require_once QA_INCLUDE_DIR . 'db/selects.php';
    $categories = qa_db_select_with_pending(qa_db_category_nav_selectspec(null, true));

    $context->output('<div class="qa_gallery first most_popular">
	<!-- ** START QUESTIONS & ANSWERS FILTER ** -->
    <div class="container">
	<div class="row">
		<div class="qa_filter clearfix">
			<div class="col-md-5 col-xs-7">
				<ul class="type_filter clearfix">
					<li id="most_popular_fad">
					<a href="javascript:void(0);"><i class="fa fa-caret-up most_popular_fad_up font-size-30"></i></a><a>
					</a><a href="javascript:void(0);"><i class="fa fa-caret-down most_popular_fad_down font-size-30"></i></a>
					</li>
					<li>
						<div>
						<a onclick="mostpopular();" class="active_section" id="most-sectiontitle">Most Popular</a>
						</div>
					</li>
				</ul>
			</div>
  
  			<div class="col-md-3 col-xs-5">
				<ul class="type_filter clearfix">
					
					<li><a onclick="mostweek();" id="most-thisweeklink">This Week</a></li>
					<li><a onclick="mostmonth();" id="most-thismonthlink">This Month</a></li>
					<li><a onclick="mostyear();" id="most-thisyearlink">This Year</a></li>
				</ul>
			</div>

			<div class="col-md-4 col-xs-6">
				<ul class="filter clearfix">
					<li>
						<span>View</span>
						<a class="view_icons">
						<span id="most-view-list"><i class="fa fa-align-right"></i></span>
						<span class="icon red-active" id="most-view-grid"><i class="fa fa-th-large"></i></span>
						</a>
					</li>
					<li>
						<div class="dropdown">
							<button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" id="dropdownMenu3" type="button" class="btn btn-default dropdown-toggle">
								Categories <span class="caret"></span>
							</button>
							<ul aria-labelledby="dropdownMenu3" class="dropdown-menu">
								');
                                foreach ($categories as $category)
                                    $context->output('<li ><a onClick="mostcategory('.$category["categoryid"].');" class="mp_cat" cat_id="' . $category["categoryid"] . '" >' . $category["title"] . ' </a ></li>');
			$context->output('</ul>
						</div>
					</li>
				</ul>
			</div>
                        <div class="col-xs-12 visible-xs">
				<ul class="type_filter clearfix type_filter_custom">
					<li><a href="javascript:void(0);"><i class="fa fa-angle-left"></i></a></li>
					<li class="hidden-xs"><a href="javascript:void(0);" class="active_section">Most Popular</a></li>
					<li><a href="javascript:void(0);">This Week</a></li>
					<li><a href="javascript:void(0);">This Month</a></li>
					<li><a href="javascript:void(0);">This Year</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-right"></i></a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
			</div>
		</div>');
		}
		
		
    function landing_page_most_popular_structure($context) 
	/**
	*
	*/
	{

	$local_index = 0;
	$listview_mostindex = $_SESSION['listview_mostindex'];

// In the call of the ajax functions we can pass the $context object to set the image background for questions div -->
    $context->output('
	<!-- ** GRID VIEW STRUCTURE MOST VIEW ** -->
	<div id="most_popular_start">
    <div id="mostpopular_grid">
    <div class="container popular_answer_wrapper">
    <div id="mostpopular_caresol">
	<!-- In the called of the ajax functions we can pass the $context object to set the image background for questions div -->
	<a onclick="arrowleft_mostpopular();" class="left-arrow" id="left"><i class="fa fa-angle-left"></i></a>
	<a onclick="arrowright_mostpopular();" class="right-arrow" id="right"><i class="fa fa-angle-right"></i></a>
        <div class="row" id="most_first_five">');
    
    $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid  WHERE type='Q' ORDER BY netvotes DESC, upvotes DESC LIMIT " . listview_questions));
    $most_countque = sizeof($most_sectionquery);
    
    $mostQueryans = array();
    for ($i=0; $i < $most_countque; $i++)
    {
		$mostQueryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));

        if ($mostQueryans[$i] != NULL) {
            //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
            $countans[$i] = sizeof($mostQueryans[$i]);
        }
        else {
            $countans[$i] = 0;
        }
    }
    
    $context->output('<div class="col-md-6">
				<div class="row">
					<div class="gallery_four_thumb">
                        <div class="clearfix">
							<div class="thumb_half">
								<div class="thumb_hover half">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">' . calculate_weeks_ago($most_sectionquery[0]["created"]) . ' w ago</span>
												<span class="topic right">'.$most_sectionquery[0]["cattitle"].'</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line bounceInUp">
												<div class="h6">');

    								//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
									
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);
									
									$context->output('
                                            <a href="' . $ref . '">' . $most_sectionquery[0]["title"] . '</a>
                                            </div>
                                            <div class="ans">');

                                    //start answer section                                               
                                    	if ($mostQueryans[0] != NULL) {
                                        	$context->output('Ans: '.substr($mostQueryans[0][0]['content'],0,100));
                                        }
                                        else {
                                        	$context->output('No Answer');
                                        }
                                    //end ansewr section
                                                                                                 
                                               	$context->output('
                                                </div>
                                                <div class="toolbar">
													<div class="tools left">
														<ul>
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
														<ul class="list-unstyled pull-left">
                                                            <li>
                                                                <a onClick="addVote('.$most_sectionquery[0]['postid'].');">
                                                                    <i class="fa fa-angle-double-up"></i>
                                                                    <span id=span-'.$most_sectionquery[0]['postid'].'> '.$most_sectionquery[0]['upvotes'].'</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onClick="downVote('.$most_sectionquery[0]['postid'].');">
                                                                    <i class="fa fa-angle-double-down"></i>
                                                                    <span id=span-'.$most_sectionquery[0]['postid'].'-downvote> '.$most_sectionquery[0]['downvotes'].'</span>
                                                                </a>
                                                            </li>
                                                		</ul>
													</div>
													<div class="share right">
														<ul>');
														
														if ($countans[0] > 1) {
															$ans = " answers";
														}
														else {
															$ans = " answer";
														}
														$context->output('
                                    						<li>' . $countans[0] . $ans . '</li>
                                    						<li>
                                        						<div class="share right">
																<ul>
																	<li>
                                                        			<div class="tooltip-custom">
                                                         				<i class="fa fa-ellipsis-h"></i>
                                                            			<div class="tooltip-inner tooltip-inner2">
                                                                			<a href="#">Invite authoritative user to answer</a></br>
                                                        					<a href="#">Add to Reading List</a></br>
                                                        					<a href="#">Show Related Questions</a></br>
                                                        					<a href="#">Create Summary Answer</a></br>
                                                        					<a href="#">Mark Favorite</a></br>
                                                        					<a href="#">Report</a></br>
                                                            			</div>
                                                         			</div>
                                                 					</li>
																</ul>
																</div>
                                        					</li>
                                    					</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[0]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							//Here the code to select the plain color to set in the background question
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}
						$local_index += 1;

							$context->output('
							  </div>
							</div>
							<div class="thumb_half">
								<div class="thumb_hover half">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">' . calculate_weeks_ago($most_sectionquery[1]["created"]) . ' w ago</span>
												<span class="topic right">'.$most_sectionquery[1]["cattitle"].'</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line bounceInUp">
												<div class="h6">');

												//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    											$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[1]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[1]["title"]);

												$context->output('                                      
                                                	<a href="' . $ref . '">' . $most_sectionquery[1]["title"] . '</a>
                                                    </div>
                                                    <div class="ans">');

                                                //start answer section                                                
                                                    if ($mostQueryans[1] != NULL) {
                                                     $context->output('Ans: '.substr($mostQueryans[1][0]['content'],0,100));
                                                    }
                                                    else {
                                                     $context->output('No Answer');
                                                    }
                                                //end ansewr section
                                                                                                 
                                               $context->output('
                                               	</div>
                                                <div class="toolbar">
													<div class="tools left">
														<ul>
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
                                                        <ul class="list-unstyled pull-left">
                                                            <li>
                                                                <a onClick="addVote('.$most_sectionquery[1]['postid'].');">
                                                                    <i class="fa fa-angle-double-up"></i>
                                                                    <span id=span-'.$most_sectionquery[1]['postid'].'> '.$most_sectionquery[1]['upvotes'].'</span>
                                                                </a>

                                                            </li>
                                                            <li>
                                                                <a onClick="downVote('.$most_sectionquery[1]['postid'].');">
                                                                    <i class="fa fa-angle-double-down"></i>
                                                                    <span id=span-'.$most_sectionquery[1]['postid'].'-downvote> '.$most_sectionquery[1]['downvotes'].'</span>
                                                                </a>
                                                            </li>
                                                    	</ul>
													</div>
													<div class="share right">
														<ul>');
														
														if ($countans[1] > 1) {
															$ans = " answers";
														}
														else {
															$ans = " answer";
														}
														$context->output('
                                    						<li>' . $countans[1] . $ans . '</li>
                                        					<li>
                                        						<div class="share right">
																<ul>
																	<li>
                                                        			<div class="tooltip-custom">
                                                         				<i class="fa fa-ellipsis-h"></i>
                                                            			<div class="tooltip-inner tooltip-inner2">
                                                                			<a href="#">Invite authoritative user to answer</a></br>
                                                        					<a href="#">Add to Reading List</a></br>
                                                        					<a href="#">Show Related Questions</a></br>
                                                        					<a href="#">Create Summary Answer</a></br>
                                                        					<a href="#">Mark Favorite</a></br>
                                                        					<a href="#">Report</a></br>
                                                            			</div>
                                                         			</div>
                                                 					</li>
																</ul>
																</div>
                                        					</li>
                                    					</ul>
													</div>													
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[1]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME. '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');
						
						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							//Here the code to select the plain color to set in the background question
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

							$context->output('
								</div>
							</div>
						</div>

						<div class="thumb_full">
							<div class="thumb_hover full">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">' . calculate_weeks_ago($most_sectionquery[2]["created"]) . ' w ago</span>
											<span class="topic right">'.$most_sectionquery[2]["cattitle"].'</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line bounceInUp">
											<div class="h5">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[2]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[2]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[2]["title"] . '</a>
                                            </div>
                                            <div class="ans">');

                                //start answer section                                    
                                    if ($mostQueryans[2] != NULL) {
                                    	$context->output('Ans: '.substr($mostQueryans[2][0]['content'],0,300));
                                    }
                                    else {
                                    	$context->output('No Answer');
                                    }
                                //end answer section
                                                                                                 
                          					$context->output('</div>
                          						<div class="toolbar">
												<div class="tools left">
													<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                                            <ul class="list-unstyled pull-left">
                                                                <li><a onClick="addVote(' . $most_sectionquery[2]['postid'] . ');">
                                                                    <i class="fa fa-angle-double-up"></i>
                                                                    <span id=span-' . $most_sectionquery[2]['postid'] . '> ' . $most_sectionquery[2]['upvotes'] . '</span>
                                                                    </a>
																</li>
                                                                <li><a onClick="downVote(' . $most_sectionquery[2]['postid'] . ');">
                                                                    <i class="fa fa-angle-double-down"></i>
                                                                    <span id=span-' . $most_sectionquery[2]['postid'] . '-downvote> ' . $most_sectionquery[2]['downvotes'] . '</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
														<li><a href="javascript:void(0);">3k views</a></li>
														<li><a href="javascript:void(0);">235 comments</a></li>
														<li>															
                                                        	<div class="tooltip-social">
                                                         		<a href="#">Share</a>
                                                            	<div class="tooltip-inner tooltip-inner3">
                                                            		<div class="social_icon facebook_icon">
                                                                		<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                	</div>
                                                                	<div class="social_icon twitter_icon">
                                                        				<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        			</div>
                                                        			<div class="social_icon google_icon">
                                                        				<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        			</div>
                                                        			<div class="social_icon instagram_icon">
                                                        				<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        			</div>
                                                            	</div>
                                                         	</div>                                                 				
														</li>
													</ul>
												</div>
												<div class="share right">
													<ul>');
														
														if ($countans[2] > 1) {
															$ans = " answers";
														}
														else {
															$ans = " answer";
														}
														$context->output('
                                    					<li>' . $countans[2] . $ans . '</li>
														<li>
                                                        <div class="tooltip-custom">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                            <div class="tooltip-inner tooltip-inner2">
                                                                <a href="#">Invite authoritative user to answer</a></br>
                                                        		<a href="#">Add to Reading List</a></br>
                                                        		<a href="#">Show Related Questions</a></br>
                                                        		<a href="#">Create Summary Answer</a></br>
                                                        		<a href="#">Mark Favorite</a></br>
                                                        		<a href="#">Report</a></br>
                                                            </div>
                                                         </div>
                                                 		</li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[2]['postid'];

						//The next line is to complete the image's path		
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

							$context->output('
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row">
					<div class="thumb_full_single_right">
						<div class="thumb_hover full_single">
							<div class="hover_overlay">
								<div class="inner">
									<div class="top_line">
										<span class="time left">' . calculate_weeks_ago($most_sectionquery[3]["created"]) . ' w ago</span>
										<span class="topic right">' . $most_sectionquery[3]["cattitle"] . '</span>
										<div class="clearfix"></div>
									</div>
									<div class="bottom_line bounceInUp">
										<div class="h5">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[3]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);

									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[3]["title"] . '</a>
                                            </div><div class="ans">');

                                //start answer section                                    
                                    if ($mostQueryans[3] != NULL) {
                                    	$context->output('Ans: '.substr($mostQueryans[3][0]['content'],0,300));
                                    }
                                    else {
                                    	$context->output('No Answer');
                                    }
                                //end ansewr section
                                                                                                 
                      			$context->output('
                      				</div>
                      					<div class="toolbar">
											<div class="tools left">
												<ul>');

                                            $context->output('
													<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
													<ul class="list-unstyled pull-left">
                                                        <li><a onClick="addVote('.$most_sectionquery[3]['postid'].');">
                                                         <i class="fa fa-angle-double-up"></i>
                                                            <span id=span-'.$most_sectionquery[3]['postid'].'> '.$most_sectionquery[3]['upvotes'].'</span>
                                                            </a>
                                                        </li>
                                                        <li><a onClick="downVote('.$most_sectionquery[3]['postid'].');">
                                                            <i class="fa fa-angle-double-down"></i>
                                                            <span id=span-'.$most_sectionquery[3]['postid'].'-downvote> '.$most_sectionquery[3]['downvotes'].'</span>
                                                         </a>
                                                        </li>
                                                    </ul>
                                                    <li><a href="javascript:void(0);">3k views</a></li>
													<li><a href="javascript:void(0);">235 comments</a></li>
													<li>															
                                                        <div class="tooltip-social">
                                                         	<a href="#">Share</a>
                                                            <div class="tooltip-inner tooltip-inner3">
                                                            	<div class="social_icon facebook_icon">
                                                                	<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                            </div>
                                                         </div>                                                 				
													</li>
												</ul>
											</div>
											<div class="share right">
												<ul>');
														
												if ($countans[3] > 1) {
													$ans = " answers";
												}
												else {
													$ans = " answer";
												}
												$context->output('
                                    				<li>' . $countans[3] . $ans . '</li>
													<li>
                                              		<div class="tooltip-custom">
                                           	 			<i class="fa fa-ellipsis-h"></i>
                                                    	<div class="tooltip-inner tooltip-inner2">
                                                        	<a href="#">Invite authoritative user to answer</a></br>
                                                        	<a href="#">Add to Reading List</a></br>
                                                        	<a href="#">Show Related Questions</a></br>
                                                        	<a href="#">Create Summary Answer</a></br>
                                                        	<a href="#">Mark Favorite</a></br>
                                                        	<a href="#">Report</a></br>
                                                    	</div>
                                                 	</div>
                                                 	</li>
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[3]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

						$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>
								-->
						</div>
					</div>
				</div>
			</div>
		</div>');

	$context->output("<input type='hidden' value='" . $_SESSION['most_slide_index'] . "' id='most_slide_index'>");
	$context->output("<input type='hidden' value='" . $_SESSION['most_total_slides'] . "' id='most_total_slides'>");						
                     

	$context->output('	
	</div></div></div></div>');

	$context->output('<div id="mostpopular_table">
					<div id="allthis-week-res">
						<div id="allthis-week">');

	$i=0;
    foreach ($most_sectionquery as $list)
    {
                $context->output('
                     <!-- ** LIST VIEW STRUCTURE MOST VIEW ** -->
                    <div class="container">
                        <div class="table-responsive">
                            <table class="table table-striped custom_table">
                                <tbody>
                                    <tr>
                                        <td style="width:5%;">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a onClick="addVote('.$list['postid'].');">
                                                        <i class="fa fa-angle-double-up"></i>
                                                        <span id=span-'.$list['postid'].'> '.$list['upvotes'].'</span>
                                                    </a>
                                                </li>
                                                <li>
                                                     <a onClick="downVote('.$list['postid'].');">
                                                        <i class="fa fa-angle-double-down"></i>
                                                        <span id=span-'.$list['postid'].'-downvote> '.$list['downvotes'].'</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </td>

                                        <td colspan="4" style="width:70%;">
                                        <h3 style="padding:0; margin:0;">');

										//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    									$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $list["postid"] . "/" . preg_replace("/[\s_]/", "-", $list["title"]);

                                        $context->output('<a href="' . $ref . '" style="color:black;">' . $list['title'] . '</a></h3>
                                            <p style="margin:0 0 5px 0; font-size:90%;">asked <strong>' . date("M d", strtotime($list['created'])) . '</strong> in <strong>' . $list['cattitle'] . '</strong> </p>
                                            <p id="more" style="padding:0; margin:0;">
                                                <span class=more>');

                        //start answer section
                                if ($mostQueryans[$i] != NULL) {
                                	$context->output('Last Answer: ' . $mostQueryans[$i][0]['content']);
                                }
                                else {
                                    $context->output('No Answer');
                                }
                        //end answer section
                                
                    $context->output('</span></p>');

                    $relatedqlink = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/ask?follow=' . $list['postid'] . '&cat=' . $list['categoryid'];
/*
                    start section username
                    $fetchusername = qa_db_read_all_assoc(qa_db_query_sub("select handle from qa_users where userid='".$list['userid']."' "));
         
                    foreach ($fetchusername as $userlist) {
                    	$context->output('by <strong>' . $userlist['handle'] . '</strong>');
                    }                               
                    end section username
*/
                    
                    		$context->output('</p>
                                            <ul class="list-inline">
                                                    <li class="active">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-pencil"></i>
                                                            <span>                                                           
                                                               <a href=' . $ref . '>Answer</a>
                                                             </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                                <i class="fa fa-bars"></i>
                                                            <span>
                                                                <a href=' . $relatedqlink . '>Ask related question</a>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <div class="tooltip-social">
                                            				<a href="javascript:void(0);">Share</a>
                                                			<div class="tooltip-inner">
                                                    			<div class="social_icon facebook_icon">
                                                                	<a href="javascript:void(0);"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="javascript:void(0);"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="javascript:void(0);"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="javascript:void(0);"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                			</div>
                                        				</div>
													</li>
                                            	</ul>
                                    </td>
                                    <td style="width:10%;">
                                            ' . ShowWantAnswer($countans[$i], $list['postid'], $ref) . '
                                        <div class="tooltip-social">
                                            <i class="fa fa-ellipsis-h"></i>
                                                <div class="tooltip-inner">
                                                    <a href="#">Invite authoritative user to answer</a><br>
                                                    <a href="#">Add to Reading List</a><br>
                                                    <a href="#">Show Related Questions</a><br>
                                                    <a href="#">Create Summary Answer</a><br>
                                                    <a href="#">Mark Favorite</a><br>
                                                    <a href="#">Report</a>
                                                </div>
                                        </div>
                                    </td>
                                    <td style="width:10%;">
                                        <a href="javascript:void(0)" class="btn btn-red">
                                            <i class="fa fa-circle"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
                </div>');

			$i+=1;
    }

    $most_countque = listview_questions;
    $most_listview_firsttime = $_SESSION['most_listview_firsttime'];

	$context->output('<input type="hidden" id="most_countque" value="' . $most_countque . '">
                        <input type="hidden" id="most_index" value="' . $listview_mostindex . '">
                        <input type="hidden" id="most_listview_firsttime" value="' . $most_listview_firsttime . '">');

	$context->output('</div></div></div>');

	$context->output('<div class="more_questions" id="more_questions_most"><p class="more_text">To see more questions, click 
    					<a class="questions" id="more_link_most" onClick="more_questions_listview_most();" href="javascript:void(0);"><b class="red">here.</b></a></p>
    				  </div>');

	$context->output('</div>');
 }
 
 
   function landing_page_recently_asked_header($context) 
  /**
  *
  */
  {
    require_once QA_INCLUDE_DIR . 'db/selects.php';
    $categories = qa_db_select_with_pending(qa_db_category_nav_selectspec(null, true));

    $context->output('<div class="qa_gallery first recently_ask">
	<!-- ** START QUESTIONS & ANSWERS FILTER ** -->
	<div class="container">
	<div class="row">
		<div class="qa_filter clearfix">
			<div class="col-md-5 col-xs-7">
				<ul class="type_filter clearfix">
					<li id="recently_fad">
					<a href="javascript:void(0);"><i class="fa fa-caret-up recently_fad_up font-size-30"></i></a>
					<a href="javascript:void(0);"><i class="fa fa-caret-down recently_fad_down font-size-30"></i></a>
					</li>
					<li>
						<div>	
						<a onclick="recentlyask();" class="active_section" id="recent-sectiontitle">Recently Asked</a>
						</div>
					</li>
				</ul>
			</div>
  
  			<div class="col-md-3 col-xs-5">
				<ul class="type_filter clearfix">
						
					<li class="hidden-xs"><a onclick="recentlyweek();" id="recent-thisweeklink">This Week</a></li>
					<li class="hidden-xs"><a onclick="recentlymonth();" id="recent-thismonthlink">This Month</a></li>
					<li class="hidden-xs"><a onclick="recentlyyear();" id="recent-thisyearlink">This Year</a></li>
				</ul>
			</div>

			<div class="col-md-4 col-xs-6">
				<ul class="filter clearfix">
					<li>
					<span>View</span>
					<a class="view_icons">
					<span id="recently-view-list"><i class="fa fa-align-right"></i></span> 
					<span class="icon red-active" id="recently-view-grid"><i class="fa fa-th-large"></i></span> 
					</a></li>
					<li>
						<div class="dropdown">
							<button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" id="dropdownMenu3" type="button" class="btn btn-default dropdown-toggle">
								Categories <span class="caret"></span>
							</button>
							<ul aria-labelledby="dropdownMenu3" class="dropdown-menu">
								');
    foreach ($categories as $category)
        $context->output('<li ><a onClick="recentlycategory('.$category["categoryid"].');" class="mp_cat" cat_id="' . $category["categoryid"] . '" >' . $category["title"] . ' </a></li>');

    $context->output('</ul>
						</div>
					</li>
				</ul>
			</div>
                        <div class="col-xs-12 visible-xs">
				<ul class="type_filter clearfix type_filter_custom">
					<li><a href="javascript:void(0);"><i class="fa fa-angle-left"></i></a></li>
					<li class="hidden-xs"><a href="javascript:void(0);" class="active_section">Most Popular</a></li>
					<li><a href="javascript:void(0);">This Week</a></li>
					<li><a href="javascript:void(0);">This Month</a></li>
					<li><a href="javascript:void(0);">This Year</a></li>
                                        <li><a href="javascript:void(0);"><i class="fa fa-angle-right"></i></a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>');
}

	
	function landing_page_recently_asked_structure($context)
	/**
	*
	*/
	{
	$local_index = 0;
	$listview_recentindex = $_SESSION['listview_recentindex'];

    $context->output('
<!-- ** GRID VIEW STRUCTURE MOST VIEW ** -->
<div id="recently_start">
    <div id="recently_grid">
    <div class="container popular_answer_wrapper">
	<a onclick="arrowleft_recently();" class="left-arrow" id="left-recently"><i class="fa fa-angle-left"></i></a>
	<a onclick="arrowright_recently();" class="right-arrow" id="right-recently"><i class="fa fa-angle-right"></i></a>
        <div class="row" id="recently_first_five">');
    
    $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created DESC LIMIT " . listview_questions));
    $most_countque = sizeof($most_sectionquery);
    
    $mostQueryans = array();
    for ($i=0; $i < $most_countque; $i++)
    {
		$mostQueryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
		
        if ($mostQueryans[$i] != NULL) {
            $countans[$i] = sizeof($mostQueryans[$i]);
            //$most_countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
        }
        else {
            $countans[$i] = 0;
        }
    }
    
    $context->output('<div class="col-md-6">
				<div class="row">
					<div class="gallery_four_thumb">
                        <div class="clearfix">
							<div class="thumb_half">
								<div class="thumb_hover half">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">' . calculate_weeks_ago($most_sectionquery[0]["created"]) . ' w ago</span>
												<span class="topic right">'.$most_sectionquery[0]["cattitle"].'</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line bounceInUp">
												<div class="h6">');

    								//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);
									
									$context->output('<a href="' . $ref . '">' . $most_sectionquery[0]["title"] . '</a></div><div class="ans">');

                                //start answer section                                    
                                    if ($mostQueryans[0] != NULL) {
                                    	$context->output('Ans: '.substr($mostQueryans[0][0]['content'],0,100));
                                    }
                                    else {
                                        $context->output('No Answer');
                                    }
                                //end answer section
                                                                                                 
                                            $context->output('
                                            	</div>
                                                <div class="toolbar">
													<div class="tools left">
														<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
                                                            <ul class="list-unstyled pull-left">
                                                                <li>
                                                                    <a onClick="addVote('.$most_sectionquery[0]['postid'].');">
                                                                        <i class="fa fa-angle-double-up"></i>
                                                                        <span id=span-'.$most_sectionquery[0]['postid'].'> '.$most_sectionquery[0]['upvotes'].'</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a onClick="downVote('.$most_sectionquery[0]['postid'].');">
                                                                        <i class="fa fa-angle-double-down"></i>
                                                                        <span id=span-'.$most_sectionquery[0]['postid'].'-downvote> '.$most_sectionquery[0]['downvotes'].'</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
													</div>
													<div class="share right">
														<ul>');
														
														if ($countans[0] > 1) {
															$ans = " answers";
														}
														else {
															$ans = " answer";
														}
														$context->output('
                                    						<li>' . $countans[0] . $ans . '</li>
                                        					<li>
                                              					<div class="tooltip-custom">
                                           	 						<i class="fa fa-ellipsis-h"></i>
                                                    				<div class="tooltip-inner tooltip-inner2">
                                                        				<a href="#">Invite authoritative user to answer</a></br>
                                                        				<a href="#">Add to Reading List</a></br>
                                                        				<a href="#">Show Related Questions</a></br>
                                                        				<a href="#">Create Summary Answer</a></br>
                                                        				<a href="#">Mark Favorite</a></br>
                                                        				<a href="#">Report</a></br>
                                                    				</div>
                                                 				</div>
                                                 			</li>
                                    					</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[0]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME. '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

							$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="half" alt="" widsrc="' . $imgpath . '"></div>
								-->
								</div>
							</div>

							<div class="thumb_half">
								<div class="thumb_hover half">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">' . calculate_weeks_ago($most_sectionquery[1]["created"]) . ' w ago</span>
												<span class="topic right">'.$most_sectionquery[1]["cattitle"].'</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line bounceInUp">
												<div class="h6">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[1]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[1]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[1]["title"] . '</a>
                                            </div>
                                            <div class="ans">');

                                //start answer section                                    
                                    if ($mostQueryans[1] != NULL) {
                                    	$context->output('Ans: '.substr($mostQueryans[1][0]['content'],0,100));
                                    }
                                    else {
                                        $context->output('No Answer');
                                    }
                                //end ansewr section
                                                                                                 
                                        $context->output('
                                            </div>
                                            	<div class="toolbar">
													<div class="tools left">
														<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
                                                    	<ul class="list-unstyled pull-left">
                                                            <li>
                                                                <a onClick="addVote('.$most_sectionquery[1]['postid'].');">
                                                                    <i class="fa fa-angle-double-up"></i>
                                                                    <span id=span-'.$most_sectionquery[1]['postid'].'> '.$most_sectionquery[1]['upvotes'].'</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onClick="downVote('.$most_sectionquery[1]['postid'].');">
                                                                    <i class="fa fa-angle-double-down"></i>
                                                                    <span id=span-'.$most_sectionquery[1]['postid'].'-downvote> '.$most_sectionquery[1]['downvotes'].'</span>
                                                                </a>
                                                            </li>
                                                        </ul>
													</div>
													<div class="share right">
														<ul>');
														
														if ($countans[1] > 1) {
															$ans = " answers";
														}
														else {
															$ans = " answer";
														}
														$context->output('
                                    						<li>' . $countans[1] . $ans . '</li>
                                        					<li>
                                              					<div class="tooltip-custom">
                                           	 						<i class="fa fa-ellipsis-h"></i>
                                                    				<div class="tooltip-inner tooltip-inner2">
                                                        				<a href="#">Invite authoritative user to answer</a></br>
                                                        				<a href="#">Add to Reading List</a></br>
                                                        				<a href="#">Show Related Questions</a></br>
                                                        				<a href="#">Create Summary Answer</a></br>
                                                        				<a href="#">Mark Favorite</a></br>
                                                        				<a href="#">Report</a></br>
                                                    				</div>
                                                 				</div>
                                                 			</li>
                                    					</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[1]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');
						
						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

							$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="half" alt="" widsrc="' . $imgpath . '"></div>
								-->
								</div>
							</div>
						</div>

						<div class="thumb_full">
							<div class="thumb_hover full">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">' . calculate_weeks_ago($most_sectionquery[2]["created"]) . ' w ago</span>
											<span class="topic right">'.$most_sectionquery[2]["cattitle"].'</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line bounceInUp">
											<div class="h5">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[2]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[2]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[2]["title"] . '</a>
                                            </div>
                                            <div class="ans">');
                                //start answer section                                    
                                    if ($mostQueryans[2] != NULL) {
                                    	$context->output('Ans: '.substr($mostQueryans[2][0]['content'],0,300));
                                    } 
                                    else {
                                    	$context->output('No Answer');
                                    }
                                //end ansewr section
                                                                                                 
                                    $context->output('
                                        </div>
                                        	<div class="toolbar">
												<div class="tools left">
													<ul>');

                                               	$context->output('
														<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
														<ul class="list-unstyled pull-left">
                                                            <li>
                                                                <a onClick="addVote('.$most_sectionquery[2]['postid'].');">
                                                                    <i class="fa fa-angle-double-up"></i>
                                                                    <span id=span-'.$most_sectionquery[2]['postid'].'> '.$most_sectionquery[2]['upvotes'].'</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onClick="downVote('.$most_sectionquery[2]['postid'].');">
                                                                    <i class="fa fa-angle-double-down"></i>
                                                                    <span id=span-'.$most_sectionquery[2]['postid'].'-downvote> '.$most_sectionquery[2]['downvotes'].'</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    <li><a href="javascript:void(0);">3k views</a></li>
														<li><a href="javascript:void(0);">235 comments</a></li>
														<li>															
                                                        <div class="tooltip-social">
                                                         	<a href="#">Share</a>
                                                            <div class="tooltip-inner tooltip-inner3">
                                                            	<div class="social_icon facebook_icon">
                                                                	<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                            </div>
                                                         </div>                                                 				
													</li>
													</ul>                                                                                                        
												</div>
												<div class="share right">
													<ul>');
														
													if ($countans[2] > 1) {
														$ans = " answers";
													}
													else {
														$ans = " answer";
													}
													$context->output('
                                    					<li>' . $countans[2] . $ans . '</li>
														<li><a href="javascript:void(0);">
                                            				<div class="tooltip-custom">
                                           	 				<i class="fa fa-ellipsis-h"></i>
                                                    			<div class="tooltip-inner tooltip-inner2">
                                                        			<a href="#">Invite authoritative user to answer</a></br>
                                                        			<a href="#">Add to Reading List</a></br>
                                                        			<a href="#">Show Related Questions</a></br>
                                                        			<a href="#">Create Summary Answer</a></br>
                                                        			<a href="#">Mark Favorite</a></br>
                                                        			<a href="#">Report</a></br>
                                                    			</div>
                                                 			</div>
                                                        </a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[2]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

							$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>
								-->
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row">
					<div class="thumb_full_single_right">
						<div class="thumb_hover full_single">
							<div class="hover_overlay">
								<div class="inner">
									<div class="top_line">
										<span class="time left">' . calculate_weeks_ago($most_sectionquery[3]["created"]) . ' w ago</span>
										<span class="topic right">'.$most_sectionquery[3]["cattitle"].'</span>
										<div class="clearfix"></div>
									</div>
									<div class="bottom_line bounceInUp">
										<div class="h5">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[3]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[3]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[3]["title"] . '</a>
                                            </div>
                                        	<div class="ans">');
      
                                //start answer section                                    
                                    if ($mostQueryans[3] != NULL) {
                                    	$context->output('Ans: '.substr($mostQueryans[3][0]['content'],0,300));
                                    } 
                                    else {
                                    	$context->output('No Answer');
                                    }
                                //end ansewr section
                                                                                                 
                                $context->output('
                                	</div>
                                     	<div class="toolbar">
											<div class="tools left">
												<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>														
                                                                <ul class="list-unstyled pull-left">
                                                                    <li>
                                                                        <a onClick="addVote('.$most_sectionquery[3]['postid'].');">
                                                                            <i class="fa fa-angle-double-up"></i>
                                                                            <span id=span-'.$most_sectionquery[3]['postid'].'> '.$most_sectionquery[3]['upvotes'].'</span>
                                                                        </a>

                                                                    </li>
                                                                    <li>
                                                                        <a onClick="downVote('.$most_sectionquery[3]['postid'].');">
                                                                            <i class="fa fa-angle-double-down"></i>
                                                                            <span id=span-'.$most_sectionquery[3]['postid'].'-downvote> '.$most_sectionquery[3]['downvotes'].'</span>
                                                                        </a>

                                                                    </li>
                                                                </ul>
                                                        <li><a href="javascript:void(0);">3k views</a></li>
													<li><a href="javascript:void(0);">235 comments</a></li>
													<li>															
                                                        <div class="tooltip-social">
                                                         	<a href="#">Share</a>
                                                            <div class="tooltip-inner tooltip-inner3">
                                                            	<div class="social_icon facebook_icon">
                                                                	<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                            </div>
                                                         </div>                                                 				
													</li>
												</ul>
											</div>
											<div class="share right">
												<ul>');
														
												if ($countans[3] > 1) {
													$ans = " answers";
												}
												else {
													$ans = " answer";
												}
												$context->output('
                                    				<li>' . $countans[3] . $ans . '</li>
													<li><a href="javascript:void(0);"><div class="tooltip-custom">
                                           	 		<i class="fa fa-ellipsis-h"></i>
                                                    <div class="tooltip-inner tooltip-inner2">
                                                        <a href="#">Invite authoritative user to answer</a><br>
                                                        <a href="#">Add to Reading List</a><br>
                                                        <a href="#">Show Related Questions</a><br>
                                                        <a href="#">Create Summary Answer</a><br>
                                                        <a href="#">Mark Favorite</a><br>
                                                        <a href="#">Report</a>
                                                    </div>
                                                 </div></a></li>
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[3]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

						$context->output('
						</div>
					</div>
				</div>
			</div>
		</div>');

	$context->output("<input type='hidden' value='" . $_SESSION['recent_slide_index'] . "' id='recent_slide_index'>");
	$context->output("<input type='hidden' value='" . $_SESSION['recent_total_slides'] . "' id='recent_total_slides'>");							

$context->output('	
</div></div></div>');

    $context->output('<div id="recently_table">
    					<div id="allthis-week-res">
    						<div id="allthis-week">');
    $i=0;

    foreach ($most_sectionquery as $list)
            {
                $context->output('
                    <div class="container">
                        <div class="table-responsive">
                            <table class="table table-striped custom_table">
                                <tbody>
                                    <tr>
                                        <td style="width:5%;">
                                            <ul class="list-unstyled">
                                                <li>                                               
                                                    <a onClick="addVote('.$list['postid'].');">
                                                        <i class="fa fa-angle-double-up"></i>
                                                        <span id=span-'.$list['postid'].'> '.$list['upvotes'].'</span>
                                                    </a>
                                                </li>
                                                <li>
                                                     <a onClick="downVote('.$list['postid'].');">
                                                        <i class="fa fa-angle-double-down"></i>
                                                        <span id=span-'.$list['postid'].'-downvote> '.$list['downvotes'].'</span>
                                                    </a>
                                                </li>
                                            </ul>  
                                        </td>

                                        <td colspan="4" style="width:70%;">
                                        <h3 style="padding:0; margin:0;">');

										//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    									$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $list["postid"] . "/" . preg_replace("/[\s_]/", "-", $list["title"]);                                        

                                        $context->output('
                                        <a href="' . $ref . '" style="color:black;">' . $list['title'] . '</a></h3>
                                            <p style="margin:0 0 5px 0; font-size:90%;">asked <strong>' . date("M d", strtotime($list['created'])) . '</strong> in <strong>' . $list['cattitle'] . '</strong> </p>
                                            <p id="more" style="padding:0; margin:0;">
                                            	<span class="more" style="margin:0;">');

                        //start answer section                            
                            if ($mostQueryans[$i] != NULL) {
                            	$context->output('Last Answer: ' . $mostQueryans[$i][0]['content']);
                            }
                            else {
                            	$context->output('No Answer');
                            }
                        //end ansewr section
                            
                            $relatedqlink = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/ask?follow=' . $list['postid'] . '&cat=' . $list['categoryid'];

                                
                    $context->output('</span></p>');
                            
/*                    
//					start section username
                    $fetchusername = qa_db_read_all_assoc(qa_db_query_sub("select handle from qa_users where userid='".$list['userid']."' "));
         
                    foreach ($fetchusername as $userlist)
                     {
                          
                         // $context->output('by <strong>' . $userlist['handle'] . '</strong>');
                      }
                               
//                  end section username
*/
                    
                    $context->output('
                                            <ul class="list-inline">
                                                    <li class="active">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-pencil"></i>
                                                            <span>
                                                               <a href=' . $ref . '>Answer</a>
                                                             </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                                <i class="fa fa-bars"></i>
                                                            <span>
                                                                <a href=' . $relatedqlink . '>Ask related question</a>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>															
                                                        <div class="tooltip-social">
                                                         	<a href="#">Share</a>
                                                            <div class="tooltip-inner tooltip-inner3">
                                                            	<div class="social_icon facebook_icon">
                                                                	<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                            </div>
                                                         </div>                                                 				
													</li>
                                            </ul>
                                    </td>
                                    <td style="width:10%;">
                                    	'.ShowWantAnswer($countans[$i], $list['postid'], $ref).'
                                      
                                        <div class="tooltip-custom">
                                            <i class="fa fa-ellipsis-h"></i>
                                                    <div class="tooltip-inner">
                                                        <a href="#">Invite authoritative user to answer</a><br>
                                                        <a href="#">Add to Reading List</a><br>
                                                        <a href="#">Show Related Questions</a><br>
                                                        <a href="#">Create Summary Answer</a><br>
                                                        <a href="#">Mark Favorite</a><br>
                                                        <a href="#">Report</a>
                                                    </div>

                                        </div>
                                    </td>
                                    <td style="width:10%;">
                                        <a href="javascript:void(0)" class="btn btn-red">
                                            <i class="fa fa-circle"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
                </div>');
		$i+=1;
    }

    $recent_countque = listview_questions;
    $recent_listview_firsttime = $_SESSION['recent_listview_firsttime'];

	$context->output('<input type="hidden" id="recent_countque" value="' . $recent_countque . '">
                        <input type="hidden" id="recent_index" value="' . $listview_recentindex . '">
						<input type="hidden" id="recent_listview_firsttime" value="' . $recent_listview_firsttime . '">');

	$context->output('</div></div></div>');

	$context->output('<div class="more_questions" id="more_questions_recent"><p class="more_text">To see more questions, click 
    					<a class="questions" id="more_link_recent" onClick="more_questions_listview_recent();" href="javascript:void(0);"><b class="red">here.</b></a></p>
    				  </div>');

	$context->output('</div>');
  }
  
  
	function landing_page_all_questions_header($context) 
	/**
	*
	*/
	{
    require_once QA_INCLUDE_DIR . 'db/selects.php';
    $categories = qa_db_select_with_pending(qa_db_category_nav_selectspec(null, true));
    
    $ajaxlink = '/qa-theme/Islamiqa-as-dev/thisweekall.php';
    
    $context->output('<div class="qa_gallery first allquation">
	<!-- ** START QUESTIONS & ANSWERS FILTER ** -->
        <div class="container">
            <div class="row">
		<div class="qa_filter clearfix">
			<div class="col-md-5 col-xs-7">
				<ul class="type_filter clearfix">
                    <li id="allque_fad">
					<a href="javascript:void(0);"><i class="fa fa-caret-up allque_fad_up font-size-30"></i></a>
					<a href="javascript:void(0);"><i class="fa fa-caret-down allque_fad_down font-size-30"></i></a></li>
					<li>
						<div>
						<a onclick="allquationask();" class="active_section" id="all-sectiontitle">All Questions</a>
						</div>
					</li>
				</ul>
			</div>
  
  			<div class="col-md-3 col-xs-5">
				<ul class="type_filter clearfix">
					
					<li class="hidden-xs"><a onClick="allquationweek();" id="allque-thisweeklink">This Week</a></li>
					<li class="hidden-xs"><a onClick="allquationmonth();" id="allque-thismonthlink">This Month</a></li>
					<li class="hidden-xs"><a onClick="allquationyear();" id="allque-thisyearlink">This Year</a></li>
				</ul>
			</div>

			<div class="col-md-4 col-xs-6">
				<ul class="filter clearfix">
					
                    <li>
					<span>View</span>
					<a class="view_icons">
					<span id="allquation-list"><i class="fa fa-align-right"></i></span>
					<span class="icon red-active" id="allquation-grid"><i class="fa fa-th-large active"></i></span>
					</a></li>
					<li>
						<div class="dropdown">
							<button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" id="dropdownMenu3" type="button" class="btn btn-default dropdown-toggle">
								Categories <span class="caret"></span>
							</button>
							<ul aria-labelledby="dropdownMenu3" class="dropdown-menu">
								');
                                        foreach ($categories as $category)
                                            $context->output('<li ><a class="mp_cat" cat_id="' . $category["categoryid"] . '" onClick="allquationcategory('.$category["categoryid"].');" id="all-thiscatlink">' . $category["title"] . ' </a ></li >');
                                        $context->output('</ul>
						</div>
					</li>
				</ul>
			</div>
                        <div class="col-xs-12 visible-xs">
				<ul class="type_filter clearfix type_filter_custom">
					<li><a href="javascript:void(0);"><i class="fa fa-angle-left"></i></a></li>
					<li class="hidden-xs"><a href="javascript:void(0);" class="active_section">Most Popular</a></li>
					<li><a href="javascript:void(0);">This Week</a></li>
					<li><a href="javascript:void(0);">This Month</a></li>
					<li><a href="javascript:void(0);">This Year</a></li>
                                        <li><a href="javascript:void(0);"><i class="fa fa-angle-right"></i></a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	</div>');
	}


	
	function landing_page_all_questions_structure($context) 
	/**
	*
	*/
	{

	$local_index = 0;
	$listview_allqueindex = $_SESSION['listview_allqueindex'];
	
    $context->output('
<!-- ** GRID VIEW STRUCTURE MOST VIEW ** -->
<div id="allquation_start">
    <div id="allquation_grid">
    <div class="container popular_answer_wrapper">
	<a onclick="arrowleft_allquestions();" class="left-arrow" id="allquation-left"><i class="fa fa-angle-left"></i></a>
	<a onclick="arrowright_allquestions();" class="right-arrow" id="allquation-right"><i class="fa fa-angle-right"></i></a>
        <div class="row" id="allquation_first_five">');
    
    $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid WHERE type='Q' ORDER BY created ASC LIMIT " . listview_questions));
    $most_countque = sizeof($most_sectionquery);
    
    $mostQueryans = array();
    for ($i=0; $i < $most_countque; $i++)
    {
		$mostQueryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));

        if ($mostQueryans[$i] != NULL) {
            //$countans[$i] = qa_db_query_sub("SELECT COUNT(postid) FROM qa_posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ");
            $countans[$i] = sizeof($mostQueryans[$i]);
        }
        else {
            $countans[$i] = 0;
        }
    }
    
    $context->output('<div class="col-md-6">
				<div class="row">
					<div class="gallery_four_thumb">
                        <div class="clearfix">
							<div class="thumb_half">
								<div class="thumb_hover half">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">' . calculate_weeks_ago($most_sectionquery[0]["created"]) . ' w ago</span>
												<span class="topic right">'.$most_sectionquery[0]["cattitle"].'</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line bounceInUp">
												<div class="h6">');

    								//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);
									
									$context->output('                                      
                                           	<a href="' . $ref . '">' . $most_sectionquery[0]["title"] . '</a>
                                            </div>
                                            <div class="ans">');

                                        //start answer section                                            
                                            if ($mostQueryans[0] != NULL) {
                                            	$context->output('Ans: '.substr($mostQueryans[0][0]['content'],0,100));
                                            } 
                                            else {
                                                $context->output('No Answer');
                                            }
                                        //end ansewr section
                                                                                                 
                                            $context->output('
                                                </div>
                                                <div class="toolbar">
													<div class="tools left">
														<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
														<ul class="list-unstyled pull-left">
                                                            <li>
                                                                <a onClick="addVote('.$most_sectionquery[0]['postid'].');">
                                                                    <i class="fa fa-angle-double-up"></i>
                                                                    <span id=span-'.$most_sectionquery[0]['postid'].'> '.$most_sectionquery[0]['upvotes'].'</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onClick="downVote('.$most_sectionquery[0]['postid'].');">
                                                                    <i class="fa fa-angle-double-down"></i>
                                                                    <span id=span-'.$most_sectionquery[0]['postid'].'-downvote> '.$most_sectionquery[0]['downvotes'].'</span>
                                                                </a>
                                                            </li>
                                                		</ul>
													</div>
													<div class="share right">
														<ul>');
														
														if ($countans[0] > 1) {
															$ans = " answers";
														}
														else {
															$ans = " answer";
														}
														$context->output('
                                    						<li>' . $countans[0] . $ans . '</li>                                                        
                                        					<li>
                                              					<div class="tooltip-custom">
                                           	 						<i class="fa fa-ellipsis-h"></i>
                                                    				<div class="tooltip-inner tooltip-inner2">
                                                        				<a href="#">Invite authoritative user to answer</a></br>
                                                        				<a href="#">Add to Reading List</a></br>
                                                        				<a href="#">Show Related Questions</a></br>
                                                        				<a href="#">Create Summary Answer</a></br>
                                                        				<a href="#">Mark Favorite</a></br>
                                                        				<a href="#">Report</a></br>
                                                    				</div>
                                                 				</div>
                                                 			</li>
                                    					</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[0]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME. '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

						$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>
								-->
								</div>
							</div>

							<div class="thumb_half">
								<div class="thumb_hover half">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">' . calculate_weeks_ago($most_sectionquery[1]["created"]) . ' w ago</span>
												<span class="topic right">'.$most_sectionquery[1]["cattitle"].'</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line bounceInUp">
												<div class="h6">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[1]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[1]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[1]["title"] . '</a>
                                            </div>
                                            <div class="ans">');
      
                                      //start answer section                                            
                                            if ($mostQueryans[1] != NULL) {
                                            	$context->output('Ans: '.substr($mostQueryans[1][0]['content'],0,100));
                                            }
                                            else {
                                                $context->output('No Answer');
                                            }
                                        //end ansewr section
                                                                                                 
                                    $context->output('
                                    		</div>
                                    			<div class="toolbar">
													<div class="tools left">
														<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
                                                        <ul class="list-unstyled pull-left">
                                                    		<li>
                                                            <a onClick="addVote('.$most_sectionquery[1]['postid'].');">
                                                                <i class="fa fa-angle-double-up"></i>
                                                                <span id=span-'.$most_sectionquery[1]['postid'].'> '.$most_sectionquery[1]['upvotes'].'</span>
                                                            </a>
                                                    		</li>
                                                    		<li>
                                                            <a onClick="downVote('.$most_sectionquery[1]['postid'].');">
                                                                <i class="fa fa-angle-double-down"></i>
                                                                <span id=span-'.$most_sectionquery[1]['postid'].'-downvote> '.$most_sectionquery[1]['downvotes'].'</span>
                                                            </a>
                                                    		</li>
                                                		</ul>
													</ul>
												</div>
													<div class="share right">
														<ul>');
														
														if ($countans[1] > 1) {
															$ans = " answers";
														}
														else {
															$ans = " answer";
														}
														$context->output('
                                    						<li>' . $countans[1] . $ans . '</li>
                                        					<li>
                                              					<div class="tooltip-custom">
                                           	 						<i class="fa fa-ellipsis-h"></i>
                                                    				<div class="tooltip-inner tooltip-inner2">
                                                        				<a href="#">Invite authoritative user to answer</a></br>
                                                        				<a href="#">Add to Reading List</a></br>
                                                        				<a href="#">Show Related Questions</a></br>
                                                        				<a href="#">Create Summary Answer</a></br>
                                                        				<a href="#">Mark Favorite</a></br>
                                                        				<a href="#">Report</a></br>
                                                    				</div>
                                                 				</div>
                                                 			</li>
                                    					</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[1]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

						$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>
								-->
								</div>
							</div>
						</div>

						<div class="thumb_full">
							<div class="thumb_hover full">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">' . calculate_weeks_ago($most_sectionquery[2]["created"]) . ' w ago</span>
											<span class="topic right">' . $most_sectionquery[2]["cattitle"] . '</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line bounceInUp">
											<div class="h5">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[2]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[2]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[2]["title"] . '</a>
                                            </div>
                                            <div class="ans">');												
      
                                      //start answer section                                            
                                            if ($mostQueryans[2] != NULL) {
                                            	$context->output('Ans: '.substr($mostQueryans[2][0]['content'],0,300));
                                            } 
                                            else {
                                                $context->output('No Answer');
                                            }
                                        //end ansewr section
                                                                                                 
                                                                              $context->output('</div><div class="toolbar">
												<div class="tools left">
													<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                                            <ul class="list-unstyled pull-left">
                                                                <li>
                                                                    <a onClick="addVote('.$most_sectionquery[2]['postid'].');">
                                                                        <i class="fa fa-angle-double-up"></i>
                                                                        <span id=span-'.$most_sectionquery[2]['postid'].'> '.$most_sectionquery[2]['upvotes'].'</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a onClick="downVote('.$most_sectionquery[2]['postid'].');">
                                                                        <i class="fa fa-angle-double-down"></i>
                                                                        <span id=span-'.$most_sectionquery[2]['postid'].'-downvote> '.$most_sectionquery[2]['downvotes'].'</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
														<li><a href="javascript:void(0);">3k views</a></li>
														<li><a href="javascript:void(0);">235 comments</a></li>
														<li>															
                                                        <div class="tooltip-social">
                                                         	<a href="#">Share</a>
                                                            <div class="tooltip-inner tooltip-inner3">
                                                            	<div class="social_icon facebook_icon">
                                                                	<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                            </div>
                                                         </div>                                                 				
													</li>
													</ul>
												</div>
												<div class="share right">
													<ul>');
														
														if ($countans[2] > 1) {
															$ans = " answers";
														}
														else {
															$ans = " answer";
														}
														$context->output('
                                    					<li>' . $countans[2] . $ans . '</li>
														<li><a href="javascript:void(0);"><div class="tooltip-custom">
                                           	 				<i class="fa fa-ellipsis-h"></i>
                                                    		<div class="tooltip-inner tooltip-inner2">
                                                        		<a href="#">Invite authoritative user to answer</a><br>
                                                        		<a href="#">Add to Reading List</a><br>
                                                        		<a href="#">Show Related Questions</a><br>
                                                        		<a href="#">Create Summary Answer</a><br>
                                                        		<a href="#">Mark Favorite</a><br>
                                                        		<a href="#">Report</a>
                                                    		</div>
                                                 		</div></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[2]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

						$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>
								-->
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row">
					<div class="thumb_full_single_right">
						<div class="thumb_hover full_single">
							<div class="hover_overlay">
								<div class="inner">
									<div class="top_line">
										<span class="time left">' . calculate_weeks_ago($most_sectionquery[3]["created"]) . ' w ago</span>
										<span class="topic right">'.$most_sectionquery[3]["cattitle"].'</span>
										<div class="clearfix"></div>
									</div>
									<div class="bottom_line bounceInUp">
										<div class="h5">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[3]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[3]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[3]["title"] . '</a>
                                            </div>
                                            <div class="ans">');
      
                                      //start answer section                                            
                                            if ($mostQueryans[3] != NULL) {
                                            	$context->output('Ans: '.substr($mostQueryans[3][0]['content'],0,300));
                                            } 
                                            else {
                                                $context->output('No Answer');
                                            }
                                        //end ansewr section
                                                                                                 
                                    $context->output('
                                    	</div>
                                    	<div class="toolbar">
											<div class="tools left">
												<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                                            <ul class="list-unstyled pull-left">
                                                                <li>
                                                                    <a onClick="addVote('.$most_sectionquery[3]['postid'].');">
                                                                        <i class="fa fa-angle-double-up"></i>
                                                                        <span id=span-'.$most_sectionquery[3]['postid'].'> '.$most_sectionquery[3]['upvotes'].'</span>
                                                                    </a>

                                                                </li>
                                                                <li>
                                                                    <a onClick="downVote('.$most_sectionquery[3]['postid'].');">
                                                                        <i class="fa fa-angle-double-down"></i>
                                                                        <span id=span-'.$most_sectionquery[3]['postid'].'-downvote> '.$most_sectionquery[3]['downvotes'].'</span>
                                                                    </a>
                                                                </li>
                                                            </ul>

													<li><a href="javascript:void(0);">3k views</a></li>
													<li><a href="javascript:void(0);">235 comments</a></li>
													<li>															
                                                        <div class="tooltip-social">
                                                         	<a href="#">Share</a>
                                                            <div class="tooltip-inner tooltip-inner3">
                                                            	<div class="social_icon facebook_icon">
                                                                	<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                            </div>
                                                         </div>                                                 				
													</li>
												</ul>
											</div>
											<div class="share right">
												<ul>');
														
												if ($countans[3] > 1) {
													$ans = " answers";
												}
												else {
													$ans = " answer";
												}
												$context->output('
                                    				<li>' . $countans[3] . $ans . '</li>
													<li><a href="javascript:void(0);"><div class="tooltip-custom">
                                           	 		<i class="fa fa-ellipsis-h"></i>
                                                    <div class="tooltip-inner tooltip-inner2">
                                                        <a href="#">Invite authoritative user to answer</a><br>
                                                        <a href="#">Add to Reading List</a><br>
                                                        <a href="#">Show Related Questions</a><br>
                                                        <a href="#">Create Summary Answer</a><br>
                                                        <a href="#">Mark Favorite</a><br>
                                                        <a href="#">Report</a>
                                                    </div>
                                                 </div></a></li>
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[3]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

						$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>
								-->
						</div>
					</div>
				</div>
			</div>
		</div>');

	$context->output("<input type='hidden' value='" . $_SESSION['allque_slide_index'] . "' id='allque_slide_index'>");
	$context->output("<input type='hidden' value='" . $_SESSION['allque_total_slides'] . "' id='allque_total_slides'>");

$context->output('	
</div></div></div>');

    $context->output('<div id="allquation_table">
    					<div id="allthis-week-res">
    						<div id="allthis-week">');
    $i=0;
    foreach ($most_sectionquery as $list)
            {
                $context->output('
                     <!-- ** LIST VIEW STRUCTURE MOST VIEW ** -->
                    <div class="container">
                        <div class="table-responsive">
                            <table class="table table-striped custom_table">
                                <tbody>
                                    <tr>
                                        <td style="width:5%;">
                                            <ul class="list-unstyled">
                                                <li>                                               
                                                    <a onClick="addVote('.$list['postid'].');">
                                                        <i class="fa fa-angle-double-up"></i>
                                                        <span id=span-'.$list['postid'].'> '.$list['upvotes'].'</span>
                                                    </a>
                                                </li>
                                                <li>
                                                     <a onClick="downVote('.$list['postid'].');">
                                                        <i class="fa fa-angle-double-down"></i>
                                                        <span id=span-'.$list['postid'].'-downvote> '.$list['downvotes'].'</span>
                                                    </a>
                                                </li>
                                            </ul>  
                                        </td>

                                        <td colspan="4" style="width:70%;">                                           
                                        <h3 style="padding:0; margin:0;">');

										//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    									$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $list["postid"] . "/" . preg_replace("/[\s_]/", "-", $list["title"]);                                        

                                        $context->output('
                                        <a href="' . $ref . '" style="color:black;">' . $list['title'] . '</a></h3>
                                            <p style="margin:0 0 5px 0; font-size:90%;">asked <strong>' . date("M d", strtotime($list['created'])) . '</strong> in <strong>' . $list['cattitle'] . '</strong> </p>
                                            <p id="more" style="padding:0; margin:0;">
                                                <span class="more" style="margin:0;">');

							//start answer section                            
                                if ($mostQueryans[$i] != NULL) {
                                	$context->output('Last Answer: ' . $mostQueryans[$i][0]['content']);
                                }
                                else {
                                    $context->output('No Answer');
                                }
                        //end answer section 
                                
                                $relatedqlink = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/ask?follow=' . $list['postid'] . '&cat=' . $list['categoryid'];
                                
                    $context->output('</span></p> ');
                            
/*                    
//                    start section username
                    $fetchusername = qa_db_read_all_assoc(qa_db_query_sub("select handle from qa_users where userid='".$list['userid']."' "));
         
                    foreach ($fetchusername as $userlist) 
                     {
                        //$context->output('by <strong>' . $userlist['handle'] . '</strong>');
                      }                               
//                  end section username
*/
                    
                    			$context->output('</p>
                                            	<ul class="list-inline">
                                                    <li class="active">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-pencil"></i>
                                                            <span>
                                                               <a href=' . $ref . '>Answer</a>
                                                             </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                                <i class="fa fa-bars"></i>
                                                            <span>
                                                                <a href=' . $relatedqlink . '>Ask related question</a>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>															
                                                        <div class="tooltip-social">
                                                         	<a href="#">Share</a>
                                                            <div class="tooltip-inner tooltip-inner3">
                                                            	<div class="social_icon facebook_icon">
                                                                	<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                            </div>
                                                         </div>                                                 				
													</li>
                                            </ul>
                                    </td>
                                    <td style="width:10%;">
                                        '.ShowWantAnswer($countans[$i], $list['postid'], $ref).'
                                        <div class="tooltip-custom">
                                            <i class="fa fa-ellipsis-h"></i>
                                                    <div class="tooltip-inner">
                                                        <a href="#">Invite authoritative user to answer</a><br>
                                                        <a href="#">Add to Reading List</a><br>
                                                        <a href="#">Show Related Questions</a><br>
                                                        <a href="#">Create Summary Answer</a><br>
                                                        <a href="#">Mark Favorite</a><br>
                                                        <a href="#">Report</a>
                                                    </div>
                                        </div>
                                    </td>
                                    <td style="width:10%;">
                                        <a href="javascript:void(0)" class="btn btn-red">
                                            <i class="fa fa-circle"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
                </div>');		
		$i+=1;
    }

    $allque_countque = listview_questions;
    $allque_listview_firsttime = $_SESSION['allque_listview_firsttime'];

	$context->output('<input type="hidden" id="allque_countque" value="' . $allque_countque . '">
                        <input type="hidden" id="allque_index" value="' . $listview_allqueindex . '">
						<input type="hidden" id="allque_listview_firsttime" value="' . $allque_listview_firsttime . '">');

	$context->output('</div></div></div>');

	$context->output('<div class="more_questions" id="more_questions_allque"><p class="more_text">To see more questions, click 
    					<a class="questions" id="more_link_allque" onClick="more_questions_listview_allque();" href="javascript:void(0);"><b class="red">here.</b></a></p>
    				  </div>');

	$context->output('</div>');
	}

	
		function landing_page_unanswered_header($context) 
	/**
	*
	*/
	{
    require_once QA_INCLUDE_DIR . 'db/selects.php';
    $categories = qa_db_select_with_pending(qa_db_category_nav_selectspec(null, true));
    
    $ajaxlink = '/qa-theme/Islamiqa-as-dev/thisweekall.php';
    
    $context->output('<div class="qa_gallery first unansquation">
	<!-- ** START QUESTIONS & ANSWERS FILTER ** -->
        <div class="container">
            <div class="row">
		<div class="qa_filter clearfix">
			<div class="col-md-5 col-xs-7">
				<ul class="type_filter clearfix">
                    <li id="unansque_fad">
					<a href="javascript:void(0);"><i class="fa fa-caret-up unansque_fad_up font-size-30"></i></a>
					<a href="javascript:void(0);"><i class="fa fa-caret-down unansque_fad_down font-size-30"></i></a></li>
					<li>
						<div>
						<a onclick="unansquationask();" class="active_section" id="unans-sectiontitle">Unanswered Questions</a>
						</div>
					</li>
				</ul>
			</div>
  
  			<div class="col-md-3 col-xs-5">
				<ul class="type_filter clearfix">	

					<li class="hidden-xs"><a onClick="unansquationweek();" id="unans-thisweeklink">This Week</a></li>
					<li class="hidden-xs"><a onClick="unansquationmonth();" id="unans-thismonthlink">This Month</a></li>
					<li class="hidden-xs"><a onClick="unansquationyear();" id="unans-thisyearlink">This Year</a></li>
				</ul>
			</div>

			<div class="col-md-4 col-xs-6">
				<ul class="filter clearfix">
                    <li>
					<span>View</span>
					<a class="view_icons">
					<span id="unansquation-list"><i class="fa fa-align-right"></i></span>
					<span class="icon red-active" id="unansquation-grid"><i class="fa fa-th-large active"></i></span>
					</a></li>
					<li>
						<div class="dropdown">
							<button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" id="dropdownMenu3" type="button" class="btn btn-default dropdown-toggle">
								Categories <span class="caret"></span>
							</button>
							<ul aria-labelledby="dropdownMenu3" class="dropdown-menu">');

                                                        foreach ($categories as $category)
                                                            $context->output('<li><a class="mp_cat" cat_id="' . $category["categoryid"] . '" onClick="unansquationcategory('.$category["categoryid"].');" id="all-thiscatlink">' . $category["title"] . ' </a ></li>');
                                                        $context->output('</ul>
						</div>
					</li>
				</ul>
			</div>
                        <div class="col-xs-12 visible-xs">
				<ul class="type_filter clearfix type_filter_custom">
					<li><a href="javascript:void(0);"><i class="fa fa-angle-left"></i></a></li>
					<li class="hidden-xs"><a href="javascript:void(0);" class="active_section">Most Popular</a></li>
					<li><a href="javascript:void(0);">This Week</a></li>
					<li><a href="javascript:void(0);">This Month</a></li>
					<li><a href="javascript:void(0);">This Year</a></li>
                                        <li><a href="javascript:void(0);"><i class="fa fa-angle-right"></i></a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	</div>');
	}

	

	function landing_page_unanswered_structure($context)
	/**
	*
	*/
	{
	$local_index = 0;
	$listview_unansindex = $_SESSION['listview_unansindex'];
     
	$context->output('<!-- ** GRID VIEW STRUCTURE MOST VIEW ** -->
	<div id="unansquation_start">
    <div id="unansquation_grid">
    <div class="container popular_answer_wrapper">
	<a onclick="arrowleft_unanswered();" class="left-arrow" id="unansquation-left"><i class="fa fa-angle-left"></i></a>
	<a onclick="arrowright_unanswered();" class="right-arrow" id="unansquation-right"><i class="fa fa-angle-right"></i></a>
        <div class="row" id="unansquation_first_five">');
    
    $most_sectionquery = qa_db_read_all_assoc(qa_db_query_sub("SELECT ^posts.*, ^categories.title AS cattitle FROM ^posts LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
            WHERE type='Q' AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') ORDER BY created DESC LIMIT " . listview_questions));
    $most_countque = sizeof($most_sectionquery);
    
    $mostQueryans=array();
    for($i=0; $i < $most_countque; $i++)
    {
		$mostQueryans[$i] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $most_sectionquery[$i]['postid'] . "' ORDER BY created DESC"));
            
        if($mostQueryans[$i] != NULL) {
            $countans[$i] = sizeof($mostQueryans[$i]);
        }
        else {
            $countans[$i] = 0;
        }
    }
    
    $context->output('<div class="col-md-6">
				<div class="row">
					<div class="gallery_four_thumb">
                    	<div class="clearfix">
							<div class="thumb_half">
								<div class="thumb_hover half">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">' . calculate_weeks_ago($most_sectionquery[0]["created"]) . ' w ago</span>
												<span class="topic right">'.$most_sectionquery[0]["cattitle"].'</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line bounceInUp">
												<div class="h6">');

    								//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[0]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[0]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[0]["title"] . '</a>
                                            </div>
                                            <div class="ans">');
      
                                      //start answer section                                            
                                            if ($mostQueryans[0] != NULL) {
                                            	$context->output('Ans: '.substr($mostQueryans[0][0]['content'],0,100));
                                            } 
                                            else {
                                                $context->output('No Answer');
                                            }
                                        //end ansewr section
                                                                                                 
                                    $context->output('
                                    		</div>
                                    			<div class="toolbar">
													<div class="tools left">
														<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
                                                        <ul class="list-unstyled pull-left">
                                                    		<li>
                                                            <a onClick="addVote('.$most_sectionquery[0]['postid'].');">
                                                                <i class="fa fa-angle-double-up"></i>
                                                                <span id=span-'.$most_sectionquery[0]['postid'].'> '.$most_sectionquery[0]['upvotes'].'</span>
                                                            </a>
                                                   			</li>
                                                    		<li>
                                                            <a onClick="downVote('.$most_sectionquery[0]['postid'].');">
                                                                <i class="fa fa-angle-double-down"></i>
                                                                <span id=span-'.$most_sectionquery[0]['postid'].'-downvote> '.$most_sectionquery[0]['downvotes'].'</span>
                                                            </a>
                                                    		</li>
                                                		</ul>
													</ul>
													</div>
													<div class="share right">
														<ul>');
														
														if ($countans[0] > 1) {
															$ans = " answers";
														}
														else {
															$ans = " answer";
														}
														$context->output('
                                    						<li>' . $countans[0] . $ans . '</li>
                                        					<li>
                                              					<div class="tooltip-custom">
                                           	 						<i class="fa fa-ellipsis-h"></i>
                                                    				<div class="tooltip-inner tooltip-inner2">
                                                        				<a href="#">Invite authoritative user to answer</a></br>
                                                        				<a href="#">Add to Reading List</a></br>
                                                        				<a href="#">Show Related Questions</a></br>
                                                        				<a href="#">Create Summary Answer</a></br>
                                                        				<a href="#">Mark Favorite</a></br>
                                                        				<a href="#">Report</a></br>
                                                    				</div>
                                                 				</div>
                                                 			</li>
                                    					</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[0]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

						$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>
								-->
								</div>
							</div>

							<div class="thumb_half">
								<div class="thumb_hover half">
									<div class="hover_overlay">
										<div class="inner">
											<div class="top_line">
												<span class="time left">' . calculate_weeks_ago($most_sectionquery[1]["created"]) . ' w ago</span>
												<span class="topic right">'.$most_sectionquery[1]["cattitle"].'</span>
												<div class="clearfix"></div>
											</div>
											<div class="bottom_line bounceInUp">
												<div class="h6">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[1]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[1]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[1]["title"] . '</a>
                                            </div>
                                        	<div class="ans">');
                                              
                                        //start answer section                                            
                                            if ($mostQueryans[1] != NULL) {
                                            	$context->output('Ans: '.substr($mostQueryans[1][0]['content'],0,100));
                                            } 
                                            else {
                                                $context->output('No Answer');
                                            }
                                        //end ansewr section
                                                                                                 
                                        $context->output('
                                        		</div>
                                        		<div class="toolbar">
													<div class="tools left">
														<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
														</ul>
                                                        <ul class="list-unstyled pull-left">
                                                    		<li>
                                                            <a onClick="addVote('.$most_sectionquery[1]['postid'].');">
                                                                <i class="fa fa-angle-double-up"></i>
                                                                <span id=span-'.$most_sectionquery[1]['postid'].'> '.$most_sectionquery[1]['upvotes'].'</span>
                                                            </a>
                                                    		</li>
                                                    		<li>
                                                            <a onClick="downVote('.$most_sectionquery[1]['postid'].');">
                                                                <i class="fa fa-angle-double-down"></i>
                                                                <span id=span-'.$most_sectionquery[1]['postid'].'-downvote> '.$most_sectionquery[1]['downvotes'].'</span>
                                                            </a>
                                                    		</li>
                                                		</ul>
													</ul>
												</div>
													<div class="share right">
														<ul>');
														
														if ($countans[1] > 1) {
															$ans = " answers";
														}
														else {
															$ans = " answer";
														}
														$context->output('
                                    						<li>' . $countans[1] . $ans . '</li>
                                        					<li>
                                              					<div class="tooltip-custom">
                                           	 						<i class="fa fa-ellipsis-h"></i>
                                                    				<div class="tooltip-inner tooltip-inner2">
                                                        				<a href="#">Invite authoritative user to answer</a></br>
                                                        				<a href="#">Add to Reading List</a></br>
                                                        				<a href="#">Show Related Questions</a></br>
                                                        				<a href="#">Create Summary Answer</a></br>
                                                        				<a href="#">Mark Favorite</a></br>
                                                        				<a href="#">Report</a></br>
                                                    				</div>
                                                 				</div>
                                                 			</li>
                                    					</ul>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[1]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

						$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="half" alt="" src="' . $imgpath . '"></div>
								-->
								</div>
							</div>
						</div>

						<div class="thumb_full">
							<div class="thumb_hover full">
								<div class="hover_overlay">
									<div class="inner">
										<div class="top_line">
											<span class="time left">' . calculate_weeks_ago($most_sectionquery[2]["created"]) . ' w ago</span>
											<span class="topic right">'.$most_sectionquery[2]["cattitle"].'</span>
											<div class="clearfix"></div>
										</div>
										<div class="bottom_line bounceInUp">
											<div class="h5">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[2]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[2]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[2]["title"] . '</a>
                                            </div>
                                            <div class="ans">');
                                        
                                        //start answer section                                            
                                            if ($mostQueryans[2] != NULL) {
                                            	$context->output('Ans: '.substr($mostQueryans[2][0]['content'],0,300));
                                            }
                                            else {
                                                $context->output('No Answer');
                                            }
                                        //end ansewr section
                                                                                                 
                                        $context->output('
                                        	</div>
                                        	<div class="toolbar">
												<div class="tools left">
													<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                                        <ul class="list-unstyled pull-left">
                                                            <li>
                                                                <a onClick="addVote('.$most_sectionquery[2]['postid'].');">
                                                                    <i class="fa fa-angle-double-up"></i>
                                                                    <span id=span-'.$most_sectionquery[2]['postid'].'> '.$most_sectionquery[2]['upvotes'].'</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onClick="downVote('.$most_sectionquery[2]['postid'].');">
                                                                    <i class="fa fa-angle-double-down"></i>
                                                                    <span id=span-'.$most_sectionquery[2]['postid'].'-downvote> '.$most_sectionquery[2]['downvotes'].'</span>
                                                                </a>
                                                            </li>
                                                        </ul>
														<li><a href="javascript:void(0);">3k views</a></li>
														<li><a href="javascript:void(0);">235 comments</a></li>
														<li>															
                                                        <div class="tooltip-social">
                                                         	<a href="#">Share</a>
                                                            <div class="tooltip-inner tooltip-inner3">
                                                            	<div class="social_icon facebook_icon">
                                                                	<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                            </div>
                                                         </div>                                                 				
													</li>
													</ul>
												</div>
												<div class="share right">
													<ul>');
														
													if ($countans[2] > 1) {
														$ans = " answers";
													}
													else {
														$ans = " answer";
													}
													$context->output('
                                    					<li>' . $countans[2] . $ans . '</li>
														<li><a href="javascript:void(0);"><div class="tooltip-custom">
                                           	 			<i class="fa fa-ellipsis-h"></i>
                                                    		<div class="tooltip-inner tooltip-inner2">
                                                        		<a href="#">Invite authoritative user to answer</a><br>
                                                        		<a href="#">Add to Reading List</a><br>
                                                        		<a href="#">Show Related Questions</a><br>
                                                        		<a href="#">Create Summary Answer</a><br>
                                                        		<a href="#">Mark Favorite</a><br>
                                                        		<a href="#">Report</a>
                                                    		</div>
                                                 		</div></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[2]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

						$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="full" alt="" src="' . $imgpath . '"></div>
								-->
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row">
					<div class="thumb_full_single_right">
						<div class="thumb_hover full_single">
							<div class="hover_overlay">
								<div class="inner">
									<div class="top_line">
										<span class="time left">' . calculate_weeks_ago($most_sectionquery[3]["created"]) . ' w ago</span>
										<span class="topic right">'.$most_sectionquery[3]["cattitle"].'</span>
										<div class="clearfix"></div>
									</div>
									<div class="bottom_line bounceInUp">
										<div class="h5">');

									//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    								$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $most_sectionquery[3]["postid"] . "/" . preg_replace("/[\s_]/", "-", $most_sectionquery[3]["title"]);
									
									$context->output('                                      
                                            <a href="' . $ref . '">' . $most_sectionquery[3]["title"] . '</a>
                                            </div>
                                            <div class="ans">');
                                        
                                        //start answer section                                            
                                            if ($mostQueryans[3] != NULL) {
                                            	$context->output('Ans: '.substr($mostQueryans[3][0]['content'],0,300));
                                            } 
                                            else {
                                                $context->output('No Answer');
                                            }
                                        //end ansewr section
                                                                                                 
                                    $context->output('
                                    	</div>
                                    	<div class="toolbar">
											<div class="tools left">
												<ul>');

                                               			$context->output('
															<li><a href="' . $ref . '"><i class="fa fa-pencil"></i> Answer</a></li>
                                                        <ul class="list-unstyled pull-left">
                                                            <li>
                                                                <a onClick="addVote('.$most_sectionquery[3]['postid'].');">
                                                                    <i class="fa fa-angle-double-up"></i>
                                                                    <span id=span-'.$most_sectionquery[3]['postid'].'> '.$most_sectionquery[3]['upvotes'].'</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onClick="downVote('.$most_sectionquery[3]['postid'].');">
                                                                    <i class="fa fa-angle-double-down"></i>
                                                                    <span id=span-'.$most_sectionquery[3]['postid'].'-downvote> '.$most_sectionquery[3]['downvotes'].'</span>
                                                                </a>
                                                            </li>
                                                        </ul>
													<li><a href="javascript:void(0);">3k views</a></li>
													<li><a href="javascript:void(0);">235 comments</a></li>
													<li>															
                                                        <div class="tooltip-social">
                                                         	<a href="#">Share</a>
                                                            <div class="tooltip-inner tooltip-inner3">
                                                            	<div class="social_icon facebook_icon">
                                                                	<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                            </div>
                                                         </div>                                                 				
													</li>
												</ul>
											</div>
											<div class="share right">
												<ul>');
														
												if ($countans[3] > 1) {
													$ans = " answers";
												}
												else {
													$ans = " answer";
												}
												$context->output('
                                    				<li>' . $countans[3] . $ans . '</li>
													<li><a href="javascript:void(0);"><div class="tooltip-custom">
                                           	 		<i class="fa fa-ellipsis-h"></i>
                                                    <div class="tooltip-inner tooltip-inner2">
                                                        <a href="#">Invite authoritative user to answer</a><br>
                                                        <a href="#">Add to Reading List</a><br>
                                                        <a href="#">Show Related Questions</a><br>
                                                        <a href="#">Create Summary Answer</a><br>
                                                        <a href="#">Mark Favorite</a><br>
                                                        <a href="#">Report</a>
                                                    </div>
                                                 </div></a></li>
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>');

						//The next line is to get the postid of the question
						$postid_num = $most_sectionquery[3]['postid'];

						//The next line is to complete the image's path			
						$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/images/' . $postid_num . '.jpg';
						
						//This code is to verify if the server path to the image exists
						$imgexist = file_exists(__DIR__ . '/images/' . $postid_num . '.jpg');

						//This code is to set an image background for questions if the img file exist,
						//else set a plain color background for questions
						if ($imgexist) {
							$context->output('
								<!-- Now, set the image path as source of the img element -->
								<div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>');
						}
						else {
							$context->output('
								<!-- In this case just apply the style to the question background  -->
								<div class="thumb"></div>');
						}

						$local_index += 1;

						$context->output('
								<!-- Here, set the image to the img element
								<div class="thumb"><img class="bigimg" alt="" src="' . $imgpath . '"></div>
								-->
						</div>
					</div>
				</div>
			</div>
		</div>');

	$context->output("<input type='hidden' value='" . $_SESSION['unans_slide_index'] . "' id='unans_slide_index'>");
	$context->output("<input type='hidden' value='" . $_SESSION['unans_total_slides'] . "' id='unans_total_slides'>");						


	$context->output('</div></div></div>');
    $context->output('<div id="unansquation_table">
    					<div id="allthis-week-res">
    						<div id="allthis-week">');
    $i=0;
    foreach ($most_sectionquery as $list)
    {
                $context->output('
                     <!-- ** LIST VIEW STRUCTURE MOST VIEW ** -->
                    <div class="container">
                        <div class="table-responsive">
                            <table class="table table-striped custom_table">
                                <tbody>
                                    <tr>
                                        <td style="width:5%;">
                                            <ul class="list-unstyled">
                                                <li>
                                               
                                                    <a onClick="addVote('.$list['postid'].');">
                                                        <i class="fa fa-angle-double-up"></i>
                                                        <span id=span-'.$list['postid'].'> '.$list['upvotes'].'</span>
                                                    </a>
                                                </li>
                                                <li>
                                                     <a onClick="downVote('.$list['postid'].');">
                                                        <i class="fa fa-angle-double-down"></i>
                                                        <span id=span-'.$list['postid'].'-downvote> '.$list['downvotes'].'</span>
                                                    </a>
                                                </li>
                                            </ul>  
                                        </td>

                                        <td colspan="4" style="width:70%;">                                           
                                        <h3 style="padding:0; margin:0;">');

										//Codeline made by Julio Velasquez at 20160523 in order to make the path to question's link
    									$ref = str_replace($this->rooturl, "?qa=", $this->rooturl) . $list["postid"] . "/" . preg_replace("/[\s_]/", "-", $list["title"]);                                        

                                        $context->output('
                                        <a href="' . $ref . '" style="color:black;">' . $list['title'] . '</a></h3>
                                            <p style="margin:0 0 5px 0; font-size:90%;">asked <strong>' . date("M d", strtotime($list['created'])) . '</strong> in <strong>' . $list['cattitle'] . '</strong> </p>
                                            <p id="more" style="padding:0; margin:0;">
                                                <span class="more">');

                        //start answer section                            
                                if ($mostQueryans[$i] != NULL) {
                                    $context->output('Last Answer: ' . $mostQueryans[$i][0]['content']);
                                }
                                else {
                                    $context->output('No Answer');
                                }
                        //end answer section                                                            

                    			$context->output('</span></p>');

                    			$relatedqlink = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/ask?follow=' . $list['postid'] . '&cat=' . $list['categoryid'];

/*                    
				//start section username
                    $fetchusername = qa_db_read_all_assoc(qa_db_query_sub("select handle from qa_users where userid='".$list['userid']."' "));
         
                    foreach ($fetchusername as $userlist) {
                          // $context->output('by <strong>' . $userlist['handle'] . '</strong>');
                    }
				//end section username
*/				
                    
                    $context->output('</p>
                                            <ul class="list-inline">
                                                    <li class="active">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-pencil"></i>
                                                            <span>
                                                               <a href=' . $ref . '>Answer</a>
                                                             </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                                <i class="fa fa-bars"></i>
                                                            <span>
                                                                <a href=' . $relatedqlink . '>Ask related question</a>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>															
                                                        <div class="tooltip-social">
                                                         	<a href="#">Share</a>
                                                            <div class="tooltip-inner tooltip-inner3">
                                                            	<div class="social_icon facebook_icon">
                                                                	<a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                                                </div>
                                                                <div class="social_icon twitter_icon">
                                                        			<a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon google_icon">
                                                        			<a href="#"><i class="fa fa-google fa-2x"></i></a>
                                                        		</div>
                                                        		<div class="social_icon instagram_icon">
                                                        			<a href="#"><i class="fa fa-envelope fa-2x"></i></a>
                                                        		</div>
                                                            </div>
                                                         </div>                                                 				
													</li>
                                            </ul>
                                    </td>
                                    <td style="width:10%;">
                                    	'.ShowWantAnswer($countans[$i], $list['postid'], $ref).'
                                        <div class="tooltip-custom">
                                            <i class="fa fa-ellipsis-h"></i>
                                                    <div class="tooltip-inner">
                                                        <a href="#">Invite authoritative user to answer</a><br>
                                                        <a href="#">Add to Reading List</a><br>
                                                        <a href="#">Show Related Questions</a><br>
                                                        <a href="#">Create Summary Answer</a><br>
                                                        <a href="#">Mark Favorite</a><br>
                                                        <a href="#">Report</a>
                                                    </div>

                                        </div>
                                    </td>
                                    <td style="width:10%;">
                                        <a href="javascript:void(0)" class="btn btn-red">
                                            <i class="fa fa-circle"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
                </div>');
		$i+=1;
	}

	$unans_countque = listview_questions;
	$unans_listview_firsttime = $_SESSION['unans_listview_firsttime'];

	$context->output('<input type="hidden" id="unans_countque" value="' . $unans_countque . '">
                        <input type="hidden" id="unans_index" value="' . $listview_unansindex . '">
						<input type="hidden" id="unans_listview_firsttime" value="' . $unans_listview_firsttime . '">');

	$context->output('</div></div></div>');

	$context->output('<div class="more_questions" id="more_questions_unans"><p class="more_text">To see more questions, click 
    					<a class="questions" id="more_link_unans" onClick="more_questions_listview_unans();" href="javascript:void(0);"><b class="red">here.</b></a></p>
    				  </div>');

	$context->output('</div>');


	$ajaxlinkvotes = $this->rooturl . 'updownvote.php';
	$ajaxlink = $this->rooturl . 'header_filters.php';

        $root = qa_path_to_root();
        $url = $this->rooturl;
        $ajaxlink = str_replace($root, "",$url);
        $context->output("<input type = 'hidden' value = '$ajaxlink' id = 'ajaxlink'>");

	$_SESSION['rooturl'] = $this->rooturl;

    }

}