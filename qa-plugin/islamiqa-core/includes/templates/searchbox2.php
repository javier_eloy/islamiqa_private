<?php
include "askbox.php";

class Rendeder_searchbox
{

    public function render($class='ask_question_bar')
    {
	
	     $askbox=(new Renderer_askbox())->render();
		 
         return "<div class='container' >                        
                        <div class='{$class}'>
						     <div class='bar_center'>
                                <input type='text'  class='ask_question_input form-control' id='title' onchange='qa_title_change(this.value);'  placeholder='ask a new question or search for existing questions' aria-describedby='basic-addon'>
                                <div class='button_ask' id='asknew' >
								      <p>Ask a Question</p>
								</div>
							 </div>	
						</div>	 
                </div>
				{$askbox}";
						
    }

}


