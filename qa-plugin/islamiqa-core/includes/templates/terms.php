<?php 

/// Created by: Javier Hernandez
/// Date: 28/07/2016

class Renderer_terms
{

    public function render($sidepanel) {
		
	$link_privacy_policy="<a class='nonblock' href='" . qa_path_html("privacy") . "'>Privacy Policy</a>";	
	
	$html="<div class='container_custom_page terms-page'>
					<div class='row'>
						<div class='col-sm-8'>
							<p class='heading'>Terms</p>
							<div class='in-content'>
							<p class='ct-main-head'>The following document outlines the terms of use of the Islamiqa website. Before using any of the Islamiqa services, you are required to read, understand, and agree to these terms.</p>
							<p class='ct-heading'>ACCEPTANCE OF TERMS</p> 
							<p class='ct-main-content2'>The web pages available at Islamiqa.com and all linked pages (“Site”), are owned and operated by Islamiqa, Inc. (“Islamiqa”) a corporation and is accessed by you under the Terms of Use described below (“Terms”).</p>
							<p class='ct-main-content2'>Please read these terms carefully before using the services. By accessing the site, viewing any content or using any services available on the site (as each is defined below) you are agreeing to be bound by these terms, which together with our {$link_privacy_policy}</p>                
							<p class='ct-heading'>DESCRIPTION OF SERVICE</p>
							<p class='ct-main-content2'>The Site is an online community which enables photographers and graphic artists to post photographs and images, share comments, opinions and ideas, promote their work, participate in contests and promotions, and access and/or purchase services from time to time made available on the Site (“Services”). Services include, but are not limited to, any service and/or content Islamiqa makes available to or performs for you, as well as the offering of any materials displayed, transmitted or performed on the Site or through the Services. Content (“Content”) includes, but is not limited to text, user comments, messages, information, data, graphics, news articles, photographs, images, illustrations, and software.</p>
							<p class='ct-main-content2'>Your access to and use of the Site may be interrupted from time to time as a result of equipment malfunction, updating, maintenance or repair of the Site or any other reason within or outside the control of Islamiqa. Islamiqa reserves the right to suspend or discontinue the availability of the Site and/or any Service and/or remove any Content at any time at its sole discretion and without prior notice. Islamiqa may also impose limits on certain features and Services or restrict your access to parts of or all of the Site and the Services without notice or liability. The Site should not be used or relied upon for storage of your photographs and images and you are directed to retain your own copies of all Content posted on the Site.</p>
							<p class='ct-heading'>RESGISTRATION</p>							
							<p class='ct-main-content2'>As a condition to using Services, you are required to open an account with Islamiqa and select a password and username, and to provide registration information. The registration information you provide must be accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your Islamiqa account.</p>
							<p class='ct-main-content2'>You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trade mark that is subject to any rights of another person or entity other than you without appropriate authorization, or a name that is otherwise offensive, vulgar or obscene.</p>
							<p class='ct-main-content2'>You are responsible for maintaining the confidentiality of your password and are solely responsible for all activities resulting from the use of your password and conducted through your Islamiqa account.</p>
							<p class='ct-main-content2'>Services are available only to individuals who are either (i) at least 18 years old, or (ii) at least 14 years old, and who are authorized to access the Site by a parent or legal guardian. If you have authorized a minor to use the Site, you are responsible for the online conduct of such minor, and the consequences of any misuse of the Site by the minor. Parents and legal guardians are warned that the Site does display photographs and images containing nudity and violence that may be offensive to some.</p>							
							<p class='ct-main-content2'>The Services are for use by individuals who are photographers and graphic artists. Accounts may not be opened by galleries, agents and other market intermediaries and entities who represent photographers and graphic artists or sell their works. If you do not qualify for registration you are not permitted to open an account or use the Services.</p>
							<p class='ct-heading'>USER CONDUCT</p>							
							<p class='ct-main-content2'>All Content posted or otherwise submitted to the Site is the sole responsibility of the account holder from which such Content originates and you acknowledge and agree that you, and not Islamiqa are entirely responsible for all Content that you post, or otherwise submit to the Site. Islamiqa does not control user submitted Content and, as such, does not guarantee the accuracy, integrity or quality of such Content. You understand that by using the Site you may be exposed to Content that is offensive, indecent or objectionable.</p>
							<p class='ct-main-content2'>As a condition of use, you promise not to use the Services for any purpose that is unlawful or prohibited by these Terms, or any other purpose not reasonably intended by Islamiqa. By way of example, and not as a limitation, you agree not to use the Services:</p>
							
							<ul class='ct-main-content'>
							<li>To abuse, harass, threaten, impersonate or intimidate any person;</li>
							<li>To post or transmit, or cause to be posted or transmitted, any Content that is libellous, defamatory, obscene, pornographic, abusive, offensive, profane, or that infringes any copyright or other right of any person;</li>
							<li>For any purpose (including posting or viewing Content) that is not permitted under the laws of the jurisdiction where you use the Services;</li>
							<li>To post or transmit, or cause to be posted or transmitted, any communication or solicitation designed or intended to obtain password, account, or private information from any Islamiqa user;</li>
							<li>To create or transmit unwanted ‘spam’ to any person or any URL</li>
							<li>To create multiple accounts for the purpose of voting for or against users’ photographs or images;</li>
							<li>To post copyrighted Content which doesn’t belong to you, with exception of Blogs, where you may post such Content with explicit mention of the author’s name and a link to the source of the Content;</li>
							</ul>
							<p class='ct-main-content2'>With the exception of accessing RSS feeds, you will not use any robot, spider, scraper or other automated means to access the Site for any purpose without our express written permission. Additionally, you agree that you will not: (i) take any action that imposes, or may impose in our sole discretion an unreasonable or disproportionately large load on our infrastructure; (ii) interfere or attempt to interfere with the proper working of the Site or any activities conducted on the Site; or (iii) bypass any measures we may use to prevent or restrict access to the Site;</p>
							<p class='ct-main-content2'>To artificially inﬂate or alter vote counts, blog counts, comments, or any other Service or for the purpose of giving or receiving money or other compensation in exchange for votes, or for participating in any other organized effort that in any way artificially alters the results of Services;</p>							
							<p class='ct-main-content2'>To advertise to, or solicit, any user to buy or sell any products or services, or to use any information obtained from the Services in order to contact, advertise to, solicit, or sell to any user without their prior explicit consent;</p>
							<p class='ct-main-content2'>To promote or sell Content of another person</p>
							<p class='ct-main-content2'>To sell or otherwise transfer your profile.</p>
							<p class='ct-main-content2'>To report a suspected abuse of the Site or a breach of the Terms (other than relating to copyright infringement which is addressed under “COPYRIGHT COMPLAINTS” below) please send written notice to Islamiqa at email: help@Islamiqa.com.</p>
							
							<p class='ct-main-content2'>You are solely responsible for your interactions with other users of the Site. Islamiqa reserves the right, but has no obligation, to monitor disputes between you and other users.</p>
							
							<p class='ct-heading'>CONTENT SUBMITTED OR MADE AVAILABLE FOR INCLUSION ON THE SERVICE</p>	

							<p class='ct-main-content2'>Please read this section carefully before posting, uploading, or otherwise submitting any Content to the site. By submitting content to the site you are granting Islamiqa a worldwide, Non exclusive license to use the content and are representing and warranting to Islamiqa That the content is owned or duly licensed by you, and that Islamiqa is free to publish, Distribute and use the content as hereinafter provided for without obtaining permission Or license from any third party.</p>
							<p class='ct-main-content2'>In consideration of Islamiqa’s agreement to allow you to post Content to the Site and Islamiqa’s agreement to publish such Content and for other valuable consideration the receipt and sufficiency of which are hereby expressly and irrevocably acknowledged, you agree with Islamiqa as follows:</p>
							
							<p class='ct-heading'>You acknowledge that:</p>	
							<p class='ct-main-content2'>By uploading your photographic or graphic works to Islamiqa you retain full rights to those works that you had prior to uploading.</p>
							<p class='ct-main-content2'>By posting Content to the Site you hereby grant to Islamiqa a non-exclusive, transferable, fully paid, worldwide license (with the right to sublicense) to use, distribute, reproduce, modify, adapt, publicly perform and publicly display such Content in connection with the Services. This license will exist for the period during which the Content is posted on the Site and will automatically terminate upon the removal of the Content from the Site;</p>
							
							<p class='ct-main-content2'>The license granted to Islamiqa includes the right to use your Content fully or partially for promotional reasons and to distribute and redistribute your Content to other parties, web-sites, applications, and other entities, provided such Content is attributed to you in accordance with the credits (i.e. username, profile picture, photo title, descriptions, tags, and other accompanying information) if any and as appropriate, all as submitted to Islamiqa by you;</p>							
							<p class='ct-main-content2'>Islamiqa uses industry recognized software and measures to restrict the ability of users and visitors to the Site to make high resolution copies of Content posted on the Site. Notwithstanding this, Islamiqa makes no representation and warranty that Content posted on the Site will not be unlawfully copied without your consent. Islamiqa does not restrict the ability of users and visitors to the Site to make low resolution or ‘thumbnail’ copies of Content posted on the Site and you hereby expressly authorize Islamiqa to permit users and visitors to the Site to make such low resolution copies of your Content; and</p>														
							<p class='ct-main-content2'>Subject to the terms of the foregoing license, you retain full ownership or other rights in your Content and any intellectual property rights or other proprietary rights associated with your Content.</p>														

							<p class='ct-heading'>You represent and warrant that:</p>	
							<p class='ct-main-content2'>You are the owner of all rights, including all copy rights in and to all Content you submit to the site;</p>														
							<p class='ct-main-content2'>You have the full and complete right to enter into this agreement and to grant to Islamiqa the rights in the Content herein granted, and that no further permissions are required from, nor payments required to be made to any other person in connection with the use by Islamiqa of the Content as contemplated herein; and</p>																					
							<p class='ct-main-content2'>The Content does not defame any person and does not infringe upon the copyright, moral rights, publicity rights, privacy rights or any other right of any person, or violate any law or judicial or governmental order.</p>																												
							<p class='ct-main-content2'>You shall not have any right to terminate the permissions granted herein, nor to seek, obtain, or enforce any injunctive or other equitable relief against Islamiqa, all of which such rights are hereby expressly and irrevocably waived by you in favour of Islamiqa.</p>																																			
							
							<p class='ct-heading'>PREMIUM SERVICES</p>	
							<p class='ct-main-content2'>Islamiqa provides premium services to holders of its 'Plus' or ‘Awesome’ (\"premium\") accounts. In addition to the general terms and conditions provided for by these Terms, the following terms and conditions apply specifically to premium account holders:</p>

							<ul class='ct-main-content'>
							<li>Services available to premium account holders are described on the account Upgrade page.</li>
							<li>Islamiqa may modify, suspend or discontinue Services provided to premium account holders at any time at its sole discretion and without prior notice.</li>
							<li>Islamiqa provides new premium account holders with a 14 day trial period and will provide a full refund if you choose to terminate your Awesome account within this period. If you choose to terminate your Awesome account after the 14 day trial period you will be responsible for paying your Awesome account fee up to and including the Termination Date.</li>
							<li>Any refund owing to you will be paid within 30 days of the Termination Date provided your request for termination included your username and full name as it appears in your PayPal account or on your credit card.</li>
							</ul>
							
							<p class='ct-heading'>FAIR USAGE POLICY</p>	
							<p class='ct-main-content2'>Islamiqa maintains a fair usage policy to ensure stable and fast service to all users.</p>
							<p class='ct-main-content2'>Free accounts are limited to a maximum of 20 new photographs/images per week and 2,000 photographs/images in total (approximately 60Gb of storage) and 1Gb of data transfer from profile and portfolios per month. Any additional usage may result in restrictions on your account including limited access to your portfolio or a requirement to upgrade if the limit is exceeded for several months.</p>
							<p class='ct-main-content2'>Premium accounts are limited to maximum of 1,000 new photographs/images per week 100,000 photographs/ images in total (approximately 3,000Gb of storage) and 10Gb of data transfer per month. Awesome accounts that exceed a limit for several months will be notified of their usage and restrictions may be imposed if usage is not corrected.</p>
														
							</div>
						</div>
						{$sidepanel}
					</div>
			</div>";
		return $html;
	
	
	}
}