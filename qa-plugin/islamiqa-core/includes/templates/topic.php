<?php 

/// Created by: Javier Hernandez
/// Date: 28/07/2016

require_once QA_INCLUDE_DIR.'app/blobs.php';

class Renderer_topic
{

    public function render($list) {
	
	
	 $list_topic=$this->html_topic($list);
	
	 $html="<div class='topic-section'>
					<div class='topic-list'>
					{$list_topic}
					</div>
				</div>";
				
     return $html;
	
	
	}
	
	private function html_topic($list)
	{
	  $html="";
	  $action = qa_self_html();
	  
	  foreach($list As $item)
	  {
	    $img=qa_get_blob_url($item['content'],true);
		$html.="<form class='align-inline' method='POST' action='{$action}' > 
					<div class='box-topic' style='background:#efefef url({$img}) no-repeat scroll center center/cover'>
						<p class='box-square underlined-topic'> {$item['word']}</p>
					</div>
					<input type='hidden' name='id' value={$item['tag']}>
					<input type='hidden' name='topic' value={$item['word']}>
					<input type='hidden' name='content' value={$item['content']}>
					<input type='submit' name='modify_topic' value='modify' class='modify-topic qa-form-wide-button'>
				</form>";
       }
	   return $html;
	}
}