<?php 

/// Created by: Javier Hernandez
/// Date: 28/07/2016

require_once QA_INCLUDE_DIR.'qa-app-blobs.php';

class Renderer_gallery
{

    public function render($list) {
	
	
	 $list_gallery=$this->html_gallery($list);
	
	 $html="<div class='gallery-section'>
					<div class='gallery-list'>
					{$list_gallery}
					</div>
				</div>";
				
     return $html;
	
	
	}
	
	private function html_gallery($list)
	{
	  $html="";
	  $action = qa_self_html();
	  
	  foreach($list As $item)
	  {
	    $img=qa_get_blob_url($item['content'],true);
		$html.="<form class='align-inline' method='POST' action='{$action}' > 
					<div class='box-gallery' style='background-image:url({$img})'>
						<p class='box-gallery-title'> {$item['word']}</p>
					</div>
					<input type='hidden' name='id' value='{$item['tag']}'>
					<input type='hidden' name='gallery' value='{$item['word']}'>
					<input type='hidden' name='content' value='{$item['content']}'>
					<input type='submit' name='modify_gallery' value='modify' class='modify-gallery qa-form-wide-button'>
				</form>";
       }
	   return $html;
	}
}