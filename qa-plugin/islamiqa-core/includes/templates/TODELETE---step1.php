<?php

/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 21/07/16
 * Time: 05:10 AM
 */
class Step1
{

    public function render()
    {


        return '
<!---------------------------------------------------------------------content------------------------------------------------------------------------>

<div class="container">
	<div class="row">
    	<div class="col-xs-12 slider-get">
        	<img class="dpn" src="./qa-theme/Islamiqa-as-dev/images/st0-slider.jpg">
        	<img class="dpb" src="./qa-theme/Islamiqa-as-dev/images/mb-st2-slider.jpg">        	
        </div>
    </div>
</div>
<div class="container">

    <div class="row border-arround">
    	<div class="col-xs-5 border-right">
        	<div class="scr">
        	<li class="st-li">
            	<div class="checkbox-step">
  <label><input type="checkbox" value="">Beliefs</label>
  <span class="fa fa-chevron-circle-right fa-lg"></span>
				</div>
                
            </li>
            
            <li class="st-li">
            	<div class="checkbox-step">
  <label><input type="checkbox" value="">Culture</label>
    <span class="fa fa-chevron-circle-right fa-lg"></span>
				</div>
              
            </li>
            
            <li class="st-li">
            	<div class="checkbox-step">
  <label><input type="checkbox" value="">Family</label>
  <span class="fa fa-chevron-circle-right fa-lg"></span>
				</div>
                
            </li>
            
            <li class="st-li">
            	<div class="checkbox-step">
  <label><input type="checkbox" value="">History</label>
    <span class="fa fa-chevron-circle-right fa-lg"></span>
				</div>
                
            </li>
            
            <li class="st-li">
            	<div class="checkbox-step">
  <label><input type="checkbox" value="">Sciences</label>
    <span class="fa fa-chevron-circle-right fa-lg"></span>
				</div>
              
            </li>
            
            <li class="st-li">
            	<div class="checkbox-step">
                    <label><input type="checkbox" value="">Beliefs</label>
                <span class="fa fa-chevron-circle-right fa-lg"></span>    
				</div>
                
            </li>
            
            </div>
            <li class="st-li tc"><span class="fa fa-angle-double-down"></span></li>
        </div>
        <div class="col-xs-7">
        	<div class="six-boxes-gs" style="background-image: url(./qa-theme/Islamiqa-as-dev/images/organitations.jpg);">
            	<div class="outer-text-b-six">
            	<p class="bottom-six-boxes-text">Banking</p>
                </div>
            </div>
           
            <div class="six-boxes-gs" style="background-image: url(./qa-theme/Islamiqa-as-dev/images/sciense.jpg);">
            	<div class="outer-text-b-six">
            	<p class="bottom-six-boxes-text">Banking</p>
                </div>
            </div>
            
            <div class="six-boxes-gs" style="background-image: url(./qa-theme/Islamiqa-as-dev/images/education.jpg);">
            	<div class="outer-text-b-six">
            	<p class="bottom-six-boxes-text">Banking</p>
                </div>
            </div>
            
            <div class="six-boxes-gs" style="background-image: url(./qa-theme/Islamiqa-as-dev/images/culture.jpg);">
            	<div class="outer-text-b-six">
            	<p class="bottom-six-boxes-text">Banking</p>
                </div>
            </div>
            
            <div class="six-boxes-gs" style="background-image: url(./qa-theme/Islamiqa-as-dev/images/education.jpg);">
            	<div class="outer-text-b-six">
            	<p class="bottom-six-boxes-text">Banking</p>
                </div>
            </div>
            
            <div class="six-boxes-gs" style="background-image: url(./qa-theme/Islamiqa-as-dev/images/organitations.jpg);">
            	<div class="outer-text-b-six">
            	<p class="bottom-six-boxes-text">Banking</p>
                </div>
            </div>
            
        </div>
        
        <div class="row">
    	<div class="col-sm-12 btn-np-right">
        	
            <button type="button" class="btn btn-previous np">Previous step</button>
            <button type="button" class="btn btn-next np">Next step</button>
        </div> 
         
    </div>
        
    </div>
    
    
    
    
</div>';


    }


}