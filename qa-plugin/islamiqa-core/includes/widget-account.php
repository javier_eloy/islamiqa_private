<?php 

class class_widget_accountsetup
{
	var $directory;
	var $urltoroot;
	
    public function load_module($directory, $urltoroot) {
        $this->directory = $directory;
        $this->urltoroot = $urltoroot;
    }
    
	function allow_template($template)
	{
		return true;
	}
	

	function allow_region($region)
	{
		if($region =='side')
			return true;
		return false;
	}
	
		
   function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
   {
		global $followcount;
		$userid=qa_get_logged_in_userid();
		
	    require $this->directory ."includes/templates/account_setup.php";
		
		
		if(empty($followcount)) $followcount=$this->follow_count($userid); 
		$picktopiccount=$this->picktopic_count($userid);
		$is_usericon=$this->has_avatar($userid);
		$is_expertise=$this->has_about($userid);
		$is_worksat=$this->has_worksat($userid);

		$account_setup =  (new Renderer_account())->render($followcount, 20,
														   $picktopiccount, 5,
														   $is_usericon,
														   $is_expertise,
														   $is_worksat);
		$themeobject->output($account_setup);
	   
   }  

   
   private function follow_count($userid)
   {
	  
	  $row=qa_db_single_select(qa_db_selectspec_count(qa_db_user_favorite_users_selectspec($userid)));
	  return $row['count'];
   }
   
   private function picktopic_count($userid)
   {
      $row=qa_db_single_select(qa_db_is_favorite_selectspec($userid, QA_FAVORITE_CATEGORY,null));
    
	  return (int) $row;
   }
	
   private function has_avatar($userid)
   {
		$userinfo=qa_db_select_with_pending(qa_db_user_account_selectspec($userid, true));
		
		return ($userinfo['avatarblobid'] != null && ($userinfo['flags'] && QA_USER_FLAGS_SHOW_GRAVATAR ) );
    }
	
	private function has_about($userid)
	{
	     global $userprofile;
		 
		 if(empty($userprofile)) $userprofile=qa_db_select_with_pending(qa_db_user_profile_selectspec($userid, true));
	 
		 return (!empty($userprofile['about']));
	}
	
	private function has_worksat($userid)
	{
	   global $userprofile;	
	   
	   if(empty($userprofile)) qa_db_select_with_pending(qa_db_user_profile_selectspec($userid, true));
	   
	   return (!empty($userprofile[QA_WORKS_AT]));
	}
	
}