<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 09/07/16
 * Time: 11:33 AM
 */


require_once QA_INCLUDE_DIR.'qa-app-users.php';
require_once QA_INCLUDE_DIR.'qa-app-posts.php';


class class_questions_page{
    
    
    var $directory;
    var $urltoroot;

    public function __construct()
    {}

    public function load_module( $directory, $urltoroot ){
        $this->directory = $directory;
        $this->urltoroot = $urltoroot;
    }

    public function match_request( $request ){
        return strpos($request,"questions_page")!==FALSE;
    }

    public function process_request( $request ){

        $qa_content = qa_content_prepare();
        unset($qa_content['widgets']);
        unset($qa_content['main']);
        unset($qa_content['direction']);
        unset($qa_content['logo']);
        unset($qa_content['sidebar']);
        unset($qa_content['sidepanel']);

       // ob_start();   // http://php.net/manual/es/function.ob-get-clean.php -- en el vps actual no funciono se agregor ua clase y render

        require_once($this->directory ."includes/data-core.php");
        require_once($this->directory ."includes/templates/searchbox2.php");
		require_once($this->directory ."includes/templates/questions.php");
        require_once($this->directory ."includes/templates/sidepanel.php");		
		
        $userid = qa_get_logged_in_userid();                
		$searchbox =  (new Rendeder_searchbox())->render();       
        $template= (new Render_question())->render((new data_core())->questions($userid,0,'recent',null,10));
        $sidepanel = (new Rendeder_side_panel_logged())->render(null,$this->urltoroot);


       // ob_end_flush();

        $qa_content['qa-searchbox'] = $searchbox;
        $qa_content['sidepanel'] = $sidepanel;

        $qa_content['custom']=  $template;
        $qa_content['custom-body'] ="allquestion";
        return $qa_content;
    }
}
	