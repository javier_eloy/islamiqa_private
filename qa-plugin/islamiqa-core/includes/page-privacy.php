<?php

require_once QA_INCLUDE_DIR.'qa-app-users.php';
require_once QA_INCLUDE_DIR.'qa-app-posts.php';


  class class_privacy
  {
    var $directory;
    var $urltoroot;
    
    public function __construct() {

    }
    
    public function load_module($directory, $urltoroot) {
        $this->directory = $directory;
        $this->urltoroot = $urltoroot;
    }
    
    public function match_request($request) {
        $parts = explode( '/', $request );
        return $parts[0] == 'privacy_page';
    }
    
    public function process_request($request) {

		require $this->directory ."includes/templates/searchbox2.php";
		require $this->directory ."includes/templates/privacy.php";
		require $this->directory ."includes/templates/sidepanelfaq.php";
		
        $qa_content = qa_content_prepare();
        unset($qa_content['widgets']);
        unset($qa_content['direction']);
        unset($qa_content['logo']);
        unset($qa_content['sidebar']);
        unset($qa_content['sidepanel']);
 		
          
        $searchbox =  (new Rendeder_searchbox())->render();
		$sidepanelfaq = (new Renderer_sidepanel_faq())->render();
		$body = (new Renderer_privacy())->render($sidepanelfaq);
		
		$qa_content['qa-searchbox'] =$searchbox;
		$qa_content['custom']= $body;  
		$qa_content['custom-body'] ="privacy";
            
    return $qa_content;
  }
}
	

/*
Omit PHP closing tag to help avoid accidental output
*/