<?php

require_once QA_INCLUDE_DIR.'qa-app-users.php';
require_once QA_INCLUDE_DIR.'qa-app-posts.php';


class class_faq_page
  {
    var $directory;
    var $urltoroot;
    
    public function __construct() {

    }
    
    public function load_module($directory, $urltoroot) {
        $this->directory = $directory;
        $this->urltoroot = $urltoroot;
    }
    
    public function match_request($request) {
        $parts = explode( '/', $request );
        return $parts[0] == 'faq_page';
    }
    
    public function process_request($request) {

       	require $this->directory ."includes/templates/searchbox2.php";
		require $this->directory ."includes/templates/faq.php";
		require $this->directory ."includes/templates/sidepaneltopic.php";
		
        $qa_content = qa_content_prepare();
        unset($qa_content['widgets']);
        unset($qa_content['direction']);
        unset($qa_content['logo']);
        unset($qa_content['sidebar']);
        unset($qa_content['sidepanel']);
 		
          
        $searchbox =  (new Rendeder_searchbox())->render();
		$sidepaneltopic = (new Renderer_sidepanel_topic())->render();
		$body = (new Renderer_faq())->render($sidepaneltopic);
		
		$qa_content['qa-searchbox'] =$searchbox;
		$qa_content['custom']= $body;  
		$qa_content['custom-body'] ="faq";
            
		return $qa_content;
  }
}
	

/*
Omit PHP closing tag to help avoid accidental output
*/