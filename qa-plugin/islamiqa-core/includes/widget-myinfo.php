<?php
	
require_once QA_INCLUDE_DIR.'qa-db-selects.php';	

class class_widget_myinfo
{
	var $directory;
	var $urltoroot;
	
    public function load_module($directory, $urltoroot) {
        $this->directory = $directory;
        $this->urltoroot = $urltoroot;
    }
    
	function allow_template($template)
	{
		return true;
	}
	

	function allow_region($region)
	{
		if($region =='side')
			return true;
		return false;
	}
	
		
   function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
   {
		
		require $this->directory ."includes/templates/myinfo.php";
		
		$ufollow=null;
		$ufollower=null;
		$ucategory=null;
		$umycategory = null;
		
		if(qa_is_logged_in()) 
		{
			$userid=qa_get_logged_in_userid();	
			$ufollow=$this->get_userfollow($userid);
			$ufollower=$this->get_userfollower($userid);
			$ucategory=$this->get_categories_resume();
			$umycategory = $this->get_mycategories($userid);
			
		}
	    	
		$myinfo =  (new Renderer_myinfo())->render($ufollow, $ufollower,$ucategory, $umycategory);
		$themeobject->output($myinfo);
		
	}
	
	private function get_userfollow($userid)
	/**
	*
	*/
	{
		
		$result=qa_db_single_select(qa_db_user_favorite_users_selectspec($userid));
		
		return $result;
	}
	
	private function get_userfollower($userid)
	/**
	*
	*/
	{
	   
		$source = '^users JOIN ^userpoints ON ^users.userid=^userpoints.userid JOIN ^userfavorites ON ^users.userid=^userfavorites.userid WHERE ^userfavorites.entityid=$ AND ^userfavorites.entitytype=$ ORDER BY ^users.handle';
		$arguments = array($userid, QA_ENTITY_USER);

		$qselect = array(
			'columns' => array('^users.userid', 'handle', 'points', 'flags', '^users.email', 'avatarblobid' => 'BINARY avatarblobid', 'avatarwidth', 'avatarheight'),
			'source' => $source,
			'arguments' => $arguments,
			'sortasc' => 'handle',
		);
		
		$result=qa_db_single_select($qselect);		
		
		return $result;
		
	}
	
	private function get_categories_resume()
	/**
	*
	*/
	{
	   // Get Top Parent
	    $i=0;
		$top_category=qa_db_select_with_pending(qa_db_category_nav_selectspec(null,true));
		$result = array();
		
		
		// Get siblings		
		foreach($top_category as $elem)
		{
		   $result[$i++] = array('categoryid' => $elem['categoryid'],
								 'parentid' => $elem['parentid'],
								 'title' => $elem['title'],
								 'qcount' => $elem['qcount']);
								
		   $siblings=qa_db_select_with_pending(qa_db_category_sub_selectspec($elem['categoryid']));		   
		   foreach($siblings as $sibling)
		   {
		      $result[$i++]=array('categoryid' => $sibling['categoryid'],
								 'parentid' => $elem['categoryid'],
								 'title' => $sibling['title'],
								 'qcount' => $sibling['qcount']);
			      
		   }
		   
		}
		
		return $result;
	}
	
	
	private function get_mycategories($userid)
	/**
	*
	*/
	{	
	  $selectspec['columns'][0] = '^categories.categoryid';
	  $selectspec['columns'][1] = '^categories.title';
	  $selectspec['columns'][2] = '^categories.qcount';
	  $selectspec['columns'][3] = '^categories.content';	 
	  $selectspec['columns'][4] = '^categorymetas.content As blobid';	 
	  $selectspec['source'] = '^categories JOIN ^userfavorites ON ^categories.categoryid=^userfavorites.entityid LEFT JOIN ^categorymetas ON  ^categorymetas.categoryid = ^categories.categoryid WHERE ^userfavorites.userid=$ AND ^userfavorites.entitytype=$ AND ^categorymetas.title=$';
	  $selectspec['arguments'][0] = $userid;
	  $selectspec['arguments'][1] = QA_FAVORITE_CATEGORY;
	  $selectspec['arguments'][2] = QA_CATEGORY_IMG;
	  $selectspec['sortasc'] = 'title';
	  
	  $mycategories = qa_db_select_with_pending($selectspec);
	   
	  return $mycategories;
		
	}
	
}

