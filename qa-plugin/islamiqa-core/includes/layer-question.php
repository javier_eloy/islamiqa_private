<?php 

class qa_html_theme_layer 
		extends qa_html_theme_base
{


	
	public function main()
	{	
		$this->content['title']= $this->translate_title($this->content['title']);

	    qa_html_theme_base::main();		
	}


	public function widgets($region, $place)
	{
			
		if($this->request == "questions" || $this->request == "unanswered" )
		{

		// Add Fixed Widgets
		 if( $region == "full" && $place == "high")
		 {
			 $module = qa_load_module('widget',QA_WIDGET_SEARCHBOX);
			 $this->content['widgets'][$region][$place][] = $module;

		 }
		 if($region == "side" && $place == "high")
		 {   $module_side = array();
			 $more_side = array();
			 // Remove internal categories navigation
			 unset($this->content['navigation']['cat'])	;
			 
			 if (in_array($region,$this->content['widgets'])) 
				 if (in_array($region,$this->content['widgets'][$region])) 
				   $more_side = $this->content['widgets'][$region][$side];
			  
	 		 $module_side[] = qa_load_module('widget', QA_WIDGET_ACCOUNT); 	
			 $module_side[] = qa_load_module('widget',QA_WIDGET_MYINFO); 	 
			 $this->content['widgets'][$region][$place] = $module_side + $more_side;
		 }
		}
			
		qa_html_theme_base::widgets($region,$place);
		
	}

   /****
    
	**/
	private function translate_title($source)
	/**
	*
	*/
	{
	   switch ($source) {
		    case 'Hot questions': 
			    return 'Popular questions';				
			case 'Recent questions':
			    return '<div class="qa_filter_home"> 
								<ul class="type_filter clearfix"> 
								<li>
									<div class="arrow_up"><i class="fa fa-caret-up"></i></div>
									<div class="arrow_down"><i class="fa fa-caret-down"></i></div>
							   </li>
								<li><div>Home</div></li>
							   </ul>
						   </div>
					   ';
			default: 
			    return $source;
		 }	
	}
	
}