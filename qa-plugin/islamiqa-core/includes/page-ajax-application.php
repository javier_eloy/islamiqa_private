<?php 
ini_set('display_errors', '0');
error_reporting(E_ERROR | E_PARSE);

require_once QA_INCLUDE_DIR .'qa-base.php';
require_once QA_INCLUDE_DIR . 'qa-db.php';
require_once QA_INCLUDE_DIR . 'qa-app-users.php';
require_once QA_INCLUDE_DIR . 'qa-app-posts.php';

require_once ISLAMIQA_PLUGIN_BASE_DIR.'/includes/data-core.php';



class class_ajax{

    private $directory;


    public function load_module($directory, $urltoroot)
    {
        $this->directory =$directory;
    }


    public function match_request($request)
    {
	
	   switch ($request)
	   {
	
	       case "qajax-question":
		   case "qajax-points":
		   case "qajax-votes" : 
		   case "qajax-tags":
		   case "qajax-favorite":
		   case "qajax-islogged":
		   case "qajax-autocomplete":
		   case "qajax-newquestion":
		   case "qajax-newanswer":
		   case "qajax-favorite":
		   case "qajax-bestanswer":
				$isMatch=true;
				break;
		   default:
				$isMatch = false;
				break;
	   }
	   
	   return $isMatch;
    }


    public function process_request($request)
	/**
	*  Route to action
	*/
    {
	
	  switch ($request) {		  
			case 'qajax-question' : $this->question(); break;		
	        case 'qajax-points' : $this->userpoints(); break;
			case 'qajax-votes' : $this->vote(); break;
			case 'qajax-tags' : $this->tags(); break;
			case 'qajax-favorite': $this->favorite(); break;
			case 'qajax-bestanswer': $this->best_answer(); break;
			case 'qajax-islogged' : $this->islogged(); break;
			case 'qajax-newquestion' : $this->newquestion(); break;
			case 'qajax-autocomplete' : $this->autocomplete(); break;
			case 'qajax-newanswer': $this->newanswer(); break;
		  }
	
    }


     protected function question() 
	 /**
	 *  Get question ajax request
	 */
	 {
//        $categoryslugs = qa_request_parts(1);
        $categoryslugs = qa_get('category');
        $countslugs = count($categoryslugs);

        $sort = ($countslugs && !QA_ALLOW_UNINDEXED_QUERIES) ? null : qa_get('sort');
        $start = qa_get_start();
        $userid = qa_get_logged_in_userid();
        $pageSize = qa_get('pageSize');
        $pageSize = isset($pageSize) ? (int)$pageSize : 10;

        $questions= (new data_core())->questions($userid,$start,$sort,$categoryslugs,$pageSize);
        header('Content-Type: application/json');
        print json_encode(array("result"=>$questions));
	  }
	  
	  
	  protected function favorite()
	  /***
	  *
	  */
	  {
		header('Content-Type: application/json');
		
        $entitytype=qa_post_text('entitytype');
        $entityid=qa_post_text('entityid');
        $favorite=qa_post_text('favorite');
        $userid=qa_get_logged_in_userid();
		
		

        if (!(isset($entitytype) || !isset($entityid) ||  !isset($favorite))) {
            $result['reason'] = 'missing required fields.';
			$result['success'] = false;
        } else if(!isset($userid))  {
            $result["reason"]='Please login or register to check favorite.';
			$result['success'] = false;
        } else {
			$newfav_value = ($favorite == 0 ) ? 1 : 0;
            $post= (new data_core())->qa_db_favorite($userid, qa_get_logged_in_handle(), qa_cookie_get(), $entitytype, $entityid, $newfav_value);

			if ($favorite != $post)		
			{
				$result["value"]= $post;
				$result["success"]= true;
			} else 
			{
				
				$result["reason"]= "Could not set favorite";
			}
		
        }
  
		print json_encode($result);
 	  }
	 
	 
	  protected function userpoints() 
	  /**
	  *
	  */
	  {
	  
			$userid = qa_post_text('id');
			$questions= (new data_core())->user_points($userid);
			header('Content-Type: application/json');
			print json_encode(array("result"=>$questions));
	  }
	  
	
	   protected function best_answer()
	   /**
	   *
	   */
	   {
			$success = false;
			$questionid = qa_post_text('questionid');
			$answerid = qa_post_text('answerid');
			$userid = qa_get_logged_in_userid();
			$error = false;
			
			if(!isset($questionid) || !isset($answerid))
			{
			  $result['success'] = false;
			  $result['reason'] = 'Missing fields';
			  $error = true;
			} elseif(qa_is_logged_in() === FALSE)
			{
			  $result['success'] = false;
			  $result['reason'] = 'Please log in or register to change answer status';
			  $error = true;
			} 
			
			$datacore = new data_core();			
			if ($error == false)
			{
				$result['success'] = $datacore->qa_db_best_post($questionid, $answerid,$dberror);
				if($result['success'] == false) $result['reason'] = $dberror;
			}
		
		   print json_encode($result);
		}
	   
	   	   
	   protected function vote()
		/**
		*  Route to action
		*/
		{
		    $success = false;		
			$type = $_REQUEST['type'];
			$postid = $_REQUEST['id'];
			$userid = qa_get_logged_in_userid();
			$error=false;
			
			if(qa_is_logged_in() === FALSE && ($type == 'up' || $type == 'down')) 
			{	
		      $result['success'] = false;
			  $result['reason'] = 'Please log in or register to vote';
			  $error=true;
			}
			$datacore = new data_core();
			if($error == false)
			{
				switch($type)
				{
					  case 'up':
						$result = $datacore->qa_db_votes($postid, $userid, "up");
						if ($result['numnetvote'] > 0) $success = true;
												  else $result['reason'] = 'You have reached your vote limits.';
						$result['success'] = $success;
						break;
						
					  case 'down':
						$result = $datacore->qa_db_votes($postid, $userid, "down");
						if ($result['numnetvote'] > 0) $success = true;
												  else $result['reason'] = 'You have reached your vote limits.';
						$result['success'] = $success;
						break;
						
					 case 'wantanswer':
						$userid = $_REQUEST['userid'];
						$result = $datacore->qa_wantanswer_set($userid, $postid);
						break;
				}
			}
		   print json_encode($result);
		}
	
	
	  protected function tags()
	  /**
	  *
	  */
	  {

		$title = $_REQUEST['title'];
		$words = array();
		$keywords = array();

		$words = explode(" ",$title);
		$n = count($words);

		for ($i=0; $i < $n; $i++) { 
			$sql = "SELECT word FROM ^words WHERE tagcount > 0 AND word = '" . $words[$i] . "'";
			$keywords[$i] = qa_db_read_all_assoc(qa_db_query_sub($sql));
		}
		print json_encode($keywords);
	  }
	  
	  
		protected function newanswer()
		/***
		* Add new answer
		*/
		{
			require_once QA_INCLUDE_DIR.'db/metas.php';
			
			$type= QA_LIMIT_ANSWERS;	   
			$questionid= $_REQUEST['questionid'];
			$content= $_REQUEST['content'];
			$settings = $_REQUEST['setanswer'];

			$answer_anom = (array_search('answer_anom',$settings) !== FALSE) ? true : false;
			$notify_author= (array_search('notify',$settings) !== FALSE) ? true : false;
			$error = false;
			$userid = null;
			$format = '';
			$notify = false;

			$question=qa_db_select_with_pending(qa_db_full_post_selectspec($userid, $questionid));

			if( $content === "") 
			{
			
			  $result['reason'] = 'Answer empty is not allowed.';
			  $error=true;
			} 
			//	Check if the question exists, is not closed, and whether the user has permission to do this
			else if ((@$question['basetype']==QA_LIMIT_QUESTIONS) && (!isset($question['closedbyid'])) && 
				 !qa_user_post_permit_error('permit_post_a', $question, QA_LIMIT_ANSWERS)) 
				 {

				   // Get questions settings	
				   $question= qa_db_single_select(qa_db_full_post_selectspec(null, $questionid));
				   $additional=json_decode(qa_db_postmeta_get($questionid,'qa_q_extra'),true);

				   // Check if try to post answer anonymously on question did not accept
				   if($additional && in_array('answer_anonymously',$additional) === false && $answer_anom) {
						$result['reason'] = 'This question not allow anonymous answer';
						$error=true;
					} else if ($answer_anom === false )
						$userid=qa_get_logged_in_userid();
					
					// Check if notify to question's author
					if($additional && in_array('allow_notify',$additional) === true && $notify_author && !$error)
					{	
						if($question['userid'] == null )
						{
							$result['reason'] = 'You can not send mail to anonymouse question.';
							$error=true;
						} else {
							$notify=true;
						} 
					}
					// remove <p>, <br>, etc... since those are OK in text
					$htmlformatting = preg_replace('/<\s*\/?\s*(br|p)\s*\/?\s*>/i', '', $content);
					if (preg_match('/<.+>/', $htmlformatting)) {
						// if still some other tags, it's worth keeping in HTML
						$format='html';
						$content=qa_sanitize_html($content, false, true);
					}	
					
					
				   //	Try to create the new answer
				   if(!$error) {
						$answerid=qa_post_create($type, $questionid, null, $content, $format, null, null, $userid, $notify);
						$result['postid'] = $answerid;
				   }
				   
				 } else {
					 $result['reason'] = 'You do not access to post answers or the question is closed.';
					 $error=true;
				 }
				 
			$result['success'] = !$error;			 	   
			print json_encode($result);
		}	
		

		protected function newquestion() 
		/**
		*
		*/
		{
		
			if (!$this->checkRequest('cat_array','set_array','tag_array','fld_array')) 
				 return print json_encode(array('success' => false));


			$type = QA_LIMIT_QUESTIONS; // question
			$parentid = NULL; // does not follow another answer
			$format = ''; // plain text
			$category = json_decode($_REQUEST['cat_array'])[0]; //array of categories selected from new question dialog
			$settings = json_decode($_REQUEST['set_array']); //array of settings selected from new question dialog
			$tags = json_decode($_REQUEST['tag_array']); //array of tags added from selected from new question dialog
			$userid = NULL; //in case that the new question is set with option "post anonymously"

			$fld_array = json_decode($_REQUEST['fld_array']);
			$title = $fld_array[0];
			$content = $fld_array[1];
			$error = false;
			$uploaded = false;

			// Allow notify question
			$notify = (array_search('email',$settings) !== FALSE) ? true : false;

			$string_tags = qa_tags_to_tagstring($tags);
			// Allow answer anonymous does not support by Q2A. It will be save as extra information
			$answer_anonymously = (array_search('answer',$settings) !== FALSE) ? true : false;
			$additional=json_encode(array('answer_anonymously' => $answer_anonymously, 
										  'allow_notify' => $notify));


			// Post Anonymously this question
			$post_anonymously = (array_search('post',$settings) !== FALSE) ? true : false;
			

			if ($title == "" || $content == "")
			{
				$result['reason'] = 'Title and Question must have content.';
				$error=true;
			}
			// Check if upload an image file
			if (!$error && !empty($_FILES) && is_uploaded_file($_FILES['file_image']['tmp_name'])) { 
				$uploaded = true;	
				if(mime_content_type($_FILES['file_image']['type']) !== 'image/jpeg') 
				{
					 $result['reason'] = 'Could not upload image.';
					 $error = true;
				}
			}

			if (!$error && !$post_anonymously) {
				//check if an user is logged in
				if( qa_is_logged_in())	$userid = qa_get_logged_in_userid();		
								 else   {
									$result['reason'] = 'You need to be logged in to ask a new question.';
									$error=true;
								 }
			 }		
			 //create the new question 
			 if (!$error) {			
				$postid = qa_post_create($type, $parentid, $title, $content, $format, $category /* Categories id*/, $string_tags, $userid, 0, null, $additional);

				// Save categories as postmeta  -- NOTE: REMOVE AFTER CHECK IT IS NECESARY
				/*if($postid > 0) {
					require_once QA_INCLUDE_DIR.'db/metas.php';
					qa_db_postmeta_set($postid, QA_POST_CATEGORIES, implode(",",$category)); // Tips: Use FIND_IN_SET in MySQL
					(new data_core())->qa_db_update_qcategories(); // Update counts question cache by categories
				}*/
				$result['postid'] = $postid;
			 }	
			 
			 // Rename file to set posit.jpg format
			 if (!$error && $uploaded) {
			  $file_upload = ISLAMIQA_PLUGIN_BASE_DIR . '/upload/'.$result['postid'].'.jpg';
			  if (!move_uploaded_file($_FILES['file_image']['tmp_name'], $file_upload)) 
			  {
				$result['reason'] = 'Could not set image to question.';
				$error = true;
			  }
			}
		 
			$result['success'] = !$error;
			print json_encode($result);		
		}
		
		
	 protected function islogged() 
	  /**
	  * Return true or false wheter is loogged
	  */
	  {
			if( qa_is_logged_in() ) {
				$result['islogged'] = TRUE;
				$result['userid'] = qa_get_logged_in_userid();
			}
			else {
				$result['islogged'] = FALSE;
			}
			print (json_encode($result));
	  
	}		
	
	/*----------------------------------------------------------------------------------------------*/
	/*-----------------------------------------  INTERNAL FUNCTION ---------------------------------*/
	/*----------------------------------------------------------------------------------------------*/
	private function autocomplete() 
	/*
	* Created by Julio Velasquez
	* Date: 13/06/2016
	* Time: 16:45
	*/
	/*
	* Updated by Julio A. Velasquez Basabe
	* Date: 04/07/2016
	* Time: 17:48
	*/
	{

		require_once QA_INCLUDE_DIR . 'db/selects.php';

		  $search = $_GET['term'];
		  $result = array();
		  $data = array();

		  $sql = "SELECT postid, title FROM qa_posts WHERE type='Q' AND title LIKE '%" . $search . "%' ORDER BY created ASC";
		  $result = qa_db_read_all_assoc(qa_db_query_sub($sql));

		  foreach($result as $r) {
		   $data[] = array("value" => $r['postid'], "label" => $r['title']);
		  }
		  print json_encode($data);


	
	}
	

	private function checkRequest() 
	/**
	*  Check all requests 
	*/
    {
	   	$tags = func_get_args();
		foreach ($tags as $tag)
		{
		   if(!isset($_REQUEST[$tag])) return false;
		}
		
		return true;
    }
	 
	
	
}