<?php

class module_admin 
 {
 		function init_queries($tableslc) {
		
			/*$tablename = qa_db_add_table_prefix('islamiqa_topics_user_interest');
			
			// check if event logger has been initialized already (check for one of the options and existing table)
			require_once QA_INCLUDE_DIR.'qa-app-options.php';
			if(qa_opt('plugin_islamiqa_custom_pages') && in_array($tablename, $tableslc)) {
				// options exist, but check if really enabled
				if(qa_opt('plugin_islamiqa_custom_pages')=='' ) {
					// enabled database logging
					qa_opt('plugin_islamiqa_custom_pages', 1);
				}
			}
			else {
				// not enabled, let's enable the event logger
			
				// set option values for event logger
				qa_opt('plugin_islamiqa_custom_pages', '');
			
				if (!in_array($tablename, $tableslc)) {
					require_once QA_INCLUDE_DIR.'qa-app-users.php';
					require_once QA_INCLUDE_DIR.'qa-db-maxima.php';

					return 'CREATE TABLE IF NOT EXISTS ^islamiqa_topics_user_interest(
								id INT(11) AUTO_INCREMENT PRIMARY KEY,
								userid INT(10) NOT NULL,
								topicid INT(11) NOT NULL
							) ENGINE=MyISAM DEFAULT CHARSET=utf8';
				}
			}
			// memo: would be best to check if plugin is installed in qa-plugin folder or using plugin_exists()
			// however this functionality is not available in q2a v1.6.3
			
			// create table qa_usermeta which stores the last visit of each user
			$tablename2 = qa_db_add_table_prefix('usermeta2');
			if (!in_array($tablename2, $tableslc)) {
				qa_db_query_sub(
					'CREATE TABLE IF NOT EXISTS ^usermeta2 (
					meta_id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					user_id bigint(20) unsigned NOT NULL,
					meta_key varchar(255) DEFAULT NULL,
					meta_value longtext,
					PRIMARY KEY (meta_id),
					UNIQUE (user_id,meta_key)
					) ENGINE=MyISAM  DEFAULT CHARSET=utf8'
				);
			}

			// create table qa_postviews which stores the last visit of each user
			//add 01/03/2016 by Ronel Lezama
			$tablename3 = qa_db_add_table_prefix('qa_postviews');
			if (!in_array($tablename3, $tableslc)) {
				qa_db_query_sub(
					'CREATE TABLE IF NOT EXISTS ^postviews (
						postviewid int(10) NOT NULL AUTO_INCREMENT,
					  	postid int(10) unsigned NOT NULL,
					  	type enum("Q","A","C","Q_HIDDEN","A_HIDDEN","C_HIDDEN","Q_QUEUED","A_QUEUED","C_QUEUED","NOTE") CHARACTER SET utf8 NOT NULL,
					  	views int(11) NOT NULL,
					  	dateviews date NOT NULL,
					  PRIMARY KEY (postviewid),
					  KEY postid (postid),
					  FOREIGN KEY (postid) REFERENCES qa_post (postid)
					) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1' 
				);
			}

*/
		} // end init_queries

	
	function allow_template($template)
	{
		return ($template!='admin');
	}

	function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
	{
	}

	function option_default($option)
	{
		if ($option=='plugin_islamiqa_custom_pages')
			return 0;

		return null;
	}

	function admin_form(&$qa_content)
	{
		$saved=false;
		
		if (qa_clicked('plugin_islamiqa_custom_pages_save_button')) {
            qa_opt(ISLAMIQA_PLUGIN_NAME , (int)qa_post_text('plugin_islamiqa_custom_pages_field'));

			qa_opt('facebook_app_id', qa_post_text('facebook_app_id_field'));
            qa_opt('facebook_app_secret', qa_post_text('facebook_app_secret_field'));
            qa_opt('twitter_app_id', qa_post_text('twitter_app_id_field'));
            qa_opt('twitter_app_secret', qa_post_text('twitter_app_secret_field'));

            qa_opt('google_app_id', qa_post_text('google_app_id_field'));
            qa_opt('google_app_secret', qa_post_text('google_app_secret_field'));

            qa_opt('linkedin_app_id', qa_post_text('linkedin_app_id_field'));
            qa_opt('linkedin_app_secret', qa_post_text('linkedin_app_secret_field'));

			$saved=true;
		}
		 
		$ready=strlen(qa_opt('facebook_app_id')) && strlen(qa_opt('facebook_app_secret'));
		return array(
			'ok' => $saved ? 'Islamiqa Custom Page settings saved' : null,
			
			'fields' => array(
				array(
					'label' => 'Activate plugin for custom pages in islamiqa theme',
					'type' => 'checkbox',
					'value' => (int)qa_opt(ISLAMIQA_PLUGIN_NAME),
					'suffix' => '',
					'tags' => 'NAME="plugin_islamiqa_custom_pages_field"',
				),
               array(
                    'label' => 'Facebook App ID:',
                    'value' => qa_html(qa_opt('facebook_app_id')),
                    'tags' => 'name="facebook_app_id_field"',
                ),

                array(
                    'label' => 'Facebook App Secret:',
                    'value' => qa_html(qa_opt('facebook_app_secret')),
                    'tags' => 'name="facebook_app_secret_field"',
                    'error' => $ready ? null : 'To use Facebook Login, please <a href="http://developers.facebook.com/setup/" target="_blank">set up a Facebook application</a>.',
                ),

                array(
                    'label' => 'Twitter App ID:',
                    'value' => qa_html(qa_opt('twitter_app_id')),
                    'tags' => 'name="twitter_app_id_field"',
                ),
                array(
                    'label' => 'Twitter App Secret:',
                    'value' => qa_html(qa_opt('twitter_app_secret')),
                    'tags' => 'name="twitter_app_secret_field"',
                    'error' => $ready ? null : 'To use Facebook Login, please <a href="http://developers.facebook.com/setup/" target="_blank">set up a Facebook application</a>.',
                ),

                array(
				'label' => 'Google App ID:',
                    'value' => qa_html(qa_opt('google_app_id')),
                    'tags' => 'name="google_app_id_field"',
                ),
                array(
                    'label' => 'Google App Secret:',
                    'value' => qa_html(qa_opt('google_app_secret')),
                    'tags' => 'name="google_app_secret_field"',
                    'error' => $ready ? null : 'To use Facebook Login, please <a href="http://developers.facebook.com/setup/" target="_blank">set up a Facebook application</a>.',
                ),


                array(
                    'label' => 'LinkedIn App ID:',
                    'value' => qa_html(qa_opt('linkedin_app_id')),
                    'tags' => 'name="linkedin_app_id_field"',
                ),
                array(
                    'label' => 'LinkedIn App Secret:',
                    'value' => qa_html(qa_opt('linkedin_app_secret')),
                    'tags' => 'name="linkedin_app_secret_field"',
                    'error' => $ready ? null : 'To use Facebook Login, please <a href="http://developers.facebook.com/setup/" target="_blank">set up a Facebook application</a>.',
                ),				
			),
            
			'buttons' => array(
				array(
					'label' => 'Save Changes',
					'tags' => 'NAME="plugin_islamiqa_custom_pages_save_button"',
				),
			),
		);
	}

}
 