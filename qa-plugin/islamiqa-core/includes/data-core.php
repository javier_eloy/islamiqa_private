<?php

/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 12/07/16
 * Time: 02:15 PM
 */
require_once QA_INCLUDE_DIR . 'qa-base.php';
require_once QA_INCLUDE_DIR . 'qa-db-selects.php';
require_once QA_INCLUDE_DIR . 'qa-db-favorites.php';
require_once QA_INCLUDE_DIR . 'qa-app-favorites.php';
require_once QA_INCLUDE_DIR . 'qa-app-format.php';
require_once QA_INCLUDE_DIR . 'qa-app-q-list.php';
require_once QA_INCLUDE_DIR . 'qa-app-users.php';
require_once QA_INCLUDE_DIR . 'qa-app-limits.php';
require_once QA_INCLUDE_DIR . 'qa-app-votes.php';
require_once QA_INCLUDE_DIR . 'qa-app-cookies.php';


class data_core
{


    public function questions($userid, $start, $sort, $categoryslugs = null, $pageSize = 10)
    {

        $countslugs = count($categoryslugs);
        $countslugs = count($categoryslugs);
        
        switch ($sort) {
            case 'hot':
                $selectsort = 'hotness';
                break;
            case 'recent':
                $selectsort = 'created';
                break;
            case 'votes':
                $selectsort = 'netvotes';
                break;
            case 'answers':
                $selectsort = 'acount';
                break;
            case 'views':
                $selectsort = 'views';
                break;
            default:
                $selectsort = 'created';
                break;
        }

        // search questios sor by criteria
        $result = qa_db_select_with_pending(
            qa_db_qs_selectspec($userid, $selectsort, $start, $categoryslugs, null, false, false, $pageSize),
            qa_db_category_nav_selectspec($categoryslugs, false, false, true),
            $countslugs ? qa_db_slugs_to_category_id_selectspec($categoryslugs) : null
        );

        $ready = [];
        // search answer of question and aditional information

        foreach ($result[0] as $key => $value) {
            $ready[] = $this->question($result[0][$key],$userid);
        }

        $result = [];
        $result['userid'] = $userid;
        $result['data'] = $ready;

        return $result;

    }


    private function question($question,$userid){

//        $questionid = $result[0][$key]['postid'];
        $questionid = $question['postid'];
        $admin = qa_get_logged_in_level();

        list($question, $childposts, $achildposts, $parentquestion, $closepost, $extravalue, $categories, $favorite) = qa_db_select_with_pending(
            qa_db_full_post_selectspec($userid, $questionid),
            qa_db_full_child_posts_selectspec($userid, $questionid),
            qa_db_full_a_child_posts_selectspec($userid, $questionid),
            qa_db_post_parent_q_selectspec($questionid),
            qa_db_post_close_post_selectspec($questionid),
            qa_db_post_meta_selectspec($questionid, 'qa_q_extra'),
            qa_db_category_nav_selectspec($questionid, true, true, true),
            isset($userid) ? qa_db_is_favorite_selectspec($userid, QA_ENTITY_QUESTION, $questionid) : null
        );

        $answer = [];
        foreach ($childposts as $answerKey => $answervalue) {
            $answer[] = $childposts[$answerKey];
        }

        $question['user_access'] = $admin;
        $question['countcomments'] = $this->count_comments($questionid)[0]['count_comments'];
        $question['countviwed'] = $this->count_viwed($questionid);
        $question['listTags'] = explode(",", $question['tags']);
        $question['answer'] = $answer;
        $question['favorite'] = $favorite;
        $question['time_ago'] = qa_when_to_html($question['created'], null);
        if (isset($question['avatarblobid'])) {
            $question['user_logo'] = qa_path_absolute('') . "?qa=image&qa_blobid=" . $question['avatarblobid'];
        } else {
//                $result[0][$key]['user_logo'] = "http://civ.iqsociety.org/wp-content/uploads/2014/11/profile_icon-252x300.png";
            $question['user_logo'] = "https://placeholdit.imgix.net/~text?txtsize=30&bg=f7f7f7&txt=&w=150&h=150";
        }
        
        return $question;
    }



    public function count_comments($postid)
    {

        return qa_db_read_all_assoc(qa_db_query_sub(
            "select " .
//            " (select count(*) from  ^posts as ans where ans.parentid = ^posts.postid and ans.type='A') as count_answ,  " .
            " (select count(*) from  ^posts as com where com.type='C' and parentid in( " .
            " (select ans.postid from  ^posts as ans where ans.type='A' and ans.parentid = qa_posts.postid))) as count_comments " .
            " from   ^posts where type='Q' and  postid =  " . $postid));

    }

    public function count_viwed($postid)
    {


        $query = "SELECT COUNT(postid) as count FROM ^userviewedquestions WHERE  postid = #";
        $result = qa_db_read_one_assoc(qa_db_query_sub($query, $postid), true);
        return $result['count'];

    }
	

	public function qa_wantanswer_set($userid, $postid)
	/**
	*
	*/
	{
        
        $query = "SELECT COUNT(postid) as count FROM ^userwantanswer WHERE userid = #  AND postid = #";
        $result = qa_db_read_one_assoc(qa_db_query_sub($query, $userid, $postid ), true);
        $count = $result['count'];

        
        if ($count == 0){
            $query = "INSERT INTO ^userwantanswer (userid, postid) VALUES (#,#)"; 
            qa_db_query_sub($query, $userid, $postid );
        }
        $query = "SELECT COUNT(postid) as count FROM ^userwantanswer WHERE postid = #";
        $result = qa_db_read_one_assoc(qa_db_query_sub($query, $postid ), true);
        $count = $result['count'];
        $row['count'] = $count;
        return $row;
    }    
	
	
	public function qa_db_votes($postid, $userid, $type )
	/**
	*
	*/
	{
		
		if ($type == "up"){
				$query = "INSERT INTO ^uservotes (postid, userid, vote, flag) VALUES (".$postid.", ".$userid.", 1, 0) ON DUPLICATE KEY UPDATE vote=1";     
				$numrows =  qa_db_affected_rows(qa_db_query_sub($query));
		}
		
		
		if ($type == "down"){
				$query = "INSERT INTO ^uservotes (postid, userid, vote, flag) VALUES (".$postid.", ".$userid.", -1, 0) ON DUPLICATE KEY UPDATE vote=-1";     
				$numrows =  qa_db_affected_rows(qa_db_query_sub($query));
			} 

			$query = "SELECT count(vote) as  upvote FROM ^uservotes WHERE  vote > 0  and postid = $postid";    
			$result = qa_db_read_one_assoc(qa_db_query_sub($query ), true);
			$upvote = $result['upvote'];

			
			$query = "SELECT count(vote) as  downvote FROM ^uservotes WHERE  vote < 0  and postid = $postid";    
			$result = qa_db_read_one_assoc(qa_db_query_sub($query ), true);
			$downvote = $result['downvote'];
			
			$netvote = $upvote - $downvote;
			$query = " UPDATE qa_posts a  SET upvotes = $upvote,  downvotes = $downvote, netvotes = $netvote WHERE a.postid = $postid ";    
			$numrows =  qa_db_affected_rows(qa_db_query_sub($query));
			
			$row['upvote'] = $upvote;
			$row['downvote'] = $downvote;
			$row['netvote'] = $netvote;
			$row['numnetvote'] = $numrows;
			
			return $row;
		
	}

  /*  public function vote($userid, $postid, $vote ,$post_parent)
    {

        // $post_parent = isset($post_parent) ? $post_parent : $postid;

        $post = qa_db_select_with_pending(qa_db_full_post_selectspec($userid, $postid));
        $qa_page_error_html = qa_vote_error_html($post, $vote, $userid, qa_request());

        if (!$qa_page_error_html) {
            qa_vote_set($post, $userid, qa_get_logged_in_handle(), qa_cookie_get(), $vote);
            $post = qa_db_select_with_pending(qa_db_full_post_selectspec($userid, $post_parent));
            $post = $this->question($post,$userid);
            return $post;
        }

        throw new Exception($qa_page_error_html);


    }*/

    public function qa_db_favorite($userid,$handle,$cookie, $entitytype,$entityid,$favorite)
	/**
	* Return state after mark as favorite
	*/
	{

       $this->qa_custom_favorite_set($userid,$handle,$cookie, $entitytype,$entityid,$favorite);	   
	   $result=qa_db_single_select(qa_db_is_favorite_selectspec($userid, $entitytype, $entityid));
       return $result;
        

    }
	
	public function qa_db_best_post($questionid, $answerid,&$error)
	/**
	*
	*/
	{
	require_once QA_INCLUDE_DIR . 'pages/question-view.php';
	require_once QA_INCLUDE_DIR . 'pages/question-submit.php';

	$userid=qa_get_logged_in_userid();

	list($answer, $question, $qchildposts, $achildposts)=qa_db_select_with_pending(
		qa_db_full_post_selectspec($userid, $answerid),
		qa_db_full_post_selectspec($userid, $questionid),
		qa_db_full_child_posts_selectspec($userid, $questionid),
		qa_db_full_child_posts_selectspec($userid, $answerid)
	);


		//	Check if there was an operation that succeeded

		if ((@$answer['basetype']=='A') && (@$question['basetype']=='Q')) 
		{

			$answers=qa_page_q_load_as($question, $qchildposts);

			$question=$question+qa_page_q_post_rules($question, null, null, $qchildposts); // array union
			$answer=$answer+qa_page_q_post_rules($answer, $question, $qchildposts, $achildposts);
			$result=qa_page_q_single_click_a($answer, $question, $answers, $achildposts, true, $error);
			if(!$result) $error = "You don't have permission to select the question, please refer to administrator";
			
			return $result;
			
		} else
			 $error = 'No base type recognize';
		 
		  return false;
		
	}

    public function user_points($userid)
	/**
	*
	*/
	{

        list($useraccount, $userprofile, $userfields, $userpoints, $userlevels, $navcategories, $userrank) =
            qa_db_select_with_pending(
               qa_db_user_account_selectspec($userid, $userid),
                 qa_db_user_profile_selectspec($userid, $userid),
                 qa_db_userfields_selectspec(),
                qa_db_user_points_selectspec($userid,$userid),
                qa_db_user_levels_selectspec($userid, $userid, true),
                qa_db_category_nav_selectspec(null, true),
                qa_db_user_rank_selectspec($userid,$userid)
            );

         $result=[];

        $result['userpoints']['voted_on_questions']= @$userpoints['qupvotes']+@$userpoints['qdownvotes'];
        $result['userpoints']['voted_on_answers']= @$userpoints['qupvotes']+@$userpoints['qdownvotes'];
        $result['userpoints']['gave_out_up_vote']= @$userpoints['qupvotes']+@$userpoints['aupvotes'];
        $result['userpoints']['gave_down_votes']= @$userpoints['qdownvotes']+@$userpoints['adownvotes'];
        $result['userpoints']['received_up_votes']=@$userpoints['upvoteds'];
        $result['userpoints']['received_down_votes']= @$userpoints['downvoteds'];
        $result['userpoints']['score']= @$userpoints['points'];
        $result['userpoints']['questions']= @$userpoints['qposts'];
        $result['userpoints']['answers']= @$userpoints['aposts'];
//        $result['userlevels'] = $userlevels;
//        $result['navcategories'] = $navcategories;
        $result['userrank'] = $userrank;



        return $result;

    }

 
		function qa_db_update_qcategories()
		/**
		 * Update categories using postmetas to save multiples one
		 * IMPORTANT: Call every time to add new question
		 * Return false if not complete
		 */
		{
			
		  return qa_db_query_sub("UPDATE ^categories tabcat
								 INNER JOIN (SELECT cat.categoryid, COUNT(post.postid) as qcount
								 FROM ^categories  cat
								 LEFT JOIN  (SELECT post.postid, postmeta.content
											FROM ^posts post
											INNER JOIN ^postmetas postmeta ON post.postid = postmeta.postid AND postmeta.title=$
											) As post 
								ON FIND_IN_SET(cat.categoryid, post.content) > 0
								GROUP BY cat.categoryid) catcount
							 ON catcount.categoryid = tabcat.categoryid
							 SET tabcat.qcount= catcount.qcount", QA_POST_CATEGORIES); 
		}


		public function qa_custom_favorite_set($userid, $handle, $cookieid, $entitytype, $entityid, $favorite)
		{
			
			switch($entitytype)
			{
			  case QA_FAVORITE_ANSWER:
			  case QA_FAVORITE_CATEGORY:
			  case QA_FAVORITE_KNOWLEDGE:
			  
					if ($favorite == 1)
						qa_db_favorite_create($userid, $entitytype, $entityid);
					else
						qa_db_favorite_delete($userid, $entitytype, $entityid);

				   $action=$favorite ? $entitytype.'_favorite' : $entitytype.'_unfavorite';
				   $params=array('entityid' => $entityid);			   
				   qa_report_event($action, $userid, $handle, $cookieid, $params);				
				   break;
				   
			  default:
			      qa_user_favorite_set($userid, $handle, $cookieid, $entitytype, $entityid, $favorite);
				  break;
			} 
						
		}
		
		
		public function qa_user_folllow($userid, $start, $limit)
		{
			
				$selectspec=array(
				'columns' => array('^users.userid', 'handle',  'flags',  'avatarblobid' => 'BINARY avatarblobid', 'favoriteuser', 'is_userfavorite'),
				'source' => "^users 
						LEFT JOIN (SELECT userid, COUNT(*) As favoriteuser FROM ^userfavorites WHERE entitytype=$ GROUP BY userid) y ON ^users.userid=y.userid 
						LEFT JOIN (SELECT entityid, userid, 1 As is_userfavorite FROM ^userfavorites WHERE entitytype=$ ) z ON  ^users.userid=z.entityid AND z.userid=#
						WHERE ^users.userid <> #							
						LIMIT #,#",
				'arguments' => array(QA_FAVORITE_USER, QA_FAVORITE_USER, $userid, $userid , $start, $limit),
				'arraykey' => 'userid'
				);
				$users=qa_db_select_with_pending($selectspec);
				
				return $users;
			
	   }
	   
	   	public function qa_user_folllower($userid, $start, $limit)
		{
			
				$selectspec=array(
				'columns' => array('^users.userid', '^users.handle',  '^users.flags',  'avatarblobid' => 'BINARY ^users.avatarblobid', 'favoriteuser'),
				'source' => "^users 
						INNER JOIN ^userfavorites  ON  ^userfavorites.entitytype=$ AND ^users.userid=^userfavorites.entityid AND ^userfavorites.userid=#
						LEFT JOIN (SELECT userid, COUNT(*) As favoriteuser FROM ^userfavorites WHERE entitytype=$ GROUP BY userid) y ON ^users.userid=y.userid 											
						LIMIT #,#",
				'arguments' => array(QA_FAVORITE_USER, $userid, QA_FAVORITE_USER, $start, $limit),
				'arraykey' => 'userid'
				);
				$users=qa_db_select_with_pending($selectspec);
				
				return $users;
			
	   }
	   
	   public function qa_get_list_gallery()
	   {
		  $result=qa_db_read_all_assoc(qa_db_query_sub("SELECT ^tagmetas.tag, ^tagmetas.content, REPLACE(^words.word, '_', ' ') As word
														FROM ^tagmetas INNER JOIN ^words ON CAST(^tagmetas.tag AS UNSIGNED) =  ^words.wordid   
														WHERE title=$", QA_GALLERY_IMG));
		
  		  return $result;	
	
	   }
}