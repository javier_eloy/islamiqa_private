<?php



class class_widget_searchbox
{
	var $directory;
	var $urltoroot;
	
    public function load_module($directory, $urltoroot) {
        $this->directory = $directory;
        $this->urltoroot = $urltoroot;
    }
    
	function allow_template($template)
	{
		return true;
	}
	

	function allow_region($region)
	{
		if($region =='full')
			return true;
		return false;
	}
	
		
   function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
   {
	    require $this->directory ."includes/templates/searchbox2.php";
		$searchbox =  (new Rendeder_searchbox())->render();
		$themeobject->output($searchbox);
	   
   }  

}