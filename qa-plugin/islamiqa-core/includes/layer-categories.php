<?php 
require_once QA_INCLUDE_DIR.'qa-db-selects.php';
require_once QA_INCLUDE_DIR.'qa-app-upload.php';
require_once QA_INCLUDE_DIR.'qa-app-blobs.php';
require_once QA_INCLUDE_DIR.'qa-db-metas.php';

class qa_html_theme_layer 
		extends qa_html_theme_base
{


	function doctype()
	{
	
	 if ($this->request == 'admin/categories' ) {
		$part=$this->content['form'];
		 // Edit first subcatergories to add image2wbmp

		$form=$this->save_imageform($part);
		if(isset($form)) $this->content['form_2'] = $form;
		 
		}
		
		qa_html_theme_base::doctype();
	}
	

	private function save_imageform($part)
	{
		if(/*@isset($part['hidden']['parent']) &&*/ @isset($part['hidden']['edit']))  
		 {	
		   $url = '';
		   $error = '';		   
		   $id=qa_post_text('edit') | qa_get('edit');
		   
		   $oldblobid=$this->get_image($id);
		   if(!empty($oldblobid)) $url=qa_get_blob_url($oldblobid,true);		   
		   $preview = $this->set_htmlblob($url);
	    

		   if(qa_clicked('category_image_save'))
		   {				
			   $inserted = $this->insert_image($id,$oldblobid);
			   if(!$inserted['success'] )  
			   {
				  $error = $inserted['reason'];
			   } else
				  $preview=$this->set_htmlblob(qa_get_blob_url($inserted['blobid'],true));  
			   
		   } else
			   $id=$part['hidden']['edit'];
	 
			
		    $form=array(
					'ok' => (!$error && qa_clicked('category_image_save')) ? 'Image Saved' : null,
					'notice' => " sadasdsa ",
					'style' => 'tall',
					'title' => '<a name="topic_image"></a> Category Image',
					'tags' =>  'action="'.qa_self_html().'#category_image" method="POST" enctype="multipart/form-data" ',
					'fields' => array(
						'image'=> array(
									'label' => "Title Image",
									'type' => "file",
									'tags'=> "NAME='category_image' ID='category_image' type='file' onchange='setdiv_image(this,\"#preview\");'  "
									),		
						'preview' =>  array(
									'label' => $preview,
									'tags' => " style='display:none' "	
								  ),		
							),
					'hidden' => array(
					       'edit' => $id,
					   ),
					'buttons' => array(
						array(
							'label' => qa_lang_html('main/save_button'),
							'tags' => 'NAME="category_image_save"',
						     ),
						),
				   );
				   
		     if($error)
			 {
				 $form['fields']['image']['error'] = $error;
			 }
			 
		 } else 
			 return null;
	    
		return $form;
	}
		
	private function insert_image($id, $oldblobid)
	/**
	*
	*/
	{
		$file=reset($_FILES);
		$rtn['success'] = false;
		$rtn['reason'] = "unknown";
		
		$file_error = $file['error'];
		// Note if $_FILES['file']['error'] === 1 then upload_max_filesize has been exceeded
		switch($file_error)		
		{
		   case 1: 
				   $rtn['reason'] = 'Max size exceded';
				   return $rtn;	
		   case 4:
					$rtn['reason'] = 'No Image File selected';
					return $rtn;			   
		}

		$result=qa_upload_file($file['tmp_name'], $file['name'], null, true);
	
		if ( empty($result['error']) )
		{		
		  $blobid = $result['blobid'];

		  // Insert tag with id into tagmetas
		  qa_db_categorymeta_set($id, QA_CATEGORY_IMG, $blobid);
		  
		  // Delete old blob
		  if(!empty($oldblobid)) qa_delete_blob($oldblobid);
		  
		  $rtn['blobid'] = $blobid;
		  $rtn['success'] = true;
		  
		} else
		  $rtn['reason'] = @$result['error'];
		
		return $rtn;
	}
	
	private function set_htmlblob($url)
	/**
	*
	*/
	{   
		if(isset($url)) $style= "style='background-image:url(./?qa=image&qa_blobid={$url}&qa_size=100)'";
				  else  $style= "";
				  
		return "<div id='preview' class='box-topic topic-image' {$style} ></div><br>";
	}
	
	private function get_image($id)
	{	
	   $blobid=qa_db_categorymeta_get($id, QA_CATEGORY_IMG);		   
	   return $blobid;
	}

}