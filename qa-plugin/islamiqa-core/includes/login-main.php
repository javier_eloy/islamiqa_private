<?php

class class_login_main
{
    var $directory;
    var $urltoroot;
    var $provider;

    function load_module($directory, $urltoroot, $type, $provider) {
        $this->directory = $directory;
        $this->urltoroot = $urltoroot;
        $this->provider = $provider;
    }


    public function match_source($source)
    {
        return $source=='hybridauth';
    }

  public function logout_html($tourl)
  {
        $app_id=qa_opt('facebook_app_id');
        if (!strlen($app_id))
            return;
        echo  '<li class="qa-item qa-logout"><a href="./index.php?qa=logout" class="qa-link">Logout</a></li>';
        //$this->facebook_html($tourl, true, 'menu');
    }

}