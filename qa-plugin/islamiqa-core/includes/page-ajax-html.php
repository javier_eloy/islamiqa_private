<?php



require_once QA_INCLUDE_DIR .'qa-base.php';
require_once QA_INCLUDE_DIR . 'qa-db.php';
require_once QA_INCLUDE_DIR . 'qa-app-users.php';
require_once QA_INCLUDE_DIR . 'qa-app-posts.php';
require_once QA_INCLUDE_DIR . 'qa-app-blobs.php';

require_once ISLAMIQA_PLUGIN_BASE_DIR.'/includes/data-core.php';


class class_ajax_html
{

    private $directory;


    public function load_module($directory, $urltoroot)
    {
        $this->directory =$directory;
    }


    public function match_request($request)
    {

	   switch($request)
	   {
	      case "qajax-html-answer":
		  case "qajax-html-gallery":
		  case "qajax-html-userfollow":
		  case "qajax-html-userfollower":
		     return true;
		}
	  return false;
    }


    public function process_request($request)
	/**
	*  Route to action
	*/
    {
	
	  $userid = qa_get_logged_in_userid();
	  switch ($request) {		  
			case 'qajax-html-answer' : $this->answerhtml(); break;
			case 'qajax-html-gallery' : $this->galleryhtml(); break;
			case 'qajax-html-userfollow': $this->follow_user($userid); break;
			case 'qajax-html-userfollower': $this->follower_user($userid); break;
		  }
	
    }
	
	private function galleryhtml()
	/**
	*
	*/
	{
					 
	 $datacore = new data_core();			
	 $gallery=$datacore->qa_get_list_gallery();
	 $html="";
		foreach($gallery as $item)
		{
			$idtag=$item['tag'];
			$content = $item['content'];
			$title= $item['word'];
			$blob_url=qa_get_blob_url($content,true);
			
			$html.="<div class='box-gallery' style='background-image:url({$blob_url})' data-image='{$blob_url}'> 	
						<p class='box-gallery-title'>{$title}</p>
					</div>";	
		}		
		print $html;
	}
	
	
	private function answerhtml()
	/**
	*
	*/
	{
		
		 header('Content-Type: text/html');
		 echo "<script src='",qa_path_html(""),"./qa-plugin/wysiwyg-editor/ckeditor/ckeditor.js?1.7.4'></script>";
		 echo "<link rel='stylesheet' href='",ISLAMIQA_PLUGIN_BASE_DIR_NAME,"/css/app/themestyle.css?1.7.4'>";
		 echo "<h2 class='editor'>Your answer</h2>";
		 echo "<textarea cols='40' id='qa_editor' name='qa_editor' rows='12' ></textarea>";
	     echo "<script>",
			   "CKEDITOR.replace( 'qa_editor', {defaultLanguage: 'en',language: '', width: '600px'} ); </script>",
			   "</script>";
		
	}
	
	private function follow_user($userid)
	/**
	*
	*/
	{
	 $html="";
	 $start = $_REQUEST["start"];
	 $offset = $_REQUEST["offset"];
	 
	 
	 $datacore = new data_core();			
	 $users=$datacore->qa_user_folllow($userid,$start,$offset);
	 foreach($users as $user)
	 {
	    $is_userfavorite = $user['is_userfavorite'] | 0;
		$title=($is_userfavorite == 1) ? "UnFollow" : "Follow";
		$fa_icon = ($is_userfavorite == 1) ? "fa-star" : "fa-star-o";
		$title_image=($user['avatarblobid'] != null) ? "style='background-image:url(".qa_get_blob_url($user['avatarblobid']).");'" : "";
		$favoriteuser = thousandsCurrencyFormat($user['favoriteuser'] | 0);
		$userflags = thousandsCurrencyFormat($user['flags'] | 0);
		$ufollowid = $user['userid'];
		
		$html.="<!-- User -->
				   <div class='userlist uid-{$ufollowid}'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner' {$title_image}></div>
					 </div>					
					 <div> 
						<div class='title-user'><i class='fa {$fa_icon} fa-yellow'></i>&nbsp;{$user['handle']}</div>
						<span class='follow-user'><i class='fa fa-group'></i>&nbsp;{$favoriteuser}</span>
						<span class='flag-user'><i class='fa fa-flag'></i>&nbsp;{$userflags}</span>
					 </div>
					 <button class='unfollow' data-uid='{$ufollowid}' data-status='{$is_userfavorite}' >{$title}</button>
				   </div>";
	 }
	 $result= array();
	 $count_user = count($users);	
	 $result['count'] = $count_user;
	 
	 if($count_user > 0) {
		$result['success'] = true; 
		$result['content'] = $html;
	 }  else  {
	    $result['success'] = false;
		$result['content'] = "Empty";		
	 }
		
	 
	 print json_encode($result);
	
	}
	
	private function follower_user($userid)
	/**
	*
	*/
	{
	 $html="";
	 $start = $_POST["start"];
	 $offset = $_POST["offset"];     
	 	 
	 $datacore = new data_core();			
	 $users=$datacore->qa_user_folllower($userid,$start,$offset);
	 foreach($users as $user)
	 {
		//$title=($user['is_user_favorite'] == 1) ? "UnFollow" : "Follow";
		$fa_icon = "fa-star";
		$title_image=($user['avatarblobid'] != null) ? "style='background-image:url(".qa_get_blob_url($user['avatarblobid']).");'" : "";
		$favoriteuser = thousandsCurrencyFormat($user['favoriteuser'] | 0);
		$userflags = thousandsCurrencyFormat($user['flags'] | 0);
		
		$html.="<!-- User -->
				   <div class='userlist'>
					 <div class='col-image-fixed'> 
						<div class='rounded-corner' {$title_image}></div>
					 </div>					
					 <div> 
						<div class='title-user'><i class='fa {$fa_icon} fa-yellow'></i>&nbsp;{$user['handle']}</div>
						<span class='follow-user'><i class='fa fa-group'></i>&nbsp;{$favoriteuser}</span>
						<span class='flag-user'><i class='fa fa-flag'></i>&nbsp;{$userflags}</span>
					 </div>
					 <!-- <button class='unfollow'>{$title}</button> -->
				   </div>";
	 }
	 
	 $result= array();
	 $count_user = count($users);	
	 $result['count'] = $count_user;
	 
	 if($count_user > 0) {
		$result['success'] = true; 
		$result['content'] = $html;
	 }  else  {
	    $result['success'] = false;
		$result['content'] = "Empty";		
	 }
	 	
	 print json_encode($result);	
	}

	
}