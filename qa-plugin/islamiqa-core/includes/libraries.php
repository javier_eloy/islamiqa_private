<?php
/*
	MODIFIED 24-12-2015 by Carlos Parra
*/
 

function calculate_weeks_ago($created_date)
/**
 *
*/
{	
	$today_in_weeks = date("W", strtotime(date('Y-m-d H:m:s')));
	$created_in_weeks = date("W", strtotime($created_date));

$current_year = date("o", strtotime(date('Y-m-d H:m:s')));
	$created_year = date("o", strtotime($created_date));

	if ($current_year === $created_year) {
		$ago = $today_in_weeks - $created_in_weeks;
	}
	elseif ($current_year > $created_year) {
		$years_diff = $current_year - $created_year;
		$weeks_diff = 53 * $years_diff;
		$ago = ($weeks_diff - $created_in_weeks) + $today_in_weeks;
	}
	else {
		echo "ERROR";
		return null;
	}
	return $ago;
}


function ShowWantAnswer($answercount, $postid = 0, $ref = "")
/**
*
*/
{
	require_once QA_INCLUDE_DIR . 'qa-db.php';
	require_once QA_INCLUDE_DIR . 'app/users.php';
	require_once QA_INCLUDE_DIR . 'app/options.php';

	$wantbutton = " <p class='table-countans'>".$answercount."</p>
            <a href='".$ref."'><p class='table-want-answers'>answers</p></a>
        </p>";

    if ($answercount > 0) {
        $wantbutton = "
        <p  class='table-countans'>".$answercount."</p>
            <a href='".$ref."'><p class='table-want-answers'>answers</p></a>
        </p>";
        
    }
    else {        
        $userid = qa_get_logged_in_userid();
        
        if (!isset($userid))
            $userid = 0;
        
        $wantanswer = qa_opt('plugin_want_answer');
        
        if ($wantanswer) {
            $linkwantanswer = "";

            if ($userid > 0) {
                $linkwantanswer = "<a href='#' onclick = 'wantAnswerClick(this, ".$userid.", ".$postid.");return false;'>";
            }
            $query = "SELECT COUNT(postid) as count FROM ^userwantanswer WHERE postid = #";
            $result = qa_db_read_one_assoc(qa_db_query_sub($query, $postid ), true);
        
            $count = $result['count'];
        
            $wantbutton = "<p class='table-countans'>".$count."</p>
                            $linkwantanswer                
                            <p class='table-want-answers'>want</br>
                            answers</p>
                            </p>";
            }
        }
        
     return $wantbutton;
}


  
  function qa_link_selected($path,$text,$selected) 
  /**
  * Function: Convert html path into link and select if requested
  */
  {
	require_once QA_INCLUDE_DIR . 'qa-db.php';
	require_once QA_INCLUDE_DIR . 'app/options.php';
  
	 $htmlpath=qa_path_html($path);
     $style= ($path == $selected) ?  "style='font-weight:900; color:#313131'" : "";
	   
	 return "<a href='{$htmlpath}' {$style} > {$text} </a>";
  }

  
  
function qa_hybrid_user_find_by_email($email)   
{
	require_once QA_INCLUDE_DIR . 'qa-db.php';

    require_once QA_INCLUDE_DIR.'qa-db.php';
    return qa_db_read_all_values(qa_db_query_sub(
        'SELECT userid FROM ^users WHERE email=$ and NOT exists (select userid from ^userlogins where ^users.userid =  ^userlogins.userid )',
        $email
    ));
}


function qa_hybrid_user_find_by_handle($handle)    
{
    require_once QA_INCLUDE_DIR.'qa-db.php';
    return qa_db_read_all_values(qa_db_query_sub(
        'SELECT userid FROM ^users WHERE handle=$ and NOT exists (select userid from ^userlogins where ^users.userid =  ^userlogins.userid )',
        $handle
    ));
}


function toUrl($to)
{
    require_once QA_INCLUDE_DIR.'qa-base.php';
    $tourl = qa_post_text($to);
    if (!strlen($tourl))
        $tourl = qa_path_absolute('');
    return $tourl;
}


function shrink_text($sobject, $nmax=10)
/**
* Reduce text and add point
*/
{
	
	if(strlen($sobject) > $nmax) {
		  return trim(substr($sobject,0,$nmax - 2))."...";
	}
	
	return $sobject;
}


function thousandsCurrencyFormat($num) 
/**
* Format number like K count
*/
{
  $x = round($num);
  $x_number_format = number_format($x);
  $x_array = explode(',', $x_number_format);
  $x_parts = array('k', 'm', 'b', 't');
  $x_count_parts = count($x_array) - 1;
  $x_display = $x;
  $x_display = $x_array[0] . ((int) @$x_array[1][0] !== 0 ? '.' . @$x_array[1][0] : '');
  $x_display .= @$x_parts[$x_count_parts - 1];
  return $x_display;
}