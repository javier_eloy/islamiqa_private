<?php



require_once QA_INCLUDE_DIR.'qa-app-users.php';
require_once QA_INCLUDE_DIR.'qa-app-posts.php';


class class_allquestion_page{
    
    
    var $directory;
    var $urltoroot;

    public function __construct()
    {}

    public function load_module( $directory, $urltoroot ){
        $this->directory = $directory;
        $this->urltoroot = $urltoroot;
    }

    public function match_request( $request ){
        return ( $request == "allquestion" );
    }

    public function process_request( $request ){

	    require $this->directory ."includes/templates/allquestion.php";
		
		$qa_content = qa_content_prepare();
		
	    $body =  (new Renderer_allquestion())->render();
   
		$qa_content=$this->loadfixedwidget($qa_content);
		$qa_content['custom']=$body;
		
        return $qa_content;
    }
	
	
	private function loadfixedwidget($qa_content)
	{
		
	  	  // WIDGET CALL: Load fixed widgets
		 
		 // Load top widget like ask box
		 $region = "full";
		 $place = "high";
		 
		 $module = qa_load_module('widget',QA_WIDGET_SEARCHBOX); 	 
		 $qa_content['widgets'][$region][$place][] = $module;
		 
		return $qa_content;
		// Load individual side   
	/*	$region = "side";
		$place = "high";
		
		
		$module=qa_load_module('widget', 'title');
		$qa_content['widgets'][$region][$place][] = $module;*/
	}
	
	
}