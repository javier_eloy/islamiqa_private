<?php

require_once(ISLAMIQA_PLUGIN_BASE_DIR ."/includes/thirdparty/hybridauth-2.6.0/hybridauth/Hybrid/Auth.php");
require_once(ISLAMIQA_PLUGIN_BASE_DIR."/includes/thirdparty/hybridauth-2.6.0/hybridauth/Hybrid/Endpoint.php");

class class_login
{
	
	
	private $directory;
    private $urlroot;
	
    public function load_module($directory, $urltoroot)
    {

        $this->directory = $directory;
		$this->urlroot = $urltoroot;
		
    }
	
	
	 public function match_request($request)
    {
        switch($request) {		
		 /** Facebook */
		 case 'facebook-login': 
		 /**Google */
		 case 'Google':
         case 'hauth.start=Google':
         case 'hauth.done=Google':
		 /**Linked In */
		 case 'hauth.done=LinkedIn':
		 case 'hauth.start=LinkedIn':
		 /** Natural */
		 case 'hauth.done=Natural': 
         case 'hauth.start=Natural':
		 /** Register */
		 case 'hauth.done=Register':
		 case 'hauth.start=Register':
		 /** Twitter */
		 case 'hauth.done=Twitter':
		 case 'hauth.start=Twitter':
		 /** Check Mail */
		 case 'check_mail':
		     return true;
		}
		
		return false;
    }

	
	
    public function process_request($request)
    {
	    $provider="";
		
	    switch ($request) 
		{   /** Facebook */
		   case 'facebook-login' : 
		        $provider='facebook';
				break;
		   /** Google */
		   case 'Google':
		   case 'hauth.start=Google':
           case 'hauth.done=Google':
				$provider='google';
				 break;
		   /** Linkedin */		 
		   case 'hauth.done=LinkedIn':
		   case 'hauth.start=LinkedIn':	 
			   $provider='linkedin';
			   break;
			/** Natural */   
		   case 'hauth.done=Natural': 
           case 'hauth.start=Natural':
			   $provider='natural';
			   break;
			/** Register */
		   case 'hauth.done=Register':
		   case 'hauth.start=Register':
		       $provider='register';
			   break;
			/**Twitter */
		   case 'hauth.done=Twitter':
		   case 'hauth.start=Twitter':
			   $provider='twitter';
			   break;
			/**Check Mail*/
		   case 'check_mail':
				$provider='checkmail';
				break;
		
		}
		
		if (isset($provider)) {
	       if (!session_id()) 
			   if (!session_start()) 
				    session_start();
			$provider='process_'.$provider;
			
			$this->$provider(); 
	     }
	

	}	
	
	
	/**
	*
	**/
	private  function process_facebook()
	/**
	*  Process request to faebook
	*/
    {

        if (!session_id()) {
            if (!session_start()) {
                session_start();
            }
        }

        require_once QA_INCLUDE_DIR.'app/users.php';
        require_once QA_INCLUDE_DIR.'qa-base.php';


        $app_id=qa_opt('facebook_app_id');
        $secret_id=qa_opt('facebook_app_secret');
        $url_auth= qa_path_absolute('facebook-login');


        if (isset($_REQUEST['hauth_start']) || isset($_REQUEST['hauth_done'])) {
            Hybrid_Endpoint::process();
        } else {
            try {
                $config_array = array(
                    "base_url" => $url_auth,
                    "providers" => array(
                        "Facebook" => array(
                            "enabled" => true,
                            "keys" => array("id" => $app_id, "secret" => $secret_id, 'cookie' => true),
                            "scope" => "email, user_about_me, user_birthday, user_hometown"  //optional.
                        ),
                    )
                );
                //email,name,verified,location,website,about,picture

                $hybridauth = new Hybrid_Auth($config_array);
                $facebook_adapter = $hybridauth->authenticate("Facebook");

                $facebook_user_profile = $facebook_adapter->getUserProfile();

                if ($facebook_user_profile) {
                    qa_log_in_external_user('facebook', $facebook_user_profile->identifier, array(
                        'email' => $facebook_user_profile->email,
                        'handle' => $facebook_user_profile->displayName,
                        'confirmed' => $facebook_user_profile->emailVerified,
                        'name' => $facebook_user_profile->displayName,
                        'location' => $facebook_user_profile->city . " " . $facebook_user_profile->region . $facebook_user_profile->country,
                        'website' => $facebook_user_profile->webSiteURL,
                        'about' => $facebook_user_profile->description,
                        'avatar' => strlen($facebook_user_profile->photoURL) ? qa_retrieve_url($facebook_user_profile->photoURL) : null,
                    ));

                }

                $tourl = qa_get('to');
                if (!strlen($tourl))
                    $tourl = qa_path_absolute('');

                qa_redirect_raw($tourl);


            } catch (Exception $ex) {

                echo $ex->getMessage();

            }
        }

    }
	
	
	 public function process_google()
	 /**
	 * Process requesto from google
	 */
    {

        require_once QA_INCLUDE_DIR.'app/users.php';
        require_once QA_INCLUDE_DIR.'qa-base.php';


        $app_id=qa_opt('google_app_id');
        $secret_id=qa_opt('google_app_secret');

        $url_auth= qa_path_absolute('Google');
        $config = array(
//            "base_url" => "http://localhost.com/index.php?qa=Google",
            "base_url" => $url_auth,
            "providers" => array(
                "Google" => array(
                    "enabled" => true,
                    "keys" => array("id" => $app_id, "secret" => $secret_id),
                    "scope" => "https://www.googleapis.com/auth/userinfo.profile " . // optional
                        "https://www.googleapis.com/auth/userinfo.email", // optional
                )));


        if (isset($_REQUEST['hauth_start']) || isset($_REQUEST['hauth_done'])) {
            Hybrid_Endpoint::process();
        } else {


            $hybridauth = new Hybrid_Auth($config);
    		$authProvider = $hybridauth->authenticate("Google");
	    	$user_profile = $authProvider->getUserProfile();

            if($user_profile && isset($user_profile->identifier))
            {

                qa_log_in_external_user('google', $user_profile->identifier, array(
                    'email' => $user_profile->email,
                    'handle' => $user_profile->displayName,
                    'confirmed' => true,
                    'name' => $user_profile->displayName,
                    'website' => $user_profile->profileURL,
                    'location'=>"",
                    'about'=>"",
                    'avatar' => strlen($user_profile->photoURL) ? qa_retrieve_url($user_profile->photoURL) : null,
                ));
            }

            $tourl = qa_get('to');
            if (!strlen($tourl))
                $tourl = qa_path_absolute('');

            qa_redirect_raw($tourl);

        }
    }
	
	public function process_linkedin()
	/**
	* Process Linkedin
	*/
    {

        require_once QA_INCLUDE_DIR.'app/users.php';
        require_once QA_INCLUDE_DIR.'qa-base.php';


        $app_id=qa_opt('linkedin_app_id');
        $secret_id=qa_opt('linkedin_app_secret');
        $url_auth= qa_path_absolute('hauth.start=LinkedIn');


        if (isset($_REQUEST['hauth_start']) || isset($_REQUEST['hauth_done'])) {
            Hybrid_Endpoint::process();
        } else {
            try {

                $config = array("base_url" => $url_auth,
                    "providers" => array (
                        "LinkedIn" => array (
                            "enabled" => true,
                            "keys"    => array ( "key" => $app_id, "secret" => $secret_id ),

                        )
                    ),
                    // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
                    "debug_mode" => false,
                    "debug_file" => "debug.log",
                );


                //email,name,verified,location,website,about,picture

                $hybridauth = new Hybrid_Auth($config);
                $facebook_adapter = $hybridauth->authenticate("LinkedIn");

                $user_profile = $facebook_adapter->getUserProfile();

                if($user_profile && isset($user_profile->identifier))
                {
                    qa_log_in_external_user('LinkedIn', $user_profile->identifier, array(
                        'email' => $user_profile->email,
                        'handle' => $user_profile->displayName,
                        'confirmed' => $user_profile->emailVerified,
                        'name' => $user_profile->displayName,
                        'location' => $user_profile->city . " " . $user_profile->region . $user_profile->country,
                        'website' => $user_profile->webSiteURL,
                        'about' => $user_profile->description,
                        'avatar' => strlen($user_profile->photoURL) ? qa_retrieve_url($user_profile->photoURL) : null,
                    ));

                }

                $tourl = qa_get('to');
                if (!strlen($tourl))
                    $tourl = qa_path_absolute('');

                qa_redirect_raw($tourl);


            } catch (Exception $ex) {

                echo $ex->getMessage();

            }
        }
  
	}
	
    private function process_natural()
	/**
	* Natural
	*/
    {

        require_once QA_INCLUDE_DIR.'app/users.php';
        require_once QA_INCLUDE_DIR.'db/users.php';
        require_once QA_INCLUDE_DIR.'qa-base.php';


        if (QA_FINAL_EXTERNAL_USERS)
            qa_fatal_error('User login is handled by external code');

        if (qa_is_logged_in())
            qa_redirect('');


        $inemailhandle=qa_post_text('emailhandle');
        $inpassword=qa_post_text('password');
        $inremember=qa_post_text('remember');


        if (qa_opt('allow_login_email_only') || (strpos($inemailhandle, '@')!==false)) // handles can't contain @ symbols
            $matchusers= qa_hybrid_user_find_by_email($inemailhandle);
        else
            $matchusers=qa_db_user_find_by_handle($inemailhandle);

        if (count($matchusers)==1) { // if matches more than one (should be impossible), don't log in
            $inuserid=$matchusers[0];
            $userinfo=qa_db_select_with_pending(qa_db_user_account_selectspec($inuserid, true));

            var_dump($userinfo);

            var_dump(strtolower(qa_db_calc_passcheck($inpassword, $userinfo['passsalt'])));
            var_dump(strtolower($userinfo['passcheck']));

            if (strtolower(qa_db_calc_passcheck($inpassword, $userinfo['passsalt'])) == strtolower($userinfo['passcheck'])) { // login and redirect
                require_once QA_INCLUDE_DIR.'app/users.php';

                qa_set_logged_in_user($inuserid, $userinfo['handle'], !empty($inremember));
                $tourl = toUrl("tologged");

            } else{

                $tourl = toUrl("toInvalid");
            }
                

        } else{
//            $errors['emailhandle']=qa_lang('users/user_not_found');
            $tourl = toUrl("toRegister");
        }
            
        qa_redirect_raw($tourl);
    }
 

    public function process_register()
	/**
	*   Process Register
	*/
    {
        if (qa_is_logged_in())
            qa_redirect('');

        require_once QA_INCLUDE_DIR . 'app/users.php';
        require_once QA_INCLUDE_DIR . 'qa-base.php';
        require_once QA_INCLUDE_DIR . 'app/limits.php';
        require_once QA_INCLUDE_DIR . 'app/users-edit.php';

        $inemail = qa_post_text('email');
        $inpassword = qa_post_text('password');
        $inhandle = qa_post_text('name');

        $matchusers= qa_hybrid_user_find_by_email($inemail);
        $errors = [];

        if (count($matchusers)==1) { // if matches more than one (should be impossible), don't log in
            $errors['email']=qa_lang('users/email_exists');
        }

            // core validation
        $errors = array_merge(
            $errors
         //   qa_password_validate($inpassword)
        );


        if (empty($errors)) {
            qa_limits_increment(null, QA_LIMIT_REGISTRATIONS);

            $userid = qa_create_new_user($inemail, $inpassword, $inhandle);
            qa_set_logged_in_user($userid, $inhandle);
            $tourl = toUrl("toRegister");


        } else {
            $tourl = toUrl("toInvalid");
        }

        qa_redirect_raw($tourl);

    }
	
	
	
	public function process_twitter()
	/**
	*  Process Twitter
	*/
    {

        require_once QA_INCLUDE_DIR.'app/users.php';
        require_once QA_INCLUDE_DIR.'qa-base.php';


        $app_id=qa_opt('twitter_app_id');
        $secret_id=qa_opt('twitter_app_secret');
        $url_auth= qa_path_absolute('hauth.start=Twitter');


        if (isset($_REQUEST['hauth_start']) || isset($_REQUEST['hauth_done'])) {
            Hybrid_Endpoint::process();
        } else {
            try {
                $config = array("base_url" => $url_auth,
                    "providers" => array (

                        "Twitter" => array (
                            "enabled" => true,
                            "includeEmail" => true,
                            "keys"    => array ( "key" => $app_id, "secret" => $secret_id )
                        ),
                    ),
                    // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
                    "debug_mode" => false,
                    "debug_file" => "debug.log",
                );

                //email,name,verified,location,website,about,picture

                $hybridauth = new Hybrid_Auth($config);
                $facebook_adapter = $hybridauth->authenticate("Twitter");

                $user_profile = $facebook_adapter->getUserProfile();

                if($user_profile && isset($user_profile->identifier))
                {
                    qa_log_in_external_user('twitter', $user_profile->identifier, array(
                        'email' => $user_profile->email,
                        'handle' => $user_profile->displayName,
                        'confirmed' => $user_profile->emailVerified,
                        'name' => $user_profile->displayName,
                        'location' => $user_profile->city . " " . $user_profile->region . $user_profile->country,
                        'website' => $user_profile->webSiteURL,
                        'about' => $user_profile->description,
                        'avatar' => strlen($user_profile->photoURL) ? qa_retrieve_url($user_profile->photoURL) : null,
                    ));

                }

                $tourl = qa_get('to');
                if (!strlen($tourl))
                    $tourl = qa_path_absolute('');

                qa_redirect_raw($tourl);


            } catch (Exception $ex) {

                echo $ex->getMessage();

            }
        }

    }
	
	 public function process_checkmail()
	 /**
	 * Check mail
	 */
    {

        header('Content-Type: application/json');

        if(isset($_REQUEST['email'])){
            $matchusers = qa_hybrid_user_find_by_email($_REQUEST['email']);
            print ('{"result": ' .count($matchusers) . ' , "message": "' . qa_lang('users/email_exists') . '"  }');
        }
        
        else if(isset($_REQUEST['handle'])){
            $matchusers= qa_hybrid_user_find_by_handle($_REQUEST['handle']);
            print ('{"result": ' .count($matchusers) . ' , "message": "' . qa_lang('users/handle_exists') . '"  }');
        }
        else{
            print ('{"result": 0, "message" : "( no info " }');
        }


    }	
	
	
}  /** End Class **/