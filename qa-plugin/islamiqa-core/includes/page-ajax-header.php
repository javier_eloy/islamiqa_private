<?php
/*
 * Modified by Javier Hernandez (All code changed)
 * Date: 30/08/2016
 */


require_once QA_INCLUDE_DIR .'qa-base.php';
require_once QA_INCLUDE_DIR . 'qa-db.php';
require_once ISLAMIQA_PLUGIN_BASE_DIR.'/includes/libraries.php';

	/*		
const posts_per_slide = 4;
const listview_questions = 10;*/

class class_ajax_header 
{
   
	private $directory;
    private $urltoroot;
	
    public function load_module($directory, $urltoroot)
    {
        $this->directory=$directory;
		$this->urltoroot=$urltoroot;
    }


    public function match_request($request)
    {

	   if ( $request == "qajax-header" )
		   return true;
	 
	  return false;
    }

	
    public function process_request($request)
	/**
	*  Route to action
	*/
	{

		$type = $_REQUEST['type'];
		$name = $_REQUEST['name'];
		$page = $_REQUEST['page'];		
		$view = $_REQUEST['view'];
		switch($view)
		{
		  case 'grid':  $page_size=4;
						break;
		  case 'list':  $page_size=10;
						break;
		}
		$this->html_format($type, $name, $page, $view,$page_size); 
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------*/
	/*--------------------------------------------- PRIVATE FUMCTIONS ---------------------------------------------*/
	/*-------------------------------------------------------------------------------------------------------------*/
	
	private function get_array_questions($current_page = 0, $pagesize = 4, $checkmore=false, $orders = null, $filter_date = null, $filter_additional = null)
	/**
	*
	*/
	{
	  $sql_order="";
	  $sql_where="";
	  $sql_additional="";
	  $sql_limit="";
	  
		 
	  if($current_page > 0)
	  {
		$start_page= ($current_page - 1) * $pagesize;
		if($checkmore) $pagesize += 1;
		$sql_limit=" LIMIT {$pagesize} OFFSET {$start_page}";
	  }
	  // Create order by
	  switch($orders)
	  {
	     case 'most': $sql_order = "ORDER BY netvotes DESC, upvotes DESC"; break;
		 case 'recently': $sql_order = "ORDER BY created DESC"; break;		
	  }
	  
	  // Create filter of dates 
	  switch($filter_date)
	  {
	     case 'week': $sql_where = " AND WEEKOFYEAR(created) = WEEKOFYEAR(NOW()) "; break;
		 case 'year': $sql_where = " AND YEAR(created) = YEAR(NOW()) "; break;
		 case 'month': $sql_where = " AND MONTH(created) = MONTH(NOW()) "; break;	  
		 case 'all': break; /*Nothing to do */
	  }
	  // Create aditional filter
	  switch($filter_additional)
	  {
		 case 'unanswered': $sql_additional =  " AND postid NOT IN(SELECT parentid FROM ^posts WHERE parentid != 'NULL') "; break;
	  }
	  
	  $sql_where .= $sql_additional;
	  
	  // Final SQL -------------------
	  $sql= " SELECT ^posts.*, ^categories.title AS cattitle 
					FROM ^posts 
					LEFT JOIN ^categories ON ^posts.categoryid = ^categories.categoryid 
				WHERE type='Q' {$sql_where}
				{$sql_order}
				{$sql_limit}";	   
				
	 $records = qa_db_read_all_assoc(qa_db_query_sub($sql));
	 
	 return $records;
		
	}
	
	private function get_array_answers($array_questions)
	/**
	*
	*/
	{	
	 $answer=array();
	 foreach($array_questions as $key => $question)
	 {
		
		$answer[$key]['answers'] = qa_db_read_all_assoc(qa_db_query_sub("SELECT * FROM ^posts WHERE type='A' AND parentid ='" . $question['postid'] . "' ORDER BY created DESC"));
		$answer[$key]['count']= count($answer[$key]['answers']);
		/*qa_db_read_one_value(
		    qa_db_query_sub("SELECT COUNT(*) FROM ^posts WHERE type='A' AND parentid ='" . $array_questions['postid'] . "' ORDER BY created DESC")
			);*/	
	 }
	  return $answer;
	}
	
	
	private function format_html_thumb_half($postid,$created, $cattitle, $title, 
											$answers, $countanswers, 
										    $upvotes, $downvotes, 
											$rooturl)
	/**
	*
	*/
	{
	
		$ref = str_replace($rooturl, "?qa=", $rooturl) . $postid . "/" . preg_replace("/[\s_]/", "-", $title);
		$answerhtml= ($answers != null) ? substr($answers[0]['content'],0,100) : "No Answer";
		$answertitle = $countanswers . (($countanswers > 1) ? " answers" : " answer" );
		
		$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/upload/' . $postid . '.jpg';							
		$imgexist = file_exists(ISLAMIQA_PLUGIN_BASE_DIR . '/upload/' . $postid . '.jpg');
		$htmlimage =  ($imgexist) ? "<!-- Now, set the image path as source of the img element --> 
								    <div class='thumb'><img class='half' alt='' src='{$imgpath}' ></div>" 
								  : "<!-- In this case just apply the style to the question background  -->
									<div class='thumb'></div>";

		$html="<div class='thumb_half'>
				<div class='thumb_hover half'>
					<div class='hover_overlay'>
						<div class='inner'>
							<div class='top_line'>
								<span class='time left'>" . calculate_weeks_ago($created) . " w ago</span>
								<span class='topic right'> {$cattitle}</span>
								<div class='clearfix'></div>
							</div>
							<div class='bottom_line bounceInUp'>
								<div class='h6'>
									<a href='{$ref}'> {$title} </a>
								</div>
								<div class='ans'>
									{$answerhtml}									
								</div>
								<div class='toolbar'>
									<div class='tools left'>
										<ul>
											<li><a href='{$ref}' class='answerjs'><i class='fa fa-pencil'></i> Answer</a></li>
										</ul>
										<ul class='list-unstyled pull-left'>
											<li>
												<a onclick='addVote(\"{$postid}\");'>
												<i class='fa fa-angle-double-up'></i><span id='span-{$postid}'>{$upvotes}</span>
												</a>
											</li>
											<li>
												<a onClick='downVote(\"{$postid}\");'>
													<i class='fa fa-angle-double-down'></i><span id='span-{$postid}-downvote'> {$downvotes} </span>
												</a>
											</li>
										</ul>
										</div>
										<div class='share right'>
										<ul>																
											<li>{$answertitle}</li>
												<li>
													<div class='tooltip-custom'>
														<i class='fa fa-ellipsis-h'></i>
														<div class='tooltip-inner tooltip-inner2'>
															<a href='#'>Invite authoritative user to answer</a></br>
															<a href='#'>Add to Reading List</a></br>
															<a href='#'>Show Related Questions</a></br>
															<a href='#'>Create Summary Answer</a></br>
															<a href='#'>Mark Favorite</a></br>
															<a href='#'>Report</a></br>
														</div>
													</div>
												</li>
											</ul>
										</div>
										<div class='clearfix'></div>
									</div>
								</div>
							</div>
						</div>
						{$htmlimage}
					</div>
					</div>";
		
		return $html;		
	}
	
	
	private function format_html_thumb_full($postid,$created, $cattitle, $title, 
											$answers, $countanswers, 
											$upvotes, $downvotes,
											$rooturl)
	/**
	*
	*/
	{
	
		
		$ref = str_replace($rooturl, "?qa=", $rooturl) . $postid . "/" . preg_replace("/[\s_]/", "-", $title);
		$answerhtml= ($answers != null) ? substr($answers[0]['content'],0,100) : "No Answer";
		$answertitle = $countanswers . (($countanswers > 1) ? " answers" : " answer") ;
		
		$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/upload/' . $postid . '.jpg';							
		$imgexist = file_exists(ISLAMIQA_PLUGIN_BASE_DIR . '/upload/' . $postid . '.jpg');
		$htmlimage =  ($imgexist) ? "<!-- Now, set the image path as source of the img element --> 
								    <div class='thumb'><img class='full' alt='' src='{$imgpath}' ></div>" 
								  : "<!-- In this case just apply the style to the question background  -->
									<div class='thumb'></div>";

		
		$html="<div class='thumb_full'>
				<div class='thumb_hover full'>
				<div class='hover_overlay'>
					<div class='inner'>
						<div class='top_line'>
							<span class='time left'>" . calculate_weeks_ago($created) . " w ago</span>
							<span class='topic right'> {$cattitle} </span>
							<div class='clearfix'></div>
						</div>
						<div class='bottom_line bounceInUp'>
							<div class='h5'>
								<a href='{$ref}'>{$title}</a>
							</div>
							<div class='ans'>
								{$answerhtml}
							</div>
						<div class='toolbar'>
							<div class='tools left'>
							<ul>
								<li><a href='{$ref}' class='answerjs'><i class='fa fa-pencil'></i> Answer</a></li>
									<ul class='list-unstyled pull-left'>
										<li><a onClick='addVote(\"{$postid}\");'>
											<i class='fa fa-angle-double-up'></i>
											<span id='span-{$postid}'> {$upvotes} </span>
											</a>
										</li>
										<li><a onclick='downVote(\"{$postid}\");'>
											<i class='fa fa-angle-double-down'></i>
											<span id='span-{$postid}-downvote' > {$downvotes} </span>
											</a>
										</li>
									</ul>
								<li><a href='javascript:void(0);'>3k views</a></li>
								<li><a href='javascript:void(0);'>235 comments</a></li>
								<li>                                                            
										<ul>
											<li>
											<div class='tooltip-social'>
												<a href='#'>Share</a>
												<div class='tooltip-inner tooltip-inner3'>
													<div class='social_icon facebook_icon'>
														<a href='#'><i class='fa fa-facebook fa-2x'></i></a>
													</div>
													<div class='social_icon twitter_icon'>
														<a href='#'><i class='fa fa-twitter fa-2x'></i></a>
													</div>
													<div class='social_icon google_icon'>
														<a href='#'><i class='fa fa-google fa-2x'></i></a>
													</div>
													<div class='social_icon instagram_icon'>
														<a href='#'><i class='fa fa-envelope fa-2x'></i></a>
													</div>
												</div>
											</div>
											</li>
										</ul>                                                       
									</li>
							</ul>
						</div>
						<div class='share right'>
							<ul>							
								<li>{$answertitle}</li>
								<li>
								<div class='tooltip-custom'>
									<i class='fa fa-ellipsis-h'></i>
									<div class='tooltip-inner tooltip-inner2'>
										<a href='#'>Invite authoritative user to answer</a><br>
										<a href='#'>Add to Reading List</a><br>
										<a href='#'>Show Related Questions</a><br>
										<a href='#'>Create Summary Answer</a><br>
										<a href='#'>Mark Favorite</a><br>
										<a href='#'>Report</a>
									</div>
									</div>
								</li>
							</ul>
							</div>
							<div class='clearfix'></div>
						</div>
					</div>
				</div>
			</div>
		    	{$htmlimage}
			</div>
		</div>";
		
	  return $html;	
	}
	
	private function format_html_thumb_column($postid,$created, $cattitle, $title, 
											  $answers, $countanswers, 
											  $upvotes, $downvotes,
											  $rooturl)
	/**
	*
	*/
	{
	$ref = str_replace($rooturl, "?qa=", $rooturl) . $postid . "/" . preg_replace("/[\s_]/", "-", $title);
	$answerhtml= ($answers != null) ? substr($answers[0]['content'],0,100) : "No Answer";
	$answertitle = $countanswers . (($countanswers > 1) ? " answers" : " answer") ;
	
	$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/upload/' . $postid . '.jpg';							
	$imgexist = file_exists(ISLAMIQA_PLUGIN_BASE_DIR . '/upload/' . $postid . '.jpg');
	$htmlimage =  ($imgexist) ? "<!-- Now, set the image path as source of the img element --> 
								<div class='thumb'><img class='bigimg' alt='' src='{$imgpath}' ></div>" 
							  : "<!-- In this case just apply the style to the question background  -->
								<div class='thumb'></div>";
	
		
	 $html="<div class='thumb_hover full_single'>
				<div class='hover_overlay'>
					<div class='inner'>
						<div class='top_line'>
							<span class='time left'>" . calculate_weeks_ago($created) . " w ago</span>
							<span class='topic right'> {$cattitle}</span>
						<div class='clearfix'></div>
					</div>
					<div class='bottom_line bounceInUp'>
						<div class='h5'>
							<a href='{$ref}'>{$title}</a>
						</div>
						<div class='ans'>
							{$answerhtml}
						</div>
						<div class='toolbar'>
							<div class='tools left'>
							  <ul>
								<li><a href='{$ref}' class='answerjs'><i class='fa fa-pencil'></i> Answer</a></li>
									<ul class='list-unstyled pull-left'>
										<li><a onClick='addVote(\"{$postid}\");'>
											<i class='fa fa-angle-double-up'></i>
											<span id='span-{$postid}'> {$upvotes}</span>
											</a>
										</li>
										<li><a onclick='downVote(\"{$postid}\");'>
											<i class='fa fa-angle-double-down'></i>
											<span id='span-{$postid}-downvote' > {$downvotes}</span>
											</a>
										</li>
									</ul>
									<li><a href='javascript:void(0);'>3k views</a></li>
									<li><a href='javascript:void(0);'>235 comments</a></li>
									<li>                                                            
										<ul>
											<li>
											<div class='tooltip-social'>
												<a href='#'>Share</a>
												<div class='tooltip-inner tooltip-inner3'>
													<div class='social_icon facebook_icon'>
														<a href='#'><i class='fa fa-facebook fa-2x'></i></a>
													</div>
													<div class='social_icon twitter_icon'>
														<a href='#'><i class='fa fa-twitter fa-2x'></i></a>
													</div>
													<div class='social_icon google_icon'>
														<a href='#'><i class='fa fa-google fa-2x'></i></a>
													</div>
													<div class='social_icon instagram_icon'>
														<a href='#'><i class='fa fa-envelope fa-2x'></i></a>
													</div>
												</div>
											</div>
											</li>
										</ul>                                                       
									</li>
								</ul>
							</div>
							<div class='share right'>
								<ul>
									<li>{$answertitle}</li>
									<li>
									<div class='tooltip-custom'>
										<i class='fa fa-ellipsis-h'></i>
									<div class='tooltip-inner tooltip-inner2'>
										<a href='#'>Invite authoritative user to answer</a><br>
										<a href='#'>Add to Reading List</a><br>
										<a href='#'>Show Related Questions</a><br>
										<a href='#'>Create Summary Answer</a><br>
										<a href='#'>Mark Favorite</a><br>
										<a href='#'>Report</a>
									</div>
									</div>
									</li>
								</ul>
							</div>
							<div class='clearfix'></div>
						</div>
					</div>
				</div>
			</div>
			{$htmlimage}
		</div>";
		
		return $html;
	}
	
	
	private function format_html_grid($show_noquestion=false,
									  $htmlhalfleft= null, $htmlhalfright = null, $htmlfull = null, $htmlfullcolumn = null,
									  $type = "",$name = "",$next_page = 0, $prev_page = 0)
	/**
	* Parameters:
	*	$show_noquestion: Show message no question
	*	$htmlhalfleft: Left side
	*	$htmlhalfright: Right side
	*/
	{  
	$display_next = "";
	$display_prev = "";
	if($prev_page <= 0 ) $display_prev = "style='display:none'";
	if($next_page <= 0 ) $display_next = "style='display:none'";
	
	$html="<div id='view_grid'>
			<div class='container popular_answer_wrapper'>
				<a onclick='prev_page(\"{$type}\",\"{$name}\",{$prev_page});' class='left-arrow' id='left' {$display_prev} ><i class='fa fa-angle-left'></i></a>
				<a onclick='next_page(\"{$type}\",\"{$name}\",{$next_page});' class='right-arrow' id='right' {$display_next} ><i class='fa fa-angle-right'></i></a>
				<div class='row' id='most_first_five'>";
									
	if ($show_noquestion) 
	{
		$html.="<div class='noqueans'>No questions and answers available</div>";
	} else {
		$html.= "<div class='col-md-6'>
				 <div class='row'>
				 	<div class='gallery_four_thumb'>
						<div class='clearfix'>
								{$htmlhalfleft}
								{$htmlhalfright}		   
						</div>
						{$htmlfull}
					</div>
				  </div> 
				</div>		
				<div class='col-md-6'>
					<div class='row'>
						<div class='thumb_full_single_right'>
							{$htmlfullcolumn}
						</div>
					 </div>
				</div>
			</div>
		</div>";
	}	
		
	 $html.="</div>";
	 
	 return $html;
	}
	
	
	private function format_html_list($show_noquestion=false, $showmore = false,
									  $questions, $answers,									  
									  $type = "",$name = "",
									  $page,$page_size, $next_page)
	/**
	* Parameters:
	*   $type: Type of request, could be unanswered, allquestions, most views, recently
	*   $name: Filter by year / month / week / all
	*   $page: Page No
	*/
	{
	 $rooturl=$this->urltoroot;
	 $html="";
	 
	 if($page == 1) $html="<div id='view_table'>";
	 $html.="<div id='row_list'>";
	
	if ($show_noquestion) 
	{
		$html.="<div class='noqueans'>No questions and answers available</div>";
	} else {	
	
	 $max=min($page_size,count($questions));
	 
     for($key=0; $key < $max; $key++)
	 {
		$question=$questions[$key]; 
		
		$postid=$question['postid'];
		$upvotes=$question['upvotes'];
		$downvotes=$question['downvotes'];
		$title=$question['title'];
		$cattitle=$question['cattitle'];
		$categoryid=$question['categoryid'];
		$created=$question['created'];

		$html_last_answer = ($answers[$key]['count'] > 0) ? 'Last Answer: ' . $answers[$key]['answers'][0]['content'] : 'No Answer';
		$html_created=(date('Y',strtotime($created)) == date('Y')) ? date("M d", strtotime($created)) : date("d M Y", strtotime($created));

		$ref = str_replace($rooturl, "?qa=", $rooturl) . $postid . "/" . preg_replace("/[\s_]/", "-", $title);
		$relatedqlink = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/ask?follow=' . $postid . '&cat=' . $categoryid;
		$html_answers= ShowWantAnswer($answers[$key]['count'], $postid, $ref);

		$imgpath = ISLAMIQA_PLUGIN_BASE_DIR_NAME . '/upload/' . $postid . '.jpg';							
		$imgexist = file_exists(ISLAMIQA_PLUGIN_BASE_DIR . '/upload/' . $postid . '.jpg');
		$htmlimage =  ($imgexist) ? "<img class='circle-imb' src='{$imgpath}'>" : "<div class='circle-imb'></div>";
		
		$html.="<!-- ** LIST VIEW STRUCTURE MOST VIEW ** -->
							<div class='container'>
								<div class='table-responsive'>
									<table class='table table-striped custom_table'>
										<tbody>
											<tr>
												<td style='width:5%;'>
													<ul class='list-unstyled'>
														<li>
															<a onClick='addVote(\"{$postid}\");'>
																<i class='fa fa-angle-double-up'></i>
																<span id=span-{$postid}> {$upvotes} </span>
															</a>
														</li>
														<li>
															 <a onClick='downVote(\"{$postid}\");'>
																<i class='fa fa-angle-double-down'></i>
																<span id=span-{$postid}-downvote> {$downvotes}</span>
															</a>
														</li>
													</ul>
												</td>
												<td colspan='4' style='width:70%;'>
												<h3 style='padding:0; margin:0;'>
													<a href='{$ref}' style='color:black;'> {$title}</a>
												</h3>
												<p style='margin:0 0 5px 0; font-size:90%;'>asked <strong>{$html_created}</strong> in <strong> {$cattitle} </strong> </p>
												<p id='more' style='padding:0; margin:0;'>
												<span class='more'>
												{$html_last_answer}									
												</span>
												</p>									
												</p>
													<ul class='list-inline'>
															<li class='active'>
																<a href='javascript:void(0);'>
																	<i class='fa fa-pencil'></i>
																	<span>                                                           
																	   <a href='{$ref}' class='answerjs'>Answer</a>
																	 </span>
																</a>
															</li>
															<li>
																<a href='javascript:void(0);'>
																		<i class='fa fa-bars'></i>
																	<span>
																		<a href='{$relatedqlink}'>Ask related question</a>
																	</span>
																</a>
															</li>
															<li>
																<div class='tooltip-social'>
																	<a href='javascript:void(0);'>Share</a>
																	<div class='tooltip-inner'>
																		<div class='social_icon facebook_icon'>
																			<a href='javascript:void(0);'><i class='fa fa-facebook fa-2x'></i></a>
																		</div>
																		<div class='social_icon twitter_icon'>
																			<a href='javascript:void(0);'><i class='fa fa-twitter fa-2x'></i></a>
																		</div>
																		<div class='social_icon google_icon'>
																			<a href='javascript:void(0);'><i class='fa fa-google fa-2x'></i></a>
																		</div>
																		<div class='social_icon instagram_icon'>
																			<a href='javascript:void(0);'><i class='fa fa-envelope fa-2x'></i></a>
																		</div>
																	</div>
																</div>                                                          
															</li>
														</ul>
											</td>
											<td style='width:10%;'>
												{$html_answers}
												<div class='tooltip-social'>
													<i class='fa fa-ellipsis-h'></i>
														<div class='tooltip-inner'>
															<a href='#'>Invite authoritative user to answer</a><br>
															<a href='#'>Add to Reading List</a><br>
															<a href='#'>Show Related Questions</a><br>
															<a href='#'>Create Summary Answer</a><br>
															<a href='#'>Mark Favorite</a><br>
															<a href='#'>Report</a>
														</div>
												</div>
											</td>
											<td style='width:10%;'>
												 {$htmlimage} 
												<!-- <a href='javascript:void(0)' class='btn btn-red'><i class='fa fa-circle'></i></a> -->
											</td>
										</tr>
									</tbody>
							</table>
						</div>
					</div>";
		}

		$html.=" </div>";
				
		$html.="<div id='row_list_new'>"; // ID to add new page and replace more questions button
		if ($showmore) $html.= "<!-- More Button -->
								<div class='more_questions' id='more_questions_most'>
								<p class='more_text'>To see more questions, click 
								<a class='questions' id='more_link_most' onclick='more_page(\"{$type}\", \"{$name}\",{$next_page});' href='javascript:void(0);'><b class='red'>here.</b></a>
								</p>
								</div>";
		$html.= "<script src='{$rooturl}js/app/readmore.js'></script>";
		$html.="</div>";		
				
		if($page == 1) $html.="</div>"; // Last div to close main section

		}
		
		return $html;
			
	}
	
	private function html_format($type, $name, $page, $view, $page_size = 4)
	/**
	* Parameters:
	*   $type: Type of request, could be unanswered, allquestions, most views, recently
	*   $name: Filter by year / month / week / all
	*   $page: Page No
	*	$view: Grid / List
	*/	
	{
	  
	   switch($type)
	   {

		   case 'unans':
				$questions=$this->get_array_questions($page, $page_size,true, null, $name, 'unanswered');
				$answers = $this->get_array_answers($questions);											
				break;
		   case 'allque':
				$questions=$this->get_array_questions($page, $page_size, true,null,$name);
				$answers = $this->get_array_answers($questions);									   
				break;
		   case 'most':
				$questions=$this->get_array_questions($page, $page_size,true, 'most',$name);
				$answers = $this->get_array_answers($questions);  		   
				break;
		   case 'recent':
				$questions=$this->get_array_questions($page, $page_size, true,'recently',$name);
				$answers = $this->get_array_answers($questions);  		   
				break;
		}
		
   	   if($view == 'grid')  /* Grid View */
		{

			$morequestions=count($questions);
			$html_left = "";
			$html_right = "";
			$html_full = "";
			$html_column = "";

			if($morequestions > 0) 
			{	
				$question=$questions[0];
				$answer=$answers[0];
				$html_left=$this->format_html_thumb_half($question['postid'],$question['created'], 
														$question['cattitle'], $question['title'], 
														$answer['answers'], $answer['count'], 
														$question['upvotes'],$question['downvotes'],
														$this->urltoroot);
			}
			if($morequestions > 1) 
			{	
				$question=$questions[1];
				$answer=$answers[1];
				$html_right=$this->format_html_thumb_half($question['postid'],$question['created'], 
														  $question['cattitle'], $question['title'], 
														  $answer['answers'], $answer['count'], 
														  $question['upvotes'],$question['downvotes'],
														  $this->urltoroot);
			}
			if($morequestions > 2) 
			{	
				$question=$questions[2];
				$answer=$answers[2];
				$html_full=$this->format_html_thumb_full($question['postid'],$question['created'], 
														 $question['cattitle'], $question['title'], 
														 $answer['answers'], $answer['count'], 
														 $question['upvotes'],$question['downvotes'],
														 $this->urltoroot);
			}
			if($morequestions > 3) 
			{	
				$question=$questions[3];
				$answer=$answers[3];
				$html_column=$this->format_html_thumb_column($question['postid'],$question['created'], 
															 $question['cattitle'], $question['title'], 
															 $answer['answers'], $answer['count'], 
															 $question['upvotes'], $question['downvotes'],
															 $this->urltoroot);
			}
			$prev=$page - 1;
			$next=($morequestions > $page_size) ? $page + 1: 0;					
			$html=$this->format_html_grid(($morequestions == 0),$html_left,$html_right,$html_full, $html_column,
										  $type,$name,$next,$prev);
		} 
		 else if($view == 'list')
		{
			$morequestions=count($questions);
			$more = ($morequestions > $page_size) ? $page + 1: 0;
			
			$html= $this->format_html_list(($morequestions == 0),($more > 0), 
											$questions, $answers,
											$type,$name, 
											$page,$page_size,$more);
		
		}
		
		/** Send to client **/
		print $html;	
	}
	
	
} /** End Class */