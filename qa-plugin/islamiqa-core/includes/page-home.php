<?php
	
/*
 * Updated by Julio A. Velasquez Basabe
 * Date: 11/06/2016
 * Time: 00:00
 */

require_once ISLAMIQA_PLUGIN_BASE_DIR.'/includes/libraries.php';

class class_home_page
  {
    var $directory;
    var $rooturl;

	public function load_module( $directory, $urltoroot )
	{
        $this->directory = $directory;
        $this->rooturl = $urltoroot;
    }
    
	public function match_request( $request ){
        
        return ($request == 'home_page');
    }
    

	public function process_request($render) 
	/*
	* Empty because is not loaded directly
	*/
	{
		$this->process($this->context);
	}
	
    public function process( $context )
	/**
	*
	*/
	{
          
	  require $this->directory ."includes/templates/home.php";
	
	  $body = (new Renderer_home())->render($context, $this->rooturl);	 
	  return $body;

   }
 

}
