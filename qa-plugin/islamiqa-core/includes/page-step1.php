<?php

require_once QA_INCLUDE_DIR . 'qa-app-users.php';
require_once QA_INCLUDE_DIR . 'qa-app-posts.php';

class class_step1 {

	var $directory;
	var $urltoroot;

	public function __construct() {

	}

	public function load_module($directory, $urltoroot) {
		$this->directory = $directory;
		$this->urltoroot = $urltoroot;
	}


	public function match_request($request) {
		$parts = explode('/', $request);

		return $parts[0] == 'step1_page';
	}

	public function process_request($request) {
		$qa_content = qa_content_prepare();

/*		unset($qa_content['widgets']);
		unset($qa_content['main']);
		unset($qa_content['direction']);
		unset($qa_content['logo']);
		unset($qa_content['sidebar']);
		unset($qa_content['sidepanel']);
		$qa_content['navigation']['main'] = array();
		$qa_content['navigation']['footer'] = array();
		$qa_content['sidebar'] = "";
*/
		unset($qa_content['qa_sidepanel']);

		require $this->directory ."includes/templates/step.php";
		
		$step = (new Renderer_step())->render($this->urltoroot);
		
		
		$qa_content['custom'] = $step;
		$qa_content['custom-body'] ="step1_page";
		
		return $qa_content;
	}
	

}

/*
Omit PHP closing tag to help avoid accidental output
*/
