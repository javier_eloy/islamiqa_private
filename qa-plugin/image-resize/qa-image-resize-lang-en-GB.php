<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/image-resize/qa-image-resize-lang-en-GB.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: UK English language phrases for image resize plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

return array(
	'imageresize_enable_label' => 'Enable Image Resize',
	'imageresize_resize_label' => 'Max size of long neighborhood',
	'imageresize_resize_desc' => 'pix',
	'imageresize_resize_error' => 'Please specify positive integer.',
	'imageresize_quality_label' => 'Image quality of resampling',
	'imageresize_quality_desc' => 'Recommend: 75',
	'imageresize_description' => 'Note: Program hack of wysiwyg editor plugin is required for this plugin.',
	'save_button' => 'Save Changes',
	'default_button' => 'Restore Default',
	'saved_message' => 'Plugin settings saved',
);

/*
	Omit PHP closing tag to help avoid accidental output
*/