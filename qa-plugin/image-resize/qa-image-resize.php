<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/image-resize/qa-image-resize.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: Page module class for image resize plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

class qa_image_resize {
	
	const PLUGIN			= 'imageresize';
	const ENABLE			= 'imageresize_enable';
	const ENABLE_DFL		= false;
	const RESIZE			= 'imageresize_resize';
	const RESIZE_DFL		= 640;
	const QUARITY			= 'imageresize_quality';
	const QUARITY_DFL		= 75;
	const DESCRIPTION		= 'imageresize_description';
	const SAVE_BUTTON		= 'imageresize_save_button';
	const DFL_BUTTON		= 'imageresize_dfl_button';

	var $directory;
	var $urltoroot;

	function load_module($directory, $urltoroot) {
		$this->directory=$directory;
		$this->urltoroot=$urltoroot;
	}
	function option_default($option) {
		if ($option==self::ENABLE) return self::ENABLE_DFL;
		if ($option==self::RESIZE) return self::RESIZE_DFL;
		if ($option==self::QUARITY) return self::QUARITY_DFL;
	}
	function admin_form(&$qa_content) {
		$saved=false;
		$error='';
		if (qa_clicked(self::SAVE_BUTTON)) {
			if(qa_post_text(self::ENABLE.'_field')) {
				if (qa_post_text(self::RESIZE.'_field') == '')
					$error = qa_lang_html(self::PLUGIN.'/'.self::RESIZE.'_error');
				if (!is_numeric(qa_post_text(self::RESIZE.'_field')))
					$error = qa_lang_html(self::PLUGIN.'/'.self::RESIZE.'_error');
			}
			if ($error == '') {
				qa_opt(self::ENABLE,(int)qa_post_text(self::ENABLE.'_field'));
				qa_opt(self::RESIZE,(int)qa_post_text(self::RESIZE.'_field'));
				qa_opt(self::QUARITY,(int)qa_post_text(self::QUARITY.'_field'));
				$saved=true;
			}
		}
		if (qa_clicked(self::DFL_BUTTON)) {
			qa_opt(self::ENABLE,self::ENABLE_DFL);
			qa_opt(self::RESIZE,self::RESIZE_DFL);
			qa_opt(self::QUARITY,self::QUARITY_DFL);
			$saved=true;
		}
		$rules = array();
		$rules[self::RESIZE] = self::ENABLE.'_field';
		$rules[self::QUARITY] = self::ENABLE.'_field';
		$rules[self::DESCRIPTION] = self::ENABLE.'_field';
		qa_set_display_rules($qa_content, $rules);

		$ret = array();
		if($saved)
			$ret['ok'] = qa_lang_html(self::PLUGIN.'/saved_message');
		else {
			if($error != '')
				$ret['ok'] = '<SPAN STYLE="color:#F00;">'.$error.'</SPAN>';
		}
		
		$fields = array();
		$fields[] = array(
			'id' => self::ENABLE,
			'label' => qa_lang_html(self::PLUGIN.'/'.self::ENABLE.'_label'),
			'type' => 'checkbox',
			'value' => (int)qa_opt(self::ENABLE),
			'tags' => 'NAME="'.self::ENABLE.'_field" ID="'.self::ENABLE.'_field"',
		);
		$fields[] = array(
			'id' => self::RESIZE,
			'label' => qa_lang_html(self::PLUGIN.'/'.self::RESIZE.'_label'),
			'type' => 'number',
			'value' => (int)qa_opt(self::RESIZE),
			'tags' => 'NAME="'.self::RESIZE.'_field" ID="'.self::RESIZE.'_field"',
			'suffix' => qa_lang_html(self::PLUGIN.'/'.self::RESIZE.'_desc'),
		);
		$fields[] = array(
			'id' => self::QUARITY,
			'label' => qa_lang_html(self::PLUGIN.'/'.self::QUARITY.'_label'),
			'type' => 'select',
			'value' => (int)qa_opt(self::QUARITY),
			'tags' => 'NAME="'.self::QUARITY.'_field"',
			'options' => array(0=>0,5=>5,10=>10,15=>15,20=>20,25=>25,30=>30,35=>35,40=>40,45=>45,50=>50,55=>55,60=>60,65=>65,70=>70,75=>75,80=>80,85=>85,90=>90,95=>95,100=>100),
			'suffix' => qa_lang_html(self::PLUGIN.'/'.self::QUARITY.'_desc'),
		);
		$fields[] = array(
			'id' => self::DESCRIPTION,
			'label' => '',
			'type' => 'custom',
			'html' => '<DIV style="border:1px dashed #ccc;margin-top:10px;padding:5px 10px;color:#666;">'.qa_lang_html(self::PLUGIN.'/'.self::DESCRIPTION).'</DIV>',
		);
		$ret['fields'] = $fields;

		$buttons = array();
		$buttons[] = array(
			'label' => qa_lang_html(self::PLUGIN.'/save_button'),
			'tags' => 'NAME="'.self::SAVE_BUTTON.'" ID="'.self::SAVE_BUTTON.'"',
		);
		$buttons[] = array(
			'label' => qa_lang_html(self::PLUGIN.'/default_button'),
			'tags' => 'NAME="'.self::DFL_BUTTON.'" ID="'.self::DFL_BUTTON.'"',
		);
		$ret['buttons'] = $buttons;

		return $ret;
	}
	function suggest_requests() {
		return array();
	}
	function match_request($request) {
		return false;
	}
	function process_request($request) {
		return qa_content_prepare();
	}

}

/*
	Omit PHP closing tag to help avoid accidental output
*/