<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/image-resize/qa-image-resize-lang-ja.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: Japanese language phrases for image resize plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

return array(
	'imageresize_enable_label' => 'イメージリサイズを有効にする',
	'imageresize_resize_label' => '長辺の最大サイズ',
	'imageresize_resize_desc' => '画素',
	'imageresize_resize_error' => '正の整数を指定してください',
	'imageresize_quality_label' => '再サンプリングの画質',
	'imageresize_quality_desc' => '推奨値: 75',
	'imageresize_description' => '注意： このプラグインはwysiwyg editorプラグインのプログラム変更が必要です。',
	'save_button' => '変更を保存',
	'default_button' => '初期値に戻す',
	'saved_message' => 'プラグインの設定を保存しました。',
);

/*
	Omit PHP closing tag to help avoid accidental output
*/