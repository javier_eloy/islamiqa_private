[Plugin hacking]

File: qa-plugin/wysiwyg-editor/qa-wysiwyg-upload.php

Find: 
					$blobid=qa_db_blob_create(file_get_contents($file['tmp_name']), $extension, @$file['name'], $userid, $cookieid, qa_remote_ip_address());

Replace: 
					if($extension == 'png' || $extension == 'gif' || $extension == 'jpeg' || $extension == 'jpg') {
						$module=qa_load_module('module', 'Image Resize');
						$copied = false;
						if(isset($module)) {
							if(qa_opt('imageresize_enable')) {
								$oldfile = $file['tmp_name'];
								$newfile = preg_replace("/.[^.]+$/","",$oldfile).'.'.$extension;
								copy($oldfile, $newfile);
								$file['tmp_name'] = $newfile;
								$copied = true;
								$resize = qa_opt('imageresize_resize');
								require_once QA_PLUGIN_DIR.'image-resize/qa-image-resize-class.php';
								$Obj = new qa_image_resize_class($file['tmp_name']);
								if($Obj->width > $resize || $Obj->height > $resize) {
									$Obj->resizeImage($resize, $resize, "auto");
									$Obj->saveImage($file['tmp_name'], qa_opt('imageresize_quality'));
								}
								unset($Obj);
							}
						}
						$blobid=qa_db_blob_create(file_get_contents($file['tmp_name']), $extension, @$file['name'], $userid, $cookieid, qa_remote_ip_address());
						if($copied) {
							unlink($newfile);
							$file['tmp_name'] = $oldfile;
						}
					} else {
						$blobid=qa_db_blob_create(file_get_contents($file['tmp_name']), $extension, @$file['name'], $userid, $cookieid, qa_remote_ip_address());
					}

