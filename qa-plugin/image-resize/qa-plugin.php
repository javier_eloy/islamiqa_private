<?php
/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/
	
	File: qa-plugin/google-cse/qa-plugin.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: Initiates google custom search plugin

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

/*
	Plugin Name: Image Resize
	Plugin URI: 
	Plugin Description: Uploaded picture is resized in specific size. 
	Plugin Version: 1.0.3
	Plugin Date: 2014-12-19
	Plugin Author: sama55@CMSBOX
	Plugin Author URI: http://www.cmsbox.jp/
	Plugin License: GPLv2
	Plugin Minimum Question2Answer Version: 1.5
	Plugin Update Check URI: 
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}

qa_register_plugin_phrases('qa-image-resize-lang-*.php', 'imageresize');
qa_register_plugin_module('module', 'qa-image-resize.php', 'qa_image_resize', 'Image Resize');

/*
	Omit PHP closing tag to help avoid accidental output
*/