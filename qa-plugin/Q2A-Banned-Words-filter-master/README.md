# Q2A Banned Words filter [![Question2Answer](http://qa-themes.com/files/q2a-logo.png)](http://www.question2answer.org/) [![Question2Answer Themes and plugins](http://qa-themes.com/files/qa-logo.jpg)](http://qa-themes.com/)

## Description
[Banned Words filter](qa-themes.com/plugins/banned-words-filter "Question2Answer Banned Words filter plugin") will send 
new posts with banned words to moderation list. it's a very useful moderation tool to keep site's content clean and prevent swear words.

## Installation

1. Download plugin.
2. extract "banned-word-filter" directory, then upload it to your Q2A site's themes directory: `qa-themes/banned-word-filter`.
3. In your Q2A site, go to **Admin > plugins** and select options for this plugin and add banned words, then save it.

## Author
This free Q2A plugin is created by [Towhid Nategheian](http://TowhidN.com "Freelance Question2Answer Developer"), from [QA-Themes.com](http://QA-Themes.com "Question2Answer Themes and Plugins"). check out our other Q2A projects:
* [Question2Answer Themes](http://QA-Themes.com "Question2Answer Themes")
* [Question2Answer Plugins](http://QA-Themes.com "Free Question2Answer Plugins")

### Copyright

this theme is not published under any license.

### About Q2A

**[Question2Answer(Q2A)](http://qa-themes.com/question2answer "Q2A Features")**  is a free and open source script for building Question & Answer communities. For more information, visit it's official site at http://www.question2answer.org/